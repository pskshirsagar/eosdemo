<?php

class CBaseInsuranceCachedRate extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_cached_rates';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intPersonalInsuranceCarrierLimitId;
	protected $m_intDeductibleInsuranceCarrierLimitId;
	protected $m_intLiabilityInsuranceCarrierLimitId;
	protected $m_strStateCode;
	protected $m_boolIsPreferred;
	protected $m_fltPremiumAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPreferred = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['personal_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonalInsuranceCarrierLimitId', trim( $arrValues['personal_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['personal_insurance_carrier_limit_id'] ) ) $this->setPersonalInsuranceCarrierLimitId( $arrValues['personal_insurance_carrier_limit_id'] );
		if( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDeductibleInsuranceCarrierLimitId', trim( $arrValues['deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) ) $this->setDeductibleInsuranceCarrierLimitId( $arrValues['deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intLiabilityInsuranceCarrierLimitId', trim( $arrValues['liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['liability_insurance_carrier_limit_id'] ) ) $this->setLiabilityInsuranceCarrierLimitId( $arrValues['liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['is_preferred'] ) && $boolDirectSet ) $this->set( 'm_boolIsPreferred', trim( stripcslashes( $arrValues['is_preferred'] ) ) ); elseif( isset( $arrValues['is_preferred'] ) ) $this->setIsPreferred( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_preferred'] ) : $arrValues['is_preferred'] );
		if( isset( $arrValues['premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumAmount', trim( $arrValues['premium_amount'] ) ); elseif( isset( $arrValues['premium_amount'] ) ) $this->setPremiumAmount( $arrValues['premium_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId ) {
		$this->set( 'm_intPersonalInsuranceCarrierLimitId', CStrings::strToIntDef( $intPersonalInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getPersonalInsuranceCarrierLimitId() {
		return $this->m_intPersonalInsuranceCarrierLimitId;
	}

	public function sqlPersonalInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intPersonalInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intPersonalInsuranceCarrierLimitId : 'NULL';
	}

	public function setDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDeductibleInsuranceCarrierLimitId() {
		return $this->m_intDeductibleInsuranceCarrierLimitId;
	}

	public function sqlDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getLiabilityInsuranceCarrierLimitId() {
		return $this->m_intLiabilityInsuranceCarrierLimitId;
	}

	public function sqlLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 50, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setIsPreferred( $boolIsPreferred ) {
		$this->set( 'm_boolIsPreferred', CStrings::strToBool( $boolIsPreferred ) );
	}

	public function getIsPreferred() {
		return $this->m_boolIsPreferred;
	}

	public function sqlIsPreferred() {
		return ( true == isset( $this->m_boolIsPreferred ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPreferred ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->set( 'm_fltPremiumAmount', CStrings::strToFloatDef( $fltPremiumAmount, NULL, false, 2 ) );
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function sqlPremiumAmount() {
		return ( true == isset( $this->m_fltPremiumAmount ) ) ? ( string ) $this->m_fltPremiumAmount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_carrier_id, insurance_policy_type_id, personal_insurance_carrier_limit_id, deductible_insurance_carrier_limit_id, liability_insurance_carrier_limit_id, state_code, is_preferred, premium_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlInsurancePolicyTypeId() . ', ' .
 						$this->sqlPersonalInsuranceCarrierLimitId() . ', ' .
 						$this->sqlDeductibleInsuranceCarrierLimitId() . ', ' .
 						$this->sqlLiabilityInsuranceCarrierLimitId() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlIsPreferred() . ', ' .
 						$this->sqlPremiumAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; } elseif( true == array_key_exists( 'PersonalInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; } elseif( true == array_key_exists( 'DeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId() . ','; } elseif( true == array_key_exists( 'LiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_preferred = ' . $this->sqlIsPreferred() . ','; } elseif( true == array_key_exists( 'IsPreferred', $this->getChangedColumns() ) ) { $strSql .= ' is_preferred = ' . $this->sqlIsPreferred() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; } elseif( true == array_key_exists( 'PremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'personal_insurance_carrier_limit_id' => $this->getPersonalInsuranceCarrierLimitId(),
			'deductible_insurance_carrier_limit_id' => $this->getDeductibleInsuranceCarrierLimitId(),
			'liability_insurance_carrier_limit_id' => $this->getLiabilityInsuranceCarrierLimitId(),
			'state_code' => $this->getStateCode(),
			'is_preferred' => $this->getIsPreferred(),
			'premium_amount' => $this->getPremiumAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>