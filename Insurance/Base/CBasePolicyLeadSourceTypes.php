<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CPolicyLeadSourceTypes
 * Do not add any new functions to this class.
 */

class CBasePolicyLeadSourceTypes extends CEosPluralBase {

	/**
	 * @return CPolicyLeadSourceType[]
	 */
	public static function fetchPolicyLeadSourceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPolicyLeadSourceType', $objDatabase );
	}

	/**
	 * @return CPolicyLeadSourceType
	 */
	public static function fetchPolicyLeadSourceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPolicyLeadSourceType', $objDatabase );
	}

	public static function fetchPolicyLeadSourceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'policy_lead_source_types', $objDatabase );
	}

	public static function fetchPolicyLeadSourceTypeById( $intId, $objDatabase ) {
		return self::fetchPolicyLeadSourceType( sprintf( 'SELECT * FROM policy_lead_source_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>