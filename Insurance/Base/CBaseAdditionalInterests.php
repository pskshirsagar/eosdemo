<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CAdditionalInterests
 * Do not add any new functions to this class.
 */

class CBaseAdditionalInterests extends CEosPluralBase {

	/**
	 * @return CAdditionalInterest[]
	 */
	public static function fetchAdditionalInterests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAdditionalInterest', $objDatabase );
	}

	/**
	 * @return CAdditionalInterest
	 */
	public static function fetchAdditionalInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAdditionalInterest', $objDatabase );
	}

	public static function fetchAdditionalInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'additional_interests', $objDatabase );
	}

	public static function fetchAdditionalInterestById( $intId, $objDatabase ) {
		return self::fetchAdditionalInterest( sprintf( 'SELECT * FROM additional_interests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>