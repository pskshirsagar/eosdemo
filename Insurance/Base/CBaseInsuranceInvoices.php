<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInvoices
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInvoices extends CEosPluralBase {

	/**
	 * @return CInsuranceInvoice[]
	 */
	public static function fetchInsuranceInvoices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceInvoice::class, $objDatabase );
	}

	/**
	 * @return CInsuranceInvoice
	 */
	public static function fetchInsuranceInvoice( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceInvoice::class, $objDatabase );
	}

	public static function fetchInsuranceInvoiceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_invoices', $objDatabase );
	}

	public static function fetchInsuranceInvoiceById( $intId, $objDatabase ) {
		return self::fetchInsuranceInvoice( sprintf( 'SELECT * FROM insurance_invoices WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceInvoicesByInsuranceInvoiceStatusTypeId( $intInsuranceInvoiceStatusTypeId, $objDatabase ) {
		return self::fetchInsuranceInvoices( sprintf( 'SELECT * FROM insurance_invoices WHERE insurance_invoice_status_type_id = %d', ( int ) $intInsuranceInvoiceStatusTypeId ), $objDatabase );
	}

	public static function fetchInsuranceInvoicesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceInvoices( sprintf( 'SELECT * FROM insurance_invoices WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

}
?>