<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyVendors
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyVendors extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyVendor[]
	 */
	public static function fetchInsurancePolicyVendors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyVendor', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyVendor
	 */
	public static function fetchInsurancePolicyVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyVendor', $objDatabase );
	}

	public static function fetchInsurancePolicyVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_vendors', $objDatabase );
	}

	public static function fetchInsurancePolicyVendorById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyVendor( sprintf( 'SELECT * FROM insurance_policy_vendors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>