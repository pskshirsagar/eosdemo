<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceFaqs
 * Do not add any new functions to this class.
 */

class CBaseInsuranceFaqs extends CEosPluralBase {

	/**
	 * @return CInsuranceFaq[]
	 */
	public static function fetchInsuranceFaqs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceFaq::class, $objDatabase );
	}

	/**
	 * @return CInsuranceFaq
	 */
	public static function fetchInsuranceFaq( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceFaq::class, $objDatabase );
	}

	public static function fetchInsuranceFaqCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_faqs', $objDatabase );
	}

	public static function fetchInsuranceFaqById( $intId, $objDatabase ) {
		return self::fetchInsuranceFaq( sprintf( 'SELECT * FROM insurance_faqs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>