<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierPreferenceKeys
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierPreferenceKeys extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierPreferenceKey[]
	 */
	public static function fetchInsuranceCarrierPreferenceKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceCarrierPreferenceKey', $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierPreferenceKey
	 */
	public static function fetchInsuranceCarrierPreferenceKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceCarrierPreferenceKey', $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferenceKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_preference_keys', $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferenceKeyById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierPreferenceKey( sprintf( 'SELECT * FROM insurance_carrier_preference_keys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>