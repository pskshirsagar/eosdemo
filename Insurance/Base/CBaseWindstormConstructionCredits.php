<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CWindstormConstructionCredits
 * Do not add any new functions to this class.
 */

class CBaseWindstormConstructionCredits extends CEosPluralBase {

	/**
	 * @return CWindstormConstructionCredit[]
	 */
	public static function fetchWindstormConstructionCredits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CWindstormConstructionCredit::class, $objDatabase );
	}

	/**
	 * @return CWindstormConstructionCredit
	 */
	public static function fetchWindstormConstructionCredit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CWindstormConstructionCredit::class, $objDatabase );
	}

	public static function fetchWindstormConstructionCreditCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'windstorm_construction_credits', $objDatabase );
	}

	public static function fetchWindstormConstructionCreditById( $intId, $objDatabase ) {
		return self::fetchWindstormConstructionCredit( sprintf( 'SELECT * FROM windstorm_construction_credits WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>