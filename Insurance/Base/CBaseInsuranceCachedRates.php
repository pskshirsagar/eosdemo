<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCachedRates
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCachedRates extends CEosPluralBase {

	/**
	 * @return CInsuranceCachedRate[]
	 */
	public static function fetchInsuranceCachedRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCachedRate::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCachedRate
	 */
	public static function fetchInsuranceCachedRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCachedRate::class, $objDatabase );
	}

	public static function fetchInsuranceCachedRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_cached_rates', $objDatabase );
	}

	public static function fetchInsuranceCachedRateById( $intId, $objDatabase ) {
		return self::fetchInsuranceCachedRate( sprintf( 'SELECT * FROM insurance_cached_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCachedRatesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCachedRates( sprintf( 'SELECT * FROM insurance_cached_rates WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCachedRatesByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceCachedRates( sprintf( 'SELECT * FROM insurance_cached_rates WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCachedRatesByPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCachedRates( sprintf( 'SELECT * FROM insurance_cached_rates WHERE personal_insurance_carrier_limit_id = %d', ( int ) $intPersonalInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsuranceCachedRatesByDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCachedRates( sprintf( 'SELECT * FROM insurance_cached_rates WHERE deductible_insurance_carrier_limit_id = %d', ( int ) $intDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsuranceCachedRatesByLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCachedRates( sprintf( 'SELECT * FROM insurance_cached_rates WHERE liability_insurance_carrier_limit_id = %d', ( int ) $intLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

}
?>