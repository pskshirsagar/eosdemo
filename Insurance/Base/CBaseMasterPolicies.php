<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicies
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicies extends CEosPluralBase {

	/**
	 * @return CMasterPolicy[]
	 */
	public static function fetchMasterPolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicy::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicy
	 */
	public static function fetchMasterPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicy::class, $objDatabase );
	}

	public static function fetchMasterPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policies', $objDatabase );
	}

	public static function fetchMasterPolicyByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMasterPolicy( sprintf( 'SELECT * FROM master_policies WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMasterPoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMasterPoliciesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE unit_space_id = %d', $intUnitSpaceId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByPolicyCarrierId( $intPolicyCarrierId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE policy_carrier_id = %d', $intPolicyCarrierId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE insurance_policy_type_id = %d', $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByPolicyStatusTypeId( $intPolicyStatusTypeId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE policy_status_type_id = %d', $intPolicyStatusTypeId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByLiabilityCarrierLimitId( $intLiabilityCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE liability_carrier_limit_id = %d', $intLiabilityCarrierLimitId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByPersonalCarrierLimitId( $intPersonalCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE personal_carrier_limit_id = %d', $intPersonalCarrierLimitId ), $objDatabase );
	}

	public static function fetchMasterPoliciesByDeductibleCarrierLimitId( $intDeductibleCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicies( sprintf( 'SELECT * FROM master_policies WHERE deductible_carrier_limit_id = %d', $intDeductibleCarrierLimitId ), $objDatabase );
	}

}
?>