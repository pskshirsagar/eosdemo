<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicies
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicies extends CEosPluralBase {

	/**
	 * @return CInsurancePolicy[]
	 */
	public static function fetchInsurancePolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicy::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicy
	 */
	public static function fetchInsurancePolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicy::class, $objDatabase );
	}

	public static function fetchInsurancePolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policies', $objDatabase );
	}

	public static function fetchInsurancePolicyById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicy( sprintf( 'SELECT * FROM insurance_policies WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE entity_id = %d', $intEntityId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_carrier_classification_type_id = %d', $intInsuranceCarrierClassificationTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE account_id = %d', $intAccountId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_carrier_id = %d', $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_policy_type_id = %d', $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByFrequencyId( $intFrequencyId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE frequency_id = %d', $intFrequencyId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByLeadSourceTypeId( $intLeadSourceTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE lead_source_type_id = %d', $intLeadSourceTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_policy_status_type_id = %d', $intInsurancePolicyStatusTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsurancePolicyCancellationTypeId( $intInsurancePolicyCancellationTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_policy_cancellation_type_id = %d', $intInsurancePolicyCancellationTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByInsuranceTermsId( $intInsuranceTermsId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE insurance_terms_id = %d', $intInsuranceTermsId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByPaymentTimeoutTypeId( $strPaymentTimeoutTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE payment_timeout_type_id = \'%s\'', $strPaymentTimeoutTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByPaymentTypeId( $strPaymentTypeId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE payment_type_id = \'%s\'', $strPaymentTypeId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchInsurancePoliciesByOriginalPropertyId( $intOriginalPropertyId, $objDatabase ) {
		return self::fetchInsurancePolicies( sprintf( 'SELECT * FROM insurance_policies WHERE original_property_id = %d', $intOriginalPropertyId ), $objDatabase );
	}

}
?>