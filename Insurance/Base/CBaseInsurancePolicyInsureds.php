<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyInsureds
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyInsureds extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyInsured[]
	 */
	public static function fetchInsurancePolicyInsureds( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyInsured::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyInsured
	 */
	public static function fetchInsurancePolicyInsured( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyInsured::class, $objDatabase );
	}

	public static function fetchInsurancePolicyInsuredCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_insureds', $objDatabase );
	}

	public static function fetchInsurancePolicyInsuredById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyInsured( sprintf( 'SELECT * FROM insurance_policy_insureds WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyInsuredsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyInsureds( sprintf( 'SELECT * FROM insurance_policy_insureds WHERE insurance_policy_id = %d', $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyInsuredsByInsurancePolicyCoverageDetailId( $intInsurancePolicyCoverageDetailId, $objDatabase ) {
		return self::fetchInsurancePolicyInsureds( sprintf( 'SELECT * FROM insurance_policy_insureds WHERE insurance_policy_coverage_detail_id = %d', $intInsurancePolicyCoverageDetailId ), $objDatabase );
	}

	public static function fetchInsurancePolicyInsuredsByInsuranceInsuredTypeId( $intInsuranceInsuredTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyInsureds( sprintf( 'SELECT * FROM insurance_policy_insureds WHERE insurance_insured_type_id = %d', $intInsuranceInsuredTypeId ), $objDatabase );
	}

}
?>