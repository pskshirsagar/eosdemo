<?php

class CBaseInsurancePolicyQuoteError extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_quote_errors';

	protected $m_intId;
	protected $m_intQuoteId;
	protected $m_strErrorCode;
	protected $m_strError;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['quote_id'] ) && $boolDirectSet ) $this->set( 'm_intQuoteId', trim( $arrValues['quote_id'] ) ); elseif( isset( $arrValues['quote_id'] ) ) $this->setQuoteId( $arrValues['quote_id'] );
		if( isset( $arrValues['error_code'] ) && $boolDirectSet ) $this->set( 'm_strErrorCode', trim( stripcslashes( $arrValues['error_code'] ) ) ); elseif( isset( $arrValues['error_code'] ) ) $this->setErrorCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_code'] ) : $arrValues['error_code'] );
		if( isset( $arrValues['error'] ) && $boolDirectSet ) $this->set( 'm_strError', trim( stripcslashes( $arrValues['error'] ) ) ); elseif( isset( $arrValues['error'] ) ) $this->setError( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error'] ) : $arrValues['error'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setQuoteId( $intQuoteId ) {
		$this->set( 'm_intQuoteId', CStrings::strToIntDef( $intQuoteId, NULL, false ) );
	}

	public function getQuoteId() {
		return $this->m_intQuoteId;
	}

	public function sqlQuoteId() {
		return ( true == isset( $this->m_intQuoteId ) ) ? ( string ) $this->m_intQuoteId : 'NULL';
	}

	public function setErrorCode( $strErrorCode ) {
		$this->set( 'm_strErrorCode', CStrings::strTrimDef( $strErrorCode, 200, NULL, true ) );
	}

	public function getErrorCode() {
		return $this->m_strErrorCode;
	}

	public function sqlErrorCode() {
		return ( true == isset( $this->m_strErrorCode ) ) ? '\'' . addslashes( $this->m_strErrorCode ) . '\'' : 'NULL';
	}

	public function setError( $strError ) {
		$this->set( 'm_strError', CStrings::strTrimDef( $strError, -1, NULL, true ) );
	}

	public function getError() {
		return $this->m_strError;
	}

	public function sqlError() {
		return ( true == isset( $this->m_strError ) ) ? '\'' . addslashes( $this->m_strError ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, quote_id, error_code, error, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlQuoteId() . ', ' .
 						$this->sqlErrorCode() . ', ' .
 						$this->sqlError() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; } elseif( true == array_key_exists( 'QuoteId', $this->getChangedColumns() ) ) { $strSql .= ' quote_id = ' . $this->sqlQuoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; } elseif( true == array_key_exists( 'ErrorCode', $this->getChangedColumns() ) ) { $strSql .= ' error_code = ' . $this->sqlErrorCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error = ' . $this->sqlError() . ','; } elseif( true == array_key_exists( 'Error', $this->getChangedColumns() ) ) { $strSql .= ' error = ' . $this->sqlError() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'quote_id' => $this->getQuoteId(),
			'error_code' => $this->getErrorCode(),
			'error' => $this->getError(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>