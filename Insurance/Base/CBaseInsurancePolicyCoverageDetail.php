<?php

class CBaseInsurancePolicyCoverageDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_coverage_details';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_intDeductibleInsuranceCarrierLimitId;
	protected $m_intLiabilityInsuranceCarrierLimitId;
	protected $m_intPersonalInsuranceCarrierLimitId;
	protected $m_intInsurancePolicyDetailStatusTypeId;
	protected $m_strCarrierPolicyNumber;
	protected $m_fltPremiumAmount;
	protected $m_fltAnnualPremiumAmount;
	protected $m_fltAnnualPremiumAmountPif;
	protected $m_fltLiabilityPremiumAmount;
	protected $m_fltPersonalPremiumAmount;
	protected $m_fltPremiumTaxAmount;
	protected $m_fltAdminFee;
	protected $m_fltPropertyFee;
	protected $m_boolIsReturnAdminFee;
	protected $m_strUnitNumber;
	protected $m_strStreetLine1;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strCountryCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strMailingStreetLine1;
	protected $m_strMailingCity;
	protected $m_strMailingStateCode;
	protected $m_strMailingPostalCode;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intMedicalPaymentLimitId;
	protected $m_intHurricaneDeductibleInsuranceCarrierLimitId;
	protected $m_fltWindstormConstructionCreditValue;
	protected $m_fltBuildingCodeGradingValue;
	protected $m_fltAutomaticSprinklerCreditValue;
	protected $m_fltStatePolicyFee;
	protected $m_boolIsAddressFailed;
	protected $m_fltNonHurricanePremium;

	public function __construct() {
		parent::__construct();

		$this->m_fltAnnualPremiumAmount = NULL;
		$this->m_fltPremiumTaxAmount = NULL;
		$this->m_fltAdminFee = '2.49';
		$this->m_fltPropertyFee = '0';
		$this->m_boolIsReturnAdminFee = false;
		$this->m_fltWindstormConstructionCreditValue = NULL;
		$this->m_fltBuildingCodeGradingValue = NULL;
		$this->m_fltAutomaticSprinklerCreditValue = NULL;
		$this->m_boolIsAddressFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDeductibleInsuranceCarrierLimitId', trim( $arrValues['deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) ) $this->setDeductibleInsuranceCarrierLimitId( $arrValues['deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intLiabilityInsuranceCarrierLimitId', trim( $arrValues['liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['liability_insurance_carrier_limit_id'] ) ) $this->setLiabilityInsuranceCarrierLimitId( $arrValues['liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['personal_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonalInsuranceCarrierLimitId', trim( $arrValues['personal_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['personal_insurance_carrier_limit_id'] ) ) $this->setPersonalInsuranceCarrierLimitId( $arrValues['personal_insurance_carrier_limit_id'] );
		if( isset( $arrValues['insurance_policy_detail_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyDetailStatusTypeId', trim( $arrValues['insurance_policy_detail_status_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_detail_status_type_id'] ) ) $this->setInsurancePolicyDetailStatusTypeId( $arrValues['insurance_policy_detail_status_type_id'] );
		if( isset( $arrValues['carrier_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strCarrierPolicyNumber', trim( stripcslashes( $arrValues['carrier_policy_number'] ) ) ); elseif( isset( $arrValues['carrier_policy_number'] ) ) $this->setCarrierPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['carrier_policy_number'] ) : $arrValues['carrier_policy_number'] );
		if( isset( $arrValues['premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumAmount', trim( $arrValues['premium_amount'] ) ); elseif( isset( $arrValues['premium_amount'] ) ) $this->setPremiumAmount( $arrValues['premium_amount'] );
		if( isset( $arrValues['annual_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAnnualPremiumAmount', trim( $arrValues['annual_premium_amount'] ) ); elseif( isset( $arrValues['annual_premium_amount'] ) ) $this->setAnnualPremiumAmount( $arrValues['annual_premium_amount'] );
		if( isset( $arrValues['annual_premium_amount_pif'] ) && $boolDirectSet ) $this->set( 'm_fltAnnualPremiumAmountPif', trim( $arrValues['annual_premium_amount_pif'] ) ); elseif( isset( $arrValues['annual_premium_amount_pif'] ) ) $this->setAnnualPremiumAmountPif( $arrValues['annual_premium_amount_pif'] );
		if( isset( $arrValues['liability_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltLiabilityPremiumAmount', trim( $arrValues['liability_premium_amount'] ) ); elseif( isset( $arrValues['liability_premium_amount'] ) ) $this->setLiabilityPremiumAmount( $arrValues['liability_premium_amount'] );
		if( isset( $arrValues['personal_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPersonalPremiumAmount', trim( $arrValues['personal_premium_amount'] ) ); elseif( isset( $arrValues['personal_premium_amount'] ) ) $this->setPersonalPremiumAmount( $arrValues['personal_premium_amount'] );
		if( isset( $arrValues['premium_tax_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumTaxAmount', trim( $arrValues['premium_tax_amount'] ) ); elseif( isset( $arrValues['premium_tax_amount'] ) ) $this->setPremiumTaxAmount( $arrValues['premium_tax_amount'] );
		if( isset( $arrValues['admin_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAdminFee', trim( $arrValues['admin_fee'] ) ); elseif( isset( $arrValues['admin_fee'] ) ) $this->setAdminFee( $arrValues['admin_fee'] );
		if( isset( $arrValues['property_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPropertyFee', trim( $arrValues['property_fee'] ) ); elseif( isset( $arrValues['property_fee'] ) ) $this->setPropertyFee( $arrValues['property_fee'] );
		if( isset( $arrValues['is_return_admin_fee'] ) && $boolDirectSet ) $this->set( 'm_boolIsReturnAdminFee', trim( stripcslashes( $arrValues['is_return_admin_fee'] ) ) ); elseif( isset( $arrValues['is_return_admin_fee'] ) ) $this->setIsReturnAdminFee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_return_admin_fee'] ) : $arrValues['is_return_admin_fee'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['mailing_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strMailingStreetLine1', trim( stripcslashes( $arrValues['mailing_street_line1'] ) ) ); elseif( isset( $arrValues['mailing_street_line1'] ) ) $this->setMailingStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_street_line1'] ) : $arrValues['mailing_street_line1'] );
		if( isset( $arrValues['mailing_city'] ) && $boolDirectSet ) $this->set( 'm_strMailingCity', trim( stripcslashes( $arrValues['mailing_city'] ) ) ); elseif( isset( $arrValues['mailing_city'] ) ) $this->setMailingCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_city'] ) : $arrValues['mailing_city'] );
		if( isset( $arrValues['mailing_state_code'] ) && $boolDirectSet ) $this->set( 'm_strMailingStateCode', trim( stripcslashes( $arrValues['mailing_state_code'] ) ) ); elseif( isset( $arrValues['mailing_state_code'] ) ) $this->setMailingStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_state_code'] ) : $arrValues['mailing_state_code'] );
		if( isset( $arrValues['mailing_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strMailingPostalCode', trim( stripcslashes( $arrValues['mailing_postal_code'] ) ) ); elseif( isset( $arrValues['mailing_postal_code'] ) ) $this->setMailingPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_postal_code'] ) : $arrValues['mailing_postal_code'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['medical_payment_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMedicalPaymentLimitId', trim( $arrValues['medical_payment_limit_id'] ) ); elseif( isset( $arrValues['medical_payment_limit_id'] ) ) $this->setMedicalPaymentLimitId( $arrValues['medical_payment_limit_id'] );
		if( isset( $arrValues['hurricane_deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intHurricaneDeductibleInsuranceCarrierLimitId', trim( $arrValues['hurricane_deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['hurricane_deductible_insurance_carrier_limit_id'] ) ) $this->setHurricaneDeductibleInsuranceCarrierLimitId( $arrValues['hurricane_deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['windstorm_construction_credit_value'] ) && $boolDirectSet ) $this->set( 'm_fltWindstormConstructionCreditValue', trim( $arrValues['windstorm_construction_credit_value'] ) ); elseif( isset( $arrValues['windstorm_construction_credit_value'] ) ) $this->setWindstormConstructionCreditValue( $arrValues['windstorm_construction_credit_value'] );
		if( isset( $arrValues['building_code_grading_value'] ) && $boolDirectSet ) $this->set( 'm_fltBuildingCodeGradingValue', trim( $arrValues['building_code_grading_value'] ) ); elseif( isset( $arrValues['building_code_grading_value'] ) ) $this->setBuildingCodeGradingValue( $arrValues['building_code_grading_value'] );
		if( isset( $arrValues['automatic_sprinkler_credit_value'] ) && $boolDirectSet ) $this->set( 'm_fltAutomaticSprinklerCreditValue', trim( $arrValues['automatic_sprinkler_credit_value'] ) ); elseif( isset( $arrValues['automatic_sprinkler_credit_value'] ) ) $this->setAutomaticSprinklerCreditValue( $arrValues['automatic_sprinkler_credit_value'] );
		if( isset( $arrValues['state_policy_fee'] ) && $boolDirectSet ) $this->set( 'm_fltStatePolicyFee', trim( $arrValues['state_policy_fee'] ) ); elseif( isset( $arrValues['state_policy_fee'] ) ) $this->setStatePolicyFee( $arrValues['state_policy_fee'] );
		if( isset( $arrValues['is_address_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsAddressFailed', trim( stripcslashes( $arrValues['is_address_failed'] ) ) ); elseif( isset( $arrValues['is_address_failed'] ) ) $this->setIsAddressFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_address_failed'] ) : $arrValues['is_address_failed'] );
		if( isset( $arrValues['non_hurricane_premium'] ) && $boolDirectSet ) $this->set( 'm_fltNonHurricanePremium', trim( $arrValues['non_hurricane_premium'] ) ); elseif( isset( $arrValues['non_hurricane_premium'] ) ) $this->setNonHurricanePremium( $arrValues['non_hurricane_premium'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDeductibleInsuranceCarrierLimitId() {
		return $this->m_intDeductibleInsuranceCarrierLimitId;
	}

	public function sqlDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getLiabilityInsuranceCarrierLimitId() {
		return $this->m_intLiabilityInsuranceCarrierLimitId;
	}

	public function sqlLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId ) {
		$this->set( 'm_intPersonalInsuranceCarrierLimitId', CStrings::strToIntDef( $intPersonalInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getPersonalInsuranceCarrierLimitId() {
		return $this->m_intPersonalInsuranceCarrierLimitId;
	}

	public function sqlPersonalInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intPersonalInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intPersonalInsuranceCarrierLimitId : 'NULL';
	}

	public function setInsurancePolicyDetailStatusTypeId( $intInsurancePolicyDetailStatusTypeId ) {
		$this->set( 'm_intInsurancePolicyDetailStatusTypeId', CStrings::strToIntDef( $intInsurancePolicyDetailStatusTypeId, NULL, false ) );
	}

	public function getInsurancePolicyDetailStatusTypeId() {
		return $this->m_intInsurancePolicyDetailStatusTypeId;
	}

	public function sqlInsurancePolicyDetailStatusTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyDetailStatusTypeId ) ) ? ( string ) $this->m_intInsurancePolicyDetailStatusTypeId : 'NULL';
	}

	public function setCarrierPolicyNumber( $strCarrierPolicyNumber ) {
		$this->set( 'm_strCarrierPolicyNumber', CStrings::strTrimDef( $strCarrierPolicyNumber, 64, NULL, true ) );
	}

	public function getCarrierPolicyNumber() {
		return $this->m_strCarrierPolicyNumber;
	}

	public function sqlCarrierPolicyNumber() {
		return ( true == isset( $this->m_strCarrierPolicyNumber ) ) ? '\'' . addslashes( $this->m_strCarrierPolicyNumber ) . '\'' : 'NULL';
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->set( 'm_fltPremiumAmount', CStrings::strToFloatDef( $fltPremiumAmount, NULL, false, 2 ) );
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function sqlPremiumAmount() {
		return ( true == isset( $this->m_fltPremiumAmount ) ) ? ( string ) $this->m_fltPremiumAmount : 'NULL';
	}

	public function setAnnualPremiumAmount( $fltAnnualPremiumAmount ) {
		$this->set( 'm_fltAnnualPremiumAmount', CStrings::strToFloatDef( $fltAnnualPremiumAmount, NULL, false, 2 ) );
	}

	public function getAnnualPremiumAmount() {
		return $this->m_fltAnnualPremiumAmount;
	}

	public function sqlAnnualPremiumAmount() {
		return ( true == isset( $this->m_fltAnnualPremiumAmount ) ) ? ( string ) $this->m_fltAnnualPremiumAmount : 'NULL::numeric';
	}

	public function setAnnualPremiumAmountPif( $fltAnnualPremiumAmountPif ) {
		$this->set( 'm_fltAnnualPremiumAmountPif', CStrings::strToFloatDef( $fltAnnualPremiumAmountPif, NULL, false, 2 ) );
	}

	public function getAnnualPremiumAmountPif() {
		return $this->m_fltAnnualPremiumAmountPif;
	}

	public function sqlAnnualPremiumAmountPif() {
		return ( true == isset( $this->m_fltAnnualPremiumAmountPif ) ) ? ( string ) $this->m_fltAnnualPremiumAmountPif : 'NULL';
	}

	public function setLiabilityPremiumAmount( $fltLiabilityPremiumAmount ) {
		$this->set( 'm_fltLiabilityPremiumAmount', CStrings::strToFloatDef( $fltLiabilityPremiumAmount, NULL, false, 2 ) );
	}

	public function getLiabilityPremiumAmount() {
		return $this->m_fltLiabilityPremiumAmount;
	}

	public function sqlLiabilityPremiumAmount() {
		return ( true == isset( $this->m_fltLiabilityPremiumAmount ) ) ? ( string ) $this->m_fltLiabilityPremiumAmount : 'NULL';
	}

	public function setPersonalPremiumAmount( $fltPersonalPremiumAmount ) {
		$this->set( 'm_fltPersonalPremiumAmount', CStrings::strToFloatDef( $fltPersonalPremiumAmount, NULL, false, 2 ) );
	}

	public function getPersonalPremiumAmount() {
		return $this->m_fltPersonalPremiumAmount;
	}

	public function sqlPersonalPremiumAmount() {
		return ( true == isset( $this->m_fltPersonalPremiumAmount ) ) ? ( string ) $this->m_fltPersonalPremiumAmount : 'NULL';
	}

	public function setPremiumTaxAmount( $fltPremiumTaxAmount ) {
		$this->set( 'm_fltPremiumTaxAmount', CStrings::strToFloatDef( $fltPremiumTaxAmount, NULL, false, 2 ) );
	}

	public function getPremiumTaxAmount() {
		return $this->m_fltPremiumTaxAmount;
	}

	public function sqlPremiumTaxAmount() {
		return ( true == isset( $this->m_fltPremiumTaxAmount ) ) ? ( string ) $this->m_fltPremiumTaxAmount : 'NULL::numeric';
	}

	public function setAdminFee( $fltAdminFee ) {
		$this->set( 'm_fltAdminFee', CStrings::strToFloatDef( $fltAdminFee, NULL, false, 2 ) );
	}

	public function getAdminFee() {
		return $this->m_fltAdminFee;
	}

	public function sqlAdminFee() {
		return ( true == isset( $this->m_fltAdminFee ) ) ? ( string ) $this->m_fltAdminFee : '2.49';
	}

	public function setPropertyFee( $fltPropertyFee ) {
		$this->set( 'm_fltPropertyFee', CStrings::strToFloatDef( $fltPropertyFee, NULL, false, 2 ) );
	}

	public function getPropertyFee() {
		return $this->m_fltPropertyFee;
	}

	public function sqlPropertyFee() {
		return ( true == isset( $this->m_fltPropertyFee ) ) ? ( string ) $this->m_fltPropertyFee : '0';
	}

	public function setIsReturnAdminFee( $boolIsReturnAdminFee ) {
		$this->set( 'm_boolIsReturnAdminFee', CStrings::strToBool( $boolIsReturnAdminFee ) );
	}

	public function getIsReturnAdminFee() {
		return $this->m_boolIsReturnAdminFee;
	}

	public function sqlIsReturnAdminFee() {
		return ( true == isset( $this->m_boolIsReturnAdminFee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReturnAdminFee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 50, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setMailingStreetLine1( $strMailingStreetLine1 ) {
		$this->set( 'm_strMailingStreetLine1', CStrings::strTrimDef( $strMailingStreetLine1, 100, NULL, true ) );
	}

	public function getMailingStreetLine1() {
		return $this->m_strMailingStreetLine1;
	}

	public function sqlMailingStreetLine1() {
		return ( true == isset( $this->m_strMailingStreetLine1 ) ) ? '\'' . addslashes( $this->m_strMailingStreetLine1 ) . '\'' : 'NULL';
	}

	public function setMailingCity( $strMailingCity ) {
		$this->set( 'm_strMailingCity', CStrings::strTrimDef( $strMailingCity, 50, NULL, true ) );
	}

	public function getMailingCity() {
		return $this->m_strMailingCity;
	}

	public function sqlMailingCity() {
		return ( true == isset( $this->m_strMailingCity ) ) ? '\'' . addslashes( $this->m_strMailingCity ) . '\'' : 'NULL';
	}

	public function setMailingStateCode( $strMailingStateCode ) {
		$this->set( 'm_strMailingStateCode', CStrings::strTrimDef( $strMailingStateCode, 2, NULL, true ) );
	}

	public function getMailingStateCode() {
		return $this->m_strMailingStateCode;
	}

	public function sqlMailingStateCode() {
		return ( true == isset( $this->m_strMailingStateCode ) ) ? '\'' . addslashes( $this->m_strMailingStateCode ) . '\'' : 'NULL';
	}

	public function setMailingPostalCode( $strMailingPostalCode ) {
		$this->set( 'm_strMailingPostalCode', CStrings::strTrimDef( $strMailingPostalCode, 20, NULL, true ) );
	}

	public function getMailingPostalCode() {
		return $this->m_strMailingPostalCode;
	}

	public function sqlMailingPostalCode() {
		return ( true == isset( $this->m_strMailingPostalCode ) ) ? '\'' . addslashes( $this->m_strMailingPostalCode ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setMedicalPaymentLimitId( $intMedicalPaymentLimitId ) {
		$this->set( 'm_intMedicalPaymentLimitId', CStrings::strToIntDef( $intMedicalPaymentLimitId, NULL, false ) );
	}

	public function getMedicalPaymentLimitId() {
		return $this->m_intMedicalPaymentLimitId;
	}

	public function sqlMedicalPaymentLimitId() {
		return ( true == isset( $this->m_intMedicalPaymentLimitId ) ) ? ( string ) $this->m_intMedicalPaymentLimitId : 'NULL';
	}

	public function setHurricaneDeductibleInsuranceCarrierLimitId( $intHurricaneDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intHurricaneDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intHurricaneDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getHurricaneDeductibleInsuranceCarrierLimitId() {
		return $this->m_intHurricaneDeductibleInsuranceCarrierLimitId;
	}

	public function sqlHurricaneDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intHurricaneDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intHurricaneDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setWindstormConstructionCreditValue( $fltWindstormConstructionCreditValue ) {
		$this->set( 'm_fltWindstormConstructionCreditValue', CStrings::strToFloatDef( $fltWindstormConstructionCreditValue, NULL, false, 4 ) );
	}

	public function getWindstormConstructionCreditValue() {
		return $this->m_fltWindstormConstructionCreditValue;
	}

	public function sqlWindstormConstructionCreditValue() {
		return ( true == isset( $this->m_fltWindstormConstructionCreditValue ) ) ? ( string ) $this->m_fltWindstormConstructionCreditValue : 'NULL::numeric';
	}

	public function setBuildingCodeGradingValue( $fltBuildingCodeGradingValue ) {
		$this->set( 'm_fltBuildingCodeGradingValue', CStrings::strToFloatDef( $fltBuildingCodeGradingValue, NULL, false, 2 ) );
	}

	public function getBuildingCodeGradingValue() {
		return $this->m_fltBuildingCodeGradingValue;
	}

	public function sqlBuildingCodeGradingValue() {
		return ( true == isset( $this->m_fltBuildingCodeGradingValue ) ) ? ( string ) $this->m_fltBuildingCodeGradingValue : 'NULL::numeric';
	}

	public function setAutomaticSprinklerCreditValue( $fltAutomaticSprinklerCreditValue ) {
		$this->set( 'm_fltAutomaticSprinklerCreditValue', CStrings::strToFloatDef( $fltAutomaticSprinklerCreditValue, NULL, false, 2 ) );
	}

	public function getAutomaticSprinklerCreditValue() {
		return $this->m_fltAutomaticSprinklerCreditValue;
	}

	public function sqlAutomaticSprinklerCreditValue() {
		return ( true == isset( $this->m_fltAutomaticSprinklerCreditValue ) ) ? ( string ) $this->m_fltAutomaticSprinklerCreditValue : 'NULL::numeric';
	}

	public function setStatePolicyFee( $fltStatePolicyFee ) {
		$this->set( 'm_fltStatePolicyFee', CStrings::strToFloatDef( $fltStatePolicyFee, NULL, false, 2 ) );
	}

	public function getStatePolicyFee() {
		return $this->m_fltStatePolicyFee;
	}

	public function sqlStatePolicyFee() {
		return ( true == isset( $this->m_fltStatePolicyFee ) ) ? ( string ) $this->m_fltStatePolicyFee : 'NULL';
	}

	public function setIsAddressFailed( $boolIsAddressFailed ) {
		$this->set( 'm_boolIsAddressFailed', CStrings::strToBool( $boolIsAddressFailed ) );
	}

	public function getIsAddressFailed() {
		return $this->m_boolIsAddressFailed;
	}

	public function sqlIsAddressFailed() {
		return ( true == isset( $this->m_boolIsAddressFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAddressFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNonHurricanePremium( $fltNonHurricanePremium ) {
		$this->set( 'm_fltNonHurricanePremium', CStrings::strToFloatDef( $fltNonHurricanePremium, NULL, false, 2 ) );
	}

	public function getNonHurricanePremium() {
		return $this->m_fltNonHurricanePremium;
	}

	public function sqlNonHurricanePremium() {
		return ( true == isset( $this->m_fltNonHurricanePremium ) ) ? ( string ) $this->m_fltNonHurricanePremium : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, deductible_insurance_carrier_limit_id, liability_insurance_carrier_limit_id, personal_insurance_carrier_limit_id, insurance_policy_detail_status_type_id, carrier_policy_number, premium_amount, annual_premium_amount, annual_premium_amount_pif, liability_premium_amount, personal_premium_amount, premium_tax_amount, admin_fee, property_fee, is_return_admin_fee, unit_number, street_line1, city, state_code, country_code, province, postal_code, mailing_street_line1, mailing_city, mailing_state_code, mailing_postal_code, start_date, end_date, updated_by, updated_on, created_by, created_on, medical_payment_limit_id, hurricane_deductible_insurance_carrier_limit_id, windstorm_construction_credit_value, building_code_grading_value, automatic_sprinkler_credit_value, state_policy_fee, is_address_failed, non_hurricane_premium )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlInsurancePolicyId() . ', ' .
						$this->sqlDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlPersonalInsuranceCarrierLimitId() . ', ' .
						$this->sqlInsurancePolicyDetailStatusTypeId() . ', ' .
						$this->sqlCarrierPolicyNumber() . ', ' .
						$this->sqlPremiumAmount() . ', ' .
						$this->sqlAnnualPremiumAmount() . ', ' .
						$this->sqlAnnualPremiumAmountPif() . ', ' .
						$this->sqlLiabilityPremiumAmount() . ', ' .
						$this->sqlPersonalPremiumAmount() . ', ' .
						$this->sqlPremiumTaxAmount() . ', ' .
						$this->sqlAdminFee() . ', ' .
						$this->sqlPropertyFee() . ', ' .
						$this->sqlIsReturnAdminFee() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlProvince() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlMailingStreetLine1() . ', ' .
						$this->sqlMailingCity() . ', ' .
						$this->sqlMailingStateCode() . ', ' .
						$this->sqlMailingPostalCode() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlMedicalPaymentLimitId() . ', ' .
						$this->sqlHurricaneDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlWindstormConstructionCreditValue() . ', ' .
						$this->sqlBuildingCodeGradingValue() . ', ' .
						$this->sqlAutomaticSprinklerCreditValue() . ', ' .
						$this->sqlStatePolicyFee() . ', ' .
						$this->sqlIsAddressFailed() . ', ' .
						$this->sqlNonHurricanePremium() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'LiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'PersonalInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_detail_status_type_id = ' . $this->sqlInsurancePolicyDetailStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyDetailStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_detail_status_type_id = ' . $this->sqlInsurancePolicyDetailStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'CarrierPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount(). ',' ; } elseif( true == array_key_exists( 'PremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_premium_amount = ' . $this->sqlAnnualPremiumAmount(). ',' ; } elseif( true == array_key_exists( 'AnnualPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' annual_premium_amount = ' . $this->sqlAnnualPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_premium_amount_pif = ' . $this->sqlAnnualPremiumAmountPif(). ',' ; } elseif( true == array_key_exists( 'AnnualPremiumAmountPif', $this->getChangedColumns() ) ) { $strSql .= ' annual_premium_amount_pif = ' . $this->sqlAnnualPremiumAmountPif() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_premium_amount = ' . $this->sqlLiabilityPremiumAmount(). ',' ; } elseif( true == array_key_exists( 'LiabilityPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' liability_premium_amount = ' . $this->sqlLiabilityPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_premium_amount = ' . $this->sqlPersonalPremiumAmount(). ',' ; } elseif( true == array_key_exists( 'PersonalPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' personal_premium_amount = ' . $this->sqlPersonalPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_tax_amount = ' . $this->sqlPremiumTaxAmount(). ',' ; } elseif( true == array_key_exists( 'PremiumTaxAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_tax_amount = ' . $this->sqlPremiumTaxAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee(). ',' ; } elseif( true == array_key_exists( 'AdminFee', $this->getChangedColumns() ) ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_fee = ' . $this->sqlPropertyFee(). ',' ; } elseif( true == array_key_exists( 'PropertyFee', $this->getChangedColumns() ) ) { $strSql .= ' property_fee = ' . $this->sqlPropertyFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_return_admin_fee = ' . $this->sqlIsReturnAdminFee(). ',' ; } elseif( true == array_key_exists( 'IsReturnAdminFee', $this->getChangedColumns() ) ) { $strSql .= ' is_return_admin_fee = ' . $this->sqlIsReturnAdminFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_street_line1 = ' . $this->sqlMailingStreetLine1(). ',' ; } elseif( true == array_key_exists( 'MailingStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' mailing_street_line1 = ' . $this->sqlMailingStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_city = ' . $this->sqlMailingCity(). ',' ; } elseif( true == array_key_exists( 'MailingCity', $this->getChangedColumns() ) ) { $strSql .= ' mailing_city = ' . $this->sqlMailingCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_state_code = ' . $this->sqlMailingStateCode(). ',' ; } elseif( true == array_key_exists( 'MailingStateCode', $this->getChangedColumns() ) ) { $strSql .= ' mailing_state_code = ' . $this->sqlMailingStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_postal_code = ' . $this->sqlMailingPostalCode(). ',' ; } elseif( true == array_key_exists( 'MailingPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' mailing_postal_code = ' . $this->sqlMailingPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' medical_payment_limit_id = ' . $this->sqlMedicalPaymentLimitId(). ',' ; } elseif( true == array_key_exists( 'MedicalPaymentLimitId', $this->getChangedColumns() ) ) { $strSql .= ' medical_payment_limit_id = ' . $this->sqlMedicalPaymentLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hurricane_deductible_insurance_carrier_limit_id = ' . $this->sqlHurricaneDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'HurricaneDeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' hurricane_deductible_insurance_carrier_limit_id = ' . $this->sqlHurricaneDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' windstorm_construction_credit_value = ' . $this->sqlWindstormConstructionCreditValue(). ',' ; } elseif( true == array_key_exists( 'WindstormConstructionCreditValue', $this->getChangedColumns() ) ) { $strSql .= ' windstorm_construction_credit_value = ' . $this->sqlWindstormConstructionCreditValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_code_grading_value = ' . $this->sqlBuildingCodeGradingValue(). ',' ; } elseif( true == array_key_exists( 'BuildingCodeGradingValue', $this->getChangedColumns() ) ) { $strSql .= ' building_code_grading_value = ' . $this->sqlBuildingCodeGradingValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' automatic_sprinkler_credit_value = ' . $this->sqlAutomaticSprinklerCreditValue(). ',' ; } elseif( true == array_key_exists( 'AutomaticSprinklerCreditValue', $this->getChangedColumns() ) ) { $strSql .= ' automatic_sprinkler_credit_value = ' . $this->sqlAutomaticSprinklerCreditValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_policy_fee = ' . $this->sqlStatePolicyFee(). ',' ; } elseif( true == array_key_exists( 'StatePolicyFee', $this->getChangedColumns() ) ) { $strSql .= ' state_policy_fee = ' . $this->sqlStatePolicyFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_address_failed = ' . $this->sqlIsAddressFailed(). ',' ; } elseif( true == array_key_exists( 'IsAddressFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_address_failed = ' . $this->sqlIsAddressFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_hurricane_premium = ' . $this->sqlNonHurricanePremium(). ',' ; } elseif( true == array_key_exists( 'NonHurricanePremium', $this->getChangedColumns() ) ) { $strSql .= ' non_hurricane_premium = ' . $this->sqlNonHurricanePremium() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'deductible_insurance_carrier_limit_id' => $this->getDeductibleInsuranceCarrierLimitId(),
			'liability_insurance_carrier_limit_id' => $this->getLiabilityInsuranceCarrierLimitId(),
			'personal_insurance_carrier_limit_id' => $this->getPersonalInsuranceCarrierLimitId(),
			'insurance_policy_detail_status_type_id' => $this->getInsurancePolicyDetailStatusTypeId(),
			'carrier_policy_number' => $this->getCarrierPolicyNumber(),
			'premium_amount' => $this->getPremiumAmount(),
			'annual_premium_amount' => $this->getAnnualPremiumAmount(),
			'annual_premium_amount_pif' => $this->getAnnualPremiumAmountPif(),
			'liability_premium_amount' => $this->getLiabilityPremiumAmount(),
			'personal_premium_amount' => $this->getPersonalPremiumAmount(),
			'premium_tax_amount' => $this->getPremiumTaxAmount(),
			'admin_fee' => $this->getAdminFee(),
			'property_fee' => $this->getPropertyFee(),
			'is_return_admin_fee' => $this->getIsReturnAdminFee(),
			'unit_number' => $this->getUnitNumber(),
			'street_line1' => $this->getStreetLine1(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'country_code' => $this->getCountryCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'mailing_street_line1' => $this->getMailingStreetLine1(),
			'mailing_city' => $this->getMailingCity(),
			'mailing_state_code' => $this->getMailingStateCode(),
			'mailing_postal_code' => $this->getMailingPostalCode(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'medical_payment_limit_id' => $this->getMedicalPaymentLimitId(),
			'hurricane_deductible_insurance_carrier_limit_id' => $this->getHurricaneDeductibleInsuranceCarrierLimitId(),
			'windstorm_construction_credit_value' => $this->getWindstormConstructionCreditValue(),
			'building_code_grading_value' => $this->getBuildingCodeGradingValue(),
			'automatic_sprinkler_credit_value' => $this->getAutomaticSprinklerCreditValue(),
			'state_policy_fee' => $this->getStatePolicyFee(),
			'is_address_failed' => $this->getIsAddressFailed(),
			'non_hurricane_premium' => $this->getNonHurricanePremium()
		);
	}

}
?>