<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyVendorTrackers
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyVendorTrackers extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyVendorTracker[]
	 */
	public static function fetchInsurancePolicyVendorTrackers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyVendorTracker', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyVendorTracker
	 */
	public static function fetchInsurancePolicyVendorTracker( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyVendorTracker', $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_vendor_trackers', $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackerById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyVendorTracker( sprintf( 'SELECT * FROM insurance_policy_vendor_trackers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackersByInsurancePolicyVendorId( $intInsurancePolicyVendorId, $objDatabase ) {
		return self::fetchInsurancePolicyVendorTrackers( sprintf( 'SELECT * FROM insurance_policy_vendor_trackers WHERE insurance_policy_vendor_id = %d', ( int ) $intInsurancePolicyVendorId ), $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackersBySourceId( $intSourceId, $objDatabase ) {
		return self::fetchInsurancePolicyVendorTrackers( sprintf( 'SELECT * FROM insurance_policy_vendor_trackers WHERE source_id = %d', ( int ) $intSourceId ), $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackersByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsurancePolicyVendorTrackers( sprintf( 'SELECT * FROM insurance_policy_vendor_trackers WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchInsurancePolicyVendorTrackersByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyVendorTrackers( sprintf( 'SELECT * FROM insurance_policy_vendor_trackers WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

}
?>