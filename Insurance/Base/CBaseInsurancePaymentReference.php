<?php

class CBaseInsurancePaymentReference extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_payment_references';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_strPaymentReferenceNumber;
	protected $m_boolIsProcessed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsProcessed = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['payment_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strPaymentReferenceNumber', trim( stripcslashes( $arrValues['payment_reference_number'] ) ) ); elseif( isset( $arrValues['payment_reference_number'] ) ) $this->setPaymentReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_reference_number'] ) : $arrValues['payment_reference_number'] );
		if( isset( $arrValues['is_processed'] ) && $boolDirectSet ) $this->set( 'm_boolIsProcessed', trim( stripcslashes( $arrValues['is_processed'] ) ) ); elseif( isset( $arrValues['is_processed'] ) ) $this->setIsProcessed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_processed'] ) : $arrValues['is_processed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setPaymentReferenceNumber( $strPaymentReferenceNumber ) {
		$this->set( 'm_strPaymentReferenceNumber', CStrings::strTrimDef( $strPaymentReferenceNumber, 200, NULL, true ) );
	}

	public function getPaymentReferenceNumber() {
		return $this->m_strPaymentReferenceNumber;
	}

	public function sqlPaymentReferenceNumber() {
		return ( true == isset( $this->m_strPaymentReferenceNumber ) ) ? '\'' . addslashes( $this->m_strPaymentReferenceNumber ) . '\'' : 'NULL';
	}

	public function setIsProcessed( $boolIsProcessed ) {
		$this->set( 'm_boolIsProcessed', CStrings::strToBool( $boolIsProcessed ) );
	}

	public function getIsProcessed() {
		return $this->m_boolIsProcessed;
	}

	public function sqlIsProcessed() {
		return ( true == isset( $this->m_boolIsProcessed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProcessed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, payment_reference_number, is_processed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlPaymentReferenceNumber() . ', ' .
 						$this->sqlIsProcessed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_reference_number = ' . $this->sqlPaymentReferenceNumber() . ','; } elseif( true == array_key_exists( 'PaymentReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' payment_reference_number = ' . $this->sqlPaymentReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_processed = ' . $this->sqlIsProcessed() . ','; } elseif( true == array_key_exists( 'IsProcessed', $this->getChangedColumns() ) ) { $strSql .= ' is_processed = ' . $this->sqlIsProcessed() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'payment_reference_number' => $this->getPaymentReferenceNumber(),
			'is_processed' => $this->getIsProcessed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>