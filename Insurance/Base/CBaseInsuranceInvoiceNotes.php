<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInvoiceNotes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInvoiceNotes extends CEosPluralBase {

	/**
	 * @return CInsuranceInvoiceNote[]
	 */
	public static function fetchInsuranceInvoiceNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceInvoiceNote', $objDatabase );
	}

	/**
	 * @return CInsuranceInvoiceNote
	 */
	public static function fetchInsuranceInvoiceNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceInvoiceNote', $objDatabase );
	}

	public static function fetchInsuranceInvoiceNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_invoice_notes', $objDatabase );
	}

	public static function fetchInsuranceInvoiceNoteById( $intId, $objDatabase ) {
		return self::fetchInsuranceInvoiceNote( sprintf( 'SELECT * FROM insurance_invoice_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceInvoiceNotesByInsuranceInvoiceId( $intInsuranceInvoiceId, $objDatabase ) {
		return self::fetchInsuranceInvoiceNotes( sprintf( 'SELECT * FROM insurance_invoice_notes WHERE insurance_invoice_id = %d', ( int ) $intInsuranceInvoiceId ), $objDatabase );
	}

}
?>