<?php

class CBaseClaimLog extends CEosSingularBase {

	const TABLE_NAME = 'public.claim_logs';

	protected $m_intId;
	protected $m_intClaimId;
	protected $m_intLossTypeId;
	protected $m_intClaimStatusTypeId;
	protected $m_strCarrierPolicyNumber;
	protected $m_strClaimNumber;
	protected $m_strInsuredName;
	protected $m_strStreetAddress;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strClaimPartyClassification;
	protected $m_strCoverageCode;
	protected $m_fltTotalClaimAmount;
	protected $m_fltCovCAmount;
	protected $m_fltCovDAmount;
	protected $m_fltCovEAmount;
	protected $m_fltLossAmount;
	protected $m_fltReserves;
	protected $m_arrintClaimCategoryIds;
	protected $m_boolIsActive;
	protected $m_strLossDescription;
	protected $m_strLossDate;
	protected $m_strClaimReportedDate;
	protected $m_strClaimOpenDate;
	protected $m_strClaimCloseDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['claim_id'] ) && $boolDirectSet ) $this->set( 'm_intClaimId', trim( $arrValues['claim_id'] ) ); elseif( isset( $arrValues['claim_id'] ) ) $this->setClaimId( $arrValues['claim_id'] );
		if( isset( $arrValues['loss_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLossTypeId', trim( $arrValues['loss_type_id'] ) ); elseif( isset( $arrValues['loss_type_id'] ) ) $this->setLossTypeId( $arrValues['loss_type_id'] );
		if( isset( $arrValues['claim_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intClaimStatusTypeId', trim( $arrValues['claim_status_type_id'] ) ); elseif( isset( $arrValues['claim_status_type_id'] ) ) $this->setClaimStatusTypeId( $arrValues['claim_status_type_id'] );
		if( isset( $arrValues['carrier_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strCarrierPolicyNumber', trim( $arrValues['carrier_policy_number'] ) ); elseif( isset( $arrValues['carrier_policy_number'] ) ) $this->setCarrierPolicyNumber( $arrValues['carrier_policy_number'] );
		if( isset( $arrValues['claim_number'] ) && $boolDirectSet ) $this->set( 'm_strClaimNumber', trim( $arrValues['claim_number'] ) ); elseif( isset( $arrValues['claim_number'] ) ) $this->setClaimNumber( $arrValues['claim_number'] );
		if( isset( $arrValues['insured_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuredName', trim( $arrValues['insured_name'] ) ); elseif( isset( $arrValues['insured_name'] ) ) $this->setInsuredName( $arrValues['insured_name'] );
		if( isset( $arrValues['street_address'] ) && $boolDirectSet ) $this->set( 'm_strStreetAddress', trim( $arrValues['street_address'] ) ); elseif( isset( $arrValues['street_address'] ) ) $this->setStreetAddress( $arrValues['street_address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['claim_party_classification'] ) && $boolDirectSet ) $this->set( 'm_strClaimPartyClassification', trim( $arrValues['claim_party_classification'] ) ); elseif( isset( $arrValues['claim_party_classification'] ) ) $this->setClaimPartyClassification( $arrValues['claim_party_classification'] );
		if( isset( $arrValues['coverage_code'] ) && $boolDirectSet ) $this->set( 'm_strCoverageCode', trim( $arrValues['coverage_code'] ) ); elseif( isset( $arrValues['coverage_code'] ) ) $this->setCoverageCode( $arrValues['coverage_code'] );
		if( isset( $arrValues['total_claim_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalClaimAmount', trim( $arrValues['total_claim_amount'] ) ); elseif( isset( $arrValues['total_claim_amount'] ) ) $this->setTotalClaimAmount( $arrValues['total_claim_amount'] );
		if( isset( $arrValues['cov_c_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCovCAmount', trim( $arrValues['cov_c_amount'] ) ); elseif( isset( $arrValues['cov_c_amount'] ) ) $this->setCovCAmount( $arrValues['cov_c_amount'] );
		if( isset( $arrValues['cov_d_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCovDAmount', trim( $arrValues['cov_d_amount'] ) ); elseif( isset( $arrValues['cov_d_amount'] ) ) $this->setCovDAmount( $arrValues['cov_d_amount'] );
		if( isset( $arrValues['cov_e_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCovEAmount', trim( $arrValues['cov_e_amount'] ) ); elseif( isset( $arrValues['cov_e_amount'] ) ) $this->setCovEAmount( $arrValues['cov_e_amount'] );
		if( isset( $arrValues['loss_amount'] ) && $boolDirectSet ) $this->set( 'm_fltLossAmount', trim( $arrValues['loss_amount'] ) ); elseif( isset( $arrValues['loss_amount'] ) ) $this->setLossAmount( $arrValues['loss_amount'] );
		if( isset( $arrValues['reserves'] ) && $boolDirectSet ) $this->set( 'm_fltReserves', trim( $arrValues['reserves'] ) ); elseif( isset( $arrValues['reserves'] ) ) $this->setReserves( $arrValues['reserves'] );
		if( isset( $arrValues['claim_category_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintClaimCategoryIds', trim( $arrValues['claim_category_ids'] ) ); elseif( isset( $arrValues['claim_category_ids'] ) ) $this->setClaimCategoryIds( $arrValues['claim_category_ids'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['loss_description'] ) && $boolDirectSet ) $this->set( 'm_strLossDescription', trim( $arrValues['loss_description'] ) ); elseif( isset( $arrValues['loss_description'] ) ) $this->setLossDescription( $arrValues['loss_description'] );
		if( isset( $arrValues['loss_date'] ) && $boolDirectSet ) $this->set( 'm_strLossDate', trim( $arrValues['loss_date'] ) ); elseif( isset( $arrValues['loss_date'] ) ) $this->setLossDate( $arrValues['loss_date'] );
		if( isset( $arrValues['claim_reported_date'] ) && $boolDirectSet ) $this->set( 'm_strClaimReportedDate', trim( $arrValues['claim_reported_date'] ) ); elseif( isset( $arrValues['claim_reported_date'] ) ) $this->setClaimReportedDate( $arrValues['claim_reported_date'] );
		if( isset( $arrValues['claim_open_date'] ) && $boolDirectSet ) $this->set( 'm_strClaimOpenDate', trim( $arrValues['claim_open_date'] ) ); elseif( isset( $arrValues['claim_open_date'] ) ) $this->setClaimOpenDate( $arrValues['claim_open_date'] );
		if( isset( $arrValues['claim_close_date'] ) && $boolDirectSet ) $this->set( 'm_strClaimCloseDate', trim( $arrValues['claim_close_date'] ) ); elseif( isset( $arrValues['claim_close_date'] ) ) $this->setClaimCloseDate( $arrValues['claim_close_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClaimId( $intClaimId ) {
		$this->set( 'm_intClaimId', CStrings::strToIntDef( $intClaimId, NULL, false ) );
	}

	public function getClaimId() {
		return $this->m_intClaimId;
	}

	public function sqlClaimId() {
		return ( true == isset( $this->m_intClaimId ) ) ? ( string ) $this->m_intClaimId : 'NULL';
	}

	public function setLossTypeId( $intLossTypeId ) {
		$this->set( 'm_intLossTypeId', CStrings::strToIntDef( $intLossTypeId, NULL, false ) );
	}

	public function getLossTypeId() {
		return $this->m_intLossTypeId;
	}

	public function sqlLossTypeId() {
		return ( true == isset( $this->m_intLossTypeId ) ) ? ( string ) $this->m_intLossTypeId : 'NULL';
	}

	public function setClaimStatusTypeId( $intClaimStatusTypeId ) {
		$this->set( 'm_intClaimStatusTypeId', CStrings::strToIntDef( $intClaimStatusTypeId, NULL, false ) );
	}

	public function getClaimStatusTypeId() {
		return $this->m_intClaimStatusTypeId;
	}

	public function sqlClaimStatusTypeId() {
		return ( true == isset( $this->m_intClaimStatusTypeId ) ) ? ( string ) $this->m_intClaimStatusTypeId : 'NULL';
	}

	public function setCarrierPolicyNumber( $strCarrierPolicyNumber ) {
		$this->set( 'm_strCarrierPolicyNumber', CStrings::strTrimDef( $strCarrierPolicyNumber, 100, NULL, true ) );
	}

	public function getCarrierPolicyNumber() {
		return $this->m_strCarrierPolicyNumber;
	}

	public function sqlCarrierPolicyNumber() {
		return ( true == isset( $this->m_strCarrierPolicyNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCarrierPolicyNumber ) : '\'' . addslashes( $this->m_strCarrierPolicyNumber ) . '\'' ) : 'NULL';
	}

	public function setClaimNumber( $strClaimNumber ) {
		$this->set( 'm_strClaimNumber', CStrings::strTrimDef( $strClaimNumber, 100, NULL, true ) );
	}

	public function getClaimNumber() {
		return $this->m_strClaimNumber;
	}

	public function sqlClaimNumber() {
		return ( true == isset( $this->m_strClaimNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strClaimNumber ) : '\'' . addslashes( $this->m_strClaimNumber ) . '\'' ) : 'NULL';
	}

	public function setInsuredName( $strInsuredName ) {
		$this->set( 'm_strInsuredName', CStrings::strTrimDef( $strInsuredName, 250, NULL, true ) );
	}

	public function getInsuredName() {
		return $this->m_strInsuredName;
	}

	public function sqlInsuredName() {
		return ( true == isset( $this->m_strInsuredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsuredName ) : '\'' . addslashes( $this->m_strInsuredName ) . '\'' ) : 'NULL';
	}

	public function setStreetAddress( $strStreetAddress ) {
		$this->set( 'm_strStreetAddress', CStrings::strTrimDef( $strStreetAddress, -1, NULL, true ) );
	}

	public function getStreetAddress() {
		return $this->m_strStreetAddress;
	}

	public function sqlStreetAddress() {
		return ( true == isset( $this->m_strStreetAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetAddress ) : '\'' . addslashes( $this->m_strStreetAddress ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 100, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 10, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setClaimPartyClassification( $strClaimPartyClassification ) {
		$this->set( 'm_strClaimPartyClassification', CStrings::strTrimDef( $strClaimPartyClassification, 250, NULL, true ) );
	}

	public function getClaimPartyClassification() {
		return $this->m_strClaimPartyClassification;
	}

	public function sqlClaimPartyClassification() {
		return ( true == isset( $this->m_strClaimPartyClassification ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strClaimPartyClassification ) : '\'' . addslashes( $this->m_strClaimPartyClassification ) . '\'' ) : 'NULL';
	}

	public function setCoverageCode( $strCoverageCode ) {
		$this->set( 'm_strCoverageCode', CStrings::strTrimDef( $strCoverageCode, 100, NULL, true ) );
	}

	public function getCoverageCode() {
		return $this->m_strCoverageCode;
	}

	public function sqlCoverageCode() {
		return ( true == isset( $this->m_strCoverageCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCoverageCode ) : '\'' . addslashes( $this->m_strCoverageCode ) . '\'' ) : 'NULL';
	}

	public function setTotalClaimAmount( $fltTotalClaimAmount ) {
		$this->set( 'm_fltTotalClaimAmount', CStrings::strToFloatDef( $fltTotalClaimAmount, NULL, false, 2 ) );
	}

	public function getTotalClaimAmount() {
		return $this->m_fltTotalClaimAmount;
	}

	public function sqlTotalClaimAmount() {
		return ( true == isset( $this->m_fltTotalClaimAmount ) ) ? ( string ) $this->m_fltTotalClaimAmount : 'NULL';
	}

	public function setCovCAmount( $fltCovCAmount ) {
		$this->set( 'm_fltCovCAmount', CStrings::strToFloatDef( $fltCovCAmount, NULL, false, 2 ) );
	}

	public function getCovCAmount() {
		return $this->m_fltCovCAmount;
	}

	public function sqlCovCAmount() {
		return ( true == isset( $this->m_fltCovCAmount ) ) ? ( string ) $this->m_fltCovCAmount : 'NULL';
	}

	public function setCovDAmount( $fltCovDAmount ) {
		$this->set( 'm_fltCovDAmount', CStrings::strToFloatDef( $fltCovDAmount, NULL, false, 2 ) );
	}

	public function getCovDAmount() {
		return $this->m_fltCovDAmount;
	}

	public function sqlCovDAmount() {
		return ( true == isset( $this->m_fltCovDAmount ) ) ? ( string ) $this->m_fltCovDAmount : 'NULL';
	}

	public function setCovEAmount( $fltCovEAmount ) {
		$this->set( 'm_fltCovEAmount', CStrings::strToFloatDef( $fltCovEAmount, NULL, false, 2 ) );
	}

	public function getCovEAmount() {
		return $this->m_fltCovEAmount;
	}

	public function sqlCovEAmount() {
		return ( true == isset( $this->m_fltCovEAmount ) ) ? ( string ) $this->m_fltCovEAmount : 'NULL';
	}

	public function setLossAmount( $fltLossAmount ) {
		$this->set( 'm_fltLossAmount', CStrings::strToFloatDef( $fltLossAmount, NULL, false, 2 ) );
	}

	public function getLossAmount() {
		return $this->m_fltLossAmount;
	}

	public function sqlLossAmount() {
		return ( true == isset( $this->m_fltLossAmount ) ) ? ( string ) $this->m_fltLossAmount : 'NULL';
	}

	public function setReserves( $fltReserves ) {
		$this->set( 'm_fltReserves', CStrings::strToFloatDef( $fltReserves, NULL, false, 2 ) );
	}

	public function getReserves() {
		return $this->m_fltReserves;
	}

	public function sqlReserves() {
		return ( true == isset( $this->m_fltReserves ) ) ? ( string ) $this->m_fltReserves : 'NULL';
	}

	public function setClaimCategoryIds( $arrintClaimCategoryIds ) {
		$this->set( 'm_arrintClaimCategoryIds', CStrings::strToArrIntDef( $arrintClaimCategoryIds, NULL ) );
	}

	public function getClaimCategoryIds() {
		return $this->m_arrintClaimCategoryIds;
	}

	public function sqlClaimCategoryIds() {
		return ( true == isset( $this->m_arrintClaimCategoryIds ) && true == valArr( $this->m_arrintClaimCategoryIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintClaimCategoryIds, NULL ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLossDescription( $strLossDescription ) {
		$this->set( 'm_strLossDescription', CStrings::strTrimDef( $strLossDescription, -1, NULL, true ) );
	}

	public function getLossDescription() {
		return $this->m_strLossDescription;
	}

	public function sqlLossDescription() {
		return ( true == isset( $this->m_strLossDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLossDescription ) : '\'' . addslashes( $this->m_strLossDescription ) . '\'' ) : 'NULL';
	}

	public function setLossDate( $strLossDate ) {
		$this->set( 'm_strLossDate', CStrings::strTrimDef( $strLossDate, -1, NULL, true ) );
	}

	public function getLossDate() {
		return $this->m_strLossDate;
	}

	public function sqlLossDate() {
		return ( true == isset( $this->m_strLossDate ) ) ? '\'' . $this->m_strLossDate . '\'' : 'NULL';
	}

	public function setClaimReportedDate( $strClaimReportedDate ) {
		$this->set( 'm_strClaimReportedDate', CStrings::strTrimDef( $strClaimReportedDate, -1, NULL, true ) );
	}

	public function getClaimReportedDate() {
		return $this->m_strClaimReportedDate;
	}

	public function sqlClaimReportedDate() {
		return ( true == isset( $this->m_strClaimReportedDate ) ) ? '\'' . $this->m_strClaimReportedDate . '\'' : 'NULL';
	}

	public function setClaimOpenDate( $strClaimOpenDate ) {
		$this->set( 'm_strClaimOpenDate', CStrings::strTrimDef( $strClaimOpenDate, -1, NULL, true ) );
	}

	public function getClaimOpenDate() {
		return $this->m_strClaimOpenDate;
	}

	public function sqlClaimOpenDate() {
		return ( true == isset( $this->m_strClaimOpenDate ) ) ? '\'' . $this->m_strClaimOpenDate . '\'' : 'NULL';
	}

	public function setClaimCloseDate( $strClaimCloseDate ) {
		$this->set( 'm_strClaimCloseDate', CStrings::strTrimDef( $strClaimCloseDate, -1, NULL, true ) );
	}

	public function getClaimCloseDate() {
		return $this->m_strClaimCloseDate;
	}

	public function sqlClaimCloseDate() {
		return ( true == isset( $this->m_strClaimCloseDate ) ) ? '\'' . $this->m_strClaimCloseDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, claim_id, loss_type_id, claim_status_type_id, carrier_policy_number, claim_number, insured_name, street_address, city, state_code, postal_code, claim_party_classification, coverage_code, total_claim_amount, cov_c_amount, cov_d_amount, cov_e_amount, loss_amount, reserves, claim_category_ids, is_active, loss_description, loss_date, claim_reported_date, claim_open_date, claim_close_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlClaimId() . ', ' .
						$this->sqlLossTypeId() . ', ' .
						$this->sqlClaimStatusTypeId() . ', ' .
						$this->sqlCarrierPolicyNumber() . ', ' .
						$this->sqlClaimNumber() . ', ' .
						$this->sqlInsuredName() . ', ' .
						$this->sqlStreetAddress() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlClaimPartyClassification() . ', ' .
						$this->sqlCoverageCode() . ', ' .
						$this->sqlTotalClaimAmount() . ', ' .
						$this->sqlCovCAmount() . ', ' .
						$this->sqlCovDAmount() . ', ' .
						$this->sqlCovEAmount() . ', ' .
						$this->sqlLossAmount() . ', ' .
						$this->sqlReserves() . ', ' .
						$this->sqlClaimCategoryIds() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlLossDescription() . ', ' .
						$this->sqlLossDate() . ', ' .
						$this->sqlClaimReportedDate() . ', ' .
						$this->sqlClaimOpenDate() . ', ' .
						$this->sqlClaimCloseDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_id = ' . $this->sqlClaimId(). ',' ; } elseif( true == array_key_exists( 'ClaimId', $this->getChangedColumns() ) ) { $strSql .= ' claim_id = ' . $this->sqlClaimId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_type_id = ' . $this->sqlLossTypeId(). ',' ; } elseif( true == array_key_exists( 'LossTypeId', $this->getChangedColumns() ) ) { $strSql .= ' loss_type_id = ' . $this->sqlLossTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_status_type_id = ' . $this->sqlClaimStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ClaimStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' claim_status_type_id = ' . $this->sqlClaimStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'CarrierPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_number = ' . $this->sqlClaimNumber(). ',' ; } elseif( true == array_key_exists( 'ClaimNumber', $this->getChangedColumns() ) ) { $strSql .= ' claim_number = ' . $this->sqlClaimNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName(). ',' ; } elseif( true == array_key_exists( 'InsuredName', $this->getChangedColumns() ) ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_address = ' . $this->sqlStreetAddress(). ',' ; } elseif( true == array_key_exists( 'StreetAddress', $this->getChangedColumns() ) ) { $strSql .= ' street_address = ' . $this->sqlStreetAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_party_classification = ' . $this->sqlClaimPartyClassification(). ',' ; } elseif( true == array_key_exists( 'ClaimPartyClassification', $this->getChangedColumns() ) ) { $strSql .= ' claim_party_classification = ' . $this->sqlClaimPartyClassification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_code = ' . $this->sqlCoverageCode(). ',' ; } elseif( true == array_key_exists( 'CoverageCode', $this->getChangedColumns() ) ) { $strSql .= ' coverage_code = ' . $this->sqlCoverageCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_claim_amount = ' . $this->sqlTotalClaimAmount(). ',' ; } elseif( true == array_key_exists( 'TotalClaimAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_claim_amount = ' . $this->sqlTotalClaimAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cov_c_amount = ' . $this->sqlCovCAmount(). ',' ; } elseif( true == array_key_exists( 'CovCAmount', $this->getChangedColumns() ) ) { $strSql .= ' cov_c_amount = ' . $this->sqlCovCAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cov_d_amount = ' . $this->sqlCovDAmount(). ',' ; } elseif( true == array_key_exists( 'CovDAmount', $this->getChangedColumns() ) ) { $strSql .= ' cov_d_amount = ' . $this->sqlCovDAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cov_e_amount = ' . $this->sqlCovEAmount(). ',' ; } elseif( true == array_key_exists( 'CovEAmount', $this->getChangedColumns() ) ) { $strSql .= ' cov_e_amount = ' . $this->sqlCovEAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_amount = ' . $this->sqlLossAmount(). ',' ; } elseif( true == array_key_exists( 'LossAmount', $this->getChangedColumns() ) ) { $strSql .= ' loss_amount = ' . $this->sqlLossAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reserves = ' . $this->sqlReserves(). ',' ; } elseif( true == array_key_exists( 'Reserves', $this->getChangedColumns() ) ) { $strSql .= ' reserves = ' . $this->sqlReserves() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_category_ids = ' . $this->sqlClaimCategoryIds(). ',' ; } elseif( true == array_key_exists( 'ClaimCategoryIds', $this->getChangedColumns() ) ) { $strSql .= ' claim_category_ids = ' . $this->sqlClaimCategoryIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_description = ' . $this->sqlLossDescription(). ',' ; } elseif( true == array_key_exists( 'LossDescription', $this->getChangedColumns() ) ) { $strSql .= ' loss_description = ' . $this->sqlLossDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_date = ' . $this->sqlLossDate(). ',' ; } elseif( true == array_key_exists( 'LossDate', $this->getChangedColumns() ) ) { $strSql .= ' loss_date = ' . $this->sqlLossDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_reported_date = ' . $this->sqlClaimReportedDate(). ',' ; } elseif( true == array_key_exists( 'ClaimReportedDate', $this->getChangedColumns() ) ) { $strSql .= ' claim_reported_date = ' . $this->sqlClaimReportedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_open_date = ' . $this->sqlClaimOpenDate(). ',' ; } elseif( true == array_key_exists( 'ClaimOpenDate', $this->getChangedColumns() ) ) { $strSql .= ' claim_open_date = ' . $this->sqlClaimOpenDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' claim_close_date = ' . $this->sqlClaimCloseDate(). ',' ; } elseif( true == array_key_exists( 'ClaimCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' claim_close_date = ' . $this->sqlClaimCloseDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'claim_id' => $this->getClaimId(),
			'loss_type_id' => $this->getLossTypeId(),
			'claim_status_type_id' => $this->getClaimStatusTypeId(),
			'carrier_policy_number' => $this->getCarrierPolicyNumber(),
			'claim_number' => $this->getClaimNumber(),
			'insured_name' => $this->getInsuredName(),
			'street_address' => $this->getStreetAddress(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'claim_party_classification' => $this->getClaimPartyClassification(),
			'coverage_code' => $this->getCoverageCode(),
			'total_claim_amount' => $this->getTotalClaimAmount(),
			'cov_c_amount' => $this->getCovCAmount(),
			'cov_d_amount' => $this->getCovDAmount(),
			'cov_e_amount' => $this->getCovEAmount(),
			'loss_amount' => $this->getLossAmount(),
			'reserves' => $this->getReserves(),
			'claim_category_ids' => $this->getClaimCategoryIds(),
			'is_active' => $this->getIsActive(),
			'loss_description' => $this->getLossDescription(),
			'loss_date' => $this->getLossDate(),
			'claim_reported_date' => $this->getClaimReportedDate(),
			'claim_open_date' => $this->getClaimOpenDate(),
			'claim_close_date' => $this->getClaimCloseDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>