<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyCoverageDetailStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyCoverageDetailStatusTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyCoverageDetailStatusType[]
	 */
	public static function fetchInsurancePolicyCoverageDetailStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyCoverageDetailStatusType', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyCoverageDetailStatusType
	 */
	public static function fetchInsurancePolicyCoverageDetailStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyCoverageDetailStatusType', $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_coverage_detail_status_types', $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailStatusTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetailStatusType( sprintf( 'SELECT * FROM insurance_policy_coverage_detail_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>