<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyClients
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyClients extends CEosPluralBase {

	/**
	 * @return CMasterPolicyClient[]
	 */
	public static function fetchMasterPolicyClients( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyClient::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyClient
	 */
	public static function fetchMasterPolicyClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyClient::class, $objDatabase );
	}

	public static function fetchMasterPolicyClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_clients', $objDatabase );
	}

	public static function fetchMasterPolicyClientById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyClient( sprintf( 'SELECT * FROM master_policy_clients WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMasterPolicyClientsByCid( $intCid, $objDatabase ) {
		return self::fetchMasterPolicyClients( sprintf( 'SELECT * FROM master_policy_clients WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMasterPolicyClientsByProductId( $intProductId, $objDatabase ) {
		return self::fetchMasterPolicyClients( sprintf( 'SELECT * FROM master_policy_clients WHERE product_id = %d', $intProductId ), $objDatabase );
	}

}
?>