<?php

class CBaseInsurancePolicyVendor extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_vendors';

	protected $m_intId;
	protected $m_strName;
	protected $m_strUrl;
	protected $m_strUrlData;
	protected $m_strImage;
	protected $m_strAgentCode;
	protected $m_intIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['url_data'] ) && $boolDirectSet ) $this->set( 'm_strUrlData', trim( stripcslashes( $arrValues['url_data'] ) ) ); elseif( isset( $arrValues['url_data'] ) ) $this->setUrlData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url_data'] ) : $arrValues['url_data'] );
		if( isset( $arrValues['image'] ) && $boolDirectSet ) $this->set( 'm_strImage', trim( stripcslashes( $arrValues['image'] ) ) ); elseif( isset( $arrValues['image'] ) ) $this->setImage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image'] ) : $arrValues['image'] );
		if( isset( $arrValues['agent_code'] ) && $boolDirectSet ) $this->set( 'm_strAgentCode', trim( stripcslashes( $arrValues['agent_code'] ) ) ); elseif( isset( $arrValues['agent_code'] ) ) $this->setAgentCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agent_code'] ) : $arrValues['agent_code'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 255, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setUrlData( $strUrlData ) {
		$this->set( 'm_strUrlData', CStrings::strTrimDef( $strUrlData, 255, NULL, true ) );
	}

	public function getUrlData() {
		return $this->m_strUrlData;
	}

	public function sqlUrlData() {
		return ( true == isset( $this->m_strUrlData ) ) ? '\'' . addslashes( $this->m_strUrlData ) . '\'' : 'NULL';
	}

	public function setImage( $strImage ) {
		$this->set( 'm_strImage', CStrings::strTrimDef( $strImage, 255, NULL, true ) );
	}

	public function getImage() {
		return $this->m_strImage;
	}

	public function sqlImage() {
		return ( true == isset( $this->m_strImage ) ) ? '\'' . addslashes( $this->m_strImage ) . '\'' : 'NULL';
	}

	public function setAgentCode( $strAgentCode ) {
		$this->set( 'm_strAgentCode', CStrings::strTrimDef( $strAgentCode, 255, NULL, true ) );
	}

	public function getAgentCode() {
		return $this->m_strAgentCode;
	}

	public function sqlAgentCode() {
		return ( true == isset( $this->m_strAgentCode ) ) ? '\'' . addslashes( $this->m_strAgentCode ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, url, url_data, image, agent_code, is_disabled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlUrl() . ', ' .
 						$this->sqlUrlData() . ', ' .
 						$this->sqlImage() . ', ' .
 						$this->sqlAgentCode() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url_data = ' . $this->sqlUrlData() . ','; } elseif( true == array_key_exists( 'UrlData', $this->getChangedColumns() ) ) { $strSql .= ' url_data = ' . $this->sqlUrlData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image = ' . $this->sqlImage() . ','; } elseif( true == array_key_exists( 'Image', $this->getChangedColumns() ) ) { $strSql .= ' image = ' . $this->sqlImage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_code = ' . $this->sqlAgentCode() . ','; } elseif( true == array_key_exists( 'AgentCode', $this->getChangedColumns() ) ) { $strSql .= ' agent_code = ' . $this->sqlAgentCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'url' => $this->getUrl(),
			'url_data' => $this->getUrlData(),
			'image' => $this->getImage(),
			'agent_code' => $this->getAgentCode(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>