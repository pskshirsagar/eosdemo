<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyCoverages
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyCoverages extends CEosPluralBase {

	/**
	 * @return CMasterPolicyCoverage[]
	 */
	public static function fetchMasterPolicyCoverages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyCoverage::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyCoverage
	 */
	public static function fetchMasterPolicyCoverage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyCoverage::class, $objDatabase );
	}

	public static function fetchMasterPolicyCoverageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_coverages', $objDatabase );
	}

	public static function fetchMasterPolicyCoverageById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyCoverage( sprintf( 'SELECT * FROM master_policy_coverages WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByMasterPolicyId( $intMasterPolicyId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE master_policy_id = %d', $intMasterPolicyId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByInsuranceExportBatchId( $intInsuranceExportBatchId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE insurance_export_batch_id = %d', $intInsuranceExportBatchId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByMasterPolicyBillingBatchId( $intMasterPolicyBillingBatchId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE master_policy_billing_batch_id = %d', $intMasterPolicyBillingBatchId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByInsuranceExportTypeId( $intInsuranceExportTypeId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE insurance_export_type_id = %d', $intInsuranceExportTypeId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByMasterPolicyCoverageId( $intMasterPolicyCoverageId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE master_policy_coverage_id = %d', $intMasterPolicyCoverageId ), $objDatabase );
	}

	public static function fetchMasterPolicyCoveragesByMasterPolicyRecordTypeId( $intMasterPolicyRecordTypeId, $objDatabase ) {
		return self::fetchMasterPolicyCoverages( sprintf( 'SELECT * FROM master_policy_coverages WHERE master_policy_record_type_id = %d', $intMasterPolicyRecordTypeId ), $objDatabase );
	}

}
?>