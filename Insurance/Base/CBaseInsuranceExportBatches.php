<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceExportBatches
 * Do not add any new functions to this class.
 */

class CBaseInsuranceExportBatches extends CEosPluralBase {

	/**
	 * @return CInsuranceExportBatch[]
	 */
	public static function fetchInsuranceExportBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceExportBatch', $objDatabase );
	}

	/**
	 * @return CInsuranceExportBatch
	 */
	public static function fetchInsuranceExportBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceExportBatch', $objDatabase );
	}

	public static function fetchInsuranceExportBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_export_batches', $objDatabase );
	}

	public static function fetchInsuranceExportBatchById( $intId, $objDatabase ) {
		return self::fetchInsuranceExportBatch( sprintf( 'SELECT * FROM insurance_export_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceExportBatchesByInsuranceBatchTypeId( $intInsuranceBatchTypeId, $objDatabase ) {
		return self::fetchInsuranceExportBatches( sprintf( 'SELECT * FROM insurance_export_batches WHERE insurance_batch_type_id = %d', ( int ) $intInsuranceBatchTypeId ), $objDatabase );
	}

	public static function fetchInsuranceExportBatchesByBatchStatusTypeId( $intBatchStatusTypeId, $objDatabase ) {
		return self::fetchInsuranceExportBatches( sprintf( 'SELECT * FROM insurance_export_batches WHERE batch_status_type_id = %d', ( int ) $intBatchStatusTypeId ), $objDatabase );
	}

}
?>