<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierPreferences
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierPreferences extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierPreference[]
	 */
	public static function fetchInsuranceCarrierPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceCarrierPreference', $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierPreference
	 */
	public static function fetchInsuranceCarrierPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceCarrierPreference', $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_preferences', $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferenceById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierPreference( sprintf( 'SELECT * FROM insurance_carrier_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferencesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierPreferences( sprintf( 'SELECT * FROM insurance_carrier_preferences WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPreferencesByInsuranceCarrierPreferenceKeyId( $intInsuranceCarrierPreferenceKeyId, $objDatabase ) {
		return self::fetchInsuranceCarrierPreferences( sprintf( 'SELECT * FROM insurance_carrier_preferences WHERE insurance_carrier_preference_key_id = %d', ( int ) $intInsuranceCarrierPreferenceKeyId ), $objDatabase );
	}

}
?>