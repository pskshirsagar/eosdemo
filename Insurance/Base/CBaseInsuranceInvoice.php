<?php

class CBaseInsuranceInvoice extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_invoices';

	protected $m_intId;
	protected $m_fltTotalPremiumAmount;
	protected $m_fltTotalInvoiceAmount;
	protected $m_fltInvoiceDueAmount;
	protected $m_fltTotalEarnedCommissionAmount;
	protected $m_fltTotalCommissionAmount;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceDueDate;
	protected $m_intInsuranceInvoiceStatusTypeId;
	protected $m_intInsuranceCarrierId;
	protected $m_strCarrierInvoiceNumber;
	protected $m_strFileName;
	protected $m_intReconciledBy;
	protected $m_strReconciledOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCommissionFileName;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['total_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalPremiumAmount', trim( $arrValues['total_premium_amount'] ) ); elseif( isset( $arrValues['total_premium_amount'] ) ) $this->setTotalPremiumAmount( $arrValues['total_premium_amount'] );
		if( isset( $arrValues['total_invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalInvoiceAmount', trim( $arrValues['total_invoice_amount'] ) ); elseif( isset( $arrValues['total_invoice_amount'] ) ) $this->setTotalInvoiceAmount( $arrValues['total_invoice_amount'] );
		if( isset( $arrValues['invoice_due_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceDueAmount', trim( $arrValues['invoice_due_amount'] ) ); elseif( isset( $arrValues['invoice_due_amount'] ) ) $this->setInvoiceDueAmount( $arrValues['invoice_due_amount'] );
		if( isset( $arrValues['total_earned_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalEarnedCommissionAmount', trim( $arrValues['total_earned_commission_amount'] ) ); elseif( isset( $arrValues['total_earned_commission_amount'] ) ) $this->setTotalEarnedCommissionAmount( $arrValues['total_earned_commission_amount'] );
		if( isset( $arrValues['total_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCommissionAmount', trim( $arrValues['total_commission_amount'] ) ); elseif( isset( $arrValues['total_commission_amount'] ) ) $this->setTotalCommissionAmount( $arrValues['total_commission_amount'] );
		if( isset( $arrValues['invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDate', trim( $arrValues['invoice_date'] ) ); elseif( isset( $arrValues['invoice_date'] ) ) $this->setInvoiceDate( $arrValues['invoice_date'] );
		if( isset( $arrValues['invoice_due_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDueDate', trim( $arrValues['invoice_due_date'] ) ); elseif( isset( $arrValues['invoice_due_date'] ) ) $this->setInvoiceDueDate( $arrValues['invoice_due_date'] );
		if( isset( $arrValues['insurance_invoice_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceInvoiceStatusTypeId', trim( $arrValues['insurance_invoice_status_type_id'] ) ); elseif( isset( $arrValues['insurance_invoice_status_type_id'] ) ) $this->setInsuranceInvoiceStatusTypeId( $arrValues['insurance_invoice_status_type_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['carrier_invoice_number'] ) && $boolDirectSet ) $this->set( 'm_strCarrierInvoiceNumber', trim( stripcslashes( $arrValues['carrier_invoice_number'] ) ) ); elseif( isset( $arrValues['carrier_invoice_number'] ) ) $this->setCarrierInvoiceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['carrier_invoice_number'] ) : $arrValues['carrier_invoice_number'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['reconciled_by'] ) && $boolDirectSet ) $this->set( 'm_intReconciledBy', trim( $arrValues['reconciled_by'] ) ); elseif( isset( $arrValues['reconciled_by'] ) ) $this->setReconciledBy( $arrValues['reconciled_by'] );
		if( isset( $arrValues['reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strReconciledOn', trim( $arrValues['reconciled_on'] ) ); elseif( isset( $arrValues['reconciled_on'] ) ) $this->setReconciledOn( $arrValues['reconciled_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['commission_file_name'] ) && $boolDirectSet ) $this->set( 'm_strCommissionFileName', trim( stripcslashes( $arrValues['commission_file_name'] ) ) ); elseif( isset( $arrValues['commission_file_name'] ) ) $this->setCommissionFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['commission_file_name'] ) : $arrValues['commission_file_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTotalPremiumAmount( $fltTotalPremiumAmount ) {
		$this->set( 'm_fltTotalPremiumAmount', CStrings::strToFloatDef( $fltTotalPremiumAmount, NULL, false, 2 ) );
	}

	public function getTotalPremiumAmount() {
		return $this->m_fltTotalPremiumAmount;
	}

	public function sqlTotalPremiumAmount() {
		return ( true == isset( $this->m_fltTotalPremiumAmount ) ) ? ( string ) $this->m_fltTotalPremiumAmount : 'NULL';
	}

	public function setTotalInvoiceAmount( $fltTotalInvoiceAmount ) {
		$this->set( 'm_fltTotalInvoiceAmount', CStrings::strToFloatDef( $fltTotalInvoiceAmount, NULL, false, 2 ) );
	}

	public function getTotalInvoiceAmount() {
		return $this->m_fltTotalInvoiceAmount;
	}

	public function sqlTotalInvoiceAmount() {
		return ( true == isset( $this->m_fltTotalInvoiceAmount ) ) ? ( string ) $this->m_fltTotalInvoiceAmount : 'NULL';
	}

	public function setInvoiceDueAmount( $fltInvoiceDueAmount ) {
		$this->set( 'm_fltInvoiceDueAmount', CStrings::strToFloatDef( $fltInvoiceDueAmount, NULL, false, 2 ) );
	}

	public function getInvoiceDueAmount() {
		return $this->m_fltInvoiceDueAmount;
	}

	public function sqlInvoiceDueAmount() {
		return ( true == isset( $this->m_fltInvoiceDueAmount ) ) ? ( string ) $this->m_fltInvoiceDueAmount : 'NULL';
	}

	public function setTotalEarnedCommissionAmount( $fltTotalEarnedCommissionAmount ) {
		$this->set( 'm_fltTotalEarnedCommissionAmount', CStrings::strToFloatDef( $fltTotalEarnedCommissionAmount, NULL, false, 2 ) );
	}

	public function getTotalEarnedCommissionAmount() {
		return $this->m_fltTotalEarnedCommissionAmount;
	}

	public function sqlTotalEarnedCommissionAmount() {
		return ( true == isset( $this->m_fltTotalEarnedCommissionAmount ) ) ? ( string ) $this->m_fltTotalEarnedCommissionAmount : 'NULL';
	}

	public function setTotalCommissionAmount( $fltTotalCommissionAmount ) {
		$this->set( 'm_fltTotalCommissionAmount', CStrings::strToFloatDef( $fltTotalCommissionAmount, NULL, false, 2 ) );
	}

	public function getTotalCommissionAmount() {
		return $this->m_fltTotalCommissionAmount;
	}

	public function sqlTotalCommissionAmount() {
		return ( true == isset( $this->m_fltTotalCommissionAmount ) ) ? ( string ) $this->m_fltTotalCommissionAmount : 'NULL';
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function sqlInvoiceDate() {
		return ( true == isset( $this->m_strInvoiceDate ) ) ? '\'' . $this->m_strInvoiceDate . '\'' : 'NULL';
	}

	public function setInvoiceDueDate( $strInvoiceDueDate ) {
		$this->set( 'm_strInvoiceDueDate', CStrings::strTrimDef( $strInvoiceDueDate, -1, NULL, true ) );
	}

	public function getInvoiceDueDate() {
		return $this->m_strInvoiceDueDate;
	}

	public function sqlInvoiceDueDate() {
		return ( true == isset( $this->m_strInvoiceDueDate ) ) ? '\'' . $this->m_strInvoiceDueDate . '\'' : 'NULL';
	}

	public function setInsuranceInvoiceStatusTypeId( $intInsuranceInvoiceStatusTypeId ) {
		$this->set( 'm_intInsuranceInvoiceStatusTypeId', CStrings::strToIntDef( $intInsuranceInvoiceStatusTypeId, NULL, false ) );
	}

	public function getInsuranceInvoiceStatusTypeId() {
		return $this->m_intInsuranceInvoiceStatusTypeId;
	}

	public function sqlInsuranceInvoiceStatusTypeId() {
		return ( true == isset( $this->m_intInsuranceInvoiceStatusTypeId ) ) ? ( string ) $this->m_intInsuranceInvoiceStatusTypeId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setCarrierInvoiceNumber( $strCarrierInvoiceNumber ) {
		$this->set( 'm_strCarrierInvoiceNumber', CStrings::strTrimDef( $strCarrierInvoiceNumber, 64, NULL, true ) );
	}

	public function getCarrierInvoiceNumber() {
		return $this->m_strCarrierInvoiceNumber;
	}

	public function sqlCarrierInvoiceNumber() {
		return ( true == isset( $this->m_strCarrierInvoiceNumber ) ) ? '\'' . addslashes( $this->m_strCarrierInvoiceNumber ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 255, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setReconciledBy( $intReconciledBy ) {
		$this->set( 'm_intReconciledBy', CStrings::strToIntDef( $intReconciledBy, NULL, false ) );
	}

	public function getReconciledBy() {
		return $this->m_intReconciledBy;
	}

	public function sqlReconciledBy() {
		return ( true == isset( $this->m_intReconciledBy ) ) ? ( string ) $this->m_intReconciledBy : 'NULL';
	}

	public function setReconciledOn( $strReconciledOn ) {
		$this->set( 'm_strReconciledOn', CStrings::strTrimDef( $strReconciledOn, -1, NULL, true ) );
	}

	public function getReconciledOn() {
		return $this->m_strReconciledOn;
	}

	public function sqlReconciledOn() {
		return ( true == isset( $this->m_strReconciledOn ) ) ? '\'' . $this->m_strReconciledOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCommissionFileName( $strCommissionFileName ) {
		$this->set( 'm_strCommissionFileName', CStrings::strTrimDef( $strCommissionFileName, 255, NULL, true ) );
	}

	public function getCommissionFileName() {
		return $this->m_strCommissionFileName;
	}

	public function sqlCommissionFileName() {
		return ( true == isset( $this->m_strCommissionFileName ) ) ? '\'' . addslashes( $this->m_strCommissionFileName ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, total_premium_amount, total_invoice_amount, invoice_due_amount, total_earned_commission_amount, total_commission_amount, invoice_date, invoice_due_date, insurance_invoice_status_type_id, insurance_carrier_id, carrier_invoice_number, file_name, reconciled_by, reconciled_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, commission_file_name )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTotalPremiumAmount() . ', ' .
 						$this->sqlTotalInvoiceAmount() . ', ' .
 						$this->sqlInvoiceDueAmount() . ', ' .
 						$this->sqlTotalEarnedCommissionAmount() . ', ' .
 						$this->sqlTotalCommissionAmount() . ', ' .
 						$this->sqlInvoiceDate() . ', ' .
 						$this->sqlInvoiceDueDate() . ', ' .
 						$this->sqlInsuranceInvoiceStatusTypeId() . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlCarrierInvoiceNumber() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlReconciledBy() . ', ' .
 						$this->sqlReconciledOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlCommissionFileName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; } elseif( true == array_key_exists( 'TotalPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount() . ','; } elseif( true == array_key_exists( 'TotalInvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_amount = ' . $this->sqlInvoiceDueAmount() . ','; } elseif( true == array_key_exists( 'InvoiceDueAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_amount = ' . $this->sqlInvoiceDueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_earned_commission_amount = ' . $this->sqlTotalEarnedCommissionAmount() . ','; } elseif( true == array_key_exists( 'TotalEarnedCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_earned_commission_amount = ' . $this->sqlTotalEarnedCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_commission_amount = ' . $this->sqlTotalCommissionAmount() . ','; } elseif( true == array_key_exists( 'TotalCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_commission_amount = ' . $this->sqlTotalCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; } elseif( true == array_key_exists( 'InvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; } elseif( true == array_key_exists( 'InvoiceDueDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_invoice_status_type_id = ' . $this->sqlInsuranceInvoiceStatusTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceInvoiceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_invoice_status_type_id = ' . $this->sqlInsuranceInvoiceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_invoice_number = ' . $this->sqlCarrierInvoiceNumber() . ','; } elseif( true == array_key_exists( 'CarrierInvoiceNumber', $this->getChangedColumns() ) ) { $strSql .= ' carrier_invoice_number = ' . $this->sqlCarrierInvoiceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy() . ','; } elseif( true == array_key_exists( 'ReconciledBy', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_by = ' . $this->sqlReconciledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; } elseif( true == array_key_exists( 'ReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_file_name = ' . $this->sqlCommissionFileName() . ','; } elseif( true == array_key_exists( 'CommissionFileName', $this->getChangedColumns() ) ) { $strSql .= ' commission_file_name = ' . $this->sqlCommissionFileName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'total_premium_amount' => $this->getTotalPremiumAmount(),
			'total_invoice_amount' => $this->getTotalInvoiceAmount(),
			'invoice_due_amount' => $this->getInvoiceDueAmount(),
			'total_earned_commission_amount' => $this->getTotalEarnedCommissionAmount(),
			'total_commission_amount' => $this->getTotalCommissionAmount(),
			'invoice_date' => $this->getInvoiceDate(),
			'invoice_due_date' => $this->getInvoiceDueDate(),
			'insurance_invoice_status_type_id' => $this->getInsuranceInvoiceStatusTypeId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'carrier_invoice_number' => $this->getCarrierInvoiceNumber(),
			'file_name' => $this->getFileName(),
			'reconciled_by' => $this->getReconciledBy(),
			'reconciled_on' => $this->getReconciledOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'commission_file_name' => $this->getCommissionFileName()
		);
	}

}
?>