<?php

class CBaseInsuranceCarrierEndorsement extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_carrier_endorsements';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intInsuranceAmountTypeId;
	protected $m_intInsuranceEndorsementTypeId;
	protected $m_intParentInsuranceCarrierEndorsementId;
	protected $m_strStateCode;
	protected $m_fltBaseRate;
	protected $m_fltBaseMultiplier;
	protected $m_intIsDefault;
	protected $m_intIsBasicOption;
	protected $m_intIsMandatory;
	protected $m_intIsAvailable;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDefault = '0';
		$this->m_intIsBasicOption = '0';
		$this->m_intIsMandatory = '0';
		$this->m_intIsAvailable = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['insurance_amount_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceAmountTypeId', trim( $arrValues['insurance_amount_type_id'] ) ); elseif( isset( $arrValues['insurance_amount_type_id'] ) ) $this->setInsuranceAmountTypeId( $arrValues['insurance_amount_type_id'] );
		if( isset( $arrValues['insurance_endorsement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceEndorsementTypeId', trim( $arrValues['insurance_endorsement_type_id'] ) ); elseif( isset( $arrValues['insurance_endorsement_type_id'] ) ) $this->setInsuranceEndorsementTypeId( $arrValues['insurance_endorsement_type_id'] );
		if( isset( $arrValues['parent_insurance_carrier_endorsement_id'] ) && $boolDirectSet ) $this->set( 'm_intParentInsuranceCarrierEndorsementId', trim( $arrValues['parent_insurance_carrier_endorsement_id'] ) ); elseif( isset( $arrValues['parent_insurance_carrier_endorsement_id'] ) ) $this->setParentInsuranceCarrierEndorsementId( $arrValues['parent_insurance_carrier_endorsement_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['base_rate'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRate', trim( $arrValues['base_rate'] ) ); elseif( isset( $arrValues['base_rate'] ) ) $this->setBaseRate( $arrValues['base_rate'] );
		if( isset( $arrValues['base_multiplier'] ) && $boolDirectSet ) $this->set( 'm_fltBaseMultiplier', trim( $arrValues['base_multiplier'] ) ); elseif( isset( $arrValues['base_multiplier'] ) ) $this->setBaseMultiplier( $arrValues['base_multiplier'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['is_basic_option'] ) && $boolDirectSet ) $this->set( 'm_intIsBasicOption', trim( $arrValues['is_basic_option'] ) ); elseif( isset( $arrValues['is_basic_option'] ) ) $this->setIsBasicOption( $arrValues['is_basic_option'] );
		if( isset( $arrValues['is_mandatory'] ) && $boolDirectSet ) $this->set( 'm_intIsMandatory', trim( $arrValues['is_mandatory'] ) ); elseif( isset( $arrValues['is_mandatory'] ) ) $this->setIsMandatory( $arrValues['is_mandatory'] );
		if( isset( $arrValues['is_available'] ) && $boolDirectSet ) $this->set( 'm_intIsAvailable', trim( $arrValues['is_available'] ) ); elseif( isset( $arrValues['is_available'] ) ) $this->setIsAvailable( $arrValues['is_available'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setInsuranceAmountTypeId( $intInsuranceAmountTypeId ) {
		$this->set( 'm_intInsuranceAmountTypeId', CStrings::strToIntDef( $intInsuranceAmountTypeId, NULL, false ) );
	}

	public function getInsuranceAmountTypeId() {
		return $this->m_intInsuranceAmountTypeId;
	}

	public function sqlInsuranceAmountTypeId() {
		return ( true == isset( $this->m_intInsuranceAmountTypeId ) ) ? ( string ) $this->m_intInsuranceAmountTypeId : 'NULL';
	}

	public function setInsuranceEndorsementTypeId( $intInsuranceEndorsementTypeId ) {
		$this->set( 'm_intInsuranceEndorsementTypeId', CStrings::strToIntDef( $intInsuranceEndorsementTypeId, NULL, false ) );
	}

	public function getInsuranceEndorsementTypeId() {
		return $this->m_intInsuranceEndorsementTypeId;
	}

	public function sqlInsuranceEndorsementTypeId() {
		return ( true == isset( $this->m_intInsuranceEndorsementTypeId ) ) ? ( string ) $this->m_intInsuranceEndorsementTypeId : 'NULL';
	}

	public function setParentInsuranceCarrierEndorsementId( $intParentInsuranceCarrierEndorsementId ) {
		$this->set( 'm_intParentInsuranceCarrierEndorsementId', CStrings::strToIntDef( $intParentInsuranceCarrierEndorsementId, NULL, false ) );
	}

	public function getParentInsuranceCarrierEndorsementId() {
		return $this->m_intParentInsuranceCarrierEndorsementId;
	}

	public function sqlParentInsuranceCarrierEndorsementId() {
		return ( true == isset( $this->m_intParentInsuranceCarrierEndorsementId ) ) ? ( string ) $this->m_intParentInsuranceCarrierEndorsementId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setBaseRate( $fltBaseRate ) {
		$this->set( 'm_fltBaseRate', CStrings::strToFloatDef( $fltBaseRate, NULL, false, 2 ) );
	}

	public function getBaseRate() {
		return $this->m_fltBaseRate;
	}

	public function sqlBaseRate() {
		return ( true == isset( $this->m_fltBaseRate ) ) ? ( string ) $this->m_fltBaseRate : 'NULL';
	}

	public function setBaseMultiplier( $fltBaseMultiplier ) {
		$this->set( 'm_fltBaseMultiplier', CStrings::strToFloatDef( $fltBaseMultiplier, NULL, false, 4 ) );
	}

	public function getBaseMultiplier() {
		return $this->m_fltBaseMultiplier;
	}

	public function sqlBaseMultiplier() {
		return ( true == isset( $this->m_fltBaseMultiplier ) ) ? ( string ) $this->m_fltBaseMultiplier : 'NULL';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setIsBasicOption( $intIsBasicOption ) {
		$this->set( 'm_intIsBasicOption', CStrings::strToIntDef( $intIsBasicOption, NULL, false ) );
	}

	public function getIsBasicOption() {
		return $this->m_intIsBasicOption;
	}

	public function sqlIsBasicOption() {
		return ( true == isset( $this->m_intIsBasicOption ) ) ? ( string ) $this->m_intIsBasicOption : '0';
	}

	public function setIsMandatory( $intIsMandatory ) {
		$this->set( 'm_intIsMandatory', CStrings::strToIntDef( $intIsMandatory, NULL, false ) );
	}

	public function getIsMandatory() {
		return $this->m_intIsMandatory;
	}

	public function sqlIsMandatory() {
		return ( true == isset( $this->m_intIsMandatory ) ) ? ( string ) $this->m_intIsMandatory : '0';
	}

	public function setIsAvailable( $intIsAvailable ) {
		$this->set( 'm_intIsAvailable', CStrings::strToIntDef( $intIsAvailable, NULL, false ) );
	}

	public function getIsAvailable() {
		return $this->m_intIsAvailable;
	}

	public function sqlIsAvailable() {
		return ( true == isset( $this->m_intIsAvailable ) ) ? ( string ) $this->m_intIsAvailable : '1';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_carrier_id, insurance_policy_type_id, insurance_amount_type_id, insurance_endorsement_type_id, parent_insurance_carrier_endorsement_id, state_code, base_rate, base_multiplier, is_default, is_basic_option, is_mandatory, is_available, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlInsurancePolicyTypeId() . ', ' .
 						$this->sqlInsuranceAmountTypeId() . ', ' .
 						$this->sqlInsuranceEndorsementTypeId() . ', ' .
 						$this->sqlParentInsuranceCarrierEndorsementId() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlBaseRate() . ', ' .
 						$this->sqlBaseMultiplier() . ', ' .
 						$this->sqlIsDefault() . ', ' .
 						$this->sqlIsBasicOption() . ', ' .
 						$this->sqlIsMandatory() . ', ' .
 						$this->sqlIsAvailable() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceAmountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_amount_type_id = ' . $this->sqlInsuranceAmountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_endorsement_type_id = ' . $this->sqlInsuranceEndorsementTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceEndorsementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_endorsement_type_id = ' . $this->sqlInsuranceEndorsementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_insurance_carrier_endorsement_id = ' . $this->sqlParentInsuranceCarrierEndorsementId() . ','; } elseif( true == array_key_exists( 'ParentInsuranceCarrierEndorsementId', $this->getChangedColumns() ) ) { $strSql .= ' parent_insurance_carrier_endorsement_id = ' . $this->sqlParentInsuranceCarrierEndorsementId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; } elseif( true == array_key_exists( 'BaseRate', $this->getChangedColumns() ) ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_multiplier = ' . $this->sqlBaseMultiplier() . ','; } elseif( true == array_key_exists( 'BaseMultiplier', $this->getChangedColumns() ) ) { $strSql .= ' base_multiplier = ' . $this->sqlBaseMultiplier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_basic_option = ' . $this->sqlIsBasicOption() . ','; } elseif( true == array_key_exists( 'IsBasicOption', $this->getChangedColumns() ) ) { $strSql .= ' is_basic_option = ' . $this->sqlIsBasicOption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory() . ','; } elseif( true == array_key_exists( 'IsMandatory', $this->getChangedColumns() ) ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; } elseif( true == array_key_exists( 'IsAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'insurance_amount_type_id' => $this->getInsuranceAmountTypeId(),
			'insurance_endorsement_type_id' => $this->getInsuranceEndorsementTypeId(),
			'parent_insurance_carrier_endorsement_id' => $this->getParentInsuranceCarrierEndorsementId(),
			'state_code' => $this->getStateCode(),
			'base_rate' => $this->getBaseRate(),
			'base_multiplier' => $this->getBaseMultiplier(),
			'is_default' => $this->getIsDefault(),
			'is_basic_option' => $this->getIsBasicOption(),
			'is_mandatory' => $this->getIsMandatory(),
			'is_available' => $this->getIsAvailable(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>