<?php

class CBasePropertyInsuranceLimit extends CEosSingularBase {

	const TABLE_NAME = 'public.property_insurance_limits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_fltPremiumRate;
	protected $m_fltInvoicePremium;
	protected $m_intDefaultLiabilityInsuranceCarrierLimitId;
	protected $m_intPersonalInsuranceCarrierLimitId;
	protected $m_intDeductibleInsuranceCarrierLimitId;
	protected $m_intIsDefault;
	protected $m_intIsAllowedLiabilityOnly;
	protected $m_intMinLiabilityInsuranceCarrierLimitId;
	protected $m_intMaxLiabilityInsuranceCarrierLimitId;
	protected $m_intMinDeductibleInsuranceCarrierLimitId;
	protected $m_intIsActive;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDefaultMedicalPaymentLimitId;
	protected $m_intEnhancementEndorsementId;

	public function __construct() {
		parent::__construct();

		$this->m_intInsurancePolicyTypeId = '1';
		$this->m_intPersonalInsuranceCarrierLimitId = '2';
		$this->m_intDeductibleInsuranceCarrierLimitId = '10';
		$this->m_intIsDefault = '0';
		$this->m_intIsAllowedLiabilityOnly = '0';
		$this->m_intIsActive = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['premium_rate'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumRate', trim( $arrValues['premium_rate'] ) ); elseif( isset( $arrValues['premium_rate'] ) ) $this->setPremiumRate( $arrValues['premium_rate'] );
		if( isset( $arrValues['invoice_premium'] ) && $boolDirectSet ) $this->set( 'm_fltInvoicePremium', trim( $arrValues['invoice_premium'] ) ); elseif( isset( $arrValues['invoice_premium'] ) ) $this->setInvoicePremium( $arrValues['invoice_premium'] );
		if( isset( $arrValues['default_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLiabilityInsuranceCarrierLimitId', trim( $arrValues['default_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['default_liability_insurance_carrier_limit_id'] ) ) $this->setDefaultLiabilityInsuranceCarrierLimitId( $arrValues['default_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['personal_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonalInsuranceCarrierLimitId', trim( $arrValues['personal_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['personal_insurance_carrier_limit_id'] ) ) $this->setPersonalInsuranceCarrierLimitId( $arrValues['personal_insurance_carrier_limit_id'] );
		if( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDeductibleInsuranceCarrierLimitId', trim( $arrValues['deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) ) $this->setDeductibleInsuranceCarrierLimitId( $arrValues['deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['is_default'] ) && $boolDirectSet ) $this->set( 'm_intIsDefault', trim( $arrValues['is_default'] ) ); elseif( isset( $arrValues['is_default'] ) ) $this->setIsDefault( $arrValues['is_default'] );
		if( isset( $arrValues['is_allowed_liability_only'] ) && $boolDirectSet ) $this->set( 'm_intIsAllowedLiabilityOnly', trim( $arrValues['is_allowed_liability_only'] ) ); elseif( isset( $arrValues['is_allowed_liability_only'] ) ) $this->setIsAllowedLiabilityOnly( $arrValues['is_allowed_liability_only'] );
		if( isset( $arrValues['min_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMinLiabilityInsuranceCarrierLimitId', trim( $arrValues['min_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['min_liability_insurance_carrier_limit_id'] ) ) $this->setMinLiabilityInsuranceCarrierLimitId( $arrValues['min_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['max_liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxLiabilityInsuranceCarrierLimitId', trim( $arrValues['max_liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['max_liability_insurance_carrier_limit_id'] ) ) $this->setMaxLiabilityInsuranceCarrierLimitId( $arrValues['max_liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['min_deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intMinDeductibleInsuranceCarrierLimitId', trim( $arrValues['min_deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['min_deductible_insurance_carrier_limit_id'] ) ) $this->setMinDeductibleInsuranceCarrierLimitId( $arrValues['min_deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['default_medical_payment_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultMedicalPaymentLimitId', trim( $arrValues['default_medical_payment_limit_id'] ) ); elseif( isset( $arrValues['default_medical_payment_limit_id'] ) ) $this->setDefaultMedicalPaymentLimitId( $arrValues['default_medical_payment_limit_id'] );
		if( isset( $arrValues['enhancement_endorsement_id'] ) && $boolDirectSet ) $this->set( 'm_intEnhancementEndorsementId', trim( $arrValues['enhancement_endorsement_id'] ) ); elseif( isset( $arrValues['enhancement_endorsement_id'] ) ) $this->setEnhancementEndorsementId( $arrValues['enhancement_endorsement_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : '1';
	}

	public function setPremiumRate( $fltPremiumRate ) {
		$this->set( 'm_fltPremiumRate', CStrings::strToFloatDef( $fltPremiumRate, NULL, false, 2 ) );
	}

	public function getPremiumRate() {
		return $this->m_fltPremiumRate;
	}

	public function sqlPremiumRate() {
		return ( true == isset( $this->m_fltPremiumRate ) ) ? ( string ) $this->m_fltPremiumRate : 'NULL';
	}

	public function setInvoicePremium( $fltInvoicePremium ) {
		$this->set( 'm_fltInvoicePremium', CStrings::strToFloatDef( $fltInvoicePremium, NULL, false, 2 ) );
	}

	public function getInvoicePremium() {
		return $this->m_fltInvoicePremium;
	}

	public function sqlInvoicePremium() {
		return ( true == isset( $this->m_fltInvoicePremium ) ) ? ( string ) $this->m_fltInvoicePremium : 'NULL';
	}

	public function setDefaultLiabilityInsuranceCarrierLimitId( $intDefaultLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intDefaultLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intDefaultLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDefaultLiabilityInsuranceCarrierLimitId() {
		return $this->m_intDefaultLiabilityInsuranceCarrierLimitId;
	}

	public function sqlDefaultLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDefaultLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDefaultLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId ) {
		$this->set( 'm_intPersonalInsuranceCarrierLimitId', CStrings::strToIntDef( $intPersonalInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getPersonalInsuranceCarrierLimitId() {
		return $this->m_intPersonalInsuranceCarrierLimitId;
	}

	public function sqlPersonalInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intPersonalInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intPersonalInsuranceCarrierLimitId : '2';
	}

	public function setDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDeductibleInsuranceCarrierLimitId() {
		return $this->m_intDeductibleInsuranceCarrierLimitId;
	}

	public function sqlDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDeductibleInsuranceCarrierLimitId : '10';
	}

	public function setIsDefault( $intIsDefault ) {
		$this->set( 'm_intIsDefault', CStrings::strToIntDef( $intIsDefault, NULL, false ) );
	}

	public function getIsDefault() {
		return $this->m_intIsDefault;
	}

	public function sqlIsDefault() {
		return ( true == isset( $this->m_intIsDefault ) ) ? ( string ) $this->m_intIsDefault : '0';
	}

	public function setIsAllowedLiabilityOnly( $intIsAllowedLiabilityOnly ) {
		$this->set( 'm_intIsAllowedLiabilityOnly', CStrings::strToIntDef( $intIsAllowedLiabilityOnly, NULL, false ) );
	}

	public function getIsAllowedLiabilityOnly() {
		return $this->m_intIsAllowedLiabilityOnly;
	}

	public function sqlIsAllowedLiabilityOnly() {
		return ( true == isset( $this->m_intIsAllowedLiabilityOnly ) ) ? ( string ) $this->m_intIsAllowedLiabilityOnly : '0';
	}

	public function setMinLiabilityInsuranceCarrierLimitId( $intMinLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intMinLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intMinLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMinLiabilityInsuranceCarrierLimitId() {
		return $this->m_intMinLiabilityInsuranceCarrierLimitId;
	}

	public function sqlMinLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMinLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMinLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setMaxLiabilityInsuranceCarrierLimitId( $intMaxLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intMaxLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intMaxLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMaxLiabilityInsuranceCarrierLimitId() {
		return $this->m_intMaxLiabilityInsuranceCarrierLimitId;
	}

	public function sqlMaxLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMaxLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMaxLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setMinDeductibleInsuranceCarrierLimitId( $intMinDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intMinDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intMinDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getMinDeductibleInsuranceCarrierLimitId() {
		return $this->m_intMinDeductibleInsuranceCarrierLimitId;
	}

	public function sqlMinDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intMinDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intMinDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDefaultMedicalPaymentLimitId( $intDefaultMedicalPaymentLimitId ) {
		$this->set( 'm_intDefaultMedicalPaymentLimitId', CStrings::strToIntDef( $intDefaultMedicalPaymentLimitId, NULL, false ) );
	}

	public function getDefaultMedicalPaymentLimitId() {
		return $this->m_intDefaultMedicalPaymentLimitId;
	}

	public function sqlDefaultMedicalPaymentLimitId() {
		return ( true == isset( $this->m_intDefaultMedicalPaymentLimitId ) ) ? ( string ) $this->m_intDefaultMedicalPaymentLimitId : 'NULL';
	}

	public function setEnhancementEndorsementId( $intEnhancementEndorsementId ) {
		$this->set( 'm_intEnhancementEndorsementId', CStrings::strToIntDef( $intEnhancementEndorsementId, NULL, false ) );
	}

	public function getEnhancementEndorsementId() {
		return $this->m_intEnhancementEndorsementId;
	}

	public function sqlEnhancementEndorsementId() {
		return ( true == isset( $this->m_intEnhancementEndorsementId ) ) ? ( string ) $this->m_intEnhancementEndorsementId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, insurance_policy_type_id, premium_rate, invoice_premium, default_liability_insurance_carrier_limit_id, personal_insurance_carrier_limit_id, deductible_insurance_carrier_limit_id, is_default, is_allowed_liability_only, min_liability_insurance_carrier_limit_id, max_liability_insurance_carrier_limit_id, min_deductible_insurance_carrier_limit_id, is_active, created_by, created_on, default_medical_payment_limit_id, enhancement_endorsement_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlInsurancePolicyTypeId() . ', ' .
						$this->sqlPremiumRate() . ', ' .
						$this->sqlInvoicePremium() . ', ' .
						$this->sqlDefaultLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlPersonalInsuranceCarrierLimitId() . ', ' .
						$this->sqlDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlIsDefault() . ', ' .
						$this->sqlIsAllowedLiabilityOnly() . ', ' .
						$this->sqlMinLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlMaxLiabilityInsuranceCarrierLimitId() . ', ' .
						$this->sqlMinDeductibleInsuranceCarrierLimitId() . ', ' .
						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDefaultMedicalPaymentLimitId() . ', ' .
						$this->sqlEnhancementEndorsementId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_rate = ' . $this->sqlPremiumRate(). ',' ; } elseif( true == array_key_exists( 'PremiumRate', $this->getChangedColumns() ) ) { $strSql .= ' premium_rate = ' . $this->sqlPremiumRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_premium = ' . $this->sqlInvoicePremium(). ',' ; } elseif( true == array_key_exists( 'InvoicePremium', $this->getChangedColumns() ) ) { $strSql .= ' invoice_premium = ' . $this->sqlInvoicePremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_liability_insurance_carrier_limit_id = ' . $this->sqlDefaultLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DefaultLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' default_liability_insurance_carrier_limit_id = ' . $this->sqlDefaultLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'PersonalInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_default = ' . $this->sqlIsDefault(). ',' ; } elseif( true == array_key_exists( 'IsDefault', $this->getChangedColumns() ) ) { $strSql .= ' is_default = ' . $this->sqlIsDefault() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allowed_liability_only = ' . $this->sqlIsAllowedLiabilityOnly(). ',' ; } elseif( true == array_key_exists( 'IsAllowedLiabilityOnly', $this->getChangedColumns() ) ) { $strSql .= ' is_allowed_liability_only = ' . $this->sqlIsAllowedLiabilityOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_liability_insurance_carrier_limit_id = ' . $this->sqlMinLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MinLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' min_liability_insurance_carrier_limit_id = ' . $this->sqlMinLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_liability_insurance_carrier_limit_id = ' . $this->sqlMaxLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MaxLiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' max_liability_insurance_carrier_limit_id = ' . $this->sqlMaxLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_deductible_insurance_carrier_limit_id = ' . $this->sqlMinDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'MinDeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' min_deductible_insurance_carrier_limit_id = ' . $this->sqlMinDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_medical_payment_limit_id = ' . $this->sqlDefaultMedicalPaymentLimitId(). ',' ; } elseif( true == array_key_exists( 'DefaultMedicalPaymentLimitId', $this->getChangedColumns() ) ) { $strSql .= ' default_medical_payment_limit_id = ' . $this->sqlDefaultMedicalPaymentLimitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enhancement_endorsement_id = ' . $this->sqlEnhancementEndorsementId() ; } elseif( true == array_key_exists( 'EnhancementEndorsementId', $this->getChangedColumns() ) ) { $strSql .= ' enhancement_endorsement_id = ' . $this->sqlEnhancementEndorsementId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'premium_rate' => $this->getPremiumRate(),
			'invoice_premium' => $this->getInvoicePremium(),
			'default_liability_insurance_carrier_limit_id' => $this->getDefaultLiabilityInsuranceCarrierLimitId(),
			'personal_insurance_carrier_limit_id' => $this->getPersonalInsuranceCarrierLimitId(),
			'deductible_insurance_carrier_limit_id' => $this->getDeductibleInsuranceCarrierLimitId(),
			'is_default' => $this->getIsDefault(),
			'is_allowed_liability_only' => $this->getIsAllowedLiabilityOnly(),
			'min_liability_insurance_carrier_limit_id' => $this->getMinLiabilityInsuranceCarrierLimitId(),
			'max_liability_insurance_carrier_limit_id' => $this->getMaxLiabilityInsuranceCarrierLimitId(),
			'min_deductible_insurance_carrier_limit_id' => $this->getMinDeductibleInsuranceCarrierLimitId(),
			'is_active' => $this->getIsActive(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'default_medical_payment_limit_id' => $this->getDefaultMedicalPaymentLimitId(),
			'enhancement_endorsement_id' => $this->getEnhancementEndorsementId()
		);
	}

}
?>