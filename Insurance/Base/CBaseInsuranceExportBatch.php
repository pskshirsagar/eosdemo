<?php

class CBaseInsuranceExportBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_export_batches';

	protected $m_intId;
	protected $m_intInsuranceBatchTypeId;
	protected $m_intBatchStatusTypeId;
	protected $m_strCarrierBatchReference;
	protected $m_strBatchDate;
	protected $m_strExportedOn;
	protected $m_strFileName;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceBatchTypeId', trim( $arrValues['insurance_batch_type_id'] ) ); elseif( isset( $arrValues['insurance_batch_type_id'] ) ) $this->setInsuranceBatchTypeId( $arrValues['insurance_batch_type_id'] );
		if( isset( $arrValues['batch_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBatchStatusTypeId', trim( $arrValues['batch_status_type_id'] ) ); elseif( isset( $arrValues['batch_status_type_id'] ) ) $this->setBatchStatusTypeId( $arrValues['batch_status_type_id'] );
		if( isset( $arrValues['carrier_batch_reference'] ) && $boolDirectSet ) $this->set( 'm_strCarrierBatchReference', trim( stripcslashes( $arrValues['carrier_batch_reference'] ) ) ); elseif( isset( $arrValues['carrier_batch_reference'] ) ) $this->setCarrierBatchReference( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['carrier_batch_reference'] ) : $arrValues['carrier_batch_reference'] );
		if( isset( $arrValues['batch_date'] ) && $boolDirectSet ) $this->set( 'm_strBatchDate', trim( $arrValues['batch_date'] ) ); elseif( isset( $arrValues['batch_date'] ) ) $this->setBatchDate( $arrValues['batch_date'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceBatchTypeId( $intInsuranceBatchTypeId ) {
		$this->set( 'm_intInsuranceBatchTypeId', CStrings::strToIntDef( $intInsuranceBatchTypeId, NULL, false ) );
	}

	public function getInsuranceBatchTypeId() {
		return $this->m_intInsuranceBatchTypeId;
	}

	public function sqlInsuranceBatchTypeId() {
		return ( true == isset( $this->m_intInsuranceBatchTypeId ) ) ? ( string ) $this->m_intInsuranceBatchTypeId : 'NULL';
	}

	public function setBatchStatusTypeId( $intBatchStatusTypeId ) {
		$this->set( 'm_intBatchStatusTypeId', CStrings::strToIntDef( $intBatchStatusTypeId, NULL, false ) );
	}

	public function getBatchStatusTypeId() {
		return $this->m_intBatchStatusTypeId;
	}

	public function sqlBatchStatusTypeId() {
		return ( true == isset( $this->m_intBatchStatusTypeId ) ) ? ( string ) $this->m_intBatchStatusTypeId : 'NULL';
	}

	public function setCarrierBatchReference( $strCarrierBatchReference ) {
		$this->set( 'm_strCarrierBatchReference', CStrings::strTrimDef( $strCarrierBatchReference, 64, NULL, true ) );
	}

	public function getCarrierBatchReference() {
		return $this->m_strCarrierBatchReference;
	}

	public function sqlCarrierBatchReference() {
		return ( true == isset( $this->m_strCarrierBatchReference ) ) ? '\'' . addslashes( $this->m_strCarrierBatchReference ) . '\'' : 'NULL';
	}

	public function setBatchDate( $strBatchDate ) {
		$this->set( 'm_strBatchDate', CStrings::strTrimDef( $strBatchDate, -1, NULL, true ) );
	}

	public function getBatchDate() {
		return $this->m_strBatchDate;
	}

	public function sqlBatchDate() {
		return ( true == isset( $this->m_strBatchDate ) ) ? '\'' . $this->m_strBatchDate . '\'' : 'NOW()';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_batch_type_id, batch_status_type_id, carrier_batch_reference, batch_date, exported_on, file_name, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceBatchTypeId() . ', ' .
 						$this->sqlBatchStatusTypeId() . ', ' .
 						$this->sqlCarrierBatchReference() . ', ' .
 						$this->sqlBatchDate() . ', ' .
 						$this->sqlExportedOn() . ', ' .
 						$this->sqlFileName() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_batch_type_id = ' . $this->sqlInsuranceBatchTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_batch_type_id = ' . $this->sqlInsuranceBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_status_type_id = ' . $this->sqlBatchStatusTypeId() . ','; } elseif( true == array_key_exists( 'BatchStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' batch_status_type_id = ' . $this->sqlBatchStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_batch_reference = ' . $this->sqlCarrierBatchReference() . ','; } elseif( true == array_key_exists( 'CarrierBatchReference', $this->getChangedColumns() ) ) { $strSql .= ' carrier_batch_reference = ' . $this->sqlCarrierBatchReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate() . ','; } elseif( true == array_key_exists( 'BatchDate', $this->getChangedColumns() ) ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_batch_type_id' => $this->getInsuranceBatchTypeId(),
			'batch_status_type_id' => $this->getBatchStatusTypeId(),
			'carrier_batch_reference' => $this->getCarrierBatchReference(),
			'batch_date' => $this->getBatchDate(),
			'exported_on' => $this->getExportedOn(),
			'file_name' => $this->getFileName(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>