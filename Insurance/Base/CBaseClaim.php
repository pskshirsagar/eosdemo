<?php

class CBaseClaim extends CEosSingularBase {

	const TABLE_NAME = 'public.claims';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intInsuranceCarrierId;
	protected $m_intPolicyId;
	protected $m_strCarrierPolicyNumber;
	protected $m_strContactName;
	protected $m_strContactPhoneNumber;
	protected $m_strContactEmailAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsUploadFail;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsUploadFail = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['policy_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyId', trim( $arrValues['policy_id'] ) ); elseif( isset( $arrValues['policy_id'] ) ) $this->setPolicyId( $arrValues['policy_id'] );
		if( isset( $arrValues['carrier_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strCarrierPolicyNumber', trim( $arrValues['carrier_policy_number'] ) ); elseif( isset( $arrValues['carrier_policy_number'] ) ) $this->setCarrierPolicyNumber( $arrValues['carrier_policy_number'] );
		if( isset( $arrValues['contact_name'] ) && $boolDirectSet ) $this->set( 'm_strContactName', trim( $arrValues['contact_name'] ) ); elseif( isset( $arrValues['contact_name'] ) ) $this->setContactName( $arrValues['contact_name'] );
		if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneNumber', trim( $arrValues['contact_phone_number'] ) ); elseif( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( $arrValues['contact_phone_number'] );
		if( isset( $arrValues['contact_email_address'] ) && $boolDirectSet ) $this->set( 'm_strContactEmailAddress', trim( $arrValues['contact_email_address'] ) ); elseif( isset( $arrValues['contact_email_address'] ) ) $this->setContactEmailAddress( $arrValues['contact_email_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_upload_fail'] ) && $boolDirectSet ) $this->set( 'm_boolIsUploadFail', trim( stripcslashes( $arrValues['is_upload_fail'] ) ) ); elseif( isset( $arrValues['is_upload_fail'] ) ) $this->setIsUploadFail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_upload_fail'] ) : $arrValues['is_upload_fail'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setPolicyId( $intPolicyId ) {
		$this->set( 'm_intPolicyId', CStrings::strToIntDef( $intPolicyId, NULL, false ) );
	}

	public function getPolicyId() {
		return $this->m_intPolicyId;
	}

	public function sqlPolicyId() {
		return ( true == isset( $this->m_intPolicyId ) ) ? ( string ) $this->m_intPolicyId : 'NULL';
	}

	public function setCarrierPolicyNumber( $strCarrierPolicyNumber ) {
		$this->set( 'm_strCarrierPolicyNumber', CStrings::strTrimDef( $strCarrierPolicyNumber, 100, NULL, true ) );
	}

	public function getCarrierPolicyNumber() {
		return $this->m_strCarrierPolicyNumber;
	}

	public function sqlCarrierPolicyNumber() {
		return ( true == isset( $this->m_strCarrierPolicyNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCarrierPolicyNumber ) : '\'' . addslashes( $this->m_strCarrierPolicyNumber ) . '\'' ) : 'NULL';
	}

	public function setContactName( $strContactName ) {
		$this->set( 'm_strContactName', CStrings::strTrimDef( $strContactName, 100, NULL, true ) );
	}

	public function getContactName() {
		return $this->m_strContactName;
	}

	public function sqlContactName() {
		return ( true == isset( $this->m_strContactName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactName ) : '\'' . addslashes( $this->m_strContactName ) . '\'' ) : 'NULL';
	}

	public function setContactPhoneNumber( $strContactPhoneNumber ) {
		$this->set( 'm_strContactPhoneNumber', CStrings::strTrimDef( $strContactPhoneNumber, 20, NULL, true ) );
	}

	public function getContactPhoneNumber() {
		return $this->m_strContactPhoneNumber;
	}

	public function sqlContactPhoneNumber() {
		return ( true == isset( $this->m_strContactPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactPhoneNumber ) : '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setContactEmailAddress( $strContactEmailAddress ) {
		$this->set( 'm_strContactEmailAddress', CStrings::strTrimDef( $strContactEmailAddress, 100, NULL, true ) );
	}

	public function getContactEmailAddress() {
		return $this->m_strContactEmailAddress;
	}

	public function sqlContactEmailAddress() {
		return ( true == isset( $this->m_strContactEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactEmailAddress ) : '\'' . addslashes( $this->m_strContactEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsUploadFail( $boolIsUploadFail ) {
		$this->set( 'm_boolIsUploadFail', CStrings::strToBool( $boolIsUploadFail ) );
	}

	public function getIsUploadFail() {
		return $this->m_boolIsUploadFail;
	}

	public function sqlIsUploadFail() {
		return ( true == isset( $this->m_boolIsUploadFail ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUploadFail ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, insurance_carrier_id, policy_id, carrier_policy_number, contact_name, contact_phone_number, contact_email_address, updated_by, updated_on, created_by, created_on, is_upload_fail )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlInsuranceCarrierId() . ', ' .
						$this->sqlPolicyId() . ', ' .
						$this->sqlCarrierPolicyNumber() . ', ' .
						$this->sqlContactName() . ', ' .
						$this->sqlContactPhoneNumber() . ', ' .
						$this->sqlContactEmailAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsUploadFail() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_id = ' . $this->sqlPolicyId(). ',' ; } elseif( true == array_key_exists( 'PolicyId', $this->getChangedColumns() ) ) { $strSql .= ' policy_id = ' . $this->sqlPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'CarrierPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name = ' . $this->sqlContactName(). ',' ; } elseif( true == array_key_exists( 'ContactName', $this->getChangedColumns() ) ) { $strSql .= ' contact_name = ' . $this->sqlContactName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email_address = ' . $this->sqlContactEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ContactEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' contact_email_address = ' . $this->sqlContactEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_upload_fail = ' . $this->sqlIsUploadFail(). ',' ; } elseif( true == array_key_exists( 'IsUploadFail', $this->getChangedColumns() ) ) { $strSql .= ' is_upload_fail = ' . $this->sqlIsUploadFail() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'policy_id' => $this->getPolicyId(),
			'carrier_policy_number' => $this->getCarrierPolicyNumber(),
			'contact_name' => $this->getContactName(),
			'contact_phone_number' => $this->getContactPhoneNumber(),
			'contact_email_address' => $this->getContactEmailAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_upload_fail' => $this->getIsUploadFail()
		);
	}

}
?>