<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceExportTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceExportTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceExportType[]
	 */
	public static function fetchInsuranceExportTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceExportType::class, $objDatabase );
	}

	/**
	 * @return CInsuranceExportType
	 */
	public static function fetchInsuranceExportType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceExportType::class, $objDatabase );
	}

	public static function fetchInsuranceExportTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_export_types', $objDatabase );
	}

	public static function fetchInsuranceExportTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceExportType( sprintf( 'SELECT * FROM insurance_export_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>