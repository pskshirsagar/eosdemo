<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceEndorsementTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceEndorsementTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceEndorsementType[]
	 */
	public static function fetchInsuranceEndorsementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceEndorsementType', $objDatabase );
	}

	/**
	 * @return CInsuranceEndorsementType
	 */
	public static function fetchInsuranceEndorsementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceEndorsementType', $objDatabase );
	}

	public static function fetchInsuranceEndorsementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_endorsement_types', $objDatabase );
	}

	public static function fetchInsuranceEndorsementTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceEndorsementType( sprintf( 'SELECT * FROM insurance_endorsement_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>