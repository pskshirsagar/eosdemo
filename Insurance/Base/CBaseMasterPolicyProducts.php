<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyProducts
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyProducts extends CEosPluralBase {

	/**
	 * @return CMasterPolicyProduct[]
	 */
	public static function fetchMasterPolicyProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyProduct::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyProduct
	 */
	public static function fetchMasterPolicyProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyProduct::class, $objDatabase );
	}

	public static function fetchMasterPolicyProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_products', $objDatabase );
	}

	public static function fetchMasterPolicyProductById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyProduct( sprintf( 'SELECT * FROM master_policy_products WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMasterPolicyProductsByCid( $intCid, $objDatabase ) {
		return self::fetchMasterPolicyProducts( sprintf( 'SELECT * FROM master_policy_products WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMasterPolicyProductsByMasterPolicyClientId( $intMasterPolicyClientId, $objDatabase ) {
		return self::fetchMasterPolicyProducts( sprintf( 'SELECT * FROM master_policy_products WHERE master_policy_client_id = %d', $intMasterPolicyClientId ), $objDatabase );
	}

	public static function fetchMasterPolicyProductsByLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicyProducts( sprintf( 'SELECT * FROM master_policy_products WHERE liability_insurance_carrier_limit_id = %d', $intLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchMasterPolicyProductsByPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicyProducts( sprintf( 'SELECT * FROM master_policy_products WHERE personal_insurance_carrier_limit_id = %d', $intPersonalInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchMasterPolicyProductsByDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchMasterPolicyProducts( sprintf( 'SELECT * FROM master_policy_products WHERE deductible_insurance_carrier_limit_id = %d', $intDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

}
?>