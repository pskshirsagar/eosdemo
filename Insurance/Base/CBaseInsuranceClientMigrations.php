<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceClientMigrations
 * Do not add any new functions to this class.
 */

class CBaseInsuranceClientMigrations extends CEosPluralBase {

	/**
	 * @return CInsuranceClientMigration[]
	 */
	public static function fetchInsuranceClientMigrations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceClientMigration::class, $objDatabase );
	}

	/**
	 * @return CInsuranceClientMigration
	 */
	public static function fetchInsuranceClientMigration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceClientMigration::class, $objDatabase );
	}

	public static function fetchInsuranceClientMigrationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_client_migrations', $objDatabase );
	}

	public static function fetchInsuranceClientMigrationById( $intId, $objDatabase ) {
		return self::fetchInsuranceClientMigration( sprintf( 'SELECT * FROM insurance_client_migrations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsuranceClientMigrationsBySourceCid( $intSourceCid, $objDatabase ) {
		return self::fetchInsuranceClientMigrations( sprintf( 'SELECT * FROM insurance_client_migrations WHERE source_cid = %d', $intSourceCid ), $objDatabase );
	}

	public static function fetchInsuranceClientMigrationsByDestinationCid( $intDestinationCid, $objDatabase ) {
		return self::fetchInsuranceClientMigrations( sprintf( 'SELECT * FROM insurance_client_migrations WHERE destination_cid = %d', $intDestinationCid ), $objDatabase );
	}

	public static function fetchInsuranceClientMigrationsBySourcePropertyId( $intSourcePropertyId, $objDatabase ) {
		return self::fetchInsuranceClientMigrations( sprintf( 'SELECT * FROM insurance_client_migrations WHERE source_property_id = %d', $intSourcePropertyId ), $objDatabase );
	}

	public static function fetchInsuranceClientMigrationsByDestinationPropertyId( $intDestinationPropertyId, $objDatabase ) {
		return self::fetchInsuranceClientMigrations( sprintf( 'SELECT * FROM insurance_client_migrations WHERE destination_property_id = %d', $intDestinationPropertyId ), $objDatabase );
	}

}
?>