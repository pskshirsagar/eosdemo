<?php

class CBaseInsuranceBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInsuranceBatchTypeId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsuranceBatchStatusTypeId;
	protected $m_intPolicyNumber;
	protected $m_strCarrierBatchReference;
	protected $m_strFileName;
	protected $m_strExportedOn;
	protected $m_strBatchDate;
	protected $m_strReconciledOn;
	protected $m_fltTotalPremiumAmount;
	protected $m_fltTotalInvoiceAmount;
	protected $m_fltTotalCommission;
	protected $m_strDataSent;
	protected $m_strDataReceived;
	protected $m_strRequestStartTime;
	protected $m_strRequestEndTime;
	protected $m_boolIsFailed;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFailed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['insurance_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceBatchTypeId', trim( $arrValues['insurance_batch_type_id'] ) ); elseif( isset( $arrValues['insurance_batch_type_id'] ) ) $this->setInsuranceBatchTypeId( $arrValues['insurance_batch_type_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_batch_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceBatchStatusTypeId', trim( $arrValues['insurance_batch_status_type_id'] ) ); elseif( isset( $arrValues['insurance_batch_status_type_id'] ) ) $this->setInsuranceBatchStatusTypeId( $arrValues['insurance_batch_status_type_id'] );
		if( isset( $arrValues['policy_number'] ) && $boolDirectSet ) $this->set( 'm_intPolicyNumber', trim( $arrValues['policy_number'] ) ); elseif( isset( $arrValues['policy_number'] ) ) $this->setPolicyNumber( $arrValues['policy_number'] );
		if( isset( $arrValues['carrier_batch_reference'] ) && $boolDirectSet ) $this->set( 'm_strCarrierBatchReference', trim( stripcslashes( $arrValues['carrier_batch_reference'] ) ) ); elseif( isset( $arrValues['carrier_batch_reference'] ) ) $this->setCarrierBatchReference( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['carrier_batch_reference'] ) : $arrValues['carrier_batch_reference'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['batch_date'] ) && $boolDirectSet ) $this->set( 'm_strBatchDate', trim( $arrValues['batch_date'] ) ); elseif( isset( $arrValues['batch_date'] ) ) $this->setBatchDate( $arrValues['batch_date'] );
		if( isset( $arrValues['reconciled_on'] ) && $boolDirectSet ) $this->set( 'm_strReconciledOn', trim( $arrValues['reconciled_on'] ) ); elseif( isset( $arrValues['reconciled_on'] ) ) $this->setReconciledOn( $arrValues['reconciled_on'] );
		if( isset( $arrValues['total_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalPremiumAmount', trim( $arrValues['total_premium_amount'] ) ); elseif( isset( $arrValues['total_premium_amount'] ) ) $this->setTotalPremiumAmount( $arrValues['total_premium_amount'] );
		if( isset( $arrValues['total_invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalInvoiceAmount', trim( $arrValues['total_invoice_amount'] ) ); elseif( isset( $arrValues['total_invoice_amount'] ) ) $this->setTotalInvoiceAmount( $arrValues['total_invoice_amount'] );
		if( isset( $arrValues['total_commission'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCommission', trim( $arrValues['total_commission'] ) ); elseif( isset( $arrValues['total_commission'] ) ) $this->setTotalCommission( $arrValues['total_commission'] );
		if( isset( $arrValues['data_sent'] ) && $boolDirectSet ) $this->set( 'm_strDataSent', trim( stripcslashes( $arrValues['data_sent'] ) ) ); elseif( isset( $arrValues['data_sent'] ) ) $this->setDataSent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_sent'] ) : $arrValues['data_sent'] );
		if( isset( $arrValues['data_received'] ) && $boolDirectSet ) $this->set( 'm_strDataReceived', trim( stripcslashes( $arrValues['data_received'] ) ) ); elseif( isset( $arrValues['data_received'] ) ) $this->setDataReceived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_received'] ) : $arrValues['data_received'] );
		if( isset( $arrValues['request_start_time'] ) && $boolDirectSet ) $this->set( 'm_strRequestStartTime', trim( $arrValues['request_start_time'] ) ); elseif( isset( $arrValues['request_start_time'] ) ) $this->setRequestStartTime( $arrValues['request_start_time'] );
		if( isset( $arrValues['request_end_time'] ) && $boolDirectSet ) $this->set( 'm_strRequestEndTime', trim( $arrValues['request_end_time'] ) ); elseif( isset( $arrValues['request_end_time'] ) ) $this->setRequestEndTime( $arrValues['request_end_time'] );
		if( isset( $arrValues['is_failed'] ) && $boolDirectSet ) $this->set( 'm_boolIsFailed', trim( stripcslashes( $arrValues['is_failed'] ) ) ); elseif( isset( $arrValues['is_failed'] ) ) $this->setIsFailed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_failed'] ) : $arrValues['is_failed'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInsuranceBatchTypeId( $intInsuranceBatchTypeId ) {
		$this->set( 'm_intInsuranceBatchTypeId', CStrings::strToIntDef( $intInsuranceBatchTypeId, NULL, false ) );
	}

	public function getInsuranceBatchTypeId() {
		return $this->m_intInsuranceBatchTypeId;
	}

	public function sqlInsuranceBatchTypeId() {
		return ( true == isset( $this->m_intInsuranceBatchTypeId ) ) ? ( string ) $this->m_intInsuranceBatchTypeId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsuranceBatchStatusTypeId( $intInsuranceBatchStatusTypeId ) {
		$this->set( 'm_intInsuranceBatchStatusTypeId', CStrings::strToIntDef( $intInsuranceBatchStatusTypeId, NULL, false ) );
	}

	public function getInsuranceBatchStatusTypeId() {
		return $this->m_intInsuranceBatchStatusTypeId;
	}

	public function sqlInsuranceBatchStatusTypeId() {
		return ( true == isset( $this->m_intInsuranceBatchStatusTypeId ) ) ? ( string ) $this->m_intInsuranceBatchStatusTypeId : 'NULL';
	}

	public function setPolicyNumber( $intPolicyNumber ) {
		$this->set( 'm_intPolicyNumber', CStrings::strToIntDef( $intPolicyNumber, NULL, false ) );
	}

	public function getPolicyNumber() {
		return $this->m_intPolicyNumber;
	}

	public function sqlPolicyNumber() {
		return ( true == isset( $this->m_intPolicyNumber ) ) ? ( string ) $this->m_intPolicyNumber : 'NULL';
	}

	public function setCarrierBatchReference( $strCarrierBatchReference ) {
		$this->set( 'm_strCarrierBatchReference', CStrings::strTrimDef( $strCarrierBatchReference, 64, NULL, true ) );
	}

	public function getCarrierBatchReference() {
		return $this->m_strCarrierBatchReference;
	}

	public function sqlCarrierBatchReference() {
		return ( true == isset( $this->m_strCarrierBatchReference ) ) ? '\'' . addslashes( $this->m_strCarrierBatchReference ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 250, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setBatchDate( $strBatchDate ) {
		$this->set( 'm_strBatchDate', CStrings::strTrimDef( $strBatchDate, -1, NULL, true ) );
	}

	public function getBatchDate() {
		return $this->m_strBatchDate;
	}

	public function sqlBatchDate() {
		return ( true == isset( $this->m_strBatchDate ) ) ? '\'' . $this->m_strBatchDate . '\'' : 'NOW()';
	}

	public function setReconciledOn( $strReconciledOn ) {
		$this->set( 'm_strReconciledOn', CStrings::strTrimDef( $strReconciledOn, -1, NULL, true ) );
	}

	public function getReconciledOn() {
		return $this->m_strReconciledOn;
	}

	public function sqlReconciledOn() {
		return ( true == isset( $this->m_strReconciledOn ) ) ? '\'' . $this->m_strReconciledOn . '\'' : 'NULL';
	}

	public function setTotalPremiumAmount( $fltTotalPremiumAmount ) {
		$this->set( 'm_fltTotalPremiumAmount', CStrings::strToFloatDef( $fltTotalPremiumAmount, NULL, false, 2 ) );
	}

	public function getTotalPremiumAmount() {
		return $this->m_fltTotalPremiumAmount;
	}

	public function sqlTotalPremiumAmount() {
		return ( true == isset( $this->m_fltTotalPremiumAmount ) ) ? ( string ) $this->m_fltTotalPremiumAmount : 'NULL';
	}

	public function setTotalInvoiceAmount( $fltTotalInvoiceAmount ) {
		$this->set( 'm_fltTotalInvoiceAmount', CStrings::strToFloatDef( $fltTotalInvoiceAmount, NULL, false, 2 ) );
	}

	public function getTotalInvoiceAmount() {
		return $this->m_fltTotalInvoiceAmount;
	}

	public function sqlTotalInvoiceAmount() {
		return ( true == isset( $this->m_fltTotalInvoiceAmount ) ) ? ( string ) $this->m_fltTotalInvoiceAmount : 'NULL';
	}

	public function setTotalCommission( $fltTotalCommission ) {
		$this->set( 'm_fltTotalCommission', CStrings::strToFloatDef( $fltTotalCommission, NULL, false, 2 ) );
	}

	public function getTotalCommission() {
		return $this->m_fltTotalCommission;
	}

	public function sqlTotalCommission() {
		return ( true == isset( $this->m_fltTotalCommission ) ) ? ( string ) $this->m_fltTotalCommission : 'NULL';
	}

	public function setDataSent( $strDataSent ) {
		$this->set( 'm_strDataSent', CStrings::strTrimDef( $strDataSent, -1, NULL, true ) );
	}

	public function getDataSent() {
		return $this->m_strDataSent;
	}

	public function sqlDataSent() {
		return ( true == isset( $this->m_strDataSent ) ) ? '\'' . addslashes( $this->m_strDataSent ) . '\'' : 'NULL';
	}

	public function setDataReceived( $strDataReceived ) {
		$this->set( 'm_strDataReceived', CStrings::strTrimDef( $strDataReceived, -1, NULL, true ) );
	}

	public function getDataReceived() {
		return $this->m_strDataReceived;
	}

	public function sqlDataReceived() {
		return ( true == isset( $this->m_strDataReceived ) ) ? '\'' . addslashes( $this->m_strDataReceived ) . '\'' : 'NULL';
	}

	public function setRequestStartTime( $strRequestStartTime ) {
		$this->set( 'm_strRequestStartTime', CStrings::strTrimDef( $strRequestStartTime, -1, NULL, true ) );
	}

	public function getRequestStartTime() {
		return $this->m_strRequestStartTime;
	}

	public function sqlRequestStartTime() {
		return ( true == isset( $this->m_strRequestStartTime ) ) ? '\'' . $this->m_strRequestStartTime . '\'' : 'NULL';
	}

	public function setRequestEndTime( $strRequestEndTime ) {
		$this->set( 'm_strRequestEndTime', CStrings::strTrimDef( $strRequestEndTime, -1, NULL, true ) );
	}

	public function getRequestEndTime() {
		return $this->m_strRequestEndTime;
	}

	public function sqlRequestEndTime() {
		return ( true == isset( $this->m_strRequestEndTime ) ) ? '\'' . $this->m_strRequestEndTime . '\'' : 'NULL';
	}

	public function setIsFailed( $boolIsFailed ) {
		$this->set( 'm_boolIsFailed', CStrings::strToBool( $boolIsFailed ) );
	}

	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFailed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, insurance_batch_type_id, insurance_carrier_id, insurance_batch_status_type_id, policy_number, carrier_batch_reference, file_name, exported_on, batch_date, reconciled_on, total_premium_amount, total_invoice_amount, total_commission, data_sent, data_received, request_start_time, request_end_time, is_failed, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlInsuranceBatchTypeId() . ', ' .
 						$this->sqlInsuranceCarrierId() . ', ' .
 						$this->sqlInsuranceBatchStatusTypeId() . ', ' .
 						$this->sqlPolicyNumber() . ', ' .
 						$this->sqlCarrierBatchReference() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlExportedOn() . ', ' .
 						$this->sqlBatchDate() . ', ' .
 						$this->sqlReconciledOn() . ', ' .
 						$this->sqlTotalPremiumAmount() . ', ' .
 						$this->sqlTotalInvoiceAmount() . ', ' .
 						$this->sqlTotalCommission() . ', ' .
 						$this->sqlDataSent() . ', ' .
 						$this->sqlDataReceived() . ', ' .
 						$this->sqlRequestStartTime() . ', ' .
 						$this->sqlRequestEndTime() . ', ' .
 						$this->sqlIsFailed() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_batch_type_id = ' . $this->sqlInsuranceBatchTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_batch_type_id = ' . $this->sqlInsuranceBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_batch_status_type_id = ' . $this->sqlInsuranceBatchStatusTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceBatchStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_batch_status_type_id = ' . $this->sqlInsuranceBatchStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; } elseif( true == array_key_exists( 'PolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_batch_reference = ' . $this->sqlCarrierBatchReference() . ','; } elseif( true == array_key_exists( 'CarrierBatchReference', $this->getChangedColumns() ) ) { $strSql .= ' carrier_batch_reference = ' . $this->sqlCarrierBatchReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate() . ','; } elseif( true == array_key_exists( 'BatchDate', $this->getChangedColumns() ) ) { $strSql .= ' batch_date = ' . $this->sqlBatchDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; } elseif( true == array_key_exists( 'ReconciledOn', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_on = ' . $this->sqlReconciledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; } elseif( true == array_key_exists( 'TotalPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount() . ','; } elseif( true == array_key_exists( 'TotalInvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_invoice_amount = ' . $this->sqlTotalInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_commission = ' . $this->sqlTotalCommission() . ','; } elseif( true == array_key_exists( 'TotalCommission', $this->getChangedColumns() ) ) { $strSql .= ' total_commission = ' . $this->sqlTotalCommission() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_sent = ' . $this->sqlDataSent() . ','; } elseif( true == array_key_exists( 'DataSent', $this->getChangedColumns() ) ) { $strSql .= ' data_sent = ' . $this->sqlDataSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_received = ' . $this->sqlDataReceived() . ','; } elseif( true == array_key_exists( 'DataReceived', $this->getChangedColumns() ) ) { $strSql .= ' data_received = ' . $this->sqlDataReceived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_start_time = ' . $this->sqlRequestStartTime() . ','; } elseif( true == array_key_exists( 'RequestStartTime', $this->getChangedColumns() ) ) { $strSql .= ' request_start_time = ' . $this->sqlRequestStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_end_time = ' . $this->sqlRequestEndTime() . ','; } elseif( true == array_key_exists( 'RequestEndTime', $this->getChangedColumns() ) ) { $strSql .= ' request_end_time = ' . $this->sqlRequestEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; } elseif( true == array_key_exists( 'IsFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_failed = ' . $this->sqlIsFailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'insurance_batch_type_id' => $this->getInsuranceBatchTypeId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_batch_status_type_id' => $this->getInsuranceBatchStatusTypeId(),
			'policy_number' => $this->getPolicyNumber(),
			'carrier_batch_reference' => $this->getCarrierBatchReference(),
			'file_name' => $this->getFileName(),
			'exported_on' => $this->getExportedOn(),
			'batch_date' => $this->getBatchDate(),
			'reconciled_on' => $this->getReconciledOn(),
			'total_premium_amount' => $this->getTotalPremiumAmount(),
			'total_invoice_amount' => $this->getTotalInvoiceAmount(),
			'total_commission' => $this->getTotalCommission(),
			'data_sent' => $this->getDataSent(),
			'data_received' => $this->getDataReceived(),
			'request_start_time' => $this->getRequestStartTime(),
			'request_end_time' => $this->getRequestEndTime(),
			'is_failed' => $this->getIsFailed(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>