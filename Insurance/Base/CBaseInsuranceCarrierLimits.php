<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierLimits
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierLimits extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierLimit[]
	 */
	public static function fetchInsuranceCarrierLimits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCarrierLimit::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierLimit
	 */
	public static function fetchInsuranceCarrierLimit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCarrierLimit::class, $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_limits', $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierLimit( sprintf( 'SELECT * FROM insurance_carrier_limits WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitsByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierLimits( sprintf( 'SELECT * FROM insurance_carrier_limits WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierLimits( sprintf( 'SELECT * FROM insurance_carrier_limits WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitsByInsuranceAmountTypeId( $intInsuranceAmountTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierLimits( sprintf( 'SELECT * FROM insurance_carrier_limits WHERE insurance_amount_type_id = %d', ( int ) $intInsuranceAmountTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierLimitsByInsuranceLimitTypeId( $intInsuranceLimitTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierLimits( sprintf( 'SELECT * FROM insurance_carrier_limits WHERE insurance_limit_type_id = %d', ( int ) $intInsuranceLimitTypeId ), $objDatabase );
	}

}
?>