<?php

class CBaseInsuranceCarrierProperty extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.insurance_carrier_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsuranceCarrierClassificationTypeId;
	protected $m_intCallPhoneNumberId;
	protected $m_intMinimumBillingFrequencyId;
	protected $m_intPropertyConstructionTypeId;
	protected $m_strStreetLine1;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strCountyCode;
	protected $m_strPostalCode;
	protected $m_strBcegCode;
	protected $m_strPpcCode;
	protected $m_strCountyName;
	protected $m_strName;
	protected $m_strSubDomain;
	protected $m_strLongitude;
	protected $m_strLatitude;
	protected $m_intUnitCount;
	protected $m_strConstructionYear;
	protected $m_strProfessionallyManagedYear;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intIsActive;
	protected $m_intIsGeneric;
	protected $m_intIsDisabled;
	protected $m_intIsProfessionallyManaged;
	protected $m_intIsGatedCommunity;
	protected $m_boolIsIncludeResidentSecure;
	protected $m_boolIsResidentSecureAutoEnabled;
	protected $m_boolIsUsingNewWebsite;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intWindstormConstructionCreditId;
	protected $m_intBuildingCodeGradeId;
	protected $m_intAutoSprinklerCreditId;

	public function __construct() {
		parent::__construct();

		$this->m_intMinimumBillingFrequencyId = '4';
		$this->m_strStreetLine1 = NULL;
		$this->m_strCity = NULL;
		$this->m_intIsActive = '1';
		$this->m_intIsGeneric = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsProfessionallyManaged = '0';
		$this->m_intIsGatedCommunity = '0';
		$this->m_boolIsIncludeResidentSecure = false;
		$this->m_boolIsResidentSecureAutoEnabled = false;
		$this->m_boolIsUsingNewWebsite = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_carrier_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierClassificationTypeId', trim( $arrValues['insurance_carrier_classification_type_id'] ) ); elseif( isset( $arrValues['insurance_carrier_classification_type_id'] ) ) $this->setInsuranceCarrierClassificationTypeId( $arrValues['insurance_carrier_classification_type_id'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['minimum_billing_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intMinimumBillingFrequencyId', trim( $arrValues['minimum_billing_frequency_id'] ) ); elseif( isset( $arrValues['minimum_billing_frequency_id'] ) ) $this->setMinimumBillingFrequencyId( $arrValues['minimum_billing_frequency_id'] );
		if( isset( $arrValues['property_construction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyConstructionTypeId', trim( $arrValues['property_construction_type_id'] ) ); elseif( isset( $arrValues['property_construction_type_id'] ) ) $this->setPropertyConstructionTypeId( $arrValues['property_construction_type_id'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['county_code'] ) && $boolDirectSet ) $this->set( 'm_strCountyCode', trim( stripcslashes( $arrValues['county_code'] ) ) ); elseif( isset( $arrValues['county_code'] ) ) $this->setCountyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county_code'] ) : $arrValues['county_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['bceg_code'] ) && $boolDirectSet ) $this->set( 'm_strBcegCode', trim( stripcslashes( $arrValues['bceg_code'] ) ) ); elseif( isset( $arrValues['bceg_code'] ) ) $this->setBcegCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bceg_code'] ) : $arrValues['bceg_code'] );
		if( isset( $arrValues['ppc_code'] ) && $boolDirectSet ) $this->set( 'm_strPpcCode', trim( stripcslashes( $arrValues['ppc_code'] ) ) ); elseif( isset( $arrValues['ppc_code'] ) ) $this->setPpcCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ppc_code'] ) : $arrValues['ppc_code'] );
		if( isset( $arrValues['county_name'] ) && $boolDirectSet ) $this->set( 'm_strCountyName', trim( stripcslashes( $arrValues['county_name'] ) ) ); elseif( isset( $arrValues['county_name'] ) ) $this->setCountyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county_name'] ) : $arrValues['county_name'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['sub_domain'] ) && $boolDirectSet ) $this->set( 'm_strSubDomain', trim( stripcslashes( $arrValues['sub_domain'] ) ) ); elseif( isset( $arrValues['sub_domain'] ) ) $this->setSubDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sub_domain'] ) : $arrValues['sub_domain'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( stripcslashes( $arrValues['longitude'] ) ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['longitude'] ) : $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( stripcslashes( $arrValues['latitude'] ) ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latitude'] ) : $arrValues['latitude'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['construction_year'] ) && $boolDirectSet ) $this->set( 'm_strConstructionYear', trim( stripcslashes( $arrValues['construction_year'] ) ) ); elseif( isset( $arrValues['construction_year'] ) ) $this->setConstructionYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['construction_year'] ) : $arrValues['construction_year'] );
		if( isset( $arrValues['professionally_managed_year'] ) && $boolDirectSet ) $this->set( 'm_strProfessionallyManagedYear', trim( stripcslashes( $arrValues['professionally_managed_year'] ) ) ); elseif( isset( $arrValues['professionally_managed_year'] ) ) $this->setProfessionallyManagedYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['professionally_managed_year'] ) : $arrValues['professionally_managed_year'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['is_generic'] ) && $boolDirectSet ) $this->set( 'm_intIsGeneric', trim( $arrValues['is_generic'] ) ); elseif( isset( $arrValues['is_generic'] ) ) $this->setIsGeneric( $arrValues['is_generic'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_professionally_managed'] ) && $boolDirectSet ) $this->set( 'm_intIsProfessionallyManaged', trim( $arrValues['is_professionally_managed'] ) ); elseif( isset( $arrValues['is_professionally_managed'] ) ) $this->setIsProfessionallyManaged( $arrValues['is_professionally_managed'] );
		if( isset( $arrValues['is_gated_community'] ) && $boolDirectSet ) $this->set( 'm_intIsGatedCommunity', trim( $arrValues['is_gated_community'] ) ); elseif( isset( $arrValues['is_gated_community'] ) ) $this->setIsGatedCommunity( $arrValues['is_gated_community'] );
		if( isset( $arrValues['is_include_resident_secure'] ) && $boolDirectSet ) $this->set( 'm_boolIsIncludeResidentSecure', trim( stripcslashes( $arrValues['is_include_resident_secure'] ) ) ); elseif( isset( $arrValues['is_include_resident_secure'] ) ) $this->setIsIncludeResidentSecure( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_include_resident_secure'] ) : $arrValues['is_include_resident_secure'] );
		if( isset( $arrValues['is_resident_secure_auto_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsResidentSecureAutoEnabled', trim( stripcslashes( $arrValues['is_resident_secure_auto_enabled'] ) ) ); elseif( isset( $arrValues['is_resident_secure_auto_enabled'] ) ) $this->setIsResidentSecureAutoEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_resident_secure_auto_enabled'] ) : $arrValues['is_resident_secure_auto_enabled'] );
		if( isset( $arrValues['is_using_new_website'] ) && $boolDirectSet ) $this->set( 'm_boolIsUsingNewWebsite', trim( stripcslashes( $arrValues['is_using_new_website'] ) ) ); elseif( isset( $arrValues['is_using_new_website'] ) ) $this->setIsUsingNewWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_using_new_website'] ) : $arrValues['is_using_new_website'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['windstorm_construction_credit_id'] ) && $boolDirectSet ) $this->set( 'm_intWindstormConstructionCreditId', trim( $arrValues['windstorm_construction_credit_id'] ) ); elseif( isset( $arrValues['windstorm_construction_credit_id'] ) ) $this->setWindstormConstructionCreditId( $arrValues['windstorm_construction_credit_id'] );
		if( isset( $arrValues['building_code_grade_id'] ) && $boolDirectSet ) $this->set( 'm_intBuildingCodeGradeId', trim( $arrValues['building_code_grade_id'] ) ); elseif( isset( $arrValues['building_code_grade_id'] ) ) $this->setBuildingCodeGradeId( $arrValues['building_code_grade_id'] );
		if( isset( $arrValues['auto_sprinkler_credit_id'] ) && $boolDirectSet ) $this->set( 'm_intAutoSprinklerCreditId', trim( $arrValues['auto_sprinkler_credit_id'] ) ); elseif( isset( $arrValues['auto_sprinkler_credit_id'] ) ) $this->setAutoSprinklerCreditId( $arrValues['auto_sprinkler_credit_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId ) {
		$this->set( 'm_intInsuranceCarrierClassificationTypeId', CStrings::strToIntDef( $intInsuranceCarrierClassificationTypeId, NULL, false ) );
	}

	public function getInsuranceCarrierClassificationTypeId() {
		return $this->m_intInsuranceCarrierClassificationTypeId;
	}

	public function sqlInsuranceCarrierClassificationTypeId() {
		return ( true == isset( $this->m_intInsuranceCarrierClassificationTypeId ) ) ? ( string ) $this->m_intInsuranceCarrierClassificationTypeId : 'NULL';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setMinimumBillingFrequencyId( $intMinimumBillingFrequencyId ) {
		$this->set( 'm_intMinimumBillingFrequencyId', CStrings::strToIntDef( $intMinimumBillingFrequencyId, NULL, false ) );
	}

	public function getMinimumBillingFrequencyId() {
		return $this->m_intMinimumBillingFrequencyId;
	}

	public function sqlMinimumBillingFrequencyId() {
		return ( true == isset( $this->m_intMinimumBillingFrequencyId ) ) ? ( string ) $this->m_intMinimumBillingFrequencyId : '4';
	}

	public function setPropertyConstructionTypeId( $intPropertyConstructionTypeId ) {
		$this->set( 'm_intPropertyConstructionTypeId', CStrings::strToIntDef( $intPropertyConstructionTypeId, NULL, false ) );
	}

	public function getPropertyConstructionTypeId() {
		return $this->m_intPropertyConstructionTypeId;
	}

	public function sqlPropertyConstructionTypeId() {
		return ( true == isset( $this->m_intPropertyConstructionTypeId ) ) ? ( string ) $this->m_intPropertyConstructionTypeId : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : '\'NULL\'';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : '\'NULL\'';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCountyCode( $strCountyCode ) {
		$this->set( 'm_strCountyCode', CStrings::strTrimDef( $strCountyCode, 50, NULL, true ) );
	}

	public function getCountyCode() {
		return $this->m_strCountyCode;
	}

	public function sqlCountyCode() {
		return ( true == isset( $this->m_strCountyCode ) ) ? '\'' . addslashes( $this->m_strCountyCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setBcegCode( $strBcegCode ) {
		$this->set( 'm_strBcegCode', CStrings::strTrimDef( $strBcegCode, 20, NULL, true ) );
	}

	public function getBcegCode() {
		return $this->m_strBcegCode;
	}

	public function sqlBcegCode() {
		return ( true == isset( $this->m_strBcegCode ) ) ? '\'' . addslashes( $this->m_strBcegCode ) . '\'' : 'NULL';
	}

	public function setPpcCode( $strPpcCode ) {
		$this->set( 'm_strPpcCode', CStrings::strTrimDef( $strPpcCode, 20, NULL, true ) );
	}

	public function getPpcCode() {
		return $this->m_strPpcCode;
	}

	public function sqlPpcCode() {
		return ( true == isset( $this->m_strPpcCode ) ) ? '\'' . addslashes( $this->m_strPpcCode ) . '\'' : 'NULL';
	}

	public function setCountyName( $strCountyName ) {
		$this->set( 'm_strCountyName', CStrings::strTrimDef( $strCountyName, 50, NULL, true ) );
	}

	public function getCountyName() {
		return $this->m_strCountyName;
	}

	public function sqlCountyName() {
		return ( true == isset( $this->m_strCountyName ) ) ? '\'' . addslashes( $this->m_strCountyName ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSubDomain( $strSubDomain ) {
		$this->set( 'm_strSubDomain', CStrings::strTrimDef( $strSubDomain, 240, NULL, true ) );
	}

	public function getSubDomain() {
		return $this->m_strSubDomain;
	}

	public function sqlSubDomain() {
		return ( true == isset( $this->m_strSubDomain ) ) ? '\'' . addslashes( $this->m_strSubDomain ) . '\'' : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 25, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? '\'' . addslashes( $this->m_strLongitude ) . '\'' : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 25, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? '\'' . addslashes( $this->m_strLatitude ) . '\'' : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : 'NULL';
	}

	public function setConstructionYear( $strConstructionYear ) {
		$this->set( 'm_strConstructionYear', CStrings::strTrimDef( $strConstructionYear, 50, NULL, true ) );
	}

	public function getConstructionYear() {
		return $this->m_strConstructionYear;
	}

	public function sqlConstructionYear() {
		return ( true == isset( $this->m_strConstructionYear ) ) ? '\'' . addslashes( $this->m_strConstructionYear ) . '\'' : 'NULL';
	}

	public function setProfessionallyManagedYear( $strProfessionallyManagedYear ) {
		$this->set( 'm_strProfessionallyManagedYear', CStrings::strTrimDef( $strProfessionallyManagedYear, 50, NULL, true ) );
	}

	public function getProfessionallyManagedYear() {
		return $this->m_strProfessionallyManagedYear;
	}

	public function sqlProfessionallyManagedYear() {
		return ( true == isset( $this->m_strProfessionallyManagedYear ) ) ? '\'' . addslashes( $this->m_strProfessionallyManagedYear ) . '\'' : 'NULL';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '1';
	}

	public function setIsGeneric( $intIsGeneric ) {
		$this->set( 'm_intIsGeneric', CStrings::strToIntDef( $intIsGeneric, NULL, false ) );
	}

	public function getIsGeneric() {
		return $this->m_intIsGeneric;
	}

	public function sqlIsGeneric() {
		return ( true == isset( $this->m_intIsGeneric ) ) ? ( string ) $this->m_intIsGeneric : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsProfessionallyManaged( $intIsProfessionallyManaged ) {
		$this->set( 'm_intIsProfessionallyManaged', CStrings::strToIntDef( $intIsProfessionallyManaged, NULL, false ) );
	}

	public function getIsProfessionallyManaged() {
		return $this->m_intIsProfessionallyManaged;
	}

	public function sqlIsProfessionallyManaged() {
		return ( true == isset( $this->m_intIsProfessionallyManaged ) ) ? ( string ) $this->m_intIsProfessionallyManaged : '0';
	}

	public function setIsGatedCommunity( $intIsGatedCommunity ) {
		$this->set( 'm_intIsGatedCommunity', CStrings::strToIntDef( $intIsGatedCommunity, NULL, false ) );
	}

	public function getIsGatedCommunity() {
		return $this->m_intIsGatedCommunity;
	}

	public function sqlIsGatedCommunity() {
		return ( true == isset( $this->m_intIsGatedCommunity ) ) ? ( string ) $this->m_intIsGatedCommunity : '0';
	}

	public function setIsIncludeResidentSecure( $boolIsIncludeResidentSecure ) {
		$this->set( 'm_boolIsIncludeResidentSecure', CStrings::strToBool( $boolIsIncludeResidentSecure ) );
	}

	public function getIsIncludeResidentSecure() {
		return $this->m_boolIsIncludeResidentSecure;
	}

	public function sqlIsIncludeResidentSecure() {
		return ( true == isset( $this->m_boolIsIncludeResidentSecure ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsIncludeResidentSecure ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResidentSecureAutoEnabled( $boolIsResidentSecureAutoEnabled ) {
		$this->set( 'm_boolIsResidentSecureAutoEnabled', CStrings::strToBool( $boolIsResidentSecureAutoEnabled ) );
	}

	public function getIsResidentSecureAutoEnabled() {
		return $this->m_boolIsResidentSecureAutoEnabled;
	}

	public function sqlIsResidentSecureAutoEnabled() {
		return ( true == isset( $this->m_boolIsResidentSecureAutoEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResidentSecureAutoEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsUsingNewWebsite( $boolIsUsingNewWebsite ) {
		$this->set( 'm_boolIsUsingNewWebsite', CStrings::strToBool( $boolIsUsingNewWebsite ) );
	}

	public function getIsUsingNewWebsite() {
		return $this->m_boolIsUsingNewWebsite;
	}

	public function sqlIsUsingNewWebsite() {
		return ( true == isset( $this->m_boolIsUsingNewWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUsingNewWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setWindstormConstructionCreditId( $intWindstormConstructionCreditId ) {
		$this->set( 'm_intWindstormConstructionCreditId', CStrings::strToIntDef( $intWindstormConstructionCreditId, NULL, false ) );
	}

	public function getWindstormConstructionCreditId() {
		return $this->m_intWindstormConstructionCreditId;
	}

	public function sqlWindstormConstructionCreditId() {
		return ( true == isset( $this->m_intWindstormConstructionCreditId ) ) ? ( string ) $this->m_intWindstormConstructionCreditId : 'NULL';
	}

	public function setBuildingCodeGradeId( $intBuildingCodeGradeId ) {
		$this->set( 'm_intBuildingCodeGradeId', CStrings::strToIntDef( $intBuildingCodeGradeId, NULL, false ) );
	}

	public function getBuildingCodeGradeId() {
		return $this->m_intBuildingCodeGradeId;
	}

	public function sqlBuildingCodeGradeId() {
		return ( true == isset( $this->m_intBuildingCodeGradeId ) ) ? ( string ) $this->m_intBuildingCodeGradeId : 'NULL';
	}

	public function setAutoSprinklerCreditId( $intAutoSprinklerCreditId ) {
		$this->set( 'm_intAutoSprinklerCreditId', CStrings::strToIntDef( $intAutoSprinklerCreditId, NULL, false ) );
	}

	public function getAutoSprinklerCreditId() {
		return $this->m_intAutoSprinklerCreditId;
	}

	public function sqlAutoSprinklerCreditId() {
		return ( true == isset( $this->m_intAutoSprinklerCreditId ) ) ? ( string ) $this->m_intAutoSprinklerCreditId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, insurance_carrier_id, insurance_carrier_classification_type_id, call_phone_number_id, minimum_billing_frequency_id, property_construction_type_id, street_line1, city, state_code, county_code, postal_code, bceg_code, ppc_code, county_name, name, sub_domain, longitude, latitude, unit_count, construction_year, professionally_managed_year, details, is_active, is_generic, is_disabled, is_professionally_managed, is_gated_community, is_include_resident_secure, is_resident_secure_auto_enabled, is_using_new_website, updated_by, updated_on, created_by, created_on, windstorm_construction_credit_id, building_code_grade_id, auto_sprinkler_credit_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlInsuranceCarrierId() . ', ' .
						$this->sqlInsuranceCarrierClassificationTypeId() . ', ' .
						$this->sqlCallPhoneNumberId() . ', ' .
						$this->sqlMinimumBillingFrequencyId() . ', ' .
						$this->sqlPropertyConstructionTypeId() . ', ' .
						$this->sqlStreetLine1() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlCountyCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlBcegCode() . ', ' .
						$this->sqlPpcCode() . ', ' .
						$this->sqlCountyName() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlSubDomain() . ', ' .
						$this->sqlLongitude() . ', ' .
						$this->sqlLatitude() . ', ' .
						$this->sqlUnitCount() . ', ' .
						$this->sqlConstructionYear() . ', ' .
						$this->sqlProfessionallyManagedYear() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlIsGeneric() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsProfessionallyManaged() . ', ' .
						$this->sqlIsGatedCommunity() . ', ' .
						$this->sqlIsIncludeResidentSecure() . ', ' .
						$this->sqlIsResidentSecureAutoEnabled() . ', ' .
						$this->sqlIsUsingNewWebsite() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlWindstormConstructionCreditId() . ', ' .
						$this->sqlBuildingCodeGradeId() . ', ' .
						$this->sqlAutoSprinklerCreditId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_billing_frequency_id = ' . $this->sqlMinimumBillingFrequencyId(). ',' ; } elseif( true == array_key_exists( 'MinimumBillingFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' minimum_billing_frequency_id = ' . $this->sqlMinimumBillingFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_construction_type_id = ' . $this->sqlPropertyConstructionTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyConstructionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_construction_type_id = ' . $this->sqlPropertyConstructionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county_code = ' . $this->sqlCountyCode(). ',' ; } elseif( true == array_key_exists( 'CountyCode', $this->getChangedColumns() ) ) { $strSql .= ' county_code = ' . $this->sqlCountyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bceg_code = ' . $this->sqlBcegCode(). ',' ; } elseif( true == array_key_exists( 'BcegCode', $this->getChangedColumns() ) ) { $strSql .= ' bceg_code = ' . $this->sqlBcegCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ppc_code = ' . $this->sqlPpcCode(). ',' ; } elseif( true == array_key_exists( 'PpcCode', $this->getChangedColumns() ) ) { $strSql .= ' ppc_code = ' . $this->sqlPpcCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county_name = ' . $this->sqlCountyName(). ',' ; } elseif( true == array_key_exists( 'CountyName', $this->getChangedColumns() ) ) { $strSql .= ' county_name = ' . $this->sqlCountyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain(). ',' ; } elseif( true == array_key_exists( 'SubDomain', $this->getChangedColumns() ) ) { $strSql .= ' sub_domain = ' . $this->sqlSubDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude(). ',' ; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude(). ',' ; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' construction_year = ' . $this->sqlConstructionYear(). ',' ; } elseif( true == array_key_exists( 'ConstructionYear', $this->getChangedColumns() ) ) { $strSql .= ' construction_year = ' . $this->sqlConstructionYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' professionally_managed_year = ' . $this->sqlProfessionallyManagedYear(). ',' ; } elseif( true == array_key_exists( 'ProfessionallyManagedYear', $this->getChangedColumns() ) ) { $strSql .= ' professionally_managed_year = ' . $this->sqlProfessionallyManagedYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric(). ',' ; } elseif( true == array_key_exists( 'IsGeneric', $this->getChangedColumns() ) ) { $strSql .= ' is_generic = ' . $this->sqlIsGeneric() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_professionally_managed = ' . $this->sqlIsProfessionallyManaged(). ',' ; } elseif( true == array_key_exists( 'IsProfessionallyManaged', $this->getChangedColumns() ) ) { $strSql .= ' is_professionally_managed = ' . $this->sqlIsProfessionallyManaged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gated_community = ' . $this->sqlIsGatedCommunity(). ',' ; } elseif( true == array_key_exists( 'IsGatedCommunity', $this->getChangedColumns() ) ) { $strSql .= ' is_gated_community = ' . $this->sqlIsGatedCommunity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_include_resident_secure = ' . $this->sqlIsIncludeResidentSecure(). ',' ; } elseif( true == array_key_exists( 'IsIncludeResidentSecure', $this->getChangedColumns() ) ) { $strSql .= ' is_include_resident_secure = ' . $this->sqlIsIncludeResidentSecure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_secure_auto_enabled = ' . $this->sqlIsResidentSecureAutoEnabled(). ',' ; } elseif( true == array_key_exists( 'IsResidentSecureAutoEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_secure_auto_enabled = ' . $this->sqlIsResidentSecureAutoEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_using_new_website = ' . $this->sqlIsUsingNewWebsite(). ',' ; } elseif( true == array_key_exists( 'IsUsingNewWebsite', $this->getChangedColumns() ) ) { $strSql .= ' is_using_new_website = ' . $this->sqlIsUsingNewWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' windstorm_construction_credit_id = ' . $this->sqlWindstormConstructionCreditId(). ',' ; } elseif( true == array_key_exists( 'WindstormConstructionCreditId', $this->getChangedColumns() ) ) { $strSql .= ' windstorm_construction_credit_id = ' . $this->sqlWindstormConstructionCreditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_code_grade_id = ' . $this->sqlBuildingCodeGradeId(). ',' ; } elseif( true == array_key_exists( 'BuildingCodeGradeId', $this->getChangedColumns() ) ) { $strSql .= ' building_code_grade_id = ' . $this->sqlBuildingCodeGradeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_sprinkler_credit_id = ' . $this->sqlAutoSprinklerCreditId(). ',' ; } elseif( true == array_key_exists( 'AutoSprinklerCreditId', $this->getChangedColumns() ) ) { $strSql .= ' auto_sprinkler_credit_id = ' . $this->sqlAutoSprinklerCreditId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_carrier_classification_type_id' => $this->getInsuranceCarrierClassificationTypeId(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'minimum_billing_frequency_id' => $this->getMinimumBillingFrequencyId(),
			'property_construction_type_id' => $this->getPropertyConstructionTypeId(),
			'street_line1' => $this->getStreetLine1(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'county_code' => $this->getCountyCode(),
			'postal_code' => $this->getPostalCode(),
			'bceg_code' => $this->getBcegCode(),
			'ppc_code' => $this->getPpcCode(),
			'county_name' => $this->getCountyName(),
			'name' => $this->getName(),
			'sub_domain' => $this->getSubDomain(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'unit_count' => $this->getUnitCount(),
			'construction_year' => $this->getConstructionYear(),
			'professionally_managed_year' => $this->getProfessionallyManagedYear(),
			'details' => $this->getDetails(),
			'is_active' => $this->getIsActive(),
			'is_generic' => $this->getIsGeneric(),
			'is_disabled' => $this->getIsDisabled(),
			'is_professionally_managed' => $this->getIsProfessionallyManaged(),
			'is_gated_community' => $this->getIsGatedCommunity(),
			'is_include_resident_secure' => $this->getIsIncludeResidentSecure(),
			'is_resident_secure_auto_enabled' => $this->getIsResidentSecureAutoEnabled(),
			'is_using_new_website' => $this->getIsUsingNewWebsite(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'windstorm_construction_credit_id' => $this->getWindstormConstructionCreditId(),
			'building_code_grade_id' => $this->getBuildingCodeGradeId(),
			'auto_sprinkler_credit_id' => $this->getAutoSprinklerCreditId()
		);
	}

}
?>