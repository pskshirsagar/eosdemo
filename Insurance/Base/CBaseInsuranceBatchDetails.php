<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceBatchDetails
 * Do not add any new functions to this class.
 */

class CBaseInsuranceBatchDetails extends CEosPluralBase {

	/**
	 * @return CInsuranceBatchDetail[]
	 */
	public static function fetchInsuranceBatchDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceBatchDetail', $objDatabase );
	}

	/**
	 * @return CInsuranceBatchDetail
	 */
	public static function fetchInsuranceBatchDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceBatchDetail', $objDatabase );
	}

	public static function fetchInsuranceBatchDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_batch_details', $objDatabase );
	}

	public static function fetchInsuranceBatchDetailById( $intId, $objDatabase ) {
		return self::fetchInsuranceBatchDetail( sprintf( 'SELECT * FROM insurance_batch_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByInsuranceBatchId( $intInsuranceBatchId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE insurance_batch_id = %d', ( int ) $intInsuranceBatchId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByPropertyAccountId( $intPropertyAccountId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE property_account_id = %d', ( int ) $intPropertyAccountId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE transaction_id = %d', ( int ) $intTransactionId ), $objDatabase );
	}

	public static function fetchInsuranceBatchDetailsByReconciledTypeId( $intReconciledTypeId, $objDatabase ) {
		return self::fetchInsuranceBatchDetails( sprintf( 'SELECT * FROM insurance_batch_details WHERE reconciled_type_id = %d', ( int ) $intReconciledTypeId ), $objDatabase );
	}

}
?>