<?php

class CBaseInsuranceInvoiceDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_invoice_details';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_intInsuranceChargeId;
	protected $m_strPolicyNumber;
	protected $m_strInsuredName;
	protected $m_strStartDate;
	protected $m_strAccountingDate;
	protected $m_fltInvoiceAmount;
	protected $m_fltCommissionAmount;
	protected $m_strMemo;
	protected $m_intInsuranceInvoiceId;
	protected $m_intInsuranceInvoiceDetailResultTypeId;
	protected $m_intInitialInsuranceInvoiceDetailResultTypeId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['insurance_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceChargeId', trim( $arrValues['insurance_charge_id'] ) ); elseif( isset( $arrValues['insurance_charge_id'] ) ) $this->setInsuranceChargeId( $arrValues['insurance_charge_id'] );
		if( isset( $arrValues['policy_number'] ) && $boolDirectSet ) $this->set( 'm_strPolicyNumber', trim( stripcslashes( $arrValues['policy_number'] ) ) ); elseif( isset( $arrValues['policy_number'] ) ) $this->setPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['policy_number'] ) : $arrValues['policy_number'] );
		if( isset( $arrValues['insured_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuredName', trim( stripcslashes( $arrValues['insured_name'] ) ) ); elseif( isset( $arrValues['insured_name'] ) ) $this->setInsuredName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insured_name'] ) : $arrValues['insured_name'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['accounting_date'] ) && $boolDirectSet ) $this->set( 'm_strAccountingDate', trim( $arrValues['accounting_date'] ) ); elseif( isset( $arrValues['accounting_date'] ) ) $this->setAccountingDate( $arrValues['accounting_date'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['insurance_invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceInvoiceId', trim( $arrValues['insurance_invoice_id'] ) ); elseif( isset( $arrValues['insurance_invoice_id'] ) ) $this->setInsuranceInvoiceId( $arrValues['insurance_invoice_id'] );
		if( isset( $arrValues['insurance_invoice_detail_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceInvoiceDetailResultTypeId', trim( $arrValues['insurance_invoice_detail_result_type_id'] ) ); elseif( isset( $arrValues['insurance_invoice_detail_result_type_id'] ) ) $this->setInsuranceInvoiceDetailResultTypeId( $arrValues['insurance_invoice_detail_result_type_id'] );
		if( isset( $arrValues['initial_insurance_invoice_detail_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInitialInsuranceInvoiceDetailResultTypeId', trim( $arrValues['initial_insurance_invoice_detail_result_type_id'] ) ); elseif( isset( $arrValues['initial_insurance_invoice_detail_result_type_id'] ) ) $this->setInitialInsuranceInvoiceDetailResultTypeId( $arrValues['initial_insurance_invoice_detail_result_type_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setInsuranceChargeId( $intInsuranceChargeId ) {
		$this->set( 'm_intInsuranceChargeId', CStrings::strToIntDef( $intInsuranceChargeId, NULL, false ) );
	}

	public function getInsuranceChargeId() {
		return $this->m_intInsuranceChargeId;
	}

	public function sqlInsuranceChargeId() {
		return ( true == isset( $this->m_intInsuranceChargeId ) ) ? ( string ) $this->m_intInsuranceChargeId : 'NULL';
	}

	public function setPolicyNumber( $strPolicyNumber ) {
		$this->set( 'm_strPolicyNumber', CStrings::strTrimDef( $strPolicyNumber, 64, NULL, true ) );
	}

	public function getPolicyNumber() {
		return $this->m_strPolicyNumber;
	}

	public function sqlPolicyNumber() {
		return ( true == isset( $this->m_strPolicyNumber ) ) ? '\'' . addslashes( $this->m_strPolicyNumber ) . '\'' : 'NULL';
	}

	public function setInsuredName( $strInsuredName ) {
		$this->set( 'm_strInsuredName', CStrings::strTrimDef( $strInsuredName, 200, NULL, true ) );
	}

	public function getInsuredName() {
		return $this->m_strInsuredName;
	}

	public function sqlInsuredName() {
		return ( true == isset( $this->m_strInsuredName ) ) ? '\'' . addslashes( $this->m_strInsuredName ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setAccountingDate( $strAccountingDate ) {
		$this->set( 'm_strAccountingDate', CStrings::strTrimDef( $strAccountingDate, -1, NULL, true ) );
	}

	public function getAccountingDate() {
		return $this->m_strAccountingDate;
	}

	public function sqlAccountingDate() {
		return ( true == isset( $this->m_strAccountingDate ) ) ? '\'' . $this->m_strAccountingDate . '\'' : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 2 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : 'NULL';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 2 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, -1, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setInsuranceInvoiceId( $intInsuranceInvoiceId ) {
		$this->set( 'm_intInsuranceInvoiceId', CStrings::strToIntDef( $intInsuranceInvoiceId, NULL, false ) );
	}

	public function getInsuranceInvoiceId() {
		return $this->m_intInsuranceInvoiceId;
	}

	public function sqlInsuranceInvoiceId() {
		return ( true == isset( $this->m_intInsuranceInvoiceId ) ) ? ( string ) $this->m_intInsuranceInvoiceId : 'NULL';
	}

	public function setInsuranceInvoiceDetailResultTypeId( $intInsuranceInvoiceDetailResultTypeId ) {
		$this->set( 'm_intInsuranceInvoiceDetailResultTypeId', CStrings::strToIntDef( $intInsuranceInvoiceDetailResultTypeId, NULL, false ) );
	}

	public function getInsuranceInvoiceDetailResultTypeId() {
		return $this->m_intInsuranceInvoiceDetailResultTypeId;
	}

	public function sqlInsuranceInvoiceDetailResultTypeId() {
		return ( true == isset( $this->m_intInsuranceInvoiceDetailResultTypeId ) ) ? ( string ) $this->m_intInsuranceInvoiceDetailResultTypeId : 'NULL';
	}

	public function setInitialInsuranceInvoiceDetailResultTypeId( $intInitialInsuranceInvoiceDetailResultTypeId ) {
		$this->set( 'm_intInitialInsuranceInvoiceDetailResultTypeId', CStrings::strToIntDef( $intInitialInsuranceInvoiceDetailResultTypeId, NULL, false ) );
	}

	public function getInitialInsuranceInvoiceDetailResultTypeId() {
		return $this->m_intInitialInsuranceInvoiceDetailResultTypeId;
	}

	public function sqlInitialInsuranceInvoiceDetailResultTypeId() {
		return ( true == isset( $this->m_intInitialInsuranceInvoiceDetailResultTypeId ) ) ? ( string ) $this->m_intInitialInsuranceInvoiceDetailResultTypeId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, insurance_charge_id, policy_number, insured_name, start_date, accounting_date, invoice_amount, commission_amount, memo, insurance_invoice_id, insurance_invoice_detail_result_type_id, initial_insurance_invoice_detail_result_type_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlInsuranceChargeId() . ', ' .
 						$this->sqlPolicyNumber() . ', ' .
 						$this->sqlInsuredName() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlAccountingDate() . ', ' .
 						$this->sqlInvoiceAmount() . ', ' .
 						$this->sqlCommissionAmount() . ', ' .
 						$this->sqlMemo() . ', ' .
 						$this->sqlInsuranceInvoiceId() . ', ' .
 						$this->sqlInsuranceInvoiceDetailResultTypeId() . ', ' .
 						$this->sqlInitialInsuranceInvoiceDetailResultTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_charge_id = ' . $this->sqlInsuranceChargeId() . ','; } elseif( true == array_key_exists( 'InsuranceChargeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_charge_id = ' . $this->sqlInsuranceChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; } elseif( true == array_key_exists( 'PolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; } elseif( true == array_key_exists( 'InsuredName', $this->getChangedColumns() ) ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_date = ' . $this->sqlAccountingDate() . ','; } elseif( true == array_key_exists( 'AccountingDate', $this->getChangedColumns() ) ) { $strSql .= ' accounting_date = ' . $this->sqlAccountingDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_invoice_id = ' . $this->sqlInsuranceInvoiceId() . ','; } elseif( true == array_key_exists( 'InsuranceInvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_invoice_id = ' . $this->sqlInsuranceInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_invoice_detail_result_type_id = ' . $this->sqlInsuranceInvoiceDetailResultTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceInvoiceDetailResultTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_invoice_detail_result_type_id = ' . $this->sqlInsuranceInvoiceDetailResultTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_insurance_invoice_detail_result_type_id = ' . $this->sqlInitialInsuranceInvoiceDetailResultTypeId() . ','; } elseif( true == array_key_exists( 'InitialInsuranceInvoiceDetailResultTypeId', $this->getChangedColumns() ) ) { $strSql .= ' initial_insurance_invoice_detail_result_type_id = ' . $this->sqlInitialInsuranceInvoiceDetailResultTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'insurance_charge_id' => $this->getInsuranceChargeId(),
			'policy_number' => $this->getPolicyNumber(),
			'insured_name' => $this->getInsuredName(),
			'start_date' => $this->getStartDate(),
			'accounting_date' => $this->getAccountingDate(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'commission_amount' => $this->getCommissionAmount(),
			'memo' => $this->getMemo(),
			'insurance_invoice_id' => $this->getInsuranceInvoiceId(),
			'insurance_invoice_detail_result_type_id' => $this->getInsuranceInvoiceDetailResultTypeId(),
			'initial_insurance_invoice_detail_result_type_id' => $this->getInitialInsuranceInvoiceDetailResultTypeId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>