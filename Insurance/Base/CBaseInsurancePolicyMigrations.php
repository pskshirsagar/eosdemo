<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyMigrations
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyMigrations extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyMigration[]
	 */
	public static function fetchInsurancePolicyMigrations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyMigration::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyMigration
	 */
	public static function fetchInsurancePolicyMigration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyMigration::class, $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_migrations', $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyMigration( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationsBySourceCid( $intSourceCid, $objDatabase ) {
		return self::fetchInsurancePolicyMigrations( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE source_cid = %d', $intSourceCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationsByDestinationCid( $intDestinationCid, $objDatabase ) {
		return self::fetchInsurancePolicyMigrations( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE destination_cid = %d', $intDestinationCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationsBySourcePropertyId( $intSourcePropertyId, $objDatabase ) {
		return self::fetchInsurancePolicyMigrations( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE source_property_id = %d', $intSourcePropertyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationsByDestinationPropertyId( $intDestinationPropertyId, $objDatabase ) {
		return self::fetchInsurancePolicyMigrations( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE destination_property_id = %d', $intDestinationPropertyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyMigrationsByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchInsurancePolicyMigrations( sprintf( 'SELECT * FROM insurance_policy_migrations WHERE entity_id = %d', $intEntityId ), $objDatabase );
	}

}
?>