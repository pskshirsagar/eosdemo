<?php

class CBaseInsuranceCustomPolicyExport extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_custom_policy_exports';

	protected $m_intId;
	protected $m_intInsuranceCustomPolicyId;
	protected $m_intInsuranceExportBatchId;
	protected $m_intInsuranceBatchId;
	protected $m_intInsuranceExportTypeId;
	protected $m_intInsuranceCustomPolicyExportId;
	protected $m_fltProviderPremiumAmount;
	protected $m_fltTotalPremiumAmount;
	protected $m_fltAdminFee;
	protected $m_fltBaseRate;
	protected $m_fltPolicyFee;
	protected $m_fltBaseTax;
	protected $m_fltStampingTax;
	protected $m_fltOtherTax;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_boolIsDisabled;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intInsuranceExportTypeId = '1';
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_custom_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCustomPolicyId', trim( $arrValues['insurance_custom_policy_id'] ) ); elseif( isset( $arrValues['insurance_custom_policy_id'] ) ) $this->setInsuranceCustomPolicyId( $arrValues['insurance_custom_policy_id'] );
		if( isset( $arrValues['insurance_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceExportBatchId', trim( $arrValues['insurance_export_batch_id'] ) ); elseif( isset( $arrValues['insurance_export_batch_id'] ) ) $this->setInsuranceExportBatchId( $arrValues['insurance_export_batch_id'] );
		if( isset( $arrValues['insurance_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceBatchId', trim( $arrValues['insurance_batch_id'] ) ); elseif( isset( $arrValues['insurance_batch_id'] ) ) $this->setInsuranceBatchId( $arrValues['insurance_batch_id'] );
		if( isset( $arrValues['insurance_export_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceExportTypeId', trim( $arrValues['insurance_export_type_id'] ) ); elseif( isset( $arrValues['insurance_export_type_id'] ) ) $this->setInsuranceExportTypeId( $arrValues['insurance_export_type_id'] );
		if( isset( $arrValues['insurance_custom_policy_export_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCustomPolicyExportId', trim( $arrValues['insurance_custom_policy_export_id'] ) ); elseif( isset( $arrValues['insurance_custom_policy_export_id'] ) ) $this->setInsuranceCustomPolicyExportId( $arrValues['insurance_custom_policy_export_id'] );
		if( isset( $arrValues['provider_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltProviderPremiumAmount', trim( $arrValues['provider_premium_amount'] ) ); elseif( isset( $arrValues['provider_premium_amount'] ) ) $this->setProviderPremiumAmount( $arrValues['provider_premium_amount'] );
		if( isset( $arrValues['total_premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalPremiumAmount', trim( $arrValues['total_premium_amount'] ) ); elseif( isset( $arrValues['total_premium_amount'] ) ) $this->setTotalPremiumAmount( $arrValues['total_premium_amount'] );
		if( isset( $arrValues['admin_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAdminFee', trim( $arrValues['admin_fee'] ) ); elseif( isset( $arrValues['admin_fee'] ) ) $this->setAdminFee( $arrValues['admin_fee'] );
		if( isset( $arrValues['base_rate'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRate', trim( $arrValues['base_rate'] ) ); elseif( isset( $arrValues['base_rate'] ) ) $this->setBaseRate( $arrValues['base_rate'] );
		if( isset( $arrValues['policy_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPolicyFee', trim( $arrValues['policy_fee'] ) ); elseif( isset( $arrValues['policy_fee'] ) ) $this->setPolicyFee( $arrValues['policy_fee'] );
		if( isset( $arrValues['base_tax'] ) && $boolDirectSet ) $this->set( 'm_fltBaseTax', trim( $arrValues['base_tax'] ) ); elseif( isset( $arrValues['base_tax'] ) ) $this->setBaseTax( $arrValues['base_tax'] );
		if( isset( $arrValues['stamping_tax'] ) && $boolDirectSet ) $this->set( 'm_fltStampingTax', trim( $arrValues['stamping_tax'] ) ); elseif( isset( $arrValues['stamping_tax'] ) ) $this->setStampingTax( $arrValues['stamping_tax'] );
		if( isset( $arrValues['other_tax'] ) && $boolDirectSet ) $this->set( 'm_fltOtherTax', trim( $arrValues['other_tax'] ) ); elseif( isset( $arrValues['other_tax'] ) ) $this->setOtherTax( $arrValues['other_tax'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCustomPolicyId( $intInsuranceCustomPolicyId ) {
		$this->set( 'm_intInsuranceCustomPolicyId', CStrings::strToIntDef( $intInsuranceCustomPolicyId, NULL, false ) );
	}

	public function getInsuranceCustomPolicyId() {
		return $this->m_intInsuranceCustomPolicyId;
	}

	public function sqlInsuranceCustomPolicyId() {
		return ( true == isset( $this->m_intInsuranceCustomPolicyId ) ) ? ( string ) $this->m_intInsuranceCustomPolicyId : 'NULL';
	}

	public function setInsuranceExportBatchId( $intInsuranceExportBatchId ) {
		$this->set( 'm_intInsuranceExportBatchId', CStrings::strToIntDef( $intInsuranceExportBatchId, NULL, false ) );
	}

	public function getInsuranceExportBatchId() {
		return $this->m_intInsuranceExportBatchId;
	}

	public function sqlInsuranceExportBatchId() {
		return ( true == isset( $this->m_intInsuranceExportBatchId ) ) ? ( string ) $this->m_intInsuranceExportBatchId : 'NULL';
	}

	public function setInsuranceBatchId( $intInsuranceBatchId ) {
		$this->set( 'm_intInsuranceBatchId', CStrings::strToIntDef( $intInsuranceBatchId, NULL, false ) );
	}

	public function getInsuranceBatchId() {
		return $this->m_intInsuranceBatchId;
	}

	public function sqlInsuranceBatchId() {
		return ( true == isset( $this->m_intInsuranceBatchId ) ) ? ( string ) $this->m_intInsuranceBatchId : 'NULL';
	}

	public function setInsuranceExportTypeId( $intInsuranceExportTypeId ) {
		$this->set( 'm_intInsuranceExportTypeId', CStrings::strToIntDef( $intInsuranceExportTypeId, NULL, false ) );
	}

	public function getInsuranceExportTypeId() {
		return $this->m_intInsuranceExportTypeId;
	}

	public function sqlInsuranceExportTypeId() {
		return ( true == isset( $this->m_intInsuranceExportTypeId ) ) ? ( string ) $this->m_intInsuranceExportTypeId : '1';
	}

	public function setInsuranceCustomPolicyExportId( $intInsuranceCustomPolicyExportId ) {
		$this->set( 'm_intInsuranceCustomPolicyExportId', CStrings::strToIntDef( $intInsuranceCustomPolicyExportId, NULL, false ) );
	}

	public function getInsuranceCustomPolicyExportId() {
		return $this->m_intInsuranceCustomPolicyExportId;
	}

	public function sqlInsuranceCustomPolicyExportId() {
		return ( true == isset( $this->m_intInsuranceCustomPolicyExportId ) ) ? ( string ) $this->m_intInsuranceCustomPolicyExportId : 'NULL';
	}

	public function setProviderPremiumAmount( $fltProviderPremiumAmount ) {
		$this->set( 'm_fltProviderPremiumAmount', CStrings::strToFloatDef( $fltProviderPremiumAmount, NULL, false, 2 ) );
	}

	public function getProviderPremiumAmount() {
		return $this->m_fltProviderPremiumAmount;
	}

	public function sqlProviderPremiumAmount() {
		return ( true == isset( $this->m_fltProviderPremiumAmount ) ) ? ( string ) $this->m_fltProviderPremiumAmount : 'NULL';
	}

	public function setTotalPremiumAmount( $fltTotalPremiumAmount ) {
		$this->set( 'm_fltTotalPremiumAmount', CStrings::strToFloatDef( $fltTotalPremiumAmount, NULL, false, 2 ) );
	}

	public function getTotalPremiumAmount() {
		return $this->m_fltTotalPremiumAmount;
	}

	public function sqlTotalPremiumAmount() {
		return ( true == isset( $this->m_fltTotalPremiumAmount ) ) ? ( string ) $this->m_fltTotalPremiumAmount : 'NULL';
	}

	public function setAdminFee( $fltAdminFee ) {
		$this->set( 'm_fltAdminFee', CStrings::strToFloatDef( $fltAdminFee, NULL, false, 2 ) );
	}

	public function getAdminFee() {
		return $this->m_fltAdminFee;
	}

	public function sqlAdminFee() {
		return ( true == isset( $this->m_fltAdminFee ) ) ? ( string ) $this->m_fltAdminFee : 'NULL';
	}

	public function setBaseRate( $fltBaseRate ) {
		$this->set( 'm_fltBaseRate', CStrings::strToFloatDef( $fltBaseRate, NULL, false, 2 ) );
	}

	public function getBaseRate() {
		return $this->m_fltBaseRate;
	}

	public function sqlBaseRate() {
		return ( true == isset( $this->m_fltBaseRate ) ) ? ( string ) $this->m_fltBaseRate : 'NULL';
	}

	public function setPolicyFee( $fltPolicyFee ) {
		$this->set( 'm_fltPolicyFee', CStrings::strToFloatDef( $fltPolicyFee, NULL, false, 2 ) );
	}

	public function getPolicyFee() {
		return $this->m_fltPolicyFee;
	}

	public function sqlPolicyFee() {
		return ( true == isset( $this->m_fltPolicyFee ) ) ? ( string ) $this->m_fltPolicyFee : 'NULL';
	}

	public function setBaseTax( $fltBaseTax ) {
		$this->set( 'm_fltBaseTax', CStrings::strToFloatDef( $fltBaseTax, NULL, false, 2 ) );
	}

	public function getBaseTax() {
		return $this->m_fltBaseTax;
	}

	public function sqlBaseTax() {
		return ( true == isset( $this->m_fltBaseTax ) ) ? ( string ) $this->m_fltBaseTax : 'NULL';
	}

	public function setStampingTax( $fltStampingTax ) {
		$this->set( 'm_fltStampingTax', CStrings::strToFloatDef( $fltStampingTax, NULL, false, 2 ) );
	}

	public function getStampingTax() {
		return $this->m_fltStampingTax;
	}

	public function sqlStampingTax() {
		return ( true == isset( $this->m_fltStampingTax ) ) ? ( string ) $this->m_fltStampingTax : 'NULL';
	}

	public function setOtherTax( $fltOtherTax ) {
		$this->set( 'm_fltOtherTax', CStrings::strToFloatDef( $fltOtherTax, NULL, false, 2 ) );
	}

	public function getOtherTax() {
		return $this->m_fltOtherTax;
	}

	public function sqlOtherTax() {
		return ( true == isset( $this->m_fltOtherTax ) ) ? ( string ) $this->m_fltOtherTax : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_custom_policy_id, insurance_export_batch_id, insurance_batch_id, insurance_export_type_id, insurance_custom_policy_export_id, provider_premium_amount, total_premium_amount, admin_fee, base_rate, policy_fee, base_tax, stamping_tax, other_tax, start_date, end_date, is_disabled, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsuranceCustomPolicyId() . ', ' .
 						$this->sqlInsuranceExportBatchId() . ', ' .
 						$this->sqlInsuranceBatchId() . ', ' .
 						$this->sqlInsuranceExportTypeId() . ', ' .
 						$this->sqlInsuranceCustomPolicyExportId() . ', ' .
 						$this->sqlProviderPremiumAmount() . ', ' .
 						$this->sqlTotalPremiumAmount() . ', ' .
 						$this->sqlAdminFee() . ', ' .
 						$this->sqlBaseRate() . ', ' .
 						$this->sqlPolicyFee() . ', ' .
 						$this->sqlBaseTax() . ', ' .
 						$this->sqlStampingTax() . ', ' .
 						$this->sqlOtherTax() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_custom_policy_id = ' . $this->sqlInsuranceCustomPolicyId() . ','; } elseif( true == array_key_exists( 'InsuranceCustomPolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_custom_policy_id = ' . $this->sqlInsuranceCustomPolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_export_batch_id = ' . $this->sqlInsuranceExportBatchId() . ','; } elseif( true == array_key_exists( 'InsuranceExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_export_batch_id = ' . $this->sqlInsuranceExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_batch_id = ' . $this->sqlInsuranceBatchId() . ','; } elseif( true == array_key_exists( 'InsuranceBatchId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_batch_id = ' . $this->sqlInsuranceBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_export_type_id = ' . $this->sqlInsuranceExportTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceExportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_export_type_id = ' . $this->sqlInsuranceExportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_custom_policy_export_id = ' . $this->sqlInsuranceCustomPolicyExportId() . ','; } elseif( true == array_key_exists( 'InsuranceCustomPolicyExportId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_custom_policy_export_id = ' . $this->sqlInsuranceCustomPolicyExportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider_premium_amount = ' . $this->sqlProviderPremiumAmount() . ','; } elseif( true == array_key_exists( 'ProviderPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' provider_premium_amount = ' . $this->sqlProviderPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; } elseif( true == array_key_exists( 'TotalPremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' total_premium_amount = ' . $this->sqlTotalPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee() . ','; } elseif( true == array_key_exists( 'AdminFee', $this->getChangedColumns() ) ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; } elseif( true == array_key_exists( 'BaseRate', $this->getChangedColumns() ) ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_fee = ' . $this->sqlPolicyFee() . ','; } elseif( true == array_key_exists( 'PolicyFee', $this->getChangedColumns() ) ) { $strSql .= ' policy_fee = ' . $this->sqlPolicyFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax() . ','; } elseif( true == array_key_exists( 'BaseTax', $this->getChangedColumns() ) ) { $strSql .= ' base_tax = ' . $this->sqlBaseTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stamping_tax = ' . $this->sqlStampingTax() . ','; } elseif( true == array_key_exists( 'StampingTax', $this->getChangedColumns() ) ) { $strSql .= ' stamping_tax = ' . $this->sqlStampingTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_tax = ' . $this->sqlOtherTax() . ','; } elseif( true == array_key_exists( 'OtherTax', $this->getChangedColumns() ) ) { $strSql .= ' other_tax = ' . $this->sqlOtherTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_custom_policy_id' => $this->getInsuranceCustomPolicyId(),
			'insurance_export_batch_id' => $this->getInsuranceExportBatchId(),
			'insurance_batch_id' => $this->getInsuranceBatchId(),
			'insurance_export_type_id' => $this->getInsuranceExportTypeId(),
			'insurance_custom_policy_export_id' => $this->getInsuranceCustomPolicyExportId(),
			'provider_premium_amount' => $this->getProviderPremiumAmount(),
			'total_premium_amount' => $this->getTotalPremiumAmount(),
			'admin_fee' => $this->getAdminFee(),
			'base_rate' => $this->getBaseRate(),
			'policy_fee' => $this->getPolicyFee(),
			'base_tax' => $this->getBaseTax(),
			'stamping_tax' => $this->getStampingTax(),
			'other_tax' => $this->getOtherTax(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_disabled' => $this->getIsDisabled(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>