<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CPropertyConstructionTypes
 * Do not add any new functions to this class.
 */

class CBasePropertyConstructionTypes extends CEosPluralBase {

	/**
	 * @return CPropertyConstructionType[]
	 */
	public static function fetchPropertyConstructionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyConstructionType', $objDatabase );
	}

	/**
	 * @return CPropertyConstructionType
	 */
	public static function fetchPropertyConstructionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyConstructionType', $objDatabase );
	}

	public static function fetchPropertyConstructionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_construction_types', $objDatabase );
	}

	public static function fetchPropertyConstructionTypeById( $intId, $objDatabase ) {
		return self::fetchPropertyConstructionType( sprintf( 'SELECT * FROM property_construction_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>