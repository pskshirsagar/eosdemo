<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceClients
 * Do not add any new functions to this class.
 */

class CBaseInsuranceClients extends CEosPluralBase {

	/**
	 * @return CInsuranceClient[]
	 */
	public static function fetchInsuranceClients( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceClient', $objDatabase );
	}

	/**
	 * @return CInsuranceClient
	 */
	public static function fetchInsuranceClient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceClient', $objDatabase );
	}

	public static function fetchInsuranceClientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_clients', $objDatabase );
	}

	public static function fetchInsuranceClientById( $intId, $objDatabase ) {
		return self::fetchInsuranceClient( sprintf( 'SELECT * FROM insurance_clients WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceClientsByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceClients( sprintf( 'SELECT * FROM insurance_clients WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>