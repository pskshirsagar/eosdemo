<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePaymentTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePaymentTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePaymentType[]
	 */
	public static function fetchInsurancePaymentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePaymentType::class, $objDatabase );
	}

	/**
	 * @return CInsurancePaymentType
	 */
	public static function fetchInsurancePaymentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePaymentType::class, $objDatabase );
	}

	public static function fetchInsurancePaymentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_payment_types', $objDatabase );
	}

	public static function fetchInsurancePaymentTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePaymentType( sprintf( 'SELECT * FROM insurance_payment_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>