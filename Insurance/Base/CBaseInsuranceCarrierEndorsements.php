<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierEndorsements
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierEndorsements extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierEndorsement[]
	 */
	public static function fetchInsuranceCarrierEndorsements( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCarrierEndorsement::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierEndorsement
	 */
	public static function fetchInsuranceCarrierEndorsement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCarrierEndorsement::class, $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_endorsements', $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsement( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementsByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsements( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsements( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementsByInsuranceAmountTypeId( $intInsuranceAmountTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsements( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE insurance_amount_type_id = %d', ( int ) $intInsuranceAmountTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementsByInsuranceEndorsementTypeId( $intInsuranceEndorsementTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsements( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE insurance_endorsement_type_id = %d', ( int ) $intInsuranceEndorsementTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementsByParentInsuranceCarrierEndorsementId( $intParentInsuranceCarrierEndorsementId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsements( sprintf( 'SELECT * FROM insurance_carrier_endorsements WHERE parent_insurance_carrier_endorsement_id = %d', ( int ) $intParentInsuranceCarrierEndorsementId ), $objDatabase );
	}

}
?>