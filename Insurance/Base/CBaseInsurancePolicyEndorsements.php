<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyEndorsements
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyEndorsements extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyEndorsement[]
	 */
	public static function fetchInsurancePolicyEndorsements( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyEndorsement', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyEndorsement
	 */
	public static function fetchInsurancePolicyEndorsement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyEndorsement', $objDatabase );
	}

	public static function fetchInsurancePolicyEndorsementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_endorsements', $objDatabase );
	}

	public static function fetchInsurancePolicyEndorsementById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyEndorsement( sprintf( 'SELECT * FROM insurance_policy_endorsements WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEndorsementsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyEndorsements( sprintf( 'SELECT * FROM insurance_policy_endorsements WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEndorsementsByInsurancePolicyCoverageDetailId( $intInsurancePolicyCoverageDetailId, $objDatabase ) {
		return self::fetchInsurancePolicyEndorsements( sprintf( 'SELECT * FROM insurance_policy_endorsements WHERE insurance_policy_coverage_detail_id = %d', ( int ) $intInsurancePolicyCoverageDetailId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEndorsementsByInsuranceCarrierEndorsementId( $intInsuranceCarrierEndorsementId, $objDatabase ) {
		return self::fetchInsurancePolicyEndorsements( sprintf( 'SELECT * FROM insurance_policy_endorsements WHERE insurance_carrier_endorsement_id = %d', ( int ) $intInsuranceCarrierEndorsementId ), $objDatabase );
	}

}
?>