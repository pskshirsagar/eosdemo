<?php

class CBasePolicyReviewLog extends CEosSingularBase {

	const TABLE_NAME = 'public.policy_review_logs';

	protected $m_intId;
	protected $m_intPolicyReviewId;
	protected $m_intPolicyReviewStatusTypeId;
	protected $m_strReviewNotes;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;
		$this->m_jsonDetails = '{}';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['policy_review_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyReviewId', trim( $arrValues['policy_review_id'] ) ); elseif( isset( $arrValues['policy_review_id'] ) ) $this->setPolicyReviewId( $arrValues['policy_review_id'] );
		if( isset( $arrValues['policy_review_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyReviewStatusTypeId', trim( $arrValues['policy_review_status_type_id'] ) ); elseif( isset( $arrValues['policy_review_status_type_id'] ) ) $this->setPolicyReviewStatusTypeId( $arrValues['policy_review_status_type_id'] );
		if( isset( $arrValues['review_notes'] ) && $boolDirectSet ) $this->set( 'm_strReviewNotes', trim( $arrValues['review_notes'] ) ); elseif( isset( $arrValues['review_notes'] ) ) $this->setReviewNotes( $arrValues['review_notes'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPolicyReviewId( $intPolicyReviewId ) {
		$this->set( 'm_intPolicyReviewId', CStrings::strToIntDef( $intPolicyReviewId, NULL, false ) );
	}

	public function getPolicyReviewId() {
		return $this->m_intPolicyReviewId;
	}

	public function sqlPolicyReviewId() {
		return ( true == isset( $this->m_intPolicyReviewId ) ) ? ( string ) $this->m_intPolicyReviewId : 'NULL';
	}

	public function setPolicyReviewStatusTypeId( $intPolicyReviewStatusTypeId ) {
		$this->set( 'm_intPolicyReviewStatusTypeId', CStrings::strToIntDef( $intPolicyReviewStatusTypeId, NULL, false ) );
	}

	public function getPolicyReviewStatusTypeId() {
		return $this->m_intPolicyReviewStatusTypeId;
	}

	public function sqlPolicyReviewStatusTypeId() {
		return ( true == isset( $this->m_intPolicyReviewStatusTypeId ) ) ? ( string ) $this->m_intPolicyReviewStatusTypeId : 'NULL';
	}

	public function setReviewNotes( $strReviewNotes ) {
		$this->set( 'm_strReviewNotes', CStrings::strTrimDef( $strReviewNotes, -1, NULL, true ) );
	}

	public function getReviewNotes() {
		return $this->m_strReviewNotes;
	}

	public function sqlReviewNotes() {
		return ( true == isset( $this->m_strReviewNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReviewNotes ) : '\'' . addslashes( $this->m_strReviewNotes ) . '\'' ) : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, policy_review_id, policy_review_status_type_id, review_notes, is_active, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPolicyReviewId() . ', ' .
						$this->sqlPolicyReviewStatusTypeId() . ', ' .
						$this->sqlReviewNotes() . ', ' .
						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_review_id = ' . $this->sqlPolicyReviewId(). ',' ; } elseif( true == array_key_exists( 'PolicyReviewId', $this->getChangedColumns() ) ) { $strSql .= ' policy_review_id = ' . $this->sqlPolicyReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_review_status_type_id = ' . $this->sqlPolicyReviewStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PolicyReviewStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' policy_review_status_type_id = ' . $this->sqlPolicyReviewStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_notes = ' . $this->sqlReviewNotes(). ',' ; } elseif( true == array_key_exists( 'ReviewNotes', $this->getChangedColumns() ) ) { $strSql .= ' review_notes = ' . $this->sqlReviewNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'policy_review_id' => $this->getPolicyReviewId(),
			'policy_review_status_type_id' => $this->getPolicyReviewStatusTypeId(),
			'review_notes' => $this->getReviewNotes(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>