<?php

class CBaseInsuranceCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_charges';

	protected $m_intId;
	protected $m_intEntityId;
	protected $m_intInsurancePolicyId;
	protected $m_intPremiumTransactionId;
	protected $m_intTaxTransactionId;
	protected $m_intInsurancePolicyLiabilityChargeId;
	protected $m_intInsuranceChargeTypeId;
	protected $m_strChargeDatetime;
	protected $m_fltChargeAmount;
	protected $m_fltExportedPremium;
	protected $m_fltEarnedPremium;
	protected $m_fltLiabilityPremium;
	protected $m_strCoverageStartDate;
	protected $m_strCoverageEndDate;
	protected $m_strReturnProcessedOn;
	protected $m_strReactivatedOn;
	protected $m_strReturnedOn;
	protected $m_strExportedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intInsurancePolicyCoverageDetailId;

	public function __construct() {
		parent::__construct();

		$this->m_intInsuranceChargeTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['premium_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPremiumTransactionId', trim( $arrValues['premium_transaction_id'] ) ); elseif( isset( $arrValues['premium_transaction_id'] ) ) $this->setPremiumTransactionId( $arrValues['premium_transaction_id'] );
		if( isset( $arrValues['tax_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTaxTransactionId', trim( $arrValues['tax_transaction_id'] ) ); elseif( isset( $arrValues['tax_transaction_id'] ) ) $this->setTaxTransactionId( $arrValues['tax_transaction_id'] );
		if( isset( $arrValues['insurance_policy_liability_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyLiabilityChargeId', trim( $arrValues['insurance_policy_liability_charge_id'] ) ); elseif( isset( $arrValues['insurance_policy_liability_charge_id'] ) ) $this->setInsurancePolicyLiabilityChargeId( $arrValues['insurance_policy_liability_charge_id'] );
		if( isset( $arrValues['insurance_charge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceChargeTypeId', trim( $arrValues['insurance_charge_type_id'] ) ); elseif( isset( $arrValues['insurance_charge_type_id'] ) ) $this->setInsuranceChargeTypeId( $arrValues['insurance_charge_type_id'] );
		if( isset( $arrValues['charge_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChargeDatetime', trim( $arrValues['charge_datetime'] ) ); elseif( isset( $arrValues['charge_datetime'] ) ) $this->setChargeDatetime( $arrValues['charge_datetime'] );
		if( isset( $arrValues['charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeAmount', trim( $arrValues['charge_amount'] ) ); elseif( isset( $arrValues['charge_amount'] ) ) $this->setChargeAmount( $arrValues['charge_amount'] );
		if( isset( $arrValues['exported_premium'] ) && $boolDirectSet ) $this->set( 'm_fltExportedPremium', trim( $arrValues['exported_premium'] ) ); elseif( isset( $arrValues['exported_premium'] ) ) $this->setExportedPremium( $arrValues['exported_premium'] );
		if( isset( $arrValues['earned_premium'] ) && $boolDirectSet ) $this->set( 'm_fltEarnedPremium', trim( $arrValues['earned_premium'] ) ); elseif( isset( $arrValues['earned_premium'] ) ) $this->setEarnedPremium( $arrValues['earned_premium'] );
		if( isset( $arrValues['liability_premium'] ) && $boolDirectSet ) $this->set( 'm_fltLiabilityPremium', trim( $arrValues['liability_premium'] ) ); elseif( isset( $arrValues['liability_premium'] ) ) $this->setLiabilityPremium( $arrValues['liability_premium'] );
		if( isset( $arrValues['coverage_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCoverageStartDate', trim( $arrValues['coverage_start_date'] ) ); elseif( isset( $arrValues['coverage_start_date'] ) ) $this->setCoverageStartDate( $arrValues['coverage_start_date'] );
		if( isset( $arrValues['coverage_end_date'] ) && $boolDirectSet ) $this->set( 'm_strCoverageEndDate', trim( $arrValues['coverage_end_date'] ) ); elseif( isset( $arrValues['coverage_end_date'] ) ) $this->setCoverageEndDate( $arrValues['coverage_end_date'] );
		if( isset( $arrValues['return_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnProcessedOn', trim( $arrValues['return_processed_on'] ) ); elseif( isset( $arrValues['return_processed_on'] ) ) $this->setReturnProcessedOn( $arrValues['return_processed_on'] );
		if( isset( $arrValues['reactivated_on'] ) && $boolDirectSet ) $this->set( 'm_strReactivatedOn', trim( $arrValues['reactivated_on'] ) ); elseif( isset( $arrValues['reactivated_on'] ) ) $this->setReactivatedOn( $arrValues['reactivated_on'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['insurance_policy_coverage_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyCoverageDetailId', trim( $arrValues['insurance_policy_coverage_detail_id'] ) ); elseif( isset( $arrValues['insurance_policy_coverage_detail_id'] ) ) $this->setInsurancePolicyCoverageDetailId( $arrValues['insurance_policy_coverage_detail_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setPremiumTransactionId( $intPremiumTransactionId ) {
		$this->set( 'm_intPremiumTransactionId', CStrings::strToIntDef( $intPremiumTransactionId, NULL, false ) );
	}

	public function getPremiumTransactionId() {
		return $this->m_intPremiumTransactionId;
	}

	public function sqlPremiumTransactionId() {
		return ( true == isset( $this->m_intPremiumTransactionId ) ) ? ( string ) $this->m_intPremiumTransactionId : 'NULL';
	}

	public function setTaxTransactionId( $intTaxTransactionId ) {
		$this->set( 'm_intTaxTransactionId', CStrings::strToIntDef( $intTaxTransactionId, NULL, false ) );
	}

	public function getTaxTransactionId() {
		return $this->m_intTaxTransactionId;
	}

	public function sqlTaxTransactionId() {
		return ( true == isset( $this->m_intTaxTransactionId ) ) ? ( string ) $this->m_intTaxTransactionId : 'NULL';
	}

	public function setInsurancePolicyLiabilityChargeId( $intInsurancePolicyLiabilityChargeId ) {
		$this->set( 'm_intInsurancePolicyLiabilityChargeId', CStrings::strToIntDef( $intInsurancePolicyLiabilityChargeId, NULL, false ) );
	}

	public function getInsurancePolicyLiabilityChargeId() {
		return $this->m_intInsurancePolicyLiabilityChargeId;
	}

	public function sqlInsurancePolicyLiabilityChargeId() {
		return ( true == isset( $this->m_intInsurancePolicyLiabilityChargeId ) ) ? ( string ) $this->m_intInsurancePolicyLiabilityChargeId : 'NULL';
	}

	public function setInsuranceChargeTypeId( $intInsuranceChargeTypeId ) {
		$this->set( 'm_intInsuranceChargeTypeId', CStrings::strToIntDef( $intInsuranceChargeTypeId, NULL, false ) );
	}

	public function getInsuranceChargeTypeId() {
		return $this->m_intInsuranceChargeTypeId;
	}

	public function sqlInsuranceChargeTypeId() {
		return ( true == isset( $this->m_intInsuranceChargeTypeId ) ) ? ( string ) $this->m_intInsuranceChargeTypeId : '1';
	}

	public function setChargeDatetime( $strChargeDatetime ) {
		$this->set( 'm_strChargeDatetime', CStrings::strTrimDef( $strChargeDatetime, -1, NULL, true ) );
	}

	public function getChargeDatetime() {
		return $this->m_strChargeDatetime;
	}

	public function sqlChargeDatetime() {
		return ( true == isset( $this->m_strChargeDatetime ) ) ? '\'' . $this->m_strChargeDatetime . '\'' : 'NOW()';
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->set( 'm_fltChargeAmount', CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 ) );
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function sqlChargeAmount() {
		return ( true == isset( $this->m_fltChargeAmount ) ) ? ( string ) $this->m_fltChargeAmount : 'NULL';
	}

	public function setExportedPremium( $fltExportedPremium ) {
		$this->set( 'm_fltExportedPremium', CStrings::strToFloatDef( $fltExportedPremium, NULL, false, 2 ) );
	}

	public function getExportedPremium() {
		return $this->m_fltExportedPremium;
	}

	public function sqlExportedPremium() {
		return ( true == isset( $this->m_fltExportedPremium ) ) ? ( string ) $this->m_fltExportedPremium : 'NULL';
	}

	public function setEarnedPremium( $fltEarnedPremium ) {
		$this->set( 'm_fltEarnedPremium', CStrings::strToFloatDef( $fltEarnedPremium, NULL, false, 2 ) );
	}

	public function getEarnedPremium() {
		return $this->m_fltEarnedPremium;
	}

	public function sqlEarnedPremium() {
		return ( true == isset( $this->m_fltEarnedPremium ) ) ? ( string ) $this->m_fltEarnedPremium : 'NULL';
	}

	public function setLiabilityPremium( $fltLiabilityPremium ) {
		$this->set( 'm_fltLiabilityPremium', CStrings::strToFloatDef( $fltLiabilityPremium, NULL, false, 2 ) );
	}

	public function getLiabilityPremium() {
		return $this->m_fltLiabilityPremium;
	}

	public function sqlLiabilityPremium() {
		return ( true == isset( $this->m_fltLiabilityPremium ) ) ? ( string ) $this->m_fltLiabilityPremium : 'NULL';
	}

	public function setCoverageStartDate( $strCoverageStartDate ) {
		$this->set( 'm_strCoverageStartDate', CStrings::strTrimDef( $strCoverageStartDate, -1, NULL, true ) );
	}

	public function getCoverageStartDate() {
		return $this->m_strCoverageStartDate;
	}

	public function sqlCoverageStartDate() {
		return ( true == isset( $this->m_strCoverageStartDate ) ) ? '\'' . $this->m_strCoverageStartDate . '\'' : 'NOW()';
	}

	public function setCoverageEndDate( $strCoverageEndDate ) {
		$this->set( 'm_strCoverageEndDate', CStrings::strTrimDef( $strCoverageEndDate, -1, NULL, true ) );
	}

	public function getCoverageEndDate() {
		return $this->m_strCoverageEndDate;
	}

	public function sqlCoverageEndDate() {
		return ( true == isset( $this->m_strCoverageEndDate ) ) ? '\'' . $this->m_strCoverageEndDate . '\'' : 'NOW()';
	}

	public function setReturnProcessedOn( $strReturnProcessedOn ) {
		$this->set( 'm_strReturnProcessedOn', CStrings::strTrimDef( $strReturnProcessedOn, -1, NULL, true ) );
	}

	public function getReturnProcessedOn() {
		return $this->m_strReturnProcessedOn;
	}

	public function sqlReturnProcessedOn() {
		return ( true == isset( $this->m_strReturnProcessedOn ) ) ? '\'' . $this->m_strReturnProcessedOn . '\'' : 'NULL';
	}

	public function setReactivatedOn( $strReactivatedOn ) {
		$this->set( 'm_strReactivatedOn', CStrings::strTrimDef( $strReactivatedOn, -1, NULL, true ) );
	}

	public function getReactivatedOn() {
		return $this->m_strReactivatedOn;
	}

	public function sqlReactivatedOn() {
		return ( true == isset( $this->m_strReactivatedOn ) ) ? '\'' . $this->m_strReactivatedOn . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInsurancePolicyCoverageDetailId( $intInsurancePolicyCoverageDetailId ) {
		$this->set( 'm_intInsurancePolicyCoverageDetailId', CStrings::strToIntDef( $intInsurancePolicyCoverageDetailId, NULL, false ) );
	}

	public function getInsurancePolicyCoverageDetailId() {
		return $this->m_intInsurancePolicyCoverageDetailId;
	}

	public function sqlInsurancePolicyCoverageDetailId() {
		return ( true == isset( $this->m_intInsurancePolicyCoverageDetailId ) ) ? ( string ) $this->m_intInsurancePolicyCoverageDetailId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_id, insurance_policy_id, premium_transaction_id, tax_transaction_id, insurance_policy_liability_charge_id, insurance_charge_type_id, charge_datetime, charge_amount, exported_premium, earned_premium, liability_premium, coverage_start_date, coverage_end_date, return_processed_on, reactivated_on, returned_on, exported_on, updated_by, updated_on, created_by, created_on, insurance_policy_coverage_detail_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEntityId() . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlPremiumTransactionId() . ', ' .
 						$this->sqlTaxTransactionId() . ', ' .
 						$this->sqlInsurancePolicyLiabilityChargeId() . ', ' .
 						$this->sqlInsuranceChargeTypeId() . ', ' .
 						$this->sqlChargeDatetime() . ', ' .
 						$this->sqlChargeAmount() . ', ' .
 						$this->sqlExportedPremium() . ', ' .
 						$this->sqlEarnedPremium() . ', ' .
 						$this->sqlLiabilityPremium() . ', ' .
 						$this->sqlCoverageStartDate() . ', ' .
 						$this->sqlCoverageEndDate() . ', ' .
 						$this->sqlReturnProcessedOn() . ', ' .
 						$this->sqlReactivatedOn() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlExportedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlInsurancePolicyCoverageDetailId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_transaction_id = ' . $this->sqlPremiumTransactionId() . ','; } elseif( true == array_key_exists( 'PremiumTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' premium_transaction_id = ' . $this->sqlPremiumTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_transaction_id = ' . $this->sqlTaxTransactionId() . ','; } elseif( true == array_key_exists( 'TaxTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' tax_transaction_id = ' . $this->sqlTaxTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_liability_charge_id = ' . $this->sqlInsurancePolicyLiabilityChargeId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyLiabilityChargeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_liability_charge_id = ' . $this->sqlInsurancePolicyLiabilityChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_charge_type_id = ' . $this->sqlInsuranceChargeTypeId() . ','; } elseif( true == array_key_exists( 'InsuranceChargeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_charge_type_id = ' . $this->sqlInsuranceChargeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; } elseif( true == array_key_exists( 'ChargeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; } elseif( true == array_key_exists( 'ChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_premium = ' . $this->sqlExportedPremium() . ','; } elseif( true == array_key_exists( 'ExportedPremium', $this->getChangedColumns() ) ) { $strSql .= ' exported_premium = ' . $this->sqlExportedPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' earned_premium = ' . $this->sqlEarnedPremium() . ','; } elseif( true == array_key_exists( 'EarnedPremium', $this->getChangedColumns() ) ) { $strSql .= ' earned_premium = ' . $this->sqlEarnedPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_premium = ' . $this->sqlLiabilityPremium() . ','; } elseif( true == array_key_exists( 'LiabilityPremium', $this->getChangedColumns() ) ) { $strSql .= ' liability_premium = ' . $this->sqlLiabilityPremium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_start_date = ' . $this->sqlCoverageStartDate() . ','; } elseif( true == array_key_exists( 'CoverageStartDate', $this->getChangedColumns() ) ) { $strSql .= ' coverage_start_date = ' . $this->sqlCoverageStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' coverage_end_date = ' . $this->sqlCoverageEndDate() . ','; } elseif( true == array_key_exists( 'CoverageEndDate', $this->getChangedColumns() ) ) { $strSql .= ' coverage_end_date = ' . $this->sqlCoverageEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_processed_on = ' . $this->sqlReturnProcessedOn() . ','; } elseif( true == array_key_exists( 'ReturnProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' return_processed_on = ' . $this->sqlReturnProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reactivated_on = ' . $this->sqlReactivatedOn() . ','; } elseif( true == array_key_exists( 'ReactivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' reactivated_on = ' . $this->sqlReactivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_coverage_detail_id = ' . $this->sqlInsurancePolicyCoverageDetailId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyCoverageDetailId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_coverage_detail_id = ' . $this->sqlInsurancePolicyCoverageDetailId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_id' => $this->getEntityId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'premium_transaction_id' => $this->getPremiumTransactionId(),
			'tax_transaction_id' => $this->getTaxTransactionId(),
			'insurance_policy_liability_charge_id' => $this->getInsurancePolicyLiabilityChargeId(),
			'insurance_charge_type_id' => $this->getInsuranceChargeTypeId(),
			'charge_datetime' => $this->getChargeDatetime(),
			'charge_amount' => $this->getChargeAmount(),
			'exported_premium' => $this->getExportedPremium(),
			'earned_premium' => $this->getEarnedPremium(),
			'liability_premium' => $this->getLiabilityPremium(),
			'coverage_start_date' => $this->getCoverageStartDate(),
			'coverage_end_date' => $this->getCoverageEndDate(),
			'return_processed_on' => $this->getReturnProcessedOn(),
			'reactivated_on' => $this->getReactivatedOn(),
			'returned_on' => $this->getReturnedOn(),
			'exported_on' => $this->getExportedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'insurance_policy_coverage_detail_id' => $this->getInsurancePolicyCoverageDetailId()
		);
	}

}
?>