<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarriers
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarriers extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrier[]
	 */
	public static function fetchInsuranceCarriers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCarrier::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCarrier
	 */
	public static function fetchInsuranceCarrier( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCarrier::class, $objDatabase );
	}

	public static function fetchInsuranceCarrierCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carriers', $objDatabase );
	}

	public static function fetchInsuranceCarrierById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrier( sprintf( 'SELECT * FROM insurance_carriers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarriersByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceCarriers( sprintf( 'SELECT * FROM insurance_carriers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>