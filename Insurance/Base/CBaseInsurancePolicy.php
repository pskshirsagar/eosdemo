<?php

class CBaseInsurancePolicy extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policies';

	protected $m_intId;
	protected $m_intEntityId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInsuranceCarrierClassificationTypeId;
	protected $m_intAccountId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsurancePolicyTypeId;
	protected $m_intFrequencyId;
	protected $m_intEmployeeId;
	protected $m_intLeadSourceTypeId;
	protected $m_intInsurancePolicyStatusTypeId;
	protected $m_intInsurancePolicyCancellationTypeId;
	protected $m_intInsuranceTermsId;
	protected $m_intIsIntegrated;
	protected $m_intIsPaymentFailed;
	protected $m_intIsReinstatePolicy;
	protected $m_boolIsFreeSubscription;
	protected $m_intRequiresAdjustment;
	protected $m_intAdjustmentAttemptCount;
	protected $m_strEffectiveDate;
	protected $m_strEndDate;
	protected $m_strLapsedOn;
	protected $m_strConfirmedOn;
	protected $m_intCancelledBy;
	protected $m_strCancelledOn;
	protected $m_strLastPostedOn;
	protected $m_strNextPostOn;
	protected $m_strAdjustmentOn;
	protected $m_strIntegratedOn;
	protected $m_strNextIntegrationOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPaymentTimeoutTypeId;
	protected $m_strNewPaymentSystemMigratedOn;
	protected $m_intPaymentTypeId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intOriginalPropertyId;
	protected $m_intInsurancePolicyMatchTypeId;
	protected $m_boolIsTest;
	protected $m_boolIsFromNewWebsite;

	public function __construct() {
		parent::__construct();

		$this->m_intFrequencyId = '4';
		$this->m_intInsurancePolicyStatusTypeId = '1';
		$this->m_intIsIntegrated = '0';
		$this->m_intIsPaymentFailed = '0';
		$this->m_intIsReinstatePolicy = '0';
		$this->m_boolIsFreeSubscription = false;
		$this->m_intRequiresAdjustment = '0';
		$this->m_intPaymentTimeoutTypeId = '0';
		$this->m_intInsurancePolicyMatchTypeId = '5';
		$this->m_boolIsTest = false;
		$this->m_boolIsFromNewWebsite = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['insurance_carrier_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierClassificationTypeId', trim( $arrValues['insurance_carrier_classification_type_id'] ) ); elseif( isset( $arrValues['insurance_carrier_classification_type_id'] ) ) $this->setInsuranceCarrierClassificationTypeId( $arrValues['insurance_carrier_classification_type_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_policy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyTypeId', trim( $arrValues['insurance_policy_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_type_id'] ) ) $this->setInsurancePolicyTypeId( $arrValues['insurance_policy_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['lead_source_type_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceTypeId', trim( $arrValues['lead_source_type_id'] ) ); elseif( isset( $arrValues['lead_source_type_id'] ) ) $this->setLeadSourceTypeId( $arrValues['lead_source_type_id'] );
		if( isset( $arrValues['insurance_policy_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyStatusTypeId', trim( $arrValues['insurance_policy_status_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_status_type_id'] ) ) $this->setInsurancePolicyStatusTypeId( $arrValues['insurance_policy_status_type_id'] );
		if( isset( $arrValues['insurance_policy_cancellation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyCancellationTypeId', trim( $arrValues['insurance_policy_cancellation_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_cancellation_type_id'] ) ) $this->setInsurancePolicyCancellationTypeId( $arrValues['insurance_policy_cancellation_type_id'] );
		if( isset( $arrValues['insurance_terms_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceTermsId', trim( $arrValues['insurance_terms_id'] ) ); elseif( isset( $arrValues['insurance_terms_id'] ) ) $this->setInsuranceTermsId( $arrValues['insurance_terms_id'] );
		if( isset( $arrValues['is_integrated'] ) && $boolDirectSet ) $this->set( 'm_intIsIntegrated', trim( $arrValues['is_integrated'] ) ); elseif( isset( $arrValues['is_integrated'] ) ) $this->setIsIntegrated( $arrValues['is_integrated'] );
		if( isset( $arrValues['is_payment_failed'] ) && $boolDirectSet ) $this->set( 'm_intIsPaymentFailed', trim( $arrValues['is_payment_failed'] ) ); elseif( isset( $arrValues['is_payment_failed'] ) ) $this->setIsPaymentFailed( $arrValues['is_payment_failed'] );
		if( isset( $arrValues['is_reinstate_policy'] ) && $boolDirectSet ) $this->set( 'm_intIsReinstatePolicy', trim( $arrValues['is_reinstate_policy'] ) ); elseif( isset( $arrValues['is_reinstate_policy'] ) ) $this->setIsReinstatePolicy( $arrValues['is_reinstate_policy'] );
		if( isset( $arrValues['is_free_subscription'] ) && $boolDirectSet ) $this->set( 'm_boolIsFreeSubscription', trim( stripcslashes( $arrValues['is_free_subscription'] ) ) ); elseif( isset( $arrValues['is_free_subscription'] ) ) $this->setIsFreeSubscription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_free_subscription'] ) : $arrValues['is_free_subscription'] );
		if( isset( $arrValues['requires_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intRequiresAdjustment', trim( $arrValues['requires_adjustment'] ) ); elseif( isset( $arrValues['requires_adjustment'] ) ) $this->setRequiresAdjustment( $arrValues['requires_adjustment'] );
		if( isset( $arrValues['adjustment_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAdjustmentAttemptCount', trim( $arrValues['adjustment_attempt_count'] ) ); elseif( isset( $arrValues['adjustment_attempt_count'] ) ) $this->setAdjustmentAttemptCount( $arrValues['adjustment_attempt_count'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['lapsed_on'] ) && $boolDirectSet ) $this->set( 'm_strLapsedOn', trim( $arrValues['lapsed_on'] ) ); elseif( isset( $arrValues['lapsed_on'] ) ) $this->setLapsedOn( $arrValues['lapsed_on'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['cancelled_by'] ) && $boolDirectSet ) $this->set( 'm_intCancelledBy', trim( $arrValues['cancelled_by'] ) ); elseif( isset( $arrValues['cancelled_by'] ) ) $this->setCancelledBy( $arrValues['cancelled_by'] );
		if( isset( $arrValues['cancelled_on'] ) && $boolDirectSet ) $this->set( 'm_strCancelledOn', trim( $arrValues['cancelled_on'] ) ); elseif( isset( $arrValues['cancelled_on'] ) ) $this->setCancelledOn( $arrValues['cancelled_on'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['next_post_on'] ) && $boolDirectSet ) $this->set( 'm_strNextPostOn', trim( $arrValues['next_post_on'] ) ); elseif( isset( $arrValues['next_post_on'] ) ) $this->setNextPostOn( $arrValues['next_post_on'] );
		if( isset( $arrValues['adjustment_on'] ) && $boolDirectSet ) $this->set( 'm_strAdjustmentOn', trim( $arrValues['adjustment_on'] ) ); elseif( isset( $arrValues['adjustment_on'] ) ) $this->setAdjustmentOn( $arrValues['adjustment_on'] );
		if( isset( $arrValues['integrated_on'] ) && $boolDirectSet ) $this->set( 'm_strIntegratedOn', trim( $arrValues['integrated_on'] ) ); elseif( isset( $arrValues['integrated_on'] ) ) $this->setIntegratedOn( $arrValues['integrated_on'] );
		if( isset( $arrValues['next_integration_on'] ) && $boolDirectSet ) $this->set( 'm_strNextIntegrationOn', trim( $arrValues['next_integration_on'] ) ); elseif( isset( $arrValues['next_integration_on'] ) ) $this->setNextIntegrationOn( $arrValues['next_integration_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['payment_timeout_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTimeoutTypeId', trim( $arrValues['payment_timeout_type_id'] ) ); elseif( isset( $arrValues['payment_timeout_type_id'] ) ) $this->setPaymentTimeoutTypeId( $arrValues['payment_timeout_type_id'] );
		if( isset( $arrValues['new_payment_system_migrated_on'] ) && $boolDirectSet ) $this->set( 'm_strNewPaymentSystemMigratedOn', trim( $arrValues['new_payment_system_migrated_on'] ) ); elseif( isset( $arrValues['new_payment_system_migrated_on'] ) ) $this->setNewPaymentSystemMigratedOn( $arrValues['new_payment_system_migrated_on'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['original_property_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalPropertyId', trim( $arrValues['original_property_id'] ) ); elseif( isset( $arrValues['original_property_id'] ) ) $this->setOriginalPropertyId( $arrValues['original_property_id'] );
		if( isset( $arrValues['insurance_policy_match_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyMatchTypeId', trim( $arrValues['insurance_policy_match_type_id'] ) ); elseif( isset( $arrValues['insurance_policy_match_type_id'] ) ) $this->setInsurancePolicyMatchTypeId( $arrValues['insurance_policy_match_type_id'] );
		if( isset( $arrValues['is_test'] ) && $boolDirectSet ) $this->set( 'm_boolIsTest', trim( stripcslashes( $arrValues['is_test'] ) ) ); elseif( isset( $arrValues['is_test'] ) ) $this->setIsTest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_test'] ) : $arrValues['is_test'] );
		if( isset( $arrValues['is_from_new_website'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromNewWebsite', trim( stripcslashes( $arrValues['is_from_new_website'] ) ) ); elseif( isset( $arrValues['is_from_new_website'] ) ) $this->setIsFromNewWebsite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_new_website'] ) : $arrValues['is_from_new_website'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId ) {
		$this->set( 'm_intInsuranceCarrierClassificationTypeId', CStrings::strToIntDef( $intInsuranceCarrierClassificationTypeId, NULL, false ) );
	}

	public function getInsuranceCarrierClassificationTypeId() {
		return $this->m_intInsuranceCarrierClassificationTypeId;
	}

	public function sqlInsuranceCarrierClassificationTypeId() {
		return ( true == isset( $this->m_intInsuranceCarrierClassificationTypeId ) ) ? ( string ) $this->m_intInsuranceCarrierClassificationTypeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsurancePolicyTypeId( $intInsurancePolicyTypeId ) {
		$this->set( 'm_intInsurancePolicyTypeId', CStrings::strToIntDef( $intInsurancePolicyTypeId, NULL, false ) );
	}

	public function getInsurancePolicyTypeId() {
		return $this->m_intInsurancePolicyTypeId;
	}

	public function sqlInsurancePolicyTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyTypeId ) ) ? ( string ) $this->m_intInsurancePolicyTypeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : '4';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setLeadSourceTypeId( $intLeadSourceTypeId ) {
		$this->set( 'm_intLeadSourceTypeId', CStrings::strToIntDef( $intLeadSourceTypeId, NULL, false ) );
	}

	public function getLeadSourceTypeId() {
		return $this->m_intLeadSourceTypeId;
	}

	public function sqlLeadSourceTypeId() {
		return ( true == isset( $this->m_intLeadSourceTypeId ) ) ? ( string ) $this->m_intLeadSourceTypeId : 'NULL';
	}

	public function setInsurancePolicyStatusTypeId( $intInsurancePolicyStatusTypeId ) {
		$this->set( 'm_intInsurancePolicyStatusTypeId', CStrings::strToIntDef( $intInsurancePolicyStatusTypeId, NULL, false ) );
	}

	public function getInsurancePolicyStatusTypeId() {
		return $this->m_intInsurancePolicyStatusTypeId;
	}

	public function sqlInsurancePolicyStatusTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyStatusTypeId ) ) ? ( string ) $this->m_intInsurancePolicyStatusTypeId : '1';
	}

	public function setInsurancePolicyCancellationTypeId( $intInsurancePolicyCancellationTypeId ) {
		$this->set( 'm_intInsurancePolicyCancellationTypeId', CStrings::strToIntDef( $intInsurancePolicyCancellationTypeId, NULL, false ) );
	}

	public function getInsurancePolicyCancellationTypeId() {
		return $this->m_intInsurancePolicyCancellationTypeId;
	}

	public function sqlInsurancePolicyCancellationTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyCancellationTypeId ) ) ? ( string ) $this->m_intInsurancePolicyCancellationTypeId : 'NULL';
	}

	public function setInsuranceTermsId( $intInsuranceTermsId ) {
		$this->set( 'm_intInsuranceTermsId', CStrings::strToIntDef( $intInsuranceTermsId, NULL, false ) );
	}

	public function getInsuranceTermsId() {
		return $this->m_intInsuranceTermsId;
	}

	public function sqlInsuranceTermsId() {
		return ( true == isset( $this->m_intInsuranceTermsId ) ) ? ( string ) $this->m_intInsuranceTermsId : 'NULL';
	}

	public function setIsIntegrated( $intIsIntegrated ) {
		$this->set( 'm_intIsIntegrated', CStrings::strToIntDef( $intIsIntegrated, NULL, false ) );
	}

	public function getIsIntegrated() {
		return $this->m_intIsIntegrated;
	}

	public function sqlIsIntegrated() {
		return ( true == isset( $this->m_intIsIntegrated ) ) ? ( string ) $this->m_intIsIntegrated : '0';
	}

	public function setIsPaymentFailed( $intIsPaymentFailed ) {
		$this->set( 'm_intIsPaymentFailed', CStrings::strToIntDef( $intIsPaymentFailed, NULL, false ) );
	}

	public function getIsPaymentFailed() {
		return $this->m_intIsPaymentFailed;
	}

	public function sqlIsPaymentFailed() {
		return ( true == isset( $this->m_intIsPaymentFailed ) ) ? ( string ) $this->m_intIsPaymentFailed : '0';
	}

	public function setIsReinstatePolicy( $intIsReinstatePolicy ) {
		$this->set( 'm_intIsReinstatePolicy', CStrings::strToIntDef( $intIsReinstatePolicy, NULL, false ) );
	}

	public function getIsReinstatePolicy() {
		return $this->m_intIsReinstatePolicy;
	}

	public function sqlIsReinstatePolicy() {
		return ( true == isset( $this->m_intIsReinstatePolicy ) ) ? ( string ) $this->m_intIsReinstatePolicy : '0';
	}

	public function setIsFreeSubscription( $boolIsFreeSubscription ) {
		$this->set( 'm_boolIsFreeSubscription', CStrings::strToBool( $boolIsFreeSubscription ) );
	}

	public function getIsFreeSubscription() {
		return $this->m_boolIsFreeSubscription;
	}

	public function sqlIsFreeSubscription() {
		return ( true == isset( $this->m_boolIsFreeSubscription ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFreeSubscription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequiresAdjustment( $intRequiresAdjustment ) {
		$this->set( 'm_intRequiresAdjustment', CStrings::strToIntDef( $intRequiresAdjustment, NULL, false ) );
	}

	public function getRequiresAdjustment() {
		return $this->m_intRequiresAdjustment;
	}

	public function sqlRequiresAdjustment() {
		return ( true == isset( $this->m_intRequiresAdjustment ) ) ? ( string ) $this->m_intRequiresAdjustment : '0';
	}

	public function setAdjustmentAttemptCount( $intAdjustmentAttemptCount ) {
		$this->set( 'm_intAdjustmentAttemptCount', CStrings::strToIntDef( $intAdjustmentAttemptCount, NULL, false ) );
	}

	public function getAdjustmentAttemptCount() {
		return $this->m_intAdjustmentAttemptCount;
	}

	public function sqlAdjustmentAttemptCount() {
		return ( true == isset( $this->m_intAdjustmentAttemptCount ) ) ? ( string ) $this->m_intAdjustmentAttemptCount : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setLapsedOn( $strLapsedOn ) {
		$this->set( 'm_strLapsedOn', CStrings::strTrimDef( $strLapsedOn, -1, NULL, true ) );
	}

	public function getLapsedOn() {
		return $this->m_strLapsedOn;
	}

	public function sqlLapsedOn() {
		return ( true == isset( $this->m_strLapsedOn ) ) ? '\'' . $this->m_strLapsedOn . '\'' : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setCancelledBy( $intCancelledBy ) {
		$this->set( 'm_intCancelledBy', CStrings::strToIntDef( $intCancelledBy, NULL, false ) );
	}

	public function getCancelledBy() {
		return $this->m_intCancelledBy;
	}

	public function sqlCancelledBy() {
		return ( true == isset( $this->m_intCancelledBy ) ) ? ( string ) $this->m_intCancelledBy : 'NULL';
	}

	public function setCancelledOn( $strCancelledOn ) {
		$this->set( 'm_strCancelledOn', CStrings::strTrimDef( $strCancelledOn, -1, NULL, true ) );
	}

	public function getCancelledOn() {
		return $this->m_strCancelledOn;
	}

	public function sqlCancelledOn() {
		return ( true == isset( $this->m_strCancelledOn ) ) ? '\'' . $this->m_strCancelledOn . '\'' : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setNextPostOn( $strNextPostOn ) {
		$this->set( 'm_strNextPostOn', CStrings::strTrimDef( $strNextPostOn, -1, NULL, true ) );
	}

	public function getNextPostOn() {
		return $this->m_strNextPostOn;
	}

	public function sqlNextPostOn() {
		return ( true == isset( $this->m_strNextPostOn ) ) ? '\'' . $this->m_strNextPostOn . '\'' : 'NULL';
	}

	public function setAdjustmentOn( $strAdjustmentOn ) {
		$this->set( 'm_strAdjustmentOn', CStrings::strTrimDef( $strAdjustmentOn, -1, NULL, true ) );
	}

	public function getAdjustmentOn() {
		return $this->m_strAdjustmentOn;
	}

	public function sqlAdjustmentOn() {
		return ( true == isset( $this->m_strAdjustmentOn ) ) ? '\'' . $this->m_strAdjustmentOn . '\'' : 'NULL';
	}

	public function setIntegratedOn( $strIntegratedOn ) {
		$this->set( 'm_strIntegratedOn', CStrings::strTrimDef( $strIntegratedOn, -1, NULL, true ) );
	}

	public function getIntegratedOn() {
		return $this->m_strIntegratedOn;
	}

	public function sqlIntegratedOn() {
		return ( true == isset( $this->m_strIntegratedOn ) ) ? '\'' . $this->m_strIntegratedOn . '\'' : 'NULL';
	}

	public function setNextIntegrationOn( $strNextIntegrationOn ) {
		$this->set( 'm_strNextIntegrationOn', CStrings::strTrimDef( $strNextIntegrationOn, -1, NULL, true ) );
	}

	public function getNextIntegrationOn() {
		return $this->m_strNextIntegrationOn;
	}

	public function sqlNextIntegrationOn() {
		return ( true == isset( $this->m_strNextIntegrationOn ) ) ? '\'' . $this->m_strNextIntegrationOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPaymentTimeoutTypeId( $intPaymentTimeoutTypeId ) {
		$this->set( 'm_intPaymentTimeoutTypeId', CStrings::strToIntDef( $intPaymentTimeoutTypeId, NULL, false ) );
	}

	public function getPaymentTimeoutTypeId() {
		return $this->m_intPaymentTimeoutTypeId;
	}

	public function sqlPaymentTimeoutTypeId() {
		return ( true == isset( $this->m_intPaymentTimeoutTypeId ) ) ? ( string ) $this->m_intPaymentTimeoutTypeId : '0';
	}

	public function setNewPaymentSystemMigratedOn( $strNewPaymentSystemMigratedOn ) {
		$this->set( 'm_strNewPaymentSystemMigratedOn', CStrings::strTrimDef( $strNewPaymentSystemMigratedOn, -1, NULL, true ) );
	}

	public function getNewPaymentSystemMigratedOn() {
		return $this->m_strNewPaymentSystemMigratedOn;
	}

	public function sqlNewPaymentSystemMigratedOn() {
		return ( true == isset( $this->m_strNewPaymentSystemMigratedOn ) ) ? '\'' . $this->m_strNewPaymentSystemMigratedOn . '\'' : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setOriginalPropertyId( $intOriginalPropertyId ) {
		$this->set( 'm_intOriginalPropertyId', CStrings::strToIntDef( $intOriginalPropertyId, NULL, false ) );
	}

	public function getOriginalPropertyId() {
		return $this->m_intOriginalPropertyId;
	}

	public function sqlOriginalPropertyId() {
		return ( true == isset( $this->m_intOriginalPropertyId ) ) ? ( string ) $this->m_intOriginalPropertyId : 'NULL';
	}

	public function setInsurancePolicyMatchTypeId( $intInsurancePolicyMatchTypeId ) {
		$this->set( 'm_intInsurancePolicyMatchTypeId', CStrings::strToIntDef( $intInsurancePolicyMatchTypeId, NULL, false ) );
	}

	public function getInsurancePolicyMatchTypeId() {
		return $this->m_intInsurancePolicyMatchTypeId;
	}

	public function sqlInsurancePolicyMatchTypeId() {
		return ( true == isset( $this->m_intInsurancePolicyMatchTypeId ) ) ? ( string ) $this->m_intInsurancePolicyMatchTypeId : '5';
	}

	public function setIsTest( $boolIsTest ) {
		$this->set( 'm_boolIsTest', CStrings::strToBool( $boolIsTest ) );
	}

	public function getIsTest() {
		return $this->m_boolIsTest;
	}

	public function sqlIsTest() {
		return ( true == isset( $this->m_boolIsTest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFromNewWebsite( $boolIsFromNewWebsite ) {
		$this->set( 'm_boolIsFromNewWebsite', CStrings::strToBool( $boolIsFromNewWebsite ) );
	}

	public function getIsFromNewWebsite() {
		return $this->m_boolIsFromNewWebsite;
	}

	public function sqlIsFromNewWebsite() {
		return ( true == isset( $this->m_boolIsFromNewWebsite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromNewWebsite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_id, cid, property_id, insurance_carrier_classification_type_id, account_id, insurance_carrier_id, insurance_policy_type_id, frequency_id, employee_id, lead_source_type_id, insurance_policy_status_type_id, insurance_policy_cancellation_type_id, insurance_terms_id, is_integrated, is_payment_failed, is_reinstate_policy, is_free_subscription, requires_adjustment, adjustment_attempt_count, effective_date, end_date, lapsed_on, confirmed_on, cancelled_by, cancelled_on, last_posted_on, next_post_on, adjustment_on, integrated_on, next_integration_on, updated_by, updated_on, created_by, created_on, payment_timeout_type_id, new_payment_system_migrated_on, payment_type_id, lease_id, customer_id, original_property_id, insurance_policy_match_type_id, is_test, is_from_new_website )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlEntityId() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlInsuranceCarrierClassificationTypeId() . ', ' .
		          $this->sqlAccountId() . ', ' .
		          $this->sqlInsuranceCarrierId() . ', ' .
		          $this->sqlInsurancePolicyTypeId() . ', ' .
		          $this->sqlFrequencyId() . ', ' .
		          $this->sqlEmployeeId() . ', ' .
		          $this->sqlLeadSourceTypeId() . ', ' .
		          $this->sqlInsurancePolicyStatusTypeId() . ', ' .
		          $this->sqlInsurancePolicyCancellationTypeId() . ', ' .
		          $this->sqlInsuranceTermsId() . ', ' .
		          $this->sqlIsIntegrated() . ', ' .
		          $this->sqlIsPaymentFailed() . ', ' .
		          $this->sqlIsReinstatePolicy() . ', ' .
		          $this->sqlIsFreeSubscription() . ', ' .
		          $this->sqlRequiresAdjustment() . ', ' .
		          $this->sqlAdjustmentAttemptCount() . ', ' .
		          $this->sqlEffectiveDate() . ', ' .
		          $this->sqlEndDate() . ', ' .
		          $this->sqlLapsedOn() . ', ' .
		          $this->sqlConfirmedOn() . ', ' .
		          $this->sqlCancelledBy() . ', ' .
		          $this->sqlCancelledOn() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlNextPostOn() . ', ' .
		          $this->sqlAdjustmentOn() . ', ' .
		          $this->sqlIntegratedOn() . ', ' .
		          $this->sqlNextIntegrationOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlPaymentTimeoutTypeId() . ', ' .
		          $this->sqlNewPaymentSystemMigratedOn() . ', ' .
		          $this->sqlPaymentTypeId() . ', ' .
		          $this->sqlLeaseId() . ', ' .
		          $this->sqlCustomerId() . ', ' .
		          $this->sqlOriginalPropertyId() . ', ' .
		          $this->sqlInsurancePolicyMatchTypeId() . ', ' .
		          $this->sqlIsTest() . ', ' .
		          $this->sqlIsFromNewWebsite() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId(). ',' ; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_classification_type_id = ' . $this->sqlInsuranceCarrierClassificationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_type_id = ' . $this->sqlInsurancePolicyTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_type_id = ' . $this->sqlLeadSourceTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_status_type_id = ' . $this->sqlInsurancePolicyStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_cancellation_type_id = ' . $this->sqlInsurancePolicyCancellationTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyCancellationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_cancellation_type_id = ' . $this->sqlInsurancePolicyCancellationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_terms_id = ' . $this->sqlInsuranceTermsId(). ',' ; } elseif( true == array_key_exists( 'InsuranceTermsId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_terms_id = ' . $this->sqlInsuranceTermsId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated(). ',' ; } elseif( true == array_key_exists( 'IsIntegrated', $this->getChangedColumns() ) ) { $strSql .= ' is_integrated = ' . $this->sqlIsIntegrated() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_payment_failed = ' . $this->sqlIsPaymentFailed(). ',' ; } elseif( true == array_key_exists( 'IsPaymentFailed', $this->getChangedColumns() ) ) { $strSql .= ' is_payment_failed = ' . $this->sqlIsPaymentFailed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reinstate_policy = ' . $this->sqlIsReinstatePolicy(). ',' ; } elseif( true == array_key_exists( 'IsReinstatePolicy', $this->getChangedColumns() ) ) { $strSql .= ' is_reinstate_policy = ' . $this->sqlIsReinstatePolicy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_free_subscription = ' . $this->sqlIsFreeSubscription(). ',' ; } elseif( true == array_key_exists( 'IsFreeSubscription', $this->getChangedColumns() ) ) { $strSql .= ' is_free_subscription = ' . $this->sqlIsFreeSubscription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requires_adjustment = ' . $this->sqlRequiresAdjustment(). ',' ; } elseif( true == array_key_exists( 'RequiresAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' requires_adjustment = ' . $this->sqlRequiresAdjustment() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_attempt_count = ' . $this->sqlAdjustmentAttemptCount(). ',' ; } elseif( true == array_key_exists( 'AdjustmentAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_attempt_count = ' . $this->sqlAdjustmentAttemptCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lapsed_on = ' . $this->sqlLapsedOn(). ',' ; } elseif( true == array_key_exists( 'LapsedOn', $this->getChangedColumns() ) ) { $strSql .= ' lapsed_on = ' . $this->sqlLapsedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn(). ',' ; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy(). ',' ; } elseif( true == array_key_exists( 'CancelledBy', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_by = ' . $this->sqlCancelledBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn(). ',' ; } elseif( true == array_key_exists( 'CancelledOn', $this->getChangedColumns() ) ) { $strSql .= ' cancelled_on = ' . $this->sqlCancelledOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_post_on = ' . $this->sqlNextPostOn(). ',' ; } elseif( true == array_key_exists( 'NextPostOn', $this->getChangedColumns() ) ) { $strSql .= ' next_post_on = ' . $this->sqlNextPostOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_on = ' . $this->sqlAdjustmentOn(). ',' ; } elseif( true == array_key_exists( 'AdjustmentOn', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_on = ' . $this->sqlAdjustmentOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integrated_on = ' . $this->sqlIntegratedOn(). ',' ; } elseif( true == array_key_exists( 'IntegratedOn', $this->getChangedColumns() ) ) { $strSql .= ' integrated_on = ' . $this->sqlIntegratedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_integration_on = ' . $this->sqlNextIntegrationOn(). ',' ; } elseif( true == array_key_exists( 'NextIntegrationOn', $this->getChangedColumns() ) ) { $strSql .= ' next_integration_on = ' . $this->sqlNextIntegrationOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_timeout_type_id = ' . $this->sqlPaymentTimeoutTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTimeoutTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_timeout_type_id = ' . $this->sqlPaymentTimeoutTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_payment_system_migrated_on = ' . $this->sqlNewPaymentSystemMigratedOn(). ',' ; } elseif( true == array_key_exists( 'NewPaymentSystemMigratedOn', $this->getChangedColumns() ) ) { $strSql .= ' new_payment_system_migrated_on = ' . $this->sqlNewPaymentSystemMigratedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_property_id = ' . $this->sqlOriginalPropertyId(). ',' ; } elseif( true == array_key_exists( 'OriginalPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' original_property_id = ' . $this->sqlOriginalPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_match_type_id = ' . $this->sqlInsurancePolicyMatchTypeId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyMatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_match_type_id = ' . $this->sqlInsurancePolicyMatchTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test = ' . $this->sqlIsTest(). ',' ; } elseif( true == array_key_exists( 'IsTest', $this->getChangedColumns() ) ) { $strSql .= ' is_test = ' . $this->sqlIsTest() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_from_new_website = ' . $this->sqlIsFromNewWebsite(). ',' ; } elseif( true == array_key_exists( 'IsFromNewWebsite', $this->getChangedColumns() ) ) { $strSql .= ' is_from_new_website = ' . $this->sqlIsFromNewWebsite() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_id' => $this->getEntityId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'insurance_carrier_classification_type_id' => $this->getInsuranceCarrierClassificationTypeId(),
			'account_id' => $this->getAccountId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_policy_type_id' => $this->getInsurancePolicyTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'employee_id' => $this->getEmployeeId(),
			'lead_source_type_id' => $this->getLeadSourceTypeId(),
			'insurance_policy_status_type_id' => $this->getInsurancePolicyStatusTypeId(),
			'insurance_policy_cancellation_type_id' => $this->getInsurancePolicyCancellationTypeId(),
			'insurance_terms_id' => $this->getInsuranceTermsId(),
			'is_integrated' => $this->getIsIntegrated(),
			'is_payment_failed' => $this->getIsPaymentFailed(),
			'is_reinstate_policy' => $this->getIsReinstatePolicy(),
			'is_free_subscription' => $this->getIsFreeSubscription(),
			'requires_adjustment' => $this->getRequiresAdjustment(),
			'adjustment_attempt_count' => $this->getAdjustmentAttemptCount(),
			'effective_date' => $this->getEffectiveDate(),
			'end_date' => $this->getEndDate(),
			'lapsed_on' => $this->getLapsedOn(),
			'confirmed_on' => $this->getConfirmedOn(),
			'cancelled_by' => $this->getCancelledBy(),
			'cancelled_on' => $this->getCancelledOn(),
			'last_posted_on' => $this->getLastPostedOn(),
			'next_post_on' => $this->getNextPostOn(),
			'adjustment_on' => $this->getAdjustmentOn(),
			'integrated_on' => $this->getIntegratedOn(),
			'next_integration_on' => $this->getNextIntegrationOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'payment_timeout_type_id' => $this->getPaymentTimeoutTypeId(),
			'new_payment_system_migrated_on' => $this->getNewPaymentSystemMigratedOn(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'original_property_id' => $this->getOriginalPropertyId(),
			'insurance_policy_match_type_id' => $this->getInsurancePolicyMatchTypeId(),
			'is_test' => $this->getIsTest(),
			'is_from_new_website' => $this->getIsFromNewWebsite()
		);
	}

}
?>