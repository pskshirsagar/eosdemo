<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceNoticeTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceNoticeTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceNoticeType[]
	 */
	public static function fetchInsuranceNoticeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceNoticeType', $objDatabase );
	}

	/**
	 * @return CInsuranceNoticeType
	 */
	public static function fetchInsuranceNoticeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceNoticeType', $objDatabase );
	}

	public static function fetchInsuranceNoticeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_notice_types', $objDatabase );
	}

	public static function fetchInsuranceNoticeTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceNoticeType( sprintf( 'SELECT * FROM insurance_notice_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>