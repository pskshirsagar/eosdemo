<?php

class CBaseInsurancePolicyMigration extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_migrations';

	protected $m_intId;
	protected $m_intSourceCid;
	protected $m_intDestinationCid;
	protected $m_intSourcePropertyId;
	protected $m_intDestinationPropertyId;
	protected $m_intEntityId;
	protected $m_strPolicyNumber;
	protected $m_boolIsMigrated;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strNewPolicyNumber;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMigrated = true;
		$this->m_strNewPolicyNumber = 'NULL';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['source_cid'] ) && $boolDirectSet ) $this->set( 'm_intSourceCid', trim( $arrValues['source_cid'] ) ); elseif( isset( $arrValues['source_cid'] ) ) $this->setSourceCid( $arrValues['source_cid'] );
		if( isset( $arrValues['destination_cid'] ) && $boolDirectSet ) $this->set( 'm_intDestinationCid', trim( $arrValues['destination_cid'] ) ); elseif( isset( $arrValues['destination_cid'] ) ) $this->setDestinationCid( $arrValues['destination_cid'] );
		if( isset( $arrValues['source_property_id'] ) && $boolDirectSet ) $this->set( 'm_intSourcePropertyId', trim( $arrValues['source_property_id'] ) ); elseif( isset( $arrValues['source_property_id'] ) ) $this->setSourcePropertyId( $arrValues['source_property_id'] );
		if( isset( $arrValues['destination_property_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationPropertyId', trim( $arrValues['destination_property_id'] ) ); elseif( isset( $arrValues['destination_property_id'] ) ) $this->setDestinationPropertyId( $arrValues['destination_property_id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['policy_number'] ) && $boolDirectSet ) $this->set( 'm_strPolicyNumber', trim( stripcslashes( $arrValues['policy_number'] ) ) ); elseif( isset( $arrValues['policy_number'] ) ) $this->setPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['policy_number'] ) : $arrValues['policy_number'] );
		if( isset( $arrValues['is_migrated'] ) && $boolDirectSet ) $this->set( 'm_boolIsMigrated', trim( stripcslashes( $arrValues['is_migrated'] ) ) ); elseif( isset( $arrValues['is_migrated'] ) ) $this->setIsMigrated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_migrated'] ) : $arrValues['is_migrated'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['new_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strNewPolicyNumber', trim( stripcslashes( $arrValues['new_policy_number'] ) ) ); elseif( isset( $arrValues['new_policy_number'] ) ) $this->setNewPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_policy_number'] ) : $arrValues['new_policy_number'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSourceCid( $intSourceCid ) {
		$this->set( 'm_intSourceCid', CStrings::strToIntDef( $intSourceCid, NULL, false ) );
	}

	public function getSourceCid() {
		return $this->m_intSourceCid;
	}

	public function sqlSourceCid() {
		return ( true == isset( $this->m_intSourceCid ) ) ? ( string ) $this->m_intSourceCid : 'NULL';
	}

	public function setDestinationCid( $intDestinationCid ) {
		$this->set( 'm_intDestinationCid', CStrings::strToIntDef( $intDestinationCid, NULL, false ) );
	}

	public function getDestinationCid() {
		return $this->m_intDestinationCid;
	}

	public function sqlDestinationCid() {
		return ( true == isset( $this->m_intDestinationCid ) ) ? ( string ) $this->m_intDestinationCid : 'NULL';
	}

	public function setSourcePropertyId( $intSourcePropertyId ) {
		$this->set( 'm_intSourcePropertyId', CStrings::strToIntDef( $intSourcePropertyId, NULL, false ) );
	}

	public function getSourcePropertyId() {
		return $this->m_intSourcePropertyId;
	}

	public function sqlSourcePropertyId() {
		return ( true == isset( $this->m_intSourcePropertyId ) ) ? ( string ) $this->m_intSourcePropertyId : 'NULL';
	}

	public function setDestinationPropertyId( $intDestinationPropertyId ) {
		$this->set( 'm_intDestinationPropertyId', CStrings::strToIntDef( $intDestinationPropertyId, NULL, false ) );
	}

	public function getDestinationPropertyId() {
		return $this->m_intDestinationPropertyId;
	}

	public function sqlDestinationPropertyId() {
		return ( true == isset( $this->m_intDestinationPropertyId ) ) ? ( string ) $this->m_intDestinationPropertyId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setPolicyNumber( $strPolicyNumber ) {
		$this->set( 'm_strPolicyNumber', CStrings::strTrimDef( $strPolicyNumber, 64, NULL, true ) );
	}

	public function getPolicyNumber() {
		return $this->m_strPolicyNumber;
	}

	public function sqlPolicyNumber() {
		return ( true == isset( $this->m_strPolicyNumber ) ) ? '\'' . addslashes( $this->m_strPolicyNumber ) . '\'' : 'NULL';
	}

	public function setIsMigrated( $boolIsMigrated ) {
		$this->set( 'm_boolIsMigrated', CStrings::strToBool( $boolIsMigrated ) );
	}

	public function getIsMigrated() {
		return $this->m_boolIsMigrated;
	}

	public function sqlIsMigrated() {
		return ( true == isset( $this->m_boolIsMigrated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMigrated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setNewPolicyNumber( $strNewPolicyNumber ) {
		$this->set( 'm_strNewPolicyNumber', CStrings::strTrimDef( $strNewPolicyNumber, 64, NULL, true ) );
	}

	public function getNewPolicyNumber() {
		return $this->m_strNewPolicyNumber;
	}

	public function sqlNewPolicyNumber() {
		return ( true == isset( $this->m_strNewPolicyNumber ) ) ? '\'' . addslashes( $this->m_strNewPolicyNumber ) . '\'' : '\'NULL\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, source_cid, destination_cid, source_property_id, destination_property_id, entity_id, policy_number, is_migrated, created_by, created_on, new_policy_number )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSourceCid() . ', ' .
						$this->sqlDestinationCid() . ', ' .
						$this->sqlSourcePropertyId() . ', ' .
						$this->sqlDestinationPropertyId() . ', ' .
						$this->sqlEntityId() . ', ' .
						$this->sqlPolicyNumber() . ', ' .
						$this->sqlIsMigrated() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlNewPolicyNumber() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid() . ','; } elseif( true == array_key_exists( 'SourceCid', $this->getChangedColumns() ) ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid() . ','; } elseif( true == array_key_exists( 'DestinationCid', $this->getChangedColumns() ) ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; } elseif( true == array_key_exists( 'SourcePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; } elseif( true == array_key_exists( 'DestinationPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; } elseif( true == array_key_exists( 'PolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_number = ' . $this->sqlPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated() . ','; } elseif( true == array_key_exists( 'IsMigrated', $this->getChangedColumns() ) ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_policy_number = ' . $this->sqlNewPolicyNumber() . ','; } elseif( true == array_key_exists( 'NewPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' new_policy_number = ' . $this->sqlNewPolicyNumber() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'source_cid' => $this->getSourceCid(),
			'destination_cid' => $this->getDestinationCid(),
			'source_property_id' => $this->getSourcePropertyId(),
			'destination_property_id' => $this->getDestinationPropertyId(),
			'entity_id' => $this->getEntityId(),
			'policy_number' => $this->getPolicyNumber(),
			'is_migrated' => $this->getIsMigrated(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'new_policy_number' => $this->getNewPolicyNumber()
		);
	}

}
?>