<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceAgentTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceAgentTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceAgentType[]
	 */
	public static function fetchInsuranceAgentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceAgentType', $objDatabase );
	}

	/**
	 * @return CInsuranceAgentType
	 */
	public static function fetchInsuranceAgentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceAgentType', $objDatabase );
	}

	public static function fetchInsuranceAgentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_agent_types', $objDatabase );
	}

	public static function fetchInsuranceAgentTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceAgentType( sprintf( 'SELECT * FROM insurance_agent_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>