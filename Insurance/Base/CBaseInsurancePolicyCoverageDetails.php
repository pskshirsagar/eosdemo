<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyCoverageDetails
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyCoverageDetails extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyCoverageDetail[]
	 */
	public static function fetchInsurancePolicyCoverageDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyCoverageDetail::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyCoverageDetail
	 */
	public static function fetchInsurancePolicyCoverageDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyCoverageDetail::class, $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_coverage_details', $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetail( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE insurance_policy_id = %d', $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE deductible_insurance_carrier_limit_id = %d', $intDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE liability_insurance_carrier_limit_id = %d', $intLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE personal_insurance_carrier_limit_id = %d', $intPersonalInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByInsurancePolicyDetailStatusTypeId( $intInsurancePolicyDetailStatusTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE insurance_policy_detail_status_type_id = %d', $intInsurancePolicyDetailStatusTypeId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByMedicalPaymentLimitId( $intMedicalPaymentLimitId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE medical_payment_limit_id = %d', $intMedicalPaymentLimitId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCoverageDetailsByHurricaneDeductibleInsuranceCarrierLimitId( $intHurricaneDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsurancePolicyCoverageDetails( sprintf( 'SELECT * FROM insurance_policy_coverage_details WHERE hurricane_deductible_insurance_carrier_limit_id = %d', $intHurricaneDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

}
?>