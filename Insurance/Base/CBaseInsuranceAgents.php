<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceAgents
 * Do not add any new functions to this class.
 */

class CBaseInsuranceAgents extends CEosPluralBase {

	/**
	 * @return CInsuranceAgent[]
	 */
	public static function fetchInsuranceAgents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceAgent', $objDatabase );
	}

	/**
	 * @return CInsuranceAgent
	 */
	public static function fetchInsuranceAgent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceAgent', $objDatabase );
	}

	public static function fetchInsuranceAgentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_agents', $objDatabase );
	}

	public static function fetchInsuranceAgentById( $intId, $objDatabase ) {
		return self::fetchInsuranceAgent( sprintf( 'SELECT * FROM insurance_agents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceAgentsByInsuranceAgentTypeId( $intInsuranceAgentTypeId, $objDatabase ) {
		return self::fetchInsuranceAgents( sprintf( 'SELECT * FROM insurance_agents WHERE insurance_agent_type_id = %d', ( int ) $intInsuranceAgentTypeId ), $objDatabase );
	}

	public static function fetchInsuranceAgentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchInsuranceAgents( sprintf( 'SELECT * FROM insurance_agents WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchInsuranceAgentsByUserId( $intUserId, $objDatabase ) {
		return self::fetchInsuranceAgents( sprintf( 'SELECT * FROM insurance_agents WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>