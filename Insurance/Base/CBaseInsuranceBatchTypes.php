<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceBatchTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceBatchType[]
	 */
	public static function fetchInsuranceBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceBatchType', $objDatabase );
	}

	/**
	 * @return CInsuranceBatchType
	 */
	public static function fetchInsuranceBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceBatchType', $objDatabase );
	}

	public static function fetchInsuranceBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_batch_types', $objDatabase );
	}

	public static function fetchInsuranceBatchTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceBatchType( sprintf( 'SELECT * FROM insurance_batch_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>