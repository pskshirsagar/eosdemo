<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceChargeTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceChargeTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceChargeType[]
	 */
	public static function fetchInsuranceChargeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceChargeType', $objDatabase );
	}

	/**
	 * @return CInsuranceChargeType
	 */
	public static function fetchInsuranceChargeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceChargeType', $objDatabase );
	}

	public static function fetchInsuranceChargeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_charge_types', $objDatabase );
	}

	public static function fetchInsuranceChargeTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceChargeType( sprintf( 'SELECT * FROM insurance_charge_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>