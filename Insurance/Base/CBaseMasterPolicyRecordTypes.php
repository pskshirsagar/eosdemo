<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyRecordTypes
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyRecordTypes extends CEosPluralBase {

	/**
	 * @return CMasterPolicyRecordType[]
	 */
	public static function fetchMasterPolicyRecordTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyRecordType::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyRecordType
	 */
	public static function fetchMasterPolicyRecordType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyRecordType::class, $objDatabase );
	}

	public static function fetchMasterPolicyRecordTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_record_types', $objDatabase );
	}

	public static function fetchMasterPolicyRecordTypeById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyRecordType( sprintf( 'SELECT * FROM master_policy_record_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>