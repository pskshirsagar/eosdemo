<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierProperties
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierProperties extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierProperty[]
	 */
	public static function fetchInsuranceCarrierProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCarrierProperty::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierProperty
	 */
	public static function fetchInsuranceCarrierProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCarrierProperty::class, $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_properties', $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertyById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperty( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE insurance_carrier_id = %d', $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE insurance_carrier_classification_type_id = %d', $intInsuranceCarrierClassificationTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE call_phone_number_id = %d', $intCallPhoneNumberId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByMinimumBillingFrequencyId( $intMinimumBillingFrequencyId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE minimum_billing_frequency_id = %d', $intMinimumBillingFrequencyId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByPropertyConstructionTypeId( $intPropertyConstructionTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE property_construction_type_id = %d', $intPropertyConstructionTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByWindstormConstructionCreditId( $intWindstormConstructionCreditId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE windstorm_construction_credit_id = %d', $intWindstormConstructionCreditId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByBuildingCodeGradeId( $intBuildingCodeGradeId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE building_code_grade_id = %d', $intBuildingCodeGradeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierPropertiesByAutoSprinklerCreditId( $intAutoSprinklerCreditId, $objDatabase ) {
		return self::fetchInsuranceCarrierProperties( sprintf( 'SELECT * FROM insurance_carrier_properties WHERE auto_sprinkler_credit_id = %d', $intAutoSprinklerCreditId ), $objDatabase );
	}

}
?>