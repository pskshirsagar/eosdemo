<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CPropertyStatisticHistories
 * Do not add any new functions to this class.
 */

class CBasePropertyStatisticHistories extends CEosPluralBase {

	/**
	 * @return CPropertyStatisticHistory[]
	 */
	public static function fetchPropertyStatisticHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyStatisticHistory', $objDatabase );
	}

	/**
	 * @return CPropertyStatisticHistory
	 */
	public static function fetchPropertyStatisticHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyStatisticHistory', $objDatabase );
	}

	public static function fetchPropertyStatisticHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_statistic_histories', $objDatabase );
	}

	public static function fetchPropertyStatisticHistoryById( $intId, $objDatabase ) {
		return self::fetchPropertyStatisticHistory( sprintf( 'SELECT * FROM property_statistic_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyStatisticHistoriesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyStatisticHistories( sprintf( 'SELECT * FROM property_statistic_histories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyStatisticHistoriesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyStatisticHistories( sprintf( 'SELECT * FROM property_statistic_histories WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>