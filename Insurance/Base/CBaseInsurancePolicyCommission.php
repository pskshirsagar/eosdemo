<?php

class CBaseInsurancePolicyCommission extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_commissions';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_intAgentCommissionId;
	protected $m_intManagerCommissionId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['agent_commission_id'] ) && $boolDirectSet ) $this->set( 'm_intAgentCommissionId', trim( $arrValues['agent_commission_id'] ) ); elseif( isset( $arrValues['agent_commission_id'] ) ) $this->setAgentCommissionId( $arrValues['agent_commission_id'] );
		if( isset( $arrValues['manager_commission_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerCommissionId', trim( $arrValues['manager_commission_id'] ) ); elseif( isset( $arrValues['manager_commission_id'] ) ) $this->setManagerCommissionId( $arrValues['manager_commission_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setAgentCommissionId( $intAgentCommissionId ) {
		$this->set( 'm_intAgentCommissionId', CStrings::strToIntDef( $intAgentCommissionId, NULL, false ) );
	}

	public function getAgentCommissionId() {
		return $this->m_intAgentCommissionId;
	}

	public function sqlAgentCommissionId() {
		return ( true == isset( $this->m_intAgentCommissionId ) ) ? ( string ) $this->m_intAgentCommissionId : 'NULL';
	}

	public function setManagerCommissionId( $intManagerCommissionId ) {
		$this->set( 'm_intManagerCommissionId', CStrings::strToIntDef( $intManagerCommissionId, NULL, false ) );
	}

	public function getManagerCommissionId() {
		return $this->m_intManagerCommissionId;
	}

	public function sqlManagerCommissionId() {
		return ( true == isset( $this->m_intManagerCommissionId ) ) ? ( string ) $this->m_intManagerCommissionId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, agent_commission_id, manager_commission_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlInsurancePolicyId() . ', ' .
						$this->sqlAgentCommissionId() . ', ' .
						$this->sqlManagerCommissionId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId(). ',' ; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_commission_id = ' . $this->sqlAgentCommissionId(). ',' ; } elseif( true == array_key_exists( 'AgentCommissionId', $this->getChangedColumns() ) ) { $strSql .= ' agent_commission_id = ' . $this->sqlAgentCommissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_commission_id = ' . $this->sqlManagerCommissionId() ; } elseif( true == array_key_exists( 'ManagerCommissionId', $this->getChangedColumns() ) ) { $strSql .= ' manager_commission_id = ' . $this->sqlManagerCommissionId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'agent_commission_id' => $this->getAgentCommissionId(),
			'manager_commission_id' => $this->getManagerCommissionId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>