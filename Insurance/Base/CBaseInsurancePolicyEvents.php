<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyEvents
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyEvents extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyEvent[]
	 */
	public static function fetchInsurancePolicyEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyEvent', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyEvent
	 */
	public static function fetchInsurancePolicyEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyEvent', $objDatabase );
	}

	public static function fetchInsurancePolicyEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_events', $objDatabase );
	}

	public static function fetchInsurancePolicyEventById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyEvent( sprintf( 'SELECT * FROM insurance_policy_events WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEventsByEventTypeId( $intEventTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyEvents( sprintf( 'SELECT * FROM insurance_policy_events WHERE event_type_id = %d', ( int ) $intEventTypeId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEventsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyEvents( sprintf( 'SELECT * FROM insurance_policy_events WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

}
?>