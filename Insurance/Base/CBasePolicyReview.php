<?php

class CBasePolicyReview extends CEosSingularBase {

	const TABLE_NAME = 'public.policy_reviews';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intLeaseId;
	protected $m_intInsuranceCarrierId;
	protected $m_intPolicyReviewStatusTypeId;
	protected $m_intPolicyReviewPriorityTypeId;
	protected $m_strCarrierPolicyNumber;
	protected $m_strInsuredName;
	protected $m_strOriginalReviewStartedOn;
	protected $m_strReviewStartedOn;
	protected $m_strReviewCompletedOn;
	protected $m_intReviewedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['policy_review_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyReviewStatusTypeId', trim( $arrValues['policy_review_status_type_id'] ) ); elseif( isset( $arrValues['policy_review_status_type_id'] ) ) $this->setPolicyReviewStatusTypeId( $arrValues['policy_review_status_type_id'] );
		if( isset( $arrValues['policy_review_priority_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyReviewPriorityTypeId', trim( $arrValues['policy_review_priority_type_id'] ) ); elseif( isset( $arrValues['policy_review_priority_type_id'] ) ) $this->setPolicyReviewPriorityTypeId( $arrValues['policy_review_priority_type_id'] );
		if( isset( $arrValues['carrier_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strCarrierPolicyNumber', trim( $arrValues['carrier_policy_number'] ) ); elseif( isset( $arrValues['carrier_policy_number'] ) ) $this->setCarrierPolicyNumber( $arrValues['carrier_policy_number'] );
		if( isset( $arrValues['insured_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuredName', trim( $arrValues['insured_name'] ) ); elseif( isset( $arrValues['insured_name'] ) ) $this->setInsuredName( $arrValues['insured_name'] );
		if( isset( $arrValues['original_review_started_on'] ) && $boolDirectSet ) $this->set( 'm_strOriginalReviewStartedOn', trim( $arrValues['original_review_started_on'] ) ); elseif( isset( $arrValues['original_review_started_on'] ) ) $this->setOriginalReviewStartedOn( $arrValues['original_review_started_on'] );
		if( isset( $arrValues['review_started_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewStartedOn', trim( $arrValues['review_started_on'] ) ); elseif( isset( $arrValues['review_started_on'] ) ) $this->setReviewStartedOn( $arrValues['review_started_on'] );
		if( isset( $arrValues['review_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewCompletedOn', trim( $arrValues['review_completed_on'] ) ); elseif( isset( $arrValues['review_completed_on'] ) ) $this->setReviewCompletedOn( $arrValues['review_completed_on'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setPolicyReviewStatusTypeId( $intPolicyReviewStatusTypeId ) {
		$this->set( 'm_intPolicyReviewStatusTypeId', CStrings::strToIntDef( $intPolicyReviewStatusTypeId, NULL, false ) );
	}

	public function getPolicyReviewStatusTypeId() {
		return $this->m_intPolicyReviewStatusTypeId;
	}

	public function sqlPolicyReviewStatusTypeId() {
		return ( true == isset( $this->m_intPolicyReviewStatusTypeId ) ) ? ( string ) $this->m_intPolicyReviewStatusTypeId : 'NULL';
	}

	public function setPolicyReviewPriorityTypeId( $intPolicyReviewPriorityTypeId ) {
		$this->set( 'm_intPolicyReviewPriorityTypeId', CStrings::strToIntDef( $intPolicyReviewPriorityTypeId, NULL, false ) );
	}

	public function getPolicyReviewPriorityTypeId() {
		return $this->m_intPolicyReviewPriorityTypeId;
	}

	public function sqlPolicyReviewPriorityTypeId() {
		return ( true == isset( $this->m_intPolicyReviewPriorityTypeId ) ) ? ( string ) $this->m_intPolicyReviewPriorityTypeId : 'NULL';
	}

	public function setCarrierPolicyNumber( $strCarrierPolicyNumber ) {
		$this->set( 'm_strCarrierPolicyNumber', CStrings::strTrimDef( $strCarrierPolicyNumber, 100, NULL, true ) );
	}

	public function getCarrierPolicyNumber() {
		return $this->m_strCarrierPolicyNumber;
	}

	public function sqlCarrierPolicyNumber() {
		return ( true == isset( $this->m_strCarrierPolicyNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCarrierPolicyNumber ) : '\'' . addslashes( $this->m_strCarrierPolicyNumber ) . '\'' ) : 'NULL';
	}

	public function setInsuredName( $strInsuredName ) {
		$this->set( 'm_strInsuredName', CStrings::strTrimDef( $strInsuredName, 200, NULL, true ) );
	}

	public function getInsuredName() {
		return $this->m_strInsuredName;
	}

	public function sqlInsuredName() {
		return ( true == isset( $this->m_strInsuredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInsuredName ) : '\'' . addslashes( $this->m_strInsuredName ) . '\'' ) : 'NULL';
	}

	public function setOriginalReviewStartedOn( $strOriginalReviewStartedOn ) {
		$this->set( 'm_strOriginalReviewStartedOn', CStrings::strTrimDef( $strOriginalReviewStartedOn, -1, NULL, true ) );
	}

	public function getOriginalReviewStartedOn() {
		return $this->m_strOriginalReviewStartedOn;
	}

	public function sqlOriginalReviewStartedOn() {
		return ( true == isset( $this->m_strOriginalReviewStartedOn ) ) ? '\'' . $this->m_strOriginalReviewStartedOn . '\'' : 'NULL';
	}

	public function setReviewStartedOn( $strReviewStartedOn ) {
		$this->set( 'm_strReviewStartedOn', CStrings::strTrimDef( $strReviewStartedOn, -1, NULL, true ) );
	}

	public function getReviewStartedOn() {
		return $this->m_strReviewStartedOn;
	}

	public function sqlReviewStartedOn() {
		return ( true == isset( $this->m_strReviewStartedOn ) ) ? '\'' . $this->m_strReviewStartedOn . '\'' : 'NULL';
	}

	public function setReviewCompletedOn( $strReviewCompletedOn ) {
		$this->set( 'm_strReviewCompletedOn', CStrings::strTrimDef( $strReviewCompletedOn, -1, NULL, true ) );
	}

	public function getReviewCompletedOn() {
		return $this->m_strReviewCompletedOn;
	}

	public function sqlReviewCompletedOn() {
		return ( true == isset( $this->m_strReviewCompletedOn ) ) ? '\'' . $this->m_strReviewCompletedOn . '\'' : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, lease_id, insurance_carrier_id, policy_review_status_type_id, policy_review_priority_type_id, carrier_policy_number, insured_name, original_review_started_on, review_started_on, review_completed_on, reviewed_by, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlLeaseId() . ', ' .
						$this->sqlInsuranceCarrierId() . ', ' .
						$this->sqlPolicyReviewStatusTypeId() . ', ' .
						$this->sqlPolicyReviewPriorityTypeId() . ', ' .
						$this->sqlCarrierPolicyNumber() . ', ' .
						$this->sqlInsuredName() . ', ' .
						$this->sqlOriginalReviewStartedOn() . ', ' .
						$this->sqlReviewStartedOn() . ', ' .
						$this->sqlReviewCompletedOn() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId(). ',' ; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId(). ',' ; } elseif( true == array_key_exists( 'InsuranceCarrierId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_id = ' . $this->sqlInsuranceCarrierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_review_status_type_id = ' . $this->sqlPolicyReviewStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PolicyReviewStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' policy_review_status_type_id = ' . $this->sqlPolicyReviewStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_review_priority_type_id = ' . $this->sqlPolicyReviewPriorityTypeId(). ',' ; } elseif( true == array_key_exists( 'PolicyReviewPriorityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' policy_review_priority_type_id = ' . $this->sqlPolicyReviewPriorityTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'CarrierPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' carrier_policy_number = ' . $this->sqlCarrierPolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName(). ',' ; } elseif( true == array_key_exists( 'InsuredName', $this->getChangedColumns() ) ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_review_started_on = ' . $this->sqlOriginalReviewStartedOn(). ',' ; } elseif( true == array_key_exists( 'OriginalReviewStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' original_review_started_on = ' . $this->sqlOriginalReviewStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_started_on = ' . $this->sqlReviewStartedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' review_started_on = ' . $this->sqlReviewStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_completed_on = ' . $this->sqlReviewCompletedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' review_completed_on = ' . $this->sqlReviewCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'lease_id' => $this->getLeaseId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'policy_review_status_type_id' => $this->getPolicyReviewStatusTypeId(),
			'policy_review_priority_type_id' => $this->getPolicyReviewPriorityTypeId(),
			'carrier_policy_number' => $this->getCarrierPolicyNumber(),
			'insured_name' => $this->getInsuredName(),
			'original_review_started_on' => $this->getOriginalReviewStartedOn(),
			'review_started_on' => $this->getReviewStartedOn(),
			'review_completed_on' => $this->getReviewCompletedOn(),
			'reviewed_by' => $this->getReviewedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>