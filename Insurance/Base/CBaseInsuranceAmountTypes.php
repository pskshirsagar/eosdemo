<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceAmountTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceAmountTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceAmountType[]
	 */
	public static function fetchInsuranceAmountTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceAmountType', $objDatabase );
	}

	/**
	 * @return CInsuranceAmountType
	 */
	public static function fetchInsuranceAmountType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceAmountType', $objDatabase );
	}

	public static function fetchInsuranceAmountTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_amount_types', $objDatabase );
	}

	public static function fetchInsuranceAmountTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceAmountType( sprintf( 'SELECT * FROM insurance_amount_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>