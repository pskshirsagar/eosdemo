<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceEmailNotificationTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceEmailNotificationTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceEmailNotificationType[]
	 */
	public static function fetchInsuranceEmailNotificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceEmailNotificationType', $objDatabase );
	}

	/**
	 * @return CInsuranceEmailNotificationType
	 */
	public static function fetchInsuranceEmailNotificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceEmailNotificationType', $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_email_notification_types', $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceEmailNotificationType( sprintf( 'SELECT * FROM insurance_email_notification_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>