<?php

class CBaseInsurancePolicyEndorsement extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_policy_endorsements';

	protected $m_intId;
	protected $m_intInsurancePolicyId;
	protected $m_intInsurancePolicyCoverageDetailId;
	protected $m_intInsuranceCarrierEndorsementId;
	protected $m_fltPremiumAmount;
	protected $m_strEffectiveOn;
	protected $m_strExpiresOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltPremiumAmount = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['insurance_policy_coverage_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyCoverageDetailId', trim( $arrValues['insurance_policy_coverage_detail_id'] ) ); elseif( isset( $arrValues['insurance_policy_coverage_detail_id'] ) ) $this->setInsurancePolicyCoverageDetailId( $arrValues['insurance_policy_coverage_detail_id'] );
		if( isset( $arrValues['insurance_carrier_endorsement_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierEndorsementId', trim( $arrValues['insurance_carrier_endorsement_id'] ) ); elseif( isset( $arrValues['insurance_carrier_endorsement_id'] ) ) $this->setInsuranceCarrierEndorsementId( $arrValues['insurance_carrier_endorsement_id'] );
		if( isset( $arrValues['premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumAmount', trim( $arrValues['premium_amount'] ) ); elseif( isset( $arrValues['premium_amount'] ) ) $this->setPremiumAmount( $arrValues['premium_amount'] );
		if( isset( $arrValues['effective_on'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveOn', trim( $arrValues['effective_on'] ) ); elseif( isset( $arrValues['effective_on'] ) ) $this->setEffectiveOn( $arrValues['effective_on'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setInsurancePolicyCoverageDetailId( $intInsurancePolicyCoverageDetailId ) {
		$this->set( 'm_intInsurancePolicyCoverageDetailId', CStrings::strToIntDef( $intInsurancePolicyCoverageDetailId, NULL, false ) );
	}

	public function getInsurancePolicyCoverageDetailId() {
		return $this->m_intInsurancePolicyCoverageDetailId;
	}

	public function sqlInsurancePolicyCoverageDetailId() {
		return ( true == isset( $this->m_intInsurancePolicyCoverageDetailId ) ) ? ( string ) $this->m_intInsurancePolicyCoverageDetailId : 'NULL';
	}

	public function setInsuranceCarrierEndorsementId( $intInsuranceCarrierEndorsementId ) {
		$this->set( 'm_intInsuranceCarrierEndorsementId', CStrings::strToIntDef( $intInsuranceCarrierEndorsementId, NULL, false ) );
	}

	public function getInsuranceCarrierEndorsementId() {
		return $this->m_intInsuranceCarrierEndorsementId;
	}

	public function sqlInsuranceCarrierEndorsementId() {
		return ( true == isset( $this->m_intInsuranceCarrierEndorsementId ) ) ? ( string ) $this->m_intInsuranceCarrierEndorsementId : 'NULL';
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->set( 'm_fltPremiumAmount', CStrings::strToFloatDef( $fltPremiumAmount, NULL, false, 2 ) );
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function sqlPremiumAmount() {
		return ( true == isset( $this->m_fltPremiumAmount ) ) ? ( string ) $this->m_fltPremiumAmount : 'NULL::numeric';
	}

	public function setEffectiveOn( $strEffectiveOn ) {
		$this->set( 'm_strEffectiveOn', CStrings::strTrimDef( $strEffectiveOn, -1, NULL, true ) );
	}

	public function getEffectiveOn() {
		return $this->m_strEffectiveOn;
	}

	public function sqlEffectiveOn() {
		return ( true == isset( $this->m_strEffectiveOn ) ) ? '\'' . $this->m_strEffectiveOn . '\'' : 'NOW()';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, insurance_policy_id, insurance_policy_coverage_detail_id, insurance_carrier_endorsement_id, premium_amount, effective_on, expires_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlInsurancePolicyCoverageDetailId() . ', ' .
 						$this->sqlInsuranceCarrierEndorsementId() . ', ' .
 						$this->sqlPremiumAmount() . ', ' .
 						$this->sqlEffectiveOn() . ', ' .
 						$this->sqlExpiresOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_coverage_detail_id = ' . $this->sqlInsurancePolicyCoverageDetailId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyCoverageDetailId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_coverage_detail_id = ' . $this->sqlInsurancePolicyCoverageDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_carrier_endorsement_id = ' . $this->sqlInsuranceCarrierEndorsementId() . ','; } elseif( true == array_key_exists( 'InsuranceCarrierEndorsementId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_carrier_endorsement_id = ' . $this->sqlInsuranceCarrierEndorsementId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; } elseif( true == array_key_exists( 'PremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; } elseif( true == array_key_exists( 'EffectiveOn', $this->getChangedColumns() ) ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; } elseif( true == array_key_exists( 'ExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'insurance_policy_coverage_detail_id' => $this->getInsurancePolicyCoverageDetailId(),
			'insurance_carrier_endorsement_id' => $this->getInsuranceCarrierEndorsementId(),
			'premium_amount' => $this->getPremiumAmount(),
			'effective_on' => $this->getEffectiveOn(),
			'expires_on' => $this->getExpiresOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>