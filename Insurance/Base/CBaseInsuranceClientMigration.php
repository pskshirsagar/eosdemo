<?php

class CBaseInsuranceClientMigration extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_client_migrations';

	protected $m_intId;
	protected $m_intSourceCid;
	protected $m_intDestinationCid;
	protected $m_intSourcePropertyId;
	protected $m_intDestinationPropertyId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strRiPolicyMigrationCompletedOn;
	protected $m_strCustomPolicyMigrationCompletedOn;
	protected $m_strMasterPolicyMigrationCompletedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['source_cid'] ) && $boolDirectSet ) $this->set( 'm_intSourceCid', trim( $arrValues['source_cid'] ) ); elseif( isset( $arrValues['source_cid'] ) ) $this->setSourceCid( $arrValues['source_cid'] );
		if( isset( $arrValues['destination_cid'] ) && $boolDirectSet ) $this->set( 'm_intDestinationCid', trim( $arrValues['destination_cid'] ) ); elseif( isset( $arrValues['destination_cid'] ) ) $this->setDestinationCid( $arrValues['destination_cid'] );
		if( isset( $arrValues['source_property_id'] ) && $boolDirectSet ) $this->set( 'm_intSourcePropertyId', trim( $arrValues['source_property_id'] ) ); elseif( isset( $arrValues['source_property_id'] ) ) $this->setSourcePropertyId( $arrValues['source_property_id'] );
		if( isset( $arrValues['destination_property_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationPropertyId', trim( $arrValues['destination_property_id'] ) ); elseif( isset( $arrValues['destination_property_id'] ) ) $this->setDestinationPropertyId( $arrValues['destination_property_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ri_policy_migration_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strRiPolicyMigrationCompletedOn', trim( $arrValues['ri_policy_migration_completed_on'] ) ); elseif( isset( $arrValues['ri_policy_migration_completed_on'] ) ) $this->setRiPolicyMigrationCompletedOn( $arrValues['ri_policy_migration_completed_on'] );
		if( isset( $arrValues['custom_policy_migration_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCustomPolicyMigrationCompletedOn', trim( $arrValues['custom_policy_migration_completed_on'] ) ); elseif( isset( $arrValues['custom_policy_migration_completed_on'] ) ) $this->setCustomPolicyMigrationCompletedOn( $arrValues['custom_policy_migration_completed_on'] );
		if( isset( $arrValues['master_policy_migration_completed_on'] ) && $boolDirectSet ) $this->set( 'm_strMasterPolicyMigrationCompletedOn', trim( $arrValues['master_policy_migration_completed_on'] ) ); elseif( isset( $arrValues['master_policy_migration_completed_on'] ) ) $this->setMasterPolicyMigrationCompletedOn( $arrValues['master_policy_migration_completed_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSourceCid( $intSourceCid ) {
		$this->set( 'm_intSourceCid', CStrings::strToIntDef( $intSourceCid, NULL, false ) );
	}

	public function getSourceCid() {
		return $this->m_intSourceCid;
	}

	public function sqlSourceCid() {
		return ( true == isset( $this->m_intSourceCid ) ) ? ( string ) $this->m_intSourceCid : 'NULL';
	}

	public function setDestinationCid( $intDestinationCid ) {
		$this->set( 'm_intDestinationCid', CStrings::strToIntDef( $intDestinationCid, NULL, false ) );
	}

	public function getDestinationCid() {
		return $this->m_intDestinationCid;
	}

	public function sqlDestinationCid() {
		return ( true == isset( $this->m_intDestinationCid ) ) ? ( string ) $this->m_intDestinationCid : 'NULL';
	}

	public function setSourcePropertyId( $intSourcePropertyId ) {
		$this->set( 'm_intSourcePropertyId', CStrings::strToIntDef( $intSourcePropertyId, NULL, false ) );
	}

	public function getSourcePropertyId() {
		return $this->m_intSourcePropertyId;
	}

	public function sqlSourcePropertyId() {
		return ( true == isset( $this->m_intSourcePropertyId ) ) ? ( string ) $this->m_intSourcePropertyId : 'NULL';
	}

	public function setDestinationPropertyId( $intDestinationPropertyId ) {
		$this->set( 'm_intDestinationPropertyId', CStrings::strToIntDef( $intDestinationPropertyId, NULL, false ) );
	}

	public function getDestinationPropertyId() {
		return $this->m_intDestinationPropertyId;
	}

	public function sqlDestinationPropertyId() {
		return ( true == isset( $this->m_intDestinationPropertyId ) ) ? ( string ) $this->m_intDestinationPropertyId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRiPolicyMigrationCompletedOn( $strRiPolicyMigrationCompletedOn ) {
		$this->set( 'm_strRiPolicyMigrationCompletedOn', CStrings::strTrimDef( $strRiPolicyMigrationCompletedOn, -1, NULL, true ) );
	}

	public function getRiPolicyMigrationCompletedOn() {
		return $this->m_strRiPolicyMigrationCompletedOn;
	}

	public function sqlRiPolicyMigrationCompletedOn() {
		return ( true == isset( $this->m_strRiPolicyMigrationCompletedOn ) ) ? '\'' . $this->m_strRiPolicyMigrationCompletedOn . '\'' : 'NULL';
	}

	public function setCustomPolicyMigrationCompletedOn( $strCustomPolicyMigrationCompletedOn ) {
		$this->set( 'm_strCustomPolicyMigrationCompletedOn', CStrings::strTrimDef( $strCustomPolicyMigrationCompletedOn, -1, NULL, true ) );
	}

	public function getCustomPolicyMigrationCompletedOn() {
		return $this->m_strCustomPolicyMigrationCompletedOn;
	}

	public function sqlCustomPolicyMigrationCompletedOn() {
		return ( true == isset( $this->m_strCustomPolicyMigrationCompletedOn ) ) ? '\'' . $this->m_strCustomPolicyMigrationCompletedOn . '\'' : 'NULL';
	}

	public function setMasterPolicyMigrationCompletedOn( $strMasterPolicyMigrationCompletedOn ) {
		$this->set( 'm_strMasterPolicyMigrationCompletedOn', CStrings::strTrimDef( $strMasterPolicyMigrationCompletedOn, -1, NULL, true ) );
	}

	public function getMasterPolicyMigrationCompletedOn() {
		return $this->m_strMasterPolicyMigrationCompletedOn;
	}

	public function sqlMasterPolicyMigrationCompletedOn() {
		return ( true == isset( $this->m_strMasterPolicyMigrationCompletedOn ) ) ? '\'' . $this->m_strMasterPolicyMigrationCompletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, source_cid, destination_cid, source_property_id, destination_property_id, created_by, created_on, ri_policy_migration_completed_on, custom_policy_migration_completed_on, master_policy_migration_completed_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSourceCid() . ', ' .
						$this->sqlDestinationCid() . ', ' .
						$this->sqlSourcePropertyId() . ', ' .
						$this->sqlDestinationPropertyId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRiPolicyMigrationCompletedOn() . ', ' .
						$this->sqlCustomPolicyMigrationCompletedOn() . ', ' .
						$this->sqlMasterPolicyMigrationCompletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid() . ','; } elseif( true == array_key_exists( 'SourceCid', $this->getChangedColumns() ) ) { $strSql .= ' source_cid = ' . $this->sqlSourceCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid() . ','; } elseif( true == array_key_exists( 'DestinationCid', $this->getChangedColumns() ) ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; } elseif( true == array_key_exists( 'SourcePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; } elseif( true == array_key_exists( 'DestinationPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_policy_migration_completed_on = ' . $this->sqlRiPolicyMigrationCompletedOn() . ','; } elseif( true == array_key_exists( 'RiPolicyMigrationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' ri_policy_migration_completed_on = ' . $this->sqlRiPolicyMigrationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_policy_migration_completed_on = ' . $this->sqlCustomPolicyMigrationCompletedOn() . ','; } elseif( true == array_key_exists( 'CustomPolicyMigrationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' custom_policy_migration_completed_on = ' . $this->sqlCustomPolicyMigrationCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' master_policy_migration_completed_on = ' . $this->sqlMasterPolicyMigrationCompletedOn() . ','; } elseif( true == array_key_exists( 'MasterPolicyMigrationCompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' master_policy_migration_completed_on = ' . $this->sqlMasterPolicyMigrationCompletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'source_cid' => $this->getSourceCid(),
			'destination_cid' => $this->getDestinationCid(),
			'source_property_id' => $this->getSourcePropertyId(),
			'destination_property_id' => $this->getDestinationPropertyId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ri_policy_migration_completed_on' => $this->getRiPolicyMigrationCompletedOn(),
			'custom_policy_migration_completed_on' => $this->getCustomPolicyMigrationCompletedOn(),
			'master_policy_migration_completed_on' => $this->getMasterPolicyMigrationCompletedOn()
		);
	}

}
?>