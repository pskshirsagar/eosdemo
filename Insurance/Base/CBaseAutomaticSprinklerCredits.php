<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CAutomaticSprinklerCredits
 * Do not add any new functions to this class.
 */

class CBaseAutomaticSprinklerCredits extends CEosPluralBase {

	/**
	 * @return CAutomaticSprinklerCredit[]
	 */
	public static function fetchAutomaticSprinklerCredits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAutomaticSprinklerCredit::class, $objDatabase );
	}

	/**
	 * @return CAutomaticSprinklerCredit
	 */
	public static function fetchAutomaticSprinklerCredit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAutomaticSprinklerCredit::class, $objDatabase );
	}

	public static function fetchAutomaticSprinklerCreditCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'automatic_sprinkler_credits', $objDatabase );
	}

	public static function fetchAutomaticSprinklerCreditById( $intId, $objDatabase ) {
		return self::fetchAutomaticSprinklerCredit( sprintf( 'SELECT * FROM automatic_sprinkler_credits WHERE id = %d', $intId ), $objDatabase );
	}

}
?>