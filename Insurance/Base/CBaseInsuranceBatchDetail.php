<?php

class CBaseInsuranceBatchDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_batch_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intInsuranceBatchId;
	protected $m_intInsurancePolicyId;
	protected $m_intPropertyAccountId;
	protected $m_intTransactionId;
	protected $m_strInsurancePolicyNumber;
	protected $m_strInsuredName;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltPremiumAmount;
	protected $m_fltInvoiceAmount;
	protected $m_fltCommissionAmount;
	protected $m_intReconciledTypeId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['insurance_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceBatchId', trim( $arrValues['insurance_batch_id'] ) ); elseif( isset( $arrValues['insurance_batch_id'] ) ) $this->setInsuranceBatchId( $arrValues['insurance_batch_id'] );
		if( isset( $arrValues['insurance_policy_id'] ) && $boolDirectSet ) $this->set( 'm_intInsurancePolicyId', trim( $arrValues['insurance_policy_id'] ) ); elseif( isset( $arrValues['insurance_policy_id'] ) ) $this->setInsurancePolicyId( $arrValues['insurance_policy_id'] );
		if( isset( $arrValues['property_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyAccountId', trim( $arrValues['property_account_id'] ) ); elseif( isset( $arrValues['property_account_id'] ) ) $this->setPropertyAccountId( $arrValues['property_account_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['insurance_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strInsurancePolicyNumber', trim( stripcslashes( $arrValues['insurance_policy_number'] ) ) ); elseif( isset( $arrValues['insurance_policy_number'] ) ) $this->setInsurancePolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insurance_policy_number'] ) : $arrValues['insurance_policy_number'] );
		if( isset( $arrValues['insured_name'] ) && $boolDirectSet ) $this->set( 'm_strInsuredName', trim( stripcslashes( $arrValues['insured_name'] ) ) ); elseif( isset( $arrValues['insured_name'] ) ) $this->setInsuredName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['insured_name'] ) : $arrValues['insured_name'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['premium_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPremiumAmount', trim( $arrValues['premium_amount'] ) ); elseif( isset( $arrValues['premium_amount'] ) ) $this->setPremiumAmount( $arrValues['premium_amount'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['reconciled_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReconciledTypeId', trim( $arrValues['reconciled_type_id'] ) ); elseif( isset( $arrValues['reconciled_type_id'] ) ) $this->setReconciledTypeId( $arrValues['reconciled_type_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setInsuranceBatchId( $intInsuranceBatchId ) {
		$this->set( 'm_intInsuranceBatchId', CStrings::strToIntDef( $intInsuranceBatchId, NULL, false ) );
	}

	public function getInsuranceBatchId() {
		return $this->m_intInsuranceBatchId;
	}

	public function sqlInsuranceBatchId() {
		return ( true == isset( $this->m_intInsuranceBatchId ) ) ? ( string ) $this->m_intInsuranceBatchId : 'NULL';
	}

	public function setInsurancePolicyId( $intInsurancePolicyId ) {
		$this->set( 'm_intInsurancePolicyId', CStrings::strToIntDef( $intInsurancePolicyId, NULL, false ) );
	}

	public function getInsurancePolicyId() {
		return $this->m_intInsurancePolicyId;
	}

	public function sqlInsurancePolicyId() {
		return ( true == isset( $this->m_intInsurancePolicyId ) ) ? ( string ) $this->m_intInsurancePolicyId : 'NULL';
	}

	public function setPropertyAccountId( $intPropertyAccountId ) {
		$this->set( 'm_intPropertyAccountId', CStrings::strToIntDef( $intPropertyAccountId, NULL, false ) );
	}

	public function getPropertyAccountId() {
		return $this->m_intPropertyAccountId;
	}

	public function sqlPropertyAccountId() {
		return ( true == isset( $this->m_intPropertyAccountId ) ) ? ( string ) $this->m_intPropertyAccountId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setInsurancePolicyNumber( $strInsurancePolicyNumber ) {
		$this->set( 'm_strInsurancePolicyNumber', CStrings::strTrimDef( $strInsurancePolicyNumber, 64, NULL, true ) );
	}

	public function getInsurancePolicyNumber() {
		return $this->m_strInsurancePolicyNumber;
	}

	public function sqlInsurancePolicyNumber() {
		return ( true == isset( $this->m_strInsurancePolicyNumber ) ) ? '\'' . addslashes( $this->m_strInsurancePolicyNumber ) . '\'' : 'NULL';
	}

	public function setInsuredName( $strInsuredName ) {
		$this->set( 'm_strInsuredName', CStrings::strTrimDef( $strInsuredName, 200, NULL, true ) );
	}

	public function getInsuredName() {
		return $this->m_strInsuredName;
	}

	public function sqlInsuredName() {
		return ( true == isset( $this->m_strInsuredName ) ) ? '\'' . addslashes( $this->m_strInsuredName ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->set( 'm_fltPremiumAmount', CStrings::strToFloatDef( $fltPremiumAmount, NULL, false, 2 ) );
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function sqlPremiumAmount() {
		return ( true == isset( $this->m_fltPremiumAmount ) ) ? ( string ) $this->m_fltPremiumAmount : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 2 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : 'NULL';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 2 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setReconciledTypeId( $intReconciledTypeId ) {
		$this->set( 'm_intReconciledTypeId', CStrings::strToIntDef( $intReconciledTypeId, NULL, false ) );
	}

	public function getReconciledTypeId() {
		return $this->m_intReconciledTypeId;
	}

	public function sqlReconciledTypeId() {
		return ( true == isset( $this->m_intReconciledTypeId ) ) ? ( string ) $this->m_intReconciledTypeId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, insurance_batch_id, insurance_policy_id, property_account_id, transaction_id, insurance_policy_number, insured_name, start_date, end_date, premium_amount, invoice_amount, commission_amount, reconciled_type_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlInsuranceBatchId() . ', ' .
 						$this->sqlInsurancePolicyId() . ', ' .
 						$this->sqlPropertyAccountId() . ', ' .
 						$this->sqlTransactionId() . ', ' .
 						$this->sqlInsurancePolicyNumber() . ', ' .
 						$this->sqlInsuredName() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlPremiumAmount() . ', ' .
 						$this->sqlInvoiceAmount() . ', ' .
 						$this->sqlCommissionAmount() . ', ' .
 						$this->sqlReconciledTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_batch_id = ' . $this->sqlInsuranceBatchId() . ','; } elseif( true == array_key_exists( 'InsuranceBatchId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_batch_id = ' . $this->sqlInsuranceBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; } elseif( true == array_key_exists( 'InsurancePolicyId', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_id = ' . $this->sqlInsurancePolicyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_account_id = ' . $this->sqlPropertyAccountId() . ','; } elseif( true == array_key_exists( 'PropertyAccountId', $this->getChangedColumns() ) ) { $strSql .= ' property_account_id = ' . $this->sqlPropertyAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber() . ','; } elseif( true == array_key_exists( 'InsurancePolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' insurance_policy_number = ' . $this->sqlInsurancePolicyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; } elseif( true == array_key_exists( 'InsuredName', $this->getChangedColumns() ) ) { $strSql .= ' insured_name = ' . $this->sqlInsuredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; } elseif( true == array_key_exists( 'PremiumAmount', $this->getChangedColumns() ) ) { $strSql .= ' premium_amount = ' . $this->sqlPremiumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reconciled_type_id = ' . $this->sqlReconciledTypeId() . ','; } elseif( true == array_key_exists( 'ReconciledTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reconciled_type_id = ' . $this->sqlReconciledTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'insurance_batch_id' => $this->getInsuranceBatchId(),
			'insurance_policy_id' => $this->getInsurancePolicyId(),
			'property_account_id' => $this->getPropertyAccountId(),
			'transaction_id' => $this->getTransactionId(),
			'insurance_policy_number' => $this->getInsurancePolicyNumber(),
			'insured_name' => $this->getInsuredName(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'premium_amount' => $this->getPremiumAmount(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'commission_amount' => $this->getCommissionAmount(),
			'reconciled_type_id' => $this->getReconciledTypeId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>