<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CPropertyStatistics
 * Do not add any new functions to this class.
 */

class CBasePropertyStatistics extends CEosPluralBase {

	/**
	 * @return CPropertyStatistic[]
	 */
	public static function fetchPropertyStatistics( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyStatistic', $objDatabase );
	}

	/**
	 * @return CPropertyStatistic
	 */
	public static function fetchPropertyStatistic( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyStatistic', $objDatabase );
	}

	public static function fetchPropertyStatisticCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_statistics', $objDatabase );
	}

	public static function fetchPropertyStatisticById( $intId, $objDatabase ) {
		return self::fetchPropertyStatistic( sprintf( 'SELECT * FROM property_statistics WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyStatisticsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyStatistics( sprintf( 'SELECT * FROM property_statistics WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyStatisticsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyStatistics( sprintf( 'SELECT * FROM property_statistics WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>