<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyEventTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyEventTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyEventType[]
	 */
	public static function fetchInsurancePolicyEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyEventType', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyEventType
	 */
	public static function fetchInsurancePolicyEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyEventType', $objDatabase );
	}

	public static function fetchInsurancePolicyEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_event_types', $objDatabase );
	}

	public static function fetchInsurancePolicyEventTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyEventType( sprintf( 'SELECT * FROM insurance_policy_event_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>