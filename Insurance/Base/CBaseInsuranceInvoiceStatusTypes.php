<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInvoiceStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInvoiceStatusTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceInvoiceStatusType[]
	 */
	public static function fetchInsuranceInvoiceStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceInvoiceStatusType', $objDatabase );
	}

	/**
	 * @return CInsuranceInvoiceStatusType
	 */
	public static function fetchInsuranceInvoiceStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceInvoiceStatusType', $objDatabase );
	}

	public static function fetchInsuranceInvoiceStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_invoice_status_types', $objDatabase );
	}

	public static function fetchInsuranceInvoiceStatusTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceInvoiceStatusType( sprintf( 'SELECT * FROM insurance_invoice_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>