<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyQuotes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyQuotes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyQuote[]
	 */
	public static function fetchInsurancePolicyQuotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePolicyQuote::class, $objDatabase );
	}

	/**
	 * @return CInsurancePolicyQuote
	 */
	public static function fetchInsurancePolicyQuote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePolicyQuote::class, $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_quotes', $objDatabase );
	}

	public static function fetchInsurancePolicyQuoteById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyQuote( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByCid( $intCid, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE entity_id = %d', $intEntityId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE insurance_policy_id = %d', $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByInsurancePropertyId( $intInsurancePropertyId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE insurance_property_id = %d', $intInsurancePropertyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByLeadSourceTypeId( $intLeadSourceTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE lead_source_type_id = %d', $intLeadSourceTypeId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByOriginalLeadSourceTypeId( $intOriginalLeadSourceTypeId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE original_lead_source_type_id = %d', $intOriginalLeadSourceTypeId ), $objDatabase );
	}

	public static function fetchInsurancePolicyQuotesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsurancePolicyQuotes( sprintf( 'SELECT * FROM insurance_policy_quotes WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

}
?>