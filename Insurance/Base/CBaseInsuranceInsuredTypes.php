<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInsuredTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInsuredTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceInsuredType[]
	 */
	public static function fetchInsuranceInsuredTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceInsuredType', $objDatabase );
	}

	/**
	 * @return CInsuranceInsuredType
	 */
	public static function fetchInsuranceInsuredType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceInsuredType', $objDatabase );
	}

	public static function fetchInsuranceInsuredTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_insured_types', $objDatabase );
	}

	public static function fetchInsuranceInsuredTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceInsuredType( sprintf( 'SELECT * FROM insurance_insured_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>