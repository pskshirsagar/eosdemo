<?php

class CBaseInsuranceProperty extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.insurance_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallPhoneNumberId;
	protected $m_intAutoSprinklerCreditId;
	protected $m_intPropertyConstructionTypeId;
	protected $m_intWindstormConstructionCreditId;
	protected $m_intUnitCount;
	protected $m_intBcegCodeId;
	protected $m_strPpcCode;
	protected $m_strName;
	protected $m_strStreetAddress;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strCountyCode;
	protected $m_strPostalCode;
	protected $m_strCountyName;
	protected $m_strLongitude;
	protected $m_strLatitude;
	protected $m_strConstructionYear;
	protected $m_strProfessionallyManagedYear;
	protected $m_boolIsActive;
	protected $m_boolIsDisabled;
	protected $m_boolIsProfessionallyManaged;
	protected $m_boolIsGatedCommunity;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = true;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsProfessionallyManaged = false;
		$this->m_boolIsGatedCommunity = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['auto_sprinkler_credit_id'] ) && $boolDirectSet ) $this->set( 'm_intAutoSprinklerCreditId', trim( $arrValues['auto_sprinkler_credit_id'] ) ); elseif( isset( $arrValues['auto_sprinkler_credit_id'] ) ) $this->setAutoSprinklerCreditId( $arrValues['auto_sprinkler_credit_id'] );
		if( isset( $arrValues['property_construction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyConstructionTypeId', trim( $arrValues['property_construction_type_id'] ) ); elseif( isset( $arrValues['property_construction_type_id'] ) ) $this->setPropertyConstructionTypeId( $arrValues['property_construction_type_id'] );
		if( isset( $arrValues['windstorm_construction_credit_id'] ) && $boolDirectSet ) $this->set( 'm_intWindstormConstructionCreditId', trim( $arrValues['windstorm_construction_credit_id'] ) ); elseif( isset( $arrValues['windstorm_construction_credit_id'] ) ) $this->setWindstormConstructionCreditId( $arrValues['windstorm_construction_credit_id'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['bceg_code_id'] ) && $boolDirectSet ) $this->set( 'm_intBcegCodeId', trim( $arrValues['bceg_code_id'] ) ); elseif( isset( $arrValues['bceg_code_id'] ) ) $this->setBcegCodeId( $arrValues['bceg_code_id'] );
		if( isset( $arrValues['ppc_code'] ) && $boolDirectSet ) $this->set( 'm_strPpcCode', trim( $arrValues['ppc_code'] ) ); elseif( isset( $arrValues['ppc_code'] ) ) $this->setPpcCode( $arrValues['ppc_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['street_address'] ) && $boolDirectSet ) $this->set( 'm_strStreetAddress', trim( $arrValues['street_address'] ) ); elseif( isset( $arrValues['street_address'] ) ) $this->setStreetAddress( $arrValues['street_address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['county_code'] ) && $boolDirectSet ) $this->set( 'm_strCountyCode', trim( $arrValues['county_code'] ) ); elseif( isset( $arrValues['county_code'] ) ) $this->setCountyCode( $arrValues['county_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['county_name'] ) && $boolDirectSet ) $this->set( 'm_strCountyName', trim( $arrValues['county_name'] ) ); elseif( isset( $arrValues['county_name'] ) ) $this->setCountyName( $arrValues['county_name'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( $arrValues['longitude'] ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( $arrValues['latitude'] ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( $arrValues['latitude'] );
		if( isset( $arrValues['construction_year'] ) && $boolDirectSet ) $this->set( 'm_strConstructionYear', trim( $arrValues['construction_year'] ) ); elseif( isset( $arrValues['construction_year'] ) ) $this->setConstructionYear( $arrValues['construction_year'] );
		if( isset( $arrValues['professionally_managed_year'] ) && $boolDirectSet ) $this->set( 'm_strProfessionallyManagedYear', trim( $arrValues['professionally_managed_year'] ) ); elseif( isset( $arrValues['professionally_managed_year'] ) ) $this->setProfessionallyManagedYear( $arrValues['professionally_managed_year'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_professionally_managed'] ) && $boolDirectSet ) $this->set( 'm_boolIsProfessionallyManaged', trim( stripcslashes( $arrValues['is_professionally_managed'] ) ) ); elseif( isset( $arrValues['is_professionally_managed'] ) ) $this->setIsProfessionallyManaged( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_professionally_managed'] ) : $arrValues['is_professionally_managed'] );
		if( isset( $arrValues['is_gated_community'] ) && $boolDirectSet ) $this->set( 'm_boolIsGatedCommunity', trim( stripcslashes( $arrValues['is_gated_community'] ) ) ); elseif( isset( $arrValues['is_gated_community'] ) ) $this->setIsGatedCommunity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gated_community'] ) : $arrValues['is_gated_community'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setAutoSprinklerCreditId( $intAutoSprinklerCreditId ) {
		$this->set( 'm_intAutoSprinklerCreditId', CStrings::strToIntDef( $intAutoSprinklerCreditId, NULL, false ) );
	}

	public function getAutoSprinklerCreditId() {
		return $this->m_intAutoSprinklerCreditId;
	}

	public function sqlAutoSprinklerCreditId() {
		return ( true == isset( $this->m_intAutoSprinklerCreditId ) ) ? ( string ) $this->m_intAutoSprinklerCreditId : 'NULL';
	}

	public function setPropertyConstructionTypeId( $intPropertyConstructionTypeId ) {
		$this->set( 'm_intPropertyConstructionTypeId', CStrings::strToIntDef( $intPropertyConstructionTypeId, NULL, false ) );
	}

	public function getPropertyConstructionTypeId() {
		return $this->m_intPropertyConstructionTypeId;
	}

	public function sqlPropertyConstructionTypeId() {
		return ( true == isset( $this->m_intPropertyConstructionTypeId ) ) ? ( string ) $this->m_intPropertyConstructionTypeId : 'NULL';
	}

	public function setWindstormConstructionCreditId( $intWindstormConstructionCreditId ) {
		$this->set( 'm_intWindstormConstructionCreditId', CStrings::strToIntDef( $intWindstormConstructionCreditId, NULL, false ) );
	}

	public function getWindstormConstructionCreditId() {
		return $this->m_intWindstormConstructionCreditId;
	}

	public function sqlWindstormConstructionCreditId() {
		return ( true == isset( $this->m_intWindstormConstructionCreditId ) ) ? ( string ) $this->m_intWindstormConstructionCreditId : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : 'NULL';
	}

	public function setBcegCodeId( $intBcegCodeId ) {
		$this->set( 'm_intBcegCodeId', CStrings::strToIntDef( $intBcegCodeId, NULL, false ) );
	}

	public function getBcegCodeId() {
		return $this->m_intBcegCodeId;
	}

	public function sqlBcegCodeId() {
		return ( true == isset( $this->m_intBcegCodeId ) ) ? ( string ) $this->m_intBcegCodeId : 'NULL';
	}

	public function setPpcCode( $strPpcCode ) {
		$this->set( 'm_strPpcCode', CStrings::strTrimDef( $strPpcCode, 20, NULL, true ) );
	}

	public function getPpcCode() {
		return $this->m_strPpcCode;
	}

	public function sqlPpcCode() {
		return ( true == isset( $this->m_strPpcCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPpcCode ) : '\'' . addslashes( $this->m_strPpcCode ) . '\'' ) : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setStreetAddress( $strStreetAddress ) {
		$this->set( 'm_strStreetAddress', CStrings::strTrimDef( $strStreetAddress, 100, NULL, true ) );
	}

	public function getStreetAddress() {
		return $this->m_strStreetAddress;
	}

	public function sqlStreetAddress() {
		return ( true == isset( $this->m_strStreetAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetAddress ) : '\'' . addslashes( $this->m_strStreetAddress ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setCountyCode( $strCountyCode ) {
		$this->set( 'm_strCountyCode', CStrings::strTrimDef( $strCountyCode, 50, NULL, true ) );
	}

	public function getCountyCode() {
		return $this->m_strCountyCode;
	}

	public function sqlCountyCode() {
		return ( true == isset( $this->m_strCountyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountyCode ) : '\'' . addslashes( $this->m_strCountyCode ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setCountyName( $strCountyName ) {
		$this->set( 'm_strCountyName', CStrings::strTrimDef( $strCountyName, 50, NULL, true ) );
	}

	public function getCountyName() {
		return $this->m_strCountyName;
	}

	public function sqlCountyName() {
		return ( true == isset( $this->m_strCountyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountyName ) : '\'' . addslashes( $this->m_strCountyName ) . '\'' ) : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 25, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLongitude ) : '\'' . addslashes( $this->m_strLongitude ) . '\'' ) : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 25, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLatitude ) : '\'' . addslashes( $this->m_strLatitude ) . '\'' ) : 'NULL';
	}

	public function setConstructionYear( $strConstructionYear ) {
		$this->set( 'm_strConstructionYear', CStrings::strTrimDef( $strConstructionYear, 50, NULL, true ) );
	}

	public function getConstructionYear() {
		return $this->m_strConstructionYear;
	}

	public function sqlConstructionYear() {
		return ( true == isset( $this->m_strConstructionYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConstructionYear ) : '\'' . addslashes( $this->m_strConstructionYear ) . '\'' ) : 'NULL';
	}

	public function setProfessionallyManagedYear( $strProfessionallyManagedYear ) {
		$this->set( 'm_strProfessionallyManagedYear', CStrings::strTrimDef( $strProfessionallyManagedYear, 50, NULL, true ) );
	}

	public function getProfessionallyManagedYear() {
		return $this->m_strProfessionallyManagedYear;
	}

	public function sqlProfessionallyManagedYear() {
		return ( true == isset( $this->m_strProfessionallyManagedYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProfessionallyManagedYear ) : '\'' . addslashes( $this->m_strProfessionallyManagedYear ) . '\'' ) : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsProfessionallyManaged( $boolIsProfessionallyManaged ) {
		$this->set( 'm_boolIsProfessionallyManaged', CStrings::strToBool( $boolIsProfessionallyManaged ) );
	}

	public function getIsProfessionallyManaged() {
		return $this->m_boolIsProfessionallyManaged;
	}

	public function sqlIsProfessionallyManaged() {
		return ( true == isset( $this->m_boolIsProfessionallyManaged ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsProfessionallyManaged ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGatedCommunity( $boolIsGatedCommunity ) {
		$this->set( 'm_boolIsGatedCommunity', CStrings::strToBool( $boolIsGatedCommunity ) );
	}

	public function getIsGatedCommunity() {
		return $this->m_boolIsGatedCommunity;
	}

	public function sqlIsGatedCommunity() {
		return ( true == isset( $this->m_boolIsGatedCommunity ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGatedCommunity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, call_phone_number_id, auto_sprinkler_credit_id, property_construction_type_id, windstorm_construction_credit_id, unit_count, bceg_code_id, ppc_code, name, street_address, city, state_code, county_code, postal_code, county_name, longitude, latitude, construction_year, professionally_managed_year, is_active, is_disabled, is_professionally_managed, is_gated_community, details, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCallPhoneNumberId() . ', ' .
						$this->sqlAutoSprinklerCreditId() . ', ' .
						$this->sqlPropertyConstructionTypeId() . ', ' .
						$this->sqlWindstormConstructionCreditId() . ', ' .
						$this->sqlUnitCount() . ', ' .
						$this->sqlBcegCodeId() . ', ' .
						$this->sqlPpcCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlStreetAddress() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlCountyCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountyName() . ', ' .
						$this->sqlLongitude() . ', ' .
						$this->sqlLatitude() . ', ' .
						$this->sqlConstructionYear() . ', ' .
						$this->sqlProfessionallyManagedYear() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsProfessionallyManaged() . ', ' .
						$this->sqlIsGatedCommunity() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_sprinkler_credit_id = ' . $this->sqlAutoSprinklerCreditId(). ',' ; } elseif( true == array_key_exists( 'AutoSprinklerCreditId', $this->getChangedColumns() ) ) { $strSql .= ' auto_sprinkler_credit_id = ' . $this->sqlAutoSprinklerCreditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_construction_type_id = ' . $this->sqlPropertyConstructionTypeId(). ',' ; } elseif( true == array_key_exists( 'PropertyConstructionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_construction_type_id = ' . $this->sqlPropertyConstructionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' windstorm_construction_credit_id = ' . $this->sqlWindstormConstructionCreditId(). ',' ; } elseif( true == array_key_exists( 'WindstormConstructionCreditId', $this->getChangedColumns() ) ) { $strSql .= ' windstorm_construction_credit_id = ' . $this->sqlWindstormConstructionCreditId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bceg_code_id = ' . $this->sqlBcegCodeId(). ',' ; } elseif( true == array_key_exists( 'BcegCodeId', $this->getChangedColumns() ) ) { $strSql .= ' bceg_code_id = ' . $this->sqlBcegCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ppc_code = ' . $this->sqlPpcCode(). ',' ; } elseif( true == array_key_exists( 'PpcCode', $this->getChangedColumns() ) ) { $strSql .= ' ppc_code = ' . $this->sqlPpcCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_address = ' . $this->sqlStreetAddress(). ',' ; } elseif( true == array_key_exists( 'StreetAddress', $this->getChangedColumns() ) ) { $strSql .= ' street_address = ' . $this->sqlStreetAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county_code = ' . $this->sqlCountyCode(). ',' ; } elseif( true == array_key_exists( 'CountyCode', $this->getChangedColumns() ) ) { $strSql .= ' county_code = ' . $this->sqlCountyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county_name = ' . $this->sqlCountyName(). ',' ; } elseif( true == array_key_exists( 'CountyName', $this->getChangedColumns() ) ) { $strSql .= ' county_name = ' . $this->sqlCountyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude(). ',' ; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude(). ',' ; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' construction_year = ' . $this->sqlConstructionYear(). ',' ; } elseif( true == array_key_exists( 'ConstructionYear', $this->getChangedColumns() ) ) { $strSql .= ' construction_year = ' . $this->sqlConstructionYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' professionally_managed_year = ' . $this->sqlProfessionallyManagedYear(). ',' ; } elseif( true == array_key_exists( 'ProfessionallyManagedYear', $this->getChangedColumns() ) ) { $strSql .= ' professionally_managed_year = ' . $this->sqlProfessionallyManagedYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_professionally_managed = ' . $this->sqlIsProfessionallyManaged(). ',' ; } elseif( true == array_key_exists( 'IsProfessionallyManaged', $this->getChangedColumns() ) ) { $strSql .= ' is_professionally_managed = ' . $this->sqlIsProfessionallyManaged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gated_community = ' . $this->sqlIsGatedCommunity(). ',' ; } elseif( true == array_key_exists( 'IsGatedCommunity', $this->getChangedColumns() ) ) { $strSql .= ' is_gated_community = ' . $this->sqlIsGatedCommunity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'auto_sprinkler_credit_id' => $this->getAutoSprinklerCreditId(),
			'property_construction_type_id' => $this->getPropertyConstructionTypeId(),
			'windstorm_construction_credit_id' => $this->getWindstormConstructionCreditId(),
			'unit_count' => $this->getUnitCount(),
			'bceg_code_id' => $this->getBcegCodeId(),
			'ppc_code' => $this->getPpcCode(),
			'name' => $this->getName(),
			'street_address' => $this->getStreetAddress(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'county_code' => $this->getCountyCode(),
			'postal_code' => $this->getPostalCode(),
			'county_name' => $this->getCountyName(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'construction_year' => $this->getConstructionYear(),
			'professionally_managed_year' => $this->getProfessionallyManagedYear(),
			'is_active' => $this->getIsActive(),
			'is_disabled' => $this->getIsDisabled(),
			'is_professionally_managed' => $this->getIsProfessionallyManaged(),
			'is_gated_community' => $this->getIsGatedCommunity(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>