<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceBlockedCounties
 * Do not add any new functions to this class.
 */

class CBaseInsuranceBlockedCounties extends CEosPluralBase {

	/**
	 * @return CInsuranceBlockedCounty[]
	 */
	public static function fetchInsuranceBlockedCounties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceBlockedCounty', $objDatabase );
	}

	/**
	 * @return CInsuranceBlockedCounty
	 */
	public static function fetchInsuranceBlockedCounty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceBlockedCounty', $objDatabase );
	}

	public static function fetchInsuranceBlockedCountyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_blocked_counties', $objDatabase );
	}

	public static function fetchInsuranceBlockedCountyById( $intId, $objDatabase ) {
		return self::fetchInsuranceBlockedCounty( sprintf( 'SELECT * FROM insurance_blocked_counties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceBlockedCountiesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceBlockedCounties( sprintf( 'SELECT * FROM insurance_blocked_counties WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

}
?>