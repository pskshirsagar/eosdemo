<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceNotices
 * Do not add any new functions to this class.
 */

class CBaseInsuranceNotices extends CEosPluralBase {

	/**
	 * @return CInsuranceNotice[]
	 */
	public static function fetchInsuranceNotices( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceNotice', $objDatabase );
	}

	/**
	 * @return CInsuranceNotice
	 */
	public static function fetchInsuranceNotice( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceNotice', $objDatabase );
	}

	public static function fetchInsuranceNoticeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_notices', $objDatabase );
	}

	public static function fetchInsuranceNoticeById( $intId, $objDatabase ) {
		return self::fetchInsuranceNotice( sprintf( 'SELECT * FROM insurance_notices WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceNoticesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceNotices( sprintf( 'SELECT * FROM insurance_notices WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceNoticesByInsuranceNoticeTypeId( $intInsuranceNoticeTypeId, $objDatabase ) {
		return self::fetchInsuranceNotices( sprintf( 'SELECT * FROM insurance_notices WHERE insurance_notice_type_id = %d', ( int ) $intInsuranceNoticeTypeId ), $objDatabase );
	}

}
?>