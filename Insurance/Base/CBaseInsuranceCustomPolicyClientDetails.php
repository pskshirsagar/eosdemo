<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCustomPolicyClientDetails
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCustomPolicyClientDetails extends CEosPluralBase {

	/**
	 * @return CInsuranceCustomPolicyClientDetail[]
	 */
	public static function fetchInsuranceCustomPolicyClientDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCustomPolicyClientDetail::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCustomPolicyClientDetail
	 */
	public static function fetchInsuranceCustomPolicyClientDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCustomPolicyClientDetail::class, $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyClientDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_custom_policy_client_details', $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyClientDetailById( $intId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyClientDetail( sprintf( 'SELECT * FROM insurance_custom_policy_client_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyClientDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyClientDetails( sprintf( 'SELECT * FROM insurance_custom_policy_client_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyClientDetailsByInsuranceCarrierLimitId( $intInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicyClientDetails( sprintf( 'SELECT * FROM insurance_custom_policy_client_details WHERE insurance_carrier_limit_id = %d', ( int ) $intInsuranceCarrierLimitId ), $objDatabase );
	}

}
?>