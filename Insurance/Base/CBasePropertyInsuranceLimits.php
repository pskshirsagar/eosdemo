<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CPropertyInsuranceLimits
 * Do not add any new functions to this class.
 */

class CBasePropertyInsuranceLimits extends CEosPluralBase {

	/**
	 * @return CPropertyInsuranceLimit[]
	 */
	public static function fetchPropertyInsuranceLimits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyInsuranceLimit::class, $objDatabase );
	}

	/**
	 * @return CPropertyInsuranceLimit
	 */
	public static function fetchPropertyInsuranceLimit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyInsuranceLimit::class, $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_insurance_limits', $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitById( $intId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimit( sprintf( 'SELECT * FROM property_insurance_limits WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE insurance_policy_type_id = %d', $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByDefaultLiabilityInsuranceCarrierLimitId( $intDefaultLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE default_liability_insurance_carrier_limit_id = %d', $intDefaultLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE personal_insurance_carrier_limit_id = %d', $intPersonalInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE deductible_insurance_carrier_limit_id = %d', $intDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByMinLiabilityInsuranceCarrierLimitId( $intMinLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE min_liability_insurance_carrier_limit_id = %d', $intMinLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByMaxLiabilityInsuranceCarrierLimitId( $intMaxLiabilityInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE max_liability_insurance_carrier_limit_id = %d', $intMaxLiabilityInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByMinDeductibleInsuranceCarrierLimitId( $intMinDeductibleInsuranceCarrierLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE min_deductible_insurance_carrier_limit_id = %d', $intMinDeductibleInsuranceCarrierLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByDefaultMedicalPaymentLimitId( $intDefaultMedicalPaymentLimitId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE default_medical_payment_limit_id = %d', $intDefaultMedicalPaymentLimitId ), $objDatabase );
	}

	public static function fetchPropertyInsuranceLimitsByEnhancementEndorsementId( $intEnhancementEndorsementId, $objDatabase ) {
		return self::fetchPropertyInsuranceLimits( sprintf( 'SELECT * FROM property_insurance_limits WHERE enhancement_endorsement_id = %d', $intEnhancementEndorsementId ), $objDatabase );
	}

}
?>