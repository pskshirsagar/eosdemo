<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceLimitTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceLimitTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceLimitType[]
	 */
	public static function fetchInsuranceLimitTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceLimitType', $objDatabase );
	}

	/**
	 * @return CInsuranceLimitType
	 */
	public static function fetchInsuranceLimitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceLimitType', $objDatabase );
	}

	public static function fetchInsuranceLimitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_limit_types', $objDatabase );
	}

	public static function fetchInsuranceLimitTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceLimitType( sprintf( 'SELECT * FROM insurance_limit_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>