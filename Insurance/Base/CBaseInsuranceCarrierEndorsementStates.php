<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierEndorsementStates
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierEndorsementStates extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierEndorsementState[]
	 */
	public static function fetchInsuranceCarrierEndorsementStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceCarrierEndorsementState', $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierEndorsementState
	 */
	public static function fetchInsuranceCarrierEndorsementState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceCarrierEndorsementState', $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_endorsement_states', $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementStateById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsementState( sprintf( 'SELECT * FROM insurance_carrier_endorsement_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementStatesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsementStates( sprintf( 'SELECT * FROM insurance_carrier_endorsement_states WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementStatesByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsementStates( sprintf( 'SELECT * FROM insurance_carrier_endorsement_states WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierEndorsementStatesByInsuranceCarrierEndorsementTypeId( $intInsuranceCarrierEndorsementTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierEndorsementStates( sprintf( 'SELECT * FROM insurance_carrier_endorsement_states WHERE insurance_carrier_endorsement_type_id = %d', ( int ) $intInsuranceCarrierEndorsementTypeId ), $objDatabase );
	}

}
?>