<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyBatchTypes
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyBatchTypes extends CEosPluralBase {

	/**
	 * @return CMasterPolicyBatchType[]
	 */
	public static function fetchMasterPolicyBatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyBatchType::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyBatchType
	 */
	public static function fetchMasterPolicyBatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyBatchType::class, $objDatabase );
	}

	public static function fetchMasterPolicyBatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_batch_types', $objDatabase );
	}

	public static function fetchMasterPolicyBatchTypeById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyBatchType( sprintf( 'SELECT * FROM master_policy_batch_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>