<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyPropertyProducts
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyPropertyProducts extends CEosPluralBase {

	/**
	 * @return CMasterPolicyPropertyProduct[]
	 */
	public static function fetchMasterPolicyPropertyProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyPropertyProduct::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyPropertyProduct
	 */
	public static function fetchMasterPolicyPropertyProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyPropertyProduct::class, $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_property_products', $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductById( $intId, $objDatabase ) {
		return self::fetchMasterPolicyPropertyProduct( sprintf( 'SELECT * FROM master_policy_property_products WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductsByCid( $intCid, $objDatabase ) {
		return self::fetchMasterPolicyPropertyProducts( sprintf( 'SELECT * FROM master_policy_property_products WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMasterPolicyPropertyProducts( sprintf( 'SELECT * FROM master_policy_property_products WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductsByMasterPolicyProductId( $intMasterPolicyProductId, $objDatabase ) {
		return self::fetchMasterPolicyPropertyProducts( sprintf( 'SELECT * FROM master_policy_property_products WHERE master_policy_product_id = %d', $intMasterPolicyProductId ), $objDatabase );
	}

	public static function fetchMasterPolicyPropertyProductsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchMasterPolicyPropertyProducts( sprintf( 'SELECT * FROM master_policy_property_products WHERE insurance_policy_type_id = %d', $intInsurancePolicyTypeId ), $objDatabase );
	}

}
?>