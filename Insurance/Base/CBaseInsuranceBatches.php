<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceBatches
 * Do not add any new functions to this class.
 */

class CBaseInsuranceBatches extends CEosPluralBase {

	/**
	 * @return CInsuranceBatch[]
	 */
	public static function fetchInsuranceBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceBatch', $objDatabase );
	}

	/**
	 * @return CInsuranceBatch
	 */
	public static function fetchInsuranceBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceBatch', $objDatabase );
	}

	public static function fetchInsuranceBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_batches', $objDatabase );
	}

	public static function fetchInsuranceBatchById( $intId, $objDatabase ) {
		return self::fetchInsuranceBatch( sprintf( 'SELECT * FROM insurance_batches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceBatches( sprintf( 'SELECT * FROM insurance_batches WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsuranceBatchesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceBatches( sprintf( 'SELECT * FROM insurance_batches WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceBatchesByInsuranceBatchTypeId( $intInsuranceBatchTypeId, $objDatabase ) {
		return self::fetchInsuranceBatches( sprintf( 'SELECT * FROM insurance_batches WHERE insurance_batch_type_id = %d', ( int ) $intInsuranceBatchTypeId ), $objDatabase );
	}

	public static function fetchInsuranceBatchesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceBatches( sprintf( 'SELECT * FROM insurance_batches WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceBatchesByInsuranceBatchStatusTypeId( $intInsuranceBatchStatusTypeId, $objDatabase ) {
		return self::fetchInsuranceBatches( sprintf( 'SELECT * FROM insurance_batches WHERE insurance_batch_status_type_id = %d', ( int ) $intInsuranceBatchStatusTypeId ), $objDatabase );
	}

}
?>