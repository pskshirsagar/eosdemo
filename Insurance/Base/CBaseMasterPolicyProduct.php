<?php

class CBaseMasterPolicyProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.master_policy_products';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMasterPolicyClientId;
	protected $m_intLiabilityInsuranceCarrierLimitId;
	protected $m_intPersonalInsuranceCarrierLimitId;
	protected $m_intDeductibleInsuranceCarrierLimitId;
	protected $m_strClientNumber;
	protected $m_strMasterPolicyNumber;
	protected $m_fltBaseRate;
	protected $m_fltPolicyFee;
	protected $m_fltAdminFee;
	protected $m_fltBasicTaxPercent;
	protected $m_fltStampingTaxPercent;
	protected $m_fltOtherTaxPercent;
	protected $m_strFileName;
	protected $m_boolIsActive;
	protected $m_strIssuedDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strEffectiveDate;

	public function __construct() {
		parent::__construct();

		$this->m_fltBasicTaxPercent = '0';
		$this->m_fltStampingTaxPercent = '0';
		$this->m_fltOtherTaxPercent = '0';
		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['master_policy_client_id'] ) && $boolDirectSet ) $this->set( 'm_intMasterPolicyClientId', trim( $arrValues['master_policy_client_id'] ) ); elseif( isset( $arrValues['master_policy_client_id'] ) ) $this->setMasterPolicyClientId( $arrValues['master_policy_client_id'] );
		if( isset( $arrValues['liability_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intLiabilityInsuranceCarrierLimitId', trim( $arrValues['liability_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['liability_insurance_carrier_limit_id'] ) ) $this->setLiabilityInsuranceCarrierLimitId( $arrValues['liability_insurance_carrier_limit_id'] );
		if( isset( $arrValues['personal_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonalInsuranceCarrierLimitId', trim( $arrValues['personal_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['personal_insurance_carrier_limit_id'] ) ) $this->setPersonalInsuranceCarrierLimitId( $arrValues['personal_insurance_carrier_limit_id'] );
		if( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) && $boolDirectSet ) $this->set( 'm_intDeductibleInsuranceCarrierLimitId', trim( $arrValues['deductible_insurance_carrier_limit_id'] ) ); elseif( isset( $arrValues['deductible_insurance_carrier_limit_id'] ) ) $this->setDeductibleInsuranceCarrierLimitId( $arrValues['deductible_insurance_carrier_limit_id'] );
		if( isset( $arrValues['client_number'] ) && $boolDirectSet ) $this->set( 'm_strClientNumber', trim( stripcslashes( $arrValues['client_number'] ) ) ); elseif( isset( $arrValues['client_number'] ) ) $this->setClientNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_number'] ) : $arrValues['client_number'] );
		if( isset( $arrValues['master_policy_number'] ) && $boolDirectSet ) $this->set( 'm_strMasterPolicyNumber', trim( stripcslashes( $arrValues['master_policy_number'] ) ) ); elseif( isset( $arrValues['master_policy_number'] ) ) $this->setMasterPolicyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['master_policy_number'] ) : $arrValues['master_policy_number'] );
		if( isset( $arrValues['base_rate'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRate', trim( $arrValues['base_rate'] ) ); elseif( isset( $arrValues['base_rate'] ) ) $this->setBaseRate( $arrValues['base_rate'] );
		if( isset( $arrValues['policy_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPolicyFee', trim( $arrValues['policy_fee'] ) ); elseif( isset( $arrValues['policy_fee'] ) ) $this->setPolicyFee( $arrValues['policy_fee'] );
		if( isset( $arrValues['admin_fee'] ) && $boolDirectSet ) $this->set( 'm_fltAdminFee', trim( $arrValues['admin_fee'] ) ); elseif( isset( $arrValues['admin_fee'] ) ) $this->setAdminFee( $arrValues['admin_fee'] );
		if( isset( $arrValues['basic_tax_percent'] ) && $boolDirectSet ) $this->set( 'm_fltBasicTaxPercent', trim( $arrValues['basic_tax_percent'] ) ); elseif( isset( $arrValues['basic_tax_percent'] ) ) $this->setBasicTaxPercent( $arrValues['basic_tax_percent'] );
		if( isset( $arrValues['stamping_tax_percent'] ) && $boolDirectSet ) $this->set( 'm_fltStampingTaxPercent', trim( $arrValues['stamping_tax_percent'] ) ); elseif( isset( $arrValues['stamping_tax_percent'] ) ) $this->setStampingTaxPercent( $arrValues['stamping_tax_percent'] );
		if( isset( $arrValues['other_tax_percent'] ) && $boolDirectSet ) $this->set( 'm_fltOtherTaxPercent', trim( $arrValues['other_tax_percent'] ) ); elseif( isset( $arrValues['other_tax_percent'] ) ) $this->setOtherTaxPercent( $arrValues['other_tax_percent'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['issued_date'] ) && $boolDirectSet ) $this->set( 'm_strIssuedDate', trim( $arrValues['issued_date'] ) ); elseif( isset( $arrValues['issued_date'] ) ) $this->setIssuedDate( $arrValues['issued_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMasterPolicyClientId( $intMasterPolicyClientId ) {
		$this->set( 'm_intMasterPolicyClientId', CStrings::strToIntDef( $intMasterPolicyClientId, NULL, false ) );
	}

	public function getMasterPolicyClientId() {
		return $this->m_intMasterPolicyClientId;
	}

	public function sqlMasterPolicyClientId() {
		return ( true == isset( $this->m_intMasterPolicyClientId ) ) ? ( string ) $this->m_intMasterPolicyClientId : 'NULL';
	}

	public function setLiabilityInsuranceCarrierLimitId( $intLiabilityInsuranceCarrierLimitId ) {
		$this->set( 'm_intLiabilityInsuranceCarrierLimitId', CStrings::strToIntDef( $intLiabilityInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getLiabilityInsuranceCarrierLimitId() {
		return $this->m_intLiabilityInsuranceCarrierLimitId;
	}

	public function sqlLiabilityInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intLiabilityInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intLiabilityInsuranceCarrierLimitId : 'NULL';
	}

	public function setPersonalInsuranceCarrierLimitId( $intPersonalInsuranceCarrierLimitId ) {
		$this->set( 'm_intPersonalInsuranceCarrierLimitId', CStrings::strToIntDef( $intPersonalInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getPersonalInsuranceCarrierLimitId() {
		return $this->m_intPersonalInsuranceCarrierLimitId;
	}

	public function sqlPersonalInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intPersonalInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intPersonalInsuranceCarrierLimitId : 'NULL';
	}

	public function setDeductibleInsuranceCarrierLimitId( $intDeductibleInsuranceCarrierLimitId ) {
		$this->set( 'm_intDeductibleInsuranceCarrierLimitId', CStrings::strToIntDef( $intDeductibleInsuranceCarrierLimitId, NULL, false ) );
	}

	public function getDeductibleInsuranceCarrierLimitId() {
		return $this->m_intDeductibleInsuranceCarrierLimitId;
	}

	public function sqlDeductibleInsuranceCarrierLimitId() {
		return ( true == isset( $this->m_intDeductibleInsuranceCarrierLimitId ) ) ? ( string ) $this->m_intDeductibleInsuranceCarrierLimitId : 'NULL';
	}

	public function setClientNumber( $strClientNumber ) {
		$this->set( 'm_strClientNumber', CStrings::strTrimDef( $strClientNumber, 64, NULL, true ) );
	}

	public function getClientNumber() {
		return $this->m_strClientNumber;
	}

	public function sqlClientNumber() {
		return ( true == isset( $this->m_strClientNumber ) ) ? '\'' . addslashes( $this->m_strClientNumber ) . '\'' : 'NULL';
	}

	public function setMasterPolicyNumber( $strMasterPolicyNumber ) {
		$this->set( 'm_strMasterPolicyNumber', CStrings::strTrimDef( $strMasterPolicyNumber, 64, NULL, true ) );
	}

	public function getMasterPolicyNumber() {
		return $this->m_strMasterPolicyNumber;
	}

	public function sqlMasterPolicyNumber() {
		return ( true == isset( $this->m_strMasterPolicyNumber ) ) ? '\'' . addslashes( $this->m_strMasterPolicyNumber ) . '\'' : 'NULL';
	}

	public function setBaseRate( $fltBaseRate ) {
		$this->set( 'm_fltBaseRate', CStrings::strToFloatDef( $fltBaseRate, NULL, false, 2 ) );
	}

	public function getBaseRate() {
		return $this->m_fltBaseRate;
	}

	public function sqlBaseRate() {
		return ( true == isset( $this->m_fltBaseRate ) ) ? ( string ) $this->m_fltBaseRate : 'NULL';
	}

	public function setPolicyFee( $fltPolicyFee ) {
		$this->set( 'm_fltPolicyFee', CStrings::strToFloatDef( $fltPolicyFee, NULL, false, 2 ) );
	}

	public function getPolicyFee() {
		return $this->m_fltPolicyFee;
	}

	public function sqlPolicyFee() {
		return ( true == isset( $this->m_fltPolicyFee ) ) ? ( string ) $this->m_fltPolicyFee : 'NULL';
	}

	public function setAdminFee( $fltAdminFee ) {
		$this->set( 'm_fltAdminFee', CStrings::strToFloatDef( $fltAdminFee, NULL, false, 2 ) );
	}

	public function getAdminFee() {
		return $this->m_fltAdminFee;
	}

	public function sqlAdminFee() {
		return ( true == isset( $this->m_fltAdminFee ) ) ? ( string ) $this->m_fltAdminFee : 'NULL';
	}

	public function setBasicTaxPercent( $fltBasicTaxPercent ) {
		$this->set( 'm_fltBasicTaxPercent', CStrings::strToFloatDef( $fltBasicTaxPercent, NULL, false, 2 ) );
	}

	public function getBasicTaxPercent() {
		return $this->m_fltBasicTaxPercent;
	}

	public function sqlBasicTaxPercent() {
		return ( true == isset( $this->m_fltBasicTaxPercent ) ) ? ( string ) $this->m_fltBasicTaxPercent : '0';
	}

	public function setStampingTaxPercent( $fltStampingTaxPercent ) {
		$this->set( 'm_fltStampingTaxPercent', CStrings::strToFloatDef( $fltStampingTaxPercent, NULL, false, 3 ) );
	}

	public function getStampingTaxPercent() {
		return $this->m_fltStampingTaxPercent;
	}

	public function sqlStampingTaxPercent() {
		return ( true == isset( $this->m_fltStampingTaxPercent ) ) ? ( string ) $this->m_fltStampingTaxPercent : '0';
	}

	public function setOtherTaxPercent( $fltOtherTaxPercent ) {
		$this->set( 'm_fltOtherTaxPercent', CStrings::strToFloatDef( $fltOtherTaxPercent, NULL, false, 2 ) );
	}

	public function getOtherTaxPercent() {
		return $this->m_fltOtherTaxPercent;
	}

	public function sqlOtherTaxPercent() {
		return ( true == isset( $this->m_fltOtherTaxPercent ) ) ? ( string ) $this->m_fltOtherTaxPercent : '0';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIssuedDate( $strIssuedDate ) {
		$this->set( 'm_strIssuedDate', CStrings::strTrimDef( $strIssuedDate, -1, NULL, true ) );
	}

	public function getIssuedDate() {
		return $this->m_strIssuedDate;
	}

	public function sqlIssuedDate() {
		return ( true == isset( $this->m_strIssuedDate ) ) ? '\'' . $this->m_strIssuedDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, master_policy_client_id, liability_insurance_carrier_limit_id, personal_insurance_carrier_limit_id, deductible_insurance_carrier_limit_id, client_number, master_policy_number, base_rate, policy_fee, admin_fee, basic_tax_percent, stamping_tax_percent, other_tax_percent, file_name, is_active, issued_date, updated_by, updated_on, created_by, created_on, effective_date )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlMasterPolicyClientId() . ', ' .
		          $this->sqlLiabilityInsuranceCarrierLimitId() . ', ' .
		          $this->sqlPersonalInsuranceCarrierLimitId() . ', ' .
		          $this->sqlDeductibleInsuranceCarrierLimitId() . ', ' .
		          $this->sqlClientNumber() . ', ' .
		          $this->sqlMasterPolicyNumber() . ', ' .
		          $this->sqlBaseRate() . ', ' .
		          $this->sqlPolicyFee() . ', ' .
		          $this->sqlAdminFee() . ', ' .
		          $this->sqlBasicTaxPercent() . ', ' .
		          $this->sqlStampingTaxPercent() . ', ' .
		          $this->sqlOtherTaxPercent() . ', ' .
		          $this->sqlFileName() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          $this->sqlIssuedDate() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlEffectiveDate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' master_policy_client_id = ' . $this->sqlMasterPolicyClientId(). ',' ; } elseif( true == array_key_exists( 'MasterPolicyClientId', $this->getChangedColumns() ) ) { $strSql .= ' master_policy_client_id = ' . $this->sqlMasterPolicyClientId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'LiabilityInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' liability_insurance_carrier_limit_id = ' . $this->sqlLiabilityInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'PersonalInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' personal_insurance_carrier_limit_id = ' . $this->sqlPersonalInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId(). ',' ; } elseif( true == array_key_exists( 'DeductibleInsuranceCarrierLimitId', $this->getChangedColumns() ) ) { $strSql .= ' deductible_insurance_carrier_limit_id = ' . $this->sqlDeductibleInsuranceCarrierLimitId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_number = ' . $this->sqlClientNumber(). ',' ; } elseif( true == array_key_exists( 'ClientNumber', $this->getChangedColumns() ) ) { $strSql .= ' client_number = ' . $this->sqlClientNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' master_policy_number = ' . $this->sqlMasterPolicyNumber(). ',' ; } elseif( true == array_key_exists( 'MasterPolicyNumber', $this->getChangedColumns() ) ) { $strSql .= ' master_policy_number = ' . $this->sqlMasterPolicyNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate(). ',' ; } elseif( true == array_key_exists( 'BaseRate', $this->getChangedColumns() ) ) { $strSql .= ' base_rate = ' . $this->sqlBaseRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_fee = ' . $this->sqlPolicyFee(). ',' ; } elseif( true == array_key_exists( 'PolicyFee', $this->getChangedColumns() ) ) { $strSql .= ' policy_fee = ' . $this->sqlPolicyFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee(). ',' ; } elseif( true == array_key_exists( 'AdminFee', $this->getChangedColumns() ) ) { $strSql .= ' admin_fee = ' . $this->sqlAdminFee() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' basic_tax_percent = ' . $this->sqlBasicTaxPercent(). ',' ; } elseif( true == array_key_exists( 'BasicTaxPercent', $this->getChangedColumns() ) ) { $strSql .= ' basic_tax_percent = ' . $this->sqlBasicTaxPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stamping_tax_percent = ' . $this->sqlStampingTaxPercent(). ',' ; } elseif( true == array_key_exists( 'StampingTaxPercent', $this->getChangedColumns() ) ) { $strSql .= ' stamping_tax_percent = ' . $this->sqlStampingTaxPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_tax_percent = ' . $this->sqlOtherTaxPercent(). ',' ; } elseif( true == array_key_exists( 'OtherTaxPercent', $this->getChangedColumns() ) ) { $strSql .= ' other_tax_percent = ' . $this->sqlOtherTaxPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' issued_date = ' . $this->sqlIssuedDate(). ',' ; } elseif( true == array_key_exists( 'IssuedDate', $this->getChangedColumns() ) ) { $strSql .= ' issued_date = ' . $this->sqlIssuedDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'master_policy_client_id' => $this->getMasterPolicyClientId(),
			'liability_insurance_carrier_limit_id' => $this->getLiabilityInsuranceCarrierLimitId(),
			'personal_insurance_carrier_limit_id' => $this->getPersonalInsuranceCarrierLimitId(),
			'deductible_insurance_carrier_limit_id' => $this->getDeductibleInsuranceCarrierLimitId(),
			'client_number' => $this->getClientNumber(),
			'master_policy_number' => $this->getMasterPolicyNumber(),
			'base_rate' => $this->getBaseRate(),
			'policy_fee' => $this->getPolicyFee(),
			'admin_fee' => $this->getAdminFee(),
			'basic_tax_percent' => $this->getBasicTaxPercent(),
			'stamping_tax_percent' => $this->getStampingTaxPercent(),
			'other_tax_percent' => $this->getOtherTaxPercent(),
			'file_name' => $this->getFileName(),
			'is_active' => $this->getIsActive(),
			'issued_date' => $this->getIssuedDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'effective_date' => $this->getEffectiveDate()
		);
	}

}
?>