<?php

class CBasePropertyStatistic extends CEosSingularBase {

	const TABLE_NAME = 'public.property_statistics';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strPropertyName;
	protected $m_intUnitCount;
	protected $m_intOccupiedUnits;
	protected $m_intTotalInsuredUnits;
	protected $m_intRiInsured;
	protected $m_intNonRiInsured;
	protected $m_intTotalRi;
	protected $m_fltRiPremium;
	protected $m_fltRiEarnedPremium;
	protected $m_intMovedInMtd;
	protected $m_intMovedInInsuredMtd;
	protected $m_intMoveInRiMtd;
	protected $m_intIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUnitSpaceCount;
	protected $m_intMpInsured;
	protected $m_intMoveInMpMtd;
	protected $m_intExceptionUnitCount;
	protected $m_intMoveInExceptionUnitMtd;
	protected $m_intTotalOccupantsCount;
	protected $m_intTotalResponsibleOccupantCount;

	public function __construct() {
		parent::__construct();

		$this->m_intUnitCount = '0';
		$this->m_intOccupiedUnits = '0';
		$this->m_intTotalInsuredUnits = '0';
		$this->m_intRiInsured = '0';
		$this->m_intNonRiInsured = '0';
		$this->m_intTotalRi = '0';
		$this->m_intMovedInMtd = '0';
		$this->m_intMovedInInsuredMtd = '0';
		$this->m_intMoveInRiMtd = '0';
		$this->m_intIsActive = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['occupied_units'] ) && $boolDirectSet ) $this->set( 'm_intOccupiedUnits', trim( $arrValues['occupied_units'] ) ); elseif( isset( $arrValues['occupied_units'] ) ) $this->setOccupiedUnits( $arrValues['occupied_units'] );
		if( isset( $arrValues['total_insured_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalInsuredUnits', trim( $arrValues['total_insured_units'] ) ); elseif( isset( $arrValues['total_insured_units'] ) ) $this->setTotalInsuredUnits( $arrValues['total_insured_units'] );
		if( isset( $arrValues['ri_insured'] ) && $boolDirectSet ) $this->set( 'm_intRiInsured', trim( $arrValues['ri_insured'] ) ); elseif( isset( $arrValues['ri_insured'] ) ) $this->setRiInsured( $arrValues['ri_insured'] );
		if( isset( $arrValues['non_ri_insured'] ) && $boolDirectSet ) $this->set( 'm_intNonRiInsured', trim( $arrValues['non_ri_insured'] ) ); elseif( isset( $arrValues['non_ri_insured'] ) ) $this->setNonRiInsured( $arrValues['non_ri_insured'] );
		if( isset( $arrValues['total_ri'] ) && $boolDirectSet ) $this->set( 'm_intTotalRi', trim( $arrValues['total_ri'] ) ); elseif( isset( $arrValues['total_ri'] ) ) $this->setTotalRi( $arrValues['total_ri'] );
		if( isset( $arrValues['ri_premium'] ) && $boolDirectSet ) $this->set( 'm_fltRiPremium', trim( $arrValues['ri_premium'] ) ); elseif( isset( $arrValues['ri_premium'] ) ) $this->setRiPremium( $arrValues['ri_premium'] );
		if( isset( $arrValues['ri_earned_premium'] ) && $boolDirectSet ) $this->set( 'm_fltRiEarnedPremium', trim( $arrValues['ri_earned_premium'] ) ); elseif( isset( $arrValues['ri_earned_premium'] ) ) $this->setRiEarnedPremium( $arrValues['ri_earned_premium'] );
		if( isset( $arrValues['moved_in_mtd'] ) && $boolDirectSet ) $this->set( 'm_intMovedInMtd', trim( $arrValues['moved_in_mtd'] ) ); elseif( isset( $arrValues['moved_in_mtd'] ) ) $this->setMovedInMtd( $arrValues['moved_in_mtd'] );
		if( isset( $arrValues['moved_in_insured_mtd'] ) && $boolDirectSet ) $this->set( 'm_intMovedInInsuredMtd', trim( $arrValues['moved_in_insured_mtd'] ) ); elseif( isset( $arrValues['moved_in_insured_mtd'] ) ) $this->setMovedInInsuredMtd( $arrValues['moved_in_insured_mtd'] );
		if( isset( $arrValues['move_in_ri_mtd'] ) && $boolDirectSet ) $this->set( 'm_intMoveInRiMtd', trim( $arrValues['move_in_ri_mtd'] ) ); elseif( isset( $arrValues['move_in_ri_mtd'] ) ) $this->setMoveInRiMtd( $arrValues['move_in_ri_mtd'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceCount', trim( $arrValues['unit_space_count'] ) ); elseif( isset( $arrValues['unit_space_count'] ) ) $this->setUnitSpaceCount( $arrValues['unit_space_count'] );
		if( isset( $arrValues['mp_insured'] ) && $boolDirectSet ) $this->set( 'm_intMpInsured', trim( $arrValues['mp_insured'] ) ); elseif( isset( $arrValues['mp_insured'] ) ) $this->setMpInsured( $arrValues['mp_insured'] );
		if( isset( $arrValues['move_in_mp_mtd'] ) && $boolDirectSet ) $this->set( 'm_intMoveInMpMtd', trim( $arrValues['move_in_mp_mtd'] ) ); elseif( isset( $arrValues['move_in_mp_mtd'] ) ) $this->setMoveInMpMtd( $arrValues['move_in_mp_mtd'] );
		if( isset( $arrValues['exception_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intExceptionUnitCount', trim( $arrValues['exception_unit_count'] ) ); elseif( isset( $arrValues['exception_unit_count'] ) ) $this->setExceptionUnitCount( $arrValues['exception_unit_count'] );
		if( isset( $arrValues['move_in_exception_unit_mtd'] ) && $boolDirectSet ) $this->set( 'm_intMoveInExceptionUnitMtd', trim( $arrValues['move_in_exception_unit_mtd'] ) ); elseif( isset( $arrValues['move_in_exception_unit_mtd'] ) ) $this->setMoveInExceptionUnitMtd( $arrValues['move_in_exception_unit_mtd'] );
		if( isset( $arrValues['total_occupants_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalOccupantsCount', trim( $arrValues['total_occupants_count'] ) ); elseif( isset( $arrValues['total_occupants_count'] ) ) $this->setTotalOccupantsCount( $arrValues['total_occupants_count'] );
		if( isset( $arrValues['total_responsible_occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalResponsibleOccupantCount', trim( $arrValues['total_responsible_occupant_count'] ) ); elseif( isset( $arrValues['total_responsible_occupant_count'] ) ) $this->setTotalResponsibleOccupantCount( $arrValues['total_responsible_occupant_count'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 250, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : '0';
	}

	public function setOccupiedUnits( $intOccupiedUnits ) {
		$this->set( 'm_intOccupiedUnits', CStrings::strToIntDef( $intOccupiedUnits, NULL, false ) );
	}

	public function getOccupiedUnits() {
		return $this->m_intOccupiedUnits;
	}

	public function sqlOccupiedUnits() {
		return ( true == isset( $this->m_intOccupiedUnits ) ) ? ( string ) $this->m_intOccupiedUnits : '0';
	}

	public function setTotalInsuredUnits( $intTotalInsuredUnits ) {
		$this->set( 'm_intTotalInsuredUnits', CStrings::strToIntDef( $intTotalInsuredUnits, NULL, false ) );
	}

	public function getTotalInsuredUnits() {
		return $this->m_intTotalInsuredUnits;
	}

	public function sqlTotalInsuredUnits() {
		return ( true == isset( $this->m_intTotalInsuredUnits ) ) ? ( string ) $this->m_intTotalInsuredUnits : '0';
	}

	public function setRiInsured( $intRiInsured ) {
		$this->set( 'm_intRiInsured', CStrings::strToIntDef( $intRiInsured, NULL, false ) );
	}

	public function getRiInsured() {
		return $this->m_intRiInsured;
	}

	public function sqlRiInsured() {
		return ( true == isset( $this->m_intRiInsured ) ) ? ( string ) $this->m_intRiInsured : '0';
	}

	public function setNonRiInsured( $intNonRiInsured ) {
		$this->set( 'm_intNonRiInsured', CStrings::strToIntDef( $intNonRiInsured, NULL, false ) );
	}

	public function getNonRiInsured() {
		return $this->m_intNonRiInsured;
	}

	public function sqlNonRiInsured() {
		return ( true == isset( $this->m_intNonRiInsured ) ) ? ( string ) $this->m_intNonRiInsured : '0';
	}

	public function setTotalRi( $intTotalRi ) {
		$this->set( 'm_intTotalRi', CStrings::strToIntDef( $intTotalRi, NULL, false ) );
	}

	public function getTotalRi() {
		return $this->m_intTotalRi;
	}

	public function sqlTotalRi() {
		return ( true == isset( $this->m_intTotalRi ) ) ? ( string ) $this->m_intTotalRi : '0';
	}

	public function setRiPremium( $fltRiPremium ) {
		$this->set( 'm_fltRiPremium', CStrings::strToFloatDef( $fltRiPremium, NULL, false, 2 ) );
	}

	public function getRiPremium() {
		return $this->m_fltRiPremium;
	}

	public function sqlRiPremium() {
		return ( true == isset( $this->m_fltRiPremium ) ) ? ( string ) $this->m_fltRiPremium : 'NULL';
	}

	public function setRiEarnedPremium( $fltRiEarnedPremium ) {
		$this->set( 'm_fltRiEarnedPremium', CStrings::strToFloatDef( $fltRiEarnedPremium, NULL, false, 2 ) );
	}

	public function getRiEarnedPremium() {
		return $this->m_fltRiEarnedPremium;
	}

	public function sqlRiEarnedPremium() {
		return ( true == isset( $this->m_fltRiEarnedPremium ) ) ? ( string ) $this->m_fltRiEarnedPremium : 'NULL';
	}

	public function setMovedInMtd( $intMovedInMtd ) {
		$this->set( 'm_intMovedInMtd', CStrings::strToIntDef( $intMovedInMtd, NULL, false ) );
	}

	public function getMovedInMtd() {
		return $this->m_intMovedInMtd;
	}

	public function sqlMovedInMtd() {
		return ( true == isset( $this->m_intMovedInMtd ) ) ? ( string ) $this->m_intMovedInMtd : '0';
	}

	public function setMovedInInsuredMtd( $intMovedInInsuredMtd ) {
		$this->set( 'm_intMovedInInsuredMtd', CStrings::strToIntDef( $intMovedInInsuredMtd, NULL, false ) );
	}

	public function getMovedInInsuredMtd() {
		return $this->m_intMovedInInsuredMtd;
	}

	public function sqlMovedInInsuredMtd() {
		return ( true == isset( $this->m_intMovedInInsuredMtd ) ) ? ( string ) $this->m_intMovedInInsuredMtd : '0';
	}

	public function setMoveInRiMtd( $intMoveInRiMtd ) {
		$this->set( 'm_intMoveInRiMtd', CStrings::strToIntDef( $intMoveInRiMtd, NULL, false ) );
	}

	public function getMoveInRiMtd() {
		return $this->m_intMoveInRiMtd;
	}

	public function sqlMoveInRiMtd() {
		return ( true == isset( $this->m_intMoveInRiMtd ) ) ? ( string ) $this->m_intMoveInRiMtd : '0';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUnitSpaceCount( $intUnitSpaceCount ) {
		$this->set( 'm_intUnitSpaceCount', CStrings::strToIntDef( $intUnitSpaceCount, NULL, false ) );
	}

	public function getUnitSpaceCount() {
		return $this->m_intUnitSpaceCount;
	}

	public function sqlUnitSpaceCount() {
		return ( true == isset( $this->m_intUnitSpaceCount ) ) ? ( string ) $this->m_intUnitSpaceCount : 'NULL';
	}

	public function setMpInsured( $intMpInsured ) {
		$this->set( 'm_intMpInsured', CStrings::strToIntDef( $intMpInsured, NULL, false ) );
	}

	public function getMpInsured() {
		return $this->m_intMpInsured;
	}

	public function sqlMpInsured() {
		return ( true == isset( $this->m_intMpInsured ) ) ? ( string ) $this->m_intMpInsured : 'NULL';
	}

	public function setMoveInMpMtd( $intMoveInMpMtd ) {
		$this->set( 'm_intMoveInMpMtd', CStrings::strToIntDef( $intMoveInMpMtd, NULL, false ) );
	}

	public function getMoveInMpMtd() {
		return $this->m_intMoveInMpMtd;
	}

	public function sqlMoveInMpMtd() {
		return ( true == isset( $this->m_intMoveInMpMtd ) ) ? ( string ) $this->m_intMoveInMpMtd : 'NULL';
	}

	public function setExceptionUnitCount( $intExceptionUnitCount ) {
		$this->set( 'm_intExceptionUnitCount', CStrings::strToIntDef( $intExceptionUnitCount, NULL, false ) );
	}

	public function getExceptionUnitCount() {
		return $this->m_intExceptionUnitCount;
	}

	public function sqlExceptionUnitCount() {
		return ( true == isset( $this->m_intExceptionUnitCount ) ) ? ( string ) $this->m_intExceptionUnitCount : 'NULL';
	}

	public function setMoveInExceptionUnitMtd( $intMoveInExceptionUnitMtd ) {
		$this->set( 'm_intMoveInExceptionUnitMtd', CStrings::strToIntDef( $intMoveInExceptionUnitMtd, NULL, false ) );
	}

	public function getMoveInExceptionUnitMtd() {
		return $this->m_intMoveInExceptionUnitMtd;
	}

	public function sqlMoveInExceptionUnitMtd() {
		return ( true == isset( $this->m_intMoveInExceptionUnitMtd ) ) ? ( string ) $this->m_intMoveInExceptionUnitMtd : 'NULL';
	}

	public function setTotalOccupantsCount( $intTotalOccupantsCount ) {
		$this->set( 'm_intTotalOccupantsCount', CStrings::strToIntDef( $intTotalOccupantsCount, NULL, false ) );
	}

	public function getTotalOccupantsCount() {
		return $this->m_intTotalOccupantsCount;
	}

	public function sqlTotalOccupantsCount() {
		return ( true == isset( $this->m_intTotalOccupantsCount ) ) ? ( string ) $this->m_intTotalOccupantsCount : 'NULL';
	}

	public function setTotalResponsibleOccupantCount( $intTotalResponsibleOccupantCount ) {
		$this->set( 'm_intTotalResponsibleOccupantCount', CStrings::strToIntDef( $intTotalResponsibleOccupantCount, NULL, false ) );
	}

	public function getTotalResponsibleOccupantCount() {
		return $this->m_intTotalResponsibleOccupantCount;
	}

	public function sqlTotalResponsibleOccupantCount() {
		return ( true == isset( $this->m_intTotalResponsibleOccupantCount ) ) ? ( string ) $this->m_intTotalResponsibleOccupantCount : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, property_name, unit_count, occupied_units, total_insured_units, ri_insured, non_ri_insured, total_ri, ri_premium, ri_earned_premium, moved_in_mtd, moved_in_insured_mtd, move_in_ri_mtd, is_active, updated_by, updated_on, created_by, created_on, unit_space_count, mp_insured, move_in_mp_mtd, exception_unit_count, move_in_exception_unit_mtd, total_occupants_count, total_responsible_occupant_count )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlPropertyName() . ', ' .
		          $this->sqlUnitCount() . ', ' .
		          $this->sqlOccupiedUnits() . ', ' .
		          $this->sqlTotalInsuredUnits() . ', ' .
		          $this->sqlRiInsured() . ', ' .
		          $this->sqlNonRiInsured() . ', ' .
		          $this->sqlTotalRi() . ', ' .
		          $this->sqlRiPremium() . ', ' .
		          $this->sqlRiEarnedPremium() . ', ' .
		          $this->sqlMovedInMtd() . ', ' .
		          $this->sqlMovedInInsuredMtd() . ', ' .
		          $this->sqlMoveInRiMtd() . ', ' .
		          $this->sqlIsActive() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlUnitSpaceCount() . ', ' .
		          $this->sqlMpInsured() . ', ' .
		          $this->sqlMoveInMpMtd() . ', ' .
		          $this->sqlExceptionUnitCount() . ', ' .
		          $this->sqlMoveInExceptionUnitMtd() . ', ' .
		          $this->sqlTotalOccupantsCount() . ', ' .
		          $this->sqlTotalResponsibleOccupantCount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupied_units = ' . $this->sqlOccupiedUnits(). ',' ; } elseif( true == array_key_exists( 'OccupiedUnits', $this->getChangedColumns() ) ) { $strSql .= ' occupied_units = ' . $this->sqlOccupiedUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_insured_units = ' . $this->sqlTotalInsuredUnits(). ',' ; } elseif( true == array_key_exists( 'TotalInsuredUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_insured_units = ' . $this->sqlTotalInsuredUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_insured = ' . $this->sqlRiInsured(). ',' ; } elseif( true == array_key_exists( 'RiInsured', $this->getChangedColumns() ) ) { $strSql .= ' ri_insured = ' . $this->sqlRiInsured() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_ri_insured = ' . $this->sqlNonRiInsured(). ',' ; } elseif( true == array_key_exists( 'NonRiInsured', $this->getChangedColumns() ) ) { $strSql .= ' non_ri_insured = ' . $this->sqlNonRiInsured() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_ri = ' . $this->sqlTotalRi(). ',' ; } elseif( true == array_key_exists( 'TotalRi', $this->getChangedColumns() ) ) { $strSql .= ' total_ri = ' . $this->sqlTotalRi() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_premium = ' . $this->sqlRiPremium(). ',' ; } elseif( true == array_key_exists( 'RiPremium', $this->getChangedColumns() ) ) { $strSql .= ' ri_premium = ' . $this->sqlRiPremium() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_earned_premium = ' . $this->sqlRiEarnedPremium(). ',' ; } elseif( true == array_key_exists( 'RiEarnedPremium', $this->getChangedColumns() ) ) { $strSql .= ' ri_earned_premium = ' . $this->sqlRiEarnedPremium() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' moved_in_mtd = ' . $this->sqlMovedInMtd(). ',' ; } elseif( true == array_key_exists( 'MovedInMtd', $this->getChangedColumns() ) ) { $strSql .= ' moved_in_mtd = ' . $this->sqlMovedInMtd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' moved_in_insured_mtd = ' . $this->sqlMovedInInsuredMtd(). ',' ; } elseif( true == array_key_exists( 'MovedInInsuredMtd', $this->getChangedColumns() ) ) { $strSql .= ' moved_in_insured_mtd = ' . $this->sqlMovedInInsuredMtd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_ri_mtd = ' . $this->sqlMoveInRiMtd(). ',' ; } elseif( true == array_key_exists( 'MoveInRiMtd', $this->getChangedColumns() ) ) { $strSql .= ' move_in_ri_mtd = ' . $this->sqlMoveInRiMtd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mp_insured = ' . $this->sqlMpInsured(). ',' ; } elseif( true == array_key_exists( 'MpInsured', $this->getChangedColumns() ) ) { $strSql .= ' mp_insured = ' . $this->sqlMpInsured() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_mp_mtd = ' . $this->sqlMoveInMpMtd(). ',' ; } elseif( true == array_key_exists( 'MoveInMpMtd', $this->getChangedColumns() ) ) { $strSql .= ' move_in_mp_mtd = ' . $this->sqlMoveInMpMtd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exception_unit_count = ' . $this->sqlExceptionUnitCount(). ',' ; } elseif( true == array_key_exists( 'ExceptionUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' exception_unit_count = ' . $this->sqlExceptionUnitCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' move_in_exception_unit_mtd = ' . $this->sqlMoveInExceptionUnitMtd(). ',' ; } elseif( true == array_key_exists( 'MoveInExceptionUnitMtd', $this->getChangedColumns() ) ) { $strSql .= ' move_in_exception_unit_mtd = ' . $this->sqlMoveInExceptionUnitMtd() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_occupants_count = ' . $this->sqlTotalOccupantsCount(). ',' ; } elseif( true == array_key_exists( 'TotalOccupantsCount', $this->getChangedColumns() ) ) { $strSql .= ' total_occupants_count = ' . $this->sqlTotalOccupantsCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_responsible_occupant_count = ' . $this->sqlTotalResponsibleOccupantCount(). ',' ; } elseif( true == array_key_exists( 'TotalResponsibleOccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' total_responsible_occupant_count = ' . $this->sqlTotalResponsibleOccupantCount() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'property_name' => $this->getPropertyName(),
			'unit_count' => $this->getUnitCount(),
			'occupied_units' => $this->getOccupiedUnits(),
			'total_insured_units' => $this->getTotalInsuredUnits(),
			'ri_insured' => $this->getRiInsured(),
			'non_ri_insured' => $this->getNonRiInsured(),
			'total_ri' => $this->getTotalRi(),
			'ri_premium' => $this->getRiPremium(),
			'ri_earned_premium' => $this->getRiEarnedPremium(),
			'moved_in_mtd' => $this->getMovedInMtd(),
			'moved_in_insured_mtd' => $this->getMovedInInsuredMtd(),
			'move_in_ri_mtd' => $this->getMoveInRiMtd(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'unit_space_count' => $this->getUnitSpaceCount(),
			'mp_insured' => $this->getMpInsured(),
			'move_in_mp_mtd' => $this->getMoveInMpMtd(),
			'exception_unit_count' => $this->getExceptionUnitCount(),
			'move_in_exception_unit_mtd' => $this->getMoveInExceptionUnitMtd(),
			'total_occupants_count' => $this->getTotalOccupantsCount(),
			'total_responsible_occupant_count' => $this->getTotalResponsibleOccupantCount()
		);
	}

}
?>