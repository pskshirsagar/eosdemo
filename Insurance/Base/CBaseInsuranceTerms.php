<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceTerms
 * Do not add any new functions to this class.
 */

class CBaseInsuranceTerms extends CEosPluralBase {

	/**
	 * @return CInsuranceTerm[]
	 */
	public static function fetchInsuranceTerms( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceTerm', $objDatabase );
	}

	/**
	 * @return CInsuranceTerm
	 */
	public static function fetchInsuranceTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceTerm', $objDatabase );
	}

	public static function fetchInsuranceTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_terms', $objDatabase );
	}

	public static function fetchInsuranceTermById( $intId, $objDatabase ) {
		return self::fetchInsuranceTerm( sprintf( 'SELECT * FROM insurance_terms WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>