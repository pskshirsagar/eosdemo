<?php

class CBaseInsuranceCarrierState extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_carrier_states';

	protected $m_intId;
	protected $m_intInsuranceCarrierId;
	protected $m_intInsuranceCarrierClassificationTypeId;
	protected $m_strStateCode;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['insurance_carrier_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierId', trim( $arrValues['insurance_carrier_id'] ) ); elseif( isset( $arrValues['insurance_carrier_id'] ) ) $this->setInsuranceCarrierId( $arrValues['insurance_carrier_id'] );
		if( isset( $arrValues['insurance_carrier_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInsuranceCarrierClassificationTypeId', trim( $arrValues['insurance_carrier_classification_type_id'] ) ); elseif( isset( $arrValues['insurance_carrier_classification_type_id'] ) ) $this->setInsuranceCarrierClassificationTypeId( $arrValues['insurance_carrier_classification_type_id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->set( 'm_intInsuranceCarrierId', CStrings::strToIntDef( $intInsuranceCarrierId, NULL, false ) );
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function sqlInsuranceCarrierId() {
		return ( true == isset( $this->m_intInsuranceCarrierId ) ) ? ( string ) $this->m_intInsuranceCarrierId : 'NULL';
	}

	public function setInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId ) {
		$this->set( 'm_intInsuranceCarrierClassificationTypeId', CStrings::strToIntDef( $intInsuranceCarrierClassificationTypeId, NULL, false ) );
	}

	public function getInsuranceCarrierClassificationTypeId() {
		return $this->m_intInsuranceCarrierClassificationTypeId;
	}

	public function sqlInsuranceCarrierClassificationTypeId() {
		return ( true == isset( $this->m_intInsuranceCarrierClassificationTypeId ) ) ? ( string ) $this->m_intInsuranceCarrierClassificationTypeId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 10, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'insurance_carrier_id' => $this->getInsuranceCarrierId(),
			'insurance_carrier_classification_type_id' => $this->getInsuranceCarrierClassificationTypeId(),
			'state_code' => $this->getStateCode()
		);
	}

}
?>