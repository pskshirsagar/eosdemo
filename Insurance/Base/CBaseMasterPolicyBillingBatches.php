<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CMasterPolicyBillingBatches
 * Do not add any new functions to this class.
 */

class CBaseMasterPolicyBillingBatches extends CEosPluralBase {

	/**
	 * @return CMasterPolicyBillingBatch[]
	 */
	public static function fetchMasterPolicyBillingBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMasterPolicyBillingBatch::class, $objDatabase );
	}

	/**
	 * @return CMasterPolicyBillingBatch
	 */
	public static function fetchMasterPolicyBillingBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMasterPolicyBillingBatch::class, $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'master_policy_billing_batches', $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatch( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchesByCid( $intCid, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatches( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatches( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchesByPropertyAccountId( $intPropertyAccountId, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatches( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE property_account_id = %d', $intPropertyAccountId ), $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchesByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatches( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE transaction_id = %d', $intTransactionId ), $objDatabase );
	}

	public static function fetchMasterPolicyBillingBatchesByMasterPolicyBatchTypeId( $intMasterPolicyBatchTypeId, $objDatabase ) {
		return self::fetchMasterPolicyBillingBatches( sprintf( 'SELECT * FROM master_policy_billing_batches WHERE master_policy_batch_type_id = %d', $intMasterPolicyBatchTypeId ), $objDatabase );
	}

}
?>