<?php

class CBaseConsumerRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.consumer_requests';

	protected $m_intId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strPolicyReferenceNumber;
	protected $m_strPhoneNumber;
	protected $m_strStateCode;
	protected $m_strStreetLine;
	protected $m_strIpAddress;
	protected $m_strConsumerRequestTypeIds;
	protected $m_jsonConsumerRequestTypeIds;
	protected $m_strComments;
	protected $m_boolIsReviewed;
	protected $m_boolIsResponded;
	protected $m_intReviewedBy;
	protected $m_intRespondedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strStateCode = 'CA';
		$this->m_boolIsReviewed = false;
		$this->m_boolIsResponded = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['policy_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strPolicyReferenceNumber', trim( $arrValues['policy_reference_number'] ) ); elseif( isset( $arrValues['policy_reference_number'] ) ) $this->setPolicyReferenceNumber( $arrValues['policy_reference_number'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['street_line'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine', trim( $arrValues['street_line'] ) ); elseif( isset( $arrValues['street_line'] ) ) $this->setStreetLine( $arrValues['street_line'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['consumer_request_type_ids'] ) ) $this->set( 'm_strConsumerRequestTypeIds', trim( $arrValues['consumer_request_type_ids'] ) );
		if( isset( $arrValues['comments'] ) && $boolDirectSet ) $this->set( 'm_strComments', trim( $arrValues['comments'] ) ); elseif( isset( $arrValues['comments'] ) ) $this->setComments( $arrValues['comments'] );
		if( isset( $arrValues['is_reviewed'] ) && $boolDirectSet ) $this->set( 'm_boolIsReviewed', trim( stripcslashes( $arrValues['is_reviewed'] ) ) ); elseif( isset( $arrValues['is_reviewed'] ) ) $this->setIsReviewed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reviewed'] ) : $arrValues['is_reviewed'] );
		if( isset( $arrValues['is_responded'] ) && $boolDirectSet ) $this->set( 'm_boolIsResponded', trim( stripcslashes( $arrValues['is_responded'] ) ) ); elseif( isset( $arrValues['is_responded'] ) ) $this->setIsResponded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_responded'] ) : $arrValues['is_responded'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['responded_by'] ) && $boolDirectSet ) $this->set( 'm_intRespondedBy', trim( $arrValues['responded_by'] ) ); elseif( isset( $arrValues['responded_by'] ) ) $this->setRespondedBy( $arrValues['responded_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 100, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 100, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 100, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPolicyReferenceNumber( $strPolicyReferenceNumber ) {
		$this->set( 'm_strPolicyReferenceNumber', CStrings::strTrimDef( $strPolicyReferenceNumber, 64, NULL, true ) );
	}

	public function getPolicyReferenceNumber() {
		return $this->m_strPolicyReferenceNumber;
	}

	public function sqlPolicyReferenceNumber() {
		return ( true == isset( $this->m_strPolicyReferenceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPolicyReferenceNumber ) : '\'' . addslashes( $this->m_strPolicyReferenceNumber ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 100, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : '\'CA\'';
	}

	public function setStreetLine( $strStreetLine ) {
		$this->set( 'm_strStreetLine', CStrings::strTrimDef( $strStreetLine, 240, NULL, true ) );
	}

	public function getStreetLine() {
		return $this->m_strStreetLine;
	}

	public function sqlStreetLine() {
		return ( true == isset( $this->m_strStreetLine ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine ) : '\'' . addslashes( $this->m_strStreetLine ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, -1, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setConsumerRequestTypeIds( $jsonConsumerRequestTypeIds ) {
		if( true == valObj( $jsonConsumerRequestTypeIds, 'stdClass' ) ) {
			$this->set( 'm_jsonConsumerRequestTypeIds', $jsonConsumerRequestTypeIds );
		} elseif( true == valJsonString( $jsonConsumerRequestTypeIds ) ) {
			$this->set( 'm_jsonConsumerRequestTypeIds', CStrings::strToJson( $jsonConsumerRequestTypeIds ) );
		} else {
			$this->set( 'm_jsonConsumerRequestTypeIds', NULL ); 
		}
		unset( $this->m_strConsumerRequestTypeIds );
	}

	public function getConsumerRequestTypeIds() {
		if( true == isset( $this->m_strConsumerRequestTypeIds ) ) {
			$this->m_jsonConsumerRequestTypeIds = CStrings::strToJson( $this->m_strConsumerRequestTypeIds );
			unset( $this->m_strConsumerRequestTypeIds );
		}
		return $this->m_jsonConsumerRequestTypeIds;
	}

	public function sqlConsumerRequestTypeIds() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getConsumerRequestTypeIds() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getConsumerRequestTypeIds() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getConsumerRequestTypeIds() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setComments( $strComments ) {
		$this->set( 'm_strComments', CStrings::strTrimDef( $strComments, -1, NULL, true ) );
	}

	public function getComments() {
		return $this->m_strComments;
	}

	public function sqlComments() {
		return ( true == isset( $this->m_strComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strComments ) : '\'' . addslashes( $this->m_strComments ) . '\'' ) : 'NULL';
	}

	public function setIsReviewed( $boolIsReviewed ) {
		$this->set( 'm_boolIsReviewed', CStrings::strToBool( $boolIsReviewed ) );
	}

	public function getIsReviewed() {
		return $this->m_boolIsReviewed;
	}

	public function sqlIsReviewed() {
		return ( true == isset( $this->m_boolIsReviewed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReviewed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsResponded( $boolIsResponded ) {
		$this->set( 'm_boolIsResponded', CStrings::strToBool( $boolIsResponded ) );
	}

	public function getIsResponded() {
		return $this->m_boolIsResponded;
	}

	public function sqlIsResponded() {
		return ( true == isset( $this->m_boolIsResponded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsResponded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setRespondedBy( $intRespondedBy ) {
		$this->set( 'm_intRespondedBy', CStrings::strToIntDef( $intRespondedBy, NULL, false ) );
	}

	public function getRespondedBy() {
		return $this->m_intRespondedBy;
	}

	public function sqlRespondedBy() {
		return ( true == isset( $this->m_intRespondedBy ) ) ? ( string ) $this->m_intRespondedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name_first, name_last, email_address, policy_reference_number, phone_number, state_code, street_line, ip_address, consumer_request_type_ids, comments, is_reviewed, is_responded, reviewed_by, responded_by, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPolicyReferenceNumber() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlStreetLine() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlConsumerRequestTypeIds() . ', ' .
						$this->sqlComments() . ', ' .
						$this->sqlIsReviewed() . ', ' .
						$this->sqlIsResponded() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						$this->sqlRespondedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_reference_number = ' . $this->sqlPolicyReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'PolicyReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' policy_reference_number = ' . $this->sqlPolicyReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line = ' . $this->sqlStreetLine(). ',' ; } elseif( true == array_key_exists( 'StreetLine', $this->getChangedColumns() ) ) { $strSql .= ' street_line = ' . $this->sqlStreetLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_request_type_ids = ' . $this->sqlConsumerRequestTypeIds(). ',' ; } elseif( true == array_key_exists( 'ConsumerRequestTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' consumer_request_type_ids = ' . $this->sqlConsumerRequestTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comments = ' . $this->sqlComments(). ',' ; } elseif( true == array_key_exists( 'Comments', $this->getChangedColumns() ) ) { $strSql .= ' comments = ' . $this->sqlComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed(). ',' ; } elseif( true == array_key_exists( 'IsReviewed', $this->getChangedColumns() ) ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_responded = ' . $this->sqlIsResponded(). ',' ; } elseif( true == array_key_exists( 'IsResponded', $this->getChangedColumns() ) ) { $strSql .= ' is_responded = ' . $this->sqlIsResponded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responded_by = ' . $this->sqlRespondedBy(). ',' ; } elseif( true == array_key_exists( 'RespondedBy', $this->getChangedColumns() ) ) { $strSql .= ' responded_by = ' . $this->sqlRespondedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'email_address' => $this->getEmailAddress(),
			'policy_reference_number' => $this->getPolicyReferenceNumber(),
			'phone_number' => $this->getPhoneNumber(),
			'state_code' => $this->getStateCode(),
			'street_line' => $this->getStreetLine(),
			'ip_address' => $this->getIpAddress(),
			'consumer_request_type_ids' => $this->getConsumerRequestTypeIds(),
			'comments' => $this->getComments(),
			'is_reviewed' => $this->getIsReviewed(),
			'is_responded' => $this->getIsResponded(),
			'reviewed_by' => $this->getReviewedBy(),
			'responded_by' => $this->getRespondedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>