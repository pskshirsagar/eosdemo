<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyAdditionalInterests
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyAdditionalInterests extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyAdditionalInterest[]
	 */
	public static function fetchInsurancePolicyAdditionalInterests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyAdditionalInterest', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyAdditionalInterest
	 */
	public static function fetchInsurancePolicyAdditionalInterest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyAdditionalInterest', $objDatabase );
	}

	public static function fetchInsurancePolicyAdditionalInterestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_additional_interests', $objDatabase );
	}

	public static function fetchInsurancePolicyAdditionalInterestById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyAdditionalInterest( sprintf( 'SELECT * FROM insurance_policy_additional_interests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyAdditionalInterestsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePolicyAdditionalInterests( sprintf( 'SELECT * FROM insurance_policy_additional_interests WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsurancePolicyAdditionalInterestsByAdditionalInterestsId( $intAdditionalInterestsId, $objDatabase ) {
		return self::fetchInsurancePolicyAdditionalInterests( sprintf( 'SELECT * FROM insurance_policy_additional_interests WHERE additional_interests_id = %d', ( int ) $intAdditionalInterestsId ), $objDatabase );
	}

}
?>