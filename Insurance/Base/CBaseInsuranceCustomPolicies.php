<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCustomPolicies
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCustomPolicies extends CEosPluralBase {

	/**
	 * @return CInsuranceCustomPolicy[]
	 */
	public static function fetchInsuranceCustomPolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceCustomPolicy::class, $objDatabase );
	}

	/**
	 * @return CInsuranceCustomPolicy
	 */
	public static function fetchInsuranceCustomPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceCustomPolicy::class, $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_custom_policies', $objDatabase );
	}

	public static function fetchInsuranceCustomPolicyById( $intId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicy( sprintf( 'SELECT * FROM insurance_custom_policies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByUnitSpaceId( $intUnitSpaceId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE unit_space_id = %d', ( int ) $intUnitSpaceId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByPolicyCarrierId( $intPolicyCarrierId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE policy_carrier_id = %d', ( int ) $intPolicyCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByPolicyStatusTypeId( $intPolicyStatusTypeId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE policy_status_type_id = %d', ( int ) $intPolicyStatusTypeId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByLiabilityCarrierLimitId( $intLiabilityCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE liability_carrier_limit_id = %d', ( int ) $intLiabilityCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByPersonalCarrierLimitId( $intPersonalCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE personal_carrier_limit_id = %d', ( int ) $intPersonalCarrierLimitId ), $objDatabase );
	}

	public static function fetchInsuranceCustomPoliciesByDeductibleCarrierLimitId( $intDeductibleCarrierLimitId, $objDatabase ) {
		return self::fetchInsuranceCustomPolicies( sprintf( 'SELECT * FROM insurance_custom_policies WHERE deductible_carrier_limit_id = %d', ( int ) $intDeductibleCarrierLimitId ), $objDatabase );
	}

}
?>