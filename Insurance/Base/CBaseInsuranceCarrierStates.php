<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceCarrierStates
 * Do not add any new functions to this class.
 */

class CBaseInsuranceCarrierStates extends CEosPluralBase {

	/**
	 * @return CInsuranceCarrierState[]
	 */
	public static function fetchInsuranceCarrierStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceCarrierState', $objDatabase );
	}

	/**
	 * @return CInsuranceCarrierState
	 */
	public static function fetchInsuranceCarrierState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceCarrierState', $objDatabase );
	}

	public static function fetchInsuranceCarrierStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_carrier_states', $objDatabase );
	}

	public static function fetchInsuranceCarrierStateById( $intId, $objDatabase ) {
		return self::fetchInsuranceCarrierState( sprintf( 'SELECT * FROM insurance_carrier_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierStatesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceCarrierStates( sprintf( 'SELECT * FROM insurance_carrier_states WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceCarrierStatesByInsuranceCarrierClassificationTypeId( $intInsuranceCarrierClassificationTypeId, $objDatabase ) {
		return self::fetchInsuranceCarrierStates( sprintf( 'SELECT * FROM insurance_carrier_states WHERE insurance_carrier_classification_type_id = %d', ( int ) $intInsuranceCarrierClassificationTypeId ), $objDatabase );
	}

}
?>