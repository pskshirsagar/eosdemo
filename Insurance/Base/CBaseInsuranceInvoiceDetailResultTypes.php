<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceInvoiceDetailResultTypes
 * Do not add any new functions to this class.
 */

class CBaseInsuranceInvoiceDetailResultTypes extends CEosPluralBase {

	/**
	 * @return CInsuranceInvoiceDetailResultType[]
	 */
	public static function fetchInsuranceInvoiceDetailResultTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceInvoiceDetailResultType', $objDatabase );
	}

	/**
	 * @return CInsuranceInvoiceDetailResultType
	 */
	public static function fetchInsuranceInvoiceDetailResultType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceInvoiceDetailResultType', $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailResultTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_invoice_detail_result_types', $objDatabase );
	}

	public static function fetchInsuranceInvoiceDetailResultTypeById( $intId, $objDatabase ) {
		return self::fetchInsuranceInvoiceDetailResultType( sprintf( 'SELECT * FROM insurance_invoice_detail_result_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>