<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceEmailNotifications
 * Do not add any new functions to this class.
 */

class CBaseInsuranceEmailNotifications extends CEosPluralBase {

	/**
	 * @return CInsuranceEmailNotification[]
	 */
	public static function fetchInsuranceEmailNotifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsuranceEmailNotification::class, $objDatabase );
	}

	/**
	 * @return CInsuranceEmailNotification
	 */
	public static function fetchInsuranceEmailNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsuranceEmailNotification::class, $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_email_notifications', $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationBy( $int, $objDatabase ) {
		return self::fetchInsuranceEmailNotification( sprintf( 'SELECT * FROM insurance_email_notifications WHERE	= %d', $int ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByCid( $intCid, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE insurance_policy_id = %d', $intInsurancePolicyId ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByEmailTypeId( $intEmailTypeId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE email_type_id = %d', $intEmailTypeId ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE system_email_id = %d', $intSystemEmailId ), $objDatabase );
	}

	public static function fetchInsuranceEmailNotificationsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchInsuranceEmailNotifications( sprintf( 'SELECT * FROM insurance_email_notifications WHERE lease_id = %d', $intLeaseId ), $objDatabase );
	}

}
?>