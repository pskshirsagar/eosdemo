<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsuranceProductLimits
 * Do not add any new functions to this class.
 */

class CBaseInsuranceProductLimits extends CEosPluralBase {

	/**
	 * @return CInsuranceProductLimit[]
	 */
	public static function fetchInsuranceProductLimits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsuranceProductLimit', $objDatabase );
	}

	/**
	 * @return CInsuranceProductLimit
	 */
	public static function fetchInsuranceProductLimit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsuranceProductLimit', $objDatabase );
	}

	public static function fetchInsuranceProductLimitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_product_limits', $objDatabase );
	}

	public static function fetchInsuranceProductLimitById( $intId, $objDatabase ) {
		return self::fetchInsuranceProductLimit( sprintf( 'SELECT * FROM insurance_product_limits WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsuranceProductLimitsByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsuranceProductLimits( sprintf( 'SELECT * FROM insurance_product_limits WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

	public static function fetchInsuranceProductLimitsByInsurancePolicyTypeId( $intInsurancePolicyTypeId, $objDatabase ) {
		return self::fetchInsuranceProductLimits( sprintf( 'SELECT * FROM insurance_product_limits WHERE insurance_policy_type_id = %d', ( int ) $intInsurancePolicyTypeId ), $objDatabase );
	}

	public static function fetchInsuranceProductLimitsByInsuranceLimitTypeId( $intInsuranceLimitTypeId, $objDatabase ) {
		return self::fetchInsuranceProductLimits( sprintf( 'SELECT * FROM insurance_product_limits WHERE insurance_limit_type_id = %d', ( int ) $intInsuranceLimitTypeId ), $objDatabase );
	}

	public static function fetchInsuranceProductLimitsByInsuranceAmountTypeId( $intInsuranceAmountTypeId, $objDatabase ) {
		return self::fetchInsuranceProductLimits( sprintf( 'SELECT * FROM insurance_product_limits WHERE insurance_amount_type_id = %d', ( int ) $intInsuranceAmountTypeId ), $objDatabase );
	}

}
?>