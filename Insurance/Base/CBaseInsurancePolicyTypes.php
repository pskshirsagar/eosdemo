<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyType[]
	 */
	public static function fetchInsurancePolicyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyType', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyType
	 */
	public static function fetchInsurancePolicyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyType', $objDatabase );
	}

	public static function fetchInsurancePolicyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_types', $objDatabase );
	}

	public static function fetchInsurancePolicyTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyType( sprintf( 'SELECT * FROM insurance_policy_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>