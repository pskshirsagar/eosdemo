<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePaymentReferences
 * Do not add any new functions to this class.
 */

class CBaseInsurancePaymentReferences extends CEosPluralBase {

	/**
	 * @return CInsurancePaymentReference[]
	 */
	public static function fetchInsurancePaymentReferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInsurancePaymentReference::class, $objDatabase );
	}

	/**
	 * @return CInsurancePaymentReference
	 */
	public static function fetchInsurancePaymentReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInsurancePaymentReference::class, $objDatabase );
	}

	public static function fetchInsurancePaymentReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_payment_references', $objDatabase );
	}

	public static function fetchInsurancePaymentReferenceById( $intId, $objDatabase ) {
		return self::fetchInsurancePaymentReference( sprintf( 'SELECT * FROM insurance_payment_references WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePaymentReferencesByInsurancePolicyId( $intInsurancePolicyId, $objDatabase ) {
		return self::fetchInsurancePaymentReferences( sprintf( 'SELECT * FROM insurance_payment_references WHERE insurance_policy_id = %d', ( int ) $intInsurancePolicyId ), $objDatabase );
	}

}
?>