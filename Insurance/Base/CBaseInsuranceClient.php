<?php

class CBaseInsuranceClient extends CEosSingularBase {

	const TABLE_NAME = 'public.insurance_clients';

	protected $m_intId;
	protected $m_intCid;
	protected $m_strClientName;
	protected $m_intTotalProperties;
	protected $m_intRiActiveProperties;
	protected $m_intRiSetupProperties;
	protected $m_intTotalUnits;
	protected $m_intRiActiveUnits;
	protected $m_intSetupUnits;
	protected $m_strContractDate;
	protected $m_strTrainingDate;
	protected $m_fltFeesPerUnit;
	protected $m_fltDiscountPerUnit;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intTotalUnitSpaces;
	protected $m_intCompanyStatusTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalProperties = '0';
		$this->m_intRiActiveProperties = '0';
		$this->m_intRiSetupProperties = '0';
		$this->m_intTotalUnits = '0';
		$this->m_intRiActiveUnits = '0';
		$this->m_intSetupUnits = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['client_name'] ) && $boolDirectSet ) $this->set( 'm_strClientName', trim( $arrValues['client_name'] ) ); elseif( isset( $arrValues['client_name'] ) ) $this->setClientName( $arrValues['client_name'] );
		if( isset( $arrValues['total_properties'] ) && $boolDirectSet ) $this->set( 'm_intTotalProperties', trim( $arrValues['total_properties'] ) ); elseif( isset( $arrValues['total_properties'] ) ) $this->setTotalProperties( $arrValues['total_properties'] );
		if( isset( $arrValues['ri_active_properties'] ) && $boolDirectSet ) $this->set( 'm_intRiActiveProperties', trim( $arrValues['ri_active_properties'] ) ); elseif( isset( $arrValues['ri_active_properties'] ) ) $this->setRiActiveProperties( $arrValues['ri_active_properties'] );
		if( isset( $arrValues['ri_setup_properties'] ) && $boolDirectSet ) $this->set( 'm_intRiSetupProperties', trim( $arrValues['ri_setup_properties'] ) ); elseif( isset( $arrValues['ri_setup_properties'] ) ) $this->setRiSetupProperties( $arrValues['ri_setup_properties'] );
		if( isset( $arrValues['total_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnits', trim( $arrValues['total_units'] ) ); elseif( isset( $arrValues['total_units'] ) ) $this->setTotalUnits( $arrValues['total_units'] );
		if( isset( $arrValues['ri_active_units'] ) && $boolDirectSet ) $this->set( 'm_intRiActiveUnits', trim( $arrValues['ri_active_units'] ) ); elseif( isset( $arrValues['ri_active_units'] ) ) $this->setRiActiveUnits( $arrValues['ri_active_units'] );
		if( isset( $arrValues['setup_units'] ) && $boolDirectSet ) $this->set( 'm_intSetupUnits', trim( $arrValues['setup_units'] ) ); elseif( isset( $arrValues['setup_units'] ) ) $this->setSetupUnits( $arrValues['setup_units'] );
		if( isset( $arrValues['contract_date'] ) && $boolDirectSet ) $this->set( 'm_strContractDate', trim( $arrValues['contract_date'] ) ); elseif( isset( $arrValues['contract_date'] ) ) $this->setContractDate( $arrValues['contract_date'] );
		if( isset( $arrValues['training_date'] ) && $boolDirectSet ) $this->set( 'm_strTrainingDate', trim( $arrValues['training_date'] ) ); elseif( isset( $arrValues['training_date'] ) ) $this->setTrainingDate( $arrValues['training_date'] );
		if( isset( $arrValues['fees_per_unit'] ) && $boolDirectSet ) $this->set( 'm_fltFeesPerUnit', trim( $arrValues['fees_per_unit'] ) ); elseif( isset( $arrValues['fees_per_unit'] ) ) $this->setFeesPerUnit( $arrValues['fees_per_unit'] );
		if( isset( $arrValues['discount_per_unit'] ) && $boolDirectSet ) $this->set( 'm_fltDiscountPerUnit', trim( $arrValues['discount_per_unit'] ) ); elseif( isset( $arrValues['discount_per_unit'] ) ) $this->setDiscountPerUnit( $arrValues['discount_per_unit'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['total_unit_spaces'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitSpaces', trim( $arrValues['total_unit_spaces'] ) ); elseif( isset( $arrValues['total_unit_spaces'] ) ) $this->setTotalUnitSpaces( $arrValues['total_unit_spaces'] );
		if( isset( $arrValues['company_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyStatusTypeId', trim( $arrValues['company_status_type_id'] ) ); elseif( isset( $arrValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrValues['company_status_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setClientName( $strClientName ) {
		$this->set( 'm_strClientName', CStrings::strTrimDef( $strClientName, 250, NULL, true ) );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function sqlClientName() {
		return ( true == isset( $this->m_strClientName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strClientName ) : '\'' . addslashes( $this->m_strClientName ) . '\'' ) : 'NULL';
	}

	public function setTotalProperties( $intTotalProperties ) {
		$this->set( 'm_intTotalProperties', CStrings::strToIntDef( $intTotalProperties, NULL, false ) );
	}

	public function getTotalProperties() {
		return $this->m_intTotalProperties;
	}

	public function sqlTotalProperties() {
		return ( true == isset( $this->m_intTotalProperties ) ) ? ( string ) $this->m_intTotalProperties : '0';
	}

	public function setRiActiveProperties( $intRiActiveProperties ) {
		$this->set( 'm_intRiActiveProperties', CStrings::strToIntDef( $intRiActiveProperties, NULL, false ) );
	}

	public function getRiActiveProperties() {
		return $this->m_intRiActiveProperties;
	}

	public function sqlRiActiveProperties() {
		return ( true == isset( $this->m_intRiActiveProperties ) ) ? ( string ) $this->m_intRiActiveProperties : '0';
	}

	public function setRiSetupProperties( $intRiSetupProperties ) {
		$this->set( 'm_intRiSetupProperties', CStrings::strToIntDef( $intRiSetupProperties, NULL, false ) );
	}

	public function getRiSetupProperties() {
		return $this->m_intRiSetupProperties;
	}

	public function sqlRiSetupProperties() {
		return ( true == isset( $this->m_intRiSetupProperties ) ) ? ( string ) $this->m_intRiSetupProperties : '0';
	}

	public function setTotalUnits( $intTotalUnits ) {
		$this->set( 'm_intTotalUnits', CStrings::strToIntDef( $intTotalUnits, NULL, false ) );
	}

	public function getTotalUnits() {
		return $this->m_intTotalUnits;
	}

	public function sqlTotalUnits() {
		return ( true == isset( $this->m_intTotalUnits ) ) ? ( string ) $this->m_intTotalUnits : '0';
	}

	public function setRiActiveUnits( $intRiActiveUnits ) {
		$this->set( 'm_intRiActiveUnits', CStrings::strToIntDef( $intRiActiveUnits, NULL, false ) );
	}

	public function getRiActiveUnits() {
		return $this->m_intRiActiveUnits;
	}

	public function sqlRiActiveUnits() {
		return ( true == isset( $this->m_intRiActiveUnits ) ) ? ( string ) $this->m_intRiActiveUnits : '0';
	}

	public function setSetupUnits( $intSetupUnits ) {
		$this->set( 'm_intSetupUnits', CStrings::strToIntDef( $intSetupUnits, NULL, false ) );
	}

	public function getSetupUnits() {
		return $this->m_intSetupUnits;
	}

	public function sqlSetupUnits() {
		return ( true == isset( $this->m_intSetupUnits ) ) ? ( string ) $this->m_intSetupUnits : '0';
	}

	public function setContractDate( $strContractDate ) {
		$this->set( 'm_strContractDate', CStrings::strTrimDef( $strContractDate, -1, NULL, true ) );
	}

	public function getContractDate() {
		return $this->m_strContractDate;
	}

	public function sqlContractDate() {
		return ( true == isset( $this->m_strContractDate ) ) ? '\'' . $this->m_strContractDate . '\'' : 'NULL';
	}

	public function setTrainingDate( $strTrainingDate ) {
		$this->set( 'm_strTrainingDate', CStrings::strTrimDef( $strTrainingDate, -1, NULL, true ) );
	}

	public function getTrainingDate() {
		return $this->m_strTrainingDate;
	}

	public function sqlTrainingDate() {
		return ( true == isset( $this->m_strTrainingDate ) ) ? '\'' . $this->m_strTrainingDate . '\'' : 'NULL';
	}

	public function setFeesPerUnit( $fltFeesPerUnit ) {
		$this->set( 'm_fltFeesPerUnit', CStrings::strToFloatDef( $fltFeesPerUnit, NULL, false, 2 ) );
	}

	public function getFeesPerUnit() {
		return $this->m_fltFeesPerUnit;
	}

	public function sqlFeesPerUnit() {
		return ( true == isset( $this->m_fltFeesPerUnit ) ) ? ( string ) $this->m_fltFeesPerUnit : 'NULL';
	}

	public function setDiscountPerUnit( $fltDiscountPerUnit ) {
		$this->set( 'm_fltDiscountPerUnit', CStrings::strToFloatDef( $fltDiscountPerUnit, NULL, false, 2 ) );
	}

	public function getDiscountPerUnit() {
		return $this->m_fltDiscountPerUnit;
	}

	public function sqlDiscountPerUnit() {
		return ( true == isset( $this->m_fltDiscountPerUnit ) ) ? ( string ) $this->m_fltDiscountPerUnit : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTotalUnitSpaces( $intTotalUnitSpaces ) {
		$this->set( 'm_intTotalUnitSpaces', CStrings::strToIntDef( $intTotalUnitSpaces, NULL, false ) );
	}

	public function getTotalUnitSpaces() {
		return $this->m_intTotalUnitSpaces;
	}

	public function sqlTotalUnitSpaces() {
		return ( true == isset( $this->m_intTotalUnitSpaces ) ) ? ( string ) $this->m_intTotalUnitSpaces : 'NULL';
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->set( 'm_intCompanyStatusTypeId', CStrings::strToIntDef( $intCompanyStatusTypeId, NULL, false ) );
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function sqlCompanyStatusTypeId() {
		return ( true == isset( $this->m_intCompanyStatusTypeId ) ) ? ( string ) $this->m_intCompanyStatusTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, client_name, total_properties, ri_active_properties, ri_setup_properties, total_units, ri_active_units, setup_units, contract_date, training_date, fees_per_unit, discount_per_unit, updated_by, updated_on, created_by, created_on, total_unit_spaces, company_status_type_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlClientName() . ', ' .
		          $this->sqlTotalProperties() . ', ' .
		          $this->sqlRiActiveProperties() . ', ' .
		          $this->sqlRiSetupProperties() . ', ' .
		          $this->sqlTotalUnits() . ', ' .
		          $this->sqlRiActiveUnits() . ', ' .
		          $this->sqlSetupUnits() . ', ' .
		          $this->sqlContractDate() . ', ' .
		          $this->sqlTrainingDate() . ', ' .
		          $this->sqlFeesPerUnit() . ', ' .
		          $this->sqlDiscountPerUnit() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlTotalUnitSpaces() . ', ' .
		          $this->sqlCompanyStatusTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_name = ' . $this->sqlClientName(). ',' ; } elseif( true == array_key_exists( 'ClientName', $this->getChangedColumns() ) ) { $strSql .= ' client_name = ' . $this->sqlClientName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_properties = ' . $this->sqlTotalProperties(). ',' ; } elseif( true == array_key_exists( 'TotalProperties', $this->getChangedColumns() ) ) { $strSql .= ' total_properties = ' . $this->sqlTotalProperties() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_active_properties = ' . $this->sqlRiActiveProperties(). ',' ; } elseif( true == array_key_exists( 'RiActiveProperties', $this->getChangedColumns() ) ) { $strSql .= ' ri_active_properties = ' . $this->sqlRiActiveProperties() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_setup_properties = ' . $this->sqlRiSetupProperties(). ',' ; } elseif( true == array_key_exists( 'RiSetupProperties', $this->getChangedColumns() ) ) { $strSql .= ' ri_setup_properties = ' . $this->sqlRiSetupProperties() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits(). ',' ; } elseif( true == array_key_exists( 'TotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ri_active_units = ' . $this->sqlRiActiveUnits(). ',' ; } elseif( true == array_key_exists( 'RiActiveUnits', $this->getChangedColumns() ) ) { $strSql .= ' ri_active_units = ' . $this->sqlRiActiveUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_units = ' . $this->sqlSetupUnits(). ',' ; } elseif( true == array_key_exists( 'SetupUnits', $this->getChangedColumns() ) ) { $strSql .= ' setup_units = ' . $this->sqlSetupUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_date = ' . $this->sqlContractDate(). ',' ; } elseif( true == array_key_exists( 'ContractDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_date = ' . $this->sqlContractDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_date = ' . $this->sqlTrainingDate(). ',' ; } elseif( true == array_key_exists( 'TrainingDate', $this->getChangedColumns() ) ) { $strSql .= ' training_date = ' . $this->sqlTrainingDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fees_per_unit = ' . $this->sqlFeesPerUnit(). ',' ; } elseif( true == array_key_exists( 'FeesPerUnit', $this->getChangedColumns() ) ) { $strSql .= ' fees_per_unit = ' . $this->sqlFeesPerUnit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discount_per_unit = ' . $this->sqlDiscountPerUnit(). ',' ; } elseif( true == array_key_exists( 'DiscountPerUnit', $this->getChangedColumns() ) ) { $strSql .= ' discount_per_unit = ' . $this->sqlDiscountPerUnit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_unit_spaces = ' . $this->sqlTotalUnitSpaces(). ',' ; } elseif( true == array_key_exists( 'TotalUnitSpaces', $this->getChangedColumns() ) ) { $strSql .= ' total_unit_spaces = ' . $this->sqlTotalUnitSpaces() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'client_name' => $this->getClientName(),
			'total_properties' => $this->getTotalProperties(),
			'ri_active_properties' => $this->getRiActiveProperties(),
			'ri_setup_properties' => $this->getRiSetupProperties(),
			'total_units' => $this->getTotalUnits(),
			'ri_active_units' => $this->getRiActiveUnits(),
			'setup_units' => $this->getSetupUnits(),
			'contract_date' => $this->getContractDate(),
			'training_date' => $this->getTrainingDate(),
			'fees_per_unit' => $this->getFeesPerUnit(),
			'discount_per_unit' => $this->getDiscountPerUnit(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'total_unit_spaces' => $this->getTotalUnitSpaces(),
			'company_status_type_id' => $this->getCompanyStatusTypeId()
		);
	}

}
?>