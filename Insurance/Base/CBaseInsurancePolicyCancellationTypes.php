<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyCancellationTypes
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyCancellationTypes extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyCancellationType[]
	 */
	public static function fetchInsurancePolicyCancellationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyCancellationType', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyCancellationType
	 */
	public static function fetchInsurancePolicyCancellationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyCancellationType', $objDatabase );
	}

	public static function fetchInsurancePolicyCancellationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_cancellation_types', $objDatabase );
	}

	public static function fetchInsurancePolicyCancellationTypeById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyCancellationType( sprintf( 'SELECT * FROM insurance_policy_cancellation_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyCancellationTypesByInsuranceCarrierId( $intInsuranceCarrierId, $objDatabase ) {
		return self::fetchInsurancePolicyCancellationTypes( sprintf( 'SELECT * FROM insurance_policy_cancellation_types WHERE insurance_carrier_id = %d', ( int ) $intInsuranceCarrierId ), $objDatabase );
	}

}
?>