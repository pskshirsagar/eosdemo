<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Insurance\CInsurancePolicyEventErrors
 * Do not add any new functions to this class.
 */

class CBaseInsurancePolicyEventErrors extends CEosPluralBase {

	/**
	 * @return CInsurancePolicyEventError[]
	 */
	public static function fetchInsurancePolicyEventErrors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInsurancePolicyEventError', $objDatabase );
	}

	/**
	 * @return CInsurancePolicyEventError
	 */
	public static function fetchInsurancePolicyEventError( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInsurancePolicyEventError', $objDatabase );
	}

	public static function fetchInsurancePolicyEventErrorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'insurance_policy_event_errors', $objDatabase );
	}

	public static function fetchInsurancePolicyEventErrorById( $intId, $objDatabase ) {
		return self::fetchInsurancePolicyEventError( sprintf( 'SELECT * FROM insurance_policy_event_errors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInsurancePolicyEventErrorsByEventId( $intEventId, $objDatabase ) {
		return self::fetchInsurancePolicyEventErrors( sprintf( 'SELECT * FROM insurance_policy_event_errors WHERE event_id = %d', ( int ) $intEventId ), $objDatabase );
	}

}
?>