<?php

class CInsuranceBlockedCounty extends CBaseInsuranceBlockedCounty {

    public function valStateCode() {
        $boolIsValid = true;
        if( true == is_null( $this->getStateCode() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State is required. ' ) );
        }
        return $boolIsValid;
    }

    public function valCountyCode() {
        $boolIsValid = true;
        if( true == is_null( $this->getCountyCode() ) && false == is_null( $this->getStateCode() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'county_code', 'County is required. ' ) );
        }
        return $boolIsValid;
    }

    public function valNotes() {
        $boolIsValid = true;
        if( true == is_null( $this->getNotes() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Note is required. ' ) );
        }
        return $boolIsValid;
    }

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		if( true == is_null( $this->getInsuranceCarrierId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_carrier_id', 'Insurance carrier is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valStartdate() {
		$boolIsValid = true;
		if( true == is_null( $this->getStartdate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date is required. ' ) );
		}
		return $boolIsValid;
	}

    public function validateDuplicateCounties( $arrobjInsuranceBlockedCounties, $objInsurancePortalDatabase ) {

    	$boolIsValid = true;

    	if( false == valArr( $arrobjInsuranceBlockedCounties ) ) {
    		return $boolIsValid;
    	}

    	foreach( $arrobjInsuranceBlockedCounties as $objInsuranceBlockedCounty ) {
    		if( $objInsuranceBlockedCounty->getStateCode() == $this->getStateCode() ) {
    			if( 'ALL' == $objInsuranceBlockedCounty->getCountyCode() ) {
   					$boolIsValid = false;
        			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'County is already blocked. ' ) );
   					break 1;
   				}

   				if( $this->getCountyCode() == $objInsuranceBlockedCounty->getCountyCode() ) {
   					$boolIsValid = false;
   					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'County is already blocked. ' ) );
   					break 1;
   				}
  			}
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valStateCode();
            	$boolIsValid &= $this->valCountyCode();
            	$boolIsValid &= $this->valNotes();
	            $boolIsValid &= $this->valInsuranceCarrierId();
	            $boolIsValid &= $this->valStartdate();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>