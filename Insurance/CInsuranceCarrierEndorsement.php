<?php

class CInsuranceCarrierEndorsement extends CBaseInsuranceCarrierEndorsement {

	protected $m_strInsuranceEndorsementName;
	protected $m_strInsuranceEndorsementDescription;
	protected $m_strInsuranceEndorsementLongDescription;

	const EARTHQUAKE			   = 5;
	const FIREMAN_RELIEF_SURCHARGE = 6;
	const KY_MUNICIPAL_TAX	       = 7;

    /**
     * Get Functions
     *
     */

    public function getInsuranceEndorsementName() {
    	return $this->m_strInsuranceEndorsementName;
    }

	public function getInsuranceEndorsementDescription() {
    	return $this->m_strInsuranceEndorsementDescription;
    }

	public function getInsuranceEndorsementLongDescription() {
    	return $this->m_strInsuranceEndorsementLongDescription;
    }

    /**
     * Set Functions
     *
     */

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrmixValues['name'] ) ) 	$this->setInsuranceEndorsementName( $arrmixValues['name'] );
    	if( true == isset( $arrmixValues['description'] ) ) 	$this->setInsuranceEndorsementDescription( $arrmixValues['description'] );
    	if( true == isset( $arrmixValues['long_description'] ) ) 	$this->setInsuranceEndorsementLongDescription( $arrmixValues['long_description'] );
    }

    public function setInsuranceEndorsementName( $strEndorsementName ) {
		$this->m_strInsuranceEndorsementName = $strEndorsementName;
    }

	public function setInsuranceEndorsementDescription( $strEndorsementDescription ) {
		$this->m_strInsuranceEndorsementDescription = $strEndorsementDescription;
    }

	public function setInsuranceEndorsementLongDescription( $strEndorsementLongDescription ) {
		$this->m_strInsuranceEndorsementLongDescription = $strEndorsementLongDescription;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>