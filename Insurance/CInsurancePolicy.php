<?php

use Psi\Eos\Admin\CAccounts;

class CInsurancePolicy extends CBaseInsurancePolicy {

	const NUMBER = '0100131072';
	const RI_GENERIC_CID = 4668;

	protected $m_objInsuranceCarrier;
	protected $m_objAccount;
	protected $m_objLastInsuranceCharge;
	protected $m_objResidentInsurancePolicy;
	protected $m_objUnAccountedLiabilityCharge;
	protected $m_objInsurancePolicyLiabilityCharge;
	protected $m_objInsuranceCarrierProperty;

	protected $m_arrobjUnprocessedReturnedCharges;
	protected $m_arrobjInsurancePolicyInsureds;
	protected $m_arrobjInsurancePolicyLimits;
	protected $m_arrobjInsurancePolicyCoverageDetails;

	protected $m_arrintInsurancePolicyEndorsements;

	protected $m_fltPaymentAmount;
	protected $m_fltTotalPaidAmount;

	protected $m_strProofOfCoverageFileName;
	protected $m_strProofOfCoverageFilePath;
	protected $m_strPaymentPeriodStartDate;
	protected $m_strPaymentPeriodEndDate;
	protected $m_strCarrierPeriodStartDate;
	protected $m_strCarrierPeriodEndDate;
	protected $m_strNextCarrierPeriodStartDate;
	protected $m_strConfirmCancel;
	protected $m_strPropertyPhoneNumber;
	protected $m_strPropertyName;
	protected $m_strPrimaryInsuredNameFirst;
	protected $m_strPrimaryInsuredNameLast;
	protected $m_strPrimaryInsuredEmailAddress;
	protected $m_strPrimaryInsuredPhoneNumber;
	protected $m_strCarrierName;
	protected $m_strNextChargeDate;
	protected $m_strPaymentReferenceNumber;
	protected $m_strCoverageStartDate;
	protected $m_strCoverageEndDate;
	protected $m_strCoveragePaidThroughDate;

	protected $m_boolIsSeekingProperty;
	protected $m_boolAgreesToTerms;
	protected $m_boolAgreesToIDTheftTerms;
	protected $m_boolAgreesToNoLosses;
	protected $m_boolAgreesToPayConvenienceFees;
	protected $m_boolIsSameMailingAddress;
	protected $m_boolPolicyCarrierIsIntegrated;
	protected $m_boolIsFuture;
	protected $m_boolIsCarrierNotified;
	protected $m_boolIsInsuredChanged;
	protected $m_boolIsIncludeSpouse;
	protected $m_boolIsMinRateCall;

	protected $m_intCoApplicantCount;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intTotalInsureeCount;
	protected $m_intInsuranceChargeId;
	protected $m_intEventId;
	protected $m_intSourceCid;

	protected $m_intOrignalCid;
	protected $m_intOriginalPropertyId;

	protected $m_fltPaymentCollectionAmount;
	protected $m_fltConvenienceFee;
	protected $m_fltMonthlyLiabilityAmount;
	protected $m_fltMonthlyEarnedPremiumAmount;
	protected $m_fltMonthlyAdjustmentAmount;
	protected $m_fltProviderLiabilityAmount;
	protected $m_fltChargeAmount;

	protected $m_boolIsBillingAddressSame;

	protected $m_intPremiumTransactionId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'property_id', 'Property is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->getInsuranceCarrierId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'insurance_carrier_id', 'Insurance Provider is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		$strStartDate = $this->getEffectiveDate();

		if( true == is_null( $strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'effective_date', 'Coverage Start Date is required.', NULL ) );
		}

		if( false == is_null( $strStartDate ) && 0 < strlen( $strStartDate ) && 1 !== CValidation::checkDate( $strStartDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Coverage Start Date must be in mm/dd/yyyy form.' ) );
		}

		return $boolIsValid;
	}

	public function valCarrierPolicyNumber( $boolIsCustomPolicy = false ) {
		$boolIsValid = true;

		if( true == is_null( $this->getCarrierPolicyNumber() ) ) {
			$boolIsValid = false;

			if( true == $boolIsCustomPolicy ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'custom_policy_number', 'Policy Number is required.' ) );

			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'carrier_policy_number', 'Policy Number is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valIsSeekingProperty() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsSeekingProperty() ) || 0 == strlen( $this->getIsSeekingProperty() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'is_seeking_property', 'Please select Yes or No.' ) );

		} elseif( 0 == $this->getIsSeekingProperty() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'is_seeking_property', 'Can not proceed with \'No\' option.' ) );
		}

		return $boolIsValid;
	}

	public function valAgreesToTerms() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToTerms ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'term_and_condition', 'Agreeing to terms is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToIDTheftTerms() {
		$boolIsValid = true;

		if( false == is_null( $this->getCurrentInsurancePolicyCoverageDetail()->getIDTheftPremiumAmount() ) && false == $this->m_boolAgreesToIDTheftTerms ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'id_theft', 'Agreeing to ID Theft Protection is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToPayConvenienceFees() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToPayConvenienceFees ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'convenience_fee', 'Agreeing to pay admin fee is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valAgreesToNoLosses() {
		$boolIsValid = true;

		if( false == $this->m_boolAgreesToNoLosses ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'no_losses', 'Agreeing to no losses is required.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valCustomEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Start date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCustomEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCancelPolicyEffectiveEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cancellation_effective_end_date', 'Cancellation date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCustomStartAndEndDate() {
		$boolIsValid = true;
		if( false == is_null( $this->getEffectiveDate() ) && false == is_null( $this->getEndDate() ) ) {
			$strStartDate 	= strtotime( $this->getEffectiveDate() );
			$strEndDate 	= strtotime( $this->getEndDate() );

			if( $strEndDate == $strStartDate ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', 'Start & End date should not be same.' ) );
			} elseif( $strEndDate < $strStartDate ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', ' Start date should not greater than the end date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valConfirmCancel() {
		$boolIsValid = true;

		if( true == is_null( $this->getConfirmCancel() ) || 0 == strlen( $this->getConfirmCancel() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'confirm_cancel', 'Confirm cancel is required.', NULL ) );
		}
		if( true == $boolIsValid ) {

			if( $this->getConfirmCancel() != 'CANCEL' ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'confirm_cancel', 'Confirm cancel is not valid.', NULL ) );
			}
		}

		return $boolIsValid;

	}

	public function valConfirmPolicyCancellationReason() {
		$boolIsValid = true;

		if( true == is_null( $this->getInsurancePolicyCancellationTypeId() ) || 0 == strlen( $this->getInsurancePolicyCancellationTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'insurance_policy_cancellation_type_id', 'Policy Cancellation Reason is required.', NULL ) );

		}
		return $boolIsValid;
	}

	public function valAdditionalInsureeCount( $intAdditionalInsureeCount ) {
		$boolIsValid = true;

		if( false == is_numeric( $intAdditionalInsureeCount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'additional_insured', 'Selection is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valInsuranceCarrierId();
				$boolIsValid &= $this->valEffectiveDate();
				break;

			case VALIDATE_UPDATE:
				break;

			case 'validate_is_seeking_property':
				$boolIsValid &= $this->valIsSeekingProperty();
				break;

			case 'validate_property':
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'validate_coverage_info':
				$boolIsValid &= $this->valEffectiveDate();
				break;

			case 'validate_policy_summary_info':
				$boolIsValid &= $this->valAgreesToPayConvenienceFees();
				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valAgreesToIDTheftTerms();
				if( strtotime( date( 'm/d/Y' ) ) == strtotime( $this->getEffectiveDate() ) ) {
					$boolIsValid &= $this->valAgreesToNoLosses();
				}
				break;

			case 'validate_address_info':
				$boolIsValid &= $this->valStreetLine1();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				break;

			case 'validate_cancel_policy':
				$boolIsValid &= $this->valConfirmCancel();
				break;

			case 'validate_cancellation_policy':
				$boolIsValid &= $this->valConfirmPolicyCancellationReason();
				$boolIsValid &= $this->valCancelPolicyEffectiveEndDate();
				break;

			case 'validate_cancellation_policy_effective_end_date':
				$boolIsValid &= $this->valCancelPolicyEffectiveEndDate();
				break;

			case 'validate_terms':
				$boolIsValid &= $this->valAgreesToTerms();
				$boolIsValid &= $this->valAgreesToIDTheftTerms();
				if( strtotime( date( 'm/d/Y' ) ) == strtotime( $this->getEffectiveDate() ) ) {
					$boolIsValid &= $this->valAgreesToNoLosses();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Update Functions
	 *
	 */

	public function updatePostDates( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {
		$strSql = 'SELECT * ' .
		          'FROM insurance_policies_update_post_dates( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlInsurancePolicyStatusTypeId() . ', ' .
		          $this->sqlLastPostedOn() . ', ' .
		          $this->sqlNextPostOn() . ', ' .
		          ( int ) $intCurrentUserId . ' ) AS result;';

		return $this->executeUpdateFunction( $strSql, $objDatabase, $objClientDatabase );
	}

	public function updatePaymentFailure( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {
		$strSql = 'SELECT * ' .
		          'FROM insurance_policies_update_payment_failure( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlInsurancePolicyStatusTypeId() . ', ' .
		          $this->sqlIsPaymentFailed() . ', ' .
		          $this->sqlLapsedOn() . ', ' .
		          ( int ) $intCurrentUserId . ' ) AS result;';

		return $this->executeUpdateFunction( $strSql, $objDatabase, $objClientDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function executeUpdateFunction( $strSql, $objDatabase, $objClientDatabase = NULL ) {

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update insurance policy record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update insurance policy record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrstrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrstrValues['type'], $arrstrValues['field'], $arrstrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && false == is_null( $this->getInsuranceCarrierId() ) ) {
			return $this->insertOrUpdateResidentInsurancePolicy( CUser::ID_INSURANCE_PORTAL, $intPropertyId = NULL, $objDatabase, $objClientDatabase );
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objInsurancePortalDatabase, $objClientDatabase = NULL ) {

		if( false == valId( $this->getCid() ) ) {
			trigger_error( 'Creating policy with out cid [' . $this->getPropertyId() . ']', E_USER_WARNING );
		}

		$objInsuranceClient = \Psi\Eos\Insurance\CInsuranceClients::createService()->fetchInsuranceClientStatusTypeIdByCid( $this->getCid(), $objInsurancePortalDatabase );

		if( false == valObj( $objInsuranceClient, CInsuranceClient::class ) || false == in_array( $objInsuranceClient->getCompanyStatusTypeId(), [ CCompanyStatusType::CLIENT, CCompanyStatusType::TERMINATED ] ) ) {
			$this->setIsTest( true );
		}

		$boolIsValid = parent::insert( $intCurrentUserId, $objInsurancePortalDatabase );

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == $boolIsValid && false == is_null( $this->getInsuranceCarrierId() ) ) {
			$intPropertyId = $this->checkForKemperSingleFamily( $objClientDatabase );

			$boolIsValid &= $this->insertOrUpdateResidentInsurancePolicy( $intCurrentUserId, $intPropertyId, $objInsurancePortalDatabase, $objClientDatabase );
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objInsurancePortalDatabase, $objClientDatabase = NULL ) {

		if( false == valId( $this->getCid() ) ) {
			trigger_error( 'Creating policy with out cid [' . $this->getPropertyId() . ']', E_USER_WARNING );
		}

		$boolIsValid = parent::update( $intCurrentUserId, $objInsurancePortalDatabase );

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == $boolIsValid && false == is_null( $this->getInsuranceCarrierId() ) ) {
			$intPropertyId = $this->checkForKemperSingleFamily( $objClientDatabase );

			$boolIsValid &= $this->insertOrUpdateResidentInsurancePolicy( $intCurrentUserId, $intPropertyId, $objInsurancePortalDatabase, $objClientDatabase );
		}

		return $boolIsValid;
	}

	public function insertOrUpdateResidentInsurancePolicy( $intCurrentUserId, $intPropertyId, $objInsurancePortalDatabase, $objClientDatabase ) {

		if( CInsurancePolicy::RI_GENERIC_CID == $this->getCid() ) {
			return true;
		}

		$objInsurancePolicyCoverageDetail = $this->getCurrentInsurancePolicyCoverageDetail();

		if( CInsuranceCarrier::MARKEL == $this->getInsuranceCarrierId() ) {
				$strProviderName = 'Markel';
		} elseif( CInsuranceCarrier::KEMPER == $this->getInsuranceCarrierId() ) {
				$strProviderName = 'Kemper';
		} elseif( CInsuranceCarrier::QBE == $this->getInsuranceCarrierId() ) {
				$strProviderName = 'QBE';
		}

		if( false == is_null( $this->getLeaseId() ) ) {

			$this->m_objResidentInsurancePolicy = CResidentInsurancePolicies::fetchResidentInsurancePolicyByLeaseIdByInsurancePolicyIdByPropertyIdByCid( $this->getLeaseId(), $this->getId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );

			if( true == valObj( $this->m_objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {

				if( false == is_null( $this->getEndDate() ) ) {
					$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
				}

				if( false == is_null( $this->getConfirmedOn() ) ) {
					$this->m_objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );
				}

				if( false == is_null( $this->getCancelledOn() ) ) {
					$this->m_objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );
				}

				if( false == is_null( $this->getEffectiveDate() ) ) {
					$this->m_objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
				}

				$this->m_objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );

				$arrstrInsurancePolicyInsureds 			= $this->getInsurancePolicyInsuredsNames( $this, $objInsurancePortalDatabase );
				$arrmixInsurancePoliciesLimitAmounts 	= $this->getInsurancePolicyLimitAmounts( $this, $objInsurancePortalDatabase );

				if( true == array_key_exists( $this->getId(), $arrstrInsurancePolicyInsureds ) ) {
					$this->m_objResidentInsurancePolicy->setPrimaryInsuredName( $arrstrInsurancePolicyInsureds[$this->getId()]['primary_insured'] );

					if( true == array_key_exists( 'additional_insured', $arrstrInsurancePolicyInsureds[$this->getId()] ) ) {
						$this->m_objResidentInsurancePolicy->setAdditionalInsureds( implode( ',', $arrstrInsurancePolicyInsureds[$this->getId()]['additional_insured'] ) );
					}
				}

				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setDeductible( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setLiabilityLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setPersonalLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] );
				}

				$objClientDatabase->begin();

				if( false == $this->m_objResidentInsurancePolicy->update( $intCurrentUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}
				$objClientDatabase->commit();

			} else {

				$this->m_objResidentInsurancePolicy = CResidentInsurancePolicies::fetchResidentInsurancePolicyByInsurancePolicyIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(), $objClientDatabase );

				if( true == valObj( $this->m_objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {

					$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
					$this->m_objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );
					$this->m_objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );
					$this->m_objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );

					$arrstrInsurancePolicyInsureds 			= $this->getInsurancePolicyInsuredsNames( $this, $objInsurancePortalDatabase );
					$arrmixInsurancePoliciesLimitAmounts 	= $this->getInsurancePolicyLimitAmounts( $this, $objInsurancePortalDatabase );

					if( true == array_key_exists( $this->getId(), $arrstrInsurancePolicyInsureds ) ) {
						$this->m_objResidentInsurancePolicy->setPrimaryInsuredName( $arrstrInsurancePolicyInsureds[$this->getId()]['primary_insured'] );

						if( true == array_key_exists( 'additional_insured', $arrstrInsurancePolicyInsureds[$this->getId()] ) ) {
							$this->m_objResidentInsurancePolicy->setAdditionalInsureds( implode( ',', $arrstrInsurancePolicyInsureds[$this->getId()]['additional_insured'] ) );
						}
					}

					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setDeductible( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] );
					}
					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setLiabilityLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] );
					}
					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setPersonalLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] );
					}

					$objInsurancePolicyCustomer = new CInsurancePolicyCustomer();

					$objInsurancePolicyCustomer->setResidentInsurancePolicyId( $this->m_objResidentInsurancePolicy->getId() );
					$objInsurancePolicyCustomer->setCid( $this->m_objResidentInsurancePolicy->getCid() );
					$objInsurancePolicyCustomer->setPropertyId( $this->m_objResidentInsurancePolicy->getPropertyId() );
					$objInsurancePolicyCustomer->setLeaseId( $this->getLeaseId() );
					$objInsurancePolicyCustomer->setCustomerId( $this->getCustomerId() );
					$objInsurancePolicyCustomer->setApplicantId( $this->getApplicantId() );
					$objInsurancePolicyCustomer->setApplicationId( $this->getApplicationId() );
					$objInsurancePolicyCustomer->setIsInvalidMatch( 0 );

					if( false == is_null( $this->getEffectiveDate() ) ) {
						$this->m_objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
					}

					$objClientDatabase->begin();

					if( false == $this->m_objResidentInsurancePolicy->update( $intCurrentUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

					if( false == $objInsurancePolicyCustomer->insert( $intCurrentUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

					$objClientDatabase->commit();

				} else {

					$arrstrInsurancePolicyInsureds 			= $this->getInsurancePolicyInsuredsNames( $this, $objInsurancePortalDatabase );
					$arrmixInsurancePoliciesLimitAmounts 	= $this->getInsurancePolicyLimitAmounts( $this, $objInsurancePortalDatabase );

					$this->m_objResidentInsurancePolicy = new CResidentInsurancePolicy();

					$this->m_objResidentInsurancePolicy->setInsurancePolicyId( $this->getId() );
					$this->m_objResidentInsurancePolicy->setCid( $this->getCid() );

					$this->m_objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );
					$this->m_objResidentInsurancePolicy->setEntityId( $this->getEntityId() );

					if( true == empty( $intPropertyId ) ) {
						$this->m_objResidentInsurancePolicy->setPropertyId( $this->getPropertyId() );
					} else {
						$this->m_objResidentInsurancePolicy->setPropertyId( $intPropertyId );
					}

					$this->m_objResidentInsurancePolicy->setLeadSourceTypeId( $this->getLeadSourceTypeId() );
					$this->m_objResidentInsurancePolicy->setInsurancePolicyTypeId( $this->getInsurancePolicyTypeId() );

					$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
					$this->m_objResidentInsurancePolicy->setIsIntegrated( 1 );
					$this->m_objResidentInsurancePolicy->setUnitNumber( $objInsurancePolicyCoverageDetail->getUnitNumber() );
					$this->m_objResidentInsurancePolicy->setPolicyNumber( $objInsurancePolicyCoverageDetail->getCarrierPolicyNumber() );
					$this->m_objResidentInsurancePolicy->setInsuranceCarrierName( $strProviderName );

					if( true == array_key_exists( $this->getId(), $arrstrInsurancePolicyInsureds ) ) {
						$this->m_objResidentInsurancePolicy->setPrimaryInsuredName( $arrstrInsurancePolicyInsureds[$this->getId()]['primary_insured'] );

						if( true == array_key_exists( 'additional_insured', $arrstrInsurancePolicyInsureds[$this->getId()] ) ) {
							$this->m_objResidentInsurancePolicy->setAdditionalInsureds( implode( ',', $arrstrInsurancePolicyInsureds[$this->getId()]['additional_insured'] ) );
						}
					}

					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setDeductible( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] );
					}
					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setLiabilityLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] );
					}
					if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] ) ) {
						$this->m_objResidentInsurancePolicy->setPersonalLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] );
					}

					$this->m_objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
					$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
					$this->m_objResidentInsurancePolicy->setLapsedOn( $this->getLastPostedOn() );
					$this->m_objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );
					$this->m_objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );

					$objClientDatabase->begin();

					if( false == $this->m_objResidentInsurancePolicy->insert( $intCurrentUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

					$objInsurancePolicyCustomer = new CInsurancePolicyCustomer();

					$objInsurancePolicyCustomer->setResidentInsurancePolicyId( $this->m_objResidentInsurancePolicy->getId() );
					$objInsurancePolicyCustomer->setCid( $this->m_objResidentInsurancePolicy->getCid() );
					$objInsurancePolicyCustomer->setPropertyId( $this->m_objResidentInsurancePolicy->getPropertyId() );
					$objInsurancePolicyCustomer->setLeaseId( $this->getLeaseId() );
					$objInsurancePolicyCustomer->setCustomerId( $this->getCustomerId() );
					$objInsurancePolicyCustomer->setApplicantId( $this->getApplicantId() );
					$objInsurancePolicyCustomer->setApplicationId( $this->getApplicationId() );
					$objInsurancePolicyCustomer->setIsInvalidMatch( 0 );

					if( false == $objInsurancePolicyCustomer->insert( $intCurrentUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

					$objClientDatabase->commit();
				}

				try {

					$arrmixInsuredMessageData = [];
					$arrmixInsuredMessageData['cid'] = $this->getCId();
					$arrmixInsuredMessageData['property_id'] = $this->getPropertyId();
					$arrmixInsuredMessageData['insurance_policy_id'] = $this->getId();
					$arrmixInsuredMessageData['lease_id'] = $this->getLeaseId();
					$arrmixInsuredMessageData['customer_id'] = $this->getCustomerId();
					$arrmixInsuredMessageData['user_id'] = $intCurrentUserId;

					$objResidentInsuranceMessageSenderLibrary = new CResidentInsuranceMessageSenderLibrary();

					$objResidentInsuranceMessageSenderLibrary->setData( $arrmixInsuredMessageData );
					$objResidentInsuranceMessageSenderLibrary->setQueueName( CResidentInsuranceMatchPolicyMessage::MESSAGE_TYPE );
					$objResidentInsuranceMessageSenderLibrary->initialize();
					$objResidentInsuranceMessageSenderLibrary->send();

				} catch( Exception $objException ) {
					trigger_error( 'Unable to configure message Queue for policy id [' . $this->m_objInsurancePolicy->getId() . ']', E_USER_WARNING );
					return false;
				}
			}
		} else {
			$this->m_objResidentInsurancePolicy 	= CResidentInsurancePolicies::fetchResidentInsurancePolicyByInsurancePolicyIdByPropertyIdByCid( $this->getId(), $this->getPropertyId(), $this->getCid(),  $objClientDatabase );

			$arrstrInsurancePolicyInsureds 			= $this->getInsurancePolicyInsuredsNames( $this, $objInsurancePortalDatabase );
			$arrmixInsurancePoliciesLimitAmounts 	= $this->getInsurancePolicyLimitAmounts( $this, $objInsurancePortalDatabase );

			if( true == valObj( $this->m_objResidentInsurancePolicy, 'CResidentInsurancePolicy' ) ) {

				$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
				$this->m_objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );
				$this->m_objResidentInsurancePolicy->setPolicyNumber( $objInsurancePolicyCoverageDetail->getCarrierPolicyNumber() );
				$this->m_objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );
				$this->m_objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );

				if( false == is_null( $this->getEffectiveDate() ) ) {
					$this->m_objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
				}

				if( true == array_key_exists( $this->getId(), $arrstrInsurancePolicyInsureds ) ) {
					$this->m_objResidentInsurancePolicy->setPrimaryInsuredName( $arrstrInsurancePolicyInsureds[$this->getId()]['primary_insured'] );

					if( true == array_key_exists( 'additional_insured', $arrstrInsurancePolicyInsureds[$this->getId()] ) ) {
						$this->m_objResidentInsurancePolicy->setAdditionalInsureds( implode( ',', $arrstrInsurancePolicyInsureds[$this->getId()]['additional_insured'] ) );
					}
				}

				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setDeductible( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setLiabilityLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setPersonalLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] );
				}

				$objClientDatabase->begin();

				if( false == $this->m_objResidentInsurancePolicy->update( $intCurrentUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$objClientDatabase->commit();

			} else {

				$this->m_objResidentInsurancePolicy = new CResidentInsurancePolicy();

				$this->m_objResidentInsurancePolicy->setInsurancePolicyId( $this->getId() );
				$this->m_objResidentInsurancePolicy->setCid( $this->getCid() );
				$this->m_objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );
				$this->m_objResidentInsurancePolicy->setEntityId( $this->getEntityId() );

				if( true == empty( $intPropertyId ) ) {
					$this->m_objResidentInsurancePolicy->setPropertyId( $this->getPropertyId() );
				} else {
					$this->m_objResidentInsurancePolicy->setPropertyId( $intPropertyId );
				}

				$this->m_objResidentInsurancePolicy->setLeadSourceTypeId( $this->getLeadSourceTypeId() );
				$this->m_objResidentInsurancePolicy->setInsurancePolicyTypeId( $this->getInsurancePolicyTypeId() );
				$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
				$this->m_objResidentInsurancePolicy->setIsIntegrated( 1 );
				$this->m_objResidentInsurancePolicy->setUnitNumber( $objInsurancePolicyCoverageDetail->getUnitNumber() );
				$this->m_objResidentInsurancePolicy->setPolicyNumber( $objInsurancePolicyCoverageDetail->getCarrierPolicyNumber() );
				$this->m_objResidentInsurancePolicy->setInsuranceCarrierName( $strProviderName );

				if( true == array_key_exists( $this->getId(), $arrstrInsurancePolicyInsureds ) ) {
					$this->m_objResidentInsurancePolicy->setPrimaryInsuredName( $arrstrInsurancePolicyInsureds[$this->getId()]['primary_insured'] );

					if( true == array_key_exists( 'additional_insured', $arrstrInsurancePolicyInsureds[$this->getId()] ) ) {
						$this->m_objResidentInsurancePolicy->setAdditionalInsureds( implode( ',', $arrstrInsurancePolicyInsureds[$this->getId()]['additional_insured'] ) );
					}
				}

				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setDeductible( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['deductible_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setLiabilityLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['liability_amount'] );
				}
				if( true == isset( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] ) ) {
					$this->m_objResidentInsurancePolicy->setPersonalLimit( $arrmixInsurancePoliciesLimitAmounts[$this->getId()]['personal_amount'] );
				}

				$this->m_objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
				$this->m_objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
				$this->m_objResidentInsurancePolicy->setLapsedOn( $this->getLastPostedOn() );
				$this->m_objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );
				$this->m_objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );

				$objClientDatabase->begin();

				if( false == $this->m_objResidentInsurancePolicy->insert( $intCurrentUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$objClientDatabase->commit();
			}
		}

		return true;
	}

	public function checkForKemperSingleFamily( $objClientDatabase ) {

		if( CInsuranceCarrierClassificationType::KEMPER_PREFERRED_SINGLEFAMILY != $this->getInsuranceCarrierClassificationTypeId() ) {
			return;
		}

		$objInsurancePolicyCoverageDetail = $this->getCurrentInsurancePolicyCoverageDetail();

		if( true == valObj( $objInsurancePolicyCoverageDetail, 'CInsurancePolicyCoverageDetail' ) ) {

			$strStreetLine1	= $objInsurancePolicyCoverageDetail->getStreetLine1();
			$strPostalCode	= $objInsurancePolicyCoverageDetail->getPostalCode();
			$strStateCode 	= $objInsurancePolicyCoverageDetail->getStateCode();
			$strCity 		= $objInsurancePolicyCoverageDetail->getCity();

			$objPropertyAddress = \Psi\Eos\Entrata\CPropertyAddresses::createService()->fetchPropertyByAddressByAddressTypeIdByCid( $strStreetLine1, $strPostalCode, $strStateCode, $strCity, CAddressType::PRIMARY, $this->getCid(), $objClientDatabase );

			if( true == valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {

				$objInsurancePolicyCoverageDetail->setCity( $objPropertyAddress->getCity() );
				$objInsurancePolicyCoverageDetail->setStateCode( $objPropertyAddress->getStateCode() );
				$objInsurancePolicyCoverageDetail->setPostalCode( $objPropertyAddress->getPostalCode() );

				$this->setInsurancePolicyCoverageDetails( array( $objInsurancePolicyCoverageDetail ) );
				return $objPropertyAddress->getPropertyId();
			}
		}

		return;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( false == valId( $this->getCid() ) ) {
			trigger_error( 'Creating policy with out cid [' . $this->getPropertyId() . ']', E_USER_WARNING );
		}

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public function createInsuranceCharge() {

		$objInsuranceCharge = new CInsuranceCharge();
		$objInsuranceCharge->setEntityId( $this->getEntityId() );
		$objInsuranceCharge->setInsurancePolicyId( $this->getId() );

		return $objInsuranceCharge;
	}

	public function checkLeaseId( $objInsurancePolicyLease ) {

		$boolIsChanged = false;

		if( true == is_null( $objInsurancePolicyLease ) ) {
			return $boolIsChanged;
		}

		if( $this->getLeaseId() != $objInsurancePolicyLease->getLeaseId() ) {
			$boolIsChanged = true;
		}

		return $boolIsChanged;
	}

	public function getInsurancePolicyInsuredsNames( $objInsurancePolicy, $objInsuranceDatabase ) {

		$arrobjInsurancePolicyInsureds = \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchInsurancePolicyInsuredsByInsurancePolicyId( $objInsurancePolicy->getId(), $objInsuranceDatabase );
		$arrstrInsurancePolicyInsureds = array();

		if( true == valArr( $arrobjInsurancePolicyInsureds ) ) {

			foreach( $arrobjInsurancePolicyInsureds as $objInsurancePolicyInsured ) {

				$arrobjInsureds[$objInsurancePolicyInsured->getInsurancePolicyId()][] = $objInsurancePolicyInsured;
			}

			foreach( $arrobjInsureds as $intInsurancePolicyId => $arrobjInsured ) {

				foreach( $arrobjInsured as $objInsured ) {

					if( CInsuranceInsuredType::PRIMARY_INSURED == $objInsured->getInsuranceInsuredTypeId() ) {
						$arrstrInsurancePolicyInsureds[$intInsurancePolicyId]['primary_insured'] = $objInsured->getNameFirst() . ' ' . $objInsured->getNameLast();

					} else {
						$arrstrInsurancePolicyInsureds[$intInsurancePolicyId]['additional_insured'][] = $objInsured->getNameFirst() . ' ' . $objInsured->getNameLast();
					}
				}
			}
		}

		return $arrstrInsurancePolicyInsureds;
	}

	public function getInsurancePolicyLimitAmounts( $objInsurancePolicy, $objInsuranceDatabase ) {

		$intInsuranceCarrierId = ( int ) $objInsurancePolicy->getInsuranceCarrierId();
		$objInsurancePolicyCoverageDetail = $objInsurancePolicy->getCurrentInsurancePolicyCoverageDetail();

		$arrmixInsurancePolicyLimitAmounts = [];
		$arrobjInsuranceCarrierLiabilityLimits 	= \Psi\Eos\Insurance\CInsuranceCarrierLimits::createService()->fetchInsuranceCarrierLimitsByInsuranceLimitTypeIdByStateCode( $intInsuranceCarrierId, CInsuranceLimitType::LIABILITY, $objInsurancePolicyCoverageDetail->getStateCode(), $objInsuranceDatabase );
		$arrobjInsuranceCarrierPersonalLimits 	= \Psi\Eos\Insurance\CInsuranceCarrierLimits::createService()->fetchInsuranceCarrierLimitsByInsuranceLimitTypeIdByStateCode( $intInsuranceCarrierId, CInsuranceLimitType::PERSONAL, $objInsurancePolicyCoverageDetail->getStateCode(), $objInsuranceDatabase );
		$arrobjInsuranceCarrierDeductibleLimits = \Psi\Eos\Insurance\CInsuranceCarrierLimits::createService()->fetchInsuranceCarrierLimitsByInsuranceLimitTypeIdByStateCode( $intInsuranceCarrierId, CInsuranceLimitType::DEDUCTIBLE, $objInsurancePolicyCoverageDetail->getStateCode(), $objInsuranceDatabase );

		if( $intInsuranceCarrierId == CInsuranceCarrier::QBE ) {
			$objPropertyInsuranceLimit = \Psi\Eos\Insurance\CPropertyInsuranceLimits::createService()->fetchDefaultMedicalInsuranceLimitByInsuranceCarrierIdByInsuranceLimitTypeIdByPropertyId( $intInsuranceCarrierId, CInsuranceLimitType::MEDICALPAYMENT, $objInsurancePolicy->getPropertyId(), $objInsuranceDatabase );
		}

		if( true == valObj( $objInsurancePolicyCoverageDetail, 'CInsurancePolicyCoverageDetail' ) ) {

			if( false == is_null( $objInsurancePolicyCoverageDetail->getLiabilityInsuranceCarrierLimitId() ) && 0 != $objInsurancePolicyCoverageDetail->getLiabilityInsuranceCarrierLimitId() ) {

				if( true == valObj( $arrobjInsuranceCarrierLiabilityLimits[$objInsurancePolicyCoverageDetail->getLiabilityInsuranceCarrierLimitId()], 'CInsuranceCarrierLimit' ) ) {
					$arrmixInsurancePolicyLimitAmounts[$objInsurancePolicy->getId()]['liability_amount'] = $arrobjInsuranceCarrierLiabilityLimits[$objInsurancePolicyCoverageDetail->getLiabilityInsuranceCarrierLimitId()]->getInsuranceAmount();
				}
			}

			if( false == is_null( $objInsurancePolicyCoverageDetail->getPersonalInsuranceCarrierLimitId() ) && 0 != $objInsurancePolicyCoverageDetail->getPersonalInsuranceCarrierLimitId() ) {

				if( true == valObj( $arrobjInsuranceCarrierPersonalLimits[$objInsurancePolicyCoverageDetail->getPersonalInsuranceCarrierLimitId()], 'CInsuranceCarrierLimit' ) ) {
					$arrmixInsurancePolicyLimitAmounts[$objInsurancePolicy->getId()]['personal_amount'] = $arrobjInsuranceCarrierPersonalLimits[$objInsurancePolicyCoverageDetail->getPersonalInsuranceCarrierLimitId()]->getInsuranceAmount();
				}
			}

			if( false == is_null( $objInsurancePolicyCoverageDetail->getDeductibleInsuranceCarrierLimitId() ) && 0 != $objInsurancePolicyCoverageDetail->getDeductibleInsuranceCarrierLimitId() ) {

				if( true == valObj( $arrobjInsuranceCarrierDeductibleLimits[$objInsurancePolicyCoverageDetail->getDeductibleInsuranceCarrierLimitId()], 'CInsuranceCarrierLimit' ) ) {
					$arrmixInsurancePolicyLimitAmounts[$objInsurancePolicy->getId()]['deductible_amount'] = $arrobjInsuranceCarrierDeductibleLimits[$objInsurancePolicyCoverageDetail->getDeductibleInsuranceCarrierLimitId()]->getInsuranceAmount();
				}
			}

			if( $intInsuranceCarrierId == CInsuranceCarrier::QBE && true == valObj( $objPropertyInsuranceLimit, 'CPropertyInsuranceLimit' ) ) {
				$arrmixInsurancePolicyLimitAmounts[$objInsurancePolicy->getId()]['medical_payment_amount'] = $objPropertyInsuranceLimit->getInsuranceAmount();
			}
		}

		return $arrmixInsurancePolicyLimitAmounts;
	}

	public function validateRentalAddress( $arrmixRentalAddressDetails, $objInsurancePolicyCoverageDetail, $objEntity = NULL, $objInsurancePortalDatabase ) {

		$boolIsAddressCorrect = true;

		$arrmixAddressDetails = array();
		$arrmixAddressDetails['street_line_1'] 	= $arrmixRentalAddressDetails['street_line1'];
		$arrmixAddressDetails['unit_number']   	= $arrmixRentalAddressDetails['unit_number'];
		$arrmixAddressDetails['city'] 			= $arrmixRentalAddressDetails['city'];
		$arrmixAddressDetails['state_code'] 	= $arrmixRentalAddressDetails['state_code'];
		$arrmixAddressDetails['postal_code'] 	= $arrmixRentalAddressDetails['postal_code'];

		$arrmixResponseData = CGenericInsurancePolicyController::validateAddress( $arrmixAddressDetails, $objInsurancePortalDatabase );

		// ReturnCodes contains mutiple values
		if( 1 != $arrmixResponseData['ReturnCodes'] || ( 1 == $arrmixResponseData['ReturnCodes'] && true == isset( $arrmixResponseData['ErrorCodes'] ) ) ) {
			$boolIsAddressCorrect = false;
		} else {

			if( false == empty( $arrmixResponseData['CorrectedStreetLine1'] ) && $arrmixResponseData['CorrectedStreetLine1'] != $arrmixRentalAddressDetails['street_line1'] ) {

				$strCorrectedStreetLine1 = $arrmixResponseData['CorrectedStreetLine1'];

				if( true == valStr( $arrmixAddressDetails['unit_number'] ) && true == strpos( $strCorrectedStreetLine1, $arrmixAddressDetails['unit_number'] ) ) {

					$arrstrCorrectedStreetLine1 = explode( ' ', $strCorrectedStreetLine1 );

					// Removing unit number appended with a keyword on correct street line.
					$arrstrCorrectedStreetLine1[\Psi\Libraries\UtilFunctions\count( $arrstrCorrectedStreetLine1 ) - 1] = '';
					$arrstrCorrectedStreetLine1[\Psi\Libraries\UtilFunctions\count( $arrstrCorrectedStreetLine1 ) - 2] = '';

					$strCorrectedStreetLine1 = implode( ' ', $arrstrCorrectedStreetLine1 );
				}

				$objInsurancePolicyCoverageDetail->setStreetLine1( $strCorrectedStreetLine1 );
				if( true == valObj( $objEntity, 'CEntity' ) ) $objEntity->setStreetLine1( $strCorrectedStreetLine1 );

				if( true == $arrmixRentalAddressDetails['is_same_mailing_address'] ) {
					$objInsurancePolicyCoverageDetail->setMailingStreetLine1( $strCorrectedStreetLine1 );
					if( true == valObj( $objEntity, 'CEntity' ) ) $objEntity->setMailingStreetLine1( $strCorrectedStreetLine1 );
				}
			}
		}

		return $boolIsAddressCorrect;
	}

	public function validateBillingAddress( $arrmixBillingAddressDetails, $objAccount, $objInsurancePortalDatabase ) {

		$boolIsAddressCorrect = true;

		$arrmixAddressDetails = array();

		$arrmixAddressDetails['street_line_1'] = $arrmixBillingAddressDetails['billto_street_line1'];
		$arrmixAddressDetails['city'] 		   = $arrmixBillingAddressDetails['billto_city'];
		$arrmixAddressDetails['state_code']    = $arrmixBillingAddressDetails['billto_state_code'];
		$arrmixAddressDetails['postal_code']   = $arrmixBillingAddressDetails['billto_postal_code'];

		$arrmixResponseData = CGenericInsurancePolicyController::validateAddress( $arrmixAddressDetails, $objInsurancePortalDatabase );

		// ReturnCodes contains mutiple values
		if( 1 != $arrmixResponseData['ReturnCodes'] || ( 1 == $arrmixResponseData['ReturnCodes'] && true == isset( $arrmixResponseData['ErrorCodes'] ) ) ) {
			$boolIsAddressCorrect = false;
		} else {

			if( false == empty( $arrmixResponseData['CorrectedStreetLine1'] ) ) $objAccount->setBilltoStreetLine1( $arrmixResponseData['CorrectedStreetLine1'] );
			if( false == empty( $arrmixResponseData['CorrectedCity'] ) ) $objAccount->setBilltoCity( $arrmixResponseData['CorrectedCity'] );
			if( false == empty( $arrmixResponseData['CorrectedState'] ) ) $objAccount->setBilltoStateCode( $arrmixResponseData['CorrectedState'] );
			if( false == empty( $arrmixResponseData['CorrectedZip'] ) ) $objAccount->setBilltoPostalCode( $arrmixResponseData['CorrectedZip'] );
		}

		return $boolIsAddressCorrect;
	}

	public function validateMailingAddress( $arrmixMailingAddressDetails, $objInsurancePolicyCoverageDetail, $objInsurancePortalDatabase ) {

		$boolIsAddressCorrect = true;

		$arrmixAddressDetails = array();

		$arrmixAddressDetails['street_line_1'] = $arrmixMailingAddressDetails['mailing_street_line1'];
		$arrmixAddressDetails['city'] 		   = $arrmixMailingAddressDetails['mailing_city'];
		$arrmixAddressDetails['state_code']    = $arrmixMailingAddressDetails['mailing_state_code'];
		$arrmixAddressDetails['postal_code']   = $arrmixMailingAddressDetails['mailing_postal_code'];

		$arrmixResponseData = CGenericInsurancePolicyController::validateAddress( $arrmixAddressDetails, $objInsurancePortalDatabase );

		// ReturnCodes contains mutiple values
		if( 1 != $arrmixResponseData['ReturnCodes'] || ( 1 == $arrmixResponseData['ReturnCodes'] && true == isset( $arrmixResponseData['ErrorCodes'] ) ) ) {
			$boolIsAddressCorrect = false;
		} else {
			if( false == empty( $arrmixResponseData['CorrectedStreetLine1'] ) ) $objInsurancePolicyCoverageDetail->setMailingStreetLine1( $arrmixResponseData['CorrectedStreetLine1'] );
			if( false == empty( $arrmixResponseData['CorrectedCity'] ) ) $objInsurancePolicyCoverageDetail->setMailingCity( $arrmixResponseData['CorrectedCity'] );
			if( false == empty( $arrmixResponseData['CorrectedState'] ) ) $objInsurancePolicyCoverageDetail->setMailingStateCode( $arrmixResponseData['CorrectedState'] );
			if( false == empty( $arrmixResponseData['CorrectedZip'] ) ) $objInsurancePolicyCoverageDetail->setMailingPostalCode( $arrmixResponseData['CorrectedZip'] );
		}

		return $boolIsAddressCorrect;
	}

	public function validateCompanyBlackListAccounts( $objAccount, $objInsuranceDatabase ) {

		$boolIsValid = true;

		$objPaymentBlacklistEntry = new CPaymentBlacklistEntry();
		$objBlacklistedEntry = $objPaymentBlacklistEntry->checkBlacklistedBankAccount( $objInsuranceDatabase, $objAccount->getCheckRoutingNumber(), $objAccount->getCheckAccountNumber(), $objAccount->getCheckAccountTypeId(), $objAccount->getCheckNameOnAccount() );

		if( NULL == $objBlacklistedEntry ) {
			return true;
		}

		switch( $objBlacklistedEntry->getPaymentBlacklistTypeId() ) {
			case CPaymentBlacklistType::NONE:
				$boolIsValid = true;
			case CPaymentBlacklistType::ROUTING_NUMBER:
				$boolIsValid = false;
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getCurrentInsurancePolicyCoverageDetail() {

		$objCurrentInsurancePolicyCoverageDetail = NULL;
		$arrobjInsurancePolicyCoverageDetails 	 = $this->getInsurancePolicyCoverageDetails();

		if( valArr( $arrobjInsurancePolicyCoverageDetails ) ) {
			foreach( $arrobjInsurancePolicyCoverageDetails as $objInsurancePolicyCoverageDetail ) {
				if( true == valObj( $objInsurancePolicyCoverageDetail, 'CInsurancePolicyCoverageDetail' ) ) {
					if( CInsurancePolicyCoverageDetailStatusType::CURRENT == $objInsurancePolicyCoverageDetail->getInsurancePolicyDetailStatusTypeId() ) {
						$objCurrentInsurancePolicyCoverageDetail = clone $objInsurancePolicyCoverageDetail;
						break;
					}
				}
			}
		}

		return $objCurrentInsurancePolicyCoverageDetail;
	}

	public function getFutureInsurancePolicyCoverageDetail() {

		$objFutureInsurancePolicyCoverageDetail = NULL;
		$arrobjInsurancePolicyCoverageDetails 	= $this->getInsurancePolicyCoverageDetails();

		if( true == valArr( $arrobjInsurancePolicyCoverageDetails ) ) {
			foreach( $arrobjInsurancePolicyCoverageDetails as $objInsurancePolicyCoverageDetail ) {
				if( true == valObj( $objInsurancePolicyCoverageDetail, 'CInsurancePolicyCoverageDetail' ) ) {
					if( CInsurancePolicyCoverageDetailStatusType::FUTURE == $objInsurancePolicyCoverageDetail->getInsurancePolicyDetailStatusTypeId() ) {
						$objFutureInsurancePolicyCoverageDetail = clone $objInsurancePolicyCoverageDetail;
						break;
					}
				}
			}
		}

		return $objFutureInsurancePolicyCoverageDetail;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createResidentInsurancePolicy() {

		$objResidentInsurancePolicy = new CResidentInsurancePolicy();
		$objResidentInsurancePolicy->setEntityId( $this->getEntityId() );
		$objResidentInsurancePolicy->setCid( $this->getCid() );
		$objResidentInsurancePolicy->setInsurancePolicyId( $this->getId() );
		$objResidentInsurancePolicy->setPropertyId( $this->getPropertyId() );
		$objResidentInsurancePolicy->setPropertyUnitId( $this->getPropertyUnitId() );
		$objResidentInsurancePolicy->setUnitSpaceId( $this->getUnitSpaceId() );
		$objResidentInsurancePolicy->setLeadSourceTypeId( $this->getLeadSourceTypeId() );
		$objResidentInsurancePolicy->setInsurancePolicyStatusTypeId( $this->getInsurancePolicyStatusTypeId() );
		$objResidentInsurancePolicy->setInsurancePolicyTypeId( $this->getInsurancePolicyTypeId() );
		$objResidentInsurancePolicy->setPolicyNumber( $this->getCarrierPolicyNumber() );
		if( false == is_null( $this->getInsuranceCarrierId() ) ) {
			$objResidentInsurancePolicy->setIsIntegrated( 1 );
		}

		$objResidentInsurancePolicy->setInsuranceCarrierName( $this->getCarrierName() );
		$objResidentInsurancePolicy->setDeductible( $this->getCustomDeductible() );
		$objResidentInsurancePolicy->setLiabilityLimit( $this->getCustomLiabilityLimit() );
		$objResidentInsurancePolicy->setPersonalLimit( $this->getCustomPersonalLimit() );
		$objResidentInsurancePolicy->setUnitNumber( $this->getUnitNumber() );
		$objResidentInsurancePolicy->setEffectiveDate( $this->getEffectiveDate() );
		$objResidentInsurancePolicy->setEndDate( $this->getEndDate() );
		$objResidentInsurancePolicy->setLapsedOn( $this->getLapsedOn() );
		$objResidentInsurancePolicy->setCancelledOn( $this->getCancelledOn() );
		$objResidentInsurancePolicy->setConfirmedOn( $this->getConfirmedOn() );

		return $objResidentInsurancePolicy;
	}

	public function createInsurancePolicyCustomer( $intResidentInsurancePolicyId ) {

		$objInsurancePolicyCustomer = new CInsurancePolicyCustomer();
		$objInsurancePolicyCustomer->setResidentInsurancePolicyId( $intResidentInsurancePolicyId );
		$objInsurancePolicyCustomer->setCid( $this->getCid() );
		$objInsurancePolicyCustomer->setPropertyId( $this->getPropertyId() );
		$objInsurancePolicyCustomer->setLeaseId( $this->getLeaseId() );
		$objInsurancePolicyCustomer->setCustomerId( $this->getCustomerId() );
		$objInsurancePolicyCustomer->setApplicantId( $this->getApplicantId() );
		$objInsurancePolicyCustomer->setApplicationId( $this->getApplicationId() );

		return $objInsurancePolicyCustomer;
	}

	/**
	 * Get or Fetch Functions
	 *
	 */

	public function getOrFetchInsuranceCarrier( $objDatabase ) {

		if( false == valObj( $this->m_objInsuranceCarrier,  'CInsuranceCarrier' ) ) {
			$this->m_objInsuranceCarrier = \Psi\Eos\Insurance\CInsuranceCarriers::createService()->fetchInsuranceCarrierById( $this->getInsuranceCarrierId(), $objDatabase );
		}

		return $this->m_objInsuranceCarrier;
	}

	public function getOrFetchAccountWithBalance( $objDatabase ) {

		if( false == valObj( $this->m_objAccount, 'CAccount' ) ) {
			$this->m_objAccount = CAccounts::createService()->fetchAccountsWithBalanceById( $this->getAccountId(), $objDatabase );
		} elseif( NULL === $this->m_objAccount->getAmountDue() ) {
			$objAccount = CAccounts::createService()->fetchAccountsWithBalanceById( $this->getAccountId(), $objDatabase );
			if( valObj( $objAccount, 'CAccount' ) ) {
				$this->m_objAccount->setAmountDue( $objAccount->getAmountDue() );
			}
		}

		return $this->m_objAccount;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getInsuranceChargeId() {
		return $this->m_intInsuranceChargeId;
	}

	public function getInsurancePolicyCoverageDetails() {
		return $this->m_arrobjInsurancePolicyCoverageDetails;
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getAccount() {
		return $this->m_objAccount;
	}

	public function getInsuranceCarrier() {
		return $this->m_objInsuranceCarrier;
	}

	public function getLastInsuranceCharge() {
		return $this->m_objLastInsuranceCharge;
	}

	public function getUnAccountedLiabilityCharge() {
		return $this->m_objUnAccountedLiabilityCharge;
	}

	public function getInsurancePolicyLiabilityCharge() {
		return $this->m_objInsurancePolicyLiabilityCharge;
	}

	public function getInsurancePolicyLease() {
		return $this->m_objInsurancePolicyLease;
	}

	public function getResidentInsurancePolicy() {
		return $this->m_objResidentInsurancePolicy;
	}

	public function getUnprocessedReturnedCharges() {
		return $this->m_arrobjUnprocessedReturnedCharges;
	}

	public function getIsSeekingProperty() {
		return $this->m_boolIsSeekingProperty;
	}

	public function getAgreesToTerms() {
		return $this->m_boolAgreesToTerms;
	}

	public function getAgreesToIDTheftTerms() {
		return $this->m_boolAgreesToIDTheftTerms;
	}

	public function getAgreesToPayConvenienceFees() {
		return $this->m_boolAgreesToPayConvenienceFees;
	}

	public function getAgreesToNoLosses() {
		return $this->m_boolAgreesToNoLosses;
	}

	public function getInsurancePolicyEndorsements() {
		return $this->m_arrintInsurancePolicyEndorsements;
	}

	public function getIsSameMailingAddress() {
		return $this->m_boolIsSameMailingAddress;
	}

	public function getIsBillingAddressSame() {
		return $this->m_boolIsBillingAddressSame;
	}

	public function getInsuranceCarrierId() {
		return $this->m_intInsuranceCarrierId;
	}

	public function getRentalPropertyUnitNumber() {
		return $this->m_intRentalPropertyUnitNumber;
	}

	public function getInsurancePolicyInsureds() {
		return $this->m_arrobjInsurancePolicyInsureds;
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function getPaymentPeriodStartDate() {
		return $this->m_strPaymentPeriodStartDate;
	}

	public function getPaymentPeriodEndDate() {
		return $this->m_strPaymentPeriodEndDate;
	}

	public function getCarrierPeriodStartDate() {
		return $this->m_strCarrierPeriodStartDate;
	}

	public function getCarrierPeriodEndDate() {
		return $this->m_strCarrierPeriodEndDate;
	}

	public function getNextCarrierPeriodStartDate() {
		return $this->m_strNextCarrierPeriodStartDate;
	}

	public function getPrimaryInsuredNameFirst() {
		return $this->m_strPrimaryInsuredNameFirst;
	}

	public function getPrimaryInsuredNameLast() {
		return $this->m_strPrimaryInsuredNameLast;
	}

	public function getPrimaryInsuredEmailAddress() {
		return $this->m_strPrimaryInsuredEmailAddress;
	}

	public function getPrimaryInsuredPhoneNumber() {
		return $this->m_strPrimaryInsuredPhoneNumber;
	}

	public function getConfirmCancel() {
		return $this->m_strConfirmCancel;
	}

	public function getCarrierName() {
		return $this->m_strCarrierName;
	}

	public function getIsCarrierIntegrated() {
		return $this->m_boolPolicyCarrierIsIntegrated;
	}

	public function getIsFuture() {
		return $this->m_boolIsFuture;
	}

	public function getIsCarrierNotified() {
		return $this->m_boolIsCarrierNotified;
	}

	public function getIsInsuredChanged() {
		return $this->m_boolIsInsuredChanged;
	}

	public function getIsIncludeSpouse() {
		return $this->m_boolIsIncludeSpouse;
	}

	public function getIsMinRateCall() {
		return $this->m_boolIsMinRateCall;
	}

	public function getPropertyPhoneNumber() {
		return $this->m_strPropertyPhoneNumber;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getCoApplicantCount() {
		return $this->m_intCoApplicantCount;
	}

	public function getReplacedInsurancePolicyId() {
		return $this->m_intReplacedInsurancePolicyId;
	}

	public function getTotalPaidAmount() {
		return $this->m_fltTotalPaidAmount;
	}

	public function getPaymentCollectionAmount() {
		return $this->m_fltPaymentCollectionAmount;
	}

	public function getConvenienceFee() {
		return $this->m_fltConvenienceFee;
	}

	public function getTotalInsureeCount() {
		return $this->m_intTotalInsureeCount;
	}

	public function getNextChargeDate() {
		return $this->m_strNextChargeDate;
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function getSourceCid() {
		return $this->m_intSourceCid;
	}

	public function getOriginalCid() {
		return $this->m_intOrignalCid;
	}

	public function getOriginalPropertyId() {
		return $this->m_intOriginalPropertyId;
	}

	public function getMonthlyLiabilityAmount() {
		return $this->m_fltMonthlyLiabilityAmount;
	}

	public function getMonthlyEarnedPremiumAmount() {
		return $this->m_fltMonthlyEarnedPremiumAmount;
	}

	public function getMonthlyAdjustmentAmount() {
		return $this->m_fltMonthlyAdjustmentAmount;
	}

	public function getProviderLiabilityAmount() {
		return $this->m_fltProviderLiabilityAmount;
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function getPaymentReferenceNumber() {
		return $this->m_strPaymentReferenceNumber;
	}

	public function getPremiumTransactionId() {
		return $this->m_intPremiumTransactionId;
	}

	public function getInsuranceCarrierProperty() {
		return $this->m_objInsuranceCarrierProperty;
	}

	public function getCoverageStartDate() {
		return $this->m_strCoverageStartDate;
	}

	public function getCoverageEndDate() {
		return $this->m_strCoverageEndDate;
	}

	public function getCoveragePaidThroughDate() {
		return $this->m_strCoveragePaidThroughDate;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrstrValues['is_seeking_property'] ) )				$this->setIsSeekingProperty( $arrstrValues['is_seeking_property'] );
		if( true == isset( $arrstrValues['agrees_to_fees'] ) )					$this->setAgreesToPayConvenienceFees( $arrstrValues['agrees_to_fees'] );
		if( true == isset( $arrstrValues['agrees_to_no_losses'] ) )				$this->setAgreesToNoLosses( $arrstrValues['agrees_to_no_losses'] );
		if( true == isset( $arrstrValues['agrees_to_terms'] ) )					$this->setAgreesToTerms( $arrstrValues['agrees_to_terms'] );
		if( true == isset( $arrstrValues['agrees_to_id_theft_terms'] ) )		$this->setAgreesToIDTheftTerms( $arrstrValues['agrees_to_id_theft_terms'] );
		if( true == isset( $arrstrValues['is_same_mailing_address'] ) )			$this->setIsSameMailingAddress( $arrstrValues['is_same_mailing_address'] );
		if( true == isset( $arrstrValues['is_same_rental_billing_address'] ) )	$this->setIsSameMailingAddress( $arrstrValues['is_same_rental_billing_address'] );
		if( true == isset( $arrstrValues['confirm_cancel'] ) )					$this->setConfirmCancel( $arrstrValues['confirm_cancel'] );
		if( true == isset( $arrstrValues['insurance_carrier_is_integrated'] ) )	$this->setIsCarrierIntegrated( $arrstrValues['insurance_carrier_is_integrated'] );
		if( true == isset( $arrstrValues['insurance_carrier_name'] ) ) 			$this->setCarrierName( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['insurance_carrier_name'] ) : $arrstrValues['insurance_carrier_name'] );
		if( true == isset( $arrstrValues['primary_insured_name_first'] ) ) 		$this->setPrimaryInsuredNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['primary_insured_name_first'] ) : $arrstrValues['primary_insured_name_first'] );
		if( true == isset( $arrstrValues['primary_insured_name_last'] ) ) 		$this->setPrimaryInsuredNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['primary_insured_name_last'] ) : $arrstrValues['primary_insured_name_last'] );
		if( true == isset( $arrstrValues['primary_insured_email_address'] ) ) 	$this->setPrimaryInsuredEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['primary_insured_email_address'] ) : $arrstrValues['primary_insured_email_address'] );
		if( true == isset( $arrstrValues['primary_insured_phone_number'] ) ) 	$this->setPrimaryInsuredPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['primary_insured_phone_number'] ) : $arrstrValues['primary_insured_phone_number'] );
		if( true == isset( $arrstrValues['property_name'] ) ) 					$this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['property_name'] ) : $arrstrValues['property_name'] );
		if( true == isset( $arrstrValues['is_future'] ) )						$this->setIsFuture( $arrstrValues['is_future'] );
		if( true == isset( $arrstrValues['count'] ) )						    $this->setCoApplicantCount( $arrstrValues['count'] );
		if( true == isset( $arrstrValues['amount_received'] ) )					$this->setTotalPaidAmount( $arrstrValues['amount_received'] );
		if( true == isset( $arrstrValues['insurance_charge_id'] ) )				$this->setInsuranceChargeId( $arrstrValues['insurance_charge_id'] );
		if( true == isset( $arrstrValues['event_id'] ) )						$this->setEventId( $arrstrValues['event_id'] );
		if( true == isset( $arrstrValues['source_cid'] ) )						$this->setSourceCid( $arrstrValues['source_cid'] );
		if( true == isset( $arrstrValues['original_cid'] ) )					$this->setOriginalCid( $arrstrValues['original_cid'] );
		if( true == isset( $arrstrValues['original_property_id'] ) )			$this->setOriginalPropertyId( $arrstrValues['original_property_id'] );
		if( true == isset( $arrstrValues['additional_insuree'] ) )				$this->setTotalInsureeCount( $arrstrValues['additional_insuree'] );
		if( true == isset( $arrstrValues['payment_reference_number'] ) )		$this->setPaymentReferenceNumber( $arrstrValues['payment_reference_number'] );
		if( true == isset( $arrstrValues['coverage_start_date'] ) )             $this->setCoverageStartDate( $arrstrValues['coverage_start_date'] );
		if( true == isset( $arrstrValues['coverage_end_date'] ) )               $this->setCoverageEndDate( $arrstrValues['coverage_end_date'] );
	}

	public function setInsuranceChargeId( $intInsuranceChargeId ) {
		$this->m_intInsuranceChargeId = $intInsuranceChargeId;
	}

	public function setInsurancePolicyCoverageDetails( $arrobjInsurancePolicyCoverageDetails ) {
		$this->m_arrobjInsurancePolicyCoverageDetails = $arrobjInsurancePolicyCoverageDetails;
	}

	public function setApplicantId( $intApplicantId ) {
		$this->m_intApplicantId = $intApplicantId;
	}

	public function setApplicationId( $intApplicationId ) {
		$this->m_intApplicationId = $intApplicationId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setIsSeekingProperty( $boolIsSeekingProperty ) {
		$this->m_boolIsSeekingProperty = $boolIsSeekingProperty;
	}

	public function setCurrentInsurancePolicyDetail( $objInsurancePolicyDetail ) {
		$this->m_objCurrentInsurancePolicyDetail = $objInsurancePolicyDetail;
	}

	public function setAccount( $objAccount ) {
		$this->m_objAccount = $objAccount;
	}

	public function setLastInsuranceCharge( $objLastInsuranceCharge ) {
		$this->m_objLastInsuranceCharge = $objLastInsuranceCharge;
	}

	public function setInsurancePolicyLease( $objInsurancePolicyLease ) {
		$this->m_objInsurancePolicyLease = $objInsurancePolicyLease;
	}

	public function setResidentInsurancePolicy( $objResidentInsurancePolicy ) {
		$this->m_objResidentInsurancePolicy = $objResidentInsurancePolicy;
	}

	public function setUnprocessedReturnedCharges( $arrobjUnprocessedReturnedCharges ) {
		$this->m_arrobjUnprocessedReturnedCharges = $arrobjUnprocessedReturnedCharges;
	}

	public function setAgreesToTerms( $boolAgreesToTerms ) {
		$this->m_boolAgreesToTerms = $boolAgreesToTerms;
	}

	public function setAgreesToIDTheftTerms( $boolAgreesToIDTheftTerms ) {
		$this->m_boolAgreesToIDTheftTerms = $boolAgreesToIDTheftTerms;
	}

	public function setAgreesToPayConvenienceFees( $boolAgreeToFees ) {
		$this->m_boolAgreesToPayConvenienceFees = $boolAgreeToFees;
	}

	public function setAgreesToNoLosses( $boolAgreesToNoLosses ) {
		$this->m_boolAgreesToNoLosses = $boolAgreesToNoLosses;
	}

	public function setInsurancePolicyEndorsements( $arrintInsurancePolicyEndorsements ) {
		$this->m_arrintInsurancePolicyEndorsements = $arrintInsurancePolicyEndorsements;
	}

	public function setInsurancePolicyInsureds( $arrobjInsurancePolicyInsureds ) {
		$this->m_arrobjInsurancePolicyInsureds = $arrobjInsurancePolicyInsureds;
	}

	public function setIsSameMailingAddress( $boolIsSameMailingAddress ) {
		$this->m_boolIsSameMailingAddress = $boolIsSameMailingAddress;
	}

	public function setIsBillingAddressSame( $boolIsBillingAddessSame ) {
		$this->m_boolIsBillingAddressSame = $boolIsBillingAddessSame;
	}

	public function setInsuranceCarrierId( $intInsuranceCarrierId ) {
		$this->m_intInsuranceCarrierId = $intInsuranceCarrierId;
	}

	public function setRentalPropertyUnitNumber( $intPropertyUnitNumber ) {
		$this->m_intRentalPropertyUnitNumber = $intPropertyUnitNumber;
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->m_fltPaymentAmount = $fltPaymentAmount;
	}

	public function setPaymentPeriodStartDate( $strPaymentPeriodStartDate ) {
		$this->m_strPaymentPeriodStartDate = $strPaymentPeriodStartDate;
	}

	public function setPaymentPeriodEndDate( $strPaymentPeriodEndDate ) {
		$this->m_strPaymentPeriodEndDate = $strPaymentPeriodEndDate;
	}

	public function setCarrierPeriodStartDate( $strCarrierPeriodStartDate ) {
		$this->m_strCarrierPeriodStartDate = $strCarrierPeriodStartDate;
	}

	public function setCarrierPeriodEndDate( $strCarrierPeriodEndDate ) {
		$this->m_strCarrierPeriodEndDate = $strCarrierPeriodEndDate;
	}

	public function setNextCarrierPeriodStartDate( $strNextCarrierPeriodStartDate ) {
		$this->m_strNextCarrierPeriodStartDate = $strNextCarrierPeriodStartDate;
	}

	public function setConfirmCancel( $strConfirmCancel ) {
		$this->m_strConfirmCancel = $strConfirmCancel;
	}

	public function setPrimaryInsuredNameFirst( $strName ) {
		$this->m_strPrimaryInsuredNameFirst = $strName;
	}

	public function setPrimaryInsuredNameLast( $strName ) {
		$this->m_strPrimaryInsuredNameLast = $strName;
	}

	public function setPrimaryInsuredEmailAddress( $strEmailAddress ) {
		$this->m_strPrimaryInsuredEmailAddress = $strEmailAddress;
	}

	public function setPrimaryInsuredPhoneNumber( $strPhoneNUmber ) {
		$this->m_strPrimaryInsuredPhoneNumber = $strPhoneNUmber;
	}

	public function setCarrierName( $strName ) {
		$this->m_strCarrierName = $strName;
	}

	public function setIsCarrierIntegrated( $boolIsCarrierIntegrated ) {
		$this->m_boolPolicyCarrierIsIntegrated = $boolIsCarrierIntegrated;
	}

	public function setIsFuture( $boolIsFuture ) {
		$this->m_boolIsFuture = $boolIsFuture;
	}

	public function setIsCarrierNotified( $boolIsCarrierNotified ) {
		$this->m_boolIsCarrierNotified = $boolIsCarrierNotified;
	}

	public function setIsInsuredChanged( $boolIsInsuredChanged ) {
		$this->m_boolIsInsuredChanged = $boolIsInsuredChanged;
	}

	public function setIsIncludeSpouse( $boolIsIncludeSpouse ) {
		$this->m_boolIsIncludeSpouse = $boolIsIncludeSpouse;
	}

	public function setIsMinRateCall( $boolIsMinRateCall ) {
		$this->m_boolIsMinRateCall = $boolIsMinRateCall;
	}

	public function setPropertyPhoneNumber( $strPropertyPhoneNumber ) {
		$this->m_strPropertyPhoneNumber = $strPropertyPhoneNumber;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setCoApplicantCount( $intCoApplicantCount ) {
		$this->m_intCoApplicantCount = $intCoApplicantCount;
	}

	public function setReplacedInsurancePolicyId( $intReplacedInsurancePolicyId ) {
		$this->m_intReplacedInsurancePolicyId = $intReplacedInsurancePolicyId;
	}

	public function setTotalPaidAmount( $fltTotalPaidAmount ) {
		$this->m_fltTotalPaidAmount = $fltTotalPaidAmount;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = $intLeaseId;
	}

	public function setPaymentCollectionAmount( $fltPaymentCollectionAmount ) {
		$this->m_fltPaymentCollectionAmount = $fltPaymentCollectionAmount;
	}

	public function setConvenienceFee( $fltConvenienceFee ) {
		$this->m_fltConvenienceFee = $fltConvenienceFee;
	}

	public function setTotalInsureeCount( $intTotalInsureeCount ) {
		$this->m_intTotalInsureeCount = $intTotalInsureeCount;
	}

	public function setNextChargeDate( $strNextChargeDate ) {
		$this->m_strNextChargeDate = $strNextChargeDate;
	}

	public function setEventId( $intEventId ) {
		$this->m_intEventId = $intEventId;
	}

	public function setSourceCid( $intSourceCid ) {
		$this->m_intSourceCid = $intSourceCid;
	}

	public function setOriginalCid( $intOrignalCid ) {
		$this->m_intOrignalCid = $intOrignalCid;
	}

	public function setOriginalPropertyId( $intOriginalPropertyId ) {
		$this->m_intOriginalPropertyId = $intOriginalPropertyId;
	}

	public function setUnAccountedLiabilityCharge( $objUnAccountedLiabilityCharge ) {
		$this->m_objUnAccountedLiabilityCharge = $objUnAccountedLiabilityCharge;
	}

	public function setInsurancePolicyLiabilityCharge( $objInsurancePolicyLiabilityCharge ) {
		$this->m_objInsurancePolicyLiabilityCharge = $objInsurancePolicyLiabilityCharge;
	}

	public function setMonthlyLiabilityAmount( $fltMonthlyLiabilityAmount ) {
		$this->m_fltMonthlyLiabilityAmount = $fltMonthlyLiabilityAmount;
	}

	public function setMonthlyEarnedPremiumAmount( $fltMonthlyEarnedPremiumAmount ) {
		$this->m_fltMonthlyEarnedPremiumAmount = $fltMonthlyEarnedPremiumAmount;
	}

	public function setMonthlyAdjustmentAmount( $fltMonthlyAdjustmentAmount ) {
		$this->m_fltMonthlyAdjustmentAmount = $fltMonthlyAdjustmentAmount;
	}

	public function setProviderLiabilityAmount( $fltProviderLiabilityAmount ) {
		$this->m_fltProviderLiabilityAmount = $fltProviderLiabilityAmount;
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->m_fltChargeAmount = $fltChargeAmount;
	}

	public function setPaymentReferenceNumber( $strPaymentReferenceNumber ) {
		$this->m_strPaymentReferenceNumber = $strPaymentReferenceNumber;
	}

	public function setPremiumTransactionId( $intPremiumTransactionId ) {
		$this->m_intPremiumTransactionId = $intPremiumTransactionId;
	}

	public function setInsuranceCarrierProperty( $objInsuranceCarrierProperty ) {
		$this->m_objInsuranceCarrierProperty = $objInsuranceCarrierProperty;
	}

	public function setCoverageStartDate( $strCoverageStartDate ) {
		$this->m_strCoverageStartDate = $strCoverageStartDate;
	}

	public function setCoverageEndDate( $strCoverageEndDate ) {
		$this->m_strCoverageEndDate = $strCoverageEndDate;
	}

	public function setPropertyId( $intPropertyId ) {
		parent::setPropertyId( $intPropertyId );

		if( true == is_null( $this->getOriginalPropertyId() ) ) {
			$this->setOriginalPropertyId( $intPropertyId );
		}
	}

	public function setCoveragePaidThroughDate( $strCoveragePaidDate ) {
		$this->m_strCoveragePaidThroughDate = $strCoveragePaidDate;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchAccount( $objAdminDatabase ) {

		// $objAccount = CAccounts::fetchAccountByIdAndCid( $this->getAccountId(), $this->getCid(), $objAdminDatabase );
		$objAccount = CAccounts::createService()->fetchAccountById( $this->getAccountId(), $objAdminDatabase );

		if( true == valObj( $objAccount, 'CAccount' ) ) {
			return $objAccount;
		}

		return false;
	}

}
?>