<?php

class CInsurancePaymentReference extends CBaseInsurancePaymentReference {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPaymentReferenceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsProcessed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>