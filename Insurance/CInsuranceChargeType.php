<?php

class CInsuranceChargeType extends CBaseInsuranceChargeType {

	const EXPORT               = 1;
	const ADJUST_ACCOUNT       = 2;
	const ECHECK_RETURN        = 3;
	const CHARGE_BACKS         = 4;
	const USER_CANCELLATION    = 5;
	const NON_PAY_CANCELLATION = 6;

	public static $c_arrintCancelledInsuranceChargeTypes = [ CInsuranceChargeType::ADJUST_ACCOUNT, CInsuranceChargeType::ECHECK_RETURN, CInsuranceChargeType::CHARGE_BACKS, CInsuranceChargeType::USER_CANCELLATION, CInsuranceChargeType::NON_PAY_CANCELLATION ];

	public function valId() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valName() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valDescription() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function valIsPublished() {
    	$boolIsValid = true;
    	return $boolIsValid;
    }

    public function validate( $strAction ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    		case VALIDATE_DELETE:
    			break;

    		default:
    			$boolIsValid = false;
    	}

    	return $boolIsValid;
    }

	public static function getChargeTypeByPaymentType( $intPaymentType, $boolIsReversePayment = false ) : int {

		if( false === $boolIsReversePayment ) {
			return self::EXPORT;
		}

		switch( $intPaymentType ) {
			case CPaymentType::ACH:
				$intChargeType = self::ECHECK_RETURN;
				break;

			case CPaymentType::VISA:
			case CPaymentType::MASTERCARD:
			case CPaymentType::DISCOVER:
			case CPaymentType::AMEX:
				$intChargeType = self::CHARGE_BACKS;
				break;

			default:
				$intChargeType = self::ADJUST_ACCOUNT;
		}

		return $intChargeType;
	}

	public static function getPolicyAdjustmentChargeType( $strCancelledOnDate = NULL ) : int {

		$intInsuranceChargeTypeId = CInsuranceChargeType::ADJUST_ACCOUNT;
		if( false == is_null( $strCancelledOnDate ) ) {
			$intInsuranceChargeTypeId = CInsuranceChargeType::USER_CANCELLATION;
		}

		return $intInsuranceChargeTypeId;
	}

}
?>