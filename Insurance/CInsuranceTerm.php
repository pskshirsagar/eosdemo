<?php

class CInsuranceTerm extends CBaseInsuranceTerm {

	const TERMS_ID        = 5;
	const POL_2A_TERMS_ID = 6;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>