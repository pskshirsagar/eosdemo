<?php

class CInsuranceCustomPolicyClientDetail extends CBaseInsuranceCustomPolicyClientDetail {

	const VALIDATE_CLIENT_NUMBER        = 'VALIDATE_CLIENT_NUMBER';
	const VALIDATE_MASTER_POLICY_NUMBER = 'VALIDATE_MASTER_POLICY_NUMBER';
	const VALIDATE_CLIENT_DETAILS       = 'VALIDATE_CLIENT_DETAILS';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientNumber() {
		$boolIsValid = true;

    	if( true == is_null( $this->getClientNumber() ) || 4 > \Psi\CStringService::singleton()->strlen( ( string ) $this->getClientNumber() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter 4 digits client number. ' ) );
    	}

    	return $boolIsValid;
	}

	public function valMasterPolicyNumber() {
		$boolIsValid = true;

    	if( true == is_null( $this->getMasterPolicyNumber() ) || 15 < \Psi\CStringService::singleton()->strlen( ( string ) $this->getMasterPolicyNumber() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter max 15 characters master policy number. ' ) );
    	}

    	return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getDomicileStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select domicile state code. ' ) );
		}

		return $boolIsValid;
	}

	public function valMasterPolicyAccount( $arrobjProperties, $objAdminDatabase ) {

		$boolIsValid = true;

		if( true == valArr( $arrobjProperties ) ) {

			$arrintCids         = array_keys( rekeyObjects( 'Cid', $arrobjProperties ) );
			$arrintPropertyIds  = array_keys( $arrobjProperties );
			$arrintProductIds   = [ CPsProduct::RESIDENT_INSURE, CPsProduct::MASTER_POLICY ];

			$arrobjAccount = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountByProductIdsByAccountTypeIdByPropertyIdsByCid( $arrintProductIds, CAccountType::MASTER_POLICY, $arrintPropertyIds, $arrintCids[0], $objAdminDatabase );
			$arrobjAccount = rekeyObjects( 'PropertyId', $arrobjAccount );

			$arrstrPropertyNames = [];

			foreach( $arrobjProperties as $objProperty ) {
				$objAccount = $arrobjAccount[$objProperty->getId()];

				if( false == valObj( $objAccount, 'CAccount' ) ) {
					$arrstrPropertyNames[] = $objProperty->getPropertyName();
				}
			}

			if( true == valArr( $arrstrPropertyNames ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Selected properties ' . implode( ',', $arrstrPropertyNames ) . ' doesn\'t have master policy account.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDomicileStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRate() {
		$boolIsValid = true;

		if( true == is_null( $this->getBaseRate() ) || false == is_numeric( $this->getBaseRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter valid policy rate. ' ) );
		}
		return $boolIsValid;
	}

	public function valPolicyFee() {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyFee() ) || false == ( preg_match( '/^[0-9]*$/', $this->getPolicyFee() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter valid policy fee. ' ) );
		}
		return $boolIsValid;
	}

	public function valBasicTaxPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStampingTaxPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOtherTaxPercent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractExpirationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIssuedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $arrobjProperties = NULL, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case self::VALIDATE_CLIENT_NUMBER:
				$boolIsValid &= $this->valClientNumber();
				break;

			case self::VALIDATE_MASTER_POLICY_NUMBER:
				$boolIsValid &= $this->valMasterPolicyNumber();
				break;

			case self::VALIDATE_CLIENT_DETAILS:
				$boolIsValid &= $this->valBaseRate();
				$boolIsValid &= $this->valPolicyFee();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valMasterPolicyAccount( $arrobjProperties, $objAdminDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>