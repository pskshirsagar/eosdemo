<?php

class CInsuranceEndorsementType extends CBaseInsuranceEndorsementType {

	const REPLACEMENT_COST                       = 1;
	const THEFT_COVERAGE                         = 2;
	const WATER_DAMAGE                           = 3;
	const IDENTITY_FRAUD_EXPENCES                = 4;
	const INCREASED_MOLD                         = 5;
	const FLOOD                                  = 6;
	const EARTHQUAKE                             = 7;
	const MN_FIREMAN_RELIEF_SURCHARGE            = 8;
	const KY_MUNICIPAL_TAX                       = 9;
	const PET_DAMAGE                             = 11;
	const IDTHEFT_PROTECTION_INDIVIDUAL          = 12;
	const IDTHEFT_PROTECTION_FAMILY              = 13;
	const EQUIPMENT_BREAKDOWN                    = 15;
	const BED_BUG                                = 16;
	const SILVER_ENHANCEMENT_ENDORSEMENT         = 17;
	const ACQUISITION                            = 18;
	const LIABILITY_COVERAGE                     = 19;
	const PREFERRED_PROPERTY                     = 20;
	const WIND_HAIL_COVERAGE_EXLUSION            = 21;
	const NON_HURRICANE_FACTOR_WITHOUT_WIND_HAIL = 22;
	const NON_HURRICANE_FACTOR_WITH_WIND_HAIL    = 23;
	const HURRICANE_FACTOR                       = 24;
	const NON_HURRICANE_PREMIUM_SUBJECT_TO_WIND  = 25;
	const GOLD_ENHANCEMENT_ENDORSEMENT           = 26;
	const PLATINUM_ENHANCEMENT_ENDORSEMENT       = 27;

	const IDTHEFT_INDIVIDUAL = 8594;
	const IDTHEFT_FAMILY     = 8794;

	public static $c_arrintLocalEndorsementTypeIds = [ self::MN_FIREMAN_RELIEF_SURCHARGE, self::KY_MUNICIPAL_TAX ];
	public static $c_arrintEnhancementEndorsementTypeIds = [ self::SILVER_ENHANCEMENT_ENDORSEMENT, self::GOLD_ENHANCEMENT_ENDORSEMENT, self::PLATINUM_ENHANCEMENT_ENDORSEMENT ];
	public static $c_arrintAdditionalEnhancementEndorsementTypeIds = [ self::GOLD_ENHANCEMENT_ENDORSEMENT, self::PLATINUM_ENHANCEMENT_ENDORSEMENT ];
}

?>