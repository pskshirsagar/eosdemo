<?php

class CInsuranceCarrier extends CBaseInsuranceCarrier {

	protected $m_arrobjInsuranceCarrierStates;

	const MARKEL        = 1;
	const KEMPER        = 71;
	const KEMPER_DIRECT = 72;
	const QBE           = 75;
	const SSIS          = 76;
	const IDTHEFT       = 77;

	const KEMPER_FLORIDA         = 'kemper florida';
	const CUSTOM_RESIDENT_INSURE = 'resident insure';

	public static $c_strInsuranceCarrierSSIS = 'SSIS';
	public static $c_arrstrCorrectedCarrierNames = [ 'alstate' => 'Allstate', 'allstate' => 'Allstate', 'allsate' => 'Allstate', 'allsatet' => 'Allstate', 'allstae' => 'Allstate', 'allstat' => 'Allstate', 'allstateinsurance' => 'Allstate', 'allstateinsurancecompany' => 'Allstate', 'allatate' => 'Allstate', 'allsateinsurance' => 'Allstate', 'allstaeinsurance' => 'Allstate', 'allstateins' => 'Allstate', 'allstateinscompany' => 'Allstate', 'allstateinsuranceco' => 'Allstate', 'allstateinsuranceco.' => 'Allstate', 'allstatepropertyandcasuatlyinsurancecompany' => 'Allstate', 'allstater' => 'Allstate', 'allsttate' => 'Allstate', 'alltate' => 'Allstate', 'allstatepropertyandcasuatlyinsuranceco.' => 'Allstate', 'allstatepropertyandcasuatlyinscompany' => 'Allstate', 'allstatepropertyandcasuatlycompany' => 'Allstate', 'allstateproperty&casuatlyinsurancecompany' => 'Allstate', 'allstateproperty' => 'Allstate', 'allstateproperty&casuatlyinsurance' => 'Allstate', 'allstateinsurancegroup' => 'Allstate', 'allstateinsurancecomplany' => 'Allstate', 'erenterplan' => 'eRenterplan', 'erenters' => 'eRenterplan', 'erentersplan' => 'eRenterplan', 'e-renterplan' => 'eRenterplan', 'e-renters' => 'eRenterplan', 'erener' => 'eRenterplan', 'erenter' => 'eRenterplan', 'erenersins' => 'eRenterplan', 'erenyerins' => 'eRenterplan', 'nationwide' => 'Nationwide', 'nationawide' => 'Nationwide', 'nationwideinsurance' => 'Nationwide', 'nationwidegeneralinsurance' => 'Nationwide', 'nationwideinsco.' => 'Nationwide', 'nationwideins.coofamerica' => 'Nationwide', 'nationawideinsuranceco.' => 'Nationwide', 'nationawidemutualinsurance' => 'Nationwide', 'nationawidemutualinsuranceagency' => 'Nationwide', 'nationawidemutualinsurancecompany' => 'Nationwide', 'nationawideonyourside' => 'Nationwide', 'nationawideproperty&casualtycompany' => 'Nationwide', 'nationawideproperty&casualtyinsurancecompany' => 'Nationwide', 'nationawidepropertyandcasualty' => 'Nationwide', 'nationawidepropertyandcasualtyinsurance' => 'Nationwide', 'natiowide' => 'Nationwide', 'statefarm' => 'State Farm', 'statefarmfire' => 'State Farm', 'statefarmfireandcasualty' => 'State Farm', 'statefarmfireandcasualtyco.' => 'State Farm', 'statefarmfireandcasualtycompany' => 'State Farm', 'statefarmfireandcasulatyco.' => 'State Farm', 'statefarminsurace' => 'State Farm', 'statefarminsurance' => 'State Farm', 'statefarminsuranceco.' => 'State Farm', 'statefarminsurancecompany' => 'State Farm', 'statefarm/general' => 'State Farm', 'stateorovince:il.' => 'State Farm', 'staefarm' => 'State Farm', 'statefarmandcasualtycompany' => 'State Farm', 'statefarms' => 'State Farm', 'statesinsurance' => 'State Farm', 'usaa' => 'USAA', 'usaaCasualty' => 'USAA', 'usaacasualtyinsruancecompany' => 'USAA', 'usaacasualtyinsurance' => 'USAA', 'usaacasualtyinsuranceco' => 'USAA', 'usaacasualtyinsuranceco.' => 'USAA', 'usaacasualtyinsurancecompany' => 'USAA', 'usaacasulty' => 'USAA', 'usaageneral' => 'USAA', 'usaageneralindemnityco' => 'USAA', 'usaageneralindemnityco.' => 'USAA', 'progessive' => 'Progressive', 'progessivehome' => 'Progressive', 'progressive' => 'Progressive', 'progressivehome' => 'Progressive', 'progressivehomeadvantage' => 'Progressive', 'progressivehomeadvantage' => 'Progressive', 'progressiveinsurance' => 'Progressive', 'progressivehomeadvantage' => 'Progressive', 'progressive/homesiteinsurance' => 'Progressive', 'geicoinsagency' => 'GEICO', 'geicoinsuranceagencyinc.' => 'GEICO', 'geicoinsurance' => 'GEICO', 'geicoinsuranceagency' => 'GEICO', 'geyco' => 'GEICO', 'geico' => 'GEICO', 'geicoinsuranceagency,inc.' => 'GEICO', 'geicoinsuranceco' => 'GEICO', 'aaa' => 'AAA', 'aaainsurance' => 'AAA', 'aaainsuranceagency' => 'AAA', 'travelers' => 'Travelers', 'travellers' => 'Travelers', 'ravelers' => 'Travelers', 'traveler' => 'Travelers', 'travelershomeandmarinecompany' => 'Travelers', 'travelersinsurance' => 'Travelers', 'travelors' => 'Travelers', 'travlers' => 'Travelers', 'travelersp&ccoofamerica' => 'Travelers', 'travelers/kreisagencyllc' => 'Travelers', 'assurantgroup' => 'Assurant', 'assurantinsurance' => 'Assurant', 'assurantinsurancecenter' => 'Assurant', 'assurantpolicy' => 'Assurant', 'assurantrenters' => 'Assurant', 'assurantrenterssurvey' => 'Assurant', 'assurantspecialproperty' => 'Assurant', 'assurantspeciality' => 'Assurant', 'assurantspecialityproperty' => 'Assurant', 'assurantspecialty' => 'Assurant', 'assurantspecialtyinsurance' => 'Assurant', 'assurantspecialtyprop' => 'Assurant', 'assurantspecialtyproperty' => 'Assurant', 'asurant' => 'Assurant', 'asurent' => 'Assurant', 'asuran' => 'Assurant', 'assurent' => 'Assurant', 'assurant' => 'Assurant', 'bader' => 'Bader', 'badercompany' => 'Bader', 'baderinsurance' => 'Bader', 'baderco' => 'Bader', 'baderco.' => 'Bader', 'badr' => 'Bader', 'bedr' => 'Bader', 'bdar' => 'Bader', 'bder' => 'Bader', 'farmersinsurance' => 'Farmer', 'farmersinsurancegroups' => 'Farmer', 'fermersinsurancegroup' => 'Farmer', 'farmersinsurancegroup' => 'Farmer', 'farmer' => 'Farmer', 'libertymutual' => 'Liberty mutual', 'libertyl' => 'Liberty mutual', 'libertymutal' => 'Liberty mutual', 'libertymutualinsurance' => 'Liberty mutual', 'libertymutualfireinsurance' => 'Liberty mutual', 'libertymutualinsuranceco.' => 'Liberty mutual', 'libertymututal' => 'Liberty mutual', 'libertynutual' => 'Liberty mutual', 'liberymutual' => 'Liberty mutual', 'liberytmutual' => 'Liberty mutual', 'libetymutal' => 'Liberty mutual', 'libetymutual' => 'Liberty mutual' ];

	public static $c_arrintResidentInsuranceCarriers = [ 'RI-Markel', 'RI-Kemper', 'RI-QBE' ];
	public static $c_arrintIntegratedCarriers = [ self::MARKEL => 'Markel', self::QBE => 'QBE' ];
	public static $c_arrintCarrierIdsToShowAccountDue = [ self::KEMPER, self::QBE ];
	public static $c_arrintCarrierIdsForTransmissionLog = [ self::MARKEL => 'Markel', self::KEMPER => 'Kemper', self::QBE => 'QBE', self::SSIS => 'SSIS', self::IDTHEFT => 'IDTHEFT' ];

	public function getOrFetchInsuranceCarrierStateByStateCode( $strStateCode, $objInsurancePortalDatabase ) {

		if( false == valArr( $this->m_arrobjInsuranceCarrierStates ) ) {
			$this->m_arrobjInsuranceCarrierStates = \Psi\Eos\Insurance\CInsuranceCarrierStates::createService()->fetchInsuranceCarrierStatesByInsuranceCarrierId( $this->getId(), $objInsurancePortalDatabase );
		}

		return $this->m_arrobjInsuranceCarrierStates[$strStateCode];
	}

	public function fetchInsuranceCarrierStates( $objInsurancePortalDatabase ) {
		return \Psi\Eos\Insurance\CInsuranceCarrierStates::createService()->fetchInsuranceCarrierStatesByInsuranceCarrierId( $this->getId(), $objInsurancePortalDatabase );
	}

	public function valName( $objDatabase ) {

		$boolIsValid = true;
		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please enter the carrier name.' ) ) );
		}

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Fail to load insurance database.' ) ) );
		}

		$objInsuranceCarrier = \Psi\Eos\Insurance\CInsuranceCarriers::createService()->fetchInsuranceCarrierByNameByCid( $this->getName(), $this->getCid(), $objDatabase );

		if( true == valObj( $objInsuranceCarrier, 'CInsuranceCarrier' ) ) {

			if( $this->getId() == $objInsuranceCarrier->getId() ) {
				return $boolIsValid;
			}

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Carrier name already in use.' ) ) );
		} elseif( mb_strtolower( $this->getName() ) === mb_strtolower( 'RI-Markel' ) || mb_strtolower( $this->getName() ) === mb_strtolower( 'RI-Kemper' ) || mb_strtolower( $this->getName() ) === mb_strtolower( 'RI-QBE' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Carrier name already in use.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}

?>