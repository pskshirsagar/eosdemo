<?php

class CInsurancePolicyCoverageDetail extends CBaseInsurancePolicyCoverageDetail {

	protected $m_objInsuranceCarrier;

	protected $m_arrobjInsurancePolicyInsureds;

	protected $m_intInsuranceCarrierId;

	protected $m_fltEndorsementAmount;
	protected $m_fltUpFrontPaymentAmount;
	protected $m_fltSubsequentPaymentAmount;
	protected $m_fltIDTheftPremiumAmount;
	protected $m_fltEarthquakePremiumAmount;
	protected $m_fltWaterDamagePremiumAmount;

	protected $m_arrintInsurancePolicyEndorsements;

    public function validate( $strAction, $strPropertyAddress = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:

            case 'validate_address_info':
            	$boolIsValid &= $this->valStreetLine1( $strPropertyAddress );
            	$boolIsValid &= $this->valPostalCode();
            	$boolIsValid &= $this->valCity();
            	$boolIsValid &= $this->valStateCode();
            	break;

            case 'validate_mailing_address_info':
            	$boolIsValid &= $this->valMailingStreetLine1();
            	$boolIsValid &= $this->valMailingPostalCode();
            	$boolIsValid &= $this->valMailingCity();
            	$boolIsValid &= $this->valMailingStateCode();
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    public function setDefaults() {
		$this->setInsurancePolicyDetailStatusTypeId( CInsurancePolicyCoverageDetailStatusType::CURRENT );
    }

    public function valStreetLine1( $strPropertyAddress ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getStreetLine1() ) || 0 == strlen( $this->getStreetLine1() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, 'street_line1', 'Street Address is required.', NULL ) );
    	} elseif( false == is_null( $strPropertyAddress ) && strtolower( trim( $this->getStreetLine1() ) . trim( $this->getUnitNumber() ) ) == strtolower( trim( $strPropertyAddress ) ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, 'street_line1', 'Street Address should not be same as property address .', NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valPostalCode() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getPostalCode() ) || 0 == strlen( $this->getPostalCode() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, 'postal_code', 'Zip Code is required.', NULL ) );
    	}
    	if( true == $boolIsValid ) {
    		$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-[a-zA-Z0-9]{4}$)/';
    		if( preg_match( $strZipCodeCheck, $this->getPostalCode() ) !== 1 ) {

    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code',  'Zip Code is not valid.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valCity() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getCity() ) || 0 == strlen( $this->getCity() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, 'city', 'City is required.', NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valStateCode() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getStateCode() ) || 0 == strlen( $this->getStateCode() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, 'state_code', 'State is required.', NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valMailingStreetLine1() {
    	$boolIsValid = true;

    	$strFieldName 	= 'mailing_street_line1';
    	$strMessage 	= 'Mailing street is required.';

    	if( true == is_null( $this->getMailingStreetLine1() ) || 0 == strlen( $this->getMailingStreetLine1() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valMailingCity() {
    	$boolIsValid = true;

    	$strFieldName 	= 'mailing_city';
    	$strMessage 	= 'Mailing city is required.';

    	if( true == is_null( $this->getMailingCity() ) || 0 == strlen( $this->getMailingCity() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valMailingStateCode() {
    	$boolIsValid = true;

    	$strFieldName 	= 'mailing_state_code';
    	$strMessage 	= 'Mailing state code is required.';

    	if( true == is_null( $this->getMailingStateCode() ) || 0 == strlen( $this->getMailingStateCode() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
    	}

    	return $boolIsValid;
    }

    public function valMailingPostalCode() {
    	$boolIsValid = true;

    	$strZipCode = $this->getMailingPostalCode();

    	$strFieldName 	 = 'mailing_postal_code';
    	$strMessage 	 = 'Mailing zip code is required.';
    	$strValidMessage = 'Mailing zip code is not valid.';

    	if( true == is_null( $strZipCode ) || 0 == strlen( $strZipCode ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName, $strMessage, NULL ) );
    	}

    	if( false == is_null( $strZipCode ) && 0 < strlen( $strZipCode ) ) {
    		$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-[a-zA-Z0-9]{4}$)/';
    		if( preg_match( $strZipCodeCheck, $strZipCode ) !== 1 ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $strFieldName,  $strValidMessage ) );
    		}
    	}

    	return $boolIsValid;
    }

    /**
     * Set Functions
     *
     */

    public function setEndorsementAmount( $fltEndorsementAmount ) {
    	$this->m_fltEndorsementAmount = $fltEndorsementAmount;
    }

    public function setIDTheftPremiumAmount( $fltIDTheftPremiumAmount ) {
    	$this->m_fltIDTheftPremiumAmount = $fltIDTheftPremiumAmount;
    }

    public function setUpFrontPaymentAmount( $fltUpFrontPaymentAmount ) {
    	$this->m_fltUpFrontPaymentAmount = $fltUpFrontPaymentAmount;
    }

    public function setSubsequentPaymentAmount( $fltSubsequentPaymentAmount ) {
    	$this->m_fltSubsequentPaymentAmount = $fltSubsequentPaymentAmount;
    }

    public function setInsurancePolicyEndorsements( $arrintInsurancePolicyEndorsements ) {
    	$this->m_arrintInsurancePolicyEndorsements = $arrintInsurancePolicyEndorsements;
    }

    public function setInsurancePolicyInsureds( $arrobjInsurancePolicyInsureds ) {
    	$this->m_arrobjInsurancePolicyInsureds = $arrobjInsurancePolicyInsureds;
    }

    public function setEarthquakePremiumAmount( $fltEarthquakePremiumAmount ) {
    	$this->m_fltEarthquakePremiumAmount = $fltEarthquakePremiumAmount;
    }

	public function setWaterDamagePremiumAmount( $fltWaterDamagePremiumAmount ) {
		$this->m_fltWaterDamagePremiumAmount = $fltWaterDamagePremiumAmount;
	}

    public function getUpFrontPaymentAmount() {
    	return $this->m_fltUpFrontPaymentAmount;
    }

    public function getSubsequentPaymentAmount() {
    	return $this->m_fltSubsequentPaymentAmount;
    }

	public function getInsurancePolicyEndorsements() {
		return $this->m_arrintInsurancePolicyEndorsements;
	}

	public function getInsurancePolicyInsureds() {
		return $this->m_arrobjInsurancePolicyInsureds;
	}

	public function getEndorsementAmount() {
		return $this->m_fltEndorsementAmount;
	}

	public function getIDTheftPremiumAmount() {
		return $this->m_fltIDTheftPremiumAmount;
	}

	public function getEarthquakePremiumAmount() {
		return $this->m_fltEarthquakePremiumAmount;
	}

	public function getWaterDamagePremiumAmount() {
		return $this->m_fltWaterDamagePremiumAmount;
	}

	public function fetchInsuranceCarrierLimitsByIdsByStateCode( $strStateCode, $objDatabase ) {
		$arrintInsuranceCarrierLimitIds = [];

		if( NULL != $this->getPersonalInsuranceCarrierLimitId() ) {
			$arrintInsuranceCarrierLimitIds[] = $this->getPersonalInsuranceCarrierLimitId();
		}

		if( NULL != $this->getDeductibleInsuranceCarrierLimitId() ) {
			$arrintInsuranceCarrierLimitIds[] = $this->getDeductibleInsuranceCarrierLimitId();
		}

		if( NULL != $this->getLiabilityInsuranceCarrierLimitId() ) {
			$arrintInsuranceCarrierLimitIds[] = $this->getLiabilityInsuranceCarrierLimitId();
		}

		if( NULL != $this->getHurricaneDeductibleInsuranceCarrierLimitId() ) {
			$arrintInsuranceCarrierLimitIds[] = $this->getHurricaneDeductibleInsuranceCarrierLimitId();
		}

		return \Psi\Eos\Insurance\CInsuranceCarrierLimits::createService()->fetchInsuranceCarrierLimitsByIdsByStateCode( $arrintInsuranceCarrierLimitIds, $strStateCode, $objDatabase );
	}

	public function checkIsPolicyCoverageDetailChanged( $objInsurancePolicyCoverageDetail, $objDatabase ) {

		$boolIsChanged 			= false;
		$arrmixInsureds 		= array();
		$arrmixUpdatedInsureds 	= array();

		$arrobjInsurancePolicyEndorsements = ( array ) \Psi\Eos\Insurance\CInsurancePolicyEndorsements::createService()->fetchInsurancePolicyEndorsementsByInsurancePolicyIdByCoverageDetailId( $objInsurancePolicyCoverageDetail->getInsurancePolicyId(), $objInsurancePolicyCoverageDetail->getId(), $objDatabase );
		$arrintInsurancePolicyEndorsements = array_keys( rekeyObjects( 'InsuranceCarrierEndorsementId', $arrobjInsurancePolicyEndorsements ) );

		if( false == valObj( $objInsurancePolicyCoverageDetail, 'CInsurancePolicyCoverageDetail' ) ) {
			return $boolIsChanged;
		}

		if( $this->getLiabilityInsuranceCarrierLimitId() != $objInsurancePolicyCoverageDetail->getLiabilityInsuranceCarrierLimitId() ) {
			$boolIsChanged = true;
		}

		if( $this->getPersonalInsuranceCarrierLimitId() != $objInsurancePolicyCoverageDetail->getPersonalInsuranceCarrierLimitId() ) {
			$boolIsChanged = true;
		}

		if( $this->getDeductibleInsuranceCarrierLimitId() != $objInsurancePolicyCoverageDetail->getDeductibleInsuranceCarrierLimitId() ) {
			$boolIsChanged = true;
		}

		if( $this->getPremiumAmount() != $objInsurancePolicyCoverageDetail->getPremiumAmount() ) {
			$boolIsChanged = true;
		}

		if( true == valArr( $this->getInsurancePolicyEndorsements(), 0 ) ) {
			if( true == array_diff( $this->getInsurancePolicyEndorsements(), $arrintInsurancePolicyEndorsements ) ) {
				$boolIsChanged = true;
			}
		}

		// Creating array of additional updated insureds to compare with old additional insureds

		if( true == valArr( $this->getInsurancePolicyInsureds() ) ) {
			foreach( $this->getInsurancePolicyInsureds() as $objInsurancePolicyInsured ) {
				if( CInsuranceInsuredType::ADDITIONAL_INSURED == $objInsurancePolicyInsured->getInsuranceInsuredTypeId() ) {
					$arrmixUpdatedInsureds[] = $objInsurancePolicyInsured->getNameFirst();
					$arrmixUpdatedInsureds[] = $objInsurancePolicyInsured->getNameLast();
				}
			}
		}

		// Array of additional insureds
		$arrobjInsurancePolicyInsureds 	= ( array ) \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchInsurancePolicyInsuredsByInsurancePolicyIdByCoverageDetailIdByTypeId( $objInsurancePolicyCoverageDetail->getInsurancePolicyId(), $objInsurancePolicyCoverageDetail->getId(), CInsuranceInsuredType::ADDITIONAL_INSURED, $objDatabase );
		if( true == valArr( $arrobjInsurancePolicyInsureds ) ) {
			foreach( $arrobjInsurancePolicyInsureds as $objInsurancePolicyInsured ) {
				$arrmixInsureds[] = $objInsurancePolicyInsured->getNameFirst();
				$arrmixInsureds[] = $objInsurancePolicyInsured->getNameLast();
			}
		}

		if( true == valArr( array_diff( $arrmixUpdatedInsureds, $arrmixInsureds ), 0 ) ) {
			$boolIsChanged = true;
		}

		return $boolIsChanged;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public function fetchInsurancePolicyCoverageDetailsByInsurancePolicyId( $intInsurancePolicyId, $objInsurancePortalDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicyCoverageDetails::createService()->fetchInsurancePolicyCoverageDetailsByInsurancePolicyId( $intInsurancePolicyId, $objInsurancePortalDatabase );
	}

	public function fetchInsurancePolicyEndorsements( $strEffectiveDate, $objInsurancePortalDatabase ) {
		return \Psi\Eos\Insurance\CInsurancePolicyEndorsements::createService()->fetchInsurancePolicyEndorsementsByInsurancePolicyIdByDate( $this->getId(), $strEffectiveDate, $objInsurancePortalDatabase );
	}

}
?>