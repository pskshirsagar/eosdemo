<?php

class CInsuranceCarrierClassificationType extends CBaseInsuranceCarrierClassificationType {

	const PREFERED_CUSTOMER 				= 1;
	const NOT_PREFERED_CUSTOMER 			= 2;
	const GROUP_A 							= 3;
	const GROUP_B 							= 4;
	const GROUP_C 							= 5;
	const KEMPER_PREFERRED 					= 6;
	const KEMPER_PREFERRED_SINGLEFAMILY 	= 7;
	const KEMPER_PHONE_ONLY 				= 8;
	const QBE_PREFERED_CUSTOMER 			= 9;
	const QBE_NOT_PREFERED_CUSTOMER 		= 10;

	public static $c_arrstrUnPublishedClassificationTypeIds = [ self::GROUP_A, self::GROUP_B, self::GROUP_C, self::QBE_NOT_PREFERED_CUSTOMER ];
	public static $c_arrintPreferredPropertyIds = [ self::PREFERED_CUSTOMER, self::KEMPER_PREFERRED, self::QBE_PREFERED_CUSTOMER ];

}
?>