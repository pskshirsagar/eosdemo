<?php

class CInsuranceCustomPolicyExport extends CBaseInsuranceCustomPolicyExport {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        		break;
        }

        return $boolIsValid;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
    	}
    }

}
?>