<?php

class CPolicyReview extends CBasePolicyReview {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyReviewStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyReviewPriorityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCarrierPolicyNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuredName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalReviewStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>