<?php

use Psi\Libraries\UtilHash\CHash;

class CInsurancePolicyInsured extends CBaseInsurancePolicyInsured {

	const TYPE_PRIMARY 						= 1;
	const TYPE_SPOUSE 						= 2;
	const TYPE_CHILD 						= 3;

	public static $c_arrintAdditionalInsuredLabel = [ 1 => 'Second', 2 => 'Third', 3 => 'Fourth', 4 => 'Fifth', 5 => 'Sixth' ];

	protected $m_boolIsDelete;
	protected $m_boolIsBlanked;
	protected $m_intAssociationId;
	protected $m_strAdditionalInsuredNumber;

	protected $m_intEntityId;

	protected $m_strPassword;
	protected $m_strUserName;
	protected $m_strPasswordConfirm;
	protected $m_strOldPassword;
	protected $m_strPhoneNumber;

    public function __construct() {
        parent::__construct();

        return;
    }

    /**
     * Get Functions
     */

	public function getIsDelete() {
		return $this->m_boolIsDelete;
    }

    public function getIsBlanked() {
		return $this->m_boolIsBlanked;
    }

    public function getAssociationId() {
		return $this->m_intAssociationId;
    }

    public function getAdditionalInsuredNumber() {
		return $this->m_strAdditionalInsuredNumber;
    }

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getPasswordConfirm() {
		return $this->m_strPasswordConfirm;
	}

	public function getUserName() {
		return $this->m_strUserName;
	}

	public function getOldPassword() {
		return $this->m_strOldPassword;
	}

	public function getPhoneNumber() {
		return preg_replace( '/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $this->m_strPhoneNumber );
	}

    /**
     * Set Functions
     */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['password'] ) )            $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['password'] ) : $arrstrValues['password'] );
		if( true == isset( $arrstrValues['username'] ) )            $this->setUserName( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['username'] ) : $arrstrValues['username'] );
		if( true == isset( $arrstrValues['password_confirm'] ) ) 	$this->setPasswordConfirm( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['password_confirm'] ) : $arrstrValues['password_confirm'] );
		if( true == isset( $arrstrValues['old_password'] ) ) 		$this->setOldPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['old_password'] ) : $arrstrValues['old_password'] );
		if( true == isset( $arrstrValues['entity_id'] ) )           $this->setEntityId( ( true == $boolStripSlashes ) ? stripslashes( $arrstrValues['entity_id'] ) : $arrstrValues['entity_id'] );

		return;
	}

  	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
    }

	public function setIsBlanked( $boolIsBlanked ) {
		$this->m_boolIsBlanked = $boolIsBlanked;
    }

    public function setAssociationId( $intAssociationId ) {
		$this->m_intAssociationId = $intAssociationId;
    }

    public function setAdditionalInsuredNumber( $strAdditionalInsuredNumber ) {
    	$this->m_strAdditionalInsuredNumber = $strAdditionalInsuredNumber;
    }

	public function setEntityId( $intEntityId ) {
		$this->m_intEntityId = $intEntityId;
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = $strPassword;
	}

	public function setPasswordConfirm( $strPasswordConfirm ) {
		$this->m_strPasswordConfirm = $strPasswordConfirm;
	}

	public function setUserName( $strUserName ) {
		$this->m_strUserName = $strUserName;
	}

	public function setOldPassword( $strOldPassword ) {
		$this->m_strOldPassword = $strOldPassword;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = preg_replace( '/[^0-9]/', '', $strPhoneNumber );
	}

    /**
     * Validate Functions
     */

    public function valBirthDate( $boolBirthDateIsRequired = false, $boolIsCheckAbove18 = false ) {

        $boolIsValid = true;

        if( true == isset( $this->m_strBirthDate ) && 0 < strlen( $this->m_strBirthDate ) && 1 !== CValidation::checkDate( $this->m_strBirthDate ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Birth date must be in mm/dd/yyyy form.' ) );

        } elseif( false == isset( $this->m_strBirthDate ) && true == $boolBirthDateIsRequired ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date', 'Birth date is required.' ) );
        }

        if( true == $boolIsValid && true == $boolIsCheckAbove18 && false == $this->isAboveEighteenYearsOld() ) {
        	$boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'birth_date',  'Age should be above 18.' ) );
        }

        return $boolIsValid;
    }

    public function valFirstNameLastNameBirthDate( $objDatabase, $intInsurancePolicyId ) {

    	$boolIsValid = true;

		if( false == is_null( $this->getNameFirst() ) || false == is_null( $this->getNameLast() ) || false == is_null( $this->getBirthDate() ) ) {
			$boolIsValid &= $this->valBirthDate( true );
        }

        $this->setIsBlanked( false );
        if( true == is_null( $this->getNameFirst() ) && true == is_null( $this->getNameLast() ) && true == is_null( $this->getBirthDate() ) ) {
        	$this->setIsBlanked( true );
        }

        if( false == is_null( $objDatabase ) ) {

            $objPrexistingInsurancePolicyInsured = \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchPreExistingInsurancePolicyInsuredByInsurancePolicyIdByFirstNameByLastName( $intInsurancePolicyId, addslashes( $this->getNameFirst() ), addslashes( $this->getNameLast() ), $this->getId(), $objDatabase );

			if( true == valObj( $objPrexistingInsurancePolicyInsured, 'CInsurancePolicyInsured' ) ) {
				$this->setId( $objPrexistingInsurancePolicyInsured->getId() );
				$this->setAssociationId( $objPrexistingInsurancePolicyInsured->getId() );
			}
        }

        return $boolIsValid;
	}

	public function valNameFirst() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getNameFirst() ) || 0 == strlen( $this->getNameFirst() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, $this->getAdditionalInsuredNumber() . '_1', 'First name is required.', NULL ) );

    	} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameFirst() ) || true == stristr( $this->getNameFirst(), '"' ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getAdditionalInsuredNumber() . '_1', 'First name should be alphanumeric .' ) );
    	}

    	return $boolIsValid;
    }

	public function valNameLast() {
    	$boolIsValid = true;
    	if( true == is_null( $this->getNameLast() ) || 0 == strlen( $this->getNameLast() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, $this->getAdditionalInsuredNumber() . '_2', 'Last name is required.', NULL ) );

    	} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameLast() ) || true == stristr( $this->getNameLast(), '"' ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->getAdditionalInsuredNumber() . '_2', 'Last name should be alphanumeric.' ) );
    	}

    	return $boolIsValid;
    }

	public function valEmailAddress( $intEntityId, $objInsurancePortalDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) || 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email is required.' ) );
			return $boolIsValid;

		} else if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email does not appear to be valid.', 1 ) );
			return $boolIsValid;
		}

		// Make sure it does not already exist in the insureds table for complete policy
		if( false == is_null( $this->getEmailAddress() ) && 0 < strlen( $this->getEmailAddress() ) && true == valObj( $objInsurancePortalDatabase, 'CDatabase' ) ) {

			$objInsurancePolicyInsured = \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchInsurancePolicyInsuredByEmailAddressByEntityIdByPassword( $this->getEmailAddress(), $objInsurancePortalDatabase, $intEntityId, NULL );

			if( true == valObj( $objInsurancePolicyInsured, 'CInsurancePolicyInsured' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Email is already being used.', 2 ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valPrimaryNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameFirst() ) || true == stristr( $this->getNameFirst(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name should be set of alphabets.' ) );

		} elseif( true == stristr( $this->m_strNameFirst, '@' ) || true == stristr( $this->m_strNameFirst, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameLast() ) || true == stristr( $this->getNameLast(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name should be set of alphabets.' ) );

		} elseif( true == stristr( $this->m_strNameLast, '@' ) || true == stristr( $this->m_strNameLast, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber( $boolIsRequired = false ) {

		$boolIsValid = true;

		$intLength = strlen( $this->m_strPhoneNumber );

		$strSectionName = 'Phone';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_strPhoneNumber ) || 0 == $intLength ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number is required.' ) );
			}
		}

		if( false == is_null( $this->m_strPhoneNumber ) && 0 < $intLength && ( 15 >= $intLength ) ) {
			$strPhonenumberCheck = '/^(\([0-9]{3}\))\s([0-9]{3})-([0-9]{4})$/';
			$strPhoneNumberMatch = preg_replace( '/([0-9]{3})([0-9]{3})([0-9]{4})/', '($1) $2-$3', $this->m_strPhoneNumber );

			if( preg_match( $strPhonenumberCheck, $strPhoneNumberMatch ) !== 1 ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Valid ' . $strSectionName . ' number is required.', 3 ) );
			}
		} elseif( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number length must be between 10 to 15.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valAge() {
		$boolIsValid = true;
		$intLength   = strlen( $this->m_intAge );

		if( true == is_null( $this->m_intAge ) || 0 == $intLength ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', 'Age is required.' ) );
		} elseif( $this->m_intAge > 120 || $this->m_intAge <= 0 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'age', 'Please enter age between 1 to 120' ) );
		}

		return $boolIsValid;
	}

    public function validate( $strAction, $objDatabase, $intInsurancePolicyId, $intEntityId = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'prospect_portal_secondary_people':
           	 	$boolIsValid &= $this->valFirstNameLastNameBirthDate( $objDatabase, $intInsurancePolicyId );
            	break;

            case 'validate_insured_info':
            	$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
            	break;

	        case 'validate_personal_info':
		        $boolIsValid &= $this->valPrimaryNameFirst();
		        $boolIsValid &= $this->valPrimaryNameLast();
		        $boolIsValid &= $this->valEmailAddress( $intEntityId, $objDatabase );
		        $boolIsValid &= $this->valPhoneNumber( true );
		        break;

	        case 'validate_age':
		        $boolIsValid = $this->valAge();
		        break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	/**
	 * Validate Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

	public function insurancePortalLogin( $objInsurancePortalDatabase, $boolIsPasswordRequired = true ) {

    	// We can log in through CA for incomplete policy as well. So will not check for status
		if( true == $boolIsPasswordRequired ) {
			$objInsurancePolicyInsured = \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchInsurancePolicyInsuredByEmailAddressByEntityIdByPassword( $this->getUsername(), $objInsurancePortalDatabase, $intEntityId = NULL, $this->getPasswordEncrypted() );
		} else {
			$objInsurancePolicyInsured = \Psi\Eos\Insurance\CInsurancePolicyInsureds::createService()->fetchInsuredByEmailAddressById( $this->getUsername(), $this->getId(), $objInsurancePortalDatabase );
		}

		if( true == valObj( $objInsurancePolicyInsured, 'CInsurancePolicyInsured' ) ) {
			$this->m_boolIsLoggedIn = true;
			$this->m_intId = $objInsurancePolicyInsured->getId();
		} else {
			$this->addErrorMsg( new CErrorMsg( E_USER_ERROR, 'username', 'Invalid username or password.', 304 ) );
			return array( false, '' );
		}

		return array( true, $objInsurancePolicyInsured->getEntityId() );
	}

	public function encryptPassword() {
		$this->m_strPasswordEncrypted = CHash::createService()->hashEntity( $this->getPassword() );
	}

}
?>