<?php

class CMasterPolicyBillingBatch extends CBaseMasterPolicyBillingBatch {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMasterPolicyBatchTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchTotal() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBatchPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>