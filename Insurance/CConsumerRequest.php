<?php

class CConsumerRequest extends CBaseConsumerRequest {

	protected $m_strConsumerRequestTypeNames;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameFirst() ) || true == stristr( $this->getNameFirst(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name should be set of alphabets.' ) );

		} elseif( true == stristr( $this->m_strNameFirst, '@' ) || true == stristr( $this->m_strNameFirst, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );

		} elseif( false == preg_match( "/^[A-Za-z0-9-_.\",'\s]+$/", $this->getNameLast() ) || true == stristr( $this->getNameLast(), '"' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name should be set of alphabets.' ) );

		} elseif( true == stristr( $this->m_strNameLast, '@' ) || true == stristr( $this->m_strNameLast, 'Content-type' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) || 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email is required.' ) );
			return $boolIsValid;

		} else if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email does not appear to be valid.', 1 ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPolicyReferenceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber( $boolIsRequired = false ) {
		$boolIsValid = true;

		$intLength = strlen( $this->m_strPhoneNumber );

		$strSectionName = 'Phone';

		if( true == $boolIsRequired ) {
			if( true == is_null( $this->m_strPhoneNumber ) || 0 == $intLength ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number is required.' ) );
			}
		}

		if( false == is_null( $this->m_strPhoneNumber ) && 0 < $intLength && ( 15 >= $intLength ) ) {
			$strPhonenumberCheck = '/^(\([0-9]{3}\))\s([0-9]{3})-([0-9]{4})$/';

			if( preg_match( $strPhonenumberCheck, $this->m_strPhoneNumber ) !== 1 ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Valid ' . $strSectionName . ' number is required.', 3 ) );
			}
		} elseif( 0 < $intLength && ( 10 > $intLength || 15 < $intLength ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', $strSectionName . ' number length must be between 10 to 15.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConsumerRequestTypeIds() {
		$boolIsValid = true;

		if( NULL == $this->getConsumerRequestTypeIds() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'consumer_request_types', 'Please select atleast one option.' ) );
		}

		return $boolIsValid;
	}

	public function valComments() {
		$boolIsValid = true;

		if( true == is_null( $this->getComments() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'comments', 'comments is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsReviewed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsResponded() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRespondedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_customer_personal_info':
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valConsumerRequestTypeIds();
				if( true == $this->valConsumerRequestTypeIds() ) {
					$arrintConsumerRequestTypeIds = get_object_vars( $this->getConsumerRequestTypeIds() );
					if( true == in_array( CConsumerRequestType::OTHER, $arrintConsumerRequestTypeIds['customer_request_type_ids'] ) ) {
						$boolIsValid &= $this->valComments();
					}
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getConsumerRequestTypeNames() {
		return $this->m_strConsumerRequestTypeNames;
	}

	public function setConsumerRequestTypeNames( $strConsumerRequestTypeNames ) {
		$this->m_strConsumerRequestTypeNames = $strConsumerRequestTypeNames;
	}

}
?>