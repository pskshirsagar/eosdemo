<?php

class CInsuranceCarrierPreference extends CBaseInsuranceCarrierPreference {

	const MARKEL_PHONE_NUMBER		 	= '1-800-236-3113';
	const KEMPER_PHONE_NUMBER		 	= '1-877-586-8951';
	const QBE_PHONE_NUMBER		 		= '1-844-723-2524';
	const RI_SERVICE_PHONE_NUMBER	 	= '(877) 577-0850';
	const NEW_RI_SERVICE_PHONE_NUMBER	= '(866) 249-1066';

	const ADMIN_FEE 					= 2.49;
	const POLICY_ENROLLMENT_DATE_LIMIT	= 120;

	const IDTHEFT_INDIVIDUAL_PROTECTION_BASE_RATE 	= 3;
	const IDTHEFT_FAMILY_PROTECTION_BASE_RATE 		= 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           		break;

	        default:
		        $boolIsValid = true;
		        break;
        }

        return $boolIsValid;
    }

}
?>