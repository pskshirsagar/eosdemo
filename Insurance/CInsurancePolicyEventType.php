<?php

class CInsurancePolicyEventType extends CBaseInsurancePolicyEventType {

	const INSURANCE_POLICY_CANCEL_EVENT                          = 1;
	const INSURANCE_POLICY_EDIT_MAILING_INFO_EVENT               = 2;
	const INSURANCE_POLICY_PAYMENT_INFO_EVENT                    = 3;
	const INSURANCE_POLICY_ADDITIONAL_INSURED_INFO_EVENT         = 4;
	const INSURANCE_POLICY_LAPSE_EVENT                           = 5;
	const INSURANCE_POLICY_NOT_FOUND_EVENT                       = 6;
	const INSURANCE_POLICY_BILLING_OPTION_MISMATCH               = 7;
	const PAYMENT_INFORMATION_CHANGE                             = 8;
	const INSURANCE_POLICY_PROPERTY_CHANGE                       = 9;
	const INSURANCE_POLICY_EDIT_INSURED_ADDRESS_INFO_EVENT       = 10;
	const INSURANCE_POLICY_COVERAGE_CHANGE                       = 11;
	const INSURANCE_POLICY_PAYMENT_RE_ATTEMPT                    = 20;
	const INSURANCE_POLICY_START_DATE_CHANGE                     = 21;
	const INSURANCE_POLICY_FREQUENCY_CHANGE                      = 22;
	const INSURANCE_POLICY_REVERT_CANCEL_EVENT                   = 23;
	const INSURANCE_POLICY_ADJUSTMENT_ATTEMPT                    = 24;
	const INSURANCE_POLICY_RATE_CHANGE_EMAIL_EVENT               = 25;
	const INSURANCE_POLICY_PROOF_OF_COVERAGE_DOWNLOAD            = 26;
	const POLICY_ACCOUNT_INFORMATION_NOTICE_OF_CHANGE            = 27;
	const INSURANCE_POLICY_EDIT_EMAIL_ADDRESS_EVENT              = 28;
	const INSURANCE_POLICY_EDIT_PHONE_NUMBER_EVENT               = 29;
	const INSURANCE_POLICY_PAYMENT_COLLECTION_TIMEOUT            = 30;
	const INSURANCE_POLICY_PAYMENT_RE_COLLECTION_TIMEOUT         = 31;
	const INSURANCE_POLICY_PAYMENT_COLLECTION_ADJUSTMENT_TIMEOUT = 32;
	const INSURANCE_POLICY_PAYMENT_RETURN_ADJUSTMENT_TIMEOUT     = 33;
	const INSURANCE_POLICY_CHARGE_TIMEOUT                        = 34;
	const INSURANCE_POLICY_FIRST_LAPSE_COMMUNICATION             = 35;
	const INSURANCE_POLICY_REVERSE_LAPSE_COMMUNICATION           = 36;

	public static $c_arrintPersonalInfoChangeEventTypeIds = [ self::INSURANCE_POLICY_EDIT_EMAIL_ADDRESS_EVENT, self::INSURANCE_POLICY_EDIT_PHONE_NUMBER_EVENT ];
	public static $c_arrintPolicyInfoChangeEventTypeIds = [ self::INSURANCE_POLICY_EDIT_MAILING_INFO_EVENT, self::INSURANCE_POLICY_EDIT_INSURED_ADDRESS_INFO_EVENT, self::INSURANCE_POLICY_EDIT_EMAIL_ADDRESS_EVENT, self::INSURANCE_POLICY_EDIT_PHONE_NUMBER_EVENT ];
	public static $c_arrintCancellationEventTypeIds = [ self::INSURANCE_POLICY_CANCEL_EVENT, self::INSURANCE_POLICY_LAPSE_EVENT ];

	public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	// default case
                break;
        }

        return $boolIsValid;
    }

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}
    }

}
?>