<?php

class CPropertyStatistic extends CBasePropertyStatistic {

	protected $m_fltRIPercentage;
	protected $m_fltCompliance;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 */

	public function getRIPercentage() {
		return $this->m_fltRIPercentage;
	}

	public function getCompliance() {
		return $this->m_fltCompliance;
	}

	/**
	 * Set Functions
	 */

	public function setRIPercentage( $fltRIPercentage ) {
		$this->m_fltRIPercentage = $fltRIPercentage;
	}

	public function setCompliance( $fltCompliance ) {
		$this->m_fltCompliance = $fltCompliance;
	}
}
?>