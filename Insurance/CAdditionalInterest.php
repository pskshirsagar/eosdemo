<?php

class CAdditionalInterest extends CBaseAdditionalInterest {

	public function valName() {
		$boolIsValid = true;
		if( true == is_null( $this->getName() ) || 0 == strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) || 0 == strlen( trim( $this->getEmailAddress() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email is required.' ) );
			return $boolIsValid;

		} elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email does not appear to be valid.', 1 ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getStreetAddress() ) || 0 == strlen( $this->getStreetAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_address', 'Street Address is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;

		if( true == is_null( $this->getCity() ) || 0 == strlen( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'city', 'City is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) || 0 == strlen( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'state_code', 'State is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getPostalCode() ) || 0 == strlen( $this->getPostalCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'postal_code', 'Zip Code is required.', NULL ) );
		}
		if( true == $boolIsValid ) {
			$strZipCodeCheck = '/(^\d{5}$)|(^\d{5}-[a-zA-Z0-9]{4}$)/';
			if( preg_match( $strZipCodeCheck, $this->getPostalCode() ) !== 1 ) {

				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code',  'Zip Code is not valid.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valStreetAddress();
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>