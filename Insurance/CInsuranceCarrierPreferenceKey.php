<?php

class CInsuranceCarrierPreferenceKey extends CBaseInsuranceCarrierPreferenceKey {

	const KEMPER_COMMUNICATION_EMAIL_ADDRESS 			= 1;
	const MARKEL_POLICY_EFFECTIVE_DAYS		 			= 2;
	const KEMPER_POLICY_EFFECTIVE_DAYS		 			= 3;
	const MARKEL_ADDITIONAL_INSUREDS		 			= 4;
	const KEMPER_ADDITIONAL_INSUREDS		 			= 5;
	const NEXT_POST_ON_DAYS					 			= 6;
	const NEXT_INTEGRATION_ON_DAYS			 			= 7;
	const POLICY_LAPSED_ON_DAYS				 			= 8;
	const MARKEL_FTP_HOST_NAME_ENCRYPTED	 			= 9;
	const MARKEL_FTP_USER_NAME_ENCRYPTED	 			= 10;
	const MARKEL_FTP_PASSWORD_ENCRYPTED		 			= 11;
	const QBE_COMMUNICATION_EMAIL_ADDRESS	 			= 12;
	const QBE_POLICY_EFFECTIVE_DAYS		 	 			= 13;
	const QBE_ADDITIONAL_INSUREDS		 	 			= 14;
	const QBE_SERVICE_AUTHENTICATION 		 			= 15;
	const QBE_API_USERNAME 		 			 			= 16;
	const QBE_FTP_HOST_NAME_ENCRYPTED 	 	 			= 17;
	const QBE_FTP_USER_NAME_ENCRYPTED 	 	 			= 18;
	const QBE_FTP_PASSWORD_ENCRYPTED 	 	 			= 19;
	const QBE_POLICY_PREMIUM_FAILED_EMAIL_ADDRESS 		= 20;
	const FORCED_PLACED_POLICY_FTP_HOST_NAME_ENCRYPTED 	= 21;
	const FORCED_PLACED_POLICY_FTP_USER_NAME_ENCRYPTED  = 22;
	const FORCED_PLACED_POLICY_FTP_PASSWORD_ENCRYPTED 	= 23;
	const IDTHEFT_FTP_HOST_NAME_ENCRYPTED 				= 24;
	const IDTHEFT_FTP_USER_NAME_ENCRYPTED 				= 25;
	const IDTHEFT_FTP_PASSWORD_ENCRYPTED 				= 26;
	const IDTHEFT_POLICY_EFFECTIVE_DAYS 				= 27;
	const IDTHEFT_SINGLE_SIGN_ON_KEY 					= 28;
	const IDTHEFT_SINGLE_SIGN_ON_IV  					= 29;
	const IDTHEFT_SINGLE_SIGN_ON_API_URL  				= 30;
	const SSIS_CLAIM_FTP_HOST_NAME_ENCRYPTED 			= 31;
	const SSIS_CLAIM_FTP_USER_NAME_ENCRYPTED  			= 32;
	const SSIS_CLAIM_FTP_PASSWORD_ENCRYPTED  			= 33;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

        	case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

	        default:
		        // default case
		        $boolIsValid = true;
		        break;
        }

        return $boolIsValid;
    }

}
?>