<?php

class CInsuranceInvoiceDetailResultType extends CBaseInsuranceInvoiceDetailResultType {

	const PASS                                         = 1;
	const DUPLICATE_CHARGED                            = 2;
	const POLICY_NOT_EXIST                             = 3;
	const CHARGE_NOT_EXIST                             = 4;
	const PREMIUM_MISMATCH                             = 5;
	const COMMISSION_MISMATCH                          = 6;
	const INSURED_NAME_MISMATCH                        = 7;
	const PREMIUM_MISMATCH_CANCELLED_POLICY_OVERBILLED = 8;
	const PREMIUM_MISMATCH_ACTIVE_POLICY_OVERBILLED    = 9;

	public static $c_arrintInsuranceInvoiceDetailResultTypes = [ self::PASS => 'Pass', self::DUPLICATE_CHARGED => 'Duplicate Charged', self::POLICY_NOT_EXIST => 'Policy Not Exist', self::CHARGE_NOT_EXIST => 'Charge Not Exist', self::PREMIUM_MISMATCH => 'Premium Mismatch', self::COMMISSION_MISMATCH => 'Commission Mismatch', self::INSURED_NAME_MISMATCH => 'Insured Name Mismatch', self::PREMIUM_MISMATCH_CANCELLED_POLICY_OVERBILLED => 'Premium Mismatch Cancelled Over Billed', self::PREMIUM_MISMATCH_ACTIVE_POLICY_OVERBILLED => 'Premium Mismatch Active Over Billed' ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>