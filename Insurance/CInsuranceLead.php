<?php

class CInsuranceLead extends CBaseInsuranceLead {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyQuoteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMoveInPerformedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuredOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>