<?php

class CInsurancePolicyQuoteHistory extends CBaseInsurancePolicyQuoteHistory {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyQuoteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadSourceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>