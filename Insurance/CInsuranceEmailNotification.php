<?php

class CInsuranceEmailNotification extends CBaseInsuranceEmailNotification {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

	public function createInsuranceEmailNotification( $arrmixParam, $objInsuranceDatabase ) {
    	$this->setValues( $arrmixParam );
		$this->setId( NULL );// Since condition is Insert, ID shouldn't be set
		if( false == $this->insert( $arrmixParam['user_id'], $objInsuranceDatabase ) ) {
			return false;
		}
		return true;
	}

}
?>