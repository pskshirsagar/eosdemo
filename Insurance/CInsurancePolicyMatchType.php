<?php

class CInsurancePolicyMatchType extends CBaseInsurancePolicyMatchType {

	const MATCHED_THROUGH_SCRIPT    = 1;
	const MATCHED_DURING_ENROLLMENT = 2;
	const MATCHED_BY_ENTRATA_USER   = 3;
	const MATCHED_BY_CA_USER        = 4;
	const UNMATCHED_POLICY          = 5;
	const ARCHIEVE_POLICY           = 6;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>