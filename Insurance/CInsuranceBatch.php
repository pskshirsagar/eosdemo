<?php

class CInsuranceBatch extends CBaseInsuranceBatch {

    public function valInsuranceCarrierId() {
        $boolIsValid = true;

        if( false == is_numeric( $this->getInsuranceCarrierId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Insurance Carrier is required.', NULL ) );
        }

        return $boolIsValid;
    }

    public function valCarrierBatchReference() {
        $boolIsValid = true;

        if( true == is_null( $this->getCarrierBatchReference() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Carrier Batch Reference is required.', NULL ) );
        }

        return $boolIsValid;
    }

    public function valBatchDate() {
        $boolIsValid = true;

		if( true == is_null( $this->getBatchDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Batch Date is required.', NULL ) );
		}
        return $boolIsValid;
    }

    public function valInvoiceFileName() {
    	$boolIsValid = true;

    	if( true == is_null( $this->getInvoiceFileName() ) || 0 == strlen( $this->getInvoiceFileName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Select Insurance Invoice File.', NULL ) );
    	}
    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	case 'validate_create_batch':
        		$boolIsValid &= $this->valInsuranceCarrierId();
        		$boolIsValid &= $this->valBatchDate();
        		$boolIsValid &= $this->valCarrierBatchReference();
        		$boolIsValid &= $this->valInvoiceFileName();
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setInvoiceFileName( $strInvoiceFileName ) {
    	$this->m_strInvoiceFileName = CStrings::strTrimDef( $strInvoiceFileName, NULL, NULL, true );
    }

    public function getInvoiceFileName() {
    	return $this->m_strInvoiceFileName;
    }

}
?>