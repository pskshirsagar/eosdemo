<?php
class CInsuranceCharge extends CBaseInsuranceCharge {

	protected $m_fltTaxAmount;
	protected $m_fltPremiumAmount;
	protected $m_fltEarnedPremiumAmount;
	protected $m_fltChargeAmount;

	public function getTaxAmount() {
		return $this->m_fltTaxAmount;
	}

	public function getPremiumAmount() {
		return $this->m_fltPremiumAmount;
	}

	public function getEarnedPremiumAmount() {
		return $this->m_fltEarnedPremiumAmount;
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function setTaxAmount( $fltTaxAmount ) {
		$this->m_fltTaxAmount = $fltTaxAmount;
	}

	public function setPremiumAmount( $fltPremiumAmount ) {
		$this->m_fltPremiumAmount = $fltPremiumAmount;
	}

	public function setEarnedPremiumAmount( $fltEarnedPremiumAmount ) {
		$this->m_fltEarnedPremiumAmount = $fltEarnedPremiumAmount;
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->m_fltChargeAmount = $fltChargeAmount;
	}

	public function valId() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>