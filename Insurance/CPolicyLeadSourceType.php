<?php

class CPolicyLeadSourceType extends CBasePolicyLeadSourceType {

	const DIRECT                               = 1;
	const PHONE                                = 2;
	const APPLICATION                          = 3;
	const ONLINE_LEASE                         = 4;
	const RESIDENT_PORTAL                      = 5;
	const RENT_EMAIL                           = 6;
	const RI_ENROLLMENT                        = 7;
	const MOVE_IN_RI_ENROLLMENT                = 8;
	const ADD_NEW_POLICY                       = 9;
	const REINSTATE_POLICY                     = 10;
	const UNINSURED_RESIDENT_EMAIL             = 11;
	const ENTRATA                              = 12;
	const IDTHEFT_SUBSCRIPTION                 = 13;
	const POINT_OF_LEASE                       = 14;
	const ONLINE_LEASE_POL                     = 15;
	const CUSTOM_POLICY_VERIFICATION_DENIED    = 16;
	const MP_FIRST_NOTICE                      = 17;
	const MP_LAST_NOTICE                       = 18;
	const MP_RENEWAL_NOTICE                    = 19;
	const EXPIRING_SOON_NOTICE                 = 20;
	const LEASE_COMPLETED_INSURANCE_INVITATION = 21;
	const MANUAL_INVITATION                    = 22;
	const LEASE_COMPLETED_FOLLOWUP             = 23;
	const MOVE_IN_POL                          = 24;

	public static $c_arrintPOLLeadSourceTypeIds = [ self::ONLINE_LEASE_POL, self::CUSTOM_POLICY_VERIFICATION_DENIED, self::MP_FIRST_NOTICE, self::MP_LAST_NOTICE, self::MP_RENEWAL_NOTICE, self::EXPIRING_SOON_NOTICE, self::LEASE_COMPLETED_INSURANCE_INVITATION, self::MANUAL_INVITATION, self::LEASE_COMPLETED_FOLLOWUP, self::MOVE_IN_POL ];

}
?>