<?php

class CInsuranceInvoiceDetail extends CBaseInsuranceInvoiceDetail {

	protected $m_strStateCode;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrstrValues['state_code'] ) )	$this->setStateCode( $arrstrValues['state_code'] );
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

}
?>