<?php

class CInsurancePolicyCoverageDetailStatusType extends CBaseInsurancePolicyCoverageDetailStatusType {

	const CURRENT 	= 1;
	const FUTURE 	= 2;
	const PAST 		= 3;

	public static $c_arrintIPCoverageDetailsStausTypes = array( self::CURRENT, self::FUTURE, self::PAST );
	public static $c_arrstrIPCoverageDetailsStausTypes = array( self::CURRENT => 'Current coverage',
	                                                            self::FUTURE  => 'Future coverage',
	                                                            self::PAST    => 'Past coverage' );

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>