<?php

class CMasterPolicyClient extends CBaseMasterPolicyClient {

	const VALIDATE_MASTER_POLICY_CLIENT = 'VALIDATE_MASTER_POLICY_CLIENT';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select client. ' ) );
		}

		return $boolIsValid;
	}

	public function valProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select domicile state code. ' ) );
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeactivationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTerminationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case self::VALIDATE_MASTER_POLICY_CLIENT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valStateCode();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>