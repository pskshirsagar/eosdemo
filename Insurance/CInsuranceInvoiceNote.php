<?php

class CInsuranceInvoiceNote extends CBaseInsuranceInvoiceNote {

	public function valNote() {
		$boolIsValid = true;
		if( true == is_null( $this->getNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'note', 'Invoice note is required.', NULL ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>