<?php
class CInsurancePolicyQuoteError extends CBaseInsurancePolicyQuoteError {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            default:
            	$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

    public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

    	if( true == is_null( $this->getId() ) ) {
    		return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	} else {
    		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
    	}
    }
}
?>