<?php

class CInsurancePolicyCommission extends CBaseInsurancePolicyCommission {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAgentCommissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagerCommissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>