<?php

class CClaimLog extends CBaseClaimLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLossTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getLossTypeId() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getLossTypeId() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'loss_type', __( ' Type of loss required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valClaimStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCarrierPolicyNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuredName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimPartyClassification() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCoverageCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalClaimAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCovCAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCovDAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCovEAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLossAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReserves() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimCategoryIds() {
		$boolIsValid = true;
		if( false == valArr( $this->getClaimCategoryIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'claim_category_ids', __( ' Damage/Loss type is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLossDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLossDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getLossDate() ) || 0 == \Psi\CStringService::singleton()->strlen( trim( $this->getLossDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'loss_date', __( ' Loss date is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valClaimReportedDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimOpenDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClaimCloseDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_claim_log_details':
				$boolIsValid &= $this->valLossDate();
				$boolIsValid &= $this->valClaimCategoryIds();
				$boolIsValid &= $this->valLossTypeId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>