<?php

class CPolicyReviewStatusType extends CBasePolicyReviewStatusType {

	const PENDING_REVIEW = 1;
	const IN_PROGRESS    = 2;
	const UNDER_REVIEW   = 3;
	const APPROVED       = 4;
	const DENIED         = 5;
	const ARCHIVE        = 6;

	public static $c_arrintPolicyReviewStatus = [ self::PENDING_REVIEW => 'unverified_policies', self::IN_PROGRESS => 'inprogress_policies', self::UNDER_REVIEW => 'underreview_policies', self::APPROVED => 'approved_policies', self::DENIED => 'denied_policies' ];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>