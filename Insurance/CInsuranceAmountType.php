<?php

class CInsuranceAmountType extends CBaseInsuranceAmountType {

	const TYPE_250		 = 1;
	const TYPE_500		 = 2;
	const TYPE_1K		 = 3;
	const TYPE_5K		 = 4;
	const TYPE_10K		 = 5;
	const TYPE_15K		 = 6;
	const TYPE_20K	 	 = 7;
	const TYPE_25K		 = 8;
	const TYPE_30K		 = 9;
	const TYPE_35K		 = 10;
	const TYPE_40K		 = 11;
	const TYPE_45K		 = 12;
	const TYPE_50K		 = 13;
	const TYPE_55K		 = 14;
	const TYPE_60K		 = 15;
	const TYPE_65K		 = 16;
	const TYPE_70K		 = 17;
	const TYPE_75K		 = 18;
	const TYPE_100K		 = 19;
	const TYPE_300K	 	 = 20;
	const TYPE_12K	 	 = 22;
	const TYPE_0	 	 = 23;
	const TYPE_2K	 	 = 24;
	const TYPE_80K       = 25;

	const KEMPER_FL_MIN_LIABILITY_AMOUNT = 25000;
	const MARKEL_MIN_LIABILITY_AMOUNT    = 300000;

	// const DEFAULT_MEDICAL_PAYMENT_AMOUNT = '1,000.0000';
	// const MARKEL_DEFAULT_MEDICAL_PAYMENT_AMOUNT = '500.0000';

	const MARKEL_DEFAULT_MEDICAL_PAYMENT_AMOUNT = '$500';
	const KEMPER_DEFAULT_MEDICAL_PAYMENT_AMOUNT = '$1000';
	const QBE_DEFAULT_MEDICAL_PAYMENT_AMOUNT    = '$1000';

	public static $c_arrintKemperFLLiabilityIds = [ self::TYPE_25K, self::TYPE_50K, self::TYPE_100K ];
	public static $c_arrintKemperFLPersonalContentIds = [ self::TYPE_5K, self::TYPE_10K, self::TYPE_15K ];
	public static $c_arrintKemperFLDeductibleContentIds = [ self::TYPE_500, self::TYPE_1K ];
	public static $c_arrintMarkelPhonePersonalContentIds = [ self::TYPE_60K, self::TYPE_70K, self::TYPE_80K ];
	public static $c_arrintMarkelPhonePersonalContentValues = [ '$60,000', '$70,000', '$80,000' ];

}

?>