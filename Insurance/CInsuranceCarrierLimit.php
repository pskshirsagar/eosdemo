<?php

class CInsuranceCarrierLimit extends CBaseInsuranceCarrierLimit {

	protected $m_fltInsuranceAmount;

	public static $c_arrintCarrierMaxLiabilityLimit = [ CInsuranceCarrier::MARKEL => 300000, CInsuranceCarrier::KEMPER => 100000, CInsuranceCarrier::QBE => 100000 ];

    /**
     * Get Functions
     */

	public function getInsuranceAmount() {
		return $this->m_fltInsuranceAmount;
	}

	/**
     * Set Functions
     */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['amount'] ) ) 	$this->setInsuranceAmount( $arrmixValues['amount'] );
	}

	public function setInsuranceAmount( $fltInsuranceAmount ) {
		$this->m_fltInsuranceAmount = $fltInsuranceAmount;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>