<?php

class CInsurancePropertyProduct extends CBaseInsurancePropertyProduct {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsurancePolicyTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInsuranceCarrierClassificationTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinimumBillingFrequencyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultLiabilityInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPersonalInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeductibleInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinLiabilityInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxLiabilityInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinDeductibleInsuranceCarrierLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultMedicalPaymentLimitId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEnhancementEndorsementId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSubDomain() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAllowedLiabilityOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsGeneric() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAutoEnabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>