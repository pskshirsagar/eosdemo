<?php

class CInsuranceEmailNotificationType extends CBaseInsuranceEmailNotificationType {

	const EMAIL_NOTIFICATION_TYPE_MOVE_IN_EMAIL                             = 1;
	const EMAIL_NOTIFICATION_TYPE_CREDIT_CARD_ABOUT_TO_EXPIRE               = 2;
	const EMAIL_NOTIFICATION_TYPE_UNINSURED_RESIDENT_COMPLIANCE_EMAIL       = 3;
	const EMAIL_NOTIFICATION_TYPE_UPCOMING_AUTOMATIC_PAYMENT_REMINDER_EMAIL = 4;
	const EMAIL_NOTIFICATION_TYPE_PREMIUM_PAYMENT_RECEIPT_EMAIL             = 5;
	const EMAIL_NOTIFICATION_TYPE_FORCE_PLACED_EMAIL                        = 6;
	const EMAIL_NOTIFICATION_TYPE_ID_THEFT_SUBSCRIPTION_EMAIL               = 7;
	const LEASE_COMPLETED_INSURANCE_EMAIL                                   = 8;
	const INSURANCE_MANUAL_INVITE                                           = 10;

	const LEASE_COMPLETED_FOLLOWUP_3_DAYS_INSURANCE_EMAIL                   = 9;
	const LEASE_COMPLETED_FOLLOWUP_10_DAYS_INSURANCE_EMAIL                  = 13;
	const MOVE_IN_POL_3_DAYS_NOTICE                                         = 11;
	const MOVE_IN_POL_10_DAYS_NOTICE                                        = 12;
	const PAYMENT_SKIPPED_POLICIES_FOLLOWUP_EMAIL                           = 14;

	public static $c_arrintLeaseCompletedFollowupNotificationType = [ self::LEASE_COMPLETED_FOLLOWUP_3_DAYS_INSURANCE_EMAIL, self::LEASE_COMPLETED_FOLLOWUP_10_DAYS_INSURANCE_EMAIL ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}

?>