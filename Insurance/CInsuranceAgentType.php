<?php

class CInsuranceAgentType extends CBaseInsuranceAgentType {

	const INSURANCE_POLICY_AGENT_STATUS_TYPE_AGENCY_ADMIN  = 1;
	const INSURANCE_POLICY_AGENT_STATUS_TYPE_ADVANCE_AGENT = 2;
	const INSURANCE_POLICY_AGENT_STATUS_TYPE_NORMAL_AGENT  = 3;
	const INSURANCE_POLICY_AGENT_STATUS_TYPE_OTHER_USERS   = 4;

	public static $c_arrstrInsuranceAgantTypes = [ self::INSURANCE_POLICY_AGENT_STATUS_TYPE_AGENCY_ADMIN  => 'Agency Admin',
	                                               self::INSURANCE_POLICY_AGENT_STATUS_TYPE_ADVANCE_AGENT => 'Advanced Agent',
	                                               self::INSURANCE_POLICY_AGENT_STATUS_TYPE_NORMAL_AGENT  => 'Normal Agent',
	                                               self::INSURANCE_POLICY_AGENT_STATUS_TYPE_OTHER_USERS   => 'Other Users' ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}

?>