<?php

class CInsurancePolicyCancellationType extends CBaseInsurancePolicyCancellationType {

	const MOVING = 1;
	const NEW_PROVIDER = 2;
	const BUYING_A_HOUSE = 3;
	const DOUBLE_COVERAGE = 4;
	const CHANGE_OF_ADDRESS = 5;
	const OTHER = 6;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

			case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>