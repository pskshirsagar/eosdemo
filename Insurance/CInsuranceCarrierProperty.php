<?php
/**
 * @method int getPropertyTypeId()
 * @method void setPropertyTypeId( int $intPropertyTypeId )
 */
class CInsuranceCarrierProperty extends CBaseInsuranceCarrierProperty {

	protected $m_intClientStatusTypeId;

	protected $m_strCityLimits;
	protected $m_strAgeOfFacility;
	protected $m_strConstructionType;

	protected $m_arrobjInsuranceCarrierPreferences;

	public function setValues( $arrmixValues, $boolIsStripSlashes = true, $boolIsDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolIsStripSlashes, $boolIsDirectSet );

		if( true == isset( $arrmixValues['city_limits'] ) ) {
			$this->setCityLimits( $arrmixValues['city_limits'] );
		}
		if( true == isset( $arrmixValues['age_of_facility'] ) ) {
			$this->setAgeOfFacility( $arrmixValues['age_of_facility'] );
		}
		if( true == isset( $arrmixValues['construction_type'] ) ) {
			$this->setConstructionType( $arrmixValues['construction_type'] );
		}
	}

	public function getClientStatusTypeId() {
		return $this->m_intClientStatusTypeId;
	}

	public function getInsuranceCarrierPreferences() {
		return $this->m_arrobjInsuranceCarrierPreferences;
	}

	public function getConstructionType() {
		return $this->m_strConstructionType;
	}

	public function getCityLimits() {
		return $this->m_strCityLimits;
	}

	public function getAgeOfFacility() {
		return $this->m_strAgeOfFacility;
	}

	public function setClientStatusTypeId( $intClientStatusTypeId ) {
		$this->m_intClientStatusTypeId = $intClientStatusTypeId;
	}

	public function setInsuranceCarrierPreferences( $arrobjInsuranceCarrierPreferences ) {
		$this->m_arrobjInsuranceCarrierPreferences = $arrobjInsuranceCarrierPreferences;
	}

	public function setConstructionType( $strConstructionType ) {
		$this->m_strConstructionType = $strConstructionType;
	}

	public function setCityLimits( $strCityLimits ) {
		$this->m_strCityLimits = $strCityLimits;
	}

	public function setAgeOfFacility( $strAgeOfFacility ) {
		$this->m_strAgeOfFacility = $strAgeOfFacility;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId( $objInsurancePortalDatabase, $boolAllowDuplicate = false ) {
		$boolIsValid = true;

		if( false == is_null( $this->getCid() ) && true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required. ' ) );
		}

		if( false == $boolAllowDuplicate && false == is_null( $objInsurancePortalDatabase ) ) {
			$intPropertySetUpCount = \Psi\Eos\Insurance\CInsuranceCarrierProperties::createService()->fetchInsuranceCarrierPropertyCountByPropertyId( $this->getPropertyId(), $objInsurancePortalDatabase );

			if( 0 < $intPropertySetUpCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property already added, please select different property. ' ) );
			}
		}

		if( false == is_null( $this->getPropertyId() ) && true == is_null( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', $this->getName() . ' property is located in an unsupported state. ' ) );
		}

		return $boolIsValid;
	}

	public function valInsuranceCarrierId( $boolIsBulk = false ) {
		$boolIsValid = true;

		$strError = '';

		if( true == is_null( $this->getInsuranceCarrierId() ) ) {

			$boolIsValid = false;

			if( false == is_null( $this->getName() ) && $boolIsBulk == false ) {
				$strError = ' for ' . $this->getName();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_carrier_id', 'Insurance carrier is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyConstructionTypeId() {
		$boolIsValid = true;

		$strError = '';
		if( true == is_null( $this->getPropertyConstructionTypeId() ) && ( 'FL' == $this->getStateCode() || CInsuranceCarrier::QBE == $this->getInsuranceCarrierId() ) ) {
			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_construction_type_id', 'Construction Type is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valConstructionYear() {
		$boolIsValid = true;

		$strError = '';
		if( true == is_null( $this->getConstructionYear() ) && ( 'FL' == $this->getStateCode() || CInsuranceCarrier::QBE == $this->getInsuranceCarrierId() ) ) {

			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'construction_year', 'Construction Year is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		$strError = '';

		if( true == is_null( $this->getStateCode() ) && false == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', 'State code is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valSubDomain( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->m_strSubDomain ) ) {

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', 'Subdomain is required' . $strError . '. ' ) );
			$boolIsValid = false;
		}

		$this->m_strSubDomain = $this->checkForHttp( $this->m_strSubDomain );

		if( false == $this->checkDomainCharacters( $this->m_strSubDomain ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', 'Invalid characters in subdomain. ' ) );
			$boolIsValid = false;
		}

		$arrintReservedSubdomains = ( array ) \Psi\Eos\Insurance\CInsuranceCarrierProperties::createService()->fetchCustomInsuranceCarrierPropertyBySubDomain( $this->m_strSubDomain, $objDatabase );

		if( false == empty( $arrintReservedSubdomains[0]['id'] ) && $arrintReservedSubdomains[0]['id'] != $this->getId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_domain', 'Subdomain already exists. ' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valNoOfUnits() {
		$boolIsValid = true;

		if( true == is_null( $this->getUnitCount() ) && CInsuranceCarrier::QBE == $this->getInsuranceCarrierId() ) {
			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'construction_year', 'No of units is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getPostalCode() ) && false == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Postal code is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;

		$strError = '';

		if( false == valStr( $this->getLatitude() ) ) {
			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'longitude', 'Longitude is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;

		$strError = '';

		if( false == valStr( $this->getLatitude() ) ) {
			$boolIsValid = false;
			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'latitude', 'Latitude is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valMinimumBillingFrequencyId() {
		$boolIsValid = true;

		$strError = '';

		if( false == valStr( $this->getMinimumBillingFrequencyId() ) ) {
			$boolIsValid = false;
			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_billing_frequency_id', 'Minimum billing frequency is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valInsuranceCarrierClassificationTypeId( $boolIsBulk = false ) {
		$boolIsValid = true;

		$strError = '';

		if( false == valStr( $this->getInsuranceCarrierClassificationTypeId() ) ) {
			$boolIsValid = false;
			if( false == is_null( $this->getName() ) && $boolIsBulk == false ) {
				$strError = ' for ' . $this->getName();
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_carrier_classification_type_id', 'Property classification is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valProfessionallyManagedYear() {
		$boolIsValid = true;

		$strError = '';

		if( true == is_null( $this->getProfessionallyManagedYear() ) ) {

			$boolIsValid = false;

			if( false == is_null( $this->getName() ) ) {
				$strError = ' for ' . $this->getName();
			}
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'professionally_managed_year', 'Professionally Managed Year is required' . $strError . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valAutoSprinklerCreditId() {
		$boolIsValid = true;

		if( 'FL' == $this->getStateCode() && CInsuranceCarrier::MARKEL == $this->getInsuranceCarrierId() && true == is_null( $this->getAutoSprinklerCreditId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'auto_sprinkler_credit_id', 'Auto Sprinkler Credit is required' . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valBuildingCodeGradeId() {
		$boolIsValid = true;

		if( 'FL' == $this->getStateCode() && CInsuranceCarrier::MARKEL == $this->getInsuranceCarrierId() && true == is_null( $this->getBuildingCodeGradeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'building_code_grade_id', 'Building Grade Code is required' . '. ' ) );
		}

		return $boolIsValid;
	}

	public function valWindstormConstructionCreditId() {
		$boolIsValid = true;

		if( 'FL' == $this->getStateCode() && CInsuranceCarrier::MARKEL == $this->getInsuranceCarrierId() && true == is_null( $this->getWindstormConstructionCreditId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'windstorm_construction_credit_id', 'Windstorm Construction Credit is required' . '. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objInsurancePortalDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( $objInsurancePortalDatabase, false );
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valLongitude();
				$boolIsValid &= $this->valLatitude();
				$boolIsValid &= $this->valInsuranceCarrierId();
				$boolIsValid &= $this->valInsuranceCarrierClassificationTypeId();
				$boolIsValid &= $this->valPropertyConstructionTypeId();
				$boolIsValid &= $this->valConstructionYear();
				$boolIsValid &= $this->valMinimumBillingFrequencyId();
				$boolIsValid &= $this->valSubDomain( $objInsurancePortalDatabase );
				$boolIsValid &= $this->valNoOfUnits();

				if( true == $this->getIsProfessionallyManaged() ) {
					$boolIsValid &= $this->valProfessionallyManagedYear();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( $objInsurancePortalDatabase, true );
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valLongitude();
				$boolIsValid &= $this->valLatitude();
				$boolIsValid &= $this->valInsuranceCarrierId();
				$boolIsValid &= $this->valInsuranceCarrierClassificationTypeId();
				$boolIsValid &= $this->valPropertyConstructionTypeId();
				$boolIsValid &= $this->valConstructionYear();
				$boolIsValid &= $this->valMinimumBillingFrequencyId();
				$boolIsValid &= $this->valSubDomain( $objInsurancePortalDatabase );
				$boolIsValid &= $this->valNoOfUnits();

				if( true == $this->getIsProfessionallyManaged() ) {
					$boolIsValid &= $this->valProfessionallyManagedYear();
				}
				break;

			case 'bulk_insert_insurance_carrier_properties':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId( $objInsurancePortalDatabase, false );
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valPostalCode();
				$boolIsValid &= $this->valLongitude();
				$boolIsValid &= $this->valLatitude();
				break;

			case 'bulk_insert_carrier':
				$boolIsValid &= $this->valInsuranceCarrierId( true );
				$boolIsValid &= $this->valInsuranceCarrierClassificationTypeId( true );
				break;

			case 'bulk_insert_insurance_carrier_properties_additional_info':
				$boolIsValid &= $this->valSubDomain( $objInsurancePortalDatabase );
				$boolIsValid &= $this->valPropertyConstructionTypeId();
				$boolIsValid &= $this->valConstructionYear();
				$boolIsValid &= $this->valMinimumBillingFrequencyId();
				$boolIsValid &= $this->valNoOfUnits();
				$boolIsValid &= $this->valAutoSprinklerCreditId();
				$boolIsValid &= $this->valBuildingCodeGradeId();
				$boolIsValid &= $this->valWindstormConstructionCreditId();

				if( true == $this->getIsProfessionallyManaged() ) {
					$boolIsValid &= $this->valProfessionallyManagedYear();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function checkForHttp( $strDomain ) {
		$strHttp  = 'http:';
		$strSlash = '/';

		$strDomain = str_replace( $strHttp, '', $strDomain );
		$strDomain = str_replace( $strSlash, '', $strDomain );

		return $strDomain;
	}

	public function checkDomainCharacters( $strDomain ) {

		if( preg_match( '/[^a-zA-Z0-9.-]/', $strDomain ) ) {
			return false;
		}

		return true;
	}

}

?>