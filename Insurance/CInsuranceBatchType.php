<?php

class CInsuranceBatchType extends CBaseInsuranceBatchType {

	const MARKEL_EXPORT 							= 1;
	const CANCELLATION_COMMUNICATION				= 2;
	const INSURANCE_INVOICE 						= 3;
	const FORCED_PLACED_POLICY_EXPORT 				= 4;
	const UPDATED_INFO_COMMUNICATION				= 5;
	const ADDITIONAL_INSURED_COMMUNICATION			= 6;
	const IDTHEFT_POLICY_EXPORT						= 7;
	const MASTER_POLICY_BILLING						= 8;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>