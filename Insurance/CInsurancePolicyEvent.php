<?php

class CInsurancePolicyEvent extends CBaseInsurancePolicyEvent {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEventTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEventTypeId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_type_id', 'Note Type is required.' ) );
        }
        return $boolIsValid;
    }

    public function valEventText() {
        $boolIsValid = true;
        if( true == is_null( $this->getEventText() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'event_text', 'Note is required.' ) );
        }
        return $boolIsValid;
    }

    public function valInsurancePolicyId() {
       $boolIsValid = true;
        if( true == is_null( $this->getInsurancePolicyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'insurance_policy_id', 'Insurance Policy is required.' ) );
        }
        return $boolIsValid;
    }

    public function valIsActive() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valNotifiedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEventTypeId();
				$boolIsValid &= $this->valInsurancePolicyId();
				$boolIsValid &= $this->valEventText();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public static function createInsurancePolicyEvent( $intEventTypeId, $strEventText = NULL, $intInsurancePolicyId = NULL, $strCarrierPolicyNumber = NULL, $strEffectiveDate = NULL, $strCarrierNotifiedOn = NULL, $objInsurancePolicyEvent = NULL ) {

		if( false == valObj( $objInsurancePolicyEvent, 'CInsurancePolicyEvent' ) ) {
			$objInsurancePolicyEvent = new CInsurancePolicyEvent();
		}

		$objInsurancePolicyEvent->setEventTypeId( $intEventTypeId );
		$objInsurancePolicyEvent->setEventText( $strEventText );
		$objInsurancePolicyEvent->setInsurancePolicyId( $intInsurancePolicyId );
		$objInsurancePolicyEvent->setCarrierPolicyNumber( $strCarrierPolicyNumber );
		$objInsurancePolicyEvent->setEffectiveDate( $strEffectiveDate );
		$objInsurancePolicyEvent->setCarrierNotifiedOn( $strCarrierNotifiedOn );

		return $objInsurancePolicyEvent;
	}

}
?>