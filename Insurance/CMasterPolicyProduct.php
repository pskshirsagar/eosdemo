<?php

class CMasterPolicyProduct extends CBaseMasterPolicyProduct {

	protected $m_strStateCode;

	const VALIDATE_MASTER_POLICY_PRODUCT = 'VALIDATE_MASTER_POLICY_PRODUCT';
	const VALIDATE_MASTER_POLICY_ACCOUNT = 'VALIDATE_MASTER_POLICY_ACCOUNT';

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valMasterPolicyClientId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valClientNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getClientNumber() ) || 4 != strlen( ( string ) $this->getClientNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter 4 digits client number. ' ) );
		}

		if( false == ( preg_match( '/^[0-9]*$/', $this->getClientNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter valid client number. ' ) );
		}

		return $boolIsValid;
	}

	public function valMasterPolicyNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getMasterPolicyNumber() ) || 15 < strlen( ( string ) $this->getMasterPolicyNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter max 15 characters master policy number. ' ) );
		}

		return $boolIsValid;
	}

	public function valBaseRate() {
		$boolIsValid = true;

		if( true == is_null( $this->getBaseRate() ) || false == is_numeric( $this->getBaseRate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter valid policy rate. ' ) );
		}

		return $boolIsValid;
	}

	public function valPolicyFee() {
		$boolIsValid = true;

		if( true == is_null( $this->getPolicyFee() ) || false == ( preg_match( '/^[0-9]*$/', $this->getPolicyFee() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please enter valid policy fee. ' ) );
		}

		return $boolIsValid;
	}

	public function valAdminFee() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valBasicTaxPercent() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valStampingTaxPercent() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valOtherTaxPercent() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIssuedDate() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEffectiveDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select effective date.' ) );
		}

		return $boolIsValid;
	}

	public function valMasterPolicyAccount( $objProperty, $objAdminDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objProperty, 'CProperty' ) ) {

			$arrintCids        = [ $objProperty->getCid() ];
			$arrintPropertyIds = [ $objProperty->getId() ];
			$arrintProductIds  = [ CPsProduct::RESIDENT_INSURE, CPsProduct::MASTER_POLICY ];

			$arrobjAccount = ( array ) \Psi\Eos\Admin\CAccounts::createService()->fetchAccountByProductIdsByAccountTypeIdByPropertyIdsByCid( $arrintProductIds, CAccountType::MASTER_POLICY, $arrintPropertyIds, $arrintCids[0], $objAdminDatabase );
			$arrobjAccount = rekeyObjects( 'PropertyId', $arrobjAccount );
			$objAccount    = $arrobjAccount[$objProperty->getId()];

			$arrstrPropertyNames = [];
			if( false == valObj( $objAccount, 'CAccount' ) ) {
				$arrstrPropertyNames[] = $objProperty->getPropertyName();
			}

			if( true == valArr( $arrstrPropertyNames ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Selected properties ' . implode( ',', $arrstrPropertyNames ) . ' doesn\'t have master policy account.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valLiabilityInsuranceCarrierLimitId() {
		$boolIsValid = true;

		if( true == is_null( $this->getLiabilityInsuranceCarrierLimitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Please select liability amount. ' ) );
		}

		return $boolIsValid;
	}

	public function valPersonalInsuranceCarrierLimitId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPersonalInsuranceCarrierLimitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'personal_insurance_carrier_limit_id', ' Please select personal amount. ' ) );
		}

		return $boolIsValid;
	}

	public function valDeductibleInsuranceCarrierLimitId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeductibleInsuranceCarrierLimitId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deductible_insurance_carrier_limit_id', ' Please select deductible amount. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objProperty = NULL, $objAdminDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case self::VALIDATE_MASTER_POLICY_PRODUCT:
				$boolIsValid &= $this->valClientNumber();
				$boolIsValid &= $this->valMasterPolicyNumber();
				$boolIsValid &= $this->valBaseRate();
				$boolIsValid &= $this->valPolicyFee();
				$boolIsValid &= $this->valLiabilityInsuranceCarrierLimitId();
				$boolIsValid &= $this->valPersonalInsuranceCarrierLimitId();
				$boolIsValid &= $this->valDeductibleInsuranceCarrierLimitId();
				$boolIsValid &= $this->valEffectiveDate();
				break;

			case self::VALIDATE_MASTER_POLICY_ACCOUNT:
				$boolIsValid &= $this->valMasterPolicyAccount( $objProperty, $objAdminDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

}

?>