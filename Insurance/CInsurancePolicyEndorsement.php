<?php

class CInsurancePolicyEndorsement extends CBaseInsurancePolicyEndorsement {
	protected $m_intInsuranceEndorsementTypeId;

	public function getInsuranceEndorsementTypeId() {
		return $this->m_intInsuranceEndorsementTypeId;
	}

	public function setInsuranceEndorsementTypeId( $intInsuranceEndorsementTypeId ) {
		$this->m_intInsuranceEndorsementTypeId = $intInsuranceEndorsementTypeId;
	}

	public function  setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['insurance_endorsement_type_id'] ) ) $this->setInsuranceEndorsementTypeId( $arrValues['insurance_endorsement_type_id'] );
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
           		break;

           	default:
           		// default case
           		$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $objClientDatabase = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $objClientDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $objClientDatabase );
		}
	}

}
?>