<?php

class CInsurancePolicyType extends CBaseInsurancePolicyType {

	const HO_4                = 1;
	const FORCED_PLACED_BASIC = 2;
	const BLANKET_BASIC       = 3;
	const BLANKET_ENHANCED    = 4;
	const ID_THEFT            = 5;
	const AFFORDABLE          = 6;
	const CUSTOM              = 7;

	public static $c_arrintInsurancePolicyTypes = array(
		self::HO_4,
		self::FORCED_PLACED_BASIC,
		self::BLANKET_BASIC,
		self::BLANKET_ENHANCED,
		self::ID_THEFT,
		self::AFFORDABLE
	);

	public static $c_arrintMasterPolicyTypes              = [ self::FORCED_PLACED_BASIC, self::BLANKET_BASIC, self::BLANKET_ENHANCED ];
	public static $c_arrintMasterAndAffordablePolicyTypes = [ self::FORCED_PLACED_BASIC, self::BLANKET_BASIC, self::BLANKET_ENHANCED, self::AFFORDABLE ];
	public static $c_arrintBlanketPolicyTypes             = [ self::BLANKET_BASIC, self::BLANKET_ENHANCED ];
	public static $c_arrintNewMasterPolicyTypes           = [ self::FORCED_PLACED_BASIC, self::BLANKET_BASIC ];
	public static $c_arrintLeaseInsuredPolicyTypeIds      = [ self::FORCED_PLACED_BASIC, self::BLANKET_BASIC, self::BLANKET_ENHANCED, self::AFFORDABLE ];

}
?>