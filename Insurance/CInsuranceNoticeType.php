<?php

class CInsuranceNoticeType extends CBaseInsuranceNoticeType {

	const PURCHASE_SCREEN_DISCLAIMERS 	= 1;
	const SUMMERY_SCREEN 				= 2;
	const EMAIL_NOTICES 				= 3;
	const PURCHSAE_SCREEN_NOTICES 		= 4;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }
}
?>