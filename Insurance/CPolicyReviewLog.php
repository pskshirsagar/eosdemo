<?php

class CPolicyReviewLog extends CBasePolicyReviewLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyReviewId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyReviewStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>