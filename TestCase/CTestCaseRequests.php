<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequests
 * Do not add any new functions to this class.
 */

class CTestCaseRequests extends CBaseTestCaseRequests {

	public static function fetchPaginatedTestCaseRequestsCount( $intIsExecuteOnPatialPush, $intIsExecuteOnReleaseDay, $intUserId, $objDatabase ) {

		$strUserClause = '';
		if ( 1 == $intIsExecuteOnReleaseDay && false == is_null( $intUserId ) )
			$strUserClause = ' AND created_by =' . ( int ) $intUserId;

		$strSql = 'SELECT count(id) FROM test_case_requests WHERE is_execute_on_partial_push = ' . ( int ) $intIsExecuteOnPatialPush . ' AND is_published = 1 AND is_execute_on_release_day = ' . ( int ) $intIsExecuteOnReleaseDay . '' . $strUserClause;
		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) )
			return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchTestCaseRequestsForAutoExecution( $intLimit, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.start_date_time IS NULL AND tcr.end_date_time IS NULL AND tcr.is_recurring = 1 AND tcr.is_execute_on_partial_push = 0 ORDER BY id ASC LIMIT ' . ( int ) $intLimit;

		return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchRunningTestcaseRequests( $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.start_date_time IS NOT NULL AND tcr.end_date_time IS NULL AND tcr.is_execute_on_partial_push = 0 AND tcr.is_execute_on_release_day = 0';

		return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchReleaseDayTestCaseRequestsForAutoExecution( $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.start_date_time IS NULL AND tcr.end_date_time IS NULL  AND tcr.is_execute_on_release_day = 1 ORDER BY id ASC ';
				return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchReleaseDayRunningTestcaseRequests( $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.start_date_time IS NOT NULL AND tcr.end_date_time IS NULL AND tcr.is_execute_on_release_day = 1';

		return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchAllTestCaseRequests( $intIsExecuteOnPatialPush, $intIsExecuteOnReleaseDay, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests WHERE is_published=1 AND is_execute_on_partial_push =' . ( int ) $intIsExecuteOnPatialPush . ' AND is_execute_on_release_day =' . ( int ) $intIsExecuteOnReleaseDay;

		return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchTestCaseRequestsByIds( $arrintTestCaseRequestIds, $objDatabase ) {

		if( false == valArr( $arrintTestCaseRequestIds ) ) return NULL;

		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.id IN ( ' . implode( ',', $arrintTestCaseRequestIds ) . ')';
		return self::fetchTestCaseRequests( $strSql, $objDatabase );
	}

	public static function fetchConflictingTestCaseRequestName( $strTestCaseRequestName, $objDatabase ) {

		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.name = \'' . $strTestCaseRequestName . '\' AND is_published =1';

		return self::fetchTestCaseRequest( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTestCaseRequestsWithTotalExcutionTimeAndNumberOfScripts( $intIsExecuteOnPatialPush, $intIsExecuteOnReleaseDay, $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL, $intUserId, $objDatabase ) {

		if( 0 < $intPageNo ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit  = ( int ) $intPageSize;
		}

		$strOrderClause = '';
		$strUserClause = '';

		$strOrder = ( false == is_null( $strOrderByType ) ) ? $strOrderByType : ' DESC ';
		$strOrderField = ( false == is_null( $strOrderByField ) ) ? $strOrderByField : ' updated_on ';
		$strOrderClause = ' ORDER BY ' . $strOrderField . ' ' . $strOrder;

		if ( 1 == $intIsExecuteOnReleaseDay && false == is_null( $intUserId ) )
		$strUserClause = ' AND tcr.created_by =' . ( int ) $intUserId;

		$strSql = 'SELECT
    					tcr.*,
    					tcr.end_date_time - tcr.start_date_time AS total_execution_time,
    					subquery.no_of_scripts
					FROM
    					test_case_requests tcr
    					LEFT JOIN (
      								SELECT
   								 		tcrd.test_case_request_id,
    									count ( tcrd.id ) AS no_of_scripts
									FROM
    									test_case_request_details tcrd
    									JOIN test_cases tc ON ( tc.id = tcrd.test_case_id )
									WHERE
    									tc.is_published = 1
									GROUP BY
    									1
    							) AS subquery ON ( subquery.test_case_request_id = tcr.id ) WHERE tcr.is_execute_on_partial_push = ' . ( int ) $intIsExecuteOnPatialPush . ' AND tcr.is_published = 1 AND tcr.is_execute_on_release_day = ' . ( int ) $intIsExecuteOnReleaseDay . '' . $strUserClause . $strOrderClause;

		if( 0 < $intPageNo ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTestCaseRequestsForSendEmailNotification( $objDatabase ) {

		$strSql = 'SELECT * FROM test_case_requests tcr WHERE tcr.send_email_notification = 1';

		return self::fetchTestCaseRequests( $strSql, $objDatabase );

	}

	public static function fetchTestCaseRequestsByIsExecuteOnPartialPushBySvnRepositoryId( $intIsExecuteOnPartialPush, $intSvnRepositoryId, $objDatabase ) {

		$strSql = 'SELECT * FROM test_case_requests WHERE is_execute_on_partial_push = ' . ( int ) $intIsExecuteOnPartialPush . ' AND start_date_time IS NULL AND end_date_time IS NULL AND svn_repository_id = ' . ( int ) $intSvnRepositoryId . ' ORDER BY id ASC LIMIT 1';

		return self::fetchTestCaseRequest( $strSql, $objDatabase );
	}

	public static function fetchRunningAutoExecuteTestCaseRequestsOnPartialPush( $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests WHERE is_execute_on_partial_push = 1 AND start_date_time IS NOT NULL AND end_date_time IS NULL';

		return self::fetchTestCaseRequests( $strSql, $objDatabase );

	}

	public static function fetchRunningAutoExecuteTestCaseRequestsOnPartialPushBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_requests WHERE is_execute_on_partial_push = 1 AND start_date_time IS NOT NULL AND end_date_time IS NULL AND svn_repository_id = ' . ( int ) $intSvnRepositoryId . 'LIMIT 1';

		return self::fetchTestCaseRequest( $strSql, $objDatabase );

	}

	public static function fetchSearchedTestSuites( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
    					tcr.*,
    					tcr.end_date_time - tcr.start_date_time AS total_execution_time,
    					subquery.no_of_scripts,
						tcrst.name as cluster_name

					FROM
    					test_case_requests tcr
						LEFT JOIN test_case_request_status_types As tcrst ON( tcr.test_case_request_status_type_id = tcrst.id )
    					LEFT JOIN (
      								SELECT
   								 		tcrd.test_case_request_id,
    									count ( tcrd.id ) AS no_of_scripts
									FROM
    									test_case_request_details tcrd
    									JOIN test_cases tc ON ( tc.id = tcrd.test_case_id )
									WHERE
    									tc.is_published = 1
									GROUP BY
    									1
    							) AS subquery ON ( subquery.test_case_request_id = tcr.id ) WHERE tcr.is_execute_on_partial_push = 0 AND tcr.is_published = 1 AND tcr.is_execute_on_release_day = 0
						AND (
							 tcr.name ILIKE \'%' . implode( '%\' AND tcrst.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							 OR tcrst.name ILIKE \'%' . implode( '%\' AND tcr.name  ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							) LIMIT 15';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestCaseRequestsByClusterIdByTestCaseSuiteIdByTestCaseScriptTypeIdByUserIdBySvnRepositoryId( $intClusterId, $intTestCaseSuiteId, $intUserId, $intSvnRepositoryId, $intTestCaseScriptTypeId, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    test_case_requests
					WHERE
					    ( cluster_id = ' . ( int ) $intClusterId . '
					    AND test_case_suite_id = ' . ( int ) $intTestCaseSuiteId . '
					    AND created_by = ' . ( int ) $intUserId . '
					    AND svn_repository_id = ' . ( int ) $intSvnRepositoryId . '
					    AND test_case_script_type_id = ' . ( int ) $intTestCaseScriptTypeId . '
					    AND is_execute_on_release_day = 1
					    AND created_on::TIMESTAMP BETWEEN \'now()\'::TIMESTAMP - INTERVAL \'1 DAY\'
					    AND \'now()\'::TIMESTAMP )';

		return self::fetchTestCaseRequest( $strSql, $objDatabase );

	}

	public static function fetchTestCaseRequestExecutionTimeByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {

		$strSql = 'SELECT
					tcr.end_date_time - tcr.start_date_time AS total_execution_time
					FROM
    					test_case_requests tcr
    				WHERE
    					id = ' . ( int ) $intTestCaseRequestId . '';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTestCaseRequestsByTestCaseSuiteIdByTestCaseScriptTypeIdBySvnRepositoryId( $intTestCaseSuiteId, $intTestCaseScriptTypeId,$intSvnRepositoryId, $objDatabase ) {
		$strSql = 'SELECT
					    *
					FROM
					    test_case_requests
					WHERE
					    (  test_case_suite_id = ' . ( int ) $intTestCaseSuiteId . '
					    AND svn_repository_id = ' . ( int ) $intSvnRepositoryId . '
					    AND test_case_script_type_id = ' . ( int ) $intTestCaseScriptTypeId . '
					    AND is_execute_on_release_day = 1
					    AND created_on::TIMESTAMP BETWEEN \'now()\'::TIMESTAMP - INTERVAL \'1 DAY\'
					    AND \'now()\'::TIMESTAMP )';

		return self::fetchTestCaseRequests( $strSql, $objDatabase );

	}

}
?>