<?php

class CTestCaseRequest extends CBaseTestCaseRequest {

	public static $c_arrstrSPOCEmailAddresses 	= array(
		1	 => 'ppatil@xento.com',			// Automation
		2	 => 'sjadhav03@xento.com',		// ClientAdmin
		3	 => 'ckumar@xento.com',			// DocumentManagement
		4	 => 'pkanbargi@xento.com',		// EntrataAP
		5	 => 'snashine@xento.com',		// EntrataAR
		6	 => 'gmadane@xento.com',		// EntrataCoreMigration
		7	 => 'rfegade@xento.com',		// EntrataPaaS
		8	 => 'gmulik@xento.com',			// EntrataReports
		9	 => 'gdesale@xento.com',		// ILS
		10	 => 'rbansode01@xento.com',		// InspectionManager
		11	 => 'pdeshpande@xento.com',		// Integrations
		12	 => 'msayyed@xento.com',		// LeadManagement
		13	 => 'mmundhe@xento.com',		// LeaseExecution
		14	 => 'ssant@xento.com',			// LeaseRenewal
		15	 => 'rzende@xento.com',			// LeasingCenter
		16	 => 'sbhosale02@xento.com',		// MakeReady
		17	 => 'saher@xento.com',			// MessageCenter
		18	 => 'svaidya@xento.com',		// OnlineApplicationEntrata
		19	 => 'hkalmegh@xento.com',		// OnlineApplicationPP
		20	 => 'ashivapurkar@xento.com',	// Operations
		21	 => 'sdeshpande@xento.com',		// PricingPortal
		22	 => 'ngawade@xento.com',		// PropertyManagement
		23	 => 'bshaik@xento.com',			// ProspectPortal
		24	 => 'rjain01@xento.com',		// ResidentInsurance
		25	 => 'spandharpatte@xento.com',	// ResidentPortal
		26	 => 'rmaral@xento.com',			// RPay
		27	 => 'abhandari@xento.com',		// Screening
		28	 => 'sgedam@xento.com',			// SiteTablet
		29	 => 'nsugre@xento.com',			// StudentHousing
		30	 => '',							// SystemPortal (unpublished)
		31	 => 'aonkar@xento.com',			// UserExperience
		32	 => 'sghare@xento.com',			// UtilityBilling
		33	 => 'anegi@xento.com',			// VendorAccess
		34	 => 'msingh01@xento.com',		// WorkOrders
		35	 => 'rgawatre@xento.com',		// APIServices
		36	 => '',							// Craiglist(unpublished)
		37	 => '',							// ReputationAdvisor(unpublished)
		38	 => '',							// Craiglist(unpublished)
		39	 => '',							// EntrataDashboardLoadTimeScripts
		40	 => 'hmahajan01@xento.com',		// Migration
		41	 => '',							// MegaAPScripts
		42	 => '',							// TrunkScripts
		43	 => 'tsavadekar@xento.com',		// EntrataReportsAP
		44	 => 'ktarle@xento.com',			// EntrataReportsAR
		45	 => 'amulla@xento.com',			// EntrataReportsPaaS
		46	 => '',							// Entrata Affordable(unpublished)
		47	 => 'lwaghmare@xento.com',		// EntrataAffordable
		48	 => ''							// GreyStar
	);

	public static $c_arrintPsProductIds 	= array(
		1	 => 9447,	// Automation
		2	 => 5,		// ClientAdmin
		3	 => 3395,	// DocumentManagement
		4	 => 1,		// EntrataAP
		5	 => 1,		// EntrataAR
		6	 => 25,		// EntrataCoreMigration
		7	 => 58,		// EntrataPaaS
		8	 => 1,		// EntrataReports
		9	 => 18,		// ILS
		10	 => 56,		// InspectionManager
		11	 => 14,		// Integrations
		12	 => 43,		// LeadManagement
		13	 => 27,		// LeaseExecution
		14	 => 27,		// LeaseRenewal
		15	 => 46,		// LeasingCenter
		16	 => 82,		// MakeReady
		17	 => 28,	    // MessageCenter
		18	 => 27,		// OnlineApplicationEntrata
		19	 => 27,		// OnlineApplicationPP
		20	 => 5,	    // Operations
		21	 => 42,	    // PricingPortal
		22	 => 46,		// PropertyManagement
		23	 => 2,		// ProspectPortal
		24	 => 39,		// ResidentInsurance
		25	 => 3,	    // ResidentPortal
		26	 => 4,		// RPay
		27	 => 53,		// Screening
		28	 => 34,		// SiteTablet
		29	 => 1,		// StudentHousing
		30	 => '',		// SystemPortal (unpublished)
		31	 => '',		// UserExperience
		32	 => 4078,	// UtilityBilling
		33	 => 65,		// VendorAccess
		34	 => 82,		// WorkOrders
		35	 => 7,		// APIServices
		36	 => '',		// Craiglist(unpublished)
		37	 => '',		// ReputationAdvisor(unpublished)
		38	 => '',		// Craiglist(unpublished)
		39	 => '',		// EntrataDashboardLoadTimeScripts
		40	 => 25,		// Migration
		41	 => '',		// MegaAPScripts
		42	 => '',		// TrunkScripts
		43	 => 1,		// EntrataReportsAP
		44	 => 1,		// EntrataReportsAR
		45	 => 1,		// EntrataReportsPaaS
		46	 => '',		// Entrata Affordable(unpublished)
		47	 => 62,		// EntrataAffordable
		48	 => ''		// GreyStar
	);

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTestCaseSuiteId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        $objConflictingTestCaseRequest = CTestCaseRequests::fetchConflictingTestCaseRequestName( $this->getName(), $objDatabase );

        if( true == valObj( $objConflictingTestCaseRequest, 'CTestCaseRequest' ) ) {
			if( false == is_null( $this->getId() ) && $this->getId() != $objConflictingTestCaseRequest->getId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Suite name already exists.' ) );
				$boolIsValid = false;

	        } elseif( true == is_null( $this->getId() ) ) {
		       	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Suite name already exists.' ) );
		       	$boolIsValid = false;
	       	}
        }

        if( preg_match( '/[\'^£$%&*:;.()}{@#~?><,|=_+¬-]/', $this->getName() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special characters are not allowed in suite name.' ) );
		       	$boolIsValid = false;
        }

        if( 30 < strlen( $this->getName() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Suite name should be below 30 characters.' ) );
        	$boolIsValid = false;
        }

        if( is_numeric( $this->getName() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Only numbers are not allowed in suite name.' ) );
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function valEmailSendStatus( $objDatabase ) {
    	$boolIsValid = true;

    	$objTestCaseRequest = CTestCaseRequests::fetchTestCaseRequestById( $this->getId(), $objDatabase );

    	if( true == valObj( $objTestCaseRequest, 'CTestCaseRequest' ) ) {
    		if( 1 == $objTestCaseRequest->getSendEmailNotification() ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please wait previous execution in progress. ' ) );
    			$boolIsValid = false;
    		}
    	}
    	return $boolIsValid;
    }

    public function valSvnRepositoryId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPort() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valStartDateTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEndDateTime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailNotifications() {
        $boolIsValid = true;

        require_once( PATH_LIBRARIES_PSI . 'CValidation.class.php' );

        $arrstrEmailAddress = explode( ',', $this->getEmailNotifications() );

        $boolIsValid = true;

        foreach( $arrstrEmailAddress as $strEmailAddress ) {

        	if( 0 < strlen( trim( $strEmailAddress ) ) && false == CValidation::validateEmailAddresses( $strEmailAddress ) ) {
        		$arrstrErrorMsg[] = $strEmailAddress;
        		continue;
        	}
        }

        if( true == isset( $arrstrErrorMsg ) && true == valArr( $arrstrErrorMsg ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', implode( ', ', $arrstrErrorMsg ) . ' Email address is not valid.' ) );
        }

        return $boolIsValid;
    }

	public function valInsertStatus( $objDatabase ) {

		$boolIsValid    = true;

		$strSql         = ' SELECT
						 			 count(*)
								FROM
								    test_case_requests
								WHERE
								    created_by = ' . ( int ) $this->getRequestedBy() . '
								    AND ( start_date_time IS NULL OR end_date_time IS NULL )
								    AND is_execute_on_release_day = 1
								    AND ( created_on::TIMESTAMP BETWEEN \'now()\'::TIMESTAMP - INTERVAL \'1 DAY\' AND \'now()\'::TIMESTAMP )';

		$arrintTestCaseCount = fetchData( $strSql, $objDatabase );

		if( 0 < $arrintTestCaseCount[0][count] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Previous Execution is in Queue OR in Progress. Please Wait for Completion.' ) );
			$boolIsValid = false;
			return $boolIsValid;
		}

		$objTestCaseRequest = CTestCaseRequests::fetchTestCaseRequestsByClusterIdByTestCaseSuiteIdByTestCaseScriptTypeIdByUserIdBySvnRepositoryId( $this->getClusterId(), $this->getTestCaseSuiteId(), $this->getRequestedBy(), $this->getSvnRepositoryId(), $this->getTestCaseScriptTypeId(), $objDatabase );
		if( true == valObj( $objTestCaseRequest, 'CTestCaseRequest' ) ) {
			    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'You Have already added request for this environment. ' ) );
			    $boolIsValid = false;
	    }
	    return $boolIsValid;
	}

    public function valRequestedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsRecurring() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valEmailSendStatus( $objDatabase );

            	if( false == is_null( $this->getEmailNotifications() ) ) {
            		$boolIsValid &= $this->valEmailNotifications();
            	}
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            case 'VALIDATE_INSERT_RELEASE':
             	$boolIsValid &= $this->valInsertStatus( $objDatabase );
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>