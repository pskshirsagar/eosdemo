<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CTestCaseRequestStatusTypes extends CBaseTestCaseRequestStatusTypes {

    public static function fetchTestCaseRequestStatusTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CTestCaseRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchTestCaseRequestStatusType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CTestCaseRequestStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllTestCaseRequestStatusTypes( $objDatabase ) {

    	$strSql = 'SELECT * FROM test_case_request_status_types';

    	return self::fetchTestCaseRequestStatusTypes( $strSql, $objDatabase );
    }

}
?>