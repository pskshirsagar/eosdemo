<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseSuites
 * Do not add any new functions to this class.
 */

class CTestCaseSuites extends CBaseTestCaseSuites {

	public static function fetchPublishedTestCaseSuites( $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_suites WHERE is_published = 1 ORDER BY name ASC';
		return self::fetchTestCaseSuites( $strSql, $objDatabase );
	}

	public static function fetchUnPublishedTestCaseSuites( $objDatabase ) {
		return parent::fetchTestCaseSuites( 'SELECT * FROM test_case_suites WHERE is_published = 0 ORDER BY name', $objDatabase );
	}

	public static function fetchPublishedTestCaseSuitesBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_suites WHERE svn_repository_id = ' . ( int ) $intSvnRepositoryId . ' AND is_published = 1';
		return self::fetchTestCaseSuites( $strSql, $objDatabase );

	}

	public static function fetchPublishedTestCaseSuitesByIds( $arrintTestCaseSuiteIds, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_suites WHERE id IN ( ' . implode( ',', $arrintTestCaseSuiteIds ) . ' )';
		return self::fetchTestCaseSuites( $strSql, $objDatabase );

	}

}
?>