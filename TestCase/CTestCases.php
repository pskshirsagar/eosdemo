<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCases
 * Do not add any new functions to this class.
 */

class CTestCases extends CBaseTestCases {

	public static function fetchAllTestCases( $objDatabase ) {
		$strSql = 'SELECT * FROM test_cases';
		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByIds( $arrintTestCaseIds, $objDatabase ) {

		if( false == valArr( $arrintTestCaseIds ) ) return NULL;
		$strSql = 'SELECT * FROM test_cases WHERE id IN ( ' . implode( ',', $arrintTestCaseIds ) . ' )  AND is_published = 1';
		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByTestCaseSuiteId( $intTestCaseSuiteId, $objDatabase ) {

		$strSql = 'SELECT * FROM test_cases WHERE test_case_suite_id = ' . ( int ) $intTestCaseSuiteId . ' ORDER BY name ASC';
		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByNameByTestCaseSuiteId( $strTestCaseName, $intTestSuiteId, $objDatabase ) {

		$strSql = 'SELECT
						tc.*
					FROM
    					test_cases tc
    					JOIN test_case_request_details tcrd ON ( tcrd.test_case_id = tc.id )
    					JOIN test_case_requests tcr ON ( tcr.id = tcrd.test_case_request_id )
					WHERE
						tc.name ILIKE  \'%' . $strTestCaseName . '%\'
						AND tcr.id = ' . ( int ) $intTestSuiteId;

		return self::fetchTestCases( $strSql, $objDatabase );

	}

	public static function fetchTestCaseSuitesByTestCaseIds( $arrintTestCaseIds, $objDatabase ) {
		if( false == valArr( $arrintTestCaseIds ) ) return NULL;
		$strSql = 'SELECT * FROM test_cases WHERE id IN ( ' . implode( ',', $arrintTestCaseIds ) . ' ) AND is_published = 1 ORDER BY name ASC';

		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByRepositoryIdByTestSuiteId( $intRepositoryId, $strTestSuiteId, $objDatabase ) {
		$strSql = '	SELECT tc.*
					FROM
						 test_cases tc
					     JOIN test_case_suites tcs ON (tc.test_case_suite_id = tcs.id)
					WHERE
						tcs.id = ' . ( int ) $strTestSuiteName . '
					    AND tcs.svn_repository_id = ' . ( int ) $intRepositoryId . ' LIMIT 2';

		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchAllTestCasesByIds( $arrintTestCaseIds, $objDatabase ) {

		if( false == valArr( $arrintTestCaseIds ) ) return NULL;
		$strSql = 'SELECT * FROM test_cases WHERE id IN ( ' . implode( ',', $arrintTestCaseIds ) . ' )';
		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByTestCaseSuiteIdsByTestCaseScriptTypeIdSvnRepositoryIdByIsPublished( $arrintTestCaseSuiteIds, $intTestCaseScriptTypeId, $intSvnRepositoryId, $intIsPublished, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						test_cases
					WHERE
						test_case_suite_id IN ( ' . implode( ',', $arrintTestCaseSuiteIds ) .
						' ) AND test_case_script_type_id = ' . ( int ) $intTestCaseScriptTypeId .
						' AND svn_repository_id = ' . ( int ) $intSvnRepositoryId .
						' AND is_published = ' . ( int ) $intIsPublished . ' ORDER BY name ASC ';

		return self::fetchTestCases( $strSql, $objDatabase );
	}

	public static function fetchTestCasesByTestCaseSuiteIdsBySvnRepositoryId( $arrintTestCaseSuiteIds, $intSvnRepositoryId, $objDatabase ) {

		if( false == valArr( $arrintTestCaseSuiteIds ) ) return;
		$strSql = 'SELECT
						*
					FROM
						test_cases
					WHERE
						test_case_suite_id IN ( ' . implode( ',', $arrintTestCaseSuiteIds ) .
						' ) AND svn_repository_id = ' . ( int ) $intSvnRepositoryId .
						' AND is_published = 1';

		return self::fetchTestCases( $strSql, $objDatabase );

	}

	public static function fetchTestCasesByTestSuiteIdByTestScriptTypeIdByRepositoryId( $intTestSuiteId, $intTestCaseScriptTypeId, $intRepositoryId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						test_cases
					WHERE
						test_case_suite_id = ' . ( int ) $intTestSuiteId . '
					AND
						test_case_script_type_id = ' . ( int ) $intTestCaseScriptTypeId . '
					AND
						svn_repository_id = ' . ( int ) $intRepositoryId . '
					AND is_published=1 ';

		return self::fetchTestCases( $strSql, $objDatabase );

	}

}
?>