<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseScriptTypes
 * Do not add any new functions to this class.
 */

class CTestCaseScriptTypes extends CBaseTestCaseScriptTypes {

    public static function fetchTestCaseScriptTypes( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CTestCaseScriptType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchTestCaseScriptType( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CTestCaseScriptType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchPublishedTestCaseScriptTypes( $objDatabase ) {

    	$strSql = 'SELECT * FROM test_case_script_types WHERE is_published = 1';

    	return self::fetchTestCaseScriptTypes( $strSql, $objDatabase );
    }
}
?>