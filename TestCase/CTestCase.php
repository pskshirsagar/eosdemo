<?php

class CTestCase extends CBaseTestCase {

	public static function getRepositoryIdFromName( $boolIsName = true ) {
		$arrstrRepositoryNames['RapidStage'] 				= ( false !== $boolIsName ) ? CSvnRepository::PS_CORE_RAPIDSTAGE : CSvnRepository::PS_CORE_RAPIDSTAGE;
		$arrstrRepositoryNames['RapidProduction'] 			= ( false !== $boolIsName ) ? CSvnRepository::PS_CORE_RAPIDPRODUCTION : CSvnRepository::PS_CORE_RAPIDPRODUCTION;
		$arrstrRepositoryNames['StandardStage'] 			= ( false !== $boolIsName ) ? CSvnRepository::PS_CORE_STANDARDSTAGE : CSvnRepository::PS_CORE_STANDARDSTAGE;
		$arrstrRepositoryNames['StandardProduction'] 		= ( false !== $boolIsName ) ? CSvnRepository::PS_CORE_STANDARDPRODUCTION : CSvnRepository::PS_CORE_STANDARDPRODUCTION;

		return $arrstrRepositoryNames;
	}

	public static function getTestCaseScriptTypeIdFromName( $boolIsName = true ) {
		$arrstrTestCaseScriptTypeNames['Regression'] 		= ( false !== $boolIsName ) ? 1 : 1;
		$arrstrTestCaseScriptTypeNames['Smoke'] 			= ( false !== $boolIsName ) ? 2 : 2;

		return $arrstrTestCaseScriptTypeNames;
	}

	public static function getTestCaseSuiteIdFromName( $boolIsName = true ) {
		$arrstrTestCaseSuiteNames['Automation'] 				= ( false !== $boolIsName ) ? 1 : 1;
		$arrstrTestCaseSuiteNames['ClientAdmin'] 				= ( false !== $boolIsName ) ? 2 : 2;
		$arrstrTestCaseSuiteNames['DocumentManagement'] 		= ( false !== $boolIsName ) ? 3 : 3;
		$arrstrTestCaseSuiteNames['EntrataAP'] 					= ( false !== $boolIsName ) ? 4 : 4;
		$arrstrTestCaseSuiteNames['EntrataAR'] 					= ( false !== $boolIsName ) ? 5 : 5;
		$arrstrTestCaseSuiteNames['EntrataCoreMigration'] 		= ( false !== $boolIsName ) ? 6 : 6;
		$arrstrTestCaseSuiteNames['EntrataPaaS'] 				= ( false !== $boolIsName ) ? 7 : 7;
		$arrstrTestCaseSuiteNames['EntrataReports'] 			= ( false !== $boolIsName ) ? 8 : 8;
		$arrstrTestCaseSuiteNames['ILS'] 						= ( false !== $boolIsName ) ? 9 : 9;
		$arrstrTestCaseSuiteNames['InspectionManager'] 			= ( false !== $boolIsName ) ? 10 : 10;
		$arrstrTestCaseSuiteNames['Integrations'] 				= ( false !== $boolIsName ) ? 11 : 11;
		$arrstrTestCaseSuiteNames['LeadManagement'] 			= ( false !== $boolIsName ) ? 12 : 12;
		$arrstrTestCaseSuiteNames['LeaseExecution'] 			= ( false !== $boolIsName ) ? 13 : 13;
		$arrstrTestCaseSuiteNames['LeaseRenewal'] 				= ( false !== $boolIsName ) ? 14 : 14;
		$arrstrTestCaseSuiteNames['LeasingCenter'] 				= ( false !== $boolIsName ) ? 15 : 15;
		$arrstrTestCaseSuiteNames['MakeReady'] 					= ( false !== $boolIsName ) ? 16 : 16;
		$arrstrTestCaseSuiteNames['MessageCenter'] 				= ( false !== $boolIsName ) ? 17 : 17;
		$arrstrTestCaseSuiteNames['OnlineApplicationEntrata']	= ( false !== $boolIsName ) ? 18 : 18;
		$arrstrTestCaseSuiteNames['OnlineApplicationPP'] 		= ( false !== $boolIsName ) ? 19 : 19;
		$arrstrTestCaseSuiteNames['Operations'] 				= ( false !== $boolIsName ) ? 20 : 20;
		$arrstrTestCaseSuiteNames['PricingPortal'] 				= ( false !== $boolIsName ) ? 21 : 21;
		$arrstrTestCaseSuiteNames['PropertyManagement'] 		= ( false !== $boolIsName ) ? 22 : 22;
		$arrstrTestCaseSuiteNames['ProspectPortal'] 			= ( false !== $boolIsName ) ? 23 : 23;
		$arrstrTestCaseSuiteNames['ResidentInsurance'] 			= ( false !== $boolIsName ) ? 24 : 24;
		$arrstrTestCaseSuiteNames['ResidentPortal'] 			= ( false !== $boolIsName ) ? 25 : 25;
		$arrstrTestCaseSuiteNames['RPay'] 						= ( false !== $boolIsName ) ? 26 : 26;
		$arrstrTestCaseSuiteNames['Screening'] 					= ( false !== $boolIsName ) ? 27 : 27;
		$arrstrTestCaseSuiteNames['SiteTablet'] 				= ( false !== $boolIsName ) ? 28 : 28;
		$arrstrTestCaseSuiteNames['StudentHousing'] 			= ( false !== $boolIsName ) ? 29 : 29;
		$arrstrTestCaseSuiteNames['UserExperience'] 			= ( false !== $boolIsName ) ? 31 : 31;
		$arrstrTestCaseSuiteNames['UtilityBilling'] 			= ( false !== $boolIsName ) ? 32 : 32;
		$arrstrTestCaseSuiteNames['VendorAccess'] 				= ( false !== $boolIsName ) ? 33 : 33;
		$arrstrTestCaseSuiteNames['WorkOrders'] 				= ( false !== $boolIsName ) ? 34 : 34;
		$arrstrTestCaseSuiteNames['APIServices'] 				= ( false !== $boolIsName ) ? 35 : 35;
		$arrstrTestCaseSuiteNames['Migration'] 					= ( false !== $boolIsName ) ? 40 : 40;
		$arrstrTestCaseSuiteNames['EntrataReportsAP'] 			= ( false !== $boolIsName ) ? 43 : 43;
		$arrstrTestCaseSuiteNames['EntrataReportsAR'] 			= ( false !== $boolIsName ) ? 44 : 44;
		$arrstrTestCaseSuiteNames['EntrataReportsPaaS'] 		= ( false !== $boolIsName ) ? 45 : 45;
		$arrstrTestCaseSuiteNames['EntrataAffordable'] 			= ( false !== $boolIsName ) ? 47 : 47;

		return $arrstrTestCaseSuiteNames;
	}

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTestCaseSuiteId() {
        $boolIsValid = true;

        if( true == is_null( $this->getTestCaseSuiteId() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_case_suite_id', 'Test case suite type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
    	$boolIsValid = true;

    	if( true == is_null( $this->getName() ) ) {
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
    		$boolIsValid = false;
    		return $boolIsValid;
    	} else {
    		$intId = ( true == is_null( $this->getId() ) )? 0 : $this->getId();
    		$intCount = CTestCases::fetchTestCaseCount( 'WHERE id <> ' . ( int ) $intId . 'AND LOWER( name ) = trim( \'' . strtolower( $this->getName() ) . '\')', $objDatabase );
    		if( 0 < $intCount ) {
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already exist.' ) );
    			$boolIsValid = false;
    			return $boolIsValid;
    		}
    	}

    	if( false == preg_match( '/^C[a-zA-Z0-9]{0,255}(Test.class.php)$/', $this->getName() ) || true == is_numeric( $this->getName() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is invalid.' ) );
    		return $boolIsValid;
    	}

    	return $boolIsValid;

    }

    public function valTestCasePath() {
        $boolIsValid = true;

        if( true == is_null( $this->getTestCasePath() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_case_path', 'Test case path is required.' ) );
        }
        return $boolIsValid;
    }

    public function valDescription() {
    	$boolIsValid = true;
    	$strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getDescription() ) );

    	$strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

    	if( 0 >= strlen( $strDescription ) ) {
    		$this->addErrorMsg( new CErrorMsg( NULL, 'description', 'Description is required.' ) );
    		$boolIsValid = false;
    	}
    	return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valTestCaseSuiteId();
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valTestCasePath();
            	$boolIsValid &= $this->valDescription();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>