<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequests
 * Do not add any new functions to this class.
 */

class CBaseTestCaseRequests extends CEosPluralBase {

	/**
	 * @return CTestCaseRequest[]
	 */
	public static function fetchTestCaseRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseRequest', $objDatabase );
	}

	/**
	 * @return CTestCaseRequest
	 */
	public static function fetchTestCaseRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseRequest', $objDatabase );
	}

	public static function fetchTestCaseRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_requests', $objDatabase );
	}

	public static function fetchTestCaseRequestById( $intId, $objDatabase ) {
		return self::fetchTestCaseRequest( sprintf( 'SELECT * FROM test_case_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsByTestCaseSuiteId( $intTestCaseSuiteId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE test_case_suite_id = %d', ( int ) $intTestCaseSuiteId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsByTestCaseRequestStatusTypeId( $intTestCaseRequestStatusTypeId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE test_case_request_status_type_id = %d', ( int ) $intTestCaseRequestStatusTypeId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE svn_repository_id = %d', ( int ) $intSvnRepositoryId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsByTestCaseScriptTypeId( $intTestCaseScriptTypeId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE test_case_script_type_id = %d', ( int ) $intTestCaseScriptTypeId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

	public static function fetchTestCaseRequestsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchTestCaseRequests( sprintf( 'SELECT * FROM test_case_requests WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

}
?>