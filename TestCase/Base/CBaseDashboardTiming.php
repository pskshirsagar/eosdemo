<?php

class CBaseDashboardTiming extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intSvnRepositoryId;
    protected $m_strErrors;
    protected $m_fltLoadTime;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['svn_repository_id'] ) && $boolDirectSet ) $this->m_intSvnRepositoryId = trim( $arrValues['svn_repository_id'] ); else if( isset( $arrValues['svn_repository_id'] ) ) $this->setSvnRepositoryId( $arrValues['svn_repository_id'] );
        if( isset( $arrValues['errors'] ) && $boolDirectSet ) $this->m_strErrors = trim( stripcslashes( $arrValues['errors'] ) ); else if( isset( $arrValues['errors'] ) ) $this->setErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['errors'] ) : $arrValues['errors'] );
        if( isset( $arrValues['load_time'] ) && $boolDirectSet ) $this->m_fltLoadTime = trim( $arrValues['load_time'] ); else if( isset( $arrValues['load_time'] ) ) $this->setLoadTime( $arrValues['load_time'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setSvnRepositoryId( $intSvnRepositoryId ) {
        $this->m_intSvnRepositoryId = CStrings::strToIntDef( $intSvnRepositoryId, NULL, false );
    }

    public function getSvnRepositoryId() {
        return $this->m_intSvnRepositoryId;
    }

    public function sqlSvnRepositoryId() {
        return ( true == isset( $this->m_intSvnRepositoryId ) ) ? (string) $this->m_intSvnRepositoryId : 'NULL';
    }

    public function setErrors( $strErrors ) {
        $this->m_strErrors = CStrings::strTrimDef( $strErrors, -1, NULL, true );
    }

    public function getErrors() {
        return $this->m_strErrors;
    }

    public function sqlErrors() {
        return ( true == isset( $this->m_strErrors ) ) ? '\'' . addslashes( $this->m_strErrors ) . '\'' : 'NULL';
    }

    public function setLoadTime( $fltLoadTime ) {
        $this->m_fltLoadTime = CStrings::strToFloatDef( $fltLoadTime, NULL, false, 0 );
    }

    public function getLoadTime() {
        return $this->m_fltLoadTime;
    }

    public function sqlLoadTime() {
        return ( true == isset( $this->m_fltLoadTime ) ) ? (string) $this->m_fltLoadTime : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.dashboard_timings_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.dashboard_timings					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlSvnRepositoryId() . ', ' . 		                $this->sqlErrors() . ', ' . 		                $this->sqlLoadTime() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.dashboard_timings
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSvnRepositoryId() ) != $this->getOriginalValueByFieldName ( 'svn_repository_id' ) ) { $arrstrOriginalValueChanges['svn_repository_id'] = $this->sqlSvnRepositoryId(); $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' errors = ' . $this->sqlErrors() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlErrors() ) != $this->getOriginalValueByFieldName ( 'errors' ) ) { $arrstrOriginalValueChanges['errors'] = $this->sqlErrors(); $strSql .= ' errors = ' . $this->sqlErrors() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_time = ' . $this->sqlLoadTime() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlLoadTime() ), $this->getOriginalValueByFieldName ( 'load_time' ), 0 ) ) ) { $arrstrOriginalValueChanges['load_time'] = $this->sqlLoadTime(); $strSql .= ' load_time = ' . $this->sqlLoadTime() . ','; $boolUpdate = true; }

        $strSql = substr( $strSql, 0, -1 );

        $strSql .= ' WHERE						id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.dashboard_timings WHERE id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.dashboard_timings_id_seq', $objDatabase );
    }

}
?>