<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CDashboardTimings
 * Do not add any new functions to this class.
 */

class CBaseDashboardTimings extends CEosPluralBase {

	/**
	 * @return CDashboardTiming[]
	 */
	public static function fetchDashboardTimings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDashboardTiming', $objDatabase );
	}

	/**
	 * @return CDashboardTiming
	 */
	public static function fetchDashboardTiming( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDashboardTiming', $objDatabase );
	}

	public static function fetchDashboardTimingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dashboard_timings', $objDatabase );
	}

	public static function fetchDashboardTimingById( $intId, $objDatabase ) {
		return self::fetchDashboardTiming( sprintf( 'SELECT * FROM dashboard_timings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDashboardTimingsByCid( $intCid, $objDatabase ) {
		return self::fetchDashboardTimings( sprintf( 'SELECT * FROM dashboard_timings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDashboardTimingsBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		return self::fetchDashboardTimings( sprintf( 'SELECT * FROM dashboard_timings WHERE svn_repository_id = %d', ( int ) $intSvnRepositoryId ), $objDatabase );
	}

}
?>