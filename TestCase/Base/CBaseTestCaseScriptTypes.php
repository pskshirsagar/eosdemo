<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseScriptTypes
 * Do not add any new functions to this class.
 */

class CBaseTestCaseScriptTypes extends CEosPluralBase {

	/**
	 * @return CTestCaseScriptType[]
	 */
	public static function fetchTestCaseScriptTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseScriptType', $objDatabase );
	}

	/**
	 * @return CTestCaseScriptType
	 */
	public static function fetchTestCaseScriptType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseScriptType', $objDatabase );
	}

	public static function fetchTestCaseScriptTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_script_types', $objDatabase );
	}

	public static function fetchTestCaseScriptTypeById( $intId, $objDatabase ) {
		return self::fetchTestCaseScriptType( sprintf( 'SELECT * FROM test_case_script_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>