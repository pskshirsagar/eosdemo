<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseTestCaseRequestStatusTypes extends CEosPluralBase {

	/**
	 * @return CTestCaseRequestStatusType[]
	 */
	public static function fetchTestCaseRequestStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseRequestStatusType', $objDatabase );
	}

	/**
	 * @return CTestCaseRequestStatusType
	 */
	public static function fetchTestCaseRequestStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseRequestStatusType', $objDatabase );
	}

	public static function fetchTestCaseRequestStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_request_status_types', $objDatabase );
	}

	public static function fetchTestCaseRequestStatusTypeById( $intId, $objDatabase ) {
		return self::fetchTestCaseRequestStatusType( sprintf( 'SELECT * FROM test_case_request_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>