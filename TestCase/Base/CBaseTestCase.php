<?php

class CBaseTestCase extends CEosSingularBase {

    protected $m_intId;
    protected $m_intTestCaseSuiteId;
    protected $m_intTestCaseScriptTypeId;
    protected $m_intSvnRepositoryId;
    protected $m_strName;
    protected $m_strTestCasePath;
    protected $m_strDescription;
    protected $m_intIsPublished;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intIsPublished = '1';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['test_case_suite_id'] ) && $boolDirectSet ) $this->m_intTestCaseSuiteId = trim( $arrValues['test_case_suite_id'] ); else if( isset( $arrValues['test_case_suite_id'] ) ) $this->setTestCaseSuiteId( $arrValues['test_case_suite_id'] );
        if( isset( $arrValues['test_case_script_type_id'] ) && $boolDirectSet ) $this->m_intTestCaseScriptTypeId = trim( $arrValues['test_case_script_type_id'] ); else if( isset( $arrValues['test_case_script_type_id'] ) ) $this->setTestCaseScriptTypeId( $arrValues['test_case_script_type_id'] );
        if( isset( $arrValues['svn_repository_id'] ) && $boolDirectSet ) $this->m_intSvnRepositoryId = trim( $arrValues['svn_repository_id'] ); else if( isset( $arrValues['svn_repository_id'] ) ) $this->setSvnRepositoryId( $arrValues['svn_repository_id'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['test_case_path'] ) && $boolDirectSet ) $this->m_strTestCasePath = trim( stripcslashes( $arrValues['test_case_path'] ) ); else if( isset( $arrValues['test_case_path'] ) ) $this->setTestCasePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['test_case_path'] ) : $arrValues['test_case_path'] );
        if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->m_strDescription = trim( stripcslashes( $arrValues['description'] ) ); else if( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setTestCaseSuiteId( $intTestCaseSuiteId ) {
        $this->m_intTestCaseSuiteId = CStrings::strToIntDef( $intTestCaseSuiteId, NULL, false );
    }

    public function getTestCaseSuiteId() {
        return $this->m_intTestCaseSuiteId;
    }

    public function sqlTestCaseSuiteId() {
        return ( true == isset( $this->m_intTestCaseSuiteId ) ) ? (string) $this->m_intTestCaseSuiteId : 'NULL';
    }

    public function setTestCaseScriptTypeId( $intTestCaseScriptTypeId ) {
        $this->m_intTestCaseScriptTypeId = CStrings::strToIntDef( $intTestCaseScriptTypeId, NULL, false );
    }

    public function getTestCaseScriptTypeId() {
        return $this->m_intTestCaseScriptTypeId;
    }

    public function sqlTestCaseScriptTypeId() {
        return ( true == isset( $this->m_intTestCaseScriptTypeId ) ) ? (string) $this->m_intTestCaseScriptTypeId : 'NULL';
    }

    public function setSvnRepositoryId( $intSvnRepositoryId ) {
        $this->m_intSvnRepositoryId = CStrings::strToIntDef( $intSvnRepositoryId, NULL, false );
    }

    public function getSvnRepositoryId() {
        return $this->m_intSvnRepositoryId;
    }

    public function sqlSvnRepositoryId() {
        return ( true == isset( $this->m_intSvnRepositoryId ) ) ? (string) $this->m_intSvnRepositoryId : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 255, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setTestCasePath( $strTestCasePath ) {
        $this->m_strTestCasePath = CStrings::strTrimDef( $strTestCasePath, -1, NULL, true );
    }

    public function getTestCasePath() {
        return $this->m_strTestCasePath;
    }

    public function sqlTestCasePath() {
        return ( true == isset( $this->m_strTestCasePath ) ) ? '\'' . addslashes( $this->m_strTestCasePath ) . '\'' : 'NULL';
    }

    public function setDescription( $strDescription ) {
        $this->m_strDescription = CStrings::strTrimDef( $strDescription, -1, NULL, true );
    }

    public function getDescription() {
        return $this->m_strDescription;
    }

    public function sqlDescription() {
        return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
    }

    public function setIsPublished( $intIsPublished ) {
        $this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
    }

    public function getIsPublished() {
        return $this->m_intIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_intIsPublished ) ) ? (string) $this->m_intIsPublished : '1';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.test_cases_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.test_cases					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlTestCaseSuiteId() . ', ' . 		                $this->sqlTestCaseScriptTypeId() . ', ' . 		                $this->sqlSvnRepositoryId() . ', ' . 		                $this->sqlName() . ', ' . 		                $this->sqlTestCasePath() . ', ' . 		                $this->sqlDescription() . ', ' . 		                $this->sqlIsPublished() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.test_cases
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_suite_id = ' . $this->sqlTestCaseSuiteId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseSuiteId() ) != $this->getOriginalValueByFieldName ( 'test_case_suite_id' ) ) { $arrstrOriginalValueChanges['test_case_suite_id'] = $this->sqlTestCaseSuiteId(); $strSql .= ' test_case_suite_id = ' . $this->sqlTestCaseSuiteId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_script_type_id = ' . $this->sqlTestCaseScriptTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseScriptTypeId() ) != $this->getOriginalValueByFieldName ( 'test_case_script_type_id' ) ) { $arrstrOriginalValueChanges['test_case_script_type_id'] = $this->sqlTestCaseScriptTypeId(); $strSql .= ' test_case_script_type_id = ' . $this->sqlTestCaseScriptTypeId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSvnRepositoryId() ) != $this->getOriginalValueByFieldName ( 'svn_repository_id' ) ) { $arrstrOriginalValueChanges['svn_repository_id'] = $this->sqlSvnRepositoryId(); $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_path = ' . $this->sqlTestCasePath() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCasePath() ) != $this->getOriginalValueByFieldName ( 'test_case_path' ) ) { $arrstrOriginalValueChanges['test_case_path'] = $this->sqlTestCasePath(); $strSql .= ' test_case_path = ' . $this->sqlTestCasePath() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName ( 'description' ) ) { $arrstrOriginalValueChanges['description'] = $this->sqlDescription(); $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.test_cases WHERE id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.test_cases_id_seq', $objDatabase );
    }

}
?>