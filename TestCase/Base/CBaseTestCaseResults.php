<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseResults
 * Do not add any new functions to this class.
 */

class CBaseTestCaseResults extends CEosPluralBase {

	/**
	 * @return CTestCaseResult[]
	 */
	public static function fetchTestCaseResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseResult', $objDatabase );
	}

	/**
	 * @return CTestCaseResult
	 */
	public static function fetchTestCaseResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseResult', $objDatabase );
	}

	public static function fetchTestCaseResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_results', $objDatabase );
	}

	public static function fetchTestCaseResultById( $intId, $objDatabase ) {
		return self::fetchTestCaseResult( sprintf( 'SELECT * FROM test_case_results WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestCaseResultsByTestCaseId( $intTestCaseId, $objDatabase ) {
		return self::fetchTestCaseResults( sprintf( 'SELECT * FROM test_case_results WHERE test_case_id = %d', ( int ) $intTestCaseId ), $objDatabase );
	}

	public static function fetchTestCaseResultsByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {
		return self::fetchTestCaseResults( sprintf( 'SELECT * FROM test_case_results WHERE test_case_request_id = %d', ( int ) $intTestCaseRequestId ), $objDatabase );
	}

}
?>