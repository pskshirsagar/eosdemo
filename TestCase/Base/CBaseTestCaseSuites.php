<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseSuites
 * Do not add any new functions to this class.
 */

class CBaseTestCaseSuites extends CEosPluralBase {

	/**
	 * @return CTestCaseSuite[]
	 */
	public static function fetchTestCaseSuites( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseSuite', $objDatabase );
	}

	/**
	 * @return CTestCaseSuite
	 */
	public static function fetchTestCaseSuite( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseSuite', $objDatabase );
	}

	public static function fetchTestCaseSuiteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_suites', $objDatabase );
	}

	public static function fetchTestCaseSuiteById( $intId, $objDatabase ) {
		return self::fetchTestCaseSuite( sprintf( 'SELECT * FROM test_case_suites WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>