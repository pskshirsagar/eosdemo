<?php

class CBaseTestCaseRequest extends CEosSingularBase {

	protected $m_intId;
	protected $m_strName;
	protected $m_intTestCaseSuiteId;
	protected $m_intTestCaseRequestStatusTypeId;
	protected $m_intSvnRepositoryId;
	protected $m_intTestCaseScriptTypeId;
	protected $m_intDeploymentId;
	protected $m_intClusterId;
	protected $m_intPort;
	protected $m_strStartDateTime;
	protected $m_strEndDateTime;
	protected $m_strEmailNotifications;
	protected $m_intRequestedBy;
	protected $m_intIsPublished;
	protected $m_intIsExecuteOnPartialPush;
	protected $m_intIsExecuteOnReleaseDay;
	protected $m_intIsRecurring;
	protected $m_intSendEmailNotification;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intIsExecuteOnPartialPush = '0';
		$this->m_intIsExecuteOnReleaseDay = '0';
		$this->m_intSendEmailNotification = '0';
		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['test_case_suite_id'] ) && $boolDirectSet ) $this->m_intTestCaseSuiteId = trim( $arrValues['test_case_suite_id'] ); else if( isset( $arrValues['test_case_suite_id'] ) ) $this->setTestCaseSuiteId( $arrValues['test_case_suite_id'] );
		if( isset( $arrValues['test_case_request_status_type_id'] ) && $boolDirectSet ) $this->m_intTestCaseRequestStatusTypeId = trim( $arrValues['test_case_request_status_type_id'] ); else if( isset( $arrValues['test_case_request_status_type_id'] ) ) $this->setTestCaseRequestStatusTypeId( $arrValues['test_case_request_status_type_id'] );
		if( isset( $arrValues['svn_repository_id'] ) && $boolDirectSet ) $this->m_intSvnRepositoryId = trim( $arrValues['svn_repository_id'] ); else if( isset( $arrValues['svn_repository_id'] ) ) $this->setSvnRepositoryId( $arrValues['svn_repository_id'] );
		if( isset( $arrValues['test_case_script_type_id'] ) && $boolDirectSet ) $this->m_intTestCaseScriptTypeId = trim( $arrValues['test_case_script_type_id'] ); else if( isset( $arrValues['test_case_script_type_id'] ) ) $this->setTestCaseScriptTypeId( $arrValues['test_case_script_type_id'] );
		if( isset( $arrValues['deployment_id'] ) && $boolDirectSet ) $this->m_intDeploymentId = trim( $arrValues['deployment_id'] ); else if( isset( $arrValues['deployment_id'] ) ) $this->setDeploymentId( $arrValues['deployment_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->m_intClusterId = trim( $arrValues['cluster_id'] ); else if( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['port'] ) && $boolDirectSet ) $this->m_intPort = trim( $arrValues['port'] ); else if( isset( $arrValues['port'] ) ) $this->setPort( $arrValues['port'] );
		if( isset( $arrValues['start_date_time'] ) && $boolDirectSet ) $this->m_strStartDateTime = trim( $arrValues['start_date_time'] ); else if( isset( $arrValues['start_date_time'] ) ) $this->setStartDateTime( $arrValues['start_date_time'] );
		if( isset( $arrValues['end_date_time'] ) && $boolDirectSet ) $this->m_strEndDateTime = trim( $arrValues['end_date_time'] ); else if( isset( $arrValues['end_date_time'] ) ) $this->setEndDateTime( $arrValues['end_date_time'] );
		if( isset( $arrValues['email_notifications'] ) && $boolDirectSet ) $this->m_strEmailNotifications = trim( stripcslashes( $arrValues['email_notifications'] ) ); else if( isset( $arrValues['email_notifications'] ) ) $this->setEmailNotifications( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_notifications'] ) : $arrValues['email_notifications'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->m_intRequestedBy = trim( $arrValues['requested_by'] ); else if( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_intIsPublished = trim( $arrValues['is_published'] ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_execute_on_partial_push'] ) && $boolDirectSet ) $this->m_intIsExecuteOnPartialPush = trim( $arrValues['is_execute_on_partial_push'] ); else if( isset( $arrValues['is_execute_on_partial_push'] ) ) $this->setIsExecuteOnPartialPush( $arrValues['is_execute_on_partial_push'] );
		if( isset( $arrValues['is_execute_on_release_day'] ) && $boolDirectSet ) $this->m_intIsExecuteOnReleaseDay = trim( $arrValues['is_execute_on_release_day'] ); else if( isset( $arrValues['is_execute_on_release_day'] ) ) $this->setIsExecuteOnReleaseDay( $arrValues['is_execute_on_release_day'] );
		if( isset( $arrValues['is_recurring'] ) && $boolDirectSet ) $this->m_intIsRecurring = trim( $arrValues['is_recurring'] ); else if( isset( $arrValues['is_recurring'] ) ) $this->setIsRecurring( $arrValues['is_recurring'] );
		if( isset( $arrValues['send_email_notification'] ) && $boolDirectSet ) $this->m_intSendEmailNotification = trim( $arrValues['send_email_notification'] ); else if( isset( $arrValues['send_email_notification'] ) ) $this->setSendEmailNotification( $arrValues['send_email_notification'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->m_strName = CStrings::strTrimDef( $strName, 255, NULL, true );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTestCaseSuiteId( $intTestCaseSuiteId ) {
		$this->m_intTestCaseSuiteId = CStrings::strToIntDef( $intTestCaseSuiteId, NULL, false );
	}

	public function getTestCaseSuiteId() {
		return $this->m_intTestCaseSuiteId;
	}

	public function sqlTestCaseSuiteId() {
		return ( true == isset( $this->m_intTestCaseSuiteId ) ) ? ( string ) $this->m_intTestCaseSuiteId : 'NULL';
	}

	public function setTestCaseRequestStatusTypeId( $intTestCaseRequestStatusTypeId ) {
		$this->m_intTestCaseRequestStatusTypeId = CStrings::strToIntDef( $intTestCaseRequestStatusTypeId, NULL, false );
	}

	public function getTestCaseRequestStatusTypeId() {
		return $this->m_intTestCaseRequestStatusTypeId;
	}

	public function sqlTestCaseRequestStatusTypeId() {
		return ( true == isset( $this->m_intTestCaseRequestStatusTypeId ) ) ? ( string ) $this->m_intTestCaseRequestStatusTypeId : 'NULL';
	}

	public function setSvnRepositoryId( $intSvnRepositoryId ) {
		$this->m_intSvnRepositoryId = CStrings::strToIntDef( $intSvnRepositoryId, NULL, false );
	}

	public function getSvnRepositoryId() {
		return $this->m_intSvnRepositoryId;
	}

	public function sqlSvnRepositoryId() {
		return ( true == isset( $this->m_intSvnRepositoryId ) ) ? ( string ) $this->m_intSvnRepositoryId : 'NULL';
	}

	public function setTestCaseScriptTypeId( $intTestCaseScriptTypeId ) {
		$this->m_intTestCaseScriptTypeId = CStrings::strToIntDef( $intTestCaseScriptTypeId, NULL, false );
	}

	public function getTestCaseScriptTypeId() {
		return $this->m_intTestCaseScriptTypeId;
	}

	public function sqlTestCaseScriptTypeId() {
		return ( true == isset( $this->m_intTestCaseScriptTypeId ) ) ? ( string ) $this->m_intTestCaseScriptTypeId : 'NULL';
	}

	public function setDeploymentId( $intDeploymentId ) {
		$this->m_intDeploymentId = CStrings::strToIntDef( $intDeploymentId, NULL, false );
	}

	public function getDeploymentId() {
		return $this->m_intDeploymentId;
	}

	public function sqlDeploymentId() {
		return ( true == isset( $this->m_intDeploymentId ) ) ? ( string ) $this->m_intDeploymentId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->m_intClusterId = CStrings::strToIntDef( $intClusterId, NULL, false );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setPort( $intPort ) {
		$this->m_intPort = CStrings::strToIntDef( $intPort, NULL, false );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function sqlPort() {
		return ( true == isset( $this->m_intPort ) ) ? ( string ) $this->m_intPort : 'NULL';
	}

	public function setStartDateTime( $strStartDateTime ) {
		$this->m_strStartDateTime = CStrings::strTrimDef( $strStartDateTime, -1, NULL, true );
	}

	public function getStartDateTime() {
		return $this->m_strStartDateTime;
	}

	public function sqlStartDateTime() {
		return ( true == isset( $this->m_strStartDateTime ) ) ? '\'' . $this->m_strStartDateTime . '\'' : 'NULL';
	}

	public function setEndDateTime( $strEndDateTime ) {
		$this->m_strEndDateTime = CStrings::strTrimDef( $strEndDateTime, -1, NULL, true );
	}

	public function getEndDateTime() {
		return $this->m_strEndDateTime;
	}

	public function sqlEndDateTime() {
		return ( true == isset( $this->m_strEndDateTime ) ) ? '\'' . $this->m_strEndDateTime . '\'' : 'NULL';
	}

	public function setEmailNotifications( $strEmailNotifications ) {
		$this->m_strEmailNotifications = CStrings::strTrimDef( $strEmailNotifications, 520, NULL, true );
	}

	public function getEmailNotifications() {
		return $this->m_strEmailNotifications;
	}

	public function sqlEmailNotifications() {
		return ( true == isset( $this->m_strEmailNotifications ) ) ? '\'' . addslashes( $this->m_strEmailNotifications ) . '\'' : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->m_intRequestedBy = CStrings::strToIntDef( $intRequestedBy, NULL, false );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->m_intIsPublished = CStrings::strToIntDef( $intIsPublished, NULL, false );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsExecuteOnPartialPush( $intIsExecuteOnPartialPush ) {
		$this->m_intIsExecuteOnPartialPush = CStrings::strToIntDef( $intIsExecuteOnPartialPush, NULL, false );
	}

	public function getIsExecuteOnPartialPush() {
		return $this->m_intIsExecuteOnPartialPush;
	}

	public function sqlIsExecuteOnPartialPush() {
		return ( true == isset( $this->m_intIsExecuteOnPartialPush ) ) ? ( string ) $this->m_intIsExecuteOnPartialPush : '0';
	}

	public function setIsExecuteOnReleaseDay( $intIsExecuteOnReleaseDay ) {
		$this->m_intIsExecuteOnReleaseDay = CStrings::strToIntDef( $intIsExecuteOnReleaseDay, NULL, false );
	}

	public function getIsExecuteOnReleaseDay() {
		return $this->m_intIsExecuteOnReleaseDay;
	}

	public function sqlIsExecuteOnReleaseDay() {
		return ( true == isset( $this->m_intIsExecuteOnReleaseDay ) ) ? ( string ) $this->m_intIsExecuteOnReleaseDay : '0';
	}

	public function setIsRecurring( $intIsRecurring ) {
		$this->m_intIsRecurring = CStrings::strToIntDef( $intIsRecurring, NULL, false );
	}

	public function getIsRecurring() {
		return $this->m_intIsRecurring;
	}

	public function sqlIsRecurring() {
		return ( true == isset( $this->m_intIsRecurring ) ) ? ( string ) $this->m_intIsRecurring : 'NULL';
	}

	public function setSendEmailNotification( $intSendEmailNotification ) {
		$this->m_intSendEmailNotification = CStrings::strToIntDef( $intSendEmailNotification, NULL, false );
	}

	public function getSendEmailNotification() {
		return $this->m_intSendEmailNotification;
	}

	public function sqlSendEmailNotification() {
		return ( true == isset( $this->m_intSendEmailNotification ) ) ? ( string ) $this->m_intSendEmailNotification : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.test_case_requests_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.test_case_requests
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlTestCaseSuiteId() . ', ' .
 						$this->sqlTestCaseRequestStatusTypeId() . ', ' .
 						$this->sqlSvnRepositoryId() . ', ' .
 						$this->sqlTestCaseScriptTypeId() . ', ' .
 						$this->sqlDeploymentId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlPort() . ', ' .
 						$this->sqlStartDateTime() . ', ' .
 						$this->sqlEndDateTime() . ', ' .
 						$this->sqlEmailNotifications() . ', ' .
 						$this->sqlRequestedBy() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsExecuteOnPartialPush() . ', ' .
 						$this->sqlIsExecuteOnReleaseDay() . ', ' .
 						$this->sqlIsRecurring() . ', ' .
 						$this->sqlSendEmailNotification() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.test_case_requests
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlName() ) != $this->getOriginalValueByFieldName ( 'name' ) ) { $arrstrOriginalValueChanges['name'] = $this->sqlName(); $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_suite_id = ' . $this->sqlTestCaseSuiteId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseSuiteId() ) != $this->getOriginalValueByFieldName ( 'test_case_suite_id' ) ) { $arrstrOriginalValueChanges['test_case_suite_id'] = $this->sqlTestCaseSuiteId(); $strSql .= ' test_case_suite_id = ' . $this->sqlTestCaseSuiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_request_status_type_id = ' . $this->sqlTestCaseRequestStatusTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseRequestStatusTypeId() ) != $this->getOriginalValueByFieldName ( 'test_case_request_status_type_id' ) ) { $arrstrOriginalValueChanges['test_case_request_status_type_id'] = $this->sqlTestCaseRequestStatusTypeId(); $strSql .= ' test_case_request_status_type_id = ' . $this->sqlTestCaseRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSvnRepositoryId() ) != $this->getOriginalValueByFieldName ( 'svn_repository_id' ) ) { $arrstrOriginalValueChanges['svn_repository_id'] = $this->sqlSvnRepositoryId(); $strSql .= ' svn_repository_id = ' . $this->sqlSvnRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_script_type_id = ' . $this->sqlTestCaseScriptTypeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseScriptTypeId() ) != $this->getOriginalValueByFieldName ( 'test_case_script_type_id' ) ) { $arrstrOriginalValueChanges['test_case_script_type_id'] = $this->sqlTestCaseScriptTypeId(); $strSql .= ' test_case_script_type_id = ' . $this->sqlTestCaseScriptTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeploymentId() ) != $this->getOriginalValueByFieldName ( 'deployment_id' ) ) { $arrstrOriginalValueChanges['deployment_id'] = $this->sqlDeploymentId(); $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlClusterId() ) != $this->getOriginalValueByFieldName ( 'cluster_id' ) ) { $arrstrOriginalValueChanges['cluster_id'] = $this->sqlClusterId(); $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port = ' . $this->sqlPort() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPort() ) != $this->getOriginalValueByFieldName ( 'port' ) ) { $arrstrOriginalValueChanges['port'] = $this->sqlPort(); $strSql .= ' port = ' . $this->sqlPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date_time = ' . $this->sqlStartDateTime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlStartDateTime() ) != $this->getOriginalValueByFieldName ( 'start_date_time' ) ) { $arrstrOriginalValueChanges['start_date_time'] = $this->sqlStartDateTime(); $strSql .= ' start_date_time = ' . $this->sqlStartDateTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date_time = ' . $this->sqlEndDateTime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEndDateTime() ) != $this->getOriginalValueByFieldName ( 'end_date_time' ) ) { $arrstrOriginalValueChanges['end_date_time'] = $this->sqlEndDateTime(); $strSql .= ' end_date_time = ' . $this->sqlEndDateTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_notifications = ' . $this->sqlEmailNotifications() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmailNotifications() ) != $this->getOriginalValueByFieldName ( 'email_notifications' ) ) { $arrstrOriginalValueChanges['email_notifications'] = $this->sqlEmailNotifications(); $strSql .= ' email_notifications = ' . $this->sqlEmailNotifications() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlRequestedBy() ) != $this->getOriginalValueByFieldName ( 'requested_by' ) ) { $arrstrOriginalValueChanges['requested_by'] = $this->sqlRequestedBy(); $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName ( 'is_published' ) ) { $arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished(); $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_execute_on_partial_push = ' . $this->sqlIsExecuteOnPartialPush() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsExecuteOnPartialPush() ) != $this->getOriginalValueByFieldName ( 'is_execute_on_partial_push' ) ) { $arrstrOriginalValueChanges['is_execute_on_partial_push'] = $this->sqlIsExecuteOnPartialPush(); $strSql .= ' is_execute_on_partial_push = ' . $this->sqlIsExecuteOnPartialPush() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_execute_on_release_day = ' . $this->sqlIsExecuteOnReleaseDay() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsExecuteOnReleaseDay() ) != $this->getOriginalValueByFieldName ( 'is_execute_on_release_day' ) ) { $arrstrOriginalValueChanges['is_execute_on_release_day'] = $this->sqlIsExecuteOnReleaseDay(); $strSql .= ' is_execute_on_release_day = ' . $this->sqlIsExecuteOnReleaseDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_recurring = ' . $this->sqlIsRecurring() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsRecurring() ) != $this->getOriginalValueByFieldName ( 'is_recurring' ) ) { $arrstrOriginalValueChanges['is_recurring'] = $this->sqlIsRecurring(); $strSql .= ' is_recurring = ' . $this->sqlIsRecurring() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_email_notification = ' . $this->sqlSendEmailNotification() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSendEmailNotification() ) != $this->getOriginalValueByFieldName ( 'send_email_notification' ) ) { $arrstrOriginalValueChanges['send_email_notification'] = $this->sqlSendEmailNotification(); $strSql .= ' send_email_notification = ' . $this->sqlSendEmailNotification() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.test_case_requests WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.test_case_requests_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'test_case_suite_id' => $this->getTestCaseSuiteId(),
			'test_case_request_status_type_id' => $this->getTestCaseRequestStatusTypeId(),
			'svn_repository_id' => $this->getSvnRepositoryId(),
			'test_case_script_type_id' => $this->getTestCaseScriptTypeId(),
			'deployment_id' => $this->getDeploymentId(),
			'cluster_id' => $this->getClusterId(),
			'port' => $this->getPort(),
			'start_date_time' => $this->getStartDateTime(),
			'end_date_time' => $this->getEndDateTime(),
			'email_notifications' => $this->getEmailNotifications(),
			'requested_by' => $this->getRequestedBy(),
			'is_published' => $this->getIsPublished(),
			'is_execute_on_partial_push' => $this->getIsExecuteOnPartialPush(),
			'is_execute_on_release_day' => $this->getIsExecuteOnReleaseDay(),
			'is_recurring' => $this->getIsRecurring(),
			'send_email_notification' => $this->getSendEmailNotification(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>