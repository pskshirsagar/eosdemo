<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequestDetails
 * Do not add any new functions to this class.
 */

class CBaseTestCaseRequestDetails extends CEosPluralBase {

	/**
	 * @return CTestCaseRequestDetail[]
	 */
	public static function fetchTestCaseRequestDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCaseRequestDetail', $objDatabase );
	}

	/**
	 * @return CTestCaseRequestDetail
	 */
	public static function fetchTestCaseRequestDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCaseRequestDetail', $objDatabase );
	}

	public static function fetchTestCaseRequestDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_case_request_details', $objDatabase );
	}

	public static function fetchTestCaseRequestDetailById( $intId, $objDatabase ) {
		return self::fetchTestCaseRequestDetail( sprintf( 'SELECT * FROM test_case_request_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestCaseRequestDetailsByTestCaseId( $intTestCaseId, $objDatabase ) {
		return self::fetchTestCaseRequestDetails( sprintf( 'SELECT * FROM test_case_request_details WHERE test_case_id = %d', ( int ) $intTestCaseId ), $objDatabase );
	}

	public static function fetchTestCaseRequestDetailsByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {
		return self::fetchTestCaseRequestDetails( sprintf( 'SELECT * FROM test_case_request_details WHERE test_case_request_id = %d', ( int ) $intTestCaseRequestId ), $objDatabase );
	}

}
?>