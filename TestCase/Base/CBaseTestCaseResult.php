<?php

class CBaseTestCaseResult extends CEosSingularBase {

	protected $m_intId;
	protected $m_intTestCaseId;
	protected $m_intTestCaseRequestId;
	protected $m_fltExecutionTime;
	protected $m_strFailures;
	protected $m_strErrors;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_case_id'] ) && $boolDirectSet ) $this->m_intTestCaseId = trim( $arrValues['test_case_id'] ); else if( isset( $arrValues['test_case_id'] ) ) $this->setTestCaseId( $arrValues['test_case_id'] );
		if( isset( $arrValues['test_case_request_id'] ) && $boolDirectSet ) $this->m_intTestCaseRequestId = trim( $arrValues['test_case_request_id'] ); else if( isset( $arrValues['test_case_request_id'] ) ) $this->setTestCaseRequestId( $arrValues['test_case_request_id'] );
		if( isset( $arrValues['execution_time'] ) && $boolDirectSet ) $this->m_fltExecutionTime = trim( $arrValues['execution_time'] ); else if( isset( $arrValues['execution_time'] ) ) $this->setExecutionTime( $arrValues['execution_time'] );
		if( isset( $arrValues['failures'] ) && $boolDirectSet ) $this->m_strFailures = trim( stripcslashes( $arrValues['failures'] ) ); else if( isset( $arrValues['failures'] ) ) $this->setFailures( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['failures'] ) : $arrValues['failures'] );
		if( isset( $arrValues['errors'] ) && $boolDirectSet ) $this->m_strErrors = trim( stripcslashes( $arrValues['errors'] ) ); else if( isset( $arrValues['errors'] ) ) $this->setErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['errors'] ) : $arrValues['errors'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setTestCaseId( $intTestCaseId ) {
		$this->m_intTestCaseId = CStrings::strToIntDef( $intTestCaseId, NULL, false );
	}

	public function getTestCaseId() {
		return $this->m_intTestCaseId;
	}

	public function sqlTestCaseId() {
		return ( true == isset( $this->m_intTestCaseId ) ) ? (string) $this->m_intTestCaseId : 'NULL';
	}

	public function setTestCaseRequestId( $intTestCaseRequestId ) {
		$this->m_intTestCaseRequestId = CStrings::strToIntDef( $intTestCaseRequestId, NULL, false );
	}

	public function getTestCaseRequestId() {
		return $this->m_intTestCaseRequestId;
	}

	public function sqlTestCaseRequestId() {
		return ( true == isset( $this->m_intTestCaseRequestId ) ) ? (string) $this->m_intTestCaseRequestId : 'NULL';
	}

	public function setExecutionTime( $fltExecutionTime ) {
		$this->m_fltExecutionTime = CStrings::strToFloatDef( $fltExecutionTime, NULL, false, 0 );
	}

	public function getExecutionTime() {
		return $this->m_fltExecutionTime;
	}

	public function sqlExecutionTime() {
		return ( true == isset( $this->m_fltExecutionTime ) ) ? (string) $this->m_fltExecutionTime : 'NULL';
	}

	public function setFailures( $strFailures ) {
		$this->m_strFailures = CStrings::strTrimDef( $strFailures, -1, NULL, true );
	}

	public function getFailures() {
		return $this->m_strFailures;
	}

	public function sqlFailures() {
		return ( true == isset( $this->m_strFailures ) ) ? '\'' . addslashes( $this->m_strFailures ) . '\'' : 'NULL';
	}

	public function setErrors( $strErrors ) {
		$this->m_strErrors = CStrings::strTrimDef( $strErrors, -1, NULL, true );
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	public function sqlErrors() {
		return ( true == isset( $this->m_strErrors ) ) ? '\'' . addslashes( $this->m_strErrors ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.test_case_results_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.test_case_results
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestCaseId() . ', ' .
 						$this->sqlTestCaseRequestId() . ', ' .
 						$this->sqlExecutionTime() . ', ' .
 						$this->sqlFailures() . ', ' .
 						$this->sqlErrors() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.test_case_results
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_id = ' . $this->sqlTestCaseId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseId() ) != $this->getOriginalValueByFieldName ( 'test_case_id' ) ) { $arrstrOriginalValueChanges['test_case_id'] = $this->sqlTestCaseId(); $strSql .= ' test_case_id = ' . $this->sqlTestCaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_request_id = ' . $this->sqlTestCaseRequestId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTestCaseRequestId() ) != $this->getOriginalValueByFieldName ( 'test_case_request_id' ) ) { $arrstrOriginalValueChanges['test_case_request_id'] = $this->sqlTestCaseRequestId(); $strSql .= ' test_case_request_id = ' . $this->sqlTestCaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' execution_time = ' . $this->sqlExecutionTime() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlExecutionTime() ), $this->getOriginalValueByFieldName ( 'execution_time' ), 0 ) ) ) { $arrstrOriginalValueChanges['execution_time'] = $this->sqlExecutionTime(); $strSql .= ' execution_time = ' . $this->sqlExecutionTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failures = ' . $this->sqlFailures() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlFailures() ) != $this->getOriginalValueByFieldName ( 'failures' ) ) { $arrstrOriginalValueChanges['failures'] = $this->sqlFailures(); $strSql .= ' failures = ' . $this->sqlFailures() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' errors = ' . $this->sqlErrors() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlErrors() ) != $this->getOriginalValueByFieldName ( 'errors' ) ) { $arrstrOriginalValueChanges['errors'] = $this->sqlErrors(); $strSql .= ' errors = ' . $this->sqlErrors() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . (int) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.test_case_results WHERE id = ' . (int) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.test_case_results_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_case_id' => $this->getTestCaseId(),
			'test_case_request_id' => $this->getTestCaseRequestId(),
			'execution_time' => $this->getExecutionTime(),
			'failures' => $this->getFailures(),
			'errors' => $this->getErrors(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>