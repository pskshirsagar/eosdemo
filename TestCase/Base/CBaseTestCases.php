<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCases
 * Do not add any new functions to this class.
 */

class CBaseTestCases extends CEosPluralBase {

	/**
	 * @return CTestCase[]
	 */
	public static function fetchTestCases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestCase', $objDatabase );
	}

	/**
	 * @return CTestCase
	 */
	public static function fetchTestCase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestCase', $objDatabase );
	}

	public static function fetchTestCaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_cases', $objDatabase );
	}

	public static function fetchTestCaseById( $intId, $objDatabase ) {
		return self::fetchTestCase( sprintf( 'SELECT * FROM test_cases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestCasesByTestCaseSuiteId( $intTestCaseSuiteId, $objDatabase ) {
		return self::fetchTestCases( sprintf( 'SELECT * FROM test_cases WHERE test_case_suite_id = %d', ( int ) $intTestCaseSuiteId ), $objDatabase );
	}

	public static function fetchTestCasesByTestCaseScriptTypeId( $intTestCaseScriptTypeId, $objDatabase ) {
		return self::fetchTestCases( sprintf( 'SELECT * FROM test_cases WHERE test_case_script_type_id = %d', ( int ) $intTestCaseScriptTypeId ), $objDatabase );
	}

	public static function fetchTestCasesBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		return self::fetchTestCases( sprintf( 'SELECT * FROM test_cases WHERE svn_repository_id = %d', ( int ) $intSvnRepositoryId ), $objDatabase );
	}

}
?>