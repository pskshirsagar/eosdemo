<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseResults
 * Do not add any new functions to this class.
 */

class CTestCaseResults extends CBaseTestCaseResults {

	public static function fetchJenkinsTestCaseResults( $intTestCaseId, $objDatabase ) {
		$strSql = 'SELECT *
					FROM
    					test_case_results tcr
    					JOIN test_cases tc ON ( tcr.test_case_id = tc.id )
					WHERE
    					tcr.test_case_request_id IS NULL
    					AND tcr.test_case_id = tc.id
    					AND tc.id =' . ( int ) $intTestCaseId .
    					' ORDER BY tcr.id DESC LIMIT 1';

        return parent::fetchObject( $strSql, 'CTestCaseResult', $objDatabase );
    }

    public static function fetchAllTestCaseResults( $objDatabase ) {
    	$strSql = 'SELECT * FROM test_case_results';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchOnDemandTestCaseResults( $intIsExecuteOnPartialPush, $intIsExecuteOnReleaseDay, $objDatabase ) {

    	$strSql = 'SELECT
    					tcr.*
					FROM
    					test_case_results tcr
    					--JOIN test_cases tc ON ( tc.id = tcr.test_case_id )
					WHERE
    					tcr.test_case_request_id IN ( SELECT id FROM test_case_requests where is_published=1 AND is_execute_on_partial_push =' . ( int ) $intIsExecuteOnPartialPush . ' AND is_execute_on_release_day = ' . ( int ) $intIsExecuteOnReleaseDay . ' )
					ORDER BY
    					tcr.id DESC';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchJenkinsTestCasesResultsBetweenDateRange( $strCurrentDate, $strPreviousDate, $objDatabase ) {

    	$strSql = 'SELECT
						tcr.*
					FROM test_case_results tcr
					WHERE tcr.test_case_request_id IS NULL
						AND to_char(tcr.created_on::DATE, \'YYYY-MM-DD\')
						BETWEEN to_char(\' ' . $strPreviousDate . '\'::DATE, \'YYYY-MM-DD\' ) AND to_char( \'' . $strCurrentDate . '\'::DATE, \'YYYY-MM-DD\' )
						ORDER BY tcr.id DESC';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchJenkinsTestCasesResults( $objDatabase ) {

    	$strSql = 'SELECT
    					tcr.*
					FROM
    					test_case_results tcr
    					JOIN test_cases tc ON ( tc.id = tcr.test_case_id )
    					JOIN test_case_suites tcs ON ( tcs.id = tc.test_case_suite_id )
					WHERE
    					tcr.test_case_request_id IS NULL
						ORDER BY
    					tcr.id DESC';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchTestCaseResultsByTestCaseRequestIdWithLimit( $intTestCaseRequestId, $intLimit, $objDatabase ) {

    	$strSql = 'SELECT * FROM test_case_results WHERE test_case_request_id = ' . ( int ) $intTestCaseRequestId . ' ORDER BY id DESC LIMIT ' . ( int ) $intLimit;

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchTestCaseRequestResultsByTestCaseIds( $arrintTestCaseIds, $objDatabase ) {
    	$strSql = 'SELECT * FROM test_case_results WHERE test_case_id IN( ' . implode( ',', $arrintTestCaseIds ) . ' )';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchTestCaseResultsByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {
    	$strSql = 'SELECT * FROM test_case_results WHERE test_case_request_id = ' . ( int ) $intTestCaseRequestId . ' ORDER BY id DESC';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchTestCaseResultsByTestCaseIdByTestCaseRequestId( $intTestCaseId, $intTestCaseRequestId, $objDatabase ) {
    	$strSql = 'SELECT * FROM test_case_results WHERE test_case_request_id = ' . ( int ) $intTestCaseRequestId . ' AND test_case_id = ' . ( int ) $intTestCaseId . ' ORDER BY id ASC';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

    public static function fetchTestCaseResultsByTestCaseRequestIdByCreatedOn( $intTestCaseRequestId, $strCreatedOn, $objDatabase ) {

    	$strSql = 'SELECT * FROM test_case_results WHERE test_case_request_id= ' . ( int ) $intTestCaseRequestId . ' AND created_on =\'' . $strCreatedOn . '\'';

    	return self::fetchTestCaseResults( $strSql, $objDatabase );
    }

}
?>