<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CDashboardTimings
 * Do not add any new functions to this class.
 */

class CDashboardTimings extends CBaseDashboardTimings {

    public static function fetchPaginatedDashboardTimingsBySvnRepositoryId( $intSvnRepositoryId, $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL, $objDatabase ) {

    	if( 0 < $intPageNo ) {
    		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    		$intLimit  = ( int ) $intPageSize;
    	}

    	$strOrderClause = '';

    	$strOrder = ( false == is_null( $strOrderByType ) ) ? $strOrderByType : ' DESC ';
    	$strOrderField = ( false == is_null( $strOrderByField ) ) ? $strOrderByField : ' id ';
    	$strOrderClause = ' ORDER BY ' . $strOrderField . ' ' . $strOrder;

    	$strSql = 'SELECT * FROM dashboard_timings WHERE svn_repository_id = ' . ( int ) $intSvnRepositoryId .
    				' AND created_on::date = ( SELECT MAX( created_on::date ) FROM dashboard_timings )' . $strOrderClause;

    	if( 0 < $intPageNo ) {
    		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
    	}

    	return self::fetchDashboardTimings( $strSql, $objDatabase );

    }

    public static function fetchPaginatedDashboardTimingCountsBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {

    	$strSql = 'SELECT count(id) FROM dashboard_timings WHERE svn_repository_id = ' . ( int ) $intSvnRepositoryId .
    	 			' AND created_on::date = ( SELECT MAX( created_on::date ) FROM dashboard_timings )';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) )
			return $arrintResponse[0]['count'];

		return 0;
    }

    public static function fetchDashboardTimingsBySvnRepositoryIdByCids( $intSvnRepositoryId, $arrintCids, $objDatabase ) {

    	$strSql = 'SELECT * FROM dashboard_timings WHERE svn_repository_id = ' . ( int ) $intSvnRepositoryId . ' AND cid IN ( ' . implode( ',', $arrintCids ) . ')
    				AND created_on::date = ( SELECT MAX( created_on::date ) FROM dashboard_timings )';

    	return self::fetchObjects( $strSql, 'CDashboardTiming', $objDatabase );
    }

    public static function fetchLastExecutionDashboardTimingsByCidsBySvnRepositoryId( $arrintCids, $intSvnRepositoryId, $objDatabase ) {

    	$strSql = 'SELECT
    					*
    				FROM
    					dashboard_timings
    				WHERE
    					svn_repository_id = ' . ( int ) $intSvnRepositoryId .
    					'AND cid IN ( ' . implode( ',', $arrintCids ) . ' ) ' .
    					'AND created_on::date = ( SELECT MAX(
    																	 created_on::date ) FROM dashboard_timings
																		WHERE created_on::date <> ( SELECT MAX( created_on::date ) FROM dashboard_timings ) )';

    	return self::fetchDashboardTimings( $strSql, $objDatabase );

    }

}
?>