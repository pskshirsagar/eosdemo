<?php

class CTestCaseRequestDetail extends CBaseTestCaseRequestDetail {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTestCaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTestCaseRequestId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>