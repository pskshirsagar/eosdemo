<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\TestCase\CTestCaseRequestDetails
 * Do not add any new functions to this class.
 */

class CTestCaseRequestDetails extends CBaseTestCaseRequestDetails {

	public static function fetchTestCaseRequestDetailsByTestCaseRequestIds( $arrintTestCaseRequestIds, $objDatabase ) {
		$strSql = 'SELECT * FROM test_case_request_details WHERE test_case_request_id IN( ' . implode( ',', $arrintTestCaseRequestIds ) . ' )';

		return self::fetchTestCaseRequestDetails( $strSql, $objDatabase );
	}

	public static function fetchAllTestCaseRequestDetailes( $objDatabase ) {
		$strSql = 'SELECT
    					tcrd.*
					FROM
    					test_case_request_details tcrd
    					JOIN test_cases tc ON ( tc.id = tcrd.test_case_id )
					WHERE
   						tc.is_published = 1 ORDER BY tcrd.test_case_request_id DESC';

		return self::fetchTestCaseRequestDetails( $strSql, $objDatabase );
	}

	public static function fetchRequestTestCaseCountByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {
		$strSql = 'SELECT count(id) FROM test_case_request_details WHERE test_case_request_id = ' . ( int ) $intTestCaseRequestId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchTestCaseRequestDetailsByTestCaseIds( $arrintTestCaseIds, $objDatabase ) {

		if( false == valArr( $arrintTestCaseIds ) ) return NULL;

		$strSql = 'SELECT * FROM test_case_request_details WHERE test_case_id IN( ' . implode( ',', $arrintTestCaseIds ) . ' )';

		return self::fetchTestCaseRequestDetails( $strSql, $objDatabase );
	}

	public static function fetchTestCaseRequestIdsByTestCaseIds( $arrintTestCaseIds, $objDatabase ) {

		$strSql = 'SELECT
    					test_case_request_id
					FROM
    					test_case_request_details
    				JOIN
    					(
      						SELECT
          						tcrd.test_case_request_id AS request_id
      						FROM
          						test_case_request_details tcrd
      						WHERE
          						tcrd.test_case_id IN ( ' . implode( ',', $arrintTestCaseIds ) . ' )
      						GROUP BY
          						tcrd.test_case_request_id
      						HAVING
          						COUNT ( * ) = ' . ( int ) \Psi\Libraries\UtilFunctions\count( $arrintTestCaseIds ) . '
    					) test_case_request ON test_case_request.request_id = test_case_request_details.test_case_request_id
					GROUP BY
    					test_case_request_id
					HAVING
    					COUNT ( * ) = ' . ( int ) \Psi\Libraries\UtilFunctions\count( $arrintTestCaseIds );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPublishedTestCasesRequestDetailsByTestCaseRequestId( $intTestCaseRequestId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						test_case_request_details tcrd
						JOIN test_cases tc ON ( tc.id = tcrd.test_case_id )
					WHERE
						tcrd.test_case_request_id = ' . ( int ) $intTestCaseRequestId . '
						AND tc.is_published = 1';

		return self::fetchTestCaseRequestDetails( $strSql, $objDatabase );

	}
}
?>