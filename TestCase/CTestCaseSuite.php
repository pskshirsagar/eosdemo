<?php

class CTestCaseSuite extends CBaseTestCaseSuite {

	const AUTOMATION_SUITE	= 1;
	const IS_EXECUTE_ON_PARTIAL_PUSH = 1;
	const POST_RELEASE_VERIFICATION	 = 86;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName( $objDatabase ) {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
			$boolIsValid = false;
			return $boolIsValid;
        } else {
        	$intId = ( true == is_null( $this->getId() ) )? 0 : $this->getId();
	        $intCount = CTestCaseSuites::fetchTestCaseSuiteCount( 'WHERE id <> ' . ( int ) $intId . 'AND LOWER( name ) = trim( \'' . strtolower( $this->getName() ) . '\')', $objDatabase );
	        if( 0 < $intCount ) {
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already exist.' ) );
	        	$boolIsValid = false;
	        	return $boolIsValid;
	        }
        }

        if( false == preg_match( '/^[a-zA-Z0-9. ]{2,25}$/i', $this->getName() ) || true == is_numeric( $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is invalid.' ) );
        	return $boolIsValid;
        }

        if( false == file_exists( PATH_PHP_TESTSUITES . '/' . $this->getName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Suite folder not exist.' ) );
        	return $boolIsValid;
        }

        return $boolIsValid;

    }

    public function valDescription() {
        $boolIsValid = true;
        $strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->getDescription() ) );

        $strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

        if( 0 >= strlen( $strDescription ) ) {
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
        	$boolIsValid = false;
        }
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valName( $objDatabase );
            	$boolIsValid &= $this->valDescription();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>