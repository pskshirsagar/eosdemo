<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailCleanupExceptions
 * Do not add any new functions to this class.
 */

class CSystemEmailCleanupExceptions extends CBaseSystemEmailCleanupExceptions {

	public static function fetchSystemEmailCleanupExceptionById( $intId, $objEmailDatabase ) {
		return self::fetchSystemEmailCleanupExceptions( sprintf( 'SELECT * FROM system_email_cleanup_exceptions WHERE id = ' . ( int ) $intId . ' ' ), $objEmailDatabase );
	}

	public static function fetchIgnoredSystemEmailCleanupExceptions( $objEmailDatabase ) {
		$strSql = 'SELECT
						cid,
						system_email_type_id
					FROM
						system_email_cleanup_exceptions';

		return fetchData( $strSql, $objEmailDatabase );
	}

	public static function fetchAllSystemEmailExceptions( $objEmailDatabase ) {
		$strSql = 'SELECT
						sece.id,
						sece.cid, 
						se.name as system_email_type
					FROM
						system_email_cleanup_exceptions sece
						LEFT JOIN system_email_types se ON ( sece.system_email_type_id = se.id )
					WHERE
						sece.deleted_by IS NULL
						AND sece.deleted_on IS NULL';
		return fetchData( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailExceptionsByCidBySystemEmailTypeId( $arrintCid, $arrintSystemEmailTypeId, $objEmailDatabase ) {

		$strWhere = '';
		if( false == valArr( $arrintCid ) && false == valArr( $arrintSystemEmailTypeId ) ) {
			return false;
		}
		if( true == valArr( $arrintCid ) && false == valArr( $arrintSystemEmailTypeId ) ) {
			$strWhere = 'AND cid IN ( ' . implode( ',', $arrintCid ) . ' ) AND system_email_type_id IS NULL';
		}

		if( true == valArr( $arrintSystemEmailTypeId ) && false == valArr( $arrintCid ) ) {
			$strWhere = 'AND system_email_type_id IN ( ' . implode( ',', $arrintSystemEmailTypeId ) . ' ) AND cid IS NULL';
		}

		if( true == valArr( $arrintSystemEmailTypeId ) && true == valArr( $arrintCid ) ) {
			$strWhere = 'AND system_email_type_id IN ( ' . implode( ',', $arrintSystemEmailTypeId ) . ' ) AND cid IN ( ' . implode( ',', $arrintCid ) . ' )';
		}

		$strSql = 'SELECT
						*
					FROM
						system_email_cleanup_exceptions
					WHERE 
						deleted_by IS NULL
						AND deleted_on IS NULL 
						' . $strWhere . ' ';
		return self::fetchSystemEmailCleanupExceptions( $strSql, $objEmailDatabase );
	}

}
?>