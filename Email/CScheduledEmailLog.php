<?php

class CScheduledEmailLog extends CBaseScheduledEmailLog {

	const PHONE_NUMBER_TYPE_ID_HOME 	= 2;
	const PHONE_NUMBER_TYPE_ID_OFFICE 	= 3;
	const PHONE_NUMBER_TYPE_ID_MOBILE 	= 4;

	const HOME 			= 'Home';
	const OFFICE 		= 'Work';
	const MOBILE 		= 'Mobile';

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valSystemEmailId() {
        $boolIsValid = true;

        if( true == is_null( $this->getSystemEmailId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_id', 'System email id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valScheduledEmailFilterName() {
        $boolIsValid = true;

        if( true == is_null( $this->getScheduledEmailFilterName() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_email_filter_name', 'Scheduled email filter name is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUnitNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRecipientCreatedOn() {
        $boolIsValid = true;

        if( true == is_null( $this->getRecipientCreatedOn() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_created_on', 'Recipient created on date is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valSystemEmailId();
            	$boolIsValid &= $this->valScheduledEmailFilterName();
            	$boolIsValid &= $this->valRecipientCreatedOn();
            	break;

            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>