<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CEmailAttachment extends CBaseEmailAttachment {

	const FILE_UPLOAD_OVERWRITE	= 1;

	protected $m_strSourceFilePath;

	protected $m_intEmailAttachmentActionId;
	protected $m_intSystemEmailId;
	protected $m_boolEmailAttachmentIsWrittenToFileSystem;
	protected $m_strUrl;

	protected $m_arrmixEmailAttachmentDetails;

	/**
	 * Set Functions
	 *
	 */

	public function setSourceFilePath( $strSourceFilePath ) {
    	$this->m_strSourceFilePath = $strSourceFilePath;
    }

    public function setUrl( $strUrl ) {
    	$this->m_strUrl = $strUrl;
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrmixValues['email_attachment_action_id'] ) ) $this->setEmailAttachmentActionId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email_attachment_action_id'] ) : $arrmixValues['email_attachment_action_id'] );
        if( true == isset( $arrmixValues['system_email_id'] ) ) $this->setSystemEmailId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_email_id'] ) : $arrmixValues['system_email_id'] );
        if( true == isset( $arrmixValues['str_url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['str_url'] ) : $arrmixValues['str_url'] );
	    if( true == isset( $arrmixValues['email_attachment_details'] ) ) $this->setEmailAttachmentDetails( $arrmixValues['email_attachment_details'] );
        return;
    }

    public function setEmailAttachmentActionId( $intEmailAttachmentActionId ) {
    	$this->m_intEmailAttachmentActionId = $intEmailAttachmentActionId;
    }

    public function setSystemEmailId( $intSystemEmailId ) {
    	return $this->m_intSystemEmailId = $intSystemEmailId;
    }

    public function setEmailAttachmentDetails( $arrmixEmailAttachmentDetails ) {
		$this->m_arrmixEmailAttachmentDetails = $arrmixEmailAttachmentDetails;
    }

	/**
	 * Get Functions
	 *
	 */

	public function getSourceFilePath() {
    	return $this->m_strSourceFilePath;
    }

    public function getFullFilePath() {
    	return PATH_MOUNTS . str_replace( PATH_MOUNTS, '', $this->m_strFilePath );
    }

   	public function getEmailAttachmentIsWrittenToFileSystem() {
    	return $this->m_boolEmailAttachmentIsWrittenToFileSystem;
    }

    public function getEmailAttachmentActionId() {
    	return $this->m_intEmailAttachmentActionId;
    }

    public function getSystemEmailId() {
    	return $this->m_intSystemEmailId;
    }

    public function getUrl() {
    	return $this->m_strUrl;
    }

	public function getEmailAttachmentDetails() {
		return $this->m_arrmixEmailAttachmentDetails;
	}

	/**
	 * Validation Functions
	 *
	 */

    public function valFileExtensionId() {
        $boolIsValid = true;

        if( true == is_null( $this->getFileExtensionId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extension_id', 'File extension id is required.' ) );
        }

        return $boolIsValid;
    }

	public function valFileName() {
	    $boolIsValid = true;
		if( true == is_null( $this->getFileName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File name is required.' ) );
			$boolIsValid = false;
		}
	    return $boolIsValid;
	}

	public function valFilePath() {
	    $boolIsValid = true;
		if( true == is_null( $this->getFilePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_path', 'File path is required.' ) );
			$boolIsValid = false;
		}
	    return $boolIsValid;
	}

	public function valFileData() {
	    $boolIsValid = true;

	    return $boolIsValid;
	}

	public function valFileSize() {
	    $boolIsValid = true;
		if( false == is_null( $this->getFilePath() ) && ( false == is_null( $this->getFileName() ) ) ) {
			$strFilePath = $this->getFilePath() . '/' . $this->getFileName();
			if( CFileIo::fileExists( $strFilePath ) ) {
				$intFileSize = CFileIo::getFileSize( $strFilePath );
				// check if file size > 20 MB ( 20971520 bytes )
				if( $intFileSize > 20971520 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', 'File size shouldn\'t be greater than 20 MB.' ) );
					$boolIsValid = false;
				}
			}
		}
	    return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
	            $boolIsValid &= $this->valFileName();
	            $boolIsValid &= $this->valFilePath();
	            $boolIsValid &= $this->valFileSize();
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valFileExtensionId();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
				// default case
            	$boolIsValid = true;
				break;
        }

        return $boolIsValid;
    }

	/**
	 * Other Functions
	 *
	 */

    public function buildStoragePath() {
    	$intInvoiceDate = ( true == is_null( $this->getCreatedOn() ) ) ? time() : strtotime( $this->getCreatedOn() );
    	return 'system_email_attachments/' . date( 'Y', $intInvoiceDate ) . '/' . date( 'm', $intInvoiceDate ) . '/' . date( 'd', $intInvoiceDate ) . '/';
	}

	public function getTempStoragePath() : string {
		return PATH_NON_BACKUP_MOUNTS_SYSTEM_EMAILS . date( 'Y-m-d', time() ) . '/attachments/';
	}

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
    	$boolIsValid = true;

    	$boolIsValid = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

    	if( true == $boolIsValid ) {
	    	if( true == file_exists( $this->getFullFilePath() . $this->getFileName() ) ) {
	        	unlink( $this->getFullFilePath() . $this->getFileName() );
	        }
    	}

     	return $boolIsValid;
    }

    public function bulkDelete( $arrobjEmailAttachments, $objDatabase ) {

    	if( false == CEmailAttachments::bulkDelete( $arrobjEmailAttachments, $objDatabase ) ) {
    		return false;
    	}

	    $objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
    	foreach( $arrobjEmailAttachments as $objEmailAttachment ) {

    		if( true == file_exists( $objEmailAttachment->getFullFilePath() . $objEmailAttachment->getFileName() ) ) {
    			unlink( $objEmailAttachment->getFullFilePath() . $objEmailAttachment->getFileName() );
    		} else {
			    $strAmazonKey		= $objEmailAttachment->getFullFilePath() . $objEmailAttachment->getFileName();
			    $objGetResponse = $objObjectStorageGateway->getObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, 'checkExists' => true ] );
			    if( true == $objGetResponse->isSuccessful() ) {
				    $objObjectStorageGateway->deleteObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, ] ); // remove file from Amazon S3
			    }
		    }
    	}

    	return true;
    }

	public function setEmailAttachmentFile( $strUploadFileArrayName, $objDatabase, $strAttachmentFile = NULL ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		if( true == is_null( $strAttachmentFile ) && false == empty( $_FILES[$strUploadFileArrayName]['name'] ) ) {

			$arrstrFileInfo = pathinfo( $_FILES[$strUploadFileArrayName]['name'] );

			$objFileExtension = \Psi\Eos\Email\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $objDatabase );

			if( false == valObj( $objFileExtension, 'CFileExtension' ) ) return false;

			$this->setFileExtensionId( $objFileExtension->getId() );
			$this->setTitle( $_FILES[$strUploadFileArrayName]['name'] );
			$this->setFileName( $_FILES[$strUploadFileArrayName]['name'] );
			$this->setFileSize( $_FILES[$strUploadFileArrayName]['size'] );
			$this->setFilePath( $this->buildStoragePath() ); // This should store in folders by company by date
		}

		if( false == is_null( $strAttachmentFile ) ) {

			$arrstrFileInfo = pathinfo( $strAttachmentFile );

			$objFileExtension = \Psi\Eos\Email\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $objDatabase );

			if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
				trigger_error( 'Invalid extension for file ' . $strAttachmentFile, E_USER_WARNING );
				return false;
			}

			$this->setFileExtensionId( $objFileExtension->getId() );
			if( false == $this->getIsInline() ) {
				$this->setTitle( $arrstrFileInfo['basename'] );
			}
			$this->setFileName( $arrstrFileInfo['basename'] );

			if( true == CFileIo::fileExists( $strAttachmentFile ) ) {
				$this->setFileSize( CFileIo::getFileSize( $strAttachmentFile ) );
			}

			$this->setFilePath( $this->buildStoragePath() ); // This should store in folders by company by date
		}

		return true;
    }

    public function fetchFileExtensionId( $strExtension, $objDatabase ) {
	    $objFileExtension = \Psi\Eos\Email\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

	    if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
		    return false;
	    }
	    return $objFileExtension->getId();
    }

	/**
	 * @param int $intCurrentUserId
	 * @param string $strUploadFileArrayName
	 * @param \CDatabase $objDatabase
	 * @param string $strAttachmentFilePath
	 *
	 * @return bool
	 */
    public function insert( $intCurrentUserId, $strUploadFileArrayName, $objDatabase = NULL, $strAttachmentFilePath = NULL, $strNewFileName = NULL, $boolUploadToNonBackupMounts = false ) {
		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );

		}

    	$boolIsValid            = true;

	    $strFileNameWithoutId   = $this->m_strFileName;

	    if( false == is_null( $this->getFileName() ) ) {
	    	if( false == is_null( $strAttachmentFilePath ) && false == is_null( $strNewFileName ) ) {
				$this->setFileName( CFileUpload::cleanFilename( $strNewFileName ) );
			} else {
				$this->setFileName( CFileUpload::cleanFilename( $this->getFileName() ) );
			}
    	}

    	if( true == is_null( $this->getId() ) || 0 == strlen( $this->getId() ) ) {
	    	$this->setId( $this->fetchNextId( $objDatabase ) );
    	}

   		$this->m_strFileName = $this->getId() . '_' . $this->m_strFileName;

   		switch( NULL ) {
   			default:
		       	if( true == is_null( $strAttachmentFilePath ) && false == is_null( $this->m_strFileName ) ) {
		    		$boolIsValid = $this->uploadFile( $strUploadFileArrayName, $objDatabase );
		    	}

		    	if( false == $boolIsValid ) {
					break;
				}

			    if( true == $boolUploadToNonBackupMounts ) {
				    $strFilePath = $this->getTempStoragePath();
			    } else {
				    $strFilePath = $this->getFullFilePath();
			    }

		    	if( false == is_dir( $strFilePath ) ) {
					if( false == CFileIo::createRecursiveDir( $strFilePath ) ) {
						trigger_error( 'Couldn\'t create directory(' . $strFilePath . ' ). ', E_USER_WARNING );
						$this->setFileName( $strFileNameWithoutId );
						$boolIsValid = false;
					}
				}

				if( false == $boolIsValid ) {
				    break;
			    }

				// We need to copy the attachement file from source to destination in case of system generated email attachments
				if( false == is_null( $strAttachmentFilePath ) ) {
					// Checking if this is from queued insertion (rabbitmq) where we already copied file without email_attachment.id appended to filename. If exists, then we will just rename it to append email_attachment.id.
					if( true == CFileIo::fileExists( $strFilePath . $strFileNameWithoutId ) ) {
                      if( false == CFileIo::renameFileOrDirectory( $strFilePath . $strFileNameWithoutId, $strFilePath . $this->m_strFileName ) ) {
							$this->setFileName( $strFileNameWithoutId );
							trigger_error( 'Couldn\'t rename file ' . $strFilePath . $strFileNameWithoutId . ' to ' . $strFilePath . $this->m_strFileName, E_USER_WARNING );
							$boolIsValid = false;
                      }

					} elseif( true == CFileIo::fileExists( $strAttachmentFilePath ) && false === \Psi\CStringService::singleton()->strstr( $strAttachmentFilePath, 'NonBackupMounts' ) && false == is_null( $strNewFileName ) ) {
						if( false == CFileIo::renameFileOrDirectory( $strAttachmentFilePath, $strFilePath . $this->m_strFileName ) ) {
							$this->setFileName( $strFileNameWithoutId );
							trigger_error( 'Couldn\'t rename file ' . $strAttachmentFilePath . ' to ' . $strFilePath . $this->m_strFileName, E_USER_WARNING );
							$boolIsValid = false;
						}

					} else {
						if( false == CFileIo::copyFile( $strAttachmentFilePath, $strFilePath . $this->m_strFileName ) ) {
							$this->setFileName( $strFileNameWithoutId );
							trigger_error( 'Couldn\'t copy file (' . $strAttachmentFilePath . ') to ' . $strFilePath . $this->m_strFileName . '.', E_USER_WARNING );
							$boolIsValid = false;
						}
					}
				}
				if( false == $boolIsValid ) {
					break;
				}

				$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase );
   		}
   		return $boolIsValid;
   	}

	public function directInsert( $intCurrentUserId, CDatabase $objDatabase ) {

		$boolIsValid            = true;

		if( true == is_null( $this->getId() ) || 0 == strlen( $this->getId() ) ) {
			$this->setId( $this->fetchNextId( $objDatabase ) );
		}

		switch( NULL ) {
			default:
				$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase );
		}
		return $boolIsValid;
	}

	public function uploadFile( $strUploadFileArrayName, $objDatabase ) {
		$boolIsValid = true;

		if( false == is_dir( $this->getFullFilePath() ) ) {
			if( false == CFileIo::createRecursiveDir( $this->getFullFilePath() ) ) {
				trigger_error( 'Couldn\'t create directory(' . $this->getFullFilePath() . ').', E_USER_ERROR );
				return false;
			}
		}

		$objUpload = new CFileUpload();

		$this->m_arrobjFileExtensions = \Psi\Eos\Email\CFileExtensions::createService()->fetchAllFileExtensions( $objDatabase );

   		foreach( $this->m_arrobjFileExtensions as $objFileExtension ) {
			$arrstrAllowedTypes[] = $objFileExtension->getExtension();
		}

   		$objUpload->setAcceptableTypes( $arrstrAllowedTypes );
		$objUpload->setOverwriteMode( CFileUpload::MODE_OVERWRITE );

		$_FILES[$strUploadFileArrayName]['name'] = $this->getFileName();

		$strFileName = $objUpload->upload( $strUploadFileArrayName, $this->getFullFilePath() );
		$strError = $objUpload->getError();
		if( true == empty( $strError ) ) {
				$this->setFileName( $strFileName );
				$this->m_boolTaskAttachmentIsWrittenToFileSystem = true;
		} else {
	    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Attachment', $objUpload->getError() ) );
	    		trigger_error( $objUpload->getError(), E_USER_WARNING );
	    		$boolIsValid = false;
	    }

		if( false == is_string( $strFileName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Attachment', 'Invalid file specified for attachement.' ) );
			trigger_error( 'Invalid file specified for attachement :' . $_FILES[$strUploadFileArrayName]['name'] . ' (' . $this->getFullFilePath() . ').', E_USER_WARNING );
	    	$boolIsValid = false;
	    }

		return $boolIsValid;
	}

	public function uploadEmailAttachment( $objEmailAttachment, $boolUploadToNonBackupMounts = false ) {

		if( true == $boolUploadToNonBackupMounts ) {
			$strDestinationPath = $this->getTempStoragePath();
		} else {
			$strDestinationPath = PATH_MOUNTS_SYSTEM_EMAIL_ATTACHMENTS . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
		}

		// Adding unique string before filename so that we will be having unique attachment for every system email insert.
		$strDestinationFileName = getUniqueString() . '_' . $objEmailAttachment->getFileName();

		if( false == is_dir( $strDestinationPath ) ) {
			if( false == CFileIo::createRecursiveDir( $strDestinationPath ) ) {
				$this->addErrorMsg( "couldn't create directory at " . $strDestinationPath );

				return false;
			}
		}

		if( false == CFileIo::copyFile( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName(), $strDestinationPath . $strDestinationFileName ) ) {
			$this->addErrorMsg( "can't copy file from" . $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() . 'to' . $strDestinationPath . $strDestinationFileName );
			trigger_error( 'Failed to copy from ' . $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() . ' to ' . $strDestinationPath . $strDestinationFileName, E_USER_WARNING );
			return false;
		}
		$objEmailAttachment->setFilePath( $strDestinationPath );
		$objEmailAttachment->setFileName( $strDestinationFileName );

		return true;
	}

	public function loadEmailAttachmentContentFromS3( $boolReturnFilePath = false ) {
		$strData = '';
		$strSourcePath = $this->getFilePath() . $this->getFileName();

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
		$arrstrOptions = [
			'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET,
			'Key'       => $strSourcePath
		];
		if( $boolReturnFilePath ) {
			$arrstrOptions['outputFile'] = 'temp';
		}
		$arrobjGetResponse = $objObjectStorageGateway->getObject( $arrstrOptions );

		if( true == $arrobjGetResponse->isSuccessful() ) {
			$strData = ( $boolReturnFilePath ) ? $arrobjGetResponse['outputFile'] : $arrobjGetResponse['data'];
		}
		return $strData;
	}

	public function toArray() {
		$arrmixEmailAttachment = parent::toArray();
		$arrmixEmailAttachment['email_attachment_details'] = $this->getEmailAttachmentDetails();
		return $arrmixEmailAttachment;
	}

}
?>
