<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailRecipients
 * Do not add any new functions to this class.
 */

class CSystemEmailRecipients extends CBaseSystemEmailRecipients {

	public static function fetchSystemEmailRecipientsEmailAddressBySystemEmailId( $intSystemEmailId, $objEmailDatabase ) {

		$strSql = 'SELECT id, email_address, merge_fields FROM system_email_recipients WHERE system_email_id = ' . ( int ) $intSystemEmailId;

		$arrmixSystemEmailRecipients = fetchData( $strSql, $objEmailDatabase );

		return ( false == is_null( $arrmixSystemEmailRecipients ) ) ? $arrmixSystemEmailRecipients : NULL;

	}

	public static function fetchSystemEmailRecipientsEventsBySystemEmailId( $intPageNo, $intPageSize, $intSystemEmailId, $objEmailDatabase, $boolCountOnly = false ) {

		if( false == valId( $intSystemEmailId ) ) return NULL;

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT count(se.id) FROM system_email_recipients se WHERE system_email_id = ' . ( int ) $intSystemEmailId;

			$arrintResponse = fetchData( $strSql, $objEmailDatabase );

			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

		} else {

			$strSql = 'SELECT
							ser.id,
							ser.email_address,
							ser.recipient_type_id,
							ser.merge_fields,
							ee.email_event_type_id,
							ee.response,
							ee.updated_on,
							se.subject
						FROM
							system_emails se
							JOIN system_email_recipients ser ON ( se.id = ser.system_email_id )
							LEFT JOIN email_events ee ON ( ee.system_email_id = se.id AND ser.id = ee.system_email_recipient_id )
						WHERE
							ser.system_email_id = ' . ( int ) $intSystemEmailId . '
						ORDER BY
							ser.email_address
						OFFSET
							' . ( int ) $intOffset . '
						LIMIT
							' . ( int ) $intLimit;

		    return fetchData( $strSql, $objEmailDatabase );

		}

	}

	public static function fetchSystemEmailRecipientBySystemEmailIdByRecipientTypeIdByReferenceNumber( $intSystemEmailId, $intRecipientTypeId, $intReferenceNumber, $objEmailDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						system_email_recipients
					WHERE
						system_email_id = ' . ( int ) $intSystemEmailId . '
						AND recipient_type_id = ' . ( int ) $intRecipientTypeId . '
						AND reference_number = ' . ( int ) $intReferenceNumber . '
						LIMIT 1';

		return self::fetchSystemEmailRecipient( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientBySystemEmailIdByReferenceNumber( $intSystemEmailId, $intReferenceNumber, $objEmailDatabase ) {

		if( false == valId( $intSystemEmailId ) || false == valId( $intReferenceNumber ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						system_email_recipients
					WHERE
						system_email_id = ' . ( int ) $intSystemEmailId . '
						AND reference_number = ' . ( int ) $intReferenceNumber . '
						LIMIT 1';

		return self::fetchSystemEmailRecipient( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientBySystemEmailIdByRecipientTypeIdByReferenceNumbers( $intSystemEmailId, $intRecipientTypeId, $arrintReferenceNumbers, $objEmailDatabase ) {

		if( false == valArr( $arrintReferenceNumbers ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						system_email_recipients
					WHERE
						system_email_id = ' . ( int ) $intSystemEmailId . '
						AND recipient_type_id = ' . ( int ) $intRecipientTypeId . '
						AND reference_number IN ( ' . implode( ',', $arrintReferenceNumbers ) . ' )';

		return self::fetchSystemEmailRecipients( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientBySystemEmailIdByByReferenceNumbers( $intSystemEmailId, $arrintReferenceNumbers, $objEmailDatabase ) {

		if( false == valArr( $arrintReferenceNumbers ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						system_email_recipients
					WHERE
						system_email_id = ' . ( int ) $intSystemEmailId . '
						AND reference_number IN ( ' . implode( ',', $arrintReferenceNumbers ) . ' )';

		return self::fetchSystemEmailRecipients( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientsByIdsByCid( $arrintSystemEmailRecipientIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintSystemEmailRecipientIds ) ) return NULL;

		$strSql	= 'SELECT
					    ser.*
					FROM
					    system_emails se
						JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid )
					WHERE
						ser.cid = ' . ( int ) $intCid . '
					    AND ser.id IN ( ' . implode( ',', $arrintSystemEmailRecipientIds ) . ' )';

		return self::fetchSystemEmailRecipients( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailRecipientByIdByCid( $intSystemEmailRecipientId, $intCid, $objDatabase ) {

		if ( false == valId( $intSystemEmailRecipientId ) || false == valId( $intCid ) ) return NULL;

		$strSql	= 'SELECT
					    ser.*
					FROM
					    system_emails se
						JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid )
					WHERE
						ser.cid = ' . ( int ) $intCid . '
					    AND ser.id = ' . $intSystemEmailRecipientId;

		return self::fetchSystemEmailRecipient( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailRecipientsForEventsByIdsByCid( $arrintSystemEmailRecipientIds, $intCid, $objEmailDatabase ) {

		if( false == valArr( $arrintSystemEmailRecipientIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						system_email_recipients
					WHERE
						id IN ( ' . implode( ',', $arrintSystemEmailRecipientIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return self::fetchSystemEmailRecipients( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientIdsBySystemEmailIdsByCid( $arrintSystemEmailIds, $intCid, $objEmailDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql = '
					SET STATEMENT_TIMEOUT TO \'5s\';
					SELECT
						id,
						system_email_id,
						reference_number
					FROM
						system_email_recipients
					WHERE
						system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrmixRecords = fetchData( $strSql, $objEmailDatabase );

		if( false == valArr( $arrmixRecords ) ) {
			return NULL;
		}

		$arrintSystemEmailRecipientIds = [];
		foreach( $arrmixRecords as $arrmixRecord ) {
			$arrintSystemEmailRecipientIds[$arrmixRecord['system_email_id']][$arrmixRecord['reference_number']] = $arrmixRecord['id'];
		}

		return $arrintSystemEmailRecipientIds;
	}

	public static function fetchSystemEmailRecipientsBySystemEmailIdsByReferenceNumbersByCid( $arrintSystemEmailIds, $arrintReferenceNumbers, $intCid, $objEmailDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) || false == valArr( $arrintReferenceNumbers ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						system_email_recipients
					WHERE
					cid = ' . ( int ) $intCid . '
					AND reference_number IN ( ' . implode( ',', $arrintReferenceNumbers ) . ' )
					AND system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )';

		return self::fetchSystemEmailRecipients( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailRecipientsBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {

		if ( false == valId( $intSystemEmailId ) || false == valId( $intCid ) ) return NULL;

		$strSql	= 'SELECT
					    ser.*
					FROM
					    system_emails se
						JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid )
					WHERE
						ser.cid = ' . ( int ) $intCid . '
					    AND ser.system_email_id = ' . $intSystemEmailId;

		return self::fetchSystemEmailRecipients( $strSql, $objDatabase );
	}

}
?>