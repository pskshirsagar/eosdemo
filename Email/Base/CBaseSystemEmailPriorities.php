<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailPriorities
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailPriorities extends CEosPluralBase {

	/**
	 * @return CSystemEmailPriority[]
	 */
	public static function fetchSystemEmailPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailPriority', $objDatabase );
	}

	/**
	 * @return CSystemEmailPriority
	 */
	public static function fetchSystemEmailPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailPriority', $objDatabase );
	}

	public static function fetchSystemEmailPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_priorities', $objDatabase );
	}

	public static function fetchSystemEmailPriorityById( $intId, $objDatabase ) {
		return self::fetchSystemEmailPriority( sprintf( 'SELECT * FROM system_email_priorities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>