<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailEventLinks
 * Do not add any new functions to this class.
 */

class CBaseEmailEventLinks extends CEosPluralBase {

	/**
	 * @return CEmailEventLink[]
	 */
	public static function fetchEmailEventLinks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailEventLink', $objDatabase );
	}

	/**
	 * @return CEmailEventLink
	 */
	public static function fetchEmailEventLink( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailEventLink', $objDatabase );
	}

	public static function fetchEmailEventLinkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_event_links', $objDatabase );
	}

	public static function fetchEmailEventLinkById( $intId, $objDatabase ) {
		return self::fetchEmailEventLink( sprintf( 'SELECT * FROM email_event_links WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailEventLinksByEmailEventId( $intEmailEventId, $objDatabase ) {
		return self::fetchEmailEventLinks( sprintf( 'SELECT * FROM email_event_links WHERE email_event_id = %d', ( int ) $intEmailEventId ), $objDatabase );
	}

}
?>