<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmails
 * Do not add any new functions to this class.
 */

class CBaseSystemEmails extends CEosPluralBase {

	/**
	 * @return CSystemEmail[]
	 */
	public static function fetchSystemEmails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemEmail::class, $objDatabase );
	}

	/**
	 * @return CSystemEmail
	 */
	public static function fetchSystemEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemEmail::class, $objDatabase );
	}

	public static function fetchSystemEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_emails', $objDatabase );
	}

	public static function fetchSystemEmailById( $intId, $objDatabase ) {
		return self::fetchSystemEmail( sprintf( 'SELECT * FROM system_emails WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSystemEmailsBySystemEmailTypeId( $intSystemEmailTypeId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE system_email_type_id = %d', $intSystemEmailTypeId ), $objDatabase );
	}

	public static function fetchSystemEmailsBySystemEmailPriorityId( $intSystemEmailPriorityId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE system_email_priority_id = %d', $intSystemEmailPriorityId ), $objDatabase );
	}

	public static function fetchSystemEmailsByCid( $intCid, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSystemEmailsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE account_id = %d', $intAccountId ), $objDatabase );
	}

	public static function fetchSystemEmailsByEmailServiceProviderId( $intEmailServiceProviderId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE email_service_provider_id = %d', $intEmailServiceProviderId ), $objDatabase );
	}

	public static function fetchSystemEmailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchSystemEmailsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchSystemEmailsByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE company_employee_id = %d', $intCompanyEmployeeId ), $objDatabase );
	}

	public static function fetchSystemEmailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchSystemEmailsByScheduledEmailId( $intScheduledEmailId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE scheduled_email_id = %d', $intScheduledEmailId ), $objDatabase );
	}

	public static function fetchSystemEmailsByEventId( $intEventId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE event_id = %d', $intEventId ), $objDatabase );
	}

	public static function fetchSystemEmailsByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE applicant_id = %d', $intApplicantId ), $objDatabase );
	}

	public static function fetchSystemEmailsByScheduledEmailTransmissionId( $intScheduledEmailTransmissionId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE scheduled_email_transmission_id = %d', $intScheduledEmailTransmissionId ), $objDatabase );
	}

	public static function fetchSystemEmailsByMassEmailId( $intMassEmailId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE mass_email_id = %d', $intMassEmailId ), $objDatabase );
	}

	public static function fetchSystemEmailsByScheduledTaskLogId( $intScheduledTaskLogId, $objDatabase ) {
		return self::fetchSystemEmails( sprintf( 'SELECT * FROM system_emails WHERE scheduled_task_log_id = %d', $intScheduledTaskLogId ), $objDatabase );
	}

}
?>