<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailRecipients
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailRecipients extends CEosPluralBase {

	/**
	 * @return CSystemEmailRecipient[]
	 */
	public static function fetchSystemEmailRecipients( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailRecipient', $objDatabase );
	}

	/**
	 * @return CSystemEmailRecipient
	 */
	public static function fetchSystemEmailRecipient( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailRecipient', $objDatabase );
	}

	public static function fetchSystemEmailRecipientCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_recipients', $objDatabase );
	}

	public static function fetchSystemEmailRecipientById( $intId, $objDatabase ) {
		return self::fetchSystemEmailRecipient( sprintf( 'SELECT * FROM system_email_recipients WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemEmailRecipientsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchSystemEmailRecipients( sprintf( 'SELECT * FROM system_email_recipients WHERE system_email_id = %d', ( int ) $intSystemEmailId ), $objDatabase );
	}

	public static function fetchSystemEmailRecipientsByRecipientTypeId( $intRecipientTypeId, $objDatabase ) {
		return self::fetchSystemEmailRecipients( sprintf( 'SELECT * FROM system_email_recipients WHERE recipient_type_id = %d', ( int ) $intRecipientTypeId ), $objDatabase );
	}

	public static function fetchSystemEmailRecipientsByCid( $intCid, $objDatabase ) {
		return self::fetchSystemEmailRecipients( sprintf( 'SELECT * FROM system_email_recipients WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>