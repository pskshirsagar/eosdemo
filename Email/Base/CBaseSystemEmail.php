<?php

class CBaseSystemEmail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.system_emails';

	protected $m_intId;
	protected $m_intSystemEmailTypeId;
	protected $m_intSystemEmailPriorityId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intEmailServiceProviderId;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intCompanyEmployeeId;
	protected $m_intEmployeeId;
	protected $m_intScheduledEmailId;
	protected $m_intEventId;
	protected $m_intApplicantId;
	protected $m_intScheduledEmailTransmissionId;
	protected $m_intMassEmailId;
	protected $m_strToEmailAddress;
	protected $m_strCcEmailAddress;
	protected $m_strBccEmailAddress;
	protected $m_strFromEmailAddress;
	protected $m_strReplyToEmailAddress;
	protected $m_strSubject;
	protected $m_strScheduledSendDatetime;
	protected $m_strSentOn;
	protected $m_strFailedOn;
	protected $m_strAccessedOn;
	protected $m_intLockSequenceNumber;
	protected $m_strQueuedOn;
	protected $m_intIsQueued;
	protected $m_intIsSuspended;
	protected $m_intIsSelfDestruct;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intScheduledTaskLogId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intSystemEmailPriorityId = '3';
		$this->m_intEmailServiceProviderId = '1';
		$this->m_strFromEmailAddress = 'system@entrata.com';
		$this->m_strScheduledSendDatetime = 'now()';
		$this->m_intIsQueued = '0';
		$this->m_intIsSuspended = '0';
		$this->m_intIsSelfDestruct = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailTypeId', trim( $arrValues['system_email_type_id'] ) ); elseif( isset( $arrValues['system_email_type_id'] ) ) $this->setSystemEmailTypeId( $arrValues['system_email_type_id'] );
		if( isset( $arrValues['system_email_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailPriorityId', trim( $arrValues['system_email_priority_id'] ) ); elseif( isset( $arrValues['system_email_priority_id'] ) ) $this->setSystemEmailPriorityId( $arrValues['system_email_priority_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['email_service_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailServiceProviderId', trim( $arrValues['email_service_provider_id'] ) ); elseif( isset( $arrValues['email_service_provider_id'] ) ) $this->setEmailServiceProviderId( $arrValues['email_service_provider_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailId', trim( $arrValues['scheduled_email_id'] ) ); elseif( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['scheduled_email_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailTransmissionId', trim( $arrValues['scheduled_email_transmission_id'] ) ); elseif( isset( $arrValues['scheduled_email_transmission_id'] ) ) $this->setScheduledEmailTransmissionId( $arrValues['scheduled_email_transmission_id'] );
		if( isset( $arrValues['mass_email_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailId', trim( $arrValues['mass_email_id'] ) ); elseif( isset( $arrValues['mass_email_id'] ) ) $this->setMassEmailId( $arrValues['mass_email_id'] );
		if( isset( $arrValues['to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strToEmailAddress', trim( $arrValues['to_email_address'] ) ); elseif( isset( $arrValues['to_email_address'] ) ) $this->setToEmailAddress( $arrValues['to_email_address'] );
		if( isset( $arrValues['cc_email_address'] ) && $boolDirectSet ) $this->set( 'm_strCcEmailAddress', trim( $arrValues['cc_email_address'] ) ); elseif( isset( $arrValues['cc_email_address'] ) ) $this->setCcEmailAddress( $arrValues['cc_email_address'] );
		if( isset( $arrValues['bcc_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBccEmailAddress', trim( $arrValues['bcc_email_address'] ) ); elseif( isset( $arrValues['bcc_email_address'] ) ) $this->setBccEmailAddress( $arrValues['bcc_email_address'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( $arrValues['from_email_address'] ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( $arrValues['from_email_address'] );
		if( isset( $arrValues['reply_to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strReplyToEmailAddress', trim( $arrValues['reply_to_email_address'] ) ); elseif( isset( $arrValues['reply_to_email_address'] ) ) $this->setReplyToEmailAddress( $arrValues['reply_to_email_address'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( $arrValues['subject'] ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( $arrValues['subject'] );
		if( isset( $arrValues['scheduled_send_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledSendDatetime', trim( $arrValues['scheduled_send_datetime'] ) ); elseif( isset( $arrValues['scheduled_send_datetime'] ) ) $this->setScheduledSendDatetime( $arrValues['scheduled_send_datetime'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['accessed_on'] ) && $boolDirectSet ) $this->set( 'm_strAccessedOn', trim( $arrValues['accessed_on'] ) ); elseif( isset( $arrValues['accessed_on'] ) ) $this->setAccessedOn( $arrValues['accessed_on'] );
		if( isset( $arrValues['lock_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intLockSequenceNumber', trim( $arrValues['lock_sequence_number'] ) ); elseif( isset( $arrValues['lock_sequence_number'] ) ) $this->setLockSequenceNumber( $arrValues['lock_sequence_number'] );
		if( isset( $arrValues['queued_on'] ) && $boolDirectSet ) $this->set( 'm_strQueuedOn', trim( $arrValues['queued_on'] ) ); elseif( isset( $arrValues['queued_on'] ) ) $this->setQueuedOn( $arrValues['queued_on'] );
		if( isset( $arrValues['is_queued'] ) && $boolDirectSet ) $this->set( 'm_intIsQueued', trim( $arrValues['is_queued'] ) ); elseif( isset( $arrValues['is_queued'] ) ) $this->setIsQueued( $arrValues['is_queued'] );
		if( isset( $arrValues['is_suspended'] ) && $boolDirectSet ) $this->set( 'm_intIsSuspended', trim( $arrValues['is_suspended'] ) ); elseif( isset( $arrValues['is_suspended'] ) ) $this->setIsSuspended( $arrValues['is_suspended'] );
		if( isset( $arrValues['is_self_destruct'] ) && $boolDirectSet ) $this->set( 'm_intIsSelfDestruct', trim( $arrValues['is_self_destruct'] ) ); elseif( isset( $arrValues['is_self_destruct'] ) ) $this->setIsSelfDestruct( $arrValues['is_self_destruct'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['scheduled_task_log_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledTaskLogId', trim( $arrValues['scheduled_task_log_id'] ) ); elseif( isset( $arrValues['scheduled_task_log_id'] ) ) $this->setScheduledTaskLogId( $arrValues['scheduled_task_log_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->set( 'm_intSystemEmailTypeId', CStrings::strToIntDef( $intSystemEmailTypeId, NULL, false ) );
	}

	public function getSystemEmailTypeId() {
		return $this->m_intSystemEmailTypeId;
	}

	public function sqlSystemEmailTypeId() {
		return ( true == isset( $this->m_intSystemEmailTypeId ) ) ? ( string ) $this->m_intSystemEmailTypeId : 'NULL';
	}

	public function setSystemEmailPriorityId( $intSystemEmailPriorityId ) {
		$this->set( 'm_intSystemEmailPriorityId', CStrings::strToIntDef( $intSystemEmailPriorityId, NULL, false ) );
	}

	public function getSystemEmailPriorityId() {
		return $this->m_intSystemEmailPriorityId;
	}

	public function sqlSystemEmailPriorityId() {
		return ( true == isset( $this->m_intSystemEmailPriorityId ) ) ? ( string ) $this->m_intSystemEmailPriorityId : '3';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setEmailServiceProviderId( $intEmailServiceProviderId ) {
		$this->set( 'm_intEmailServiceProviderId', CStrings::strToIntDef( $intEmailServiceProviderId, NULL, false ) );
	}

	public function getEmailServiceProviderId() {
		return $this->m_intEmailServiceProviderId;
	}

	public function sqlEmailServiceProviderId() {
		return ( true == isset( $this->m_intEmailServiceProviderId ) ) ? ( string ) $this->m_intEmailServiceProviderId : '1';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->set( 'm_intScheduledEmailId', CStrings::strToIntDef( $intScheduledEmailId, NULL, false ) );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setScheduledEmailTransmissionId( $intScheduledEmailTransmissionId ) {
		$this->set( 'm_intScheduledEmailTransmissionId', CStrings::strToIntDef( $intScheduledEmailTransmissionId, NULL, false ) );
	}

	public function getScheduledEmailTransmissionId() {
		return $this->m_intScheduledEmailTransmissionId;
	}

	public function sqlScheduledEmailTransmissionId() {
		return ( true == isset( $this->m_intScheduledEmailTransmissionId ) ) ? ( string ) $this->m_intScheduledEmailTransmissionId : 'NULL';
	}

	public function setMassEmailId( $intMassEmailId ) {
		$this->set( 'm_intMassEmailId', CStrings::strToIntDef( $intMassEmailId, NULL, false ) );
	}

	public function getMassEmailId() {
		return $this->m_intMassEmailId;
	}

	public function sqlMassEmailId() {
		return ( true == isset( $this->m_intMassEmailId ) ) ? ( string ) $this->m_intMassEmailId : 'NULL';
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->set( 'm_strToEmailAddress', CStrings::strTrimDef( $strToEmailAddress, -1, NULL, true ) );
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function sqlToEmailAddress() {
		return ( true == isset( $this->m_strToEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strToEmailAddress ) : '\'' . addslashes( $this->m_strToEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setCcEmailAddress( $strCcEmailAddress ) {
		$this->set( 'm_strCcEmailAddress', CStrings::strTrimDef( $strCcEmailAddress, -1, NULL, true ) );
	}

	public function getCcEmailAddress() {
		return $this->m_strCcEmailAddress;
	}

	public function sqlCcEmailAddress() {
		return ( true == isset( $this->m_strCcEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcEmailAddress ) : '\'' . addslashes( $this->m_strCcEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setBccEmailAddress( $strBccEmailAddress ) {
		$this->set( 'm_strBccEmailAddress', CStrings::strTrimDef( $strBccEmailAddress, -1, NULL, true ) );
	}

	public function getBccEmailAddress() {
		return $this->m_strBccEmailAddress;
	}

	public function sqlBccEmailAddress() {
		return ( true == isset( $this->m_strBccEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBccEmailAddress ) : '\'' . addslashes( $this->m_strBccEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 240, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFromEmailAddress ) : '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' ) : '\'system@entrata.com\'';
	}

	public function setReplyToEmailAddress( $strReplyToEmailAddress ) {
		$this->set( 'm_strReplyToEmailAddress', CStrings::strTrimDef( $strReplyToEmailAddress, 240, NULL, true ) );
	}

	public function getReplyToEmailAddress() {
		return $this->m_strReplyToEmailAddress;
	}

	public function sqlReplyToEmailAddress() {
		return ( true == isset( $this->m_strReplyToEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReplyToEmailAddress ) : '\'' . addslashes( $this->m_strReplyToEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, -1, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubject ) : '\'' . addslashes( $this->m_strSubject ) . '\'' ) : 'NULL';
	}

	public function setScheduledSendDatetime( $strScheduledSendDatetime ) {
		$this->set( 'm_strScheduledSendDatetime', CStrings::strTrimDef( $strScheduledSendDatetime, -1, NULL, true ) );
	}

	public function getScheduledSendDatetime() {
		return $this->m_strScheduledSendDatetime;
	}

	public function sqlScheduledSendDatetime() {
		return ( true == isset( $this->m_strScheduledSendDatetime ) ) ? '\'' . $this->m_strScheduledSendDatetime . '\'' : 'NOW()';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setAccessedOn( $strAccessedOn ) {
		$this->set( 'm_strAccessedOn', CStrings::strTrimDef( $strAccessedOn, -1, NULL, true ) );
	}

	public function getAccessedOn() {
		return $this->m_strAccessedOn;
	}

	public function sqlAccessedOn() {
		return ( true == isset( $this->m_strAccessedOn ) ) ? '\'' . $this->m_strAccessedOn . '\'' : 'NULL';
	}

	public function setLockSequenceNumber( $intLockSequenceNumber ) {
		$this->set( 'm_intLockSequenceNumber', CStrings::strToIntDef( $intLockSequenceNumber, NULL, false ) );
	}

	public function getLockSequenceNumber() {
		return $this->m_intLockSequenceNumber;
	}

	public function sqlLockSequenceNumber() {
		return ( true == isset( $this->m_intLockSequenceNumber ) ) ? ( string ) $this->m_intLockSequenceNumber : 'NULL';
	}

	public function setQueuedOn( $strQueuedOn ) {
		$this->set( 'm_strQueuedOn', CStrings::strTrimDef( $strQueuedOn, -1, NULL, true ) );
	}

	public function getQueuedOn() {
		return $this->m_strQueuedOn;
	}

	public function sqlQueuedOn() {
		return ( true == isset( $this->m_strQueuedOn ) ) ? '\'' . $this->m_strQueuedOn . '\'' : 'NULL';
	}

	public function setIsQueued( $intIsQueued ) {
		$this->set( 'm_intIsQueued', CStrings::strToIntDef( $intIsQueued, NULL, false ) );
	}

	public function getIsQueued() {
		return $this->m_intIsQueued;
	}

	public function sqlIsQueued() {
		return ( true == isset( $this->m_intIsQueued ) ) ? ( string ) $this->m_intIsQueued : '0';
	}

	public function setIsSuspended( $intIsSuspended ) {
		$this->set( 'm_intIsSuspended', CStrings::strToIntDef( $intIsSuspended, NULL, false ) );
	}

	public function getIsSuspended() {
		return $this->m_intIsSuspended;
	}

	public function sqlIsSuspended() {
		return ( true == isset( $this->m_intIsSuspended ) ) ? ( string ) $this->m_intIsSuspended : '0';
	}

	public function setIsSelfDestruct( $intIsSelfDestruct ) {
		$this->set( 'm_intIsSelfDestruct', CStrings::strToIntDef( $intIsSelfDestruct, NULL, false ) );
	}

	public function getIsSelfDestruct() {
		return $this->m_intIsSelfDestruct;
	}

	public function sqlIsSelfDestruct() {
		return ( true == isset( $this->m_intIsSelfDestruct ) ) ? ( string ) $this->m_intIsSelfDestruct : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setScheduledTaskLogId( $intScheduledTaskLogId ) {
		$this->set( 'm_intScheduledTaskLogId', CStrings::strToIntDef( $intScheduledTaskLogId, NULL, false ) );
	}

	public function getScheduledTaskLogId() {
		return $this->m_intScheduledTaskLogId;
	}

	public function sqlScheduledTaskLogId() {
		return ( true == isset( $this->m_intScheduledTaskLogId ) ) ? ( string ) $this->m_intScheduledTaskLogId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_email_type_id, system_email_priority_id, cid, account_id, email_service_provider_id, property_id, customer_id, company_employee_id, employee_id, scheduled_email_id, event_id, applicant_id, scheduled_email_transmission_id, mass_email_id, to_email_address, cc_email_address, bcc_email_address, from_email_address, reply_to_email_address, subject, scheduled_send_datetime, sent_on, failed_on, accessed_on, lock_sequence_number, queued_on, is_queued, is_suspended, is_self_destruct, updated_by, updated_on, created_by, created_on, scheduled_task_log_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSystemEmailTypeId() . ', ' .
						$this->sqlSystemEmailPriorityId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlEmailServiceProviderId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlScheduledEmailId() . ', ' .
						$this->sqlEventId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlScheduledEmailTransmissionId() . ', ' .
						$this->sqlMassEmailId() . ', ' .
						$this->sqlToEmailAddress() . ', ' .
						$this->sqlCcEmailAddress() . ', ' .
						$this->sqlBccEmailAddress() . ', ' .
						$this->sqlFromEmailAddress() . ', ' .
						$this->sqlReplyToEmailAddress() . ', ' .
						$this->sqlSubject() . ', ' .
						$this->sqlScheduledSendDatetime() . ', ' .
						$this->sqlSentOn() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlAccessedOn() . ', ' .
						$this->sqlLockSequenceNumber() . ', ' .
						$this->sqlQueuedOn() . ', ' .
						$this->sqlIsQueued() . ', ' .
						$this->sqlIsSuspended() . ', ' .
						$this->sqlIsSelfDestruct() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlScheduledTaskLogId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_priority_id = ' . $this->sqlSystemEmailPriorityId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_priority_id = ' . $this->sqlSystemEmailPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId(). ',' ; } elseif( true == array_key_exists( 'EmailServiceProviderId', $this->getChangedColumns() ) ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId(). ',' ; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_transmission_id = ' . $this->sqlScheduledEmailTransmissionId(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_transmission_id = ' . $this->sqlScheduledEmailTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId(). ',' ; } elseif( true == array_key_exists( 'MassEmailId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_email_address = ' . $this->sqlCcEmailAddress(). ',' ; } elseif( true == array_key_exists( 'CcEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' cc_email_address = ' . $this->sqlCcEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bcc_email_address = ' . $this->sqlBccEmailAddress(). ',' ; } elseif( true == array_key_exists( 'BccEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' bcc_email_address = ' . $this->sqlBccEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress(). ',' ; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reply_to_email_address = ' . $this->sqlReplyToEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ReplyToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' reply_to_email_address = ' . $this->sqlReplyToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject(). ',' ; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledSendDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn(). ',' ; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn(). ',' ; } elseif( true == array_key_exists( 'AccessedOn', $this->getChangedColumns() ) ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber(). ',' ; } elseif( true == array_key_exists( 'LockSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queued_on = ' . $this->sqlQueuedOn(). ',' ; } elseif( true == array_key_exists( 'QueuedOn', $this->getChangedColumns() ) ) { $strSql .= ' queued_on = ' . $this->sqlQueuedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_queued = ' . $this->sqlIsQueued(). ',' ; } elseif( true == array_key_exists( 'IsQueued', $this->getChangedColumns() ) ) { $strSql .= ' is_queued = ' . $this->sqlIsQueued() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_suspended = ' . $this->sqlIsSuspended(). ',' ; } elseif( true == array_key_exists( 'IsSuspended', $this->getChangedColumns() ) ) { $strSql .= ' is_suspended = ' . $this->sqlIsSuspended() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_self_destruct = ' . $this->sqlIsSelfDestruct(). ',' ; } elseif( true == array_key_exists( 'IsSelfDestruct', $this->getChangedColumns() ) ) { $strSql .= ' is_self_destruct = ' . $this->sqlIsSelfDestruct() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_task_log_id = ' . $this->sqlScheduledTaskLogId(). ',' ; } elseif( true == array_key_exists( 'ScheduledTaskLogId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_task_log_id = ' . $this->sqlScheduledTaskLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_email_type_id' => $this->getSystemEmailTypeId(),
			'system_email_priority_id' => $this->getSystemEmailPriorityId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'email_service_provider_id' => $this->getEmailServiceProviderId(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'event_id' => $this->getEventId(),
			'applicant_id' => $this->getApplicantId(),
			'scheduled_email_transmission_id' => $this->getScheduledEmailTransmissionId(),
			'mass_email_id' => $this->getMassEmailId(),
			'to_email_address' => $this->getToEmailAddress(),
			'cc_email_address' => $this->getCcEmailAddress(),
			'bcc_email_address' => $this->getBccEmailAddress(),
			'from_email_address' => $this->getFromEmailAddress(),
			'reply_to_email_address' => $this->getReplyToEmailAddress(),
			'subject' => $this->getSubject(),
			'scheduled_send_datetime' => $this->getScheduledSendDatetime(),
			'sent_on' => $this->getSentOn(),
			'failed_on' => $this->getFailedOn(),
			'accessed_on' => $this->getAccessedOn(),
			'lock_sequence_number' => $this->getLockSequenceNumber(),
			'queued_on' => $this->getQueuedOn(),
			'is_queued' => $this->getIsQueued(),
			'is_suspended' => $this->getIsSuspended(),
			'is_self_destruct' => $this->getIsSelfDestruct(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'scheduled_task_log_id' => $this->getScheduledTaskLogId(),
			'details' => $this->getDetails()
		);
	}

}
?>