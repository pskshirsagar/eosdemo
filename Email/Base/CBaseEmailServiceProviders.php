<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailServiceProviders
 * Do not add any new functions to this class.
 */

class CBaseEmailServiceProviders extends CEosPluralBase {

	/**
	 * @return CEmailServiceProvider[]
	 */
	public static function fetchEmailServiceProviders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailServiceProvider', $objDatabase );
	}

	/**
	 * @return CEmailServiceProvider
	 */
	public static function fetchEmailServiceProvider( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailServiceProvider', $objDatabase );
	}

	public static function fetchEmailServiceProviderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_service_providers', $objDatabase );
	}

	public static function fetchEmailServiceProviderById( $intId, $objDatabase ) {
		return self::fetchEmailServiceProvider( sprintf( 'SELECT * FROM email_service_providers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>