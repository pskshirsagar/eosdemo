<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CDomainBlocks
 * Do not add any new functions to this class.
 */

class CBaseDomainBlocks extends CEosPluralBase {

	/**
	 * @return CDomainBlock[]
	 */
	public static function fetchDomainBlocks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDomainBlock', $objDatabase );
	}

	/**
	 * @return CDomainBlock
	 */
	public static function fetchDomainBlock( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDomainBlock', $objDatabase );
	}

	public static function fetchDomainBlockCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'domain_blocks', $objDatabase );
	}

	public static function fetchDomainBlockById( $intId, $objDatabase ) {
		return self::fetchDomainBlock( sprintf( 'SELECT * FROM domain_blocks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDomainBlocksByCid( $intCid, $objDatabase ) {
		return self::fetchDomainBlocks( sprintf( 'SELECT * FROM domain_blocks WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDomainBlocksByEmailBlockTypeId( $intEmailBlockTypeId, $objDatabase ) {
		return self::fetchDomainBlocks( sprintf( 'SELECT * FROM domain_blocks WHERE email_block_type_id = %d', ( int ) $intEmailBlockTypeId ), $objDatabase );
	}

}
?>