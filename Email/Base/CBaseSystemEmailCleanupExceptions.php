<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailCleanupExceptions
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailCleanupExceptions extends CEosPluralBase {

	/**
	 * @return CSystemEmailCleanupException[]
	 */
	public static function fetchSystemEmailCleanupExceptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSystemEmailCleanupException::class, $objDatabase );
	}

	/**
	 * @return CSystemEmailCleanupException
	 */
	public static function fetchSystemEmailCleanupException( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSystemEmailCleanupException::class, $objDatabase );
	}

	public static function fetchSystemEmailCleanupExceptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_cleanup_exceptions', $objDatabase );
	}

	public static function fetchSystemEmailCleanupExceptionById( $intId, $objDatabase ) {
		return self::fetchSystemEmailCleanupException( sprintf( 'SELECT * FROM system_email_cleanup_exceptions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSystemEmailCleanupExceptionsByCid( $intCid, $objDatabase ) {
		return self::fetchSystemEmailCleanupExceptions( sprintf( 'SELECT * FROM system_email_cleanup_exceptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchSystemEmailCleanupExceptionsBySystemEmailTypeId( $intSystemEmailTypeId, $objDatabase ) {
		return self::fetchSystemEmailCleanupExceptions( sprintf( 'SELECT * FROM system_email_cleanup_exceptions WHERE system_email_type_id = %d', $intSystemEmailTypeId ), $objDatabase );
	}

}
?>