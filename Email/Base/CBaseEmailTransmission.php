<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseEmailTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.email_transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intScheduledEmailId;
	protected $m_intScheduledEmailTransmissionId;
	protected $m_intRecipientTypeId;
	protected $m_intTemplateId;
	protected $m_intDeliveryTypeId;
	protected $m_strSubject;
	protected $m_strScheduledEmailFilterName;
	protected $m_intScheduledEmailCreatedBy;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailId', trim( $arrValues['scheduled_email_id'] ) ); elseif( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['scheduled_email_transmission_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailTransmissionId', trim( $arrValues['scheduled_email_transmission_id'] ) ); elseif( isset( $arrValues['scheduled_email_transmission_id'] ) ) $this->setScheduledEmailTransmissionId( $arrValues['scheduled_email_transmission_id'] );
		if( isset( $arrValues['recipient_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRecipientTypeId', trim( $arrValues['recipient_type_id'] ) ); elseif( isset( $arrValues['recipient_type_id'] ) ) $this->setRecipientTypeId( $arrValues['recipient_type_id'] );
		if( isset( $arrValues['template_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateId', trim( $arrValues['template_id'] ) ); elseif( isset( $arrValues['template_id'] ) ) $this->setTemplateId( $arrValues['template_id'] );
		if( isset( $arrValues['delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDeliveryTypeId', trim( $arrValues['delivery_type_id'] ) ); elseif( isset( $arrValues['delivery_type_id'] ) ) $this->setDeliveryTypeId( $arrValues['delivery_type_id'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['scheduled_email_filter_name'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEmailFilterName', trim( stripcslashes( $arrValues['scheduled_email_filter_name'] ) ) ); elseif( isset( $arrValues['scheduled_email_filter_name'] ) ) $this->setScheduledEmailFilterName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_email_filter_name'] ) : $arrValues['scheduled_email_filter_name'] );
		if( isset( $arrValues['scheduled_email_created_by'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailCreatedBy', trim( $arrValues['scheduled_email_created_by'] ) ); elseif( isset( $arrValues['scheduled_email_created_by'] ) ) $this->setScheduledEmailCreatedBy( $arrValues['scheduled_email_created_by'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->set( 'm_intScheduledEmailId', CStrings::strToIntDef( $intScheduledEmailId, NULL, false ) );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setScheduledEmailTransmissionId( $intScheduledEmailTransmissionId ) {
		$this->set( 'm_intScheduledEmailTransmissionId', CStrings::strToIntDef( $intScheduledEmailTransmissionId, NULL, false ) );
	}

	public function getScheduledEmailTransmissionId() {
		return $this->m_intScheduledEmailTransmissionId;
	}

	public function sqlScheduledEmailTransmissionId() {
		return ( true == isset( $this->m_intScheduledEmailTransmissionId ) ) ? ( string ) $this->m_intScheduledEmailTransmissionId : 'NULL';
	}

	public function setRecipientTypeId( $intRecipientTypeId ) {
		$this->set( 'm_intRecipientTypeId', CStrings::strToIntDef( $intRecipientTypeId, NULL, false ) );
	}

	public function getRecipientTypeId() {
		return $this->m_intRecipientTypeId;
	}

	public function sqlRecipientTypeId() {
		return ( true == isset( $this->m_intRecipientTypeId ) ) ? ( string ) $this->m_intRecipientTypeId : 'NULL';
	}

	public function setTemplateId( $intTemplateId ) {
		$this->set( 'm_intTemplateId', CStrings::strToIntDef( $intTemplateId, NULL, false ) );
	}

	public function getTemplateId() {
		return $this->m_intTemplateId;
	}

	public function sqlTemplateId() {
		return ( true == isset( $this->m_intTemplateId ) ) ? ( string ) $this->m_intTemplateId : 'NULL';
	}

	public function setDeliveryTypeId( $intDeliveryTypeId ) {
		$this->set( 'm_intDeliveryTypeId', CStrings::strToIntDef( $intDeliveryTypeId, NULL, false ) );
	}

	public function getDeliveryTypeId() {
		return $this->m_intDeliveryTypeId;
	}

	public function sqlDeliveryTypeId() {
		return ( true == isset( $this->m_intDeliveryTypeId ) ) ? ( string ) $this->m_intDeliveryTypeId : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, -1, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setScheduledEmailFilterName( $strScheduledEmailFilterName ) {
		$this->set( 'm_strScheduledEmailFilterName', CStrings::strTrimDef( $strScheduledEmailFilterName, 50, NULL, true ) );
	}

	public function getScheduledEmailFilterName() {
		return $this->m_strScheduledEmailFilterName;
	}

	public function sqlScheduledEmailFilterName() {
		return ( true == isset( $this->m_strScheduledEmailFilterName ) ) ? '\'' . addslashes( $this->m_strScheduledEmailFilterName ) . '\'' : 'NULL';
	}

	public function setScheduledEmailCreatedBy( $intScheduledEmailCreatedBy ) {
		$this->set( 'm_intScheduledEmailCreatedBy', CStrings::strToIntDef( $intScheduledEmailCreatedBy, NULL, false ) );
	}

	public function getScheduledEmailCreatedBy() {
		return $this->m_intScheduledEmailCreatedBy;
	}

	public function sqlScheduledEmailCreatedBy() {
		return ( true == isset( $this->m_intScheduledEmailCreatedBy ) ) ? ( string ) $this->m_intScheduledEmailCreatedBy : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, scheduled_email_id, scheduled_email_transmission_id, recipient_type_id, template_id, delivery_type_id, subject, scheduled_email_filter_name, scheduled_email_created_by, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlScheduledEmailId() . ', ' .
 						$this->sqlScheduledEmailTransmissionId() . ', ' .
 						$this->sqlRecipientTypeId() . ', ' .
 						$this->sqlTemplateId() . ', ' .
 						$this->sqlDeliveryTypeId() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlScheduledEmailFilterName() . ', ' .
 						$this->sqlScheduledEmailCreatedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; } elseif( true == array_key_exists( 'ScheduledEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_transmission_id = ' . $this->sqlScheduledEmailTransmissionId() . ','; } elseif( true == array_key_exists( 'ScheduledEmailTransmissionId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_transmission_id = ' . $this->sqlScheduledEmailTransmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_type_id = ' . $this->sqlRecipientTypeId() . ','; } elseif( true == array_key_exists( 'RecipientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' recipient_type_id = ' . $this->sqlRecipientTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_id = ' . $this->sqlTemplateId() . ','; } elseif( true == array_key_exists( 'TemplateId', $this->getChangedColumns() ) ) { $strSql .= ' template_id = ' . $this->sqlTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivery_type_id = ' . $this->sqlDeliveryTypeId() . ','; } elseif( true == array_key_exists( 'DeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delivery_type_id = ' . $this->sqlDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_filter_name = ' . $this->sqlScheduledEmailFilterName() . ','; } elseif( true == array_key_exists( 'ScheduledEmailFilterName', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_filter_name = ' . $this->sqlScheduledEmailFilterName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_created_by = ' . $this->sqlScheduledEmailCreatedBy() . ','; } elseif( true == array_key_exists( 'ScheduledEmailCreatedBy', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_created_by = ' . $this->sqlScheduledEmailCreatedBy() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'scheduled_email_transmission_id' => $this->getScheduledEmailTransmissionId(),
			'recipient_type_id' => $this->getRecipientTypeId(),
			'template_id' => $this->getTemplateId(),
			'delivery_type_id' => $this->getDeliveryTypeId(),
			'subject' => $this->getSubject(),
			'scheduled_email_filter_name' => $this->getScheduledEmailFilterName(),
			'scheduled_email_created_by' => $this->getScheduledEmailCreatedBy(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>