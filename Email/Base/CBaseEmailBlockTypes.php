<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailBlockTypes
 * Do not add any new functions to this class.
 */

class CBaseEmailBlockTypes extends CEosPluralBase {

	/**
	 * @return CEmailBlockType[]
	 */
	public static function fetchEmailBlockTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailBlockType', $objDatabase );
	}

	/**
	 * @return CEmailBlockType
	 */
	public static function fetchEmailBlockType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailBlockType', $objDatabase );
	}

	public static function fetchEmailBlockTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_block_types', $objDatabase );
	}

	public static function fetchEmailBlockTypeById( $intId, $objDatabase ) {
		return self::fetchEmailBlockType( sprintf( 'SELECT * FROM email_block_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>