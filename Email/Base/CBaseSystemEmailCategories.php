<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailCategories
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailCategories extends CEosPluralBase {

	/**
	 * @return CSystemEmailCategory[]
	 */
	public static function fetchSystemEmailCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailCategory', $objDatabase );
	}

	/**
	 * @return CSystemEmailCategory
	 */
	public static function fetchSystemEmailCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailCategory', $objDatabase );
	}

	public static function fetchSystemEmailCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_categories', $objDatabase );
	}

	public static function fetchSystemEmailCategoryById( $intId, $objDatabase ) {
		return self::fetchSystemEmailCategory( sprintf( 'SELECT * FROM system_email_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>