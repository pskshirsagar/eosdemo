<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailEvents
 * Do not add any new functions to this class.
 */

class CBaseEmailEvents extends CEosPluralBase {

	/**
	 * @return CEmailEvent[]
	 */
	public static function fetchEmailEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailEvent', $objDatabase );
	}

	/**
	 * @return CEmailEvent
	 */
	public static function fetchEmailEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailEvent', $objDatabase );
	}

	public static function fetchEmailEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_events', $objDatabase );
	}

	public static function fetchEmailEventById( $intId, $objDatabase ) {
		return self::fetchEmailEvent( sprintf( 'SELECT * FROM email_events WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailEventsByCid( $intCid, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailEventsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE system_email_id = %d', ( int ) $intSystemEmailId ), $objDatabase );
	}

	public static function fetchEmailEventsBySystemEmailTypeId( $intSystemEmailTypeId, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE system_email_type_id = %d', ( int ) $intSystemEmailTypeId ), $objDatabase );
	}

	public static function fetchEmailEventsByScheduledEmailId( $intScheduledEmailId, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE scheduled_email_id = %d', ( int ) $intScheduledEmailId ), $objDatabase );
	}

	public static function fetchEmailEventsByEmailEventTypeId( $intEmailEventTypeId, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE email_event_type_id = %d', ( int ) $intEmailEventTypeId ), $objDatabase );
	}

	public static function fetchEmailEventsBySystemEmailRecipientId( $intSystemEmailRecipientId, $objDatabase ) {
		return self::fetchEmailEvents( sprintf( 'SELECT * FROM email_events WHERE system_email_recipient_id = %d', ( int ) $intSystemEmailRecipientId ), $objDatabase );
	}

}
?>