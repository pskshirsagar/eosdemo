<?php

class CBaseEmailProviderConfiguration extends CEosSingularBase {

	const TABLE_NAME = 'public.email_provider_configurations';

	protected $m_intId;
	protected $m_intEmailServiceProviderId;
	protected $m_intCid;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_strServiceUri;
	protected $m_strHost;
	protected $m_intPort;
	protected $m_boolEnableSslTls;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['email_service_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailServiceProviderId', trim( $arrValues['email_service_provider_id'] ) ); elseif( isset( $arrValues['email_service_provider_id'] ) ) $this->setEmailServiceProviderId( $arrValues['email_service_provider_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['username_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strUsernameEncrypted', trim( stripcslashes( $arrValues['username_encrypted'] ) ) ); elseif( isset( $arrValues['username_encrypted'] ) ) $this->setUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username_encrypted'] ) : $arrValues['username_encrypted'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['service_uri'] ) && $boolDirectSet ) $this->set( 'm_strServiceUri', trim( stripcslashes( $arrValues['service_uri'] ) ) ); elseif( isset( $arrValues['service_uri'] ) ) $this->setServiceUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['service_uri'] ) : $arrValues['service_uri'] );
		if( isset( $arrValues['host'] ) && $boolDirectSet ) $this->set( 'm_strHost', trim( stripcslashes( $arrValues['host'] ) ) ); elseif( isset( $arrValues['host'] ) ) $this->setHost( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['host'] ) : $arrValues['host'] );
		if( isset( $arrValues['port'] ) && $boolDirectSet ) $this->set( 'm_intPort', trim( $arrValues['port'] ) ); elseif( isset( $arrValues['port'] ) ) $this->setPort( $arrValues['port'] );
		if( isset( $arrValues['enable_ssl_tls'] ) && $boolDirectSet ) $this->set( 'm_boolEnableSslTls', trim( stripcslashes( $arrValues['enable_ssl_tls'] ) ) ); elseif( isset( $arrValues['enable_ssl_tls'] ) ) $this->setEnableSslTls( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['enable_ssl_tls'] ) : $arrValues['enable_ssl_tls'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmailServiceProviderId( $intEmailServiceProviderId ) {
		$this->set( 'm_intEmailServiceProviderId', CStrings::strToIntDef( $intEmailServiceProviderId, NULL, false ) );
	}

	public function getEmailServiceProviderId() {
		return $this->m_intEmailServiceProviderId;
	}

	public function sqlEmailServiceProviderId() {
		return ( true == isset( $this->m_intEmailServiceProviderId ) ) ? ( string ) $this->m_intEmailServiceProviderId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->set( 'm_strUsernameEncrypted', CStrings::strTrimDef( $strUsernameEncrypted, 1000, NULL, true ) );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function sqlUsernameEncrypted() {
		return ( true == isset( $this->m_strUsernameEncrypted ) ) ? '\'' . addslashes( $this->m_strUsernameEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 1000, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setServiceUri( $strServiceUri ) {
		$this->set( 'm_strServiceUri', CStrings::strTrimDef( $strServiceUri, 240, NULL, true ) );
	}

	public function getServiceUri() {
		return $this->m_strServiceUri;
	}

	public function sqlServiceUri() {
		return ( true == isset( $this->m_strServiceUri ) ) ? '\'' . addslashes( $this->m_strServiceUri ) . '\'' : 'NULL';
	}

	public function setHost( $strHost ) {
		$this->set( 'm_strHost', CStrings::strTrimDef( $strHost, 128, NULL, true ) );
	}

	public function getHost() {
		return $this->m_strHost;
	}

	public function sqlHost() {
		return ( true == isset( $this->m_strHost ) ) ? '\'' . addslashes( $this->m_strHost ) . '\'' : 'NULL';
	}

	public function setPort( $intPort ) {
		$this->set( 'm_intPort', CStrings::strToIntDef( $intPort, NULL, false ) );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function sqlPort() {
		return ( true == isset( $this->m_intPort ) ) ? ( string ) $this->m_intPort : 'NULL';
	}

	public function setEnableSslTls( $boolEnableSslTls ) {
		$this->set( 'm_boolEnableSslTls', CStrings::strToBool( $boolEnableSslTls ) );
	}

	public function getEnableSslTls() {
		return $this->m_boolEnableSslTls;
	}

	public function sqlEnableSslTls() {
		return ( true == isset( $this->m_boolEnableSslTls ) ) ? '\'' . ( true == ( bool ) $this->m_boolEnableSslTls ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, email_service_provider_id, cid, username_encrypted, password_encrypted, service_uri, host, port, enable_ssl_tls, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmailServiceProviderId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlUsernameEncrypted() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlServiceUri() . ', ' .
 						$this->sqlHost() . ', ' .
 						$this->sqlPort() . ', ' .
 						$this->sqlEnableSslTls() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId() . ','; } elseif( true == array_key_exists( 'EmailServiceProviderId', $this->getChangedColumns() ) ) { $strSql .= ' email_service_provider_id = ' . $this->sqlEmailServiceProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; } elseif( true == array_key_exists( 'UsernameEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' username_encrypted = ' . $this->sqlUsernameEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_uri = ' . $this->sqlServiceUri() . ','; } elseif( true == array_key_exists( 'ServiceUri', $this->getChangedColumns() ) ) { $strSql .= ' service_uri = ' . $this->sqlServiceUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' host = ' . $this->sqlHost() . ','; } elseif( true == array_key_exists( 'Host', $this->getChangedColumns() ) ) { $strSql .= ' host = ' . $this->sqlHost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port = ' . $this->sqlPort() . ','; } elseif( true == array_key_exists( 'Port', $this->getChangedColumns() ) ) { $strSql .= ' port = ' . $this->sqlPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enable_ssl_tls = ' . $this->sqlEnableSslTls() . ','; } elseif( true == array_key_exists( 'EnableSslTls', $this->getChangedColumns() ) ) { $strSql .= ' enable_ssl_tls = ' . $this->sqlEnableSslTls() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'email_service_provider_id' => $this->getEmailServiceProviderId(),
			'cid' => $this->getCid(),
			'username_encrypted' => $this->getUsernameEncrypted(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'service_uri' => $this->getServiceUri(),
			'host' => $this->getHost(),
			'port' => $this->getPort(),
			'enable_ssl_tls' => $this->getEnableSslTls(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>