<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailProviderConfigurations
 * Do not add any new functions to this class.
 */

class CBaseEmailProviderConfigurations extends CEosPluralBase {

	/**
	 * @return CEmailProviderConfiguration[]
	 */
	public static function fetchEmailProviderConfigurations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailProviderConfiguration', $objDatabase );
	}

	/**
	 * @return CEmailProviderConfiguration
	 */
	public static function fetchEmailProviderConfiguration( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailProviderConfiguration', $objDatabase );
	}

	public static function fetchEmailProviderConfigurationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_provider_configurations', $objDatabase );
	}

	public static function fetchEmailProviderConfigurationById( $intId, $objDatabase ) {
		return self::fetchEmailProviderConfiguration( sprintf( 'SELECT * FROM email_provider_configurations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailProviderConfigurationsByEmailServiceProviderId( $intEmailServiceProviderId, $objDatabase ) {
		return self::fetchEmailProviderConfigurations( sprintf( 'SELECT * FROM email_provider_configurations WHERE email_service_provider_id = %d', ( int ) $intEmailServiceProviderId ), $objDatabase );
	}

	public static function fetchEmailProviderConfigurationsByCid( $intCid, $objDatabase ) {
		return self::fetchEmailProviderConfigurations( sprintf( 'SELECT * FROM email_provider_configurations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>