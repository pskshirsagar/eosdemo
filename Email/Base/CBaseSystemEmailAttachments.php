<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailAttachments
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailAttachments extends CEosPluralBase {

	/**
	 * @return CSystemEmailAttachment[]
	 */
	public static function fetchSystemEmailAttachments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailAttachment', $objDatabase );
	}

	/**
	 * @return CSystemEmailAttachment
	 */
	public static function fetchSystemEmailAttachment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailAttachment', $objDatabase );
	}

	public static function fetchSystemEmailAttachmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_attachments', $objDatabase );
	}

	public static function fetchSystemEmailAttachmentById( $intId, $objDatabase ) {
		return self::fetchSystemEmailAttachment( sprintf( 'SELECT * FROM system_email_attachments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemEmailAttachmentsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchSystemEmailAttachments( sprintf( 'SELECT * FROM system_email_attachments WHERE system_email_id = %d', ( int ) $intSystemEmailId ), $objDatabase );
	}

	public static function fetchSystemEmailAttachmentsByEmailAttachmentId( $intEmailAttachmentId, $objDatabase ) {
		return self::fetchSystemEmailAttachments( sprintf( 'SELECT * FROM system_email_attachments WHERE email_attachment_id = %d', ( int ) $intEmailAttachmentId ), $objDatabase );
	}

}
?>