<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailEventTypes
 * Do not add any new functions to this class.
 */

class CBaseEmailEventTypes extends CEosPluralBase {

	/**
	 * @return CEmailEventType[]
	 */
	public static function fetchEmailEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailEventType', $objDatabase );
	}

	/**
	 * @return CEmailEventType
	 */
	public static function fetchEmailEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailEventType', $objDatabase );
	}

	public static function fetchEmailEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_event_types', $objDatabase );
	}

	public static function fetchEmailEventTypeById( $intId, $objDatabase ) {
		return self::fetchEmailEventType( sprintf( 'SELECT * FROM email_event_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailEventTypesByEmailEventTypeId( $intEmailEventTypeId, $objDatabase ) {
		return self::fetchEmailEventTypes( sprintf( 'SELECT * FROM email_event_types WHERE email_event_type_id = %d', ( int ) $intEmailEventTypeId ), $objDatabase );
	}

}
?>