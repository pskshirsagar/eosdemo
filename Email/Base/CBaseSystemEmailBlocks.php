<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailBlocks
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailBlocks extends CEosPluralBase {

	/**
	 * @return CSystemEmailBlock[]
	 */
	public static function fetchSystemEmailBlocks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailBlock', $objDatabase );
	}

	/**
	 * @return CSystemEmailBlock
	 */
	public static function fetchSystemEmailBlock( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailBlock', $objDatabase );
	}

	public static function fetchSystemEmailBlockCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_blocks', $objDatabase );
	}

	public static function fetchSystemEmailBlockById( $intId, $objDatabase ) {
		return self::fetchSystemEmailBlock( sprintf( 'SELECT * FROM system_email_blocks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCid( $intCid, $objDatabase ) {
		return self::fetchSystemEmailBlocks( sprintf( 'SELECT * FROM system_email_blocks WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSystemEmailBlocksByEmailBlockTypeId( $intEmailBlockTypeId, $objDatabase ) {
		return self::fetchSystemEmailBlocks( sprintf( 'SELECT * FROM system_email_blocks WHERE email_block_type_id = %d', ( int ) $intEmailBlockTypeId ), $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchSystemEmailBlocks( sprintf( 'SELECT * FROM system_email_blocks WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

}
?>