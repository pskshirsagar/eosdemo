<?php

class CBaseScheduledEmailLog extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.scheduled_email_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSystemEmailId;
	protected $m_intSystemEmailRecipientId;
	protected $m_intTemplateId;
	protected $m_intDeliveryTypeId;
	protected $m_strScheduledEmailFilterName;
	protected $m_strNameFirstEncrypted;
	protected $m_strNameLastEncrypted;
	protected $m_strStatus;
	protected $m_strPrimaryPhoneNumberEncrypted;
	protected $m_strPrimaryPhoneNumberTypeEncrypted;
	protected $m_strPropertyName;
	protected $m_strBuildingName;
	protected $m_strUnitNumber;
	protected $m_strRecipientCreatedOn;
	protected $m_intScheduledEmailCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['system_email_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailRecipientId', trim( $arrValues['system_email_recipient_id'] ) ); elseif( isset( $arrValues['system_email_recipient_id'] ) ) $this->setSystemEmailRecipientId( $arrValues['system_email_recipient_id'] );
		if( isset( $arrValues['template_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateId', trim( $arrValues['template_id'] ) ); elseif( isset( $arrValues['template_id'] ) ) $this->setTemplateId( $arrValues['template_id'] );
		if( isset( $arrValues['delivery_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDeliveryTypeId', trim( $arrValues['delivery_type_id'] ) ); elseif( isset( $arrValues['delivery_type_id'] ) ) $this->setDeliveryTypeId( $arrValues['delivery_type_id'] );
		if( isset( $arrValues['scheduled_email_filter_name'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEmailFilterName', trim( stripcslashes( $arrValues['scheduled_email_filter_name'] ) ) ); elseif( isset( $arrValues['scheduled_email_filter_name'] ) ) $this->setScheduledEmailFilterName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['scheduled_email_filter_name'] ) : $arrValues['scheduled_email_filter_name'] );
		if( isset( $arrValues['name_first_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNameFirstEncrypted', trim( stripcslashes( $arrValues['name_first_encrypted'] ) ) ); elseif( isset( $arrValues['name_first_encrypted'] ) ) $this->setNameFirstEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first_encrypted'] ) : $arrValues['name_first_encrypted'] );
		if( isset( $arrValues['name_last_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNameLastEncrypted', trim( stripcslashes( $arrValues['name_last_encrypted'] ) ) ); elseif( isset( $arrValues['name_last_encrypted'] ) ) $this->setNameLastEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last_encrypted'] ) : $arrValues['name_last_encrypted'] );
		if( isset( $arrValues['status'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['primary_phone_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryPhoneNumberEncrypted', trim( stripcslashes( $arrValues['primary_phone_number_encrypted'] ) ) ); elseif( isset( $arrValues['primary_phone_number_encrypted'] ) ) $this->setPrimaryPhoneNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_phone_number_encrypted'] ) : $arrValues['primary_phone_number_encrypted'] );
		if( isset( $arrValues['primary_phone_number_type_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryPhoneNumberTypeEncrypted', trim( stripcslashes( $arrValues['primary_phone_number_type_encrypted'] ) ) ); elseif( isset( $arrValues['primary_phone_number_type_encrypted'] ) ) $this->setPrimaryPhoneNumberTypeEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['primary_phone_number_type_encrypted'] ) : $arrValues['primary_phone_number_type_encrypted'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['building_name'] ) && $boolDirectSet ) $this->set( 'm_strBuildingName', trim( stripcslashes( $arrValues['building_name'] ) ) ); elseif( isset( $arrValues['building_name'] ) ) $this->setBuildingName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['building_name'] ) : $arrValues['building_name'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['recipient_created_on'] ) && $boolDirectSet ) $this->set( 'm_strRecipientCreatedOn', trim( $arrValues['recipient_created_on'] ) ); elseif( isset( $arrValues['recipient_created_on'] ) ) $this->setRecipientCreatedOn( $arrValues['recipient_created_on'] );
		if( isset( $arrValues['scheduled_email_created_by'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailCreatedBy', trim( $arrValues['scheduled_email_created_by'] ) ); elseif( isset( $arrValues['scheduled_email_created_by'] ) ) $this->setScheduledEmailCreatedBy( $arrValues['scheduled_email_created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setSystemEmailRecipientId( $intSystemEmailRecipientId ) {
		$this->set( 'm_intSystemEmailRecipientId', CStrings::strToIntDef( $intSystemEmailRecipientId, NULL, false ) );
	}

	public function getSystemEmailRecipientId() {
		return $this->m_intSystemEmailRecipientId;
	}

	public function sqlSystemEmailRecipientId() {
		return ( true == isset( $this->m_intSystemEmailRecipientId ) ) ? ( string ) $this->m_intSystemEmailRecipientId : 'NULL';
	}

	public function setTemplateId( $intTemplateId ) {
		$this->set( 'm_intTemplateId', CStrings::strToIntDef( $intTemplateId, NULL, false ) );
	}

	public function getTemplateId() {
		return $this->m_intTemplateId;
	}

	public function sqlTemplateId() {
		return ( true == isset( $this->m_intTemplateId ) ) ? ( string ) $this->m_intTemplateId : 'NULL';
	}

	public function setDeliveryTypeId( $intDeliveryTypeId ) {
		$this->set( 'm_intDeliveryTypeId', CStrings::strToIntDef( $intDeliveryTypeId, NULL, false ) );
	}

	public function getDeliveryTypeId() {
		return $this->m_intDeliveryTypeId;
	}

	public function sqlDeliveryTypeId() {
		return ( true == isset( $this->m_intDeliveryTypeId ) ) ? ( string ) $this->m_intDeliveryTypeId : 'NULL';
	}

	public function setScheduledEmailFilterName( $strScheduledEmailFilterName ) {
		$this->set( 'm_strScheduledEmailFilterName', CStrings::strTrimDef( $strScheduledEmailFilterName, 50, NULL, true ) );
	}

	public function getScheduledEmailFilterName() {
		return $this->m_strScheduledEmailFilterName;
	}

	public function sqlScheduledEmailFilterName() {
		return ( true == isset( $this->m_strScheduledEmailFilterName ) ) ? '\'' . addslashes( $this->m_strScheduledEmailFilterName ) . '\'' : 'NULL';
	}

	public function setNameFirstEncrypted( $strNameFirstEncrypted ) {
		$this->set( 'm_strNameFirstEncrypted', CStrings::strTrimDef( $strNameFirstEncrypted, 240, NULL, true ) );
	}

	public function getNameFirstEncrypted() {
		return $this->m_strNameFirstEncrypted;
	}

	public function sqlNameFirstEncrypted() {
		return ( true == isset( $this->m_strNameFirstEncrypted ) ) ? '\'' . addslashes( $this->m_strNameFirstEncrypted ) . '\'' : 'NULL';
	}

	public function setNameLastEncrypted( $strNameLastEncrypted ) {
		$this->set( 'm_strNameLastEncrypted', CStrings::strTrimDef( $strNameLastEncrypted, 240, NULL, true ) );
	}

	public function getNameLastEncrypted() {
		return $this->m_strNameLastEncrypted;
	}

	public function sqlNameLastEncrypted() {
		return ( true == isset( $this->m_strNameLastEncrypted ) ) ? '\'' . addslashes( $this->m_strNameLastEncrypted ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strStatus', CStrings::strTrimDef( $strStatus, 50, NULL, true ), $strLocaleCode );
	}

	public function getStatus( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strStatus', $strLocaleCode );
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setPrimaryPhoneNumberEncrypted( $strPrimaryPhoneNumberEncrypted ) {
		$this->set( 'm_strPrimaryPhoneNumberEncrypted', CStrings::strTrimDef( $strPrimaryPhoneNumberEncrypted, 240, NULL, true ) );
	}

	public function getPrimaryPhoneNumberEncrypted() {
		return $this->m_strPrimaryPhoneNumberEncrypted;
	}

	public function sqlPrimaryPhoneNumberEncrypted() {
		return ( true == isset( $this->m_strPrimaryPhoneNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strPrimaryPhoneNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setPrimaryPhoneNumberTypeEncrypted( $strPrimaryPhoneNumberTypeEncrypted ) {
		$this->set( 'm_strPrimaryPhoneNumberTypeEncrypted', CStrings::strTrimDef( $strPrimaryPhoneNumberTypeEncrypted, 240, NULL, true ) );
	}

	public function getPrimaryPhoneNumberTypeEncrypted() {
		return $this->m_strPrimaryPhoneNumberTypeEncrypted;
	}

	public function sqlPrimaryPhoneNumberTypeEncrypted() {
		return ( true == isset( $this->m_strPrimaryPhoneNumberTypeEncrypted ) ) ? '\'' . addslashes( $this->m_strPrimaryPhoneNumberTypeEncrypted ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setBuildingName( $strBuildingName ) {
		$this->set( 'm_strBuildingName', CStrings::strTrimDef( $strBuildingName, 50, NULL, true ) );
	}

	public function getBuildingName() {
		return $this->m_strBuildingName;
	}

	public function sqlBuildingName() {
		return ( true == isset( $this->m_strBuildingName ) ) ? '\'' . addslashes( $this->m_strBuildingName ) . '\'' : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 50, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setRecipientCreatedOn( $strRecipientCreatedOn ) {
		$this->set( 'm_strRecipientCreatedOn', CStrings::strTrimDef( $strRecipientCreatedOn, -1, NULL, true ) );
	}

	public function getRecipientCreatedOn() {
		return $this->m_strRecipientCreatedOn;
	}

	public function sqlRecipientCreatedOn() {
		return ( true == isset( $this->m_strRecipientCreatedOn ) ) ? '\'' . $this->m_strRecipientCreatedOn . '\'' : 'NOW()';
	}

	public function setScheduledEmailCreatedBy( $intScheduledEmailCreatedBy ) {
		$this->set( 'm_intScheduledEmailCreatedBy', CStrings::strToIntDef( $intScheduledEmailCreatedBy, NULL, false ) );
	}

	public function getScheduledEmailCreatedBy() {
		return $this->m_intScheduledEmailCreatedBy;
	}

	public function sqlScheduledEmailCreatedBy() {
		return ( true == isset( $this->m_intScheduledEmailCreatedBy ) ) ? ( string ) $this->m_intScheduledEmailCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, system_email_id, system_email_recipient_id, template_id, delivery_type_id, scheduled_email_filter_name, name_first_encrypted, name_last_encrypted, status, primary_phone_number_encrypted, primary_phone_number_type_encrypted, property_name, building_name, unit_number, recipient_created_on, scheduled_email_created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlSystemEmailId() . ', ' .
						$this->sqlSystemEmailRecipientId() . ', ' .
						$this->sqlTemplateId() . ', ' .
						$this->sqlDeliveryTypeId() . ', ' .
						$this->sqlScheduledEmailFilterName() . ', ' .
						$this->sqlNameFirstEncrypted() . ', ' .
						$this->sqlNameLastEncrypted() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlPrimaryPhoneNumberEncrypted() . ', ' .
						$this->sqlPrimaryPhoneNumberTypeEncrypted() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlBuildingName() . ', ' .
						$this->sqlUnitNumber() . ', ' .
						$this->sqlRecipientCreatedOn() . ', ' .
						$this->sqlScheduledEmailCreatedBy() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_recipient_id = ' . $this->sqlSystemEmailRecipientId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_recipient_id = ' . $this->sqlSystemEmailRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_id = ' . $this->sqlTemplateId(). ',' ; } elseif( true == array_key_exists( 'TemplateId', $this->getChangedColumns() ) ) { $strSql .= ' template_id = ' . $this->sqlTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivery_type_id = ' . $this->sqlDeliveryTypeId(). ',' ; } elseif( true == array_key_exists( 'DeliveryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delivery_type_id = ' . $this->sqlDeliveryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_filter_name = ' . $this->sqlScheduledEmailFilterName(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailFilterName', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_filter_name = ' . $this->sqlScheduledEmailFilterName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first_encrypted = ' . $this->sqlNameFirstEncrypted(). ',' ; } elseif( true == array_key_exists( 'NameFirstEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' name_first_encrypted = ' . $this->sqlNameFirstEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last_encrypted = ' . $this->sqlNameLastEncrypted(). ',' ; } elseif( true == array_key_exists( 'NameLastEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' name_last_encrypted = ' . $this->sqlNameLastEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number_encrypted = ' . $this->sqlPrimaryPhoneNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'PrimaryPhoneNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number_encrypted = ' . $this->sqlPrimaryPhoneNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_phone_number_type_encrypted = ' . $this->sqlPrimaryPhoneNumberTypeEncrypted(). ',' ; } elseif( true == array_key_exists( 'PrimaryPhoneNumberTypeEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' primary_phone_number_type_encrypted = ' . $this->sqlPrimaryPhoneNumberTypeEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' building_name = ' . $this->sqlBuildingName(). ',' ; } elseif( true == array_key_exists( 'BuildingName', $this->getChangedColumns() ) ) { $strSql .= ' building_name = ' . $this->sqlBuildingName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber(). ',' ; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_created_on = ' . $this->sqlRecipientCreatedOn(). ',' ; } elseif( true == array_key_exists( 'RecipientCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' recipient_created_on = ' . $this->sqlRecipientCreatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_created_by = ' . $this->sqlScheduledEmailCreatedBy(). ',' ; } elseif( true == array_key_exists( 'ScheduledEmailCreatedBy', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_created_by = ' . $this->sqlScheduledEmailCreatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'system_email_id' => $this->getSystemEmailId(),
			'system_email_recipient_id' => $this->getSystemEmailRecipientId(),
			'template_id' => $this->getTemplateId(),
			'delivery_type_id' => $this->getDeliveryTypeId(),
			'scheduled_email_filter_name' => $this->getScheduledEmailFilterName(),
			'name_first_encrypted' => $this->getNameFirstEncrypted(),
			'name_last_encrypted' => $this->getNameLastEncrypted(),
			'status' => $this->getStatus(),
			'primary_phone_number_encrypted' => $this->getPrimaryPhoneNumberEncrypted(),
			'primary_phone_number_type_encrypted' => $this->getPrimaryPhoneNumberTypeEncrypted(),
			'property_name' => $this->getPropertyName(),
			'building_name' => $this->getBuildingName(),
			'unit_number' => $this->getUnitNumber(),
			'recipient_created_on' => $this->getRecipientCreatedOn(),
			'scheduled_email_created_by' => $this->getScheduledEmailCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>