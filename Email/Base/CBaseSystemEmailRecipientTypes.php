<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailRecipientTypes
 * Do not add any new functions to this class.
 */

class CBaseSystemEmailRecipientTypes extends CEosPluralBase {

	/**
	 * @return CSystemEmailRecipientType[]
	 */
	public static function fetchSystemEmailRecipientTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemEmailRecipientType', $objDatabase );
	}

	/**
	 * @return CSystemEmailRecipientType
	 */
	public static function fetchSystemEmailRecipientType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemEmailRecipientType', $objDatabase );
	}

	public static function fetchSystemEmailRecipientTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_email_recipient_types', $objDatabase );
	}

	public static function fetchSystemEmailRecipientTypeById( $intId, $objDatabase ) {
		return self::fetchSystemEmailRecipientType( sprintf( 'SELECT * FROM system_email_recipient_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>