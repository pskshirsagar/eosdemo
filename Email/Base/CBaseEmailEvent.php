<?php

class CBaseEmailEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.email_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSystemEmailId;
	protected $m_intSystemEmailTypeId;
	protected $m_intScheduledEmailId;
	protected $m_intEmailEventTypeId;
	protected $m_intSystemEmailRecipientId;
	protected $m_strRecipientEmailAddress;
	protected $m_strResponse;
	protected $m_intAttemptCount;
	protected $m_strStatusCode;
	protected $m_strProcessedOn;
	protected $m_strDeliveredOn;
	protected $m_strClickedOn;
	protected $m_strOpenedOn;
	protected $m_strBouncedOn;
	protected $m_strDeferredOn;
	protected $m_strSpammedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAttemptCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['system_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailTypeId', trim( $arrValues['system_email_type_id'] ) ); elseif( isset( $arrValues['system_email_type_id'] ) ) $this->setSystemEmailTypeId( $arrValues['system_email_type_id'] );
		if( isset( $arrValues['scheduled_email_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledEmailId', trim( $arrValues['scheduled_email_id'] ) ); elseif( isset( $arrValues['scheduled_email_id'] ) ) $this->setScheduledEmailId( $arrValues['scheduled_email_id'] );
		if( isset( $arrValues['email_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailEventTypeId', trim( $arrValues['email_event_type_id'] ) ); elseif( isset( $arrValues['email_event_type_id'] ) ) $this->setEmailEventTypeId( $arrValues['email_event_type_id'] );
		if( isset( $arrValues['system_email_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailRecipientId', trim( $arrValues['system_email_recipient_id'] ) ); elseif( isset( $arrValues['system_email_recipient_id'] ) ) $this->setSystemEmailRecipientId( $arrValues['system_email_recipient_id'] );
		if( isset( $arrValues['recipient_email_address'] ) && $boolDirectSet ) $this->set( 'm_strRecipientEmailAddress', trim( stripcslashes( $arrValues['recipient_email_address'] ) ) ); elseif( isset( $arrValues['recipient_email_address'] ) ) $this->setRecipientEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['recipient_email_address'] ) : $arrValues['recipient_email_address'] );
		if( isset( $arrValues['response'] ) && $boolDirectSet ) $this->set( 'm_strResponse', trim( stripcslashes( $arrValues['response'] ) ) ); elseif( isset( $arrValues['response'] ) ) $this->setResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response'] ) : $arrValues['response'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['status_code'] ) && $boolDirectSet ) $this->set( 'm_strStatusCode', trim( stripcslashes( $arrValues['status_code'] ) ) ); elseif( isset( $arrValues['status_code'] ) ) $this->setStatusCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_code'] ) : $arrValues['status_code'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['delivered_on'] ) && $boolDirectSet ) $this->set( 'm_strDeliveredOn', trim( $arrValues['delivered_on'] ) ); elseif( isset( $arrValues['delivered_on'] ) ) $this->setDeliveredOn( $arrValues['delivered_on'] );
		if( isset( $arrValues['clicked_on'] ) && $boolDirectSet ) $this->set( 'm_strClickedOn', trim( $arrValues['clicked_on'] ) ); elseif( isset( $arrValues['clicked_on'] ) ) $this->setClickedOn( $arrValues['clicked_on'] );
		if( isset( $arrValues['opened_on'] ) && $boolDirectSet ) $this->set( 'm_strOpenedOn', trim( $arrValues['opened_on'] ) ); elseif( isset( $arrValues['opened_on'] ) ) $this->setOpenedOn( $arrValues['opened_on'] );
		if( isset( $arrValues['bounced_on'] ) && $boolDirectSet ) $this->set( 'm_strBouncedOn', trim( $arrValues['bounced_on'] ) ); elseif( isset( $arrValues['bounced_on'] ) ) $this->setBouncedOn( $arrValues['bounced_on'] );
		if( isset( $arrValues['deferred_on'] ) && $boolDirectSet ) $this->set( 'm_strDeferredOn', trim( $arrValues['deferred_on'] ) ); elseif( isset( $arrValues['deferred_on'] ) ) $this->setDeferredOn( $arrValues['deferred_on'] );
		if( isset( $arrValues['spammed_on'] ) && $boolDirectSet ) $this->set( 'm_strSpammedOn', trim( $arrValues['spammed_on'] ) ); elseif( isset( $arrValues['spammed_on'] ) ) $this->setSpammedOn( $arrValues['spammed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setSystemEmailTypeId( $intSystemEmailTypeId ) {
		$this->set( 'm_intSystemEmailTypeId', CStrings::strToIntDef( $intSystemEmailTypeId, NULL, false ) );
	}

	public function getSystemEmailTypeId() {
		return $this->m_intSystemEmailTypeId;
	}

	public function sqlSystemEmailTypeId() {
		return ( true == isset( $this->m_intSystemEmailTypeId ) ) ? ( string ) $this->m_intSystemEmailTypeId : 'NULL';
	}

	public function setScheduledEmailId( $intScheduledEmailId ) {
		$this->set( 'm_intScheduledEmailId', CStrings::strToIntDef( $intScheduledEmailId, NULL, false ) );
	}

	public function getScheduledEmailId() {
		return $this->m_intScheduledEmailId;
	}

	public function sqlScheduledEmailId() {
		return ( true == isset( $this->m_intScheduledEmailId ) ) ? ( string ) $this->m_intScheduledEmailId : 'NULL';
	}

	public function setEmailEventTypeId( $intEmailEventTypeId ) {
		$this->set( 'm_intEmailEventTypeId', CStrings::strToIntDef( $intEmailEventTypeId, NULL, false ) );
	}

	public function getEmailEventTypeId() {
		return $this->m_intEmailEventTypeId;
	}

	public function sqlEmailEventTypeId() {
		return ( true == isset( $this->m_intEmailEventTypeId ) ) ? ( string ) $this->m_intEmailEventTypeId : 'NULL';
	}

	public function setSystemEmailRecipientId( $intSystemEmailRecipientId ) {
		$this->set( 'm_intSystemEmailRecipientId', CStrings::strToIntDef( $intSystemEmailRecipientId, NULL, false ) );
	}

	public function getSystemEmailRecipientId() {
		return $this->m_intSystemEmailRecipientId;
	}

	public function sqlSystemEmailRecipientId() {
		return ( true == isset( $this->m_intSystemEmailRecipientId ) ) ? ( string ) $this->m_intSystemEmailRecipientId : 'NULL';
	}

	public function setRecipientEmailAddress( $strRecipientEmailAddress ) {
		$this->set( 'm_strRecipientEmailAddress', CStrings::strTrimDef( $strRecipientEmailAddress, 240, NULL, true ) );
	}

	public function getRecipientEmailAddress() {
		return $this->m_strRecipientEmailAddress;
	}

	public function sqlRecipientEmailAddress() {
		return ( true == isset( $this->m_strRecipientEmailAddress ) ) ? '\'' . addslashes( $this->m_strRecipientEmailAddress ) . '\'' : 'NULL';
	}

	public function setResponse( $strResponse ) {
		$this->set( 'm_strResponse', CStrings::strTrimDef( $strResponse, -1, NULL, true ) );
	}

	public function getResponse() {
		return $this->m_strResponse;
	}

	public function sqlResponse() {
		return ( true == isset( $this->m_strResponse ) ) ? '\'' . addslashes( $this->m_strResponse ) . '\'' : 'NULL';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setStatusCode( $strStatusCode ) {
		$this->set( 'm_strStatusCode', CStrings::strTrimDef( $strStatusCode, 20, NULL, true ) );
	}

	public function getStatusCode() {
		return $this->m_strStatusCode;
	}

	public function sqlStatusCode() {
		return ( true == isset( $this->m_strStatusCode ) ) ? '\'' . addslashes( $this->m_strStatusCode ) . '\'' : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setDeliveredOn( $strDeliveredOn ) {
		$this->set( 'm_strDeliveredOn', CStrings::strTrimDef( $strDeliveredOn, -1, NULL, true ) );
	}

	public function getDeliveredOn() {
		return $this->m_strDeliveredOn;
	}

	public function sqlDeliveredOn() {
		return ( true == isset( $this->m_strDeliveredOn ) ) ? '\'' . $this->m_strDeliveredOn . '\'' : 'NULL';
	}

	public function setClickedOn( $strClickedOn ) {
		$this->set( 'm_strClickedOn', CStrings::strTrimDef( $strClickedOn, -1, NULL, true ) );
	}

	public function getClickedOn() {
		return $this->m_strClickedOn;
	}

	public function sqlClickedOn() {
		return ( true == isset( $this->m_strClickedOn ) ) ? '\'' . $this->m_strClickedOn . '\'' : 'NULL';
	}

	public function setOpenedOn( $strOpenedOn ) {
		$this->set( 'm_strOpenedOn', CStrings::strTrimDef( $strOpenedOn, -1, NULL, true ) );
	}

	public function getOpenedOn() {
		return $this->m_strOpenedOn;
	}

	public function sqlOpenedOn() {
		return ( true == isset( $this->m_strOpenedOn ) ) ? '\'' . $this->m_strOpenedOn . '\'' : 'NULL';
	}

	public function setBouncedOn( $strBouncedOn ) {
		$this->set( 'm_strBouncedOn', CStrings::strTrimDef( $strBouncedOn, -1, NULL, true ) );
	}

	public function getBouncedOn() {
		return $this->m_strBouncedOn;
	}

	public function sqlBouncedOn() {
		return ( true == isset( $this->m_strBouncedOn ) ) ? '\'' . $this->m_strBouncedOn . '\'' : 'NULL';
	}

	public function setDeferredOn( $strDeferredOn ) {
		$this->set( 'm_strDeferredOn', CStrings::strTrimDef( $strDeferredOn, -1, NULL, true ) );
	}

	public function getDeferredOn() {
		return $this->m_strDeferredOn;
	}

	public function sqlDeferredOn() {
		return ( true == isset( $this->m_strDeferredOn ) ) ? '\'' . $this->m_strDeferredOn . '\'' : 'NULL';
	}

	public function setSpammedOn( $strSpammedOn ) {
		$this->set( 'm_strSpammedOn', CStrings::strTrimDef( $strSpammedOn, -1, NULL, true ) );
	}

	public function getSpammedOn() {
		return $this->m_strSpammedOn;
	}

	public function sqlSpammedOn() {
		return ( true == isset( $this->m_strSpammedOn ) ) ? '\'' . $this->m_strSpammedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, system_email_id, system_email_type_id, scheduled_email_id, email_event_type_id, system_email_recipient_id, recipient_email_address, response, attempt_count, status_code, processed_on, delivered_on, clicked_on, opened_on, bounced_on, deferred_on, spammed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlSystemEmailTypeId() . ', ' .
 						$this->sqlScheduledEmailId() . ', ' .
 						$this->sqlEmailEventTypeId() . ', ' .
 						$this->sqlSystemEmailRecipientId() . ', ' .
 						$this->sqlRecipientEmailAddress() . ', ' .
 						$this->sqlResponse() . ', ' .
 						$this->sqlAttemptCount() . ', ' .
 						$this->sqlStatusCode() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
 						$this->sqlDeliveredOn() . ', ' .
 						$this->sqlClickedOn() . ', ' .
 						$this->sqlOpenedOn() . ', ' .
 						$this->sqlBouncedOn() . ', ' .
 						$this->sqlDeferredOn() . ', ' .
 						$this->sqlSpammedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId() . ','; } elseif( true == array_key_exists( 'SystemEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_type_id = ' . $this->sqlSystemEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; } elseif( true == array_key_exists( 'ScheduledEmailId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_email_id = ' . $this->sqlScheduledEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_event_type_id = ' . $this->sqlEmailEventTypeId() . ','; } elseif( true == array_key_exists( 'EmailEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' email_event_type_id = ' . $this->sqlEmailEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_recipient_id = ' . $this->sqlSystemEmailRecipientId() . ','; } elseif( true == array_key_exists( 'SystemEmailRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_recipient_id = ' . $this->sqlSystemEmailRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_email_address = ' . $this->sqlRecipientEmailAddress() . ','; } elseif( true == array_key_exists( 'RecipientEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' recipient_email_address = ' . $this->sqlRecipientEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; } elseif( true == array_key_exists( 'Response', $this->getChangedColumns() ) ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_code = ' . $this->sqlStatusCode() . ','; } elseif( true == array_key_exists( 'StatusCode', $this->getChangedColumns() ) ) { $strSql .= ' status_code = ' . $this->sqlStatusCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn() . ','; } elseif( true == array_key_exists( 'DeliveredOn', $this->getChangedColumns() ) ) { $strSql .= ' delivered_on = ' . $this->sqlDeliveredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clicked_on = ' . $this->sqlClickedOn() . ','; } elseif( true == array_key_exists( 'ClickedOn', $this->getChangedColumns() ) ) { $strSql .= ' clicked_on = ' . $this->sqlClickedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opened_on = ' . $this->sqlOpenedOn() . ','; } elseif( true == array_key_exists( 'OpenedOn', $this->getChangedColumns() ) ) { $strSql .= ' opened_on = ' . $this->sqlOpenedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bounced_on = ' . $this->sqlBouncedOn() . ','; } elseif( true == array_key_exists( 'BouncedOn', $this->getChangedColumns() ) ) { $strSql .= ' bounced_on = ' . $this->sqlBouncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deferred_on = ' . $this->sqlDeferredOn() . ','; } elseif( true == array_key_exists( 'DeferredOn', $this->getChangedColumns() ) ) { $strSql .= ' deferred_on = ' . $this->sqlDeferredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' spammed_on = ' . $this->sqlSpammedOn() . ','; } elseif( true == array_key_exists( 'SpammedOn', $this->getChangedColumns() ) ) { $strSql .= ' spammed_on = ' . $this->sqlSpammedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'system_email_id' => $this->getSystemEmailId(),
			'system_email_type_id' => $this->getSystemEmailTypeId(),
			'scheduled_email_id' => $this->getScheduledEmailId(),
			'email_event_type_id' => $this->getEmailEventTypeId(),
			'system_email_recipient_id' => $this->getSystemEmailRecipientId(),
			'recipient_email_address' => $this->getRecipientEmailAddress(),
			'response' => $this->getResponse(),
			'attempt_count' => $this->getAttemptCount(),
			'status_code' => $this->getStatusCode(),
			'processed_on' => $this->getProcessedOn(),
			'delivered_on' => $this->getDeliveredOn(),
			'clicked_on' => $this->getClickedOn(),
			'opened_on' => $this->getOpenedOn(),
			'bounced_on' => $this->getBouncedOn(),
			'deferred_on' => $this->getDeferredOn(),
			'spammed_on' => $this->getSpammedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>