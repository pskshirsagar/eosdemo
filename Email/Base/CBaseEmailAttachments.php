<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailAttachments
 * Do not add any new functions to this class.
 */

class CBaseEmailAttachments extends CEosPluralBase {

	/**
	 * @return CEmailAttachment[]
	 */
	public static function fetchEmailAttachments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailAttachment', $objDatabase );
	}

	/**
	 * @return CEmailAttachment
	 */
	public static function fetchEmailAttachment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailAttachment', $objDatabase );
	}

	public static function fetchEmailAttachmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_attachments', $objDatabase );
	}

	public static function fetchEmailAttachmentById( $intId, $objDatabase ) {
		return self::fetchEmailAttachment( sprintf( 'SELECT * FROM email_attachments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailAttachmentsByFileExtensionId( $intFileExtensionId, $objDatabase ) {
		return self::fetchEmailAttachments( sprintf( 'SELECT * FROM email_attachments WHERE file_extension_id = %d', ( int ) $intFileExtensionId ), $objDatabase );
	}

}
?>