<?php

class CBaseSystemEmailRecipient extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.system_email_recipients';

	protected $m_intId;
	protected $m_intSystemEmailId;
	protected $m_intRecipientTypeId;
	protected $m_intCid;
	protected $m_intReferenceNumber;
	protected $m_strEmailAddress;
	protected $m_strMergeFields;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['recipient_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRecipientTypeId', trim( $arrValues['recipient_type_id'] ) ); elseif( isset( $arrValues['recipient_type_id'] ) ) $this->setRecipientTypeId( $arrValues['recipient_type_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['merge_fields'] ) && $boolDirectSet ) $this->set( 'm_strMergeFields', trim( $arrValues['merge_fields'] ) ); elseif( isset( $arrValues['merge_fields'] ) ) $this->setMergeFields( $arrValues['merge_fields'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setRecipientTypeId( $intRecipientTypeId ) {
		$this->set( 'm_intRecipientTypeId', CStrings::strToIntDef( $intRecipientTypeId, NULL, false ) );
	}

	public function getRecipientTypeId() {
		return $this->m_intRecipientTypeId;
	}

	public function sqlRecipientTypeId() {
		return ( true == isset( $this->m_intRecipientTypeId ) ) ? ( string ) $this->m_intRecipientTypeId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, -1, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setMergeFields( $strMergeFields ) {
		$this->set( 'm_strMergeFields', CStrings::strTrimDef( $strMergeFields, -1, NULL, true ) );
	}

	public function getMergeFields() {
		return $this->m_strMergeFields;
	}

	public function sqlMergeFields() {
		return ( true == isset( $this->m_strMergeFields ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMergeFields ) : '\'' . addslashes( $this->m_strMergeFields ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, system_email_id, recipient_type_id, cid, reference_number, email_address, merge_fields, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSystemEmailId() . ', ' .
						$this->sqlRecipientTypeId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReferenceNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlMergeFields() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId(). ',' ; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recipient_type_id = ' . $this->sqlRecipientTypeId(). ',' ; } elseif( true == array_key_exists( 'RecipientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' recipient_type_id = ' . $this->sqlRecipientTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merge_fields = ' . $this->sqlMergeFields(). ',' ; } elseif( true == array_key_exists( 'MergeFields', $this->getChangedColumns() ) ) { $strSql .= ' merge_fields = ' . $this->sqlMergeFields() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'system_email_id' => $this->getSystemEmailId(),
			'recipient_type_id' => $this->getRecipientTypeId(),
			'cid' => $this->getCid(),
			'reference_number' => $this->getReferenceNumber(),
			'email_address' => $this->getEmailAddress(),
			'merge_fields' => $this->getMergeFields(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>