<?php

class CSystemEmailPriority extends CBaseSystemEmailPriority {

	const CRITICAL	 		= 1;
	const URGENT 			= 2;
	const NORMAL 			= 3;

	public static $c_arrintQueuePriorities = [ self::CRITICAL => 2, self::URGENT => 1, self::NORMAL => 0 ];

    public function valName() {
        $boolIsValid = true;

        if( true == is_null( $this->getName() ) ) {
           $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', '' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>