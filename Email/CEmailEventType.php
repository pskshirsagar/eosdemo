<?php

class CEmailEventType extends CBaseEmailEventType {

	const PROCESSED		= 1;
	const DROPPED		= 2;
	const DEFERRED		= 3;
	const DELIVERED		= 4;
	const SPAM_REPORTS	= 5;
	const CLICK			= 6;
	const OPEN			= 7;
	const BOUNCE		= 8;
	const BLOCKED		= 9;
	const EXPIRED		= 10;
	const UNSUBSCRIBE	= 11;
	const PENDING		= 12;

	public static $c_arrstrEmailEventTypes = array(
		self::PROCESSED		=> 'Processed',
		self::DROPPED		=> 'Dropped',
		self::DEFERRED		=> 'Deferred',
		self::DELIVERED		=> 'Delivered',
		self::SPAM_REPORTS	=> 'Spam Reports',
		self::CLICK			=> 'Click',
		self::OPEN			=> 'Open',
		self::BOUNCE		=> 'Bounced',
		self::BLOCKED		=> 'Blocked',
		self::EXPIRED		=> 'Expired',
		self::UNSUBSCRIBE	=> 'Unsubscribe',
		self::PENDING		=> 'Pending'
	);

	public static $c_arrstrFailedEmailEventTypes = array(
		self::BOUNCE		=> 'Bounced',
		self::BLOCKED		=> 'Blocked',
		self::DROPPED		=> 'Invalid',
		self::SPAM_REPORTS	=> 'Spam Reports',
		self::UNSUBSCRIBE	=> 'Unsubscribe'
	);

	public static $c_arrstrUnblockEventTypes = array(
		self::BLOCKED		=> 'Blocked',
		self::BOUNCE		=> 'Bounced'
	);

	public static $c_arrstrMassEmailEventTypes = array(
		self::OPEN		=> 'opened',
		self::DELIVERED	=> 'delivered',
		self::DROPPED	=> 'dropped'
	);

}
?>