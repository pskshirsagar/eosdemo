<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailEventTypes
 * Do not add any new functions to this class.
 */

class CEmailEventTypes extends CBaseEmailEventTypes {

	public static function fetchEmailEventTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmailEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmailEventType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmailEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmailEventTypesByIsPublished( $intIsPublished, $objDatabase ) {
		return self::fetchEmailEventTypes( sprintf( 'SELECT * FROM email_event_types WHERE is_published = %d', ( int ) $intIsPublished ), $objDatabase );
	}

}
?>