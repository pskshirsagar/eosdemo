<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailAttachments
 * Do not add any new functions to this class.
 */

class CEmailAttachments extends CBaseEmailAttachments {

	public static function fetchEmailAttachmentsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchEmailAttachments( 'SELECT ea.* FROM email_attachments ea, system_email_attachments sea WHERE ea.id = sea.email_attachment_id AND sea.system_email_id = ' . ( int ) $intSystemEmailId, $objDatabase );
	}

	public static function fetchEmailAttachmentsBySystemEmailIds( $arrintSystemEmailIds, $objDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$arrobjEmailAttachments = array();
		$objEmailAttachment		= NULL;

		$strSql = 'SELECT ea.*, sea.system_email_id FROM email_attachments ea, system_email_attachments sea WHERE ea.id = sea.email_attachment_id AND sea.system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' ) ';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 == $objDataset->getRecordCount() ) {
			$objDataset->cleanup();
			return NULL;
		}

		$objEmailAttachment = new CEmailAttachment();

		while( false == $objDataset->eof() ) {
			$arrmixValues = $objDataset->fetchArray();
			$objCloneEmailAttachment = clone $objEmailAttachment;

			$objCloneEmailAttachment->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

			if( true == $objCloneEmailAttachment->getAllowDifferentialUpdate() ) {
				$objCloneEmailAttachment->setSerializedOriginalValues( serialize( $arrmixValues ) );
			}

			$arrobjEmailAttachments[] = $objCloneEmailAttachment;
			$objDataset->next();
		}

		$objDataset->cleanup();

		return $arrobjEmailAttachments;
	}

	public static function fetchInlineEmailAttachmentsBySystemEmailId( $intSystemEmailId, $objDatabase ) {

		$strSql = 'SELECT
						ea.id, ea.title
					FROM
						email_attachments ea, system_email_attachments sea 
					WHERE 
						ea.id = sea.email_attachment_id
					AND
						ea.is_inline = true
					AND
						sea.system_email_id = ' . ( int ) $intSystemEmailId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmailAttachmentsByIds( $intEmailAttachmentIds, $objDatabase ) {
		if( false == valArr( $intEmailAttachmentIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
		             *
		           FROM
		             email_attachments
		           WHERE
		             id IN ( ' . sqlIntImplode( $intEmailAttachmentIds ) . ' )';
		return self::fetchEmailAttachments( $strSql, $objDatabase );
	}

}
?>