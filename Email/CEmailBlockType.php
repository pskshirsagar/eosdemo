<?php

class CEmailBlockType extends CBaseEmailBlockType {

	const RENT_REMINDER                                   = 1;
	const JOB_POSTING_NOTIFICATION                        = 2;
	const PS_NEWSLETTER                                   = 3;
	const SCHEDULED_DOWN_TIME                             = 4;
	const NEW_PRODUCT_ANNOUNCEMENT                        = 5;
	const OTHER_MASS_EMAIL                                = 6;
	const RESIDENT_UTILITY_INVOICE                        = 7;
	const PARCEL_ALERT                                    = 8;
	const PRODUCT_UPDATE_EMAIL                            = 9;
	const PRODUCT_PROMOTION                               = 10;
	const RESIDENT_PORTAL_INVITE_EMAIL                    = 11;
	const INSPECTION_REMINDER                             = 12;
	const RESIDENT_INSURE_COMPLIANCE_EMAIL                = 13;
	const RESIDENT_INSURE_MOVEIN_EMAIL                    = 14;
	const EVENT_SCHEDULING_EMAIL                          = 15;
	const EMAIL_BLOCK_AWAITING_RESPONSE_TASK_NOTIFICATION = 16;
	const CAMPAIGN                                        = 17;
	const WORKORDER                                       = 18;
	const KMV                                             = 19;
	const LEASE_COMPLETED_FOLLOWUP_INSURANCE_EMAIL        = 20;
	const RESIDENT_COMMUNICATION                          = 21;
	const LEAD_COMMUNICATION                              = 22;
	const MESSAGE_CENTER                                  = 23;
	const MARKETING_EMAILS                                = 24;

	public static $c_arrintEmailBlockTypes = [
		self::EVENT_SCHEDULING_EMAIL	=> 'Contact Point Emails',
		self::CAMPAIGN					=> 'Marketing Emails',
		self::WORKORDER					=> 'Workorder Emails',
		self::PARCEL_ALERT				=> 'Package Notification',
		self::RESIDENT_COMMUNICATION    => 'Resident Communication',
		self::LEAD_COMMUNICATION        => 'Lead Communication'
	];

	public static function getAvailableEmailBlockTypesForCampaignTarget() {
		return [ self::CAMPAIGN => __( 'Marketing Emails' ) ];
	}

	public function getAvailableEmailBlockTypesLead() {
		$arrintAvailableEmailBlockTypesLead = [ self::EVENT_SCHEDULING_EMAIL => __( 'Leasing Process Updates' ), self::MARKETING_EMAILS => __( 'Unsubscribe from all Marketing Messages' ) ];
		return $arrintAvailableEmailBlockTypesLead;
	}

	public function getAvailableEmailBlockTypesResident() {
		$arrintAvailableEmailBlockTypesResident = [
			self::WORKORDER => __( 'Maintenance Updates' ),
			self::RESIDENT_COMMUNICATION => __( 'Resident Communication' ),
			self::PARCEL_ALERT => __( 'Parcel Alert' ),
			self::RENT_REMINDER => __( 'Rent Reminders' ),
			self::INSPECTION_REMINDER => __( 'Inspection Reminders' ),
			self::EVENT_SCHEDULING_EMAIL => __( 'Leasing Process Updates' ),
			self::MARKETING_EMAILS => __( 'Unsubscribe from all Marketing Messages' )
		];
		return $arrintAvailableEmailBlockTypesResident;
	}

	public static $c_arrintOtherMassEmails						= [ CEmailBlockType::NEW_PRODUCT_ANNOUNCEMENT, CEmailBlockType::SCHEDULED_DOWN_TIME, CEmailBlockType::OTHER_MASS_EMAIL, CEmailBlockType::PRODUCT_PROMOTION ];
	public static $c_arrintProuctUpdateMassEmails				= [ CEmailBlockType::PRODUCT_UPDATE_EMAIL ];
	public static $c_arrintPsNewsLetterMassEmails				= [ CEmailBlockType::PS_NEWSLETTER ];
	public static $c_arrintShowUnsubscribeEmailBlockTypeIds	    = [ CEmailBlockType::RENT_REMINDER, CEmailBlockType::RESIDENT_PORTAL_INVITE_EMAIL ];
	public static $c_arrintMassEmailBlockTypeIds				= [ self::PS_NEWSLETTER, self::SCHEDULED_DOWN_TIME, self::NEW_PRODUCT_ANNOUNCEMENT, self::OTHER_MASS_EMAIL, self::PRODUCT_UPDATE_EMAIL, self::PRODUCT_PROMOTION ];

}
?>