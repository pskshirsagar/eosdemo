<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CDomainBlocks
 * Do not add any new functions to this class.
 */

class CDomainBlocks extends CBaseDomainBlocks {

	public static function fetchConflictingDomainBlockCountByCidByEmailBlockTypeIdByDomain( $intCid, $intEmailBloackTypeId, $strDomain, $objDatabase ) {

		$strSqlCondition = '';

		if( 0 < $intCid && true == is_numeric( $intCid ) ) {
			$strSqlCondition = ' AND cid = ' . ( int ) $intCid;
		}

		$strWhereSql = ' WHERE email_block_type_id = ' . ( int ) $intEmailBloackTypeId . ' AND lower( domain ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) . '\' ' . $strSqlCondition . ' LIMIT 1';

		return self::fetchDomainBlockCount( $strWhereSql, $objDatabase );
	}

	public static function fetchAllDomainBlocks( $objDatabase ) {

		$strSql = 'SELECT * FROM domain_blocks';

		return self::fetchDomainBlocks( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDomainBlocks( $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit	= ( int ) $intPageSize;

    	$strSql 	= 'SELECT
    						*
    					 FROM
    					 		domain_blocks
    			 	 ORDER BY
    			 				created_on DESC
    			   	   OFFSET
    			   				' . ( int ) $intOffset . '
    			   		LIMIT
    			   				' . $intLimit . ' ';

    	return self::fetchDomainBlocks( $strSql, $objDatabase );
    }

    public static function fetchPaginateDomainBlocksCount( $objDatabase ) {

    	$strSql			= 'SELECT count(id) FROM domain_blocks';
    	$arrintResponse	= fetchData( $strSql, $objDatabase );

    	if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

    	return 0;
    }

    public static function fetchDomainBlockByIds( $arrintDomainBlocksIds, $objDatabase ) {
    	if( false == valArr( $arrintDomainBlocksIds ) ) return NULL;
		$strSql = 'SELECT * FROM domain_blocks WHERE id IN ( ' . implode( ',', $arrintDomainBlocksIds ) . ' )';

    	return self::fetchDomainBlocks( $strSql, $objDatabase );
    }

    public static function fetchDomainBlockByEmailBlockTypeIds( $strEmailBlockTypeIds, $objDatabase ) {

    	$strSql = 'SELECT
						DISTINCT(lower(domain)) AS domain
					FROM
						domain_blocks
					WHERE
						email_block_type_id IN (  ' . $strEmailBlockTypeIds . ' ) ';

    	return self::fetchDomainBlocks( $strSql, $objDatabase );
    }

}
?>