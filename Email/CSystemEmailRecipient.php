<?php

class CSystemEmailRecipient extends CBaseSystemEmailRecipient {

	const DATE_FOR_EVENT_REFERENCE_CHANGE_FOR_MESSAGE_CENTER = '03/26/2015 02:00:00';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailId() {
		$boolIsValid = true;

		if( false == valId( $this->getSystemEmailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_id', 'System email id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRecipientTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getRecipientTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recipient_type_id', 'A valid recipient type id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Recipient does not contain email address.' ) );
		} elseif( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'A valid email address is required. Email address passed is ' . $this->getEmailAddress() . '.' ) );
		}
		return $boolIsValid;
	}

	public function valMergeFields() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSystemEmailId();
				$boolIsValid &= $this->valRecipientTypeId();
				$boolIsValid &= $this->valEmailAddress();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

	   if( true == \Psi\CStringService::singleton()->stristr( $this->getEmailAddress(), 'yardi.com' ) || true == \Psi\CStringService::singleton()->stristr( $this->getEmailAddress(), 'mrisoftware.com' ) ) {
	        $this->setEmailAddress( str_replace( 'yardi.com', 'yardi.lcl', $this->getEmailAddress() ) );
	        $this->setEmailAddress( str_replace( 'mrisoftware.com', 'mrisoftware.lcl', $this->getEmailAddress() ) );
	   }

		if( false == $boolReturnSqlOnly && CDatabaseType::EMAIL != $objDatabase->getDatabaseTypeId() ) {
			$objDatabase = CDatabases::createDatabase( CDatabaseUserType::PS_DEVELOPER, CDatabaseType::EMAIL );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function processViewInBrowser( &$arrmixMergeFields, $boolIsForMaintenance = false ) {
		if( 'production' == CConfig::get( 'environment' ) ) {
			$strBaseUrl = CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN;
		} else {
			$strBaseUrl = 'http://www.entrata.lcl';
		}

		$strViewInBrowserModule = $strBaseUrl . '/?module=scheduled_task_email&action=preview_email_content&key=';
		if( true == is_numeric( $this->getId() ) ) {
			$strViewInBrowserParams = '&system_email_recipient_id=' . $this->getId();

			$strViewInBrowserLink 	= $strViewInBrowserModule . \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( $strViewInBrowserParams, CONFIG_SODIUM_KEY_MESSAGE_CENTER_EMAIL );
			if( true == $boolIsForMaintenance ) {
				return $strViewInBrowserLink;
			}
			$arrmixMergeFields['*VIEW_IN_BROWSER_LINK*'] = '<a target="_blank" style="color:#0149A0;" href="' . $strViewInBrowserLink . '">View it in your browser</a>';
		} else {
			if( true == $boolIsForMaintenance ) {
				$arrmixMergeFields[CSystemEmail::MERGE_FIELD_PATTERN_ENCRYPT_URL_KEY] = '';
				return $strViewInBrowserModule . CSystemEmail::MERGE_FIELD_PATTERN_ENCRYPT_URL_KEY;
			}
			$arrmixMergeFields['*VIEW_IN_BROWSER_LINK*'] = '';
		}
	}

}
?>