<?php

class CSystemEmailAttachment extends CBaseSystemEmailAttachment {

	public function valSystemEmailId() {
		$boolIsValid = true;

		// if( true == is_null( $this->getSystemEmailId() ) ) {
		//	$boolIsValid = false;
		//	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_id', '' ) );
		// }

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Database Functions
	*/

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		$objDataset = $objDatabase->createDataset();

		$intId = $this->getId();

		if( true == is_null( $intId ) ) {
			$strSql = "SELECT nextval( 'system_email_attachments_id_seq'::text ) AS id";

			if( ( false == $objDataset->execute( $strSql ) ) || ( 1 != $objDataset->getRecordCount() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert system email attachment record. The following error was reported.' ) );
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

				$objDataset->cleanup();
				return false;
			}

			$arrValues = $objDataset->fetchArray();
			$this->setId( $arrValues['id'] );

			$objDataset->cleanup();
		}

		$strSql = 'SELECT * ' .
					'FROM system_email_attachments_insert( ' .
					$this->sqlId() . ', ' .
					$this->sqlSystemEmailId() . ', ' .
					$this->sqlEmailAttachmentId() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			// Reset id on error
			$this->setId( $intId );

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert system email attachment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to insert system email attachment record. The following error was reported.' ) );
			// Reset id on error
			$this->setId( $intId );

			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM system_email_attachments_update( ' .
					$this->sqlId() . ', ' .
					$this->sqlSystemEmailId() . ', ' .
					$this->sqlEmailAttachmentId() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update system email attachment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to update system email attachment record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		$objDataset = $objDatabase->createDataset();

		$strSql = 'SELECT * ' .
					'FROM system_email_attachments_delete( ' .
					$this->sqlId() . ', ' .
					( int ) $intCurrentUserId . ' ) AS result;';

		if( false == $objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete system email attachment record. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );

			$objDataset->cleanup();

			return false;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to delete system email attachment record. The following error was reported.' ) );
			while( false == $objDataset->eof() ) {
				$arrValues = $objDataset->fetchArray();
				$this->addErrorMsg( new CErrorMsg( $arrValues['type'], $arrValues['field'], $arrValues['message'] ) );
				$objDataset->next();
			}

			$objDataset->cleanup();

			return false;
		}

		$objDataset->cleanup();

		return true;
	}

}
?>