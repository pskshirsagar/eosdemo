<?php

class CEmailEventLink extends CBaseEmailEventLink {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailEventId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUrl() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valClickCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
        }

        return $boolIsValid;
    }
}
?>