<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailProviderConfigurations
 * Do not add any new functions to this class.
 */

class CEmailProviderConfigurations extends CBaseEmailProviderConfigurations {

	public static function fetchPublishedEmailServiceProviders( $objDatabase ) {
		$strSql = 'SELECT * FROM email_service_providers WHERE is_published = 1 ORDER BY order_num';

		return self::fetchEmailProviderConfiguration( $strSql, $objDatabase );
	}

	public static function fetchEmailProviderConfigurationByEmailServiceProviderIdByCid( $intEmailServiceProviderId, $intCid, $objDatabase ) {
		return self::fetchEmailProviderConfiguration( sprintf( 'SELECT * FROM email_provider_configurations WHERE email_service_provider_id = %d and cid = %d LIMIT 1', ( int ) $intEmailServiceProviderId, ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailProviderConfigurationByCidBySystemEmailTypeId( $intCid, $intSystemEmailTypeId, $objDatabase ) {
		$strSql = 'SELECT
						epc.*
					FROM
						email_provider_configurations epc
					JOIN system_email_types et ON ( et.email_service_provider_id = epc.email_service_provider_id AND et.id = ' . ( int ) $intSystemEmailTypeId . ' )
					WHERE
						epc.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchEmailProviderConfiguration( $strSql, $objDatabase );
	}

}
?>