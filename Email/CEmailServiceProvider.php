<?php

class CEmailServiceProvider extends CBaseEmailServiceProvider {

	const PSI						= 1;
	const SEND_GRID_SECONDARY_API	= 2;
	const SEND_GRID_SECONDARY_SMTP	= 3;
	const MAN_DRILL					= 5;
	const MAN_DRILL_SMTP			= 6;
	const SEND_GRID_PRIMARY_API		= 9;
	const SEND_GRID_PRIMARY_SMTP	= 10;
	const SMTP						= 11;
	const SEND_GRID_KMV_SMTP		= 12;
	const SEND_CLOUD_API			= 13;
	const DIRECT_MAIL_SMTP			= 14;

	const SEND_GRID_API_USER	= 'h63RdI6tPGcI/lKq5JV3P7stEOg+/eQlkFG7XeMemH0=';
	const SEND_GRID_API_KEY		= '3UUy/8fi1Yzqg4sbFa9sf0urwZnZKm+ECK2GIMrRWBg=';
	const MANDRILL_KEY			= 'WfsCAI7tlBSJEbpVqMBRMQ';

	public function getDecryptedUsername() {
		if( false == valStr( $this->getUsernameEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getUsernameEncrypted(), CONFIG_SODIUM_KEY_SEND_GRID_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_SEND_GRID_USERNAME ] );
	}

	public function getDecryptedPassword() {
		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_SEND_GRID_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_SEND_GRID_USERNAME ] );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>