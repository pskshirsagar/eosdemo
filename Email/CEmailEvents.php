<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailEvents
 * Do not add any new functions to this class.
 */

class CEmailEvents extends CBaseEmailEvents {

	public static function fetchDeliveredOpenedCountByCidByScheduledEmailTransmissionId( $intCid, $intScheduledEmailTransmissionId, $objEmailDatabase ) {

		if( false == is_numeric( $intScheduledEmailTransmissionId ) ) return NULL;

		$strSql = 'SELECT	SUM( delivered ) AS delivered,
							SUM( opened ) AS opened
					FROM (
						SELECT 	COUNT( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
								COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened
						FROM system_emails se
							  LEFT JOIN email_events ee ON (ee.system_email_id = se.id AND ee.cid = se.cid)
						WHERE se.cid = ' . ( int ) $intCid . '
								AND ee.scheduled_email_id IS NOT NULL
								AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
								AND se.created_on < \'03/06/2014\'
						UNION
						SELECT 	COUNT( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
								COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened
						FROM system_emails se
							LEFT JOIN system_email_recipients ser ON (ser.system_email_id = se.id AND ser.cid = se.cid)
							LEFT JOIN email_events ee ON (ee.system_email_id = se.id AND ee.system_email_recipient_id = ser.id AND ee.cid = se.cid)
						WHERE se.cid = ' . ( int ) $intCid . '
								AND ee.scheduled_email_id IS NOT NULL
								AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
								AND se.created_on >= \'03/06/2014\'
						) AS subsql';

		$arrmixRecord = fetchData( $strSql, $objEmailDatabase );

		return ( true == valArr( $arrmixRecord ) ) ? $arrmixRecord[0] : NULL;
	}

	public static function fetchDeliveredOpenedCountByCidByScheduledEmailTransmissionIds( $intCid, $arrintScheduledEmailTransmissionIds, $objEmailDatabase ) {

		if( false == valArr( $arrintScheduledEmailTransmissionIds ) ) return NULL;

		$strSql = 'SELECT
						MIN( scheduled_email_transmission_id ) as scheduled_email_transmission_id,
						SUM( delivered ) AS delivered,
						SUM( opened ) AS opened
					FROM
					(
						SELECT
							scheduled_email_transmission_id,
							COUNT( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
							COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened
						FROM
							system_emails se
							LEFT JOIN email_events ee ON (ee.system_email_id = se.id AND ee.cid = se.cid)
						WHERE
							se.cid = ' . ( int ) $intCid . '
							AND ee.scheduled_email_id IS NOT NULL
							AND se.scheduled_email_transmission_id IN ( ' . implode( ',', $arrintScheduledEmailTransmissionIds ) . ' )
							AND se.created_on < \'03/06/2014\'
						GROUP BY se.scheduled_email_transmission_id
						UNION
						SELECT
							scheduled_email_transmission_id,
							COUNT( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
							COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened
						FROM
							system_emails se
							LEFT JOIN system_email_recipients ser ON (ser.system_email_id = se.id AND ser.cid = se.cid)
							LEFT JOIN email_events ee ON (ee.system_email_id = se.id AND ee.system_email_recipient_id = ser.id AND ee.cid = se.cid)
						WHERE
							se.cid = ' . ( int ) $intCid . '
							AND ee.scheduled_email_id IS NOT NULL
							AND se.created_on >= \'03/06/2014\'
							AND se.scheduled_email_transmission_id IN ( ' . implode( ',', $arrintScheduledEmailTransmissionIds ) . ' )
						GROUP BY se.scheduled_email_transmission_id
					) AS subsql
				GROUP BY ( scheduled_email_transmission_id )';

		$arrmixResponse = fetchData( $strSql, $objEmailDatabase );

		return ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse : NULL;

	}

	public static function fetchEmailEventsCountByCidsByEmailEventTypeId( $arrintCids, $intEmailEventTypeId, $objEmailDatabase ) {

		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						COALESCE( COUNT( ee.id ), 0 ) AS count,
						ee.cid
					FROM
						email_events AS ee
						JOIN email_event_types AS eet ON ( eet.id = ee.email_event_type_id )
					WHERE
						ee.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND ee.email_event_type_id = ' . ( int ) $intEmailEventTypeId . '
						GROUP BY ee.cid';

		$arrintResponse = fetchData( $strSql, $objEmailDatabase );

		return ( true == valArr( $arrintResponse ) ) ? $arrintResponse : NULL;
	}

	public static function fetchEmailEventsCountByCidByPropertyIdsByEmailEventTypeId( $intCid, $arrintPropertyIds, $intEmailEventTypeId, $objEmailDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						COALESCE( COUNT( ee.id ), 0 ) AS count,
						se.property_id
					FROM
						email_events AS ee
						JOIN email_event_types AS eet ON( eet.id = ee.email_event_type_id )
						JOIN system_emails AS se ON( se.id = ee.system_email_id )
					WHERE
						se.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ee.email_event_type_id = ' . ( int ) $intEmailEventTypeId . '
						AND ee.cid = ' . ( int ) $intCid . '
					GROUP BY se.property_id';

			$arrintResponse = fetchData( $strSql, $objEmailDatabase );

			return ( true == valArr( $arrintResponse ) ) ? $arrintResponse : NULL;
	}

	public static function fetchSystemEmailsEventIdsBySystemEmailIds( $arrintSystemEmailIds, $objEmailDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql = ' SELECT
						ee.id as id,
						ee.email_event_type_id,
						ee.system_email_id,
						ee.response,
						ee.updated_on,
						ee.opened_on,
						ee.clicked_on
					FROM
						email_events ee
					WHERE
						ee.system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
					GROUP BY
						ee.system_email_id,
						ee.email_event_type_id,
						ee.id,
						ee.response,
						ee.updated_on,
						ee.opened_on,
						ee.clicked_on
					ORDER BY
						ee.id ';
		return self::fetchEmailEvents( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailEventsBySystemEmailIdsByCid( $arrintSystemEmailIds, $intCid, $objEmailDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql = ' 
                    SET STATEMENT_TIMEOUT TO \'5s\';
                    SELECT
						ee.id,
						ee.system_email_recipient_id,
						ee.email_event_type_id,
						ee.system_email_id,
						ee.updated_on,
						ee.opened_on,
						ee.clicked_on,
						ee.deferred_on,
						ee.bounced_on
					FROM
						email_events ee
						JOIN system_emails se ON ( se.id = ee. system_email_id AND ee.cid = se.cid AND ee.recipient_email_address = se.to_email_address )
					WHERE
						ee.cid = ' . ( int ) $intCid . '
						AND ee.system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )';

		return self::fetchEmailEvents( $strSql, $objEmailDatabase );
	}

	public static function fetchSystemEmailEventsBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objEmailDatabase ) {

		$strSql = 'SELECT 
						system_email_id, 
						email_event_type_id, 
						response 
					FROM 
						email_events 
					WHERE 
						cid = ' . ( int ) $intCid . ' 
						AND system_email_id = ' . ( int ) $intSystemEmailId . ' 
						ORDER BY id DESC';

		return fetchData( $strSql, $objEmailDatabase );
	}

}
?>