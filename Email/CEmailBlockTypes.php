<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailBlockTypes
 * Do not add any new functions to this class.
 */

class CEmailBlockTypes extends CBaseEmailBlockTypes {

	/**
	******************* Fetch Functions ***************
	*/
	public static function fetchEmailBlockTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmailBlockType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmailBlockType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmailBlockType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllEmailBlockTypes( $objMailDatabase ) {
		return  self::fetchEmailBlockTypes( 'SELECT * FROM email_block_types', $objMailDatabase );
	}

}
?>