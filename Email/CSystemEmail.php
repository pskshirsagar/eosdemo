<?php /** @noinspection ALL */

use Psi\Libraries\NewRelic\CNewRelic;
use Psi\Libraries\UtilPsHtmlMimeMail\CPsHtmlMimeMail;

class CSystemEmail extends CBaseSystemEmail {

	protected $m_intBounceEmailCount;
	protected $m_intEmailAttachmentCount;
	protected $m_intEmailEventId;
	protected $m_intEmailEventTypeId;
	protected $m_intIsResidentEmail;
	protected $m_intLeaseId;
	protected $m_intReadEmailCount;
	protected $m_intRecipientsCount;
	protected $m_intSystemEmailRecipientId;
	protected $m_intTotalEmailCount;
	protected $m_intSystemMessageId;
	protected $m_intTotalEmailAttachmentSize;

	protected $m_strAccountName;
	protected $m_strClientName;
	protected $m_strEmailEventBouncedOn;
	protected $m_strEmailEventClickedOn;
	protected $m_strEmailEventDeliveredOn;
	protected $m_strEmailEventName;
	protected $m_strEmailEventOpenedOn;
	protected $m_strEmailEventProcessedOn;
	protected $m_strEmailEventResponse;
	protected $m_strEmailEventSpammedOn;
	protected $m_strErrors;
	protected $m_strFileTypeSystemCode;
	protected $m_strFormattedEmailDate;
	protected $m_strFormattedFromEmailAddress;
	protected $m_strFormattedToEmailAddress;
	protected $m_strFromDate;
	protected $m_strHtmlContent;
	protected $m_strRecipientEmailAddress;
	protected $m_strSystemEmailTypeName;
	protected $m_strTextContent;
	protected $m_strToDate;
	protected $m_strHtmlContentPath;
	protected $m_strTextContentPath;
	protected $m_strAttachmentPath;
	protected $m_strNotes;
	protected $m_intHtmlFileSize;
	protected $m_intTextFileSize;

	protected $m_objClientDatabase;
	protected $m_objDocumentManager;
	protected $m_objObjectStorageGateway;
	protected $m_objEmailEvent;
	protected $m_objEmailServiceProvider;
	protected $m_objMimeEmail;
	protected $m_objSystemEmailAttachment;

	protected $m_arrobjSystemEmailRecipients;

	const MAX_REQUEUE_ATTEMPT                        = 5;
	const MAX_TTL                                    = 30000;
	const QUEUE_NAME                                 = 'system.emails';
	const EXCHANGE_NAME                              = 'system.email';
	const EXCHANGE_TYPE                              = 'fanout';
	const MAX_QUEUE_PRIORITY                         = 2;
	const MERGE_FIELD_PATTERN_SYSTEMEMAIL_RECIPIENTS = '__SystemEmailRecipientId__';
	const MERGE_FIELD_PATTERN_ENCRYPT_URL_KEY        = '__SystemEmailRecipientEncryptUrlKey__';
	const MAX_EMAIL_CONTET_SIZE                      = 20; // MB
	// release@xento.com group contains all the email addresses from Devlopment & QA groups.
	const XENTO_RELEASE_EMAIL_ADDRESS  					= 'release@xento.com';
	const CODE_RELEASE_XENTO_EMAIL_ADDRESS				= 'coderelease@xento.com';
	const XENTO_HR_EMAIL_ADDRESS						= 'PeopleOps@xento.com';
	const XENTO_HRIS_EMAIL_ADDRESS						= 'POIS@xento.com';
	const XENTO_HRIS_LABELED_EMAIL_ADDRESS 				= 'PeopleOpsInfoSystem<POIS@xento.com>';
	const XENTO_CAREERS_EMAIL_ADDRESS					= 'careers@xento.com';
	const XENTO_QA_MANAGER_EMAIL_ADDRESS				= 'pausekar@xento.com';
	const XENTO_DEPLOY_EMAIL_ADDRESS					= 'deploy@xento.com';
	const XENTO_DBA_EMAIL_ADDRESS						= 'dba@xento.com';
	const XENTO_DBHELPDESK_EMAIL_ADDRESS				= 'dbhelpdesk@xento.com';
	const XENTO_JENKINS_EMAIL_ADDRESS					= 'jenkins@xento.com';
	const XENTO_TRAINING_EMAIL_ADDRESS					= 'lnd@xento.com';
	const XENTO_LEARNING_EMAIL_ADDRESS					= 'learning@xento.com';
	const XENTO_ADMINHELPDESK_EMAIL_ADDRESS				= 'adminhelpdesk@xento.com';
	const XENTO_DIRECTORS_EMAIL_ADDRESS 				= 'directors@xento.com';
	const XENTO_DIRECTOR_EMAIL_ADDRESS 					= 'sgarud@entrata.com';
	const XENTO_MD_EMAIL_ADDRESS 						= 'pyadav@entrata.com';
	const XENTO_AUTOMATION_EMAIL_ADDRESS				= 'automation@xento.com';
	const XENTO_ADDS_EMAIL_ADDRESS 						= 'directorsofdevelopment@xento.com';
	const XENTO_TPM_EMAIL_ADDRESS 						= 'tpm@xento.com';
	const XENTO_CODEMERGING_EMAIL_ADDRESS 				= 'codemerging@xento.com';
	const XENTO_DEVOPSSUPPORT_EMAIL_ADDRESS 			= 'devopssupport@xento.com';
	const XENTO_DBASUPPORT_EMAIL_ADDRESS 				= 'dbasupport@xento.com';
	const XENTO_DBAHELPDESK_EMAIL_ADDRESS 				= 'dbahelpdesk@xento.com';
	const XENTO_DEVOPSHELPDESK_EMAIL_ADDRESS 			= 'devopshelpdesk@xento.com';
	const XENTO_VDBA_EMAIL_ADDRESS						= 'engineeringdba@xento.com';
	const XENTO_PAYROLL_EMAIL_ADDRESS					= 'payroll@xento.com';
	const XENTO_HRHELPDESK_EMAIL_ADDRESS				= 'PeopleAssistance@xento.com';
	const XENTO_FINANCE_EMAIL_ADDRESS					= 'finance@xento.com';
	const XENTO_INDIASECURITYTEAM_EMAIL_ADDRESS			= 'indiasecurityteam@googlegroups.com';

	const XENTO_AAZIZ_EMAIL_ADDRESS						= 'aaziz@xento.com';
	const XENTO_ABAMNODKAR_EMAIL_ADDRESS				= 'abamnodkar@xento.com';
	const XENTO_ABEDI_EMAIL_ADDRESS						= 'abedi@xento.com';
	const XENTO_ABHOSKAR_EMAIL_ADDRESS					= 'abhoskar@xento.com';
	const XENTO_ACHANDEKAR_EMAIL_ADDRESS                = 'achandekar@xento.com';
	const XENTO_AEKUNDE_EMAIL_ADDRESS                   = 'aekunde@xento.com';
	const XENTO_AKHAMBALKAR_EMAIL_ADDRESS               = 'akhambalkar@xento.com';
	const XENTO_HADALKONDA_EMAIL_ADDRESS				= 'hadalkonda@xento.com';
	const XENTO_ADAS_EMAIL_ADDRESS						= 'adas@xento.com';
	const XENTO_ADESHPANDE_EMAIL_ADDRESS				= 'adeshpande@xento.com';
	const XENTO_JAVHALE_EMAIL_ADDRESS					= 'javhale@xento.com';
	const XENTO_VPATIL01_EMAIL_ADDRESS					= 'vpatil01@xento.com';
	const XENTO_SICHAKE_EMAIL_ADDRESS					= 'sichake@xento.com';
	const XENTO_ADURUGKAR_EMAIL_ADDRESS					= 'adurugkar@xento.com';
	const XENTO_AHARAL_EMAIL_ADDRESS					= 'aharal@xento.com';
	const XENTO_AJAISWAL01_EMAIL_ADDRESS				= 'ajaiswal01@xento.com';
	const XENTO_AJOSHI01_EMAIL_ADDRESS					= 'ajoshi01@xento.com';
	const XENTO_AKHEDKAR_EMAIL_ADDRESS					= 'akhedkar@xento.com';
	const XENTO_AKUMAR02_EMAIL_ADDRESS					= 'akumar02@xento.com';
	const XENTO_AKUMARI_EMAIL_ADDRESS					= 'akumari@xento.com';
	const XENTO_ALANGADE_EMAIL_ADDRESS					= 'alangade@xento.com';
	const XENTO_AMISHRA_EMAIL_ADDRESS					= 'amishra@xento.com';
	const XENTO_SDESHMUKH_EMAIL_ADDRESS					= 'sdeshmukh@xento.com';
	const XENTO_SBHILARE_EMAIL_ADDRESS					= 'sbhilare@xento.com';
	const XENTO_ADHUMAL_EMAIL_ADDRESS					= 'adhumal@xento.com';
	const XENTO_ASAHU_EMAIL_ADDRESS						= 'asahu@xento.com';
	const XENTO_ANEEMA_EMAIL_ADDRESS					= 'aneema@xento.com';
	const XENTO_APADMAWAR_EMAIL_ADDRESS					= 'apadmawar@xento.com';
	const XENTO_APRASAD_EMAIL_ADDRESS					= 'aprasad@xento.com';
	const XENTO_ARPRASAD_EMAIL_ADDRESS					= 'arprasad@xento.com';
	const XENTO_ASGAIKWAD_EMAIL_ADDRESS					= 'asgaikwad@xento.com';
	const XENTO_ASHERKAR_EMAIL_ADDRESS					= 'asherkar@xento.com';
	const XENTO_ASHIDID_EMAIL_ADDRESS					= 'ashidid@xento.com';
	const XENTO_ASHINDE02_EMAIL_ADDRESS					= 'ashinde02@xento.com';
	const XENTO_ASINGH_EMAIL_ADDRESS					= 'asingh@xento.com';
	const XENTO_ATHOMAS_EMAIL_ADDRESS					= 'athomas@xento.com';
	const XENTO_AVERMA_EMAIL_ADDRESS					= 'averma@xento.com';
	const XENTO_AKUKREJA_EMAIL_ADDRESS					= 'akukreja@xento.com';
	const XENTO_BGADGE_EMAIL_ADDRESS					= 'bgadge@xento.com';
	const XENTO_BDHADGE_EMAIL_ADDRESS					= 'bdhadge@xento.com';
	const XENTO_CKUMAR_EMAIL_ADDRESS					= 'ckumar@xento.com';
	const XENTO_CSAHU_EMAIL_ADDRESS						= 'csahu@xento.com';
	const XENTO_BNAGALKAR_EMAIL_ADDRESS					= 'bnagalkar@xento.com';
	const XENTO_DBORSE_EMAIL_ADDRESS                    = 'dborse@xento.com';
	const XENTO_DESIGN_EMAIL_ADDRESS					= 'design@xento.com';
	const XENTO_DEVOPS_EMAIL_ADDRESS					= 'devops@xento.com';
	const XENTO_DIAGNOSTIC_EMAIL_ADDRESS				= 'diagnostic@xento.com';
	const XENTO_DKAMLE_EMAIL_ADDRESS					= 'dkamle@xento.com';
	const XENTO_DKUREKAR_EMAIL_ADDRESS					= 'dkurekar@xento.com';
	const XENTO_DNIRMAL_EMAIL_ADDRESS					= 'dnirmal@xento.com';
	const XENTO_DOTNET_EMAIL_ADDRESS					= 'dotnet@xento.com';
	const XENTO_DSOMA_EMAIL_ADDRESS						= 'dsoma@xento.com';
	const XENTO_ERIOS_EMAIL_ADDRESS						= 'erios@entrata.com';
	const XENTO_EVERYONE_EMAIL_ADDRESS					= 'everyone@xento.com';
	const XENTO_GBHISE_EMAIL_ADDRESS					= 'gbhise@xento.com';
	const XENTO_GCHAVAN_EMAIL_ADDRESS					= 'gchavan@xento.com';
	const XENTO_GDARKUNDE_EMAIL_ADDRESS					= 'gdarkunde@xento.com';
	const XENTO_GDESALE_EMAIL_ADDRESS					= 'gdesale@xento.com';
	const XENTO_GMADANE_EMAIL_ADDRESS					= 'gmadane@xento.com';
	const XENTO_HDANGE_EMAIL_ADDRESS					= 'hdange@xento.com';
	const XENTO_HMAHAJAN01_EMAIL_ADDRESS				= 'hmahajan01@xento.com';
	const XENTO_HVICHARE_EMAIL_ADDRESS					= 'hvichare@xento.com';
	const XENTO_ITHELPDESK_EMAIL_ADDRESS				= 'ithelpdesk@xento.com';
	const XENTO_KAADE_EMAIL_ADDRESS						= 'kaade@xento.com';
	const XENTO_KALPANA_EMAIL_ADDRESS					= 'kdevi@xento.com';
	const XENTO_KJADHAV01_EMAIL_ADDRESS					= 'kjadhav01@xento.com';
	const XENTO_KKULKARNI_EMAIL_ADDRESS					= 'kkulkarni@xento.com';
	const XENTO_LAXMIK_EMAIL_ADDRESS					= 'lkalake@xento.com';
	const XENTO_LSONAWANE_EMAIL_ADDRESS					= 'lsonawane@xento.com';
	const XENTO_MARIEF_EMAIL_ADDRESS					= 'marief@xento.com';
	const XENTO_KMAGDUM_EMAIL_ADDRESS					= 'kmagdum@xento.com';
	const XENTO_MCHOUDHARY_EMAIL_ADDRESS				= 'mchoudhary@xento.com';
	const XENTO_MDEB_EMAIL_ADDRESS						= 'mdeb@xento.com';
	const XENTO_MMARATHE_EMAIL_ADDRESS					= 'mmarathe@xento.com';
	const XENTO_MMUNDHE_EMAIL_ADDRESS					= 'mmundhe@xento.com';
	const XENTO_MPATEL_EMAIL_ADDRESS					= 'mpatel@xento.com';
	const XENTO_NBANNAGARE_EMAIL_ADDRESS				= 'nbannagare@xento.com';
	const XENTO_NIBIN_EMAIL_ADDRESS						= 'nnaduvilveetil@xento.com';
	const XENTO_NJAGTAP_EMAIL_ADDRESS					= 'njagtap@xento.com';
	const XENTO_NKADAM_EMAIL_ADDRESS					= 'nkadam@xento.com';
	const XENTO_NKUMAR01_EMAIL_ADDRESS					= 'nkumar01@xento.com';
	const XENTO_NKUSALKAR_EMAIL_ADDRESS					= 'nkusalkar@xento.com';
	const XENTO_NNAMDEV_EMAIL_ADDRESS					= 'nnamdev@xento.com';
	const XENTO_NNETI_EMAIL_ADDRESS 					= 'nneti@xento.com';
	const XENTO_NPAWAR_EMAIL_ADDRESS					= 'npawar@xento.com';
	const XENTO_NSUNNY_EMAIL_ADDRESS					= 'nsunny@xento.com';
	const XENTO_MSHENDE_EMAIL_ADDRESS                   = 'mshende@xento.com';
	const XENTO_PANKLESHWARIA_EMAIL_ADDRESS				= 'pankleshwaria@xento.com';
	const XENTO_PAUSEKAR_EMAIL_ADDRESS                  = 'pausekar@xento.com';
	const XENTO_PAYADAV_EMAIL_ADDRESS					= 'payadav@xento.com';
	const XENTO_PBANG_EMAIL_ADDRESS					    = 'pbang@xento.com';
	const XENTO_PDIGHE_EMAIL_ADDRESS					= 'pdighe@xento.com';
	const XENTO_PHEMBADE_EMAIL_ADDRESS					= 'phembade@xento.com';
	const XENTO_PHP_EMAIL_ADDRESS						= 'php@xento.com';
	const XENTO_PKALE01_EMAIL_ADDRESS					= 'pkale01@xento.com';
	const XENTO_PKATHURIA_EMAIL_ADDRESS					= 'pkathuria@xento.com';
	const XENTO_PKOTHAWADE_EMAIL_ADDRESS				= 'pkothawade@xento.com';
	const XENTO_PMANE_EMAIL_ADDRESS						= 'pmane@xento.com';
	const XENTO_PMESHRAM_EMAIL_ADDRESS					= 'pmeshram@xento.com';
	const XENTO_PPATIL02_EMAIL_ADDRESS					= 'ppatil02@xento.com';
	const XENTO_PPAWAR02_EMAIL_ADDRESS					= 'ppawar02@xento.com';
	const XENTO_PRCHAUDHARI_EMAIL_ADDRESS				= 'prchaudhari@xento.com';
	const XENTO_PSAKAT_EMAIL_ADDRESS					= 'psakat@xento.com';
	const XENTO_QAM_EMAIL_ADDRESS						= 'QAM@xento.com';
	const XENTO_QA_EMAIL_ADDRESS						= 'qa@xento.com';
	const XENTO_RBHADAURIA_EMAIL_ADDRESS				= 'rbhadauria@xento.com';
	const XENTO_RGADRE_EMAIL_ADDRESS					= 'rgadre@xento.com';
	const XENTO_RGANGULY_EMAIL_ADDRESS					= 'rganguly@xento.com';
	const XENTO_RGHADAGE_EMAIL_ADDRESS					= 'rghadage@xento.com';
	const XENTO_RG_EMAIL_ADDRESS						= 'rg@xento.com';
	const XENTO_RKALE_EMAIL_ADDRESS						= 'rkale@xento.com';
	const XENTO_RKAWALKAR_EMAIL_ADDRESS					= 'rkawalkar@xento.com';
	const XENTO_RNAGPURE_EMAIL_ADDRESS					= 'rnagpure@xento.com';
	const XENTO_RPADWAL_EMAIL_ADDRESS					= 'rpadwal@xento.com';
	const XENTO_RPARDESHI_EMAIL_ADDRESS 				= 'rpardeshi@xento.com';
	const XENTO_RPAWAR01_EMAIL_ADDRESS					= 'rpawar01@xento.com';
	const XENTO_RSHAIKH_EMAIL_ADDRESS					= 'rshaikh@xento.com';
	const XENTO_RSHARMA_EMAIL_ADDRESS					= 'rsharma@xento.com';
	const XENTO_RSHUKLA_EMAIL_ADDRESS					= 'rshukla@xento.com';
	const XENTO_RSINGH01_EMAIL_ADDRESS					= 'rsingh01@xento.com';
	const XENTO_RSONI_EMAIL_ADDRESS						= 'rsoni@xento.com';
	const XENTO_RVYAS_EMAIL_ADDRESS						= 'rvyas@xento.com';
	const XENTO_RKURANE_EMAIL_ADDRESS                   = 'rkurane@xento.com';
	const XENTO_RWAGHMARE_EMAIL_ADDRESS					= 'rwaghmare@xento.com';
	const XENTO_SALURKAR_EMAIL_ADDRESS					= 'salurkar@xento.com';
	const XENTO_SAMBULKAR_EMAIL_ADDRESS					= 'sambulkar@xento.com';
	const XENTO_SBEGUM_EMAIL_ADDRESS					= 'sbegum@xento.com';
	const XENTO_SBHANDARI_EMAIL_ADDRESS					= 'sbhandari@xento.com';
	const XENTO_SBHARAINWALA_EMAIL_ADDRESS				= 'sbahrainwala@xento.com';
	const XENTO_SBHAWARE_EMAIL_ADDRESS					= 'sbhaware@xento.com';
	const XENTO_SDURGE_EMAIL_ADDRESS					= 'sdurge@entrata.com';
	const XENTO_SGARUD_EMAIL_ADDRESS					= 'sgarud@xento.com';
	const XENTO_SGODAGE_EMAIL_ADDRESS					= 'sgodage@xento.com';
	const XENTO_SGUNDE_EMAIL_ADDRESS					= 'sgunde@xento.com';
	const XENTO_SJADHAV02_EMAIL_ADDRESS					= 'sjadhav02@xento.com';
	const XENTO_SJADHAV08_EMAIL_ADDRESS					= 'sjadhav08@xento.com';
	const XENTO_SJAINOJI_EMAIL_ADDRESS					= 'sjainoji@xento.com';
	const XENTO_SKHATTE_EMAIL_ADDRESS					= 'skhatte@xento.com';
	const XENTO_SMAKWANA_EMAIL_ADDRESS					= 'smakwana@xento.com';
	const XENTO_SMASHAL_EMAIL_ADDRESS					= 'smashal@xento.com';
	const XENTO_SMOGHE01_EMAIL_ADDRESS					= 'smoghe01@xento.com';
	const XENTO_SPRABHAKARAN_EMAIL_ADDRESS				= 'sprabhakaran@xento.com';
	const XENTO_SSATARKE_EMAIL_ADDRESS					= 'ssatarke@xento.com';
	const XENTO_SSHRIWASTAV_EMAIL_ADDRESS				= 'sshriwastav@xento.com';
	const XENTO_SSINGH03_EMAIL_ADDRESS					= 'ssingh03@xento.com';
	const XENTO_SSINGH_EMAIL_ADDRESS					= 'ssingh@xento.com';
	const XENTO_SVYAWAHARE_EMAIL_ADDRESS				= 'svyawahare@xento.com';
	const XENTO_SWABLE_EMAIL_ADDRESS					= 'swable@xento.com';
	const XENTO_SYELMAME_EMAIL_ADDRESS					= 'syelmame@xento.com';
	const XENTO_SYSYTEM_EMAIL_ADDRESS					= 'system@xento.com';
	const XENTO_TBANGAR_EMAIL_ADDRESS					= 'tbangar@xento.com';
	const XENTO_TBHATT_EMAIL_ADDRESS					= 'tbhatt@xento.com';
	const XENTO_TBOJJA_EMAIL_ADDRESS					= 'tbojja@xento.com';
	const XENTO_TMANDHANE_EMAIL_ADDRESS					= 'tmandhane@xento.com';
	const XENTO_RGAJARE_EMAIL_ADDRESS					= 'rgajare@xento.com';
	const XENTO_USHINDE_EMAIL_ADDRESS					= 'ushinde@xento.com';
	const XENTO_UVANVE_EMAIL_ADDRESS					= 'uvanve@xento.com';
	const XENTO_VMOHITE_EMAIL_ADDRESS					= 'vmohite@xento.com';
	const XENTO_VNAGLE_EMAIL_ADDRESS					= 'vnagle@xento.com';
	const XENTO_VNALAWADE_EMAIL_ADDRESS					= 'vnalawade@xento.com';
	const XENTO_VNAVALE_EMAIL_ADDRESS					= 'vnavale@xento.com';
	const XENTO_VPAWAR_EMAIL_ADDRESS					= 'vpawar@xento.com';
	const XENTO_VPAWAR01_EMAIL_ADDRESS					= 'vpawar01@xento.com';
	const XENTO_VTIWARI_EMAIL_ADDRESS					= 'vtiwari@xento.com';
	const XENTO_VTOPADEBUWA_EMAIL_ADDRESS				= 'vtopadebuwa@xento.com';
	const XENTO_YBORKAR_EMAIL_ADDRESS					= 'yborkar@xento.com';
	const XENTO_YKAPARE_EMAIL_ADDRESS					= 'ykapare@xento.com';
	const XENTO_YNIKAM_EMAIL_ADDRESS					= 'ynikam@xento.com';
	const XENTO_YPADEKAR_EMAIL_ADDRESS					= 'ypadekar@xento.com';
	const XENTO_YPOONAWALA_EMAIL_ADDRESS				= 'ypoonawala@xento.com';
	const XENTO_YRENUKE_EMAIL_ADDRESS					= 'yrenuke@xento.com';
	const XNETO_RJAIN_EMAIL_ADDRESS						= 'rjain@xento.com';
	const XENTO_SKSHIRSAGAR01_EMAIL_ADDRESS				= 'skshirsagar01@xento.com';
	const XENTO_SKULKARNI02_EMAIL_ADDRESS				= 'skulkarni02@xento.com';
	const XENTO_SJAISWAL_EMAIL_ADDRESS					= 'sjaiswal@xento.com';
	const XENTO_SBARSE_EMAIL_ADDRESS					= 'sbarse@xento.com';
	const XENTO_SWAWGE_EMAIL_ADDRESS					= 'swawge@xento.com';
	const XENTO_YABHALE_EMAIL_ADDRESS					= 'yabhale@xento.com';
	const XENTO_SKULKARNI_EMAIL_ADDRESS					= 'skulkarni@xento.com';
	const XENTO_MCHAVAN_EMAIL_ADDRESS					= 'mchavan@xento.com';
	const XENTO_NWANI_EMAIL_ADDRESS				        = 'nwani@xento.com';
	const XENTO_DKOTHARI_EMAIL_ADDRESS					= 'dkothari@xento.com';
	const XENTO_RMOHITE_EMAIL_ADDRESS                   = 'rmohite@xento.com';
	const XENTO_SBHOSALE_EMAIL_ADDRESS                  = 'sbhosale@xento.com';
	const XENTO_GSHINDE_EMAIL_ADDRESS                   = 'gshinde@xento.com';
	const XENTO_GMANE_EMAIL_ADDRESS						= 'gmane@xento.com';
	const XENTO_RMARAL_EMAIL_ADDRESS					= 'rmaral@xento.com';
	const XENTO_SVARTAK_EMAIL_ADDRESS					= 'svartak@xento.com';
	const XENTO_SWANI_EMAIL_ADDRESS						= 'swani@xento.com';
	const XENTO_RNAVALE_EMAIL_ADDRESS					= 'rnavale@xento.com';
	const XENTO_AJAMWADIKAR_EMAIL_ADDRESS				= 'ajamwadikar@xento.com';
	const XENTO_PKUMAR_EMAIL_ADDRESS				    = 'pkumar@xento.com';
	const XENTO_PLOHAKARE_EMAIL_ADDRESS				    = 'plohakare@xento.com';
	const XENTO_JJAGTAP_EMAIL_ADDRESS				    = 'jjagtap@xento.com';
	const XENTO_PDHAKAD_EMAIL_ADDRESS                   = 'pdhakad@xento.com';
	const XENTO_KSANGHAI_EMAIL_ADDRESS                  = 'ksanghai@xento.com';
	const XENTO_SDURAPHE_EMAIL_ADDRESS                  = 'sduraphe@xento.com';
	const XENTO_VHADAP_EMAIL_ADDRESS					= 'vhadap@xento.com';
	const XENTO_ASAYYAD_EMAIL_ADDRESS					= 'asayyad@xento.com';
	const XENTO_SMULANI_EMAIL_ADDRESS					= 'smulani@xento.com';
	const XENTO_PWAYAL_EMAIL_ADDRESS                    = 'pwayal@xento.com';
	const XENTO_SPATIL04_EMAIL_ADDRESS                  = 'spatil04@xento.com';
	const XENTO_RKUMAR_EMAIL_ADDRESS                    = 'rkumar@xento.com';
	const XENTO_VRAPARTI_EMAIL_ADDRESS                  = 'vraparti@xento.com';
	const XENTO_APARANDWAL_EMAIL_ADDRESS                = 'aparandwal@xento.com';
	const XENTO_SSINGH07_EMAIL_ADDRESS                  = 'ssingh07@xento.com';
	const XENTO_AKUDACHE_EMAIL_ADDRESS                  = 'akudache@xento.com';
	const XENTO_VTHORVE_EMAIL_ADDRESS                   = 'vthorve@xento.com';
	const XENTO_NJADHAV_EMAIL_ADDRESS                   = 'njadhav@xento.com';
	const XENTO_VJADHAV04_EMAIL_ADDRESS                 = 'vjadhav04@xento.com';
	const XENTO_SGUPTA02_EMAIL_ADDRESS                  = 'sgupta02@xento.com';
	const XENTO_SJOSHI05_EMAIL_ADDRESS                  = 'sjoshi05@xento.com';
	const XENTO_SBHASE_EMAIL_ADDRESS                    = 'sbhase@xento.com';
	const XENTO_VDEORE_EMAIL_ADDRESS                    = 'vdeore@xento.com';
	const XENTO_AGAIKWAD02_EMAIL_ADDRESS                = 'agaikwad02@xento.com';
	const XENTO_SSEERAPU_EMAIL_ADDRESS					= 'sseerapu@entrata.com';
	const XENTO_SSHAH_EMAIL_ADDRESS						= 'sshah@xento.com';
	const XENTO_HMAHAJAN_EMAIL_ADDRESS					= 'hmahajan@xento.com';
	const XENTO_SHEREKAR_EMAIL_ADDRESS					= 'sherekar@xento.com';
	const XENTO_MYADAV_EMAIL_ADDRESS					= 'myadav@xento.com';
	const XENTO_RWAYAL_EMAIL_ADDRESS					= 'rwayal@xento.com';
	const XENTO_PDEOKAR_EMAIL_ADDRESS					= 'pdeokar@xento.com';

	const XENTO_SSATARKAR_EMAIL_ADDRESS					= 'ssatarkar@xento.com';
	const XENTO_SKATKE_EMAIL_ADDRESS					= 'skatke@xento.com';
	const XENTO_RSONAWANE_EMAIL_ADDRESS                 = 'rsonawane@xento.com';
	const XENTO_RJACHAK_EMAIL_ADDRESS					= 'rjachak@xento.com';
	const XENTO_CGADGILWAR_EMAIL_ADDRESS				= 'cgadgilwar@xento.com';
	const XENTO_VTIJGE_EMAIL_ADDRESS					= 'vtijge@xento.com';
	const XENTO_HKATWATE_EMAIL_ADDRESS					= 'hkatwate@xento.com';
	const ERIC_RIOS_EMAIL_ADDRESS		                = 'erios@entrata.com';
	const CECIL_THOMAS_EMAIL_ADDRESS		            = 'cthomas@entrata.com';
	const ENTRATA_TIM_STANDISH_EMAIL_ADDRESS            = 'tstandish@entrata.com';
	const ENTRATA_ONICA_HANBY_EMAIL_ADDRESS             = 'ohanby@entrata.com';
	const ENTRATA_MICHAEL_LEWIS_EMAIL_ADDRESS			= 'mlewis@entrata.com';
	const ENTRATA_JOSHUA_ELLINGTON_EMAIL_ADDRESS		= 'jellington@entrata.com';
	const ENTRATA_MICHEAL_HUFF_EMAIL_ADDRESS		    = 'mhuff@entrata.com';
	const ENTRATA_SQM_EMAIL_ADDRESS						= 'sqm@entrata.com';
	const ENTRATA_ITHELPDESK_EMAIL_ADDRESS				= 'ithelpdesk@entrata.com';
	const ENTRATA_TSPENCER_EMAIL_ADDRESS				= 'tspencer@entrata.com';
	const ENTRATA_CMOON_EMAIL_ADDRESS				    = 'cmoon@entrata.com';

	const ENTRATA_RELEASE_EMAIL_ADDRESS 		        = 'release@entrata.com';
	const TERMINATIONS_EMAIL_ADDRESS					= 'terminations@entrata.com';
	const PROPERTYSOLUTIONS_BILLING_EMAIL_ADDRESS 		= 'billing@entrata.com';
	const ENTRATA_UTILITY_SUPPORT_EMAIL_ADDRESS 		= 'utilitysupport@entrata.com';
	const ENTRATA_BILLING_EMAIL_ADDRESS 		        = 'billing@entrata.com';
	const PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS		= 'system@entrata.com';
	const NPS_EMAIL_ADDRESS								= 'nps@entrata.com';
	const PROPERTYSOLUTIONS_HR_EMAIL_ADDRESS			= 'hr@entrata.com';
	const PROPERTYSOLUTIONS_TEST_EMAIL_ADDRESS			= 'tests@entrata.com';
	const PROPERTYSOLUTIONS_SALES_EMAIL_ADDRESS			= 'sales@entrata.com';
	const PROPERTYSOLUTIONS_CEO_EMAIL_ADDRESS			= 'dbateman@entrata.com';
	const PROPERTYSOLUTIONS_QADIRECTOR_EMAIL_ADDRESS	= 'bbreivik@entrata.com';
	const HR_RESUMES_EMAIL_ADDRESS	                    = 'hrresumes@entrata.com';
	const GENERALSUPPORT_EMAIL_ADDRESS					= 'generalsupport@entrata.com';
	const INFO_EMAIL_ADDRESS							= 'info@entrata.com';
	const UTILITYSUPPORT_EMAIL_ADDRESS					= 'utilitysupport@entrata.com';
	const ECHO_EMAIL_ADDRESS							= 'echo@entrata.com';
	const MERCHANTSERVICES_EMAIL_ADDRESS				= 'merchantservices@entrata.com';
	const PROCESSING_EMAIL_ADDRESS						= 'processing@entrata.com';
	const AGREEMENTS_EMAIL_ADDRESS						= 'agreements@entrata.com';
	const CONSUMERREQUESTS_EMAIL_ADDRESS				= 'consumerreports@entrata.com';
	const NOREPLY_EMAIL_ADDRESS							= 'no-reply@entrata.com';
	const CONTRACTS_EMAIL_ADDRESS						= 'contracts@entrata.com';
	const DONOTREPLY_EMAIL_ADDRESS						= 'donotreply@entrata.com';
	const TASK_EMAIL_ADDRESS							= 'tasks@entrata.com';
	const MIGRATIONS_EMAIL_ADDRESS						= 'migrations@entrata.com';
	const REVIEW_TEAM_MANAGER_EMAIL_ADDRESS				= 'reviewteammanager@entrata.com';
	const ARCHITECTS_EMAIL_ADDRESS						= 'architects@entrata.com';
	const CALLDISPUTES_EMAIL_ADDRESS					= 'calldisputes@entrata.com';
	const DATAPRIVACY_EMAIL_ADDRESS						= 'dataprivacy@entrata.com';

	const TOOLTIPS_EMAIL_ADDRESS						= 'tooltips@entrata.com';
	const LEGAL_EMAIL_ADDRESS							= 'legal@entrata.com';
	const CRONJOBS_EMAIL_ADDRESS						= 'cronjobs@entrata.com';
	const DEVSUPPORT_EMAIL_ADDRESS						= 'devsupport@entrata.com';
	const SUPPORT_EMAIL_ADDRESS							= 'support@entrata.com';
	const RESIDENT_PORTAL_APP_SUPPORT_EMAIL_ADDRESS		= 'rpapp+feedback@entrata.com';
	const ITSUPPORT_EMAIL_ADDRESS						= 'itsupport@entrata.com';
	const REPMANAGEMENTSUPPORT_EMAIL_ADDRESS 			= 'reputationadvisor@entrata.com';
	const DEV_SUPPORT_EMAIL_ADDRESS						= 'dev_support@entrata.com';
	const UNDERWRITING_EMAIL_ADDRESS					= 'underwriting@entrata.com';
	const ILSNOTICES_EMAIL_ADDRESS						= 'ilsnotices@entrata.com';
	const NEWSALE_EMAIL_ADDRESS							= 'newsale@entrata.com';
	const LCIMPLEMENTATION_EMAIL_ADDRESS				= 'lcimplementation@entrata.com';
	const ILSPORTAL_EMAIL_ADDRESS						= 'ilsportal@entrata.com';
	const INTEGRATION_EMAIL_ADDRESS						= 'integration@entrata.com';
	const GOINGPAPERLESS_EMAIL_ADDRESS					= 'goingpaperless@entrata.com';
	const LEADINQUIRIESS_EMAIL_ADDRESS					= 'leadinquiries@entrata.com';
	const OUTAGE_EMAIL_ADDRESS							= 'outage@entrata.com';
	const OUTAGEEMAIL_EMAIL_ADDRESS						= 'outage-email.entrata.com';
	const DEPOSITS_EMAIL_ADDRESS						= 'deposits@entrata.com';
	const DISRUPTINGTHESCENE_EMAIL_ADDRESS				= 'disruptingthescene@entrata.com';
	const TX_EMAIL_ADDRESS								= 'tx@entrata.com';
	const LEASINGCENTER_EMAIL_ADDRESS					= 'leasingcenter@entrata.com';
	const WEBMASTER_EMAIL_ADDRESS						= 'webmaster@entrata.com';
	const COLOSONICWALL_EMAIL_ADDRESS					= 'colosonicwall@entrata.com';
	const IT_EMAIL_ADDRESS								= 'it@entrata.com';
	const WEEKLYSALESREPORTS_EMAIL_ADDRESS				= 'weeklysalesreports@entrata.com';
	const NEWSLETTERS_EMAIL_ADDRESS						= 'newsletters@entrata.com';
	const PINGDOM_EMAIL_ADDRESS							= 'pingdom@entrata.com';
	const PRODUCTDEVMANAGERS_EMAIL_ADDRESS				= 'ProductDevManagers@entrata.com';
	const LOGWATCH_EMAIL_ADDRESS						= 'logwatch@entrata.com';
	const FOUNTAINOFKNOWLEDGE_EMAIL_ADDRESS				= 'fountainofknowledge@entrata.com';
	const ENTRATASWATTEAM_EMAIL_ADDRESS					= 'entrataswatteam@entrata.com';
	const AUDITUSER_EMAIL_ADDRESS						= 'audituser@entrata.com';
	const VENDORVERIFY_EMAIL_ADDRESS					= 'vendorverify@entrata.com';
	const MAINTENANCE_EMAIL_ADDRESS						= 'maintenance@entrata.com';
	const RETURNS_EMAIL_ADDRESS							= 'returns@entrata.com';
	const CAREERS_EMAIL_ADDRESS							= 'careers@entrata.com';
	const MASSEMAIL_EMAIL_ADDRESS						= 'massemail@entrata.com';
	const DAVE_EMAIL_ADDRESS							= 'dave@entrata.com';
	const SOFTWAREDEVMANAGERS_EMAIL_ADDRESS				= 'softwaredevmanagers@entrata.com';
	const SOFTWARE_ENGINEERS_EMAIL_ADDRESS				= 'softwareengineers@entrata.com';
	const SOFTMAINTENANCE_EMAIL_ADDRESS					= 'softmaintenance@entrata.com';
	const HELPDESK_EMAIL_ADDRESS						= 'helpdesk@entrata.com';
	const PDUNFORD_EMAIL_ADDRESS						= 'pdunford@entrata.com';
	const DOMAINSUPPORT_EMAIL_ADDRESS					= 'domainsupport@entrata.com';
	const SERVICESQUALITY_EMAIL_ADDRESS					= 'servicesquality@entrata.com';
	const RVSUPPORT_EMAIL_ADDRESS						= 'rvsupport@entrata.com';
	const IT_INFRA_SUPPORT_EMAIL_ADDRESS				= 'itinfrasupport@xento.com';
	const APPSTORE_EMAIL_ADDRESS						= 'appstore@entrata.com';
	const MARKETING_EMAIL_ADDRESS						= 'marketing@entrata.com';
	const TEST_EMAIL_ADDRESS							= 'test@entrata.com';
	const CHARGEBACKS_EMAIL_ADDRESS						= 'chargebacks@entrata.com';
	const LEASINGCENTERQA_EMAIL_ADDRESS					= 'leasingcenterqa@entrata.com';
	const DMANION_EMAIL_ADDRESS							= 'dmanion@entrata.com';
	const SHARNEKAR_EMAIL_ADDRESS						= 'sharnekar@entrata.com';
	const SURVEY_EMAIL_ADDRESS							= 'survey@entrata.com';
	const RESUMES_EMAIL_ADDRESS							= 'resumes@entrata.com';
	const ENTRATA_FACILITIES_APP_SUPPORT_EMAIL_ADDRESS	= 'facilitiesappfeedback@entrata.com';
	const LEASING_CENTER_CABINET_EMAIL_ADDRESS			= 'lccabinet@entrata.com';
	const LEASING_CENTER_SUPPORT_EMAIL_ADDRESS			= 'leasingcentersupport@entrata.com';
    const SECURITY_OPERATIONS_CENTER_EMAIL_ADDRESS		= 'soc@entrata.com';

	const XENTO_SPARSHENE_EMAIL_ADDRESS 				= 'sparshene@entrata.com';
	const SCHAVHAN_EMAIL_ADDRESS						= 'schavhan@entrata.com';
	const VTORY_EMAIL_ADDRESS							= 'vtory@entrata.com';
	const RBYRD_EMAIL_ADDRESS							= 'rbyrd@entrata.com';
	const CMARTIN_EMAIL_ADDRESS							= 'cmartin@entrata.com';
	const JJONES_EMAIL_ADDRESS 							= 'jjones@entrata.com';
	const AREID_EMAIL_ADDRESS   						= 'areid@entrata.com';
	const JJESPERSON_EMAIL_ADDRESS						= 'jjesperson@entrata.com';
	const RJENSEN_EMAIL_ADDRESS							= 'rjensen@entrata.com';
	const BBLAKER_EMAIL_ADDRESS							= 'bblaker@entrata.com';
	const TMARKS_EMAIL_ADDRESS							= 'tmarks@entrata.com';
	const JRAKOWSKI_EMAIL_ADDRESS						= 'jrakowski@entrata.com';
	const JABROWN_EMAIL_ADDRESS							= 'jabrown@entrata.com';
	const RJONES_EMAIL_ADDRESS							= 'rjones@entrata.com';
	const MFREEMAN_EMAIL_ADDRESS						= 'mfreeman@entrata.com';
	const JRENBERG_EMAIL_ADDRESS						= 'jrenberg@entrata.com';
	const DSHROFF_EMAIL_ADDRESS							= 'dshroff@entrata.com';
	const KBROGDON_EMAIL_ADDRESS						= 'kbrogdon@entrata.com';
	const CBRASHER_EMAIL_ADDRESS						= 'cbrasher@entrata.com';
	const BCHANDLER_EMAIL_ADDRESS						= 'bchandler@entrata.com';
	const VLINEGAR_EMAIL_ADDRESS						= 'vlinegar@entrata.com';
	const EVENTS_TEAM_EMAIL_ADDRESS						= 'events@entrata.com';
	const SLUKOWSKI_EMAIL_ADDRESS						= 'slukowski@entrata.com';
	const CHARGIS_EMAIL_ADDRESS						= 'chargis@entrata.com';
	const JSTARTTIN_EMAIL_ADDRESS						= 'jstartin@entrata.com';
	const MWHITE_EMAIL_ADDRESS							= 'mwhite@entrata.com';
	const CKING_EMAIL_ADDRESS 							= 'cking@entrata.com';
	const NWAKDE_EMAIL_ADDRESS							= 'nwakde@entrata.com';
	const SSONTAKKE_EMAIL_ADDRESS						= 'ssontakke@entrata.com';
	const NETAJIW_EMAIL_ADDRRESS						= 'netajiw@entrata.com';
	const GILBERT_EMAIL_ADDRESS							= 'gilbert@entrata.com';
	const BMCCLOSKEY_EMAIL_ADDRESS						= 'bmccloskey@entrata.com';
	const GILTHOMAS_EMAIL_ADDRESS						= 'gilthomas@entrata.com';
	const SCOLLINS_EMAIL_ADDRESS						= 'scollins@entrata.com';
	const CNOBLE_EMAIL_ADDRESS							= 'cnoble@entrata.com';
	const DUDELYDAVE_EMAIL_ADDRESS						= 'dudelydave@entrata.com';
	const JASHWORTH_EMAIL_ADDRESS						= 'jashworth@entrata.com';
	const JHUNSAKER_EMAIL_ADDRESS						= 'jhunsaker@entrata.com';
	const JCHRISTENSEN_EMAIL_ADDRESS					= 'jchristensen@entrata.com';
	const BFAUBUS_EMAIL_ADDRESS						    = 'bfaubus@entrata.com';
	const DMAXFIELD_EMAIL_ADDRESS						= 'dmaxfield@entrata.com';
	const HPALIWAL_EMAIL_ADDRESS						= 'hpaliwal@entrata.com';
	const KRADMALL_EMAIL_ADDRESS						= 'kradmall@entrata.com';
	const KCANEPA_EMAIL_ADDRESS							= 'kcanepa@entrata.com';
	const SNGUYEN_EMAIL_ADDRESS							= 'snguyen@entrata.com';
	const AWILDING_EMAIL_ADDRESS						= 'awilding@entrata.com';
	const JFORD_EMAIL_ADDRESS							= 'jford@entrata.com';
	const ACOWLEY_EMAIL_ADDRESS							= 'acowley@entrata.com';
	const RLOCKWOOD_EMAIL_ADDRESS						= 'rlockwood@entrata.com';
	const PWILLIAMS_EMAIL_ADDRESS						= 'pwilliams@entrata.com';
	const CLARENCE_EMAIL_ADDRESS						= 'clarence@entrata.com';
	const RESIDENT_INSURE_EMAIL_ADDRESS					= 'system@residentinsure.com';
	const DAILY_NUGGETS_EMAIL_ADDRESS					= 'dailynuggets@entrata.com';
	const MCJONES_EMAIL_ADDRESS							= 'mcjones@entrata.com';
	const MROMERO_EMAIL_ADDRESS							= 'mromero@entrata.com';
	const THOLSTON_EMAIL_ADDRESS						= 'THolston@entrata.com';
	const SPORTER_EMAIL_ADDRESS							= 'sporter@entrata.com';
	const BDUFFIN_EMAIL_ADDRESS							= 'bduffin@entrata.com';
	const REIMBURSEMENT_UPDATE_EMAIL_ADDRESS			= 'reimbursementupdates@entrata.com';
	const AMEX_UPDATE_EMAIL_ADDRESS						= 'amexupdates@entrata.com';
	const KB_UPDATE_EMAIL_ADDRESS						= 'kbupdates@entrata.com';
	const RSAUNDERS_EMAIL_ADDRESS						= 'rsaunders@entrata.com';
	const IMPLEMENTATION_AD_EMAIL_ADDRESS				= 'CoreConsultingADs@entrata.com';
	const CANCELLATIONS_EMAIL_ADDRESS					= 'cancellations@entrata.com';
	const RESIDENT_INSURE_PRODUCT_EMAIL_ADDRESS			= 'product@residentinsure.com';
	const RESIDENT_INSURE_DBULLOCK_EMAIL_ADDRESS		= 'dbullock@residentinsure.com';
	const OPSSUPPORT_EMAIL_ADDRESS						= 'opssupport@entrata.com';
	const JSPRABERRY_EMAIL_ADDRESS						= 'jspraberry@entrata.com';
	const BBAIRD_EMAIL_ADDRESS							= 'bbaird@entrata.com';
	const NEWRELIC_EMAIL_ADDRESS						= 'newrelic@entrata.com';
	const RESIDENT_VERIFY_CONSUMER_EMAIL_ADDRESS 		= 'consumerrequests@residentverify.com';
	const RESIDENT_VERIFY_ALTERNATE_CONSUMER_EMAIL_ADDRESS 	= 'consumer@residentverify.com';
	const PROPERTYSOLUTIONS_DIRECTORS_EMAIL_ADDRESS 	= 'thejoneses@entrata.com';
	const ENTRATA_HEAD_QUARTER_EMAIL_ADDRESS			= 'hq@entrata.com';
	const RESIDENT_PAY_ALERT_EMAIL_ADDRESS				= 'rpayalert@entrata.com';
	const SEO_EMAIL_ADDRESS								= 'seo@entrata.com';
	const KRUCH_EMAIL_ADDRESS							= 'kruch@entrata.com';
	const DHORNBERGER_EMAIL_ADDRESS						= 'dhornberger@entrata.com';
	const LEASING_CENTER_RECRUITMENT_EMAIL_ADDRESS		= 'LCResumes@entrata.com';
	const RESIDENT_PORTAL_ALERT_EMAIL_ADDRESS			= 'rpalert@entrata.com';
	const ERIC_WATSON_EMAIL_ADDRESS						= 'ewatson@entrata.com';
	const BBOSTER_EMAIL_ADDRESS							= 'bboster@entrata.com';
	const AMANDA_HOWELL_EMAIL_ADDRESS					= 'ahowell@entrata.com';
	const AKAVANAUGH_EMAIL_ADDRESS						= 'akavanaugh@entrata.com';
	const BFISH_EMAIL_ADDRESS                           = 'bfish@entrata.com';
	const HGANON_EMAIL_ADDRESS                          = 'hgagon@entrata.com';
	const JCOOLE_EMAIL_ADDRESS                          = 'jcoole@entrata.com';
	const ABULLOCK_EMAIL_ADDRESS 						= 'abullock@entrata.com';
	const SWAWGE_EMAIL_ADDRESS 						    = 'swawge@entrata.com';
	const TRAINTRAX_EMAIL_ADDRESS						= 'traintrax@entrata.com';
	const GLEONARD_EMAIL_ADDRESS						= 'gleonard@entrata.com';
	const XENTO_INTEGRATION_EMAIL_ADDRESS				= 'integrationsupport@entrata.com';
	const XENTO_CAREERS_GROUP_EMAIL_ADDRESS				= 'careersgroup@xento.com';
	const DMERRIMAN_SCHEDULED_EMAIL_ADDRESS				= 'dmerriman+scheduledtasks@entrata.com';
	const XENTO_REPORTS_TEAM_GROUP_EMAIL_ADDRESS		= 'reportsteamscheduledtasks@googlegroups.com';
	const SUP_MAN_EMAIL_ADDRESS							= 'supman@entrata.com';
	const IMP_MANAGEMENT_EMAIL_ADDRESS					= 'impmanagement@entrata.com';
	const IP_EMAIL_ADDRESS								= 'ip@entrata.com';
	const RTYSON_EMAIL_ADDRESS							= 'rtyson@entrata.com';
	const XENTO_BKHANDELWAL_EMAIL_ADDRESS				= 'bkhandelwal@xento.com';
	const XENTO_ARAVTOLE_EMAIL_ADDRESS					= 'aravtole@xento.com';
	const MFRANDSEN_EMAIL_ADDRESS						= 'mfrandsen@entrata.com';
	const BBROWN_EMAIL_ADDRESS							= 'bbrown@entrata.com';
	const DORDACOWSKI_EMAIL_ADDRESS						= 'dordacowski@entrata.com';
	const BBENSON_EMAIL_ADDRESS                         = 'bbenson@entrata.com';
	const TDURRANS_EMAIL_ADDRESS                        = 'tdurance@entrata.com';
	const XENTO_AHEROLI_EMAIL_ADDRESS                   = 'aheroli@xento.com';
	const XENTO_RTHORAT_EMAIL_ADDRESS                   = 'rthorat@xento.com';
	const XENTO_RBHUTADA_EMAIL_ADDRESS                  = 'rbhutada@xento.com';
	const XENTO_SDALVI_EMAIL_ADDRESS                    = 'sdalvi@xento.com';
    const XENTO_SCHEMA_CHANGE_ALERT_EMAIL_ADDRESS		= 'schemachangealert@xento.com';
    const RESIDENT_INSURE_SUPPORT_EMAIL_ADDRESS         = 'risupport@entrata.com';
	const XENTO_LAKSHMI_PARIMALA_EMAIL_ADDRESS          = 'lparimala@entrata.com';
	const XENTO_CHRISTINA_FERNANDES_EMAIL_ADDRESS       = 'cfernandes@xento.com';
	const XENTO_PRAFULL_JAISWAL_EMAIL_ADDRESS           = 'pjaiswal@xento.com';
	const ENTRATA_RECRUITING_EMAIL_ADDRESS				= 'recruiting@entrata.com';

	// Vendor Access
	const VA_SUPPORT_EMAIL_ADDRESS						= 'support@vendoraccess.com';
	const VA_GENERALSUPPORT_EMAIL_ADDRESS				= 'generalsupport@vendoraccess.com';
	const VA_LEGAL_EMAIL_ADDRESS						= 'legal@vendoraccess.com';
	const VA_INFO_EMAIL_ADDRESS							= 'info@vendoraccess.com';

	const RFELT_EMAIL_ADDRESS 							= 'rfelt@entrata.com';

	const SYSTEM_EMAIL_AWS_BUCKET						= 'system-emails';
	const CNEMELKA_EMAIL_ADDRESS                        = 'cnemelka@entrata.com';

	const RI_DATA_PRIVACY_EMAIL_ADDRESS                 = 'dataprivacy@residentinsure.com';

	const CYBERCITY_HELPDESK_EMAIL_ADDRESS 				= 'helpdesk.cybercity@cbre.co.in';
	const XENTO_RECEPTION_EMAIL_ADDRESS 				= 'reception@xento.com';
	const SYSTEM_DELAY_ALERT_EMAIL_ADDRESS              = 'emaildelayalert@xento.com';
	const BIGSALE_EMAIL_ADDRESS							= 'bigsale@entrata.com';
	const LAURA_PARKER_EMAIL_ADDRESS					= 'lparker@entrata.com';

	public static $c_arrstrRPayRelatedEmailAddress 			= [ self::RJENSEN_EMAIL_ADDRESS, self::TMARKS_EMAIL_ADDRESS, self::XENTO_RPARDESHI_EMAIL_ADDRESS ];
	public static $c_arrstrSQlDeploymentEmailAddress		= [ self::XENTO_DEPLOY_EMAIL_ADDRESS, self::XENTO_DBA_EMAIL_ADDRESS, 'rbyrd@entrata.com', 'cmartin@entrata.com', self::ARCHITECTS_EMAIL_ADDRESS ];
	public static $c_arrstrLeaseExSdmEmailAddresses			= [ self::HPALIWAL_EMAIL_ADDRESS ];
	public static $c_arrstrLeaseExLeadersEmailAddresses		= [ self::XENTO_CSAHU_EMAIL_ADDRESS, self::XENTO_SGUNDE_EMAIL_ADDRESS, self::XENTO_VMOHITE_EMAIL_ADDRESS, self::XENTO_KALPANA_EMAIL_ADDRESS ];
	public static $c_arrstrLeaseExDevelopersEmailAddresses	= [
		self::XENTO_VTIWARI_EMAIL_ADDRESS,
		self::XENTO_VNAVALE_EMAIL_ADDRESS,
		self::XENTO_ABAMNODKAR_EMAIL_ADDRESS,
		self::XENTO_ANEEMA_EMAIL_ADDRESS,
		self::XENTO_AAZIZ_EMAIL_ADDRESS,
		self::XENTO_MMARATHE_EMAIL_ADDRESS,
		self::XENTO_MDEB_EMAIL_ADDRESS,
		self::XENTO_YRENUKE_EMAIL_ADDRESS
	];

	public static $c_arrstrLeaseExQasEmailAddresses = [
		self::XENTO_SMOGHE01_EMAIL_ADDRESS,
		self::XENTO_NKUSALKAR_EMAIL_ADDRESS,
		self::XENTO_MMUNDHE_EMAIL_ADDRESS,
		self::XENTO_SAMBULKAR_EMAIL_ADDRESS,
		self::XENTO_SWABLE_EMAIL_ADDRESS
	];

	public static $c_arrstrEntrataHeadQuarterEmailAddress = [
		'accounting@entrata.com',
		'agreements@entrata.com',
		'akavanaugh@entrata.com',
		'architects@entrata.com',
		'bfish@entrata.com',
		'bizdev@entrata.com',
		'bsorensen@entrata.com',
		'businessdevelopment@entrata.com',
		'bzimmer@entrata.com',
		'charrington@entrata.com',
		'dbateman@entrata.com',
		'design@entrata.com',
		'desktopsupport@entrata.com',
		'developers@entrata.com',
		'dnorton@entrata.com',
		'drinkingthekoolaid@entrata.com',
		'dromero@entrata.com',
		'firstresponse@entrata.com',
		'frontdesk@entrata.com',
		'hgagon@entrata.com',
		'hr@entrata.com',
		'insidesales@entrata.com',
		'insurance@entrata.com',
		'jcoole@entrata.com',
		'jhall@entrata.com',
		'jhunsaker@entrata.com',
		'keepingthepeace@entrata.com',
		'legal@entrata.com',
		'marketing@entrata.com',
		'merchantservicesgroup@entrata.com',
		'projectmanagers@entrata.com',
		'provodevs@entrata.com',
		'recruiting@entrata.com',
		'residentinsurefulfillment@entrata.com',
		'residentverify@entrata.com',
		'resolutionrenegades@entrata.com',
		'ru@entrata.com',
		'rui@entrata.com',
		'rvdomain@entrata.com',
		'salesengineersutah@entrata.com',
		'seo@entrata.com',
		'seofulfillment@entrata.com',
		'sharris@entrata.com',
		'smoothoperators@entrata.com',
		'softwaredevmanagers@entrata.com',
		'softwareengineers@entrata.com',
		'spreadingthelove@entrata.com',
		'sqm@entrata.com',
		'techtonics@entrata.com',
		'usit@entrata.com',
		'utahconsultants@entrata.com',
		'utahproductmanagers@entrata.com',
		'utahtraining@entrata.com',
		'ux@entrata.com',
		'wemakeyoulookgood@entrata.com',
		'wherethemagichappens@entrata.com',
		'bwade@entrata.com',
		'dromero@entrata.com',
		'clyman@entrata.com'
	];

	public static $c_arrstrEntrataHeadQuarterEmailAddressForReleaseNotes = [
		'accounting@entrata.com',
		'agreements@entrata.com',
		'akavanaugh@entrata.com',
		'architects@entrata.com',
		'bfish@entrata.com',
		'bizdev@entrata.com',
		'bsorensen@entrata.com',
		'businessdevelopment@entrata.com',
		'bzimmer@entrata.com',
		'charrington@entrata.com',
		'dbateman@entrata.com',
		'design@entrata.com',
		'desktopsupport@entrata.com',
		'developers@entrata.com',
		'dnorton@entrata.com',
		'drinkingthekoolaid@entrata.com',
		'dromero@entrata.com',
		'firstresponse@entrata.com',
		'frontdesk@entrata.com',
		'hgagon@entrata.com',
		'hr@entrata.com',
		'insidesales@entrata.com',
		'insurance@entrata.com',
		'jcoole@entrata.com',
		'jhall@entrata.com',
		'jhunsaker@entrata.com',
		'keepingthepeace@entrata.com',
		'legal@entrata.com',
		'marketing@entrata.com',
		'merchantservicesgroup@entrata.com',
		'projectmanagers@entrata.com',
		'provodevs@entrata.com',
		'recruiting@entrata.com',
		'residentinsurefulfillment@entrata.com',
		'residentverify@entrata.com',
		'resolutionrenegades@entrata.com',
		'ru@entrata.com',
		'rui@entrata.com',
		'rvdomain@entrata.com',
		'salesengineersutah@entrata.com',
		'seo@entrata.com',
		'seofulfillment@entrata.com',
		'sharris@entrata.com',
		'smoothoperators@entrata.com',
		'softwaredevmanagers@entrata.com',
		'softwareengineers@entrata.com',
		'spreadingthelove@entrata.com',
		'sqm@entrata.com',
		'techtonics@entrata.com',
		'usit@entrata.com',
		'beacons@entrata.com',
		'utahproductmanagers@entrata.com',
		'utahtraining@entrata.com',
		'ux@entrata.com',
		'wemakeyoulookgood@entrata.com',
		'wherethemagichappens@entrata.com',
		'bwade@entrata.com',
		'dromero@entrata.com',
		'clyman@entrata.com'
	];

	public static $c_arrstrRITeamEmailAddresses = [ self::XENTO_RVYAS_EMAIL_ADDRESS, self::XENTO_PKUMAR_EMAIL_ADDRESS, self::SWAWGE_EMAIL_ADDRESS, self::XENTO_SDURAPHE_EMAIL_ADDRESS, self::XENTO_SSINGH07_EMAIL_ADDRESS, self::XENTO_AKUDACHE_EMAIL_ADDRESS, self::XENTO_VTHORVE_EMAIL_ADDRESS, self::XENTO_NJADHAV_EMAIL_ADDRESS, self::XENTO_VJADHAV04_EMAIL_ADDRESS, self::XENTO_SGUPTA02_EMAIL_ADDRESS ];

	public function __construct() {
		parent::__construct();

		$this->m_strTextContent 			 = NULL;
		$this->m_strHtmlContent 			 = NULL;
		$this->m_arrobjEmailAttachments		 = NULL;
		$this->m_arrobjSystemEmailRecipients = NULL;
		$this->m_intTotalEmailAttachmentsize = 0;

		$this->setScheduledSendDatetime( date( 'm/d/Y' ) );

		return;
	}

	/**
	 * Add Methods
	 *
	 */

	public function addSystemEmailAttachment( $objSystemEmailAttachment ) {
		$this->m_arrobjSystemEmailAttachments[$objSystemEmailAttachment->getId()] = $objSystemEmailAttachment;
	}

	public function addEmailAttachment( $objEmailAttachment ) {

		if( true == valId( $objEmailAttachment->getId() ) ) {
			$this->m_arrobjEmailAttachments[$objEmailAttachment->getId()] = $objEmailAttachment;
		} else {
			$this->m_arrobjEmailAttachments[] = clone $objEmailAttachment;
		}
	}

	/**
	 * Get Functions
	 *
	 */

	public function getFromDate() {
		return $this->m_strFromDate;
	}

	public function getToDate() {
		return $this->m_strToDate;
	}

	public function getTextContent() {

		if( true == isset( $this->m_strTextContent ) && 0 < strlen( $this->m_strTextContent ) ) {
			return $this->m_strTextContent;

		} else {
			if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_text.txt' ) && 0 < filesize( $this->buildStoragePath() . $this->getId() . '_text.txt' ) ) {
				$this->m_strTextContent = CFileIo::readFile( $this->buildStoragePath() . $this->getId() . '_text.txt' );
				return $this->m_strTextContent;
			} else {
				return NULL;
			}
		}
	}

	public function getHtmlContent() {

		if( true == isset( $this->m_strHtmlContent ) && 0 < strlen( $this->m_strHtmlContent ) ) {
			return $this->m_strHtmlContent;

		} else {

			if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_html.txt' ) && 0 < filesize( $this->buildStoragePath() . $this->getId() . '_html.txt' ) ) {

				$this->m_strHtmlContent = CFileIo::readFile( $this->buildStoragePath() . $this->getId() . '_html.txt' );

				return $this->m_strHtmlContent;

			} elseif( true == CSystemEmailType::checkIsContentStoreOnCloud( $this->getSystemEmailTypeId() ) ) {

				if( !empty( $this->getTempStorageFilePath() ) ) {
					$strSourcePath  = $this->getTempStorageFilePath();

					if( true == file_exists( $strSourcePath ) && 0 < filesize( $strSourcePath ) ) {
						$this->m_strHtmlContent = CFileIo::readFile( $strSourcePath );
						return $this->m_strHtmlContent;
					}
				}

				$strSourcePath  = $this->getTempStoragePath() . $this->getId() . '_html.txt';

				if( true == file_exists( $strSourcePath ) && 0 < filesize( $strSourcePath ) ) {
					$this->m_strHtmlContent = CFileIo::readFile( $strSourcePath );
					return $this->m_strHtmlContent;
				}
			}

			if( 'production' == CONFIG_ENVIRONMENT ) {

				$strCidPath = '';

				if( false == is_null( $this->getCid() ) ) {
					$strCidPath = $this->getCid() . '/';
				}

				if( !empty( $this->getObjectKey() ) ) {
					$strSourcePath = $this->getObjectKey();
				} else {
					$strSourcePath = $strCidPath . date( 'Y', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . $this->getId() . '_html.txt';
				}

				$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );

				$arrmixRequestConfig = array(
					'Container'	=> self::SYSTEM_EMAIL_AWS_BUCKET,
					'Key'	    => $strSourcePath,
				);

				$arrobjGetResponse = $objObjectStorageGateway->getObject( $arrmixRequestConfig );

				if( true == $arrobjGetResponse->isSuccessful() ) {

					$this->m_strHtmlContent = $arrobjGetResponse['data'];

					return $this->m_strHtmlContent;

				} else {
					if( false == valObj( $this->getDatabase(), 'CDatabase' ) ) {
						return NULL;
					}

					$objEmailDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::EMAIL, $this->getDatabase()->getDatabaseUserTypeId() );

					$objSystemEmailType = CSystemEmailTypes::fetchSystemEmailTypeById( $this->getSystemEmailTypeId(), $objEmailDatabase );

					$objEmailDatabase->close();

					if( false == is_null( $objSystemEmailType->getLastSyncedOn() ) && date( 'Y-m-d', strtotime( $this->getScheduledSendDatetime() ) ) < date( 'Y-m-d', strtotime( $objSystemEmailType->getLastSyncedOn() ) ) ) {

						$strSystemEmailPath = PATH_NON_BACKUP_MOUNTS_TEMP_SYSTEM_EMAIL_AWS_S3 . 'system_email_type_' . $this->getSystemEmailTypeId() . '/' . $this->getCid() . '/' . date( 'Y', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . $this->getId();

						if( false == file_exists( $strSystemEmailPath . '/' . $this->getId() . '_html.txt' ) ) {
							$strSystemEmailPath = $this->extractDownloadedFile( $this->downloadFileFromCloud() );
							if( true == is_null( $strSystemEmailPath ) ) {
								return NULL;
							}
						}

						if( true == file_exists( $strSystemEmailPath . '/' . $this->getId() . '_html.txt' ) ) {

							$this->m_strHtmlContent = CFileIo::readFile( $strSystemEmailPath . '/' . $this->getId() . '_html.txt' );

							return $this->m_strHtmlContent;
						}
					}

					return NULL;
				}

			} else {
				return NULL;
			}
		}
	}

	public function getParsedHtmlContent( $intAttachmentId, $strTitle ) {

		$strHtmlContent         = $this->getHtmlContent();
		$strReplacementString   = '/?module=system_emails-new&action=view_system_email_attachment&system_email_attachment[email_attachment_id]=' . ( int ) $intAttachmentId . '&system_email_id=' . $this->getId();

		if( true == \Psi\CStringService::singleton()->strpos( $strHtmlContent, $strTitle ) ) {
			$strHtmlContent = str_replace( $strTitle, $strReplacementString, $strHtmlContent );
		}

		return $strHtmlContent;
	}

	public function getSystemEmailTypeName() {
		return $this->m_strSystemEmailTypeName;
	}

	public function getFormattedEmailDate() {
		return $this->m_strFormattedEmailDate;
	}

	public function getFormattedToEmailAddress() {
		return $this->m_strFormattedToEmailAddress;
	}

	public function getFormattedFromEmailAddress() {
		return $this->m_strFormattedFromEmailAddress;
	}

	public function getReadEmailCount() {
		return $this->m_intReadEmailCount;
	}

	public function getTotalEmailCount() {
		return $this->m_intTotalEmailCount;
	}

	public function getBounceEmailCount() {
		return $this->m_intBounceEmailCount;
	}

	public function getEmailAttachmentCount() {
		return $this->m_intEmailAttachmentCount;
	}

	public function getEmailAttachments() {
		return $this->m_arrobjEmailAttachments;
	}

	public function getEmailEventId() {
		return $this->m_intEmailEventId;
	}

	public function getEmailEvent() {
		return $this->m_objEmailEvent;
	}

	public function getEmailEventTypeId() {
		return $this->m_intEmailEventTypeId;
	}

	public function getEmailEventName() {
		return $this->m_strEmailEventName;
	}

	public function getEmailEventResponse() {
		return $this->m_strEmailEventResponse;
	}

	public function getRecipientEmailAddress() {
		return $this->m_strRecipientEmailAddress;
	}

	public function getEmailEventProcessedOn() {
		return $this->m_strEmailEventProcessedOn;
	}

	public function getEmailEventDeliveredOn() {
		return $this->m_strEmailEventDeliveredOn;
	}

	public function getEmailEventClickedOn() {
		return $this->m_strEmailEventClickedOn;
	}

	public function getEmailEventOpenedOn() {
		return $this->m_strEmailEventOpenedOn;
	}

	public function getEmailEventBouncedOn() {
		return $this->m_strEmailEventBouncedOn;
	}

	public function getEmailEventSpammedOn() {
		return $this->m_strEmailEventSpammedOn;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getSystemEmailRecipients() {
		return $this->m_arrobjSystemEmailRecipients;
	}

	public function getRecipientsCount() {
		return $this->m_intRecipientsCount;
	}

	public function getSystemEmailRecipientId() {
		return $this->m_intSystemEmailRecipientId;
	}

	public function getFileTypeSystemCode() {
		return $this->m_strFileTypeSystemCode;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getIsResidentEmail() {
		return $this->m_intIsResidentEmail;
	}

	public function getDocumentManager() {
		return $this->m_objDocumentManager;
	}

	public function getObjectStorageGateway() {
		return $this->m_objObjectStorageGateway;
	}

	public function getClientDatabase() {
		return $this->m_objClientDatabase;
	}

	public function getErrors() {
		return $this->m_strErrors;
	}

	public function getHtmlContentPath() {
		return $this->m_strHtmlContentPath;
	}

	public function getTextContentPath() {
		return $this->m_strTextContentPath;
	}

	public function getAttachmentPath() {
		return unserialize( $this->m_strAttachmentPath );
	}

	public function getHtmlFileSize() {
		return $this->m_intHtmlFileSize;
	}

	public function getTextFileSize() {
		return $this->m_intTextFileSize();
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function getStorageGateway() {

		$arrstrStorageGateway = [];
		if( 'production' == CONFIG_ENVIRONMENT ) {
			$arrstrStorageGateway['container']       = CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET;
			$arrstrStorageGateway['storage_gateway'] = CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3;
		} else {
			$arrstrStorageGateway['container']       = 'system_email';
			$arrstrStorageGateway['storage_gateway'] = CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM;
		}
		return $arrstrStorageGateway;
	}

	/**
	 * @param $arrmixParameters
	 * @return string
	 * @throws \Exception
	 */
	public function uploadFileToS3( $arrmixParameters ) : string {

		if( false == valArr( $arrmixParameters ) ) {
			throw new \Exception( 'Required parameter missing to store files on Storage gateway' );
		}

		$arrstrStorageGateway    = $this->getStorageGateway();
		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( $arrstrStorageGateway['storage_gateway'] );

		$arrstrParameters = [];
		$arrstrParameters['container']  = $arrstrStorageGateway['container'];
		$arrstrParameters['noEncrypt']  = true;

		$arrstrParameters = array_merge( $arrstrParameters, $arrmixParameters );
		try{
			$arrstrResponse   = $objObjectStorageGateway->putObject( $arrstrParameters );
			if( $arrstrResponse['hasErrors'] ) {
				throw new \Exception( 'Unable to store file on Storage gateway' );
			}
		} catch( \Exception $objException ) {
			throw $objException;
		}

		return $arrstrResponse['objectURL'] ?? '';
	}

	public function downloadFileFromS3( $strKey ) {

		$arrstrStorageGateway    = $this->getStorageGateway();
		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( $arrstrStorageGateway['storage_gateway'] );

		$arrstrResponse = $objObjectStorageGateway->getObject( [
			'container'   => $arrstrStorageGateway['container'],
			'key'         => $strKey,
		] );

		if( $arrstrResponse['hasErrors'] ) {
			return false;
		}

		return $arrstrResponse['data'];
	}

	/**
	 * Set Functions
	 *
	 */

	public function setDefaults() {

		$this->setSystemEmailPriorityId( CSystemEmailPriority::NORMAL );
		$this->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$this->setScheduledSendDatetime( date( 'm/d/Y' ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['from_date'] ) )					$this->setFromDate( $arrmixValues['from_date'] );
		if( true == isset( $arrmixValues['to_date'] ) ) 					$this->setToDate( $arrmixValues['to_date'] );
		if( true == isset( $arrmixValues['text_content'] ) )				$this->setTextContent( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['text_content'] ) : $arrmixValues['text_content'] );
		if( true == isset( $arrmixValues['html_content'] ) )				$this->setHtmlContent( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['html_content'] ) : $arrmixValues['html_content'] );
		if( true == isset( $arrmixValues['system_email_type_name'] ) )		$this->setSystemEmailTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_email_type_name'] ) : $arrmixValues['system_email_type_name'] );
		if( true == isset( $arrmixValues['read_email_count'] ) ) 			$this->setReadEmailCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['read_email_count'] ) : $arrmixValues['read_email_count'] );
		if( true == isset( $arrmixValues['total_email_count'] ) ) 			$this->setTotalEmailCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['total_email_count'] ) : $arrmixValues['total_email_count'] );
		if( true == isset( $arrmixValues['bounce_email_count'] ) )			$this->setBounceEmailCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bounce_email_count'] ) : $arrmixValues['bounce_email_count'] );
		if( true == isset( $arrmixValues['event_name'] ) )					$this->setEmailEventName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['event_name'] ) : $arrmixValues['event_name'] );
		if( true == isset( $arrmixValues['response'] ) ) 					$this->setEmailEventResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['response'] ) : $arrmixValues['response'] );
		if( true == isset( $arrmixValues['email_event_type_id'] ) )			$this->setEmailEventTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email_event_type_id'] ) : $arrmixValues['email_event_type_id'] );
		if( true == isset( $arrmixValues['email_event_id'] ) )				$this->setEmailEventId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['email_event_id'] ) : $arrmixValues['email_event_id'] );
		if( true == isset( $arrmixValues['recipient_email_address'] ) )		$this->setRecipientEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['recipient_email_address'] ) : $arrmixValues['recipient_email_address'] );
		if( true == isset( $arrmixValues['processed_on'] ) ) 				$this->setEmailEventProcessedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['processed_on'] ) : $arrmixValues['processed_on'] );
		if( true == isset( $arrmixValues['delivered_on'] ) ) 				$this->setEmailEventDeliveredOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['delivered_on'] ) : $arrmixValues['delivered_on'] );
		if( true == isset( $arrmixValues['clicked_on'] ) )					$this->setEmailEventClickedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['clicked_on'] ) : $arrmixValues['clicked_on'] );
		if( true == isset( $arrmixValues['opened_on'] ) )					$this->setEmailEventOpenedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['opened_on'] ) : $arrmixValues['opened_on'] );
		if( true == isset( $arrmixValues['bounced_on'] ) )					$this->setEmailEventBouncedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['bounced_on'] ) : $arrmixValues['bounced_on'] );
		if( true == isset( $arrmixValues['spammed_on'] ) )					$this->setEmailEventSpammedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['spammed_on'] ) : $arrmixValues['spammed_on'] );
		if( true == isset( $arrmixValues['recipients_count'] ) )			$this->setRecipientsCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['recipients_count'] ) : $arrmixValues['recipients_count'] );
		if( true == isset( $arrmixValues['system_email_recipient_id'] ) )	$this->setSystemEmailRecipientId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_email_recipient_id'] ) : $arrmixValues['system_email_recipient_id'] );
		if( true == isset( $arrmixValues['errors'] ) )						$this->setErrors( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['errors'] ) : $arrmixValues['errors'] );
		if( true == isset( $arrmixValues['html_content_path'] ) )			$this->setHtmlContentPath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['html_content_path'] ) : $arrmixValues['html_content_path'] );
		if( true == isset( $arrmixValues['text_content_path'] ) )			$this->setTextContentPath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['text_content_path'] ) : $arrmixValues['text_content_path'] );
		if( true == isset( $arrmixValues['email_attachments'] ) )			$this->setEmailAttachments( $arrmixValues['email_attachments'] );
		if( true == isset( $arrmixValues['system_email_recipients'] ) )		$this->setQueuedSystemEmailRecipients( $arrmixValues['system_email_recipients'] );
		if( true == isset( $arrmixValues['html_file_size'] ) )              $this->setHtmlFileSize( $arrmixValues['html_file_size'] );
		// The following line is backward compatible code for setting email attachment path to system email object which will be removed soon.
		if( true == isset( $arrmixValues['attachment_path'] ) )				$this->setAttachmentPath( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['attachment_path'] ) : $arrmixValues['attachment_path'] );

		if( true == isset( $arrmixValues['system_message_id'] ) )			$this->setSystemMessageId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['system_message_id'] ) : $arrmixValues['system_message_id'] );
		if( true == isset( $arrmixValues['notes'] ) )                       $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['notes'] ) : $arrmixValues['notes'] );
		return;
	}

	public function setFromDate( $strFromDate ) {
		$this->m_strFromDate = $strFromDate;
	}

	public function setToDate( $strToDate ) {
		$this->m_strToDate = $strToDate;
	}

	public function setTextContent( $strTextContent ) {
		$this->m_strTextContent = CStrings::strTrimDef( $strTextContent, -1, NULL, true );
	}

	public function setHtmlContent( $strHtmlContent, $boolDoNotStripSpecialTags = false ) {
		$this->m_strHtmlContent = CStrings::strTrimDef( $strHtmlContent, -1, NULL, true, $boolDoNotStripSpecialTags );
	}

	public function setSystemEmailTypeName( $strSystemEmailTypeName ) {
		$this->m_strSystemEmailTypeName = $strSystemEmailTypeName;
	}

	public function setFormattedEmailDate( $strFormattedEmailDate ) {
		$this->m_strFormattedEmailDate = $strFormattedEmailDate;
	}

	public function setFormattedToEmailAddress( $strFormattedToEmailAddress ) {
		$this->m_strFormattedToEmailAddress = $strFormattedToEmailAddress;
	}

	public function setFormattedFromEmailAddress( $strFormattedFromEmailAddress ) {
		$this->m_strFormattedFromEmailAddress = $strFormattedFromEmailAddress;
	}

	public function setReadEmailCount( $intReadEmailCount ) {
		$this->m_intReadEmailCount = $intReadEmailCount;
	}

	public function setTotalEmailCount( $intTotalEmailCount ) {
		$this->m_intTotalEmailCount = $intTotalEmailCount;
	}

	public function setBounceEmailCount( $intBounceEmailCount ) {
		$this->m_intBounceEmailCount = $intBounceEmailCount;
	}

	public function setEmailAttachmentCount( $intEmailAttachmentCount ) {
		$this->m_intEmailAttachmentCount = ( int ) $intEmailAttachmentCount;
	}

	public function setEmailEventId( $intEmailEventId ) {
		$this->m_intEmailEventId = $intEmailEventId;
	}

	public function setEmailEvent( $objEvent ) {
		$this->m_objEmailEvent = $objEvent;
	}

	public function setEmailEventTypeId( $strEmailEventTypeId ) {
		$this->m_intEmailEventTypeId = $strEmailEventTypeId;
	}

	public function setEmailEventName( $strEmailEventName ) {
		$this->m_strEmailEventName = $strEmailEventName;
	}

	public function setEmailEventResponse( $strEmailEventResponse ) {
		$this->m_strEmailEventResponse = $strEmailEventResponse;
	}

	public function setRecipientEmailAddress( $strRecipientEmailAddress ) {
		$this->m_strRecipientEmailAddress = $strRecipientEmailAddress;
	}

	public function setEmailEventProcessedOn( $strEmailEventProcessedOn ) {
		$this->m_strEmailEventProcessedOn = $strEmailEventProcessedOn;
	}

	public function setEmailEventDeliveredOn( $strEmailEventDeliveredOn ) {
		$this->m_strEmailEventDeliveredOn = $strEmailEventDeliveredOn;
	}

	public function setEmailEventClickedOn( $strEmailEventClickedOn ) {
		$this->m_strEmailEventClickedOn = $strEmailEventClickedOn;
	}

	public function setEmailEventOpenedOn( $strEmailEventOpenedOn ) {
		$this->m_strEmailEventOpenedOn = $strEmailEventOpenedOn;
	}

	public function setEmailEventBouncedOn( $strEmailEventBouncedOn ) {
		$this->m_strEmailEventBouncedOn = $strEmailEventBouncedOn;
	}

	public function setEmailEventSpammedOn( $strEmailEventSpammedOn ) {
		$this->m_strEmailEventSpammedOn = $strEmailEventSpammedOn;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setEmailAttachments( $arrmixEmailAttachments ) {

		if( true == valArr( $arrmixEmailAttachments ) ) {
			$arrobjEmailAttachments = [];

			foreach( $arrmixEmailAttachments as $arrmixEmailAttachment ) {
				$objEmailAttachment = new \CEmailAttachment();
				$objEmailAttachment->setValues( $arrmixEmailAttachment );
				$arrobjEmailAttachments[] = $objEmailAttachment;
			}
			$this->m_arrobjEmailAttachments = $arrobjEmailAttachments;
		}
	}

	public function setSystemEmailRecipients( $arrobjSystemEmailRecipients ) {
		$this->m_arrobjSystemEmailRecipients = $arrobjSystemEmailRecipients;
	}

	public function setQueuedSystemEmailRecipients( $arrmixSystemEmailRecipients ) {

		if( valArr( $arrmixSystemEmailRecipients ) ) {
			$arrobjSystemEmailRecipients = [];

			foreach( $arrmixSystemEmailRecipients as $arrmixSystemEmailRecipient ) {
				$objSystemEmailRecipient = new \CSystemEmailRecipient();
				$arrmixSystemEmailRecipient['details'] = ( \valArr( $arrmixSystemEmailRecipient['details'] ?? [], 0 ) ) ? json_encode( $arrmixSystemEmailRecipient['details'] ?? [] ) : $arrmixSystemEmailRecipient['details'];
				$objSystemEmailRecipient->setValues( $arrmixSystemEmailRecipient );
				$arrobjSystemEmailRecipients[] = $objSystemEmailRecipient;
			}
		}
		$this->m_arrobjSystemEmailRecipients = $arrobjSystemEmailRecipients;
	}

	public function addSystemEmailRecipient( $objSystemEmailRecipient ) {
		$this->m_arrobjSystemEmailRecipients[] = $objSystemEmailRecipient;
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		if( 1 == substr_count( $strToEmailAddress, '@' ) ) {
			if( true == preg_match( '/[<]\s*/', $strToEmailAddress ) ) {
				preg_match( '/<.*>/', $strToEmailAddress, $arrstrEmailAddress );
				if( false == empty( $arrstrEmailAddress[0] ) ) $strToEmailAddress = preg_replace( '/<(.*)>/', preg_replace( '/[ ]\s*/', '', $arrstrEmailAddress[0] ), $strToEmailAddress );
			} else {
				$strToEmailAddress = preg_replace( '/[ ]\s*/', '', $strToEmailAddress );
			}
		}
		$strToEmailAddress = trim( $strToEmailAddress, ',' );
		$this->m_strToEmailAddress = $strToEmailAddress;
	}

	public function setBccEmailAddress( $strBccEmailAddress ) {
		if( 1 == substr_count( $strBccEmailAddress, '@' ) ) {
			if( true == preg_match( '/[<]\s*/', $strBccEmailAddress ) ) {
				preg_match( '/<.*>/', $strBccEmailAddress, $arrstrEmailAddress );
				if( false == empty( $arrstrEmailAddress[0] ) ) $strBccEmailAddress = preg_replace( '/<(.*)>/', preg_replace( '/[ ]\s*/', '', $arrstrEmailAddress[0] ), $strBccEmailAddress );
			} else {
				$strBccEmailAddress = preg_replace( '/[ ]\s*/', '', $strBccEmailAddress );
			}
		}
		$this->m_strBccEmailAddress = $strBccEmailAddress;
	}

	public function setRecipientsCount( $intRecipientsCount ) {
		$this->m_intRecipientsCount = ( int ) $intRecipientsCount;
	}

	public function setSystemEmailRecipientId( $intSystemEmailRecipientId ) {
		$this->m_intSystemEmailRecipientId = ( int ) $intSystemEmailRecipientId;
	}

	public function setFileTypeSystemCode( $strFileTypeSystemCode ) {
		$this->m_strFileTypeSystemCode = $strFileTypeSystemCode;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = ( int ) $intLeaseId;
	}

	public function setIsResidentEmail( $intIsResidentEmail ) {
		$this->m_intIsResidentEmail = ( int ) $intIsResidentEmail;
	}

	public function setDocumentManager( $objDocumentManager ) {
		$this->m_objDocumentManager = $objDocumentManager;
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	public function setClientDatabase( $objClientDatabase ) {
		$this->m_objClientDatabase = $objClientDatabase;
	}

	public function setErrors( $strErrors ) {
		$this->m_strErrors = CStrings::strTrimDef( $strErrors, -1, NULL, true );
	}

	public function setHtmlContentPath( $strHtmlContentPath ) {
		$this->m_strHtmlContentPath = CStrings::strTrimDef( $strHtmlContentPath, -1, NULL, true );
	}

	public function setTextContentPath( $strTextContentPath ) {
		$this->m_strTextContentPath = CStrings::strTrimDef( $strTextContentPath, -1, NULL, true );
	}

	public function setAttachmentPath( $strAttachmentPath ) {
		$this->m_strAttachmentPath = CStrings::strTrimDef( serialize( $strAttachmentPath ), -1, NULL, true );
	}

	public function setHtmlFileSize( $intHtmlFileSize ) {
		$this->m_intHtmlFileSize = CStrings::strToIntDef( $intHtmlFileSize, NULL, false );
	}

	public function setTextFileSize( $intTextFileSize ) {
		$this->m_intTextFileSize = CStrings::strToIntDef( $intTextFileSize, NULL, false );
	}

	public function setNotes( $strNotes ) {
		$this->m_strNotes = $strNotes;
	}

	public function setSubject( $strSubject ) {
		$strSubject = preg_replace( '/\s+/', ' ', $strSubject );
		parent::setSubject( $strSubject );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valSystemEmailTypeId() {
		$boolIsValid = true;

		if( true == empty( $this->getSystemEmailTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_type_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valSystemEmailPriorityId() {
		$boolIsValid = true;

		if( true == empty( $this->getSystemEmailPriorityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'system_email_priority_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', '' ) );
		}

		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		// Disable emails for Village at Merritt Park ( pid 253719 ).
		if( CClient::ID_HAMPSHIRE_ASSETS == $this->getCid() && 253719 == $this->getPropertyId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', 'Email service is disabled for Village at Merritt Park.' ) );
			return false;
		}
		return true;
	}

	public function valCustomerId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCustomerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'customer_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valCompanyEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompanyEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_employee_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', '' ) );
		}

		return $boolIsValid;
	}

	public function valToEmailAddress( $boolIsRcaTask = false ) {
		$boolIsValid = true;
		if( true == empty( $this->m_strToEmailAddress ) ) {
			$boolIsValid = false;
			if( true == $boolIsRcaTask ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Selected employee(s) does not contain email address.' ) ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'Selected company(s) does not contain email address.' ) ) );
			}
		} elseif( false == CValidation::validateEmailAddresses( $this->m_strToEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', __( 'A valid to email address is required. Email address passed is ' . $this->m_strToEmailAddress . '.' ) ) );
		}

		return $boolIsValid;
	}

	public function valRecipientEmailAddress() {
		$boolIsValid = true;

		if( false == valStr( $this->getToEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_email_address', __( 'Recipient email address is required.' ) ) );
		} elseif( false == CValidation::validateEmailAddress( $this->getToEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'to_email_address', __( 'Recipient email address is not a valid email address.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFromEmailAddress() {

		$boolIsValid = true;

		if( true == empty( $this->getFromEmailAddress() ) || false == CValidation::validateEmailAddress( $this->getFromEmailAddress(), true ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'from_email_address', 'From email address is not a valid email address.' ) );
		}

		return $boolIsValid;
	}

	public function valSubject() {
		$boolIsValid = true;

		if( true == empty( $this->getSubject() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'subject', __( 'Subject is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valHtmlContent() {
		$boolIsValid = true;

		if( true == empty( $this->getHtmlContent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'html_content', __( 'Content is required.' ) ) );
		}

		// Validate html content size. to be less than 20 MB.
		$intBytes = strlen( $this->getHtmlContent() );
		$intSize = number_format( $intBytes / 1048576, 0 );

		if( self::MAX_EMAIL_CONTET_SIZE < $intSize ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Email content size should be less than 20MB.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valScheduledSendDatetime() {
		$boolIsValid = true;

		if( true == empty( $this->getScheduledSendDatetime() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_datetime', 'Date is tequired' ) );
		}

		return $boolIsValid;
	}

	public function valSentOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getSentOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sent_on', '' ) );
		}

		return $boolIsValid;
	}

	public function valFailedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getFailedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'failed_on', '' ) );
		}

		return $boolIsValid;
	}

	public function valIsSuspended() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsSuspended() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_suspended', '' ) );
		}

		return $boolIsValid;
	}

	public function valIsSelfDestruct() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valToEmailAddress();
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valToEmailAddress();
				// Validation for from_email_address commented temporary because it's not supporting masking. [KAL]
				if( ( CSystemEmailType::APPLICATION_CONTACT_SUBMISSIONS != $this->getSystemEmailTypeId() ) && ( CSystemEmailType::MESSAGE_CENTER_EMAIL != $this->getSystemEmailTypeId() ) ) {
					$boolIsValid &= $this->valFromEmailAddress();
				}

				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valSystemEmailPriorityId();
				$boolIsValid &= $this->valSystemEmailTypeId();
				$boolIsValid &= $this->valScheduledSendDatetime();
				$boolIsValid &= $this->valHtmlContent();
				break;

			case 'validate_rca_email':
				$boolIsRcaTask = true;
				$boolIsValid &= $this->valToEmailAddress( $boolIsRcaTask );

				if( CSystemEmailType::APPLICATION_CONTACT_SUBMISSIONS != $this->getSystemEmailTypeId() ) {
					$boolIsValid &= $this->valFromEmailAddress();
				}

				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valSystemEmailPriorityId();
				$boolIsValid &= $this->valSystemEmailTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_html_content':
				$boolIsValid &= $this->valHtmlContent();
				break;

			// Validate Reputation Advisor Email Response
			case 'validate_ra_email_response':
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valToEmailAddress();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valHtmlContent();
				break;

			case 'validate_email_campaign':
				$boolIsValid &= $this->valFromEmailAddress();
				$boolIsValid &= $this->valToEmailAddress();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valHtmlContent();
				break;

			case 'validate_send_info_via_email':
				$boolIsValid &= $this->valRecipientEmailAddress();
				$boolIsValid &= $this->valSubject();
				$boolIsValid &= $this->valHtmlContent();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function buildStoragePath() {

		return PATH_MOUNTS_SYSTEM_EMAIL . $this->getCid() . '/' . date( 'Y', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $this->getScheduledSendDatetime() ) ) . '/';
	}

	public function getTempStoragePath() {
		return PATH_NON_BACKUP_MOUNTS_SYSTEM_EMAILS . date( 'Y-m-d', strtotime( $this->getScheduledSendDatetime() ) ) . '/content2/' . ( !empty( $this->getCid() ) ? $this->getCid() . '/' : '' );
	}

	public function buildS3StoragePath() : string {

		$intCid  = ( valId( $this->getCid() ) ) ? $this->getCid() . '/' : '';
		$strPath = sprintf( '%s%s/', $intCid, date( 'Y/m/d', strtotime( $this->getScheduledSendDatetime() ) ) );
		return $strPath;
	}

	public function extractDownloadedFile( $strSaveAsPath ) {

		if( true == is_null( $strSaveAsPath ) )	return NULL;

		$strSystemEmailTypeDatePath = date( 'd', strtotime( $this->getScheduledSendDatetime() ) );

		preg_match( '/(.+)\//', $strSaveAsPath, $arrstrMatches );

		$strSystemEmailPath		= current( $arrstrMatches );
		$strSystemEmailFullPath = $strSystemEmailPath . $strSystemEmailTypeDatePath . '/' . $this->getId();

		if( true == valStr( $strSaveAsPath ) && true == file_exists( $strSaveAsPath ) ) {

			if( false == is_readable( $strSystemEmailPath ) ) {
				CFileIo::recursiveMakeDir( $strSystemEmailPath );
			}

			// Untar file downloaded from AWS and removing the same tar file from temp folder.
			$strUnTarCommand = 'cd ' . $strSystemEmailPath . '; tar -xzvf ' . $strSaveAsPath . ' 2>&1';
			exec( $strUnTarCommand );
			exec( 'rm -rvf ' . $strSaveAsPath );
		}

		return $strSystemEmailFullPath;
	}

	public function downloadFileFromCloud() {
		$strCidPath = '';

		if( false == is_null( $this->getCid() ) ) {
			$strCidPath = $this->getCid() . '/';
		}

		$strSourcePath = 'system_email_type_' . $this->getSystemEmailTypeId() . '/' . $strCidPath . date( 'Y', strtotime( $this->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $this->getScheduledSendDatetime() ) );

		$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );

		$arrmixRequestConfig = array(
			'Container'	=> self::SYSTEM_EMAIL_AWS_BUCKET,
			'FilePath'	=> $strSourcePath . DIRECTORY_SEPARATOR . date( 'd', strtotime( $this->getScheduledSendDatetime() ) ) . '.tar.gz',
			'SaveAs'	=> PATH_NON_BACKUP_MOUNTS_TEMP_SYSTEM_EMAIL_AWS_S3
		);

		return $objObjectStorageGateway->downloadFile( $arrmixRequestConfig );
	}

	public function archiveSystemEmailAndAttachmentsToS3() : bool {

		$boolIsArchived = false;
		$strAmazonKey   = $this->buildS3StoragePath() . $this->getId() . '_html.txt';
		$strSourceFile  = ( !empty( $this->getTempStorageFilePath() ) ) ? $this->getTempStorageFilePath() : $this->getTempStoragePath() . $this->getId() . '_html.txt';

		$intSourceFileSize = CFileIo::getFileSize( $strSourceFile );

		if( false == $intSourceFileSize ) {
			return $boolIsArchived;
		}

		$arrmixParameters               = [];
		$arrmixParameters['key']        = $strAmazonKey;
		$arrmixParameters['sourceFile'] = $strSourceFile;

		// Syncing email content from Mounts to S3
		try {
			$strObjectUrl = $this->uploadFileToS3( $arrmixParameters );

			$this->setContentStorage( 'S3' );
			$this->setBucketName( CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET );
			$this->setContentSize( $intSourceFileSize );
			$this->setObjectUrl( $strObjectUrl );

			$boolIsArchived = true;
		} catch( \Exception $objException ) {
			return $boolIsArchived;
		}

		return $boolIsArchived;
	}

	public function storeContentToS3( $intUniqueId ) : bool {

		$boolIsArchived = false;
		$strAmazonKey   = $this->buildS3StoragePath() . $intUniqueId . '_html.txt';
		$strContent     = stripslashes( $this->getHtmlContent() );
		$intContentSize = strlen( $strContent );

		if( false == $intContentSize ) {
			return $boolIsArchived;
		}

		$arrmixParameters         = [];
		$arrmixParameters['key']  = $strAmazonKey;
		$arrmixParameters['data'] = $strContent;

		// Syncing email content from Mounts to S3
		try {
			$strObjectUrl = $this->uploadFileToS3( $arrmixParameters );
			if( !valStr( $strObjectUrl ) )
				return $boolIsArchived;

			$this->setContentStorage( 'S3' );
			$this->setBucketName( CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET );
			$this->setContentSize( $intContentSize );
			$this->setObjectUrl( $strObjectUrl );
			$this->setObjectKey( $strAmazonKey );
			$boolIsArchived = true;
		} catch( \Exception $objException ) {
			return $boolIsArchived;
		}

		return $boolIsArchived;
	}

	public function archiveEmailAttachmentsToS3( $objEmailAttachment ) : bool {

		$boolIsArchived = false;

		if( false == valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
			return $boolIsArchived;
		}

		$strAmazonKey      = $objEmailAttachment->getFilePath() . $objEmailAttachment->getFileName();
		$strSourceFile     = $objEmailAttachment->getTempStoragePath() . $objEmailAttachment->getFileName();
		$intSourceFileSize = CFileIo::getFileSize( $strSourceFile );

		if( false == $intSourceFileSize ) {
			return $boolIsArchived;
		}

		$arrmixParameters               = [];
		$arrmixParameters['key']        = $strAmazonKey;
		$arrmixParameters['sourceFile'] = $strSourceFile;

		// Syncing email attachment content from NonBackupMounts to S3
		try {
			$strObjectUrl   = $this->uploadFileToS3( $arrmixParameters );
			$boolIsArchived = valStr( $strObjectUrl );
		} catch( \Exception $objException ) {
			return $boolIsArchived;
		}

		return $boolIsArchived;
	}

	public function storeEmailAttachmentsToS3( $objEmailAttachment ) : bool {

		$boolIsArchived = false;

		if( false == valObj( $objEmailAttachment, CEmailAttachment::class ) ) {
			return $boolIsArchived;
		}

		$strFilePath        = $objEmailAttachment->buildStoragePath();
		$strFileName        = getUniqueString() . '_' . $objEmailAttachment->getFileName();
		$strAmazonKey       = $strFilePath . $strFileName;
		$strSourceFile      = str_replace( '//', '/', $objEmailAttachment->getFilePath() . '/' ) . $objEmailAttachment->getFileName();
		$intSourceFileSize  = CFileIo::getFileSize( $strSourceFile );

		if( false == $intSourceFileSize ) {
			return $boolIsArchived;
		}

		$arrmixParameters               = [];
		$arrmixParameters['key']        = $strAmazonKey;
		$arrmixParameters['sourceFile'] = $strSourceFile;

		// Syncing email attachment content from NonBackupMounts to S3
		try {
			$strObjectUrl   = $this->uploadFileToS3( $arrmixParameters );
			if( valStr( $strObjectUrl ) ) {
				$arrmixTempEmailAttachment['object_url'] = $strObjectUrl;
				$arrmixTempEmailAttachment['object_key'] = $strAmazonKey;
				$arrmixTempEmailAttachment['file_size'] = $intSourceFileSize;
				$arrmixEmailAttachmentDetails = $this->getEmailAttachmentDetails() ?? [];
				$arrmixEmailAttachmentDetails[] = $arrmixTempEmailAttachment;
				$objEmailAttachment->setFilePath( $strFilePath );
				$objEmailAttachment->setFileName( $strFileName );
				$objEmailAttachment->setEmailAttachmentDetails( $arrmixTempEmailAttachment );
				$this->setEmailAttachmentDetails( $arrmixEmailAttachmentDetails );
				$boolIsArchived = true;
			} else {
				$boolIsArchived = false;
			}
		} catch( \Exception $objException ) {
			return $boolIsArchived;
		}

		return $boolIsArchived;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchSystemEmailAttachments( $objDatabase ) {

		return CEmailAttachments::fetchEmailAttachmentsBySystemEmailId( $this->getId(), $objDatabase );
	}

	public function getOrFetchSystemEmailAttachments( $objDatabase ) {
		if( true == is_null( $this->m_arrobjEmailAttachments ) ) {
			return CEmailAttachments::fetchEmailAttachmentsBySystemEmailId( $this->getId(), $objDatabase );
		} else {
			return $this->m_arrobjEmailAttachments;
		}
	}

	public function fetchNextId( $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) || CDatabaseType::EMAIL != $objDatabase->getDatabaseTypeId() ) {
			CNewRelic::createService()->addCustomParameters( [ 'debug.system_email.fetch_next_id' => is_null( $objDatabase ) ? '' : $objDatabase->getDatabaseTypeId() ] );
			$objDatabase = CDatabases::createDatabase( CDatabaseUserType::PS_DEVELOPER, CDatabaseType::EMAIL );
		}

		return parent::fetchNextId( $objDatabase );
	}

	/**
	 * Database Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSkipQueue = false ) {

		$boolIsQueue = !in_array( $this->getSystemEmailTypeId(), CSystemEmailType::$c_arrintQueueExcludedSystemEmailTypeIds );

		if( ( false == $boolIsQueue || true == $boolIsSkipQueue ) && false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Invalid database object.', E_USER_WARNING );
			return false;
		}

		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == $this->validate( VALIDATE_INSERT ) ) {
			return false;
		}

		if( false == $boolIsSkipQueue && true == $boolIsQueue ) {

			// set created by and updated by putting message into queue.
			$this->setCreatedBy( $intCurrentUserId );
			$this->setUpdatedBy( $intCurrentUserId );
			if( \valObj( $objDatabase, CDatabase::class ) && true == is_resource( $objDatabase->getHandle() ) && PGSQL_CONNECTION_OK === pg_connection_status( $objDatabase->getHandle() ) ) {
				CNewRelic::createService()->addCustomParameters( [ 'debug.email_database.is_open' => true ] );
				if( true == $objDatabase->getIsTransaction() ) {
					CNewRelic::createService()->addCustomParameters( [ 'debug.email_database.is_transaction' => true ] );
				}
			}
			return $this->enqueue( $objDatabase );
		}

		if( true == empty( $this->getId() ) ) {
			$intId = parent::fetchNextId( $objDatabase );
			if( false == is_numeric( $intId ) ) return false;
			$this->setId( $intId );
		}

		// Check if the priority is set. If yes, then override the priority and Service provider as per system email types table.
		if( false == empty( $this->getSystemEmailTypeId() ) ) {

			$objSystemEmailType = CSystemEmailTypes::fetchCachedSystemEmailTypeById( $this->getSystemEmailTypeId(), $objDatabase );

			if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
				$this->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
			}
		}

		if( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA ) {
			$this->setEmailServiceProviderId( CEmailServiceProvider::DIRECT_MAIL_SMTP );
		}

		$this->validateAndProcessToEmailAddress();

		if( false == $this->storeContent( $this->getId(), true ) ) {
			return false;
		}

		if( false == parent::insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		if( false == $this->insertSystemEmailRecipients( $objDatabase ) ) {
			return false;
		}

		// Add default email event as pending while inserting system email if system email is associated with client or mass email. This will avoid insertion of multiple email events for same system email id.
		if( false == empty( $this->getCid() ) || false == empty( $this->getMassEmailId() ) ) {
			$this->addDefaultEmailEvent( $objDatabase );
		}

		if( true == $this->m_intIsResidentEmail ) {
			$this->insertDocument( $intCurrentUserId );
		}

		// checking attachement path code is added for backward compatibility and will be removed soon.
		if( true == valArr( $this->getEmailAttachments() ) || ( false != $this->getAttachmentPath() && false == empty( $this->getAttachmentPath() ) ) ) {
			return $this->insertAttachments( $intCurrentUserId, $objDatabase );
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
			return false;
		}

		if( false == parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		if( false == parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_html.txt' ) ) {
			unlink( $this->buildStoragePath() . $this->getId() . '_html.txt' );
		}

		if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_text.txt' ) ) {
			unlink( $this->buildStoragePath() . $this->getId() . '_text.txt' );
		}

		return true;
	}

	public function bulkDelete( $arrobjSystemEmails, $objDatabase ) {

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		// Added bulk delete code over here to avoid management company id condition in Delete statement.
		if( false == valArr( $arrobjSystemEmails ) ) {
			return false;
		}

		$arrintSystemEmailIds = array_keys( $arrobjSystemEmails );

		$strSql = 'DELETE FROM public.system_emails WHERE id IN (' . implode( ',', $arrintSystemEmailIds ) . ');';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			trigger_error( 'Facing some issue with the database ' . $objDatabase->getDatabaseId() . 'Error: ' . $objDatabase->errorMsg(), E_USER_WARNING );
			return false;
		} else {
			$objDataset->cleanup();

			$objObjectStorageGateway = CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_AMAZON_S3 );
			foreach( $arrobjSystemEmails as $objSystemEmail ) {
				if( true == file_exists( PATH_MOUNTS_SYSTEM_EMAIL . $objSystemEmail->getCid() . '/' . date( 'Y', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . $objSystemEmail->getId() . '_html.txt' ) ) {
					unlink( PATH_MOUNTS_SYSTEM_EMAIL . $objSystemEmail->getCid() . '/' . date( 'Y', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . $objSystemEmail->getId() . '_html.txt' );
				} else {
					$strAmazonKey		= ( valId( $arrmixSystemEmail['cid'] ) ? ( $arrmixSystemEmail['cid'] . '/' ) : '' ) . date( 'Y', strtotime( $strScheduledSendDateTime ) ) . '/' . date( 'm', strtotime( $strScheduledSendDateTime ) ) . '/' . date( 'd', strtotime( $strScheduledSendDateTime ) ) . '/' . $intSystemEmailId . '_html.txt';
					$objGetResponse = $objObjectStorageGateway->getObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, 'checkExists' => true ] );
					if( true == $objGetResponse->isSuccessful() ) {
						$objObjectStorageGateway->deleteObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, ] ); // remove file from Amazon S3
					}
				}

				if( true == file_exists( PATH_MOUNTS_SYSTEM_EMAIL . $objSystemEmail->getCid() . '/' . date( 'Y', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . $objSystemEmail->getId() . '_text.txt' ) ) {
					unlink( PATH_MOUNTS_SYSTEM_EMAIL . $objSystemEmail->getCid() . '/' . date( 'Y', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'm', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . date( 'd', strtotime( $objSystemEmail->getScheduledSendDatetime() ) ) . '/' . $objSystemEmail->getId() . '_text.txt' );
				} else {
					$strAmazonKey		= ( valId( $arrmixSystemEmail['cid'] ) ? ( $arrmixSystemEmail['cid'] . '/' ) : '' ) . date( 'Y', strtotime( $strScheduledSendDateTime ) ) . '/' . date( 'm', strtotime( $strScheduledSendDateTime ) ) . '/' . date( 'd', strtotime( $strScheduledSendDateTime ) ) . '/' . $intSystemEmailId . '_text.txt';
					$objGetResponse = $objObjectStorageGateway->getObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, 'checkExists' => true ] );
					if( true == $objGetResponse->isSuccessful() ) {
						$objObjectStorageGateway->deleteObject( [ 'Container' => CSystemEmail::SYSTEM_EMAIL_AWS_BUCKET, 'Key' => $strAmazonKey, ] ); // remove file from Amazon S3
					}
				}
			}
			return true;
		}
	}

	public function deleteHTML() {

		if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_html.txt' ) ) {
			unlink( $this->buildStoragePath() . $this->getId() . '_html.txt' );
		}

		if( true == file_exists( $this->buildStoragePath() . $this->getId() . '_text.txt' ) ) {
			unlink( $this->buildStoragePath() . $this->getId() . '_text.txt' );
		}

		return true;
	}

	// this function will store the email in documents

	public function insertDocument( $intCurrentUserId ) {

		$objFileLibrary = new CFileLibrary( $this->m_intCid );

		$objFileLibrary->setLeaseId( $this->m_intLeaseId );
		$objFileLibrary->setPropertyId( $this->m_intPropertyId );
		$objFileLibrary->setFileTypeSystemCode( $this->m_strFileTypeSystemCode );
		$objFileLibrary->setDocumentManager( $this->m_objDocumentManager );
		$objFileLibrary->setCompanyUserId( $intCurrentUserId );
		$objFileLibrary->setSystemEmail( $this );
		$objFileLibrary->setClientDatabase( $this->m_objClientDatabase );

		$objFileLibrary->insertDocument();

		return true;
	}

	/**
	 * Other Functions
	 *
	 */

	public function sendEmail( $objDatabase ) {

		if( true == is_null( $this->getHtmlContent() ) ) {
			return false;
		}

		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Please use email database object for system email functionality.', E_USER_WARNING );
		}

		// get email attachement
		$this->m_arrobjEmailAttachments	= $this->getOrFetchSystemEmailAttachments( $objDatabase );

		$this->validateAndProcessEmailPolicy();

		switch( $this->getEmailServiceProviderId() ) {

			case CEmailServiceProvider::SEND_GRID_SECONDARY_API:
			case CEmailServiceProvider::SEND_GRID_PRIMARY_API:

				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( $this->getEmailServiceProviderId(), $objDatabase );

				$arrmixMergeFields = $this->loadMergeFields( $this->getId(), $objDatabase );

				$objSendGridMail  = new CSendGridMail();
				$objSendGridMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objSendGridMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );
				$objSendGridMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objSendGridMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_SEND_GRID_USERNAME );
				$objSendGridMail->setEncryptionKeyPassword( CONFIG_SODIUM_KEY_SEND_GRID_PASSWORD );
				$objSendGridMail->setValuesForApi( $this );

				// Process Email
				if( true == valArr( $arrmixMergeFields ) ) {
					foreach( $arrmixMergeFields as $arrmixBunchMergeFields ) {
						$objTempSendGridMail = clone $objSendGridMail;

						$objTempSendGridMail->setMergeFields( $arrmixBunchMergeFields );

						$boolIsValid = $objTempSendGridMail->sendEmailByApi();

						if( false == $boolIsValid ) break;
					}
				} else {
					$boolIsValid = $objSendGridMail->sendEmailByApi();
				}
				break;

			case CEmailServiceProvider::MAN_DRILL:
				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( CEmailServiceProvider::MAN_DRILL, $objDatabase );

				$objMandrillMail = new CMandrillMail();
				$objMandrillMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objMandrillMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );
				$objMandrillMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objMandrillMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_MANDRILL_USERNAME );
				$objMandrillMail->setEncryptionKeyPassword( NULL );
				$objMandrillMail->setValuesForApi( $this );
				$boolIsValid = $objMandrillMail->sendEmailByApi();
				break;

			case CEmailServiceProvider::PSI:
				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( CEmailServiceProvider::PSI, $objDatabase );

				$this->loadSMTPCommonData( $objDatabase );

				$arrmixMergeFields = $this->loadMergeFields( $this->getId(), $objDatabase );

				// As our local SMTP is pointing to SendGrid SMTP, adding X-SMTPAPI header for tracking email status from SendGrid
				$objSendGridMail = new CSendGridMail();
				$objSendGridMail->setCategory( $this->getSystemEmailTypeId() );
				$objSendGridMail->setCid( $this->getCid() );
				$objSendGridMail->setSystemEmailId( $this->getId() );
				$objSendGridMail->setScheduledEmailId( $this->getScheduledEmailId() );
				$objSendGridMail->setSystemEmailTypeId( $this->getSystemEmailTypeId() );
				$objSendGridMail->setMassEmailId( $this->getMassEmailId() );

				// Process Email
				$boolIsValid = $this->processEmail( $arrmixMergeFields, $objSendGridMail, $objEmailServiceProvider );
				break;

			case CEmailServiceProvider::SEND_GRID_SECONDARY_SMTP:
			case CEmailServiceProvider::SEND_GRID_PRIMARY_SMTP:
			case CEmailServiceProvider::SEND_GRID_KMV_SMTP:

				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( $this->getEmailServiceProviderId(), $objDatabase );

				$arrmixMergeFields = $this->loadMergeFields( $this->getId(), $objDatabase );
				$this->loadSMTPCommonData( $objDatabase );

				$objSendGridMail = new CSendGridMail();
				$objSendGridMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objSendGridMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );

				$objSendGridMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objSendGridMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_SEND_GRID_USERNAME );
				$objSendGridMail->setEncryptionKeyPassword( CONFIG_SODIUM_KEY_SEND_GRID_PASSWORD );

				// Set values for tracking email status from SendGrid
				$objSendGridMail->setCategory( $this->getSystemEmailTypeId() );
				$objSendGridMail->setCid( $this->getCid() );
				$objSendGridMail->setSystemEmailId( $this->getId() );
				$objSendGridMail->setScheduledEmailId( $this->getScheduledEmailId() );
				$objSendGridMail->setMassEmailId( $this->getMassEmailId() );
				$objSendGridMail->setSystemEmailTypeId( $this->getSystemEmailTypeId() );

				// Process Email
				$boolIsValid = $this->processEmail( $arrmixMergeFields, $objSendGridMail, NULL );
				break;

			case CEmailServiceProvider::MAN_DRILL_SMTP:
				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( CEmailServiceProvider::MAN_DRILL_SMTP, $objDatabase );

				$this->loadSMTPCommonData( $objDatabase );

				$objMandrillMail = new CMandrillMail();
				$objMandrillMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objMandrillMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );
				$objMandrillMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objMandrillMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_MANDRILL_USERNAME );
				$objMandrillMail->setEncryptionKeyPassword( NULL );

				// Set values for tracking email status from Mandrill
				$objMandrillMail->setCategory( $this->getSystemEmailTypeId() );
				$objMandrillMail->setCid( $this->getCid() );
				$objMandrillMail->setSystemEmailId( $this->getId() );
				$objMandrillMail->setScheduledEmailId( $this->getScheduledEmailId() );
				$objMandrillMail->setMassEmailId( $this->getMassEmailId() );
				$objMandrillMail->setSystemEmailTypeId( $this->getSystemEmailTypeId() );

				// Setting mandril header data
				$objMandrillMail->setSmtpHeader( $this->m_objMimeEmail );

				$boolIsValid = $this->m_objEmailServiceProvider->sendMailBySmtp( $this, $this->m_objMimeEmail, $objMandrillMail );
				break;

			case CEmailServiceProvider::SEND_CLOUD_API:
				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( $this->getEmailServiceProviderId(), $objDatabase );

				$arrmixMergeFields = $this->loadMergeFields( $this->getId(), $objDatabase );

				$objSendCloudMail = new CSendCloudMail();
				$objSendCloudMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objSendCloudMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );
				$objSendCloudMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objSendCloudMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_EMAIL_SERVICE_USERNAME );
				$objSendCloudMail->setEncryptionKeyPassword( CONFIG_SODIUM_KEY_EMAIL_SERVICE_PASSWORD );

				// Process Email
				$objSendCloudMail->setValuesForApi( $this );

				if( true == valArr( $arrmixMergeFields ) ) {
					foreach( $arrmixMergeFields as $arrmixBunchMergeFields ) {
						$objTempSendCloudMail = clone $objSendCloudMail;

						$objTempSendCloudMail->setMergeFields( $arrmixBunchMergeFields );

						$boolIsValid = $objTempSendCloudMail->sendEmailByApi();

						if( false == $boolIsValid ) {
							break;
						}
					}
				} else {
					$boolIsValid = $objSendCloudMail->sendEmailByApi();
				}
				break;

			case CEmailServiceProvider::DIRECT_MAIL_SMTP:
				$objEmailServiceProvider = CEmailServiceProviders::fetchEmailServiceProviderById( $this->getEmailServiceProviderId(), $objDatabase );

				$arrmixMergeFields = $this->loadMergeFields( $this->getId(), $objDatabase );

				$objDirectMail = new CDirectMail();
				$this->m_objMimeEmail = new CPsHtmlMimeMail();

				$objDirectMail->setValuesForSmtp( $this, $this->m_objMimeEmail, $objDatabase );

				$objDirectMail->setUsernameEncrypted( $objEmailServiceProvider->getUsernameEncrypted() );
				$objDirectMail->setPasswordEncrypted( $objEmailServiceProvider->getPasswordEncrypted() );

				$objDirectMail->setServiceUrl( $objEmailServiceProvider->getServiceUrl() );
				$objDirectMail->setEncryptionKeyUserName( CONFIG_SODIUM_KEY_EMAIL_SERVICE_USERNAME );
				$objDirectMail->setEncryptionKeyPassword( CONFIG_SODIUM_KEY_EMAIL_SERVICE_PASSWORD );

				// Set values for tracking email status from SendGrid
				$objDirectMail->setCategory( $this->getSystemEmailTypeId() );
				$objDirectMail->setCid( $this->getCid() );
				$objDirectMail->setSystemEmailId( $this->getId() );
				$objDirectMail->setScheduledEmailId( $this->getScheduledEmailId() );
				$objDirectMail->setMassEmailId( $this->getMassEmailId() );
				$objDirectMail->setSystemEmailTypeId( $this->getSystemEmailTypeId() );
				// Process Email
				$boolIsValid = $objDirectMail->sendMailBySmtp( $this, $this->m_objMimeEmail );
				break;

			default:
				return false;
				break;
		}

		if( true == $boolIsValid && 1 == $this->getIsSelfDestruct() ) {
			$this->deleteHTML();
		}

		if( self::NOREPLY_EMAIL_ADDRESS == $this->getFromEmailAddress() && false == is_null( $this->getReplyToEmailAddress() ) ) {
			$this->setFromEmailAddress( $this->getReplyToEmailAddress() );
		}

		return $boolIsValid;
	}

	// In case of SMTP, we prepare object of MimeEmail and setValues.

	public function loadSMTPCommonData( $objDatabase ) {

		$this->m_objMimeEmail = new CPsHtmlMimeMail();

		$this->m_objEmailServiceProvider  = new CEmailServiceProviderMaster();
		$this->m_objEmailServiceProvider->setValuesForSmtp( $this, $this->m_objMimeEmail, $objDatabase );

		return true;
	}

	public function loadMergeFields( $intSystemEmailId, $objDatabase ) {

		$arrmixTempSystemEmailRecipients = CSystemEmailRecipients::fetchSystemEmailRecipientsEmailAddressBySystemEmailId( $intSystemEmailId, $objDatabase );

		// We need to prepare array for sendgrid header and it sholud be : "to": ["mail1@domain.com", "mail2@domain.com"], "merge_fields": {"MERGE_FIELD_PATTERN": ["Benny", "Chaim"]}
		$arrmixMergeFields		= [];
		$arrstrMergeFieldKeys	= [];
		$arrmixFinalMergeFields	= [];

		if( true == valArr( $arrmixTempSystemEmailRecipients ) ) {
			// Split the array into bunch of 10,000 items as send-grid has the hard limit 10,000 for recipeants per email.
			$arrmixTempSystemEmailRecipients = array_chunk( $arrmixTempSystemEmailRecipients, 5000, true );

			foreach( $arrmixTempSystemEmailRecipients as $arrmixSystemEmailRecipients ) {
				$arrmixAllMergeFields = [];

				// Assign all to_email_address and merge fields values
				foreach( $arrmixSystemEmailRecipients as $arrmixRecipientValue ) {

					// Sample serialize data : a:1:{s:18:"User_Email_Address";s:17:"sbhandari@xento.com";} and sample unserialize data : array("User_Email_Address"=>"sbhandari@xento.com");
					$arrmixMergeFields 		= ( false == empty( $arrmixRecipientValue['merge_fields'] ) ) ? unserialize( $arrmixRecipientValue['merge_fields'] ) : array();
					$arrstrMergeFieldKeys	= ( false == valArr( $arrstrMergeFieldKeys ) && true == valArr( $arrmixMergeFields ) ) ? array_keys( $arrmixMergeFields ) : $arrstrMergeFieldKeys;

					$arrmixAllMergeFields['to'][] = $arrmixRecipientValue['email_address'];
					$strTempMergeFieldKey = '';
					if( true == valArr( $arrstrMergeFieldKeys ) ) {
						foreach( $arrstrMergeFieldKeys as $strMergeFieldKey ) {
							if( CEmailServiceProvider::SEND_CLOUD_API == $this->getEmailServiceProviderId() ) {
								$strTempMergeFieldKey = preg_replace( '/[^a-zA-Z0-9_ ]/', '%', $strMergeFieldKey );
								$strMergeFieldKey     = $strTempMergeFieldKey;
							}
							if( self::MERGE_FIELD_PATTERN_ENCRYPT_URL_KEY == $strMergeFieldKey ) {
								$arrmixMergeFields[$strMergeFieldKey] = \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( '&system_email_recipient_id=' . $arrmixRecipientValue['id'], CONFIG_SODIUM_KEY_MESSAGE_CENTER_EMAIL );
							}
							$arrmixAllMergeFields['merge_fields'][$strMergeFieldKey][] = ( false == empty( $arrmixMergeFields[$strMergeFieldKey] ) ) ? $arrmixMergeFields[$strMergeFieldKey] : '';
						}
					}

					if( true == empty( $arrmixAllMergeFields['merge_fields'][CMassEmail::MERGE_FIELD_PATTERN_USER_EMAIL_ADDRESS] ) ) {
						$arrmixAllMergeFields['merge_fields'][CMassEmail::MERGE_FIELD_PATTERN_USER_EMAIL_ADDRESS][] = $arrmixRecipientValue['email_address'];
					}

					$arrmixAllMergeFields['merge_fields'][self::MERGE_FIELD_PATTERN_SYSTEMEMAIL_RECIPIENTS][]		= $arrmixRecipientValue['id'];

					if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixAllMergeFields['merge_fields'][self::MERGE_FIELD_PATTERN_SYSTEMEMAIL_RECIPIENTS] ) ) $intFirstSystemEmailRecipient = $arrmixRecipientValue['id'];
				}

				// Getting one weired issue in which, first email is always associating with last SystemEmailRecipient Id.
				$arrmixAllMergeFields['merge_fields'][self::MERGE_FIELD_PATTERN_SYSTEMEMAIL_RECIPIENTS][] = $intFirstSystemEmailRecipient;
				$arrmixFinalMergeFields[] = $arrmixAllMergeFields;
			}

		}

		return $arrmixFinalMergeFields;
	}

	public function addDefaultEmailEvent( $objDatabase ) {

		$strSql 			= '';
		$arrmixEmailEvent 	= array();

		$arrmixEmailEvent['cid']					= ( false == is_null( $this->getCid() ) ) ? ( string ) $this->getCid() : 'NULL';
		$arrmixEmailEvent['systemEmailId']			= $this->getId();
		$arrmixEmailEvent['systemEmailTypeId']		= ( false == is_null( $this->getSystemEmailTypeId() ) ) ? ( string ) $this->getSystemEmailTypeId() : 'NULL';
		$arrmixEmailEvent['scheduledEmailId'] 		= ( false == is_null( $this->getScheduledEmailId() ) ) ? ( string ) $this->getScheduledEmailId() : 'NULL';
		$arrmixEmailEvent['emailServiceProviderId']	= ( int ) CEmailEvent::SEND_GRID_USER_ID;
		$arrmixEmailEvent['response']				= 'NULL';
		$arrmixEmailEvent['statusCode']				= 'NULL';
		$arrmixEmailEvent['url'] 					= 'NULL';
		$arrintMandrilServiceProviders				= array( CEmailServiceProvider::MAN_DRILL, CEmailServiceProvider::MAN_DRILL_SMTP );

		// set default event as Pending
		$arrmixEmailEvent['emailEventTypeId']	    = CEmailEventType::PENDING;

		if( true == in_array( $this->getEmailServiceProviderId(), $arrintMandrilServiceProviders ) ) {
			$intServiceProviderUserId = CEmailEvent::MANDRILL_USER_ID;
		}

		$arrstrRecipientEmailAddresses = $this->loadSystemEmailRecipients( $this->getId(), $objDatabase );

		if( false == valArr( $arrstrRecipientEmailAddresses ) ) {
			$arrstrRecipientEmailAddresses = explode( ',', $this->getToEmailAddress() );
		}

		if( true == valArr( $arrstrRecipientEmailAddresses ) ) {

			$objEmailEvent = new CEmailEvent();
			foreach( $arrstrRecipientEmailAddresses as $intSystemEmailRecipientId => $strRecipientEmailAddress ) {
				if( true == is_null( $strRecipientEmailAddress ) ) {
					continue;
				}

				$strRecipientEmailAddress = '\'' . addslashes( $strRecipientEmailAddress ) . '\'';

				$strPattern = '/[\w\-_.+]+@[\w\-_.]+/i';
				preg_match( $strPattern, $strRecipientEmailAddress, $arrstrMatches );
				$strRecipientEmailAddress = ( false == empty( $arrstrMatches[0] ) ) ? $arrstrMatches[0] : $strRecipientEmailAddress;

				$strRecipientEmailAddress = '\'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strRecipientEmailAddress ) ) . '\'';

				$arrmixEmailEvent['recipientEmailAddress']	= $strRecipientEmailAddress;
				$arrmixEmailEvent['systemEmailRecipientId']	= $intSystemEmailRecipientId;

				$objTempEmailEvent = clone $objEmailEvent;
				$strSql .= $objTempEmailEvent->insertEmailEvent( $arrmixEmailEvent );

			}

			return fetchData( $strSql, $objDatabase );
		}

		return false;
	}

	public function loadSystemEmailRecipients( $intSystemEmailId, $objDatabase ) {

		$arrstrRecipientEmailAddresses	= array();
		$arrmixSystemEmailRecipients	= ( array ) CSystemEmailRecipients::fetchSystemEmailRecipientsEmailAddressBySystemEmailId( $intSystemEmailId, $objDatabase );

		if( false == valArr( $arrmixSystemEmailRecipients ) ) {
			return $arrstrRecipientEmailAddresses;
		}

		if( true == valArr( $arrmixSystemEmailRecipients ) ) {
			foreach( $arrmixSystemEmailRecipients as $arrstrRecipientValue ) {
				$arrstrRecipientEmailAddresses[$arrstrRecipientValue['id']] = $arrstrRecipientValue['email_address'];
			}
		}

		return $arrstrRecipientEmailAddresses;
	}

	// Process email though SMTP, We are processing multiple recipient in bunch of 10,000 rows as SendGrid has the hard limit 10,000 on recipient per email.

	public function processEmail( $arrmixMergeFields, $objSendGridMail, $objEmailServiceProvider ) {
		if( true == valArr( $arrmixMergeFields ) ) {
			foreach( $arrmixMergeFields as $arrmixBunchMergeFields ) {
				$objTempSendGridMail = clone $objSendGridMail;

				$objTempSendGridMail->setMergeFields( $arrmixBunchMergeFields );

				// Setting sendgrid X-SMTPAPI header data
				$this->m_objMimeEmail->setHeader( 'X-SMTPAPI', $objTempSendGridMail->getSmtpApiHeader() );

				$boolIsValid = $this->m_objEmailServiceProvider->sendMailBySmtp( $this, $this->m_objMimeEmail, ( true == is_null( $objEmailServiceProvider ) ) ? $objTempSendGridMail : $objEmailServiceProvider );

				if( false == $boolIsValid ) break;
			}
		} else {
			// Setting sendgrid X-SMTPAPI header data
			$this->m_objMimeEmail->setHeader( 'X-SMTPAPI', $objSendGridMail->getSmtpApiHeader() );

			$boolIsValid = $this->m_objEmailServiceProvider->sendMailBySmtp( $this, $this->m_objMimeEmail, ( true == is_null( $objEmailServiceProvider ) ) ? $objSendGridMail : $objEmailServiceProvider );
		}

		return $boolIsValid;
	}

	public function validateAndProcessEmailPolicy() {

		// RFC5322
		$this->setFromEmailAddress( str_replace( ',', '', $this->getFromEmailAddress() ) );

		// DMARC Poilcy
		$boolStatus = ( bool ) ( ( int ) \Psi\CStringService::singleton()->stripos( $this->getFromEmailAddress(), '@yahoo.' ) || ( int ) \Psi\CStringService::singleton()->stripos( $this->getFromEmailAddress(), '@gmail.' ) || ( int ) \Psi\CStringService::singleton()->stripos( $this->getFromEmailAddress(), '@hotmail.' )
		                         || ( int ) \Psi\CStringService::singleton()->stripos( $this->getFromEmailAddress(), '@aol.' ) || ( int ) \Psi\CStringService::singleton()->stripos( $this->getFromEmailAddress(), '@rediffmail.' ) );

		if( false == $boolStatus ) return true;

		$this->setReplyToEmailAddress( $this->getFromEmailAddress() );
		$this->setFromEmailAddress( self::NOREPLY_EMAIL_ADDRESS );
	}

	public function validateAndProcessToEmailAddress() {

		// This function will take care all the validation for ToEmailAddress while inserting into System Emails.

		if( true == \Psi\CStringService::singleton()->stristr( $this->getToEmailAddress(), 'yardi.com' ) || true == \Psi\CStringService::singleton()->stristr( $this->getToEmailAddress(), 'mrisoftware.com' ) ) {
			$this->setToEmailAddress( str_replace( 'yardi.com', 'yardi.lcl', $this->getToEmailAddress() ) );
			$this->setToEmailAddress( str_replace( 'mrisoftware.com', 'mrisoftware.lcl', $this->getToEmailAddress() ) );
		}

		if( true == \Psi\CStringService::singleton()->strstr( $this->getToEmailAddress(), ';' ) ) {
			$this->setToEmailAddress( str_replace( ';', ',', $this->getToEmailAddress() ) );
		}
	}

	/**
	 * toArray() is used to queue system email into queue (RabbitMQ). Any changes to this would directly impact queue message size and it's function. Please talk to Entrata PaaS team if any changes are required here.
	 */
	public function toArray() {
		$arrmixSystemEmail                      = parent::toArray();
		$arrmixSystemEmail['html_content_path'] = $this->getHtmlContentPath();
		$arrmixSystemEmail['text_content_path'] = $this->getTextContentPath();
		$arrmixSystemEmail['html_file_size']    = $this->getHtmlFileSize();

		// The following line is backward compatible code to set email attachment path, It will be removed soon.
		$arrmixSystemEmail['attachment_path']   = $this->getAttachmentPath();

		$arrobjEmailAttachments = $this->getEmailAttachments();

		if( true == valArr( $arrobjEmailAttachments ) ) {
			$arrmixEmailAttachments = [];
			foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
				$arrmixEmailAttachments[] = $objEmailAttachment->toArray();
			}
			$arrmixSystemEmail['email_attachments'] = $arrmixEmailAttachments;
		}

		$arrobjSystemEmailRecipients = $this->getSystemEmailRecipients();

		if( true == valArr( $arrobjSystemEmailRecipients ) ) {
			$arrmixSystemEmailRecipients = [];
			foreach( $arrobjSystemEmailRecipients as $objSystemEmailRecipient ) {
				$arrmixSystemEmailRecipients[] = $objSystemEmailRecipient->toArray();
			}
			$arrmixSystemEmail['system_email_recipients'] = $arrmixSystemEmailRecipients;
		}
		return $arrmixSystemEmail;
	}

	private function enqueue( $objDatabase ) {

		if( false == $this->validateSystemEmailWithAttachments( $objDatabase ) ) {
			return false;
		}

		$this->calculateSystemEmailContentSize();

		$intTotalEmailSize = $this->getTotalEmailSize();
		// validate if email size ( Html content size + Text content size + Email attachments size ) > 30 MB( 31457280 bytes ). If greater, return false.
		if( $intTotalEmailSize > 31457280 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Email size is greater than 30 MB' ) );
			return false;
		}

		$strUniqId = uniqid( rand( 100000, 999999 ) . '.', true );

		if( false == $this->storeContent( $strUniqId ) ) {
			return false;
		}

		$this->setHtmlContent( NULL );
		$this->setTextContent( NULL );

		$boolUploadToNonBackupMounts = false;
		if( true == CSystemEmailType::checkIsContentStoreOnCloud( $this->getSystemEmailTypeId() ) ) {
			$boolUploadToNonBackupMounts = true;
		}

		// copy attachment to PATH_MOUNTS_SYSTEM_EMAIL_ATTACHMENTS.
		$arrobjEmailAttachments = ( array ) $this->getEmailAttachments();
		if( true == valArr( $arrobjEmailAttachments ) ) {
			foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
				if( $boolUploadToNonBackupMounts && 'production' == CONFIG_ENVIRONMENT ) {
					if( false == $this->storeEmailAttachmentsToS3( $objEmailAttachment ) ) {
						return false;
					}
				} else {
					if( false == $objEmailAttachment->uploadEmailAttachment( $objEmailAttachment, $boolUploadToNonBackupMounts ) ) {

						$this->addErrorMsgs( $objEmailAttachment->getErrorMsgs() );
						return false;
					}
				}
			}
		}

		$this->setQueuedOn( date( 'm/d/Y H:i:s' ) );

		$objSystemEmailMessage = new CSystemEmailMessage();
		$objSystemEmailMessage->setSystemEmail( $this );
		$objSystemEmailMessage->setAttempt( self::MAX_REQUEUE_ATTEMPT );
		$objSystemEmailMessage->setAttemptTtl( self::MAX_TTL );
		$objSystemEmailMessage->setMessageProperties( [ 'priority' => CSystemEmailPriority::$c_arrintQueuePriorities[$this->getSystemEmailPriorityId()] ] );

		try {
			$objExchangeMessageSender = ( new \Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createExchangeSender( new \Psi\Libraries\Queue\Factory\CAmqpMessageFactory(), self::EXCHANGE_NAME, self::EXCHANGE_TYPE );
			$objExchangeMessageSender->setIsBlockNotification( true );
			// Need to check whether this code required or not and if not required then will remove below 3 lines.
			$strAMQPUserName	= \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( CONFIG_AMQP_USERNAME, CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME, 'mode' => MCRYPT_MODE_CBC ] );
			$strAMQPPassword	= \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( CONFIG_AMQP_PASSWORD, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD, 'mode' => MCRYPT_MODE_CBC ] );
			$objExchangeMessageSender->setConnection( new \Psi\Libraries\Queue\CAmqpConnection( true, CONFIG_AMQP_HOST, CONFIG_AMQP_PORT, $strAMQPUserName, $strAMQPPassword ) );

			$objExchangeMessageSender->addQueue( self::QUEUE_NAME, [ 'x-max-priority' => self::MAX_QUEUE_PRIORITY ] )
				->bindQueue( self::QUEUE_NAME, self::QUEUE_NAME )
				->send( $objSystemEmailMessage, [], self::QUEUE_NAME );

			$objExchangeMessageSender->close();
		} catch( Exception $objException ) {
			$this->setContent();
			$this->setQueuedOn( NULL );

			$boolIsTransaction = $objDatabase->getIsTransaction();

			if( false == $boolIsTransaction ) {
				$objDatabase->begin();
			}

			$boolInsert = $this->insert( $this->getCreatedBy(), $objDatabase, false, true );

			if( false == $boolIsTransaction ) {
				if( $boolInsert ) {
					$objDatabase->commit();
				} else {
					$objDatabase->rollback();
				}
			}

			if( false == $boolIsOpen ) {
				$objDatabase->close();
			}

			trigger_error( 'Inserted directly into database as unable to queue system email into rabbitmq. System Email Type ID: ' . $this->getSystemEmailTypeId() . '. Error: ' . $objException->getMessage() . ' . Host: ' . CONFIG_AMQP_HOST . '-' . CONFIG_AMQP_PORT, E_USER_WARNING );
			return $boolInsert;
		}

		return true;
	}

	private function storeContent( $intUniqueId, $boolIsInserted = false ) {

		if( true == CSystemEmailType::checkIsContentStoreOnCloud( $this->getSystemEmailTypeId() ) ) {
			$boolTempStorage = true;
			$strPath   = $this->getTempStoragePath();
		} else {
			$boolTempStorage = false;
			$strPath   = $this->buildStoragePath();
		}

		if( $boolTempStorage ) {
			if( !empty( $this->getObjectUrl() ) ) {
				return true;
			}
			if( 'production' == CONFIG_ENVIRONMENT ) {
				return $this->storeContentToS3( $intUniqueId );
			}
		}

		CFileIo::createInDepthDir( $strPath );

		// Now make the two files for each Integration result.
		$strHtmlFileName = $strPath . $intUniqueId . '_html.txt';
		$strTextFileName = $strPath . $intUniqueId . '_text.txt';

		// Write $somecontent to our opened file.
		if( ( false == $boolTempStorage || empty( $this->getTempStorageFilePath() ) ) && false === CFileIo::filePutContents( $strHtmlFileName, stripslashes( $this->getHtmlContent() ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Could not write html data to file for ID : ' . ( int ) $intUniqueId ) );
			return false;
		} else {
			$this->setHtmlContentPath( $strHtmlFileName );

			if( true == $boolIsInserted && 'production' == CONFIG_ENVIRONMENT ) {
				if( true == $boolTempStorage ) {
					if( $this->archiveSystemEmailAndAttachmentsToS3() ) {
						$strSourceFile = ( !empty( $this->getTempStorageFilePath() ) ) ? $this->getTempStorageFilePath() : $this->getTempStoragePath() . $this->getId() . '_html.txt';
						// if( 'production' == CONFIG_ENVIRONMENT ) CFileIo::deleteFile( $strSourceFile );
					}
					$this->setHtmlContentPath( $this->getTempStorageFilePath() );
				} elseif( !empty( $this->getTempStorageFilePath() ) && $this->getHtmlContentPath() != $this->getTempStorageFilePath() ) {
					if( true == CFileIo::fileExists( $this->getTempStorageFilePath() ) ) {
						return CFileIo::deleteFile( $this->getTempStorageFilePath() );
					}
				}
			}
		}

		if( true == valStr( $this->m_strTextContent ) ) {

			// Write $somecontent to our opened file.
			if( false === CFileIo::filePutContents( $strTextFileName, stripslashes( $this->getTextContent() ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Could not write text data to file for ID : ' . ( int ) $intUniqueId ) );
				return false;
			} else {
				$this->setTextContentPath( $strTextFileName );
			}

		}

		return true;
	}

	private function setContent() {

		if( true == valStr( $this->m_strHtmlContentPath ) && true == CFileIo::fileExists( $this->m_strHtmlContentPath ) ) {
			$strHtmlContent     = CFileIo::fileGetContents( $this->m_strHtmlContentPath );
			$this->setHtmlContent( $strHtmlContent );

			return true;
		}

		if( true == valStr( $this->m_strTextContentPath ) && true == CFileIo::fileExists( $this->m_strTextContentPath ) ) {
			$strTextContent     = CFileIo::fileGetContents( $this->m_strTextContentPath );
			$this->setTextContent( $strTextContent );

			return true;
		}

		return false;
	}

	private function insertAttachments( $intCurrentUserId, $objDatabase ) {

		$boolHasAttachments = false;
		$boolUploadToNonBackupMounts = false;
		if( true == CSystemEmailType::checkIsContentStoreOnCloud( $this->getSystemEmailTypeId() ) ) {
			$boolUploadToNonBackupMounts = true;
		}
		// This code will be executed when the queue message is set with EmailAttachment Objects.
		$arrobjEmailAttachments = ( array ) $this->getEmailAttachments();
		if( true == valArr( $arrobjEmailAttachments ) ) {
			$arrintEmailAttachmentIds 		= getObjectFieldValuesByFieldName( 'Id', $arrobjEmailAttachments );
			$arrobjExistingEmailAttachments = ( array ) \Psi\Eos\Email\CEmailAttachments::createService()->fetchEmailAttachmentsByIds( $arrintEmailAttachmentIds, $objDatabase );
			foreach( $arrobjEmailAttachments as $objEmailAttachment ) {
				$arrmixEmailAttachmentDetails = $objEmailAttachment->getEmailAttachmentDetails();
				$boolDirectInsert = false;

				if( !empty( getArrayElementByKey( 'object_url', $arrmixEmailAttachmentDetails ) ) ) {
					$boolDirectInsert = true;
				} elseif( 'production' == CONFIG_ENVIRONMENT ) {
					if( false == $this->storeEmailAttachmentsToS3( $objEmailAttachment ) ) {
						return false;
					}
					$boolDirectInsert = true;
				}
				// Removing Unique string from file name which was added while uploading file to Mounts/system_email_attachments.
				$strAttachmentNewFileName = preg_replace( '/^(\w)*\.(\d)*_/', '', $objEmailAttachment->getFileName() );
				if( $boolDirectInsert ) {
					if( empty( $objEmailAttachment->getFileExtensionId() ) ) {
						$arrstrFileInfo = pathinfo( $objEmailAttachment->getFileName() );
						$intExtensionId = $objEmailAttachment->fetchFileExtensionId( $arrstrFileInfo['extension'], $objDatabase );
						$objEmailAttachment->setFileExtensionId( $intExtensionId );
					}
					if( false == $objEmailAttachment->getIsInline() ) $objEmailAttachment->setTitle( $strAttachmentNewFileName );
					if( false == $objEmailAttachment->directInsert( $intCurrentUserId, $objDatabase ) ) {
						$this->addErrorMsgs( $objEmailAttachment->getErrorMsgs() );
						return false;
					}
				}
				if( !$boolDirectInsert && ( empty( $objEmailAttachment->getId() ) || !array_key_exists( $objEmailAttachment->getId(), $arrobjExistingEmailAttachments ) ) ) {
					$strAttachmentPath = $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName();
					if( false == CFileIo::fileExists( $strAttachmentPath ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, sprintf( 'Unable to locate file %s ( %s )', $strAttachmentPath, $objEmailAttachment->getFilePath() ) ) );
						trigger_error( 'Unable to locate file ' . $strAttachmentPath . ' (' . $objEmailAttachment->getFilePath() . ')', E_USER_WARNING );
						return false;
					}

					$objEmailAttachment->setEmailAttachmentFile( '', $objDatabase, $strAttachmentPath );

					if( false == $objEmailAttachment->getIsInline() ) $objEmailAttachment->setTitle( $strAttachmentNewFileName );

					if( false == $objEmailAttachment->insert( $intCurrentUserId, NULL, $objDatabase, $strAttachmentPath, $strAttachmentNewFileName, $boolUploadToNonBackupMounts ) ) {
						$this->addErrorMsgs( $objEmailAttachment->getErrorMsgs() );
						return false;
					}
				}

				$this->insertSystemEmailAttachment( $intCurrentUserId, $objEmailAttachment->getId(), $objDatabase );
				if( !$boolDirectInsert && true == $boolUploadToNonBackupMounts && 'production' == CONFIG_ENVIRONMENT ) {
					$this->archiveEmailAttachmentsToS3( $objEmailAttachment );
				}
			}
			$boolHasAttachments = true;
		}

		// This is backward compatible code to insert queue message set with attachment file path in email_attachments table, it will be removed soon.
		$arrstrAttachmentPaths = explode( ',', $this->getAttachmentPath() );
		if( true == valArr( $arrstrAttachmentPaths ) ) {

			foreach( $arrstrAttachmentPaths as $strAttachmentPath ) {

				if( 0 != strlen( $strAttachmentPath ) && false == is_null( $strAttachmentPath ) ) {

					$objEmailAttchment = new CEmailAttachment();
					$objEmailAttchment->setEmailAttachmentFile( '', $objDatabase, $strAttachmentPath );

					if( false == $objEmailAttchment->insert( $intCurrentUserId, NULL, $objDatabase, $strAttachmentPath ) ) {
						$this->addErrorMsgs( $objEmailAttchment->getErrorMsgs() );
						return false;
					}
					$this->insertSystemEmailAttachment( $intCurrentUserId, $objEmailAttchment->getId(), $objDatabase );
					if( true == $boolUploadToNonBackupMounts && 'production' == CONFIG_ENVIRONMENT ) {
						$this->archiveEmailAttachmentsToS3( $objEmailAttachment );
					}
				}
			}
			$boolHasAttachments = true;
		}

		if( $boolHasAttachments && $boolUploadToNonBackupMounts ) {
			$this->setAttachmentStorage( 'S3' );
			$this->setAllowDifferentialUpdate( true );
			$this->update( $intCurrentUserId, $objDatabase );
		}

		return true;
	}

	private function insertSystemEmailAttachment( $intCurrentUserId, $intEmailAttachmentId, $objDatabase ) {

		$objSystemEmailAttachment = new CSystemEmailAttachment();
		$objSystemEmailAttachment->setSystemEmailId( $this->getId() );
		$objSystemEmailAttachment->setEmailAttachmentId( $intEmailAttachmentId );

		if( false == $objSystemEmailAttachment->insert( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsgs( $objSystemEmailAttachment->getErrorMsgs() );
			return false;
		}

		return true;
	}

	private function insertSystemEmailRecipients( $objDatabase ) : bool {

		$arrobjSystemEmailRecipients = ( array ) $this->getSystemEmailRecipients();

		if( valArr( $arrobjSystemEmailRecipients ) ) {

			foreach( $arrobjSystemEmailRecipients as $objSystemEmailRecipient ) {

				if( false == valObj( $objSystemEmailRecipient, CSystemEmailRecipient::class ) || false == valStr( $objSystemEmailRecipient->getEmailAddress() ) ) {
					continue;
				}

				$objSystemEmailRecipient->setSystemEmailId( $this->getId() );
				if( false == $objSystemEmailRecipient->validate( VALIDATE_INSERT ) || false == $objSystemEmailRecipient->insert( $this->getCreatedBy(), $objDatabase ) ) {
					$this->addErrorMsgs( $objSystemEmailRecipient->getErrorMsgs() );
					return false;
				}
			}
		}
		return true;
	}

	private function validateSystemEmailWithAttachments( $objDatabase ) {

		// validate email_attachment objects
		$arrobjEmailAttachments = $this->getEmailAttachments();
		if( true == valArr( $arrobjEmailAttachments ) ) {
			$boolCloseDbConnection = false;
			if( !$objDatabase->getIsOpen() ) {
				$boolCloseDbConnection = true;
			}
			$arrstrFileExtension = \Psi\Eos\Email\CFileExtensions::createService()->fetchAllFileIdExtensions( $objDatabase );
			if( $boolCloseDbConnection && $objDatabase->getIsOpen() ) {
				$objDatabase->close();
			}

			foreach( ( array ) $arrobjEmailAttachments as $objEmailAttachment ) {
				if( true == is_null( $objEmailAttachment->getFileExtensionId() ) ) {
					$arrstrFileInfo   = pathinfo( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() );
					$arrmixFileExtension = $arrstrFileExtension[\Psi\CStringService::singleton()->strtolower( $arrstrFileInfo['extension'] )];
					if( true == valArr( $arrmixFileExtension ) ) {
						$objEmailAttachment->setFileExtensionId( $arrmixFileExtension['id'] );
					}
				}

				$objEmailAttachment->setFileSize( CFileIo::getFileSize( $objEmailAttachment->getFilePath() . '/' . $objEmailAttachment->getFileName() ) );

				if( false == $objEmailAttachment->validate( VALIDATE_INSERT ) ) {
					$this->addErrorMsgs( $objEmailAttachment->getErrorMsgs() );
					return false;
				}
				$this->m_intTotalEmailAttachmentsize = $this->m_intTotalEmailAttachmentsize + $objEmailAttachment->getFileSize();
			}
			return true;
		}
		return true;
	}

	private function getTotalEmailSize() {
		return  ( int ) $this->m_intHtmlFileSize + ( int ) $this->m_intTextFileSize + ( int ) $this->m_intTotalEmailAttachmentSize;
	}

	private function calculateSystemEmailContentSize() {

		if( false == is_null( $this->getHtmlContent() ) ) {
			$this->setHtmlFileSize( strlen( stripslashes( $this->getHtmlContent() ) ) );
		}
		if( false == is_null( $this->getTextContent() ) ) {
			$this->setTextFileSize( strlen( stripslashes( $this->getTextContent() ) ) );
		}

	}

	public function insertMaintenanceSystemEmailAttachments( $intCompanyUserId, $arrstrAttachmentFilePath, $objEmailDatabase = NULL ) {

		foreach( $arrstrAttachmentFilePath as $strAttachmentFilePath ) {

			if( true == file_exists( $strAttachmentFilePath ) ) {
				$objEmailAttachment = new CEmailAttachment();
				$objEmailAttachment->setDefaults();
				$objEmailAttachment->setEmailAttachmentFile( '', $objEmailDatabase, $strAttachmentFilePath );
				$objSystemEmailAttachment = new CSystemEmailAttachment();
				$objSystemEmailAttachment->setDefaults();
			}

			if( true == isset( $objEmailAttachment ) && true == valObj( $objEmailAttachment, 'CEmailAttachment' ) ) {
				if( false == $objEmailAttachment->insert( $intCompanyUserId, '', $objEmailDatabase, $strAttachmentFilePath ) ) {
					$objEmailDatabase->rollback();
					break;
				}
			}

			if( true == isset( $objEmailAttachment ) && true == valObj( $objEmailAttachment, 'CEmailAttachment' ) ) {
				$objInsertingSystemEmailAttachment = clone $objSystemEmailAttachment;
				$objInsertingSystemEmailAttachment->setSystemEmailId( $this->getId() );
				$objInsertingSystemEmailAttachment->setEmailAttachmentId( $objEmailAttachment->getId() );

				if( false == is_null( $objInsertingSystemEmailAttachment->getEmailAttachmentId() ) ) {
					if( false == $objInsertingSystemEmailAttachment->insert( $intCompanyUserId, $objEmailDatabase ) ) {
						$objEmailDatabase->rollback();
						break;
					}
				}
			}

		}

	}

}
?>