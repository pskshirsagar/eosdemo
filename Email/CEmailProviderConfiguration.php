<?php

class CEmailProviderConfiguration extends CBaseEmailProviderConfiguration {

	/**
	 * Set Functions
	 */

	public function setEncryptUserName( $strUsername ) {
		if( true == valStr( $strUsername ) ) {
			$this->setUsernameEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( trim( $strUsername ), CONFIG_SODIUM_KEY_SMTP_USERNAME ) );
		}
	}

	public function setEncryptPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_SMTP_PASSWORD ) );
		}
	}

	/**
	 * Get Functions
	 */

	public function getUsername() {
		if( false == valStr( $this->m_strUsernameEncrypted ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strUsernameEncrypted, CONFIG_SODIUM_KEY_SMTP_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_SMTP_USERNAME ] );
	}

	public function getPassword() {
		if( false == valStr( $this->m_strPasswordEncrypted ) ) {
			return NULL;
		}
		return \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->m_strPasswordEncrypted, CONFIG_SODIUM_KEY_SMTP_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_SMTP_PASSWORD ] );
	}

	public function valId() {
		return true;
	}

	public function valEmailServiceProviderId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valUsernameEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getUsernameEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host', __( 'User name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host', __( 'Password is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valServiceUri() {
		return true;
	}

	public function valHost() {
		$boolIsValid = true;

		if( true == is_null( $this->getHost() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'host', __( 'Server name is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPort() {
		$boolIsValid = true;

		if( true == is_null( $this->getPort() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port number is required.' ) ) );
		} elseif( 0 == $this->getPort() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', __( 'Port number should be valid numeric value. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valEnableSslTls() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valHost();
				$boolIsValid &= $this->valPort();
				$boolIsValid &= $this->valUsernameEncrypted();
				$boolIsValid &= $this->valPasswordEncrypted();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>