<?php

class CSystemEmailCategory extends CBaseSystemEmailCategory {

	const PAYMENT_NOTIFICATIONS				= 1;
	const RESIDENT_PORTAL_ACTIVITY			= 2;
	const GUEST_CARD_NOTIFICATIONS			= 3;
	const APPLICATION_NOTIFICATIONS			= 4;
	const RENEWAL_NOTIFICATIONS 			= 5;
	const RESIDENT_NOTIFICATIONS			= 6;
	const MAINTENANCE_NOTIFICATIONS			= 7;
	const INSURANCE_NOTIFICATIONS 			= 8;
	const RESIDENT_UTILITY_NOTIFICATIONS	= 9;
	const LATE_NOTICES_COLLECTIONS			= 10;
	const LEASING_CENTER_NOTIFICATIONS		= 11;
	const PARCEL_ALERT_NOTIFICATIONS		= 12;
	const OTHER_NOTIFICATIONS				= 15;
	const RESIDENT_VERIFY_NOTIFICATIONS		= 17;

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPsEmail() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>