<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailBlocks
 * Do not add any new functions to this class.
 */

class CSystemEmailBlocks extends CBaseSystemEmailBlocks {

	public static function fetchSystemEmailBlockByCidByCustomerIdByEmailBlockTypeIdByEmailAddress( $intCid, $intCustomerId, $intEmailBloackTypeId, $strEmailAddress, $objDatabase ) {

		$strSqlCondition = '';

		if( 0 < $intCid && true == is_numeric( $intCid ) ) {
			$strSqlCondition = ' AND cid = ' . ( int ) $intCid;
		}

		if( 0 < $intCustomerId && true == is_numeric( $intCustomerId ) ) {
			$strSqlCondition .= ' AND customer_id = ' . ( int ) $intCustomerId;
		}

		if( true == valStr( $strEmailAddress ) ) {
			$strSqlCondition .= ' AND email_address = \'' . addslashes( $strEmailAddress ) . '\' ';
		}

		$strSql = 'SELECT id FROM system_email_blocks WHERE email_block_type_id = ' . ( int ) $intEmailBloackTypeId . $strSqlCondition . ' ORDER BY id DESC LIMIT 1';

		return self::fetchSystemEmailBlock( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlockByCidByCustomerIdByEmailBlockTypeIdsByEmailAddress( $intCid, $intCustomerId, $arrintEmailBlockTypeIds, $strEmailAddress, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intCustomerId ) || false == valArr( $arrintEmailBlockTypeIds ) || false == valStr( $strEmailAddress ) ) {
			return [];
		}

		$strSql = '
					SELECT 
						id,
						email_block_type_id
					FROM
						system_email_blocks
					WHERE
						cid = ' . $intCid . '
						AND customer_id = ' . $intCustomerId . '
						AND email_address = \'' . addslashes( $strEmailAddress ) . '\'
						AND email_block_type_id IN ( ' . implode( ',', $arrintEmailBlockTypeIds ) . ' )
				  ';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCidByCustomerIdByEmailBlockTypeIdsByEmailAddress( $intCid, $intCustomerId, $arrintEmailBlockTypeIds, $strEmailAddress, $objDatabase ) {

		$strSqlCondition = '';

		if( 0 < $intCid && true == is_numeric( $intCid ) ) {
			$strSqlCondition = ' AND cid = ' . ( int ) $intCid;
		}

		if( 0 < $intCustomerId && true == is_numeric( $intCustomerId ) ) {
			$strSqlCondition .= ' AND customer_id = ' . ( int ) $intCustomerId;
		}

		if( true == valStr( $strEmailAddress ) ) {
			$strSqlCondition .= ' AND email_address = \'' . addslashes( $strEmailAddress ) . '\' ';
		}

		$strSql = 'SELECT * FROM system_email_blocks WHERE email_block_type_id IN ( ' . implode( ',', $arrintEmailBlockTypeIds ) . ' ) ' . $strSqlCondition . ' ORDER BY id DESC';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCustomerIdsByEmailBlockTypeIdByCid( $arrintCustomerIds, $intEmailBlockTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valId( $intEmailBlockTypeId ) || false == valId( $intCid ) ) {
			return false;
		}

		$strSql = '
					SELECT 
						id,
						customer_id,
						email_block_type_id
					FROM
						system_email_blocks
					WHERE
						cid = ' . $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND email_block_type_id = ' . $intEmailBlockTypeId . '
				  ';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByEmailAddressesByEmailBlockTypeIdByCid( $arrstrEmailAddresses, $intEmailBlockTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrstrEmailAddresses ) || false == valId( $intEmailBlockTypeId ) || false == valId( $intCid ) ) {
			return;
		}

		$arrstrEmailAddresses = array_map( 'addslashes', $arrstrEmailAddresses );

		$strSql = '
					SELECT 
						id,
						email_address,
						email_block_type_id
					FROM
						system_email_blocks
					WHERE
						cid = ' . $intCid . '
						AND email_address IN ( ' . sqlStrImplode( $arrstrEmailAddresses ) . ' )
						AND email_block_type_id = ' . $intEmailBlockTypeId . '
				  ';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByEmailBlockTypeIdsByEmailAddressByCid( $arrintEmailBloackTypeIds, $strEmailAddress, $intCid, $objDatabase ) {
		if( false == valArr( $arrintEmailBloackTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						* 
					FROM 
						system_email_blocks 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND email_block_type_id IN ( ' . implode( ',', $arrintEmailBloackTypeIds ) . ' )
						AND email_address = \'' . addslashes( $strEmailAddress ) . '\'
					ORDER BY id DESC';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCidByCustomerIdByEmailBlockTypeId( $arrintCids, $arrintCustomerIds, $intEmailBlockTypeId, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					 FROM system_email_blocks
					 WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )
						AND email_block_type_id = ' . ( int ) $intEmailBlockTypeId;

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCidByEmailBlockTypeId( $intCid, $intEmailBlockTypeId, $objDatabase ) {

			 $strSql = 'SELECT
							*
					 	FROM system_email_blocks
					 	WHERE
							cid = ' . ( int ) $intCid . '
							AND email_block_type_id = ' . ( int ) $intEmailBlockTypeId;

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlockByEmailBlockTypeIdByEmailAddressByCid( $intEmailBlockTypeId, $strEmailAddress, $intCid, $objDatabase ) {
		if( false == valStr( $strEmailAddress ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM system_email_blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND email_block_type_id = ' . ( int ) $intEmailBlockTypeId . '
						AND email_address = \'' . addslashes( $strEmailAddress ) . '\' ORDER BY id DESC LIMIT 1';

		return self::fetchSystemEmailBlock( $strSql, $objDatabase );
	}

	public static function fetchBlockEmailAddressesByCidByEmailBlockTypeId( $intCid, $intEmailBlockTypeId, $objDatabase ) {

		$strSql = 'SELECT
						email_address
					FROM
						system_email_blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND email_block_type_id = ' . ( int ) $intEmailBlockTypeId;

		$arrstrResultData = fetchData( $strSql, $objDatabase );

		$arrstrEmailAddresses = array();

		if( true == valArr( $arrstrResultData ) ) {
			foreach( $arrstrResultData as $arrstrResult ) {
				$arrstrEmailAddresses[$arrstrResult['email_address']] = $arrstrResult['email_address'];
			}
		}

		return $arrstrEmailAddresses;
	}

	public static function fetchConflictingSystemEmailBlockCountByCidByCustomerIdByEmailBlockTypeIdByEmailAddress( $intCid, $intCustomerId, $intEmailBloackTypeId, $strEmailAddress, $objDatabase ) {

		$strSqlCondition = '';

		if( 0 < $intCid && true == is_numeric( $intCid ) ) {
			$strSqlCondition = ' AND cid = ' . ( int ) $intCid;
		}

		if( 0 < $intCustomerId && true == is_numeric( $intCustomerId ) ) {
			$strSqlCondition .= ' AND customer_id = ' . ( int ) $intCustomerId;
		}

		$strWhereSql = ' WHERE email_block_type_id = ' . ( int ) $intEmailBloackTypeId . ' AND email_address = \'' . addslashes( $strEmailAddress ) . '\' ' . $strSqlCondition . ' GROUP BY id ORDER BY id DESC LIMIT 1';

		return self::fetchSystemEmailBlockCount( $strWhereSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByEmailBlockTypeIds( $arrintEmailBlocTypeIds, $objDatabase ) {

		if( false == valArr( $arrintEmailBlocTypeIds ) )	return NULL;

		$strSql = 'SELECT
						id,
						email_block_type_id,
						email_address
					FROM
						system_email_blocks
					WHERE
						email_block_type_id IN ( ' . implode( ',', $arrintEmailBlocTypeIds ) . ' )';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksEmailAddressesByBlockTypeIds( $strEmailBlockTypeIds, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT(lower(email_address)) AS email_address
					FROM
						system_email_blocks
					WHERE
						email_block_type_id IN (  ' . $strEmailBlockTypeIds . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleSystemEmailBlocksEmailAddressesByBlockTypeIdByCid( $intCid, $intEmailBlockTypeId, $objDatabase ) {

		if( false == valId( $intEmailBlockTypeId ) ) return NULL;

		$strWhereSql = ( true == valId( $intCid ) ) ? ' AND cid = ' . ( int ) $intCid : 'AND cid IS NULL';

		$strSql = 'SELECT
						DISTINCT(lower(email_address)) AS email_address
					FROM
						system_email_blocks
					WHERE
						email_block_type_id = ' . ( int ) $intEmailBlockTypeId . $strWhereSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailBlocksByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						system_email_blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		return self::fetchSystemEmailBlocks( $strSql, $objDatabase );
	}

	public static function fetchSimpleSystemEmailBlockTypeIdByCustomerIdsByCid( $arrintCustomerIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$arrintEmailBlockTypes = array();

		$strSql = 'SELECT
						email_block_type_id
					FROM
						system_email_blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		$arrmixEmailBlockTypes = fetchData( $strSql, $objDatabase );

		foreach( $arrmixEmailBlockTypes as  $arrmixEmailBlockType ) {
			array_push( $arrintEmailBlockTypes, $arrmixEmailBlockType['email_block_type_id'] );
		}

		return $arrintEmailBlockTypes;
	}

	public static function fetchSystemEmailBlockCountByCustomerIdsByEmailBlockTypeIdByCid( $arrintCustomerIds, $intEmailBlockTypeId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strWhereSql = ' WHERE cid = ' . ( int ) $intCid . ' AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' ) AND email_block_type_id = ' . ( int ) $intEmailBlockTypeId;

		return self::fetchSystemEmailBlockCount( $strWhereSql, $objDatabase );
	}

}
?>