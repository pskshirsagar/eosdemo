<?php

class CDomainBlock extends CBaseDomainBlock {

    public function valEmailBlockTypeId() {
        $boolIsValid = true;

        if( true == is_null( $this->getEmailBlockTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_block_type_id', 'Block type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDomain() {
        $boolIsValid = true;

        if( true == is_null( $this->getDomain() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'domain', 'Domain is required.' ) );
        } elseif( 1 != CValidation::checkDomain( $this->getDomain() ) ) {
	        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'domain', ' \'' . $this->getDomain() . '\' is not a valid domain.' ) );
	        	$boolIsValid &= false;
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmailBlockTypeId();
				$boolIsValid &= $this->valDomain();
				break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
           		break;
        }

        return $boolIsValid;
    }
}
?>