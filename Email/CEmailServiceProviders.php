<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CEmailServiceProviders
 * Do not add any new functions to this class.
 */

class CEmailServiceProviders extends CBaseEmailServiceProviders {

	public static function fetchPublishedEmailServiceProviders( $objDatabase ) {

		$strSql = 'SELECT * FROM email_service_providers WHERE is_published = 1 ORDER BY order_num';

		return self::fetchEmailServiceProviders( $strSql, $objDatabase );
	}

	public static function fetchEmailServiceProviderById( $intId, $objDatabase ) {
		$strSql = 'SELECT * FROM email_service_providers WHERE id = ' . ( int ) $intId;
		return self::fetchObject( $strSql, 'CEmailServiceProvider', $objDatabase );
	}

}
?>