<?php

class CEmailEvent extends CBaseEmailEvent {

	const SEND_GRID_USER_ID = 1;
	const MANDRILL_USER_ID = 2;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSystemEmailTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledEmailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailEventTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecipientEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponse() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAttemptCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatusCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			// default case
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*
	*/

	public function insertEmailEvent( $arrmixEmailEvent ) {

		$strSql  = '';
		$strSql .= 'SELECT * FROM func_email_events_insert_or_update( ' .
							$arrmixEmailEvent['cid'] . ', ' .
							$arrmixEmailEvent['systemEmailId'] . ', ' .
							$arrmixEmailEvent['systemEmailTypeId'] . ', ' .
							$arrmixEmailEvent['scheduledEmailId'] . ', ' .
							$arrmixEmailEvent['emailEventTypeId'] . ', ' .
							$arrmixEmailEvent['recipientEmailAddress'] . ', ' .
							$arrmixEmailEvent['response'] . ', ' .
							$arrmixEmailEvent['statusCode'] . ', ' .
							$arrmixEmailEvent['url'] . ', ' .
							$arrmixEmailEvent['emailServiceProviderId'] . ', ' .
							$arrmixEmailEvent['systemEmailRecipientId'] . '
				) AS result;';

		return $strSql;
	}

}
?>