<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailRecipientTypes
 * Do not add any new functions to this class.
 */

class CSystemEmailRecipientTypes extends CBaseSystemEmailRecipientTypes {

    public static function fetchSystemEmailRecipientTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CSystemEmailRecipientType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchSystemEmailRecipientType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CSystemEmailRecipientType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchAllSystemEmailRecipientTypes( $objEmailDatabase ) {
    	return parent::fetchSystemEmailRecipientTypes( 'SELECT sert.id, sert.name FROM system_email_recipient_types sert ORDER BY name', $objEmailDatabase );
    }

}
?>