<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailAttachments
 * Do not add any new functions to this class.
 */

class CSystemEmailAttachments extends CBaseSystemEmailAttachments {

	public static function fetchSystemEmailAttachmentsBySystemEmailIds( $arrintSystemEmailIds, $objEmailDatabase ) {

		$strSql = 'SELECT
						id,
						system_email_id
					FROM
						system_email_attachments
					WHERE
						system_email_id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )';

		return fetchData( $strSql, $objEmailDatabase );
	}

}
?>