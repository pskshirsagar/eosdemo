<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailPriorities
 * Do not add any new functions to this class.
 */

class CSystemEmailPriorities extends CBaseSystemEmailPriorities {

	public static function fetchSystemEmailPriorities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSystemEmailPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchSystemEmailPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSystemEmailPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllSystemEmailPriorities( $objDatabase ) {
		return self::fetchSystemEmailPriorities( 'SELECT * FROM system_email_priorities ORDER BY order_num', $objDatabase );
	}
}
?>