<?php

class CEmailTransmission extends CBaseEmailTransmission {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEmailId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEmailTransmissionId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valRecipientTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTemplateId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDeliveryTypeId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valSubject() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEmailFilterName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valScheduledEmailCreatedBy() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>