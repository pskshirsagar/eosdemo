<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmails
 * Do not add any new functions to this class.
 */

class CSystemEmails extends CBaseSystemEmails {

	public static function fetchPaginatedSystemEmailsBySearchParameters( $intPageNo, $intPageSize, $objSystemEmailsFilter, $objDatabase, $boolViewConfidentialEmails = false, $boolIsUserAdministrator = false ) {

		$intOffset				= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit				= $intPageSize;

		$strFromClause			= ' FROM
										system_emails se ';

		if( true == isset( $objSystemEmailsFilter ) && false == is_null( $objSystemEmailsFilter->getEmailEventTypeIds() ) && true == valArr( $objSystemEmailsFilter->getEmailEventTypeIds() ) ) {
			$strFromClause .= ' JOIN email_events ee ON ee.id = ( SELECT ee_latest.id FROM email_events ee_latest WHERE ee_latest.system_email_id = se.id ORDER BY ee_latest.id DESC LIMIT 1 )';
		}

		$strRecipientEmailAddress = $objSystemEmailsFilter->getRecipientEmailAddress();
		if( true == isset( $objSystemEmailsFilter ) && false == empty( $strRecipientEmailAddress ) ) {
			$strFromClause .= ' JOIN system_email_recipients ser ON( se. id = ser.system_email_id )';
		}

		$strOrderClause 		= '';

		$arrstrAndSearchParameters 	= [];

		if( true == valObj( $objSystemEmailsFilter, 'CAdminSystemEmailsFilter' ) ) {
			$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemEmailsFilter );
		}

		if( false == $boolViewConfidentialEmails ) {
			$strFromClause		.= ' JOIN system_email_types AS sety ON se.system_email_type_id = sety.id ';
			$arrstrAndSearchParameters[] = ' sety.is_confidential <> 1 ';
		}

		if( false == $boolIsUserAdministrator && true == $boolViewConfidentialEmails ) {
			$strFromClause		.= ' JOIN system_email_types AS sety ON se.system_email_type_id = sety.id ';
			$arrstrAndSearchParameters[] = ' sety.id <> ' . CSystemEmailType::PURCHASE_REQUEST_NOTIFICATION;
		}

		if( false == is_null( $objSystemEmailsFilter ) && false == is_null( $objSystemEmailsFilter->getOrderByField() ) && false == is_null( $objSystemEmailsFilter->getOrderByType() ) ) {

			$strOrder = ( 1 == $objSystemEmailsFilter->getOrderByType() ) ? ' ASC ' : ' DESC ';

			switch( $objSystemEmailsFilter->getOrderByField() ) {
				case NULL:
					break;

				case 'created_on':
					$strOrderClause			 = ' ORDER BY subq.created_on ' . $strOrder;
					break;

				case 'subject':
					$strOrderClause			 = ' ORDER BY subq.subject ' . $strOrder;
					break;

				case 'sent_on':
					$strOrderClause			 = ' ORDER BY subq.sent_on ' . $strOrder;
					break;

				default:
					$strOrderClause 		 = ' ORDER BY subq.' . $objSystemEmailsFilter->getOrderByField() . $strOrder;
			}

		}

		$strSqlSetMaxTime = 'SET STATEMENT_TIMEOUT = \'120s\'; ';
		executeSql( $strSqlSetMaxTime, $objDatabase );

		$strSql = 'SELECT
						subq.*,
						COUNT ( ser.id ) AS recipients_count
					FROM
						(
							SELECT
								se.id,
								se.system_email_type_id,
								se.system_email_priority_id,
								se.cid,
								se.property_id,
								se.email_service_provider_id,
								se.sent_on,
								se.created_on,
								se.subject,
								se.from_email_address,
								' . ( true == isset( $objSystemEmailsFilter ) && false == empty( $strRecipientEmailAddress ) ? 'ser.email_address' : 'se.to_email_address' ) . ' as to_email_address,
								se.accessed_on,
								se.queued_on,
								se.failed_on,
								se.mass_email_id,
								se.scheduled_send_datetime,
								' . ( true == isset( $objSystemEmailsFilter ) && false == empty( $strRecipientEmailAddress ) ? 'ser.id' : 'NULL::text' ) . ' as system_email_recipient_id
							' . $strFromClause
							. ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' WHERE ' : '' )
							. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '' )
							. ( ( 0 < strlen( $strOrderClause ) ) ? str_replace( 'subq', 'se', $strOrderClause ) : ' ORDER BY se.id DESC ' ) . '
							OFFSET ' . ( int ) $intOffset
							. ( true == isset( $intLimit ) ? ' LIMIT	' . ( int ) $intLimit : '' )
						. ' ) as subq
						LEFT JOIN system_email_recipients ser ON ser.system_email_id = subq.id
					GROUP BY
						subq.id,
						subq.system_email_type_id,
						subq.system_email_priority_id,
						subq.cid,
						subq.property_id,
						subq.email_service_provider_id,
						subq.sent_on,
						subq.created_on,
						subq.subject,
						subq.from_email_address,
						subq.to_email_address,
						subq.accessed_on,
						subq.queued_on,
						subq.failed_on,
						subq.mass_email_id,
						subq.scheduled_send_datetime,
						subq.system_email_recipient_id
					 ' . ( ( 0 < strlen( $strOrderClause ) ) ? $strOrderClause : ' ORDER BY subq.id DESC ' );

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsByIds( $arrintSystemEmailIds, $objDatabase, $objSystemEmailsFilter = NULL, $boolUseIsPaused = true ) {

		if ( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql	= 'SELECT
						se.*
					FROM
						system_emails se
						JOIN system_email_types AS set ON ( set.id = se.system_email_type_id )
					WHERE
						se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' ) ';

		if( true == $boolUseIsPaused ) {
			$strSql .= ' AND set.is_paused = 0 ';
		}

		if( false == is_null( $objSystemEmailsFilter ) && false == is_null( $objSystemEmailsFilter->getSortBy() ) ) {
			$strSortDirection = ( false == is_null( $objSystemEmailsFilter->getSortDirection() ) ) ? $objSystemEmailsFilter->getSortDirection() : 'ASC';

			$strOrderClause	= ' ORDER BY ' . addslashes( $objSystemEmailsFilter->getSortBy() ) . ' ' . $strSortDirection;
			$strSql .= $strOrderClause;
		}

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsByIdsByCid( $arrintSystemEmailIds, $intCid, $objDatabase, $objSystemEmailsFilter = NULL ) {

		if ( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql	= 'SELECT
						se.*
					FROM
						system_emails se
						JOIN system_email_types AS set ON ( set.id = se.system_email_type_id )
					WHERE
						se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND se.cid = ' . ( int ) $intCid . '
						AND set.is_paused = 0';

		if( false == is_null( $objSystemEmailsFilter ) && false == is_null( $objSystemEmailsFilter->getSortBy() ) ) {
			$strSortDirection = ( false == is_null( $objSystemEmailsFilter->getSortDirection() ) ) ? $objSystemEmailsFilter->getSortDirection() : 'ASC';

			$strOrderClause	= ' ORDER BY ' . addslashes( $objSystemEmailsFilter->getSortBy() ) . ' ' . $strSortDirection;
			$strSql .= $strOrderClause;
		}

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsSelectedFieldsByIds( $arrstrFieldNames, $arrintSystemEmailIds, $objDatabase ) {

		if ( false == valArr( $arrstrFieldNames ) || false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql	= 'SELECT
						' . implode( ',', $arrstrFieldNames ) . '
					FROM
						system_emails se
						JOIN system_email_types AS set ON ( set.id = se.system_email_type_id )
					WHERE
						se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND set.is_paused = 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailByCidByRecipientIdByRecipientTypeByScheduledEmailIdByTransmissionId( $intCid, $intRecipientId, $strRecipientType, $intScheduledEmailId, $intTransmissionId, $objDatabase ) {

		switch( $strRecipientType ) {
			case 'Applicant':
				$strSqlRecipient = ' AND applicant_id = ' . ( int ) $intRecipientId;
				break;

			case 'Customer':
				$strSqlRecipient = ' AND customer_id = ' . ( int ) $intRecipientId;
				break;

			case 'CompanyEmployee':
				$strSqlRecipient = ' AND company_employee_id = ' . ( int ) $intRecipientId;
				break;

			default:
				trigger_error( 'Unable to get valid user type. ', E_USER_WARNING );
				return false;
		}

		$strSql = 'SELECT * FROM system_emails WHERE scheduled_email_transmission_id = ' . ( int ) $intTransmissionId . ' AND cid = ' . ( int ) $intCid . '  AND scheduled_email_id=' . ( int ) $intScheduledEmailId . ' ' . $strSqlRecipient;
		$strSql .= ' ORDER BY id DESC LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsCountBySearchParameters( $objSystemEmailsFilter, $objDatabase, $boolViewConfidentialEmails = false, $boolIsUserAdministrator = false ) {

		$strSql = 'SELECT
						count(se.id)
					FROM system_emails se ';

		if( true == isset( $objSystemEmailsFilter ) && false == is_null( $objSystemEmailsFilter->getEmailEventTypeIds() ) && true == valArr( $objSystemEmailsFilter->getEmailEventTypeIds() ) ) {
			$strSql	.= ' JOIN email_events ee ON ee.id = (
															SELECT
																ee_latest.id
															FROM
																email_events ee_latest
															WHERE
																ee_latest.system_email_id = se.id
															ORDER BY
																ee_latest.id DESC
															LIMIT 1
														)';
		}
		$strRecipientEmailAddress = $objSystemEmailsFilter->getRecipientEmailAddress();
		if( true == isset( $objSystemEmailsFilter ) && false == empty( $strRecipientEmailAddress ) ) {
			$strSql .= ' JOIN system_email_recipients ser ON( se. id = ser.system_email_id )';
		}

		$strSql .= 'WHERE 1 = 1 ';

		$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemEmailsFilter );

		if( false == $boolViewConfidentialEmails ) {
			$arrstrAndSearchParameters[] = ' se.system_email_type_id NOT IN ( select id from system_email_types where is_confidential = 1 )  ';
		}

		if( false == $boolIsUserAdministrator && true == $boolViewConfidentialEmails ) {
			$arrstrAndSearchParameters[] = ' se.system_email_type_id <> ' . CSystemEmailType::PURCHASE_REQUEST_NOTIFICATION;
		}

		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchSystemEmailsStatisticsBySearchParameters( $objSystemEmailsFilter, $objDatabase ) {

		$strSql = 'SELECT
							se.cid,sets.name as system_email_type_name,
							count(se.accessed_on) as read_email_count,
							count(se.*) as total_email_count,
							count(se.failed_on) as bounce_email_count
					FROM
							system_emails as se,
							system_email_types as sets
					WHERE
							se.system_email_type_id = sets.id AND  cid IS NOT NULL ';

		$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemEmailsFilter );
		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$strSql .= ' GROUP BY system_email_type_id,sets.name,se.cid ORDER BY se.cid';

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function getSearchCriteria( $objSystemEmailsFilter ) {

		$arrstrWhereParameters = [];

		// Create SQL parameters.
		$arrintCids = $objSystemEmailsFilter->getCids();
		$arrintPropertyIds = $objSystemEmailsFilter->getPropertyIds();
		$arrintEmailServiceProviderIds = $objSystemEmailsFilter->getEmailServiceProviderIds();

		if( true == valArr( $objSystemEmailsFilter->getEmailServiceProviderIds() ) ) {
			$strServiceProviderCondition = ' se.email_service_provider_id IN ( ' . implode( ', ', $objSystemEmailsFilter->getEmailServiceProviderIds() ) . ' ) ';
		} else {
			$strServiceProviderCondition = 'se.email_service_provider_id IN ( ' . $objSystemEmailsFilter->getEmailServiceProviderIds() . ' ) ';
		}

		$strSubject 				= $objSystemEmailsFilter->getSubject();
		$strFromEmailAddress 		= $objSystemEmailsFilter->getFromEmailAddress();
		$strRecipientEmailAddress 	= $objSystemEmailsFilter->getRecipientEmailAddress();
		$strToEmailAddress 			= $objSystemEmailsFilter->getToEmailAddress();
		$intSystemEmailId 			= $objSystemEmailsFilter->getSystemEmailId();

		( false == isset( $objSystemEmailsFilter ) || true == empty( $arrintCids ) )																										?  false : array_push( $arrstrWhereParameters, 'se.cid IN ( ' . $objSystemEmailsFilter->getCids() . ' ) ' );
		( false == isset( $objSystemEmailsFilter ) || true == is_null( $arrintPropertyIds ) || false == valArr( $arrintPropertyIds ) )					                                                    ?  false : array_push( $arrstrWhereParameters, 'se.property_id IN ( ' . implode( ', ', $objSystemEmailsFilter->getPropertyIds() ) . ' ) ' );
		( false == isset( $objSystemEmailsFilter ) || true == is_null( $objSystemEmailsFilter->getSystemEmailPriorityIds() ) || false == valArr( $objSystemEmailsFilter->getSystemEmailPriorityIds() ) )?  false : array_push( $arrstrWhereParameters, 'se.system_email_priority_id IN ( ' . implode( ', ', $objSystemEmailsFilter->getSystemEmailPriorityIds() ) . ' ) ' );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $arrintEmailServiceProviderIds ) )																									?  false : array_push( $arrstrWhereParameters, $strServiceProviderCondition );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $strSubject ) )																													?  false : array_push( $arrstrWhereParameters, 'se.subject ILIKE E\'%' . addslashes( trim( $objSystemEmailsFilter->getSubject() ) ) . '%\'' );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $strFromEmailAddress ) )																											?  false : array_push( $arrstrWhereParameters, 'se.from_email_address ILIKE E\'%' . addslashes( trim( $objSystemEmailsFilter->getFromEmailAddress() ) ) . '%\'' );
		( false == isset( $objSystemEmailsFilter ) || true == is_null( $objSystemEmailsFilter->getViewPendingEmails() ) || 0 == $objSystemEmailsFilter->getViewPendingEmails() ) 						?  false : array_push( $arrstrWhereParameters, 'se.sent_on IS NULL' );
		( false == isset( $objSystemEmailsFilter ) || true == $objSystemEmailsFilter->getIsSuspended() ) 																								?  false : array_push( $arrstrWhereParameters, 'se.is_suspended = 0' );
		( false == isset( $objSystemEmailsFilter ) || true == is_null( $objSystemEmailsFilter->getEmailEventTypeIds() ) || false == valArr( $objSystemEmailsFilter->getEmailEventTypeIds() ) )			?  false : array_push( $arrstrWhereParameters, 'ee.email_event_type_id IN ( ' . implode( ', ', $objSystemEmailsFilter->getEmailEventTypeIds() ) . ' ) ' );
		( false == isset( $objSystemEmailsFilter ) || false == $objSystemEmailsFilter->getIsFailedOn() )																								?  false : array_push( $arrstrWhereParameters, 'se.failed_on IS NOT NULL AND se.sent_on IS NULL' );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $strRecipientEmailAddress ) )																										?  false : array_push( $arrstrWhereParameters, 'ser.email_address ILIKE E\'%' . addslashes( trim( $objSystemEmailsFilter->getRecipientEmailAddress() ) ) . '%\'' );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $strToEmailAddress ) )																												?  false : array_push( $arrstrWhereParameters, 'se.to_email_address ILIKE E\'%' . addslashes( trim( $objSystemEmailsFilter->getToEmailAddress() ) ) . '%\'' );
		( false == isset( $objSystemEmailsFilter ) || true == empty( $intSystemEmailId ) )																												?  false : array_push( $arrstrWhereParameters, 'se.id IN( ' . ( int ) $objSystemEmailsFilter->getSystemEmailId() . ' ) ' );

		if( false == is_null( $objSystemEmailsFilter->getIsCustomer() ) ) {
			( true == $objSystemEmailsFilter->getIsCustomer() )																																			?  array_push( $arrstrWhereParameters, 'se.customer_id IS NOT NULL ' ) : array_push( $arrstrWhereParameters, 'se.customer_id IS NULL ' );
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objSystemEmailsFilter->getSystemEmailTypeIds() ) )	$arrstrWhereParameters[] = 'se.system_email_type_id IN ( ' . $objSystemEmailsFilter->getSystemEmailTypeIds() . ' ) ';
		if( 0 < strlen( $objSystemEmailsFilter->getScheduledSendDate() ) ) 	$arrstrWhereParameters[] = 'se.scheduled_send_datetime = \'' . date( 'Y-m-d', strtotime( $objSystemEmailsFilter->getScheduledSendDate() ) ) . '\'';
		if( 0 < strlen( $objSystemEmailsFilter->getIsFailed() ) ) 			$arrstrWhereParameters[] = 'se.failed_on IS NOT NULL AND se.sent_on IS NULL ';

		$arrstrWhereParameters[] = ( 0 < strlen( $objSystemEmailsFilter->getFromDate() ) )	? 'se.created_on >= \'' . date( 'Y-m-d', strtotime( $objSystemEmailsFilter->getFromDate() ) ) . ' 00:00:00\'' : 'se.created_on >= \'' . date( 'Y-m-d H:i:s', strtotime( '-4 hours' ) ) . '\'';
		$arrstrWhereParameters[] = ( 0 < strlen( $objSystemEmailsFilter->getToDate() ) )	? 'se.created_on <= \'' . date( 'Y-m-d', strtotime( $objSystemEmailsFilter->getToDate() ) ) . ' 23:59:59\'' : 'se.created_on <= \'' . date( 'Y-m-d H:i:s' ) . '\'';

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchSystemEmailsByMassEmailId( $intMassEmailId, $objDatabase ) {

		if( true == empty( $intMassEmailId ) ) return NULL;

		$strSql = 'SELECT se.to_email_address,
						ser.email_address as recipient_email_address
					FROM
						system_emails se
					LEFT JOIN
						system_email_recipients ser ON se.id = ser.system_email_id
					WHERE
						se.mass_email_id = ' . ( int ) $intMassEmailId . '
					GROUP BY
						se.id,
						ser.email_address,
						se.to_email_address';

		return parent::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchFailedSystemEmailsByEmailAddressByEmailEventTypeId( $strEmailAddress, $intEmailEventTypeId, $objDatabase ) {

		if( false == valStr( $strEmailAddress ) ) return NULL;

		$strSql = ' SELECT
						se.id,
						max(ee.id) as event_id,
						ee.email_event_type_id,
						se.subject
					FROM
						system_emails se
					JOIN
						email_events ee ON ee.system_email_id = se.id
					WHERE
						se.is_suspended = 0 AND ee.email_event_type_id = ' . ( int ) $intEmailEventTypeId . ' AND ee.recipient_email_address = \'' . trim( $strEmailAddress ) . '\'
					GROUP BY
						se.id,
						se.subject,
						ee.email_event_type_id
					ORDER BY
						 se.id
					LIMIT 5 ';

		return parent::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchPaginatedFailedSystemEmailsByCidByPropertyIds( $intPageNo, $intPageSize, $objSystemEmailsFilter, $objDatabase, $boolCountOnly = false ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT count(se.id) FROM system_emails se WHERE 1 = 1 ';
		} else {
			$strSql = ' SELECT
						se.id,
						se.subject,
						se.property_id,
						se.system_email_type_id,
						se.to_email_address,
						ee.email_event_type_id,
						se.failed_on,
						max(ee.id) as email_event_id
					FROM
						system_emails se
					LEFT JOIN
						email_events ee ON ee.system_email_id = se.id
					WHERE 1 = 1';
		}

		$arrstrAndSearchParameters = self::getSearchCriteria( $objSystemEmailsFilter );

		if( true == valArr( $arrstrAndSearchParameters ) ) {
			$strSql	.= ' AND ' . implode( ' AND ', $arrstrAndSearchParameters );
		}

		if( false == $boolCountOnly ) {
			$strSql .= ' GROUP BY
						se.id,
						se.subject,
						se.property_id,
						se.system_email_type_id,
						se.to_email_address,
						ee.email_event_type_id,
						se.failed_on,
						ee.email_event_type_id
					ORDER BY
						 se.id DESC
					OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intLimit;

		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objDatabase );

			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

		} else {
			return self::fetchSystemEmails( $strSql, $objDatabase );
		}

	}

	public static function fetchFailedSystemEmailsCountByCidByToEmailAddressByHistoricalDays( $intCid, $strBillToEmaiAddress, $intHistoricalDays, $objDatabase ) {

		$strZendCacheKey				 = 'cachedResponseFailedSystemEmailsData' . $intCid . $strBillToEmaiAddress;
		$intFailedSystemEmailsCount		 = 0;

		$strSql = 'SELECT count(*) FROM system_emails WHERE failed_on IS NOT NULL AND cid = ' . ( int ) $intCid . ' AND to_email_address = \'' . $strBillToEmaiAddress . '\' AND failed_on > ( NOW() - INTERVAL \'' . ( int ) $intHistoricalDays . ' days\' )';

		// Get serialized array from Zend Shared Memory Cache
		$intFailedSystemEmailsCount = CCache::fetchObject( $strZendCacheKey );

		if( false === $intFailedSystemEmailsCount ) {

			$arrintResponse = fetchData( $strSql, $objDatabase );
			$intFailedSystemEmailsCount = ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

			// Storing data into cache for 1 month
			CCache::storeObject( $strZendCacheKey, $intFailedSystemEmailsCount, 2678400 );
		}

		return $intFailedSystemEmailsCount;
	}

	public static function fetchSystemEmailDetailsById( $intSystemEmailId, $objDatabase ) {

		if( true == empty( $intSystemEmailId ) ) return NULL;

		$strSql = ' SELECT
					 	se.*,
						ee.email_event_type_id as email_event_type_id,
						ee.response,
						ee.processed_on,
						ee.delivered_on,
						ee.clicked_on,
						ee.opened_on,
						ee.bounced_on,
						ee.spammed_on
					FROM
						system_emails se
					LEFT JOIN
						email_events ee ON ee.system_email_id = se.id
					WHERE
						se.id = ' . ( int ) $intSystemEmailId . '
					ORDER BY
						ee.id DESC
					LIMIT 1 ';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchFailedEmailCountForClientExecutive( $intEmpId, $objDatabase ) {

		if( true == empty( $intEmpId ) ) return NULL;

		$strSql = 'SELECT
						c.id AS cid
					FROM clients c
						JOIN ps_lead_details pld ON ( c.id = pld.cid )
					WHERE
						c.company_status_type_id IN( ' . CCompanyStatusType::PROSPECT . ' , ' . CCompanyStatusType::CLIENT . ' )
						AND pld.support_employee_id = ' . ( int ) $intEmpId . '
					ORDER BY c.company_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCountSystemEmailRecipientsByIds( $arrintSystemEmailIds, $objDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;
		$strSql = '	SELECT
						se.id,
						count(ser.id)
					FROM
						system_emails se
					LEFT JOIN
						system_email_recipients ser ON( se.id = ser.system_email_id )
					WHERE
						se.id IN( ' . implode( ',', $arrintSystemEmailIds ) . ' )
					GROUP BY
						se.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsBySystemEmailTypeIdByFromEmailAddressByFailedOnByEmailEventTypeId( $intSystemEmailTypeId, $strFromEmailAddress, $strFailedOn, $arrintEmailEventTypeIds, $objDatabase ) {

		if( false == is_numeric( $intSystemEmailTypeId ) || false == valStr( $strFromEmailAddress ) || false == valStr( $strFailedOn ) || false == valArr( $arrintEmailEventTypeIds ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						se.id,
						se.cid,
						se.account_id,
						se.subject,
						eet.name as event_name,
						ee.recipient_email_address
					FROM
						system_emails se
						JOIN email_events ee ON ee.system_email_id = se.id
						LEFT JOIN email_event_types eet ON ee.email_event_type_id = eet.id
					WHERE
						se.is_suspended = 0
						AND ee.email_event_type_id IN ( ' . implode( ',', $arrintEmailEventTypeIds ) . ' )
						AND se.system_email_type_id = ' . ( int ) $intSystemEmailTypeId . '
						AND se.from_email_address = \'' . $strFromEmailAddress . '\'
						AND date_trunc ( \'day\', se.failed_on ) = \'' . $strFailedOn . '\'::Date';

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSearchedSystemEmail( $arrmixFilteredExplodedSearch, $objDatabase, $boolViewConfidentialEmails = false ) {

		if( 1 != \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) || false == is_numeric( $arrmixFilteredExplodedSearch[0] ) ) return NULL;

		$strSystemEmailIdCondition	= 'se.id =' . $arrmixFilteredExplodedSearch[0];
		$strJoin = '';
		$strCondition = '';
		if( false == $boolViewConfidentialEmails ) {
			$strJoin	= ' JOIN system_email_types AS sety ON se.system_email_type_id = sety.id ';
			$strCondition = ' AND sety.is_confidential <> 1 ';
		}

		$strSql = ' SELECT
						se.id,
						se.subject,
						se.to_email_address,
						se.from_email_address,
						ee.email_event_type_id AS email_event_type_id
					FROM
						system_emails se
						LEFT JOIN email_events ee ON ( se.id = ee.system_email_id ) '
						. $strJoin .
					'WHERE
						' . $strSystemEmailIdCondition . $strCondition . '
					ORDER BY
						ee.id DESC
					LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailIdsBySystemEmailTypeIdByCustomerIdByCid( $intSystemEmailTypeId, $intCustomerId, $intCid, $objDatabase ) {

		$strSql = ' SELECT
						se.id as system_email_id
					FROM
						system_emails se
						JOIN system_email_recipients ser ON( se. id = ser.system_email_id AND se.cid = ser.cid )
					WHERE
						se.system_email_type_id = ' . ( int ) $intSystemEmailTypeId . '
						AND ser.reference_number = ' . ( int ) $intCustomerId . '
						AND se.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingSystemEmailsOrCountBySystemEmailTypeId( $intSystemEmailTypeId, $objDatabase, $boolIsReturnCount = false ) {

		if( false == is_numeric( $intSystemEmailTypeId ) ) return NULL;

		$strSelect = ( true == $boolIsReturnCount ) ? ' count( se.id ) ' : ' se.* ';

		$strSql = ' SELECT
						' . $strSelect . '
					FROM
						system_emails se
					WHERE
						se.system_email_type_id = ' . ( int ) $intSystemEmailTypeId . '
						AND se.lock_sequence_number IS NULL';

		if( true == $boolIsReturnCount ) {
			$arrintResponse = fetchData( $strSql, $objDatabase );

			if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

			return 0;
		}

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchStatusWiseSystemEmailsCount( $strDate, $objDatabase ) {

		if( false == valStr( $strDate ) ) return NULL;

		$strSql = 'SELECT
						sum( CASE WHEN se.sent_on IS NOT NULL THEN
						1 ELSE 0 END ) AS processed,
						sum( CASE WHEN se.lock_sequence_number IS NOT NULL AND se.sent_on IS NULL THEN
						1 ELSE 0 END ) AS queued,
						sum( CASE WHEN se.lock_sequence_number IS NULL THEN
						1 ELSE 0 END ) AS pending
					FROM
						system_emails se
					WHERE
						se.created_on > \'' . $strDate . '\'';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResponse[0] ) ) {
			return $arrmixResponse[0];
		} else {
			return [];
		}
	}

	public static function fetchSystemEmailTypeWiseSystemEmailsByCreatedOn( $strDate, $strStatusType, $objDatabase ) {

		if( false == valStr( $strDate ) || false == valStr( $strStatusType ) ) return NULL;

		if( 'processed' == $strStatusType ) {
			$strCondition = ' AND se.sent_on IS NOT NULL';
		} elseif( 'queued' == $strStatusType ) {
			$strCondition = ' AND se.lock_sequence_number IS NOT NULL AND se.sent_on IS NULL';
		} elseif( 'pending' == $strStatusType ) {
			$strCondition = ' AND se.lock_sequence_number IS NULL';
		}

		$strSql = 'SELECT
						se.system_email_type_id,
						st.name AS system_email_type_name,
						count( se.id ) AS count
					FROM
						system_emails se
						JOIN system_email_types st ON ( se.system_email_type_id = st.id )
					WHERE se.created_on >= \'' . $strDate . '\'' . $strCondition . '
					GROUP BY se.system_email_type_id,
							 st.name';

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		return rekeyArray( 'system_email_type_id', $arrmixResponse );
	}

	public static function fetchValidForgotPasswordSystemEmailBySystemEmailIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {

		if( false == valId( $intSystemEmailId ) ) {
			return false;
		}

		$strSql 		= 'SELECT id from system_emails where now() < ( sent_on + interval \'1 day\' ) and id = ' . ( int ) $intSystemEmailId . ' and cid =  ' . ( int ) $intCid;
		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrintResponse ) ) ? true : false;

	}

	public static function fetchMaintenanceEmailByEventIdByCid( $intEventId, $intCid, $objDatabase ) {

		if( false == valId( $intEventId ) ) {
			return false;
		}

		$strSql = ' SELECT
						se.*
					FROM
						system_emails se
					WHERE
						se.system_email_type_id = ' . CSystemEmailType::MAINTENANCE . '
						AND se.event_id = ' . ( int ) $intEventId . '
						AND se.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchInspectionEmailByEventIdByCid( $intEventId, $intCid, $objDatabase ) {
		if( false == valId( $intEventId ) ) {
			return false;
		}

		$strSql = ' SELECT
						se.*
					FROM
						system_emails se
					WHERE
						se.system_email_type_id = ' . CSystemEmailType::INSPECTION_MANAGER . '
						AND se.event_id = ' . ( int ) $intEventId . '
						AND se.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailEventBySubject( $strSubject, $objDatabase ) {

		$strSql = ' SELECT
						se.created_on,
						ee.delivered_on
					FROM
						system_emails se
					LEFT JOIN
						email_events ee
					ON
						ee.system_email_id = se.id
					WHERE
						se.id = ( SELECT id FROM system_emails WHERE subject = \'' . $strSubject . '\' AND system_email_type_id = ' . CSystemEmailType::STATISTICS_EMAIL . ' ORDER BY created_on DESC LIMIT 1)
					ORDER BY
						ee.id DESC
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailsBySystemEmailByIntervalBySystemEmailTypeId( $intInterval, $strLastSyncedOn, $intSystemEmailTypeId,$arrintBlockledCids, $strTargetedDate = NULL, $objDatabase ) {

		$strSelectClause = 'SELECT
								se.id,
								se.cid,
								se.scheduled_send_datetime,
								se.system_email_type_id';

		$strWhereClause = 'AND se.scheduled_send_datetime >= \'' . $strTargetedDate . '\'::DATE AND se.scheduled_send_datetime < \'' . $strTargetedDate . '\'::DATE + INTERVAL \'1 day\'';

		$strOrderBy = 'ORDER BY
							se.id';

		if( true == is_null( $strTargetedDate ) ) {
			$strSelectClause = 'SELECT
									min( se.scheduled_send_datetime ) :: DATE AS start_date,
									max( se.scheduled_send_datetime ) :: DATE AS end_date';

			$strWhereClause = 'AND se.scheduled_send_datetime < NOW() - INTERVAL \'' . ( int ) $intInterval . ' days\'';

			if( false == is_null( $strLastSyncedOn ) ) {
				$strWhereClause .= 'AND se.scheduled_send_datetime > \'' . $strLastSyncedOn . '\' :: DATE';
			}

			$strOrderBy = '';
		}

		$strSql = $strSelectClause . '
					FROM
						system_emails se
						JOIN system_email_types set ON( set.id = se.system_email_type_id )
					WHERE
						se.system_email_type_id = ' . ( int ) $intSystemEmailTypeId . '
						AND se.sent_on IS NOT NULL
						' . $strWhereClause . '
						AND se.failed_on IS NULL
						AND ( se.cid IS NULL OR se.cid NOT IN ( ' . implode( ',', $arrintBlockledCids ) . ' ) ) ' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchSystemEmailsBySystemEmailrecipientIdsByCid( $arrintSystemEmailRecipientIds,  $intCid, $objDatabase ) {

		if( false == valArr( $arrintSystemEmailRecipientIds ) ) return NULL;

		$strSql = 'SELECT
						se.*,
						ser.id AS system_email_recipient_id
					FROM
						system_emails se
						LEFT JOIN system_email_recipients ser ON se.id = ser.system_email_id
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND ser.id IN( ' . implode( ',', $arrintSystemEmailRecipientIds ) . ' ) ';

		return parent::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailContentByIds( $arrintSystemEmailIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						system_emails
					WHERE
						id IN( ' . implode( ',', $arrintSystemEmailIds ) . ' ) ';

		return parent::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailOpenCountsByScheduledEmailTransmissionIdByScheduledEmailIdByPropertyIdsByCid( $intScheduledEmailTransmissionId, $intScheduledEmailId, $arrintPropertyIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS open,
						date_trunc( \'day\', CASE WHEN ee.opened_on IS NULL THEN ee.clicked_on ELSE  ee.opened_on END ) AS opened_on
					FROM
						system_emails se
						JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid ) 
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
						AND ee.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND se.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL )
					GROUP BY 2
					ORDER BY opened_on';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData : NULL;
	}

	public static function fetchSystemEmailOpenCountsBySystemEmailIdsByCid( $arrintSystemEmailIds, $intCid, $objDatabase ) {

		if( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS open,
						date_trunc( \'day\', CASE WHEN ee.opened_on IS NULL THEN ee.clicked_on ELSE  ee.opened_on END ) AS opened_on
					FROM
						system_emails se
						JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid ) 
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND ( ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL )
					GROUP BY 2
					ORDER BY opened_on';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData : NULL;
	}

	public static function fetchSystemEmailTopClikedLinksDetailsByScheduledEmailTransmissionIdByScheduledEmailIdByPropertyIdByCid( $intScheduledEmailTransmissionId, $intScheduledEmailId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT 
						inner_sql.url,
						CASE
							WHEN POSITION( \'residentinsure\' IN inner_sql.url ) <> 0 THEN \'RESIDENT_INSURE_ENROLL_LINK\'
							WHEN POSITION( \'resident_portal\' IN inner_sql.url ) <> 0 THEN \'RESIDENT_PORTAL_ENROLL_LINK\'
							WHEN POSITION( \'amenities\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_AMENITIES_URL\'
							WHEN POSITION( \'application_authentication\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_APPLICATION_URL\'
							WHEN POSITION( \'map_and_direction\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_DIRECTIONS_URL\'
							WHEN POSITION( \'property_floorplans\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_FLOOR_PLANS_URL\'
							WHEN POSITION( \'/\' IN inner_sql.url ) = 0 THEN \'WEBSITE_HOME_URL\'
							WHEN POSITION( \'photos\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_PHOTOS_URL\'
							WHEN POSITION( \'lobby_display\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_CHECK_AVAILABILITY_URL\'
							WHEN POSITION( \'message_center_unsubscribe\' IN inner_sql.url ) <> 0 THEN \'UNSUBSCRIBE\'
							WHEN POSITION( \'message_center_scheduled_email\' IN inner_sql.url ) <> 0 THEN \'VIEW_IT_IN_YOUR_BROWSER\'
							WHEN POSITION( \'corporate_contact\' IN inner_sql.url ) <> 0 THEN \'WEBSITE_CONTACT_US_URL\'
							WHEN POSITION( \'property_info\' IN inner_sql.url ) <> 0 THEN \'CONTACT_US_GUEST_CARD\'
						ELSE 
							\'CUSTOM_LINK\'
						END AS title,
						inner_sql.click_count,
						inner_sql.email_event_ids
					FROM 
						( SELECT
								CASE
									WHEN POSITION( \'key\' IN eel.url ) <> 0 THEN TRIM( both \'&\' FROM SUBSTRING( eel.url FROM 0 for POSITION( \'key\' IN eel.url ) ) )
									WHEN POSITION( \'source\' IN eel.url ) <> 0 AND POSITION( \'residentinsure\' IN eel.url ) <> 0 THEN TRIM( both \'&\' from SUBSTRING( eel.url from 0 for POSITION( \'source\' IN eel.url ) ) )
								ELSE eel.url
								END AS url,
								SUM( eel.click_count ) AS click_count,
								array_to_string( array_agg( eel.email_event_id ), \', \') as email_event_ids
							FROM 
								system_emails se
								JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid )
								JOIN email_event_links eel ON ( eel.email_event_id = ee.id )
							WHERE 
								se.cid = ' . ( int ) $intCid . '
								AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
								AND ee.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
								AND se.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
								AND ee.clicked_on IS NOT NULL
							GROUP BY 1
							ORDER BY click_count DESC
						) AS inner_sql';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData : NULL;
	}

	public static function fetchSystemEmailLinkClickedCountByScheduledEmailTransmissionIdByScheduledEmailIdByPropertyIdsByCid( $intScheduledEmailTransmissionId, $intScheduledEmailId, $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						SUM( eel.click_count ) AS clicked
					FROM
						system_emails se
						JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid ) 
						JOIN email_event_links eel ON ( eel.email_event_id = ee.id )
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
						AND ee.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND se.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND ( ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL )
					GROUP BY 
						se.scheduled_email_transmission_id,
						se.subject';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData[0] : NULL;
	}

	public static function fetchSystemEmailDetailsByTransmissionIdByPropertyIdsByCid( $intScheduledEmailTransmissionId, $arrintPropertyIds, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						scheduled_email_transmission_id,
						subject,
						COUNT ( DISTINCT ( ser.id ) ) AS processed,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NOT NULL THEN ee.id END ) ) AS bounced,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NULL AND ee.deferred_on IS NOT NULL THEN ee.id END ) ) AS deferred,
						COUNT ( DISTINCT ( CASE WHEN ee.email_event_type_id = ' . CEmailEventType::DROPPED . ' THEN ee.id END ) ) AS dropped,
						COUNT ( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL AND ee.opened_on IS NULL AND ee.clicked_on IS NULL THEN ee.id END ) ) AS unopened,
						COUNT ( DISTINCT ( CASE WHEN ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS clicked,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL AND ee.opened_on IS NOT NULL AND ee.clicked_on IS NULL THEN ee.id END ) ) AS not_clicked,
						COUNT ( DISTINCT ( CASE WHEN ee.spammed_on IS NOT NULL THEN ee.id END ) ) AS spam
					FROM
						system_emails se
						LEFT JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid )
						LEFT JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.system_email_recipient_id = ser.id AND ee.cid = se.cid )
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND ee.scheduled_email_id IS NOT NULL
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmissionId . '
						AND se.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY 
						se.scheduled_email_transmission_id,
						se.subject';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData[0] : NULL;
	}

	public static function fetchSystemEmailDetailsByIdsByCid( $arrintSystemEmailIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintSystemEmailIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						subject,
						COUNT ( DISTINCT ( CASE WHEN ee.processed_on IS NOT NULL OR ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL OR ee.bounced_on IS NOT NULL THEN ee.id END ) ) AS processed,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL THEN ee.id END ) ) AS delivered,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NOT NULL THEN ee.id END ) ) AS bounced,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NULL AND ee.bounced_on IS NULL AND ee.deferred_on IS NOT NULL THEN ee.id END ) ) AS deferred,
						COUNT ( DISTINCT ( CASE WHEN ee.email_event_type_id = ' . CEmailEventType::DROPPED . ' THEN ee.id END ) ) AS dropped,
						COUNT ( DISTINCT ( CASE WHEN ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS opened,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL AND ee.opened_on IS NULL AND ee.clicked_on IS NULL THEN ee.id END ) ) AS unopened,
						COUNT ( DISTINCT ( CASE WHEN ee.clicked_on IS NOT NULL THEN ee.id END ) ) AS clicked,
						COUNT ( DISTINCT ( CASE WHEN ee.delivered_on IS NOT NULL AND ee.opened_on IS NOT NULL AND ee.clicked_on IS NULL THEN ee.id END ) ) AS not_clicked,
						COUNT ( DISTINCT ( CASE WHEN ee.spammed_on IS NOT NULL THEN ee.id END ) ) AS spam
					FROM
						system_emails se
						LEFT JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid )
					WHERE
						se.cid = ' . ( int ) $intCid . '
						AND se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
					GROUP BY 
						se.subject';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData[0] : NULL;
	}

	public static function fetchPaginatedSystemEmailClickedRecipientByEmailEventIdsByScheduledEmailTransmissionIdByScheduledEmailIdByCid( $intPageNo, $intPageSize, $strEmailEventIds, $intScheduledEmailTransmission, $intScheduledEmailId, $intCid, $objDatabase, $strRecipientDetails = '' ) {
		if( false == valStr( $strEmailEventIds ) ) return NULL;

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;
		$strWhereCondition = NULL;

		if( true == valStr( $strRecipientDetails ) ) {
			$strToEmailAddress = $strRecipientDetails;
			$strRecipientDetails = CEncryption::encryptText( $strRecipientDetails, CConfig::get( 'key_sensitive_data_encryption' ) );
			$strWhereCondition = 'AND ( sel.name_first_encrypted ilike \'%' . $strRecipientDetails . '%\' OR sel.name_last_encrypted ilike \'%' . $strRecipientDetails . '%\' OR ser.email_address ilike \'%' . $strToEmailAddress . '%\' )';
		}

		$strSql = 'SELECT 
						ser.email_address AS to_email_address,
						sel.name_first_encrypted,
						sel.name_last_encrypted,
						sel.property_name,
						sel.primary_phone_number_encrypted,
						sel.primary_phone_number_type_encrypted,
						sel.building_name,
						sel.unit_number,
						sel.recipient_created_on,
						ser.reference_number,
						se.id AS system_email_id,
						ser.recipient_type_id
					FROM
						system_emails se
						LEFT JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid )
						JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid AND ser.id = ee.system_email_recipient_id )
						JOIN scheduled_email_logs sel ON ( sel.system_email_id = se.id AND sel.system_email_recipient_id = ser.id AND ser.cid = se.cid )
					WHERE
						ee.id IN ( ' . $strEmailEventIds . ' )
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmission . '
						AND se.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND ee.cid = ' . ( int ) $intCid
						. $strWhereCondition . ' 
					LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $intKey => $arrmixResponse ) {
				$arrmixResponseData[$intKey]['name_first'] = CEncryption::decryptText( $arrmixResponse['name_first_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['name_last'] = CEncryption::decryptText( $arrmixResponse['name_last_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['primary_phone_number'] = CEncryption::decryptText( $arrmixResponse['primary_phone_number_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['primary_phone_number_type'] = CEncryption::decryptText( $arrmixResponse['primary_phone_number_type_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
			}
		}

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData : NULL;
	}

	public static function fetchSystemEmailClickedRecipientCountByEmailEventIdsByScheduledEmailTransmissionIdByScheduledEmailIdByCid( $strEmailEventIds, $intScheduledEmailTransmission, $intScheduledEmailId, $intCid, $objDatabase, $strRecipientDetails = '' ) {
		if( false == valStr( $strEmailEventIds ) ) return NULL;

		if( true == valStr( $strRecipientDetails ) ) {
			$strToEmailAddress = $strRecipientDetails;
			$strRecipientDetails = CEncryption::encryptText( $strRecipientDetails, CConfig::get( 'key_sensitive_data_encryption' ) );
			$strWhereCondition = 'AND ( sel.name_first_encrypted like \'%' . $strRecipientDetails . '%\' OR sel.name_last_encrypted like \'%' . $strRecipientDetails . '%\' OR ser.email_address like \'%' . $strToEmailAddress . '%\' )';
		}

		$strSql = 'SELECT
						COUNT( sel.id ) as clicked_count
					FROM
						system_emails se
						LEFT JOIN email_events ee ON ( ee.system_email_id = se.id AND ee.cid = se.cid )
						JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND ser.cid = se.cid AND ser.id = ee.system_email_recipient_id )
						JOIN scheduled_email_logs sel ON ( sel.system_email_id = se.id AND sel.system_email_recipient_id = ser.id AND ser.cid = se.cid )
					WHERE
						ee.id IN ( ' . $strEmailEventIds . ' )
						AND se.scheduled_email_transmission_id = ' . ( int ) $intScheduledEmailTransmission . '
						AND se.scheduled_email_id = ' . ( int ) $intScheduledEmailId . '
						AND ee.cid = ' . ( int ) $intCid
						. $strWhereCondition;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return ( true == valArr( $arrmixResponseData ) ) ? $arrmixResponseData[0]['clicked_count'] : NULL;
	}

	public static function fetchSentEmailRecipientsListByScheduledEmailsReportFilters( $arrmixScheduledEmailsReportFilters, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {

		$arrmixResponseData = [];
		$arrintEventType       = [];

		$arrintEventType['event_type_id']                = $arrmixScheduledEmailsReportFilters['event_type_id'];
		$arrintEventType['email_event_type_not_clicked'] = $arrmixScheduledEmailsReportFilters['email_event_type_not_clicked'];
		$arrintEventType['email_event_type_unopened']    = $arrmixScheduledEmailsReportFilters['email_event_type_unopened'];

		$strQuickSearch = $arrmixScheduledEmailsReportFilters['recipient_list_search_text'];

		$strEmailEventSql = self::buildFilterSqlByEmailEventType( $arrintEventType );

		if( strtotime( $arrmixScheduledEmailsReportFilters['min_sent_on_date'] ) <= strtotime( $arrmixScheduledEmailsReportFilters['scheduled_email_sent_on'] ) ) {
			$strInnerSql = 'SELECT
								ser.email_address AS to_email_address,
								se.id system_email_id,
								ser.id system_email_recipient_id,
								et.cid AS cid,
								se.property_id,
								et.id email_transmission_id,
								ser.reference_number,
								ser.recipient_type_id,
								ser.details->>\'customer_type_id\' AS customer_type_id,
								ser.details->>\'organization_id\' AS organization_id
							FROM
								email_transmissions et
								JOIN system_emails se ON ( et.scheduled_email_transmission_id = se.scheduled_email_transmission_id AND et.cid = se.cid )
								JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND se.cid = ser.cid )
							WHERE
								et.cid = ' . ( int ) $arrmixScheduledEmailsReportFilters['cid'] . '
								AND et.scheduled_email_transmission_id = ' . ( int ) $arrmixScheduledEmailsReportFilters['scheduled_email_transmissin_id'] . '
								AND se.created_on >= \'03/06/2014\'
								AND se.property_id IN ( ' . implode( ',', $arrmixScheduledEmailsReportFilters['property_ids'] ) . ' )
								AND EXISTS (
									SELECT 1
									FROM email_events ee
									WHERE ee.cid = et.cid
										AND ee.system_email_id = se.id
										AND ee.system_email_recipient_id = ser.id
										' . $strEmailEventSql . '
								)';
		}

		if( strtotime( $arrmixScheduledEmailsReportFilters['min_sent_on_date'] ) <= strtotime( $arrmixScheduledEmailsReportFilters['scheduled_email_sent_on'] ) ) {
			$strSql = 'SELECT
							inner_sql.to_email_address,
							sel.name_first_encrypted,
							sel.name_last_encrypted,
							sel.property_name,
							sel.primary_phone_number_encrypted,
							sel.primary_phone_number_type_encrypted,
							sel.building_name,
							sel.unit_number,
							sel.recipient_created_on,
							inner_sql.reference_number,
							inner_sql.system_email_id,
							inner_sql.recipient_type_id,
							inner_sql.customer_type_id,
							inner_sql.organization_id
						FROM scheduled_email_logs sel
						JOIN ( ' . $strInnerSql . ' ) AS inner_sql
						ON (
							inner_sql.system_email_id = sel.system_email_id
							AND inner_sql.system_email_recipient_id = sel.system_email_recipient_id
							AND inner_sql.cid = sel.cid
						)';
		} else {
			$strSql = 'SELECT
							inner_sql.to_email_address,
							sel.name_first_encrypted,
							sel.name_last_encrypted,
							sel.property_name,
							sel.primary_phone_number_encrypted,
							sel.primary_phone_number_type_encrypted,
							sel.building_name,
							sel.unit_number,
							sel.recipient_created_on
						FROM scheduled_email_logs sel
						JOIN ( ' . $strInnerSql . ' ) AS inner_sql
						ON (
							inner_sql.system_email_id = sel.system_email_id
							AND inner_sql.cid = sel.cid
						)';
		}
		if( true == valStr( $strQuickSearch ) ) {
			$strEncryptedQuickSearch = CEncryption::encryptText( $strQuickSearch, CConfig::get( 'key_sensitive_data_encryption' ) );
			$strSql .= ' WHERE sel.name_first_encrypted ILIKE(\'%' . $strEncryptedQuickSearch . '%\') OR sel.name_last_encrypted ILIKE(\'%' . $strEncryptedQuickSearch . '%\') OR inner_sql.to_email_address ILIKE(\'%' . $strQuickSearch . '%\')';
		}

		$intOffset 		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $intKey => $arrmixResponse ) {
				$arrmixResponseData[$intKey]['name_first'] = CEncryption::decryptText( $arrmixResponse['name_first_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['name_last'] = CEncryption::decryptText( $arrmixResponse['name_last_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['primary_phone_number'] = CEncryption::decryptText( $arrmixResponse['primary_phone_number_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
				$arrmixResponseData[$intKey]['primary_phone_number_type'] = CEncryption::decryptText( $arrmixResponse['primary_phone_number_type_encrypted'], CConfig::get( 'key_sensitive_data_encryption' ) );
			}
		}

		return $arrmixResponseData;
	}

	public static function fetchSentEmailRecipientsCountBycheduledEmailsReportFilters( $arrmixScheduledEmailsReportFilters, $objDatabase ) {
		$arrmixResponseData = [];
		$arrintEventType    = [];

		$arrintEventType['event_type_id']                = $arrmixScheduledEmailsReportFilters['event_type_id'];
		$arrintEventType['email_event_type_not_clicked'] = $arrmixScheduledEmailsReportFilters['email_event_type_not_clicked'];
		$arrintEventType['email_event_type_unopened']    = $arrmixScheduledEmailsReportFilters['email_event_type_unopened'];

		$strQuickSearch = $arrmixScheduledEmailsReportFilters['recipient_list_search_text'];

		$strEmailEventSql = self::buildFilterSqlByEmailEventType( $arrintEventType );

		$strInnerSql = 'SELECT
							ser.email_address AS to_email_address,
							se.id system_email_id,
							ser.id system_email_recipient_id,
							et.cid AS cid,
							se.property_id,
							et.id email_transmission_id
						FROM
							email_transmissions et
							JOIN system_emails se ON ( et.scheduled_email_transmission_id = se.scheduled_email_transmission_id AND et.cid = se.cid )
							JOIN system_email_recipients ser ON ( ser.system_email_id = se.id AND se.cid = ser.cid )
						WHERE
							et.cid = ' . ( int ) $arrmixScheduledEmailsReportFilters['cid'] . '
							AND et.scheduled_email_transmission_id = ' . ( int ) $arrmixScheduledEmailsReportFilters['scheduled_email_transmissin_id'] . '
							AND se.property_id IN ( ' . implode( ',', $arrmixScheduledEmailsReportFilters['property_ids'] ) . ' )
							AND EXISTS (
								SELECT 1
								FROM email_events ee
								WHERE ee.cid = et.cid
									AND ee.system_email_id = se.id
									AND ee.system_email_recipient_id = ser.id
									' . $strEmailEventSql . '
							)';

		if( strtotime( $arrmixScheduledEmailReportConstant['min_sent_on_date'] ) <= strtotime( $strScheduledEmailSentOn ) ) {
			$strSql = 'SELECT
							inner_sql.to_email_address,
							sel.name_first_encrypted,
							sel.name_last_encrypted,
							sel.property_name,
							sel.primary_phone_number_encrypted,
							sel.primary_phone_number_type_encrypted,
							sel.building_name,
							sel.unit_number,
							sel.recipient_created_on
						FROM scheduled_email_logs sel
						JOIN ( ' . $strInnerSql . ' ) AS inner_sql
						ON (
							inner_sql.system_email_id = sel.system_email_id
							AND inner_sql.system_email_recipient_id = sel.system_email_recipient_id
							AND inner_sql.cid = sel.cid
						)';
		} else {
			$strSql = 'SELECT
							inner_sql.to_email_address,
							sel.name_first_encrypted,
							sel.name_last_encrypted,
							sel.property_name,
							sel.primary_phone_number_encrypted,
							sel.primary_phone_number_type_encrypted,
							sel.building_name,
							sel.unit_number,
							sel.recipient_created_on
						FROM scheduled_email_logs sel
						JOIN ( ' . $strInnerSql . ' ) AS inner_sql
						ON (
							inner_sql.system_email_id = sel.system_email_id
							AND inner_sql.cid = sel.cid
						)';
		}

		if( true == valStr( $strQuickSearch ) ) {
			$strEncryptedQuickSearch = CEncryption::encryptText( $strQuickSearch, CConfig::get( 'key_sensitive_data_encryption' ) );
			$strSql .= ' WHERE sel.name_first_encrypted ILIKE(\'%' . $strEncryptedQuickSearch . '%\') OR sel.name_last_encrypted ILIKE(\'%' . $strEncryptedQuickSearch . '%\') OR inner_sql.to_email_address ILIKE(\'%' . $strQuickSearch . '%\')';
		}

		$arrmixResponseData = fetchData( $strSql, $objDatabase );
		return \Psi\Libraries\UtilFunctions\count( $arrmixResponseData );
	}

	public static function buildFilterSqlByEmailEventType( $arrintEventType ) {
		$strSql = '';
		$intEventType = $arrintEventType['event_type_id'];
		$strRecipientFilter = ' AND ( ser.id IS NOT NULL )';

		if( true == array_key_exists( 'is_consider_system_email_recipients', $arrintEventType ) && false == $arrintEventType['is_consider_system_email_recipients'] ) {
			$strRecipientFilter = ' AND ( ee.processed_on IS NOT NULL OR ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL OR ee.bounced_on IS NOT NULL )';
		}

		switch( $intEventType ) {
			case CEmailEventType::PROCESSED:
				$strSql .= $strRecipientFilter;
				break;

			case CEmailEventType::DROPPED:
				$strSql .= ' AND ee.email_event_type_id = ' . CEmailEventType::DROPPED;
				break;

			case CEmailEventType::DEFERRED:
				$strSql .= ' AND ( ee.delivered_on IS NULL AND ee.bounced_on IS NULL AND ee.deferred_on IS NOT NULL )';
				break;

			case CEmailEventType::DELIVERED:
				$strSql .= ' AND ( ee.delivered_on IS NOT NULL OR ee.opened_on IS NOT NULL ) ';
				break;

			case CEmailEventType::SPAM_REPORTS:
				$strSql .= ' AND ee.spammed_on IS NOT NULL';
				break;

			case CEmailEventType::CLICK:
				$strSql .= ' AND ee.clicked_on IS NOT NULL';
				break;

			case $arrintEventType['email_event_type_not_clicked']:
				$strSql .= ' AND ee.delivered_on IS NOT NULL AND ee.opened_on IS NOT NULL AND ee.clicked_on IS NULL';
				break;

			case CEmailEventType::OPEN:
				$strSql .= ' AND ( ee.opened_on IS NOT NULL OR ee.clicked_on IS NOT NULL )';
				break;

			case $arrintEventType['email_event_type_unopened']:
				$strSql .= ' AND ee.delivered_on IS NOT NULL AND ee.opened_on IS NULL AND ee.clicked_on IS NULL';
				break;

			case CEmailEventType::BOUNCE:
				$strSql .= ' AND ( ee.bounced_on IS NOT NULL AND ee.delivered_on IS NULL )';
				break;

			default:
				$strSql .= $strRecipientFilter;
		}
		return $strSql;
	}

	public static function fetchSystemEmailsInfoByIdsByCid( $arrintSystemEmailIds, $intCid, $objDatabase ) {

		if ( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql	= 'SELECT
						se.id,
						se.failed_on,
						se.sent_on
					FROM
						system_emails se
					WHERE
						se.id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND se.cid = ' . ( int ) $intCid;

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchMassEmailEventDetailsByMassEmailIds( $arrintMassEmailIds, $objDatabase ) {

		if( false == valArr( $arrintMassEmailIds ) ) {
			return NULL;
		}

		$strSql = '
					WITH mass_emails AS(
						SELECT
							UNNEST( ARRAY [ ' . implode( ', ', $arrintMassEmailIds ) . ' ] )::INTEGER mass_email_id
					)
					SELECT
						se.mass_email_id,
						ee.email_event_type_id,
						COUNT( 1 ) AS count
					FROM
						system_emails se
						JOIN mass_emails me ON( se.mass_email_id = me.mass_email_id )
						JOIN email_events ee ON( ee.system_email_id = se.id )
					WHERE
						ee.email_event_type_id IN(' . implode( ', ', array_keys( CEmailEventType::$c_arrstrMassEmailEventTypes ) ) . ')
					GROUP BY
						se.mass_email_id,
						ee.email_event_type_id
				';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailInfoByToEmailAddressBySubjectByHistoricalDays( $strToEmailAddress, $strSubject, $intHistoricalDays, $objDatabase ) {

		$arrintEmailRelaySystemEmailTypes = [ CSystemEmailType::APPLICATION_CONTACT_SUBMISSIONS, CSystemEmailType::MESSAGE_CENTER_EMAIL, CSystemEmailType::EVENT_SCHEDULER_EMAIL ];

		$strSql	= 'WITH cte_results AS(
						SELECT
							id,
							reply_to_email_address,
							to_email_address,
							subject
						FROM
							system_emails
						WHERE
							system_email_type_id IN ( ' . implode( ',', $arrintEmailRelaySystemEmailTypes ) . ' )
							AND sent_on > ( NOW() - INTERVAL \'' . ( int ) $intHistoricalDays . 'days\' )
					)
					SELECT
						reply_to_email_address
					FROM
						cte_results
					WHERE
						to_email_address ILIKE E\'' . addslashes( trim( $strToEmailAddress ) ) . '\'
						AND subject ILIKE E\'%' . addslashes( trim( $strSubject ) ) . '%\'
					ORDER BY id DESC
						LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

    public static function fetchSystemEmailDetailsByCidByPropertyIdByApplicantIdByScreeningReportRequestId( $intCid, $intPropertyId, $intApplicantId, $intScreeningReportRequestId, $objDatabase ) {

	    $strSql = ' SELECT
					 	se.*,
						ee.email_event_type_id as email_event_type_id,
						ee.response,
						ee.processed_on,
						ee.delivered_on,
						ee.clicked_on,
						ee.opened_on,
						ee.bounced_on,
						ee.spammed_on
					FROM
						system_emails se
					LEFT JOIN
						email_events ee ON ee.system_email_id = se.id
					WHERE
						se.cid = ' . ( int ) $intCid . '
                        AND se.property_id = ' . ( int ) $intPropertyId . '
                        AND se.applicant_id = ' . ( int ) $intApplicantId . '
                        AND se.details @> \'{"screening_report_request_id": ' . ( int ) $intScreeningReportRequestId . '}\'
					ORDER BY
						se.id ASC
					LIMIT 2 ';

	    return self::fetchSystemEmails( $strSql, $objDatabase );
    }

    public static function fetchSystemEmailIdsByCidByPropertyIdByApplicantIdByScreeningReportRequestId( $intCid, $intPropertyId, $intApplicantId, $intScreeningReportRequestId, $objDatabase ) {

        $strSql = ' SELECT
					 	se.id
					FROM
						system_emails se
					WHERE
						se.cid = ' . ( int ) $intCid . '
                        AND se.property_id = ' . ( int ) $intPropertyId . '
                        AND se.applicant_id = ' . ( int ) $intApplicantId . '
                        AND se.details @> \'{"screening_report_request_id": ' . ( int ) $intScreeningReportRequestId . '}\'
					ORDER BY
						se.id DESC
					LIMIT 2 ';

        return fetchData( $strSql, $objDatabase );
    }

	public static function fetchSentEmailsCountByScheduledEmailsReportFilters( $arrmixScheduledEmailsReportFilters, $objDatabase, $boolIsConsiderSystemEmailRecipients ) {
		$arrmixResponseData = [];
		$arrintEventType    = [];

		$arrintEventType['event_type_id']                = $arrmixScheduledEmailsReportFilters['event_type_id'];
		$arrintEventType['email_event_type_not_clicked'] = $arrmixScheduledEmailsReportFilters['email_event_type_not_clicked'];
		$arrintEventType['email_event_type_unopened']    = $arrmixScheduledEmailsReportFilters['email_event_type_unopened'];
		$arrintEventType['is_consider_system_email_recipients']    = $boolIsConsiderSystemEmailRecipients;

		$strEmailEventSql = self::buildFilterSqlByEmailEventType( $arrintEventType );

		$strSql = 'SELECT
							DISTINCT se.to_email_address,
							se.customer_id,
							se.applicant_id,
							se.cid,
							se.property_id
						FROM
							system_emails se
						WHERE
							se.cid = ' . ( int ) $arrmixScheduledEmailsReportFilters['cid'] . '
							AND se.id IN ( ' . $arrmixScheduledEmailsReportFilters['system_email_ids'] . ')
							AND EXISTS (
								SELECT 1
								FROM email_events ee
								WHERE ee.cid = se.cid
									AND ee.system_email_id = se.id
									' . $strEmailEventSql . '
							)';

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		return \Psi\Libraries\UtilFunctions\count( $arrmixResponseData );
	}

	public static function fetchSentEmailsByScheduledEmailsReportFilters( $arrmixScheduledEmailsReportFilters, $objDatabase, $boolIsConsiderSystemEmailRecipients = false ) {
		$arrintEventType    = [];

		$arrintEventType['event_type_id']                = $arrmixScheduledEmailsReportFilters['event_type_id'];
		$arrintEventType['email_event_type_not_clicked'] = $arrmixScheduledEmailsReportFilters['email_event_type_not_clicked'];
		$arrintEventType['email_event_type_unopened']    = $arrmixScheduledEmailsReportFilters['email_event_type_unopened'];
		$arrintEventType['is_consider_system_email_recipients']    = $boolIsConsiderSystemEmailRecipients;

		$strEmailEventSql = self::buildFilterSqlByEmailEventType( $arrintEventType );

		$strSql = 'SELECT
							se.to_email_address,
							se.id,
							se.customer_id,
							se.applicant_id,
							se.cid,
							se.property_id
						FROM
							system_emails se
						WHERE
							se.cid = ' . ( int ) $arrmixScheduledEmailsReportFilters['cid'] . '
							AND se.id IN ( ' . $arrmixScheduledEmailsReportFilters['system_email_ids'] . ')
							AND EXISTS (
								SELECT 1
								FROM email_events ee
								WHERE ee.cid = se.cid
									AND ee.system_email_id = se.id
									' . $strEmailEventSql . '
							)';

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchUnsubscribedContactPointEmailByEmailAddressOfSentEmails( $arrmixScheduledEmailsReportFilters, $objDatabase ) {

		$strSql = 'SELECT
							se.to_email_address,
							se.id,
							se.customer_id,
							se.applicant_id,
							se.cid,
							se.property_id
						FROM
							system_emails se
							JOIN system_email_blocks seb ON ( seb.cid = se.cid AND seb.email_address = se.to_email_address AND seb.email_block_type_id = ' . CEmailBlockType::EVENT_SCHEDULING_EMAIL . ' )
						WHERE
							se.cid = ' . ( int ) $arrmixScheduledEmailsReportFilters['cid'] . '
							AND se.id IN ( ' . $arrmixScheduledEmailsReportFilters['system_email_ids'] . ' )';

		return self::fetchSystemEmails( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailByIdByCid( $intSystemEmailId, $intCid, $objDatabase ) {
		if ( false == valId( $intSystemEmailId ) || false == valId( $intCid ) ) return NULL;

		$strSql	= 'SELECT
						se.*
					FROM
						system_emails se
					WHERE
						se.id = ' . $intSystemEmailId . '
						AND se.cid = ' . ( int ) $intCid;

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailByIdByPropertyIdByCid( $intSystemEmailId, $intPropertyId, $intCid, $objDatabase ) {
		if ( false == valId( $intSystemEmailId ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSql	= 'SELECT
						se.*
					FROM
						system_emails se
					WHERE
						se.id = ' . ( int ) $intSystemEmailId . '
						AND se.cid = ' . ( int ) $intCid . '
						AND se.property_id = ' . ( int ) $intPropertyId;

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchSystemEmailByRecipientIdByCid( $intSystemEmailRecipientId, $intCid, $objDatabase ) {
		if ( false == valId( $intSystemEmailRecipientId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT 
						se.*
					FROM
						system_emails se 
						JOIN system_email_recipients ser ON( se.cid = ser.cid and se.id = ser.system_email_id )
					WHERE
						se.system_email_type_id IN ( ' . CSystemEmailType::MESSAGE_CENTER_EMAIL . ',' . CSystemEmailType::EVENT_SCHEDULER_EMAIL . ')
						AND ser.id = ' . ( int ) $intSystemEmailRecipientId . '
						AND se.cid = ' . ( int ) $intCid;

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchLatestSystemEmailBySubjectBySentOnDateBySystemEmailTypeId( $strSubject, $strSentOnDate, $intSystemEmailTypeId, $objDatabase ) {
		if( false == valStr( $strSubject ) || false == valStr( $strSentOnDate ) || false == valId( $intSystemEmailTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						se.*
					FROM
						system_emails se 
					WHERE
						se.system_email_type_id = ' . ( int ) $intSystemEmailTypeId . '
						AND se.subject = \'' . $strSubject . '\'
						AND se.scheduled_send_datetime::DATE = \'' . $strSentOnDate . '\'
					ORDER BY
						id DESC
					LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

	public static function fetchFailedSystemEmailsByIds( $arrintSystemEmailIds, $objDatabase ) {

		if ( false == valArr( $arrintSystemEmailIds ) ) return NULL;

		$strSql	= 'SELECT
						*
					FROM
						system_emails se
					WHERE
						id IN ( ' . implode( ',', $arrintSystemEmailIds ) . ' )
						AND failed_on IS NOT NULL ';

		return self::fetchSystemEmails( $strSql, $objDatabase );

	}

	public static function fetchMaintenanceEmailByEventIdByCustomerIdsByCid( $intEventId, $arrintCustomerIds, $intCid, $objDatabase ) {

		if( false == valId( $intEventId ) ) {
			return false;
		}

		$strSql = ' SELECT
						se.*
					FROM
						system_emails se
					WHERE
						se.system_email_type_id IN ( ' . \CSystemEmailType::MAINTENANCE . ',' . \CSystemEmailType::APPLICATION_DOCUMENT . ')
						AND se.event_id = ' . ( int ) $intEventId . '
						AND customer_id IN ( ' . sqlIntImplode( $arrintCustomerIds ) . ' )
						AND se.cid = ' . ( int ) $intCid . ' LIMIT 1';

		return self::fetchSystemEmail( $strSql, $objDatabase );
	}

}
?>