<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Email\CSystemEmailCategories
 * Do not add any new functions to this class.
 */

class CSystemEmailCategories extends CBaseSystemEmailCategories {

    public static function fetchSystemEmailCategories( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CSystemEmailCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchSystemEmailCategory( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CSystemEmailCategory', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>