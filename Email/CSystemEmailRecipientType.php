<?php

class CSystemEmailRecipientType extends CBaseSystemEmailRecipientType {

	const LEAD					= 1;
	const COMPANY_EMPLOYEE		= 2;
	const PSI_EMPLOYEE			= 3;
	const RESIDENT				= 4;
	const CLIENT				= 5;
	const EMPLOYEE_APPLICATION	= 6;
	const CAMPAIGN_TARGET		= 7;
	const VENDORS 				= 8;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>