<?php

class CSystemEmailBlock extends CBaseSystemEmailBlock {

	protected $m_strEncryptedCustomerId;

	public function getEncryptedCustomerId() {
    	return $this->m_strEncryptedCustomerId;
	}

    public function setEncryptedCustomerId( $intEncryptedCustomerId ) {
    	$this->m_strEncryptedCustomerId = $intEncryptedCustomerId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		if( true == isset( $arrmixValues['encrypted_customer_id'] ) ) $this->setEncryptedCustomerId( $arrmixValues['encrypted_customer_id'] );

		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

    public function fetchDuplicateSystemEmailBlock( $objDatabase ) {

        return CSystemEmailBlocks::fetchConflictingSystemEmailBlockCountByCidByCustomerIdByEmailBlockTypeIdByEmailAddress( $this->getCid(), $this->getCustomerId(), $this->getEmailBlockTypeId(), $this->getEmailAddress(), $objDatabase );
	}

    public function valCid() {
    	$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

        return $boolIsValid;
	}

	public function valEmailBlockTypeId() {
    	$boolIsValid = true;

        if( true == is_null( $this->getEmailBlockTypeId() ) ) {
        	$boolIsValid = false;
           	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_block_type_id', 'Block type is required.' ) );
		}

        return $boolIsValid;
	}

    public function valEmailAddress() {
    	$boolIsValid = true;

        if( true == is_null( $this->getEmailAddress() ) ) {
        	$boolIsValid = false;
           	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
		} elseif( false == is_null( $this->getEmailAddress() ) ) {

			if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is not valid.' ) );
			}
		}

        return $boolIsValid;
	}

    public function valCustomerId() {
    	$boolIsValid = true;

        return $boolIsValid;
	}

    public function valIsPublished() {
    	$boolIsValid = true;

        return $boolIsValid;
    }

    public function valOrderNum() {
    	$boolIsValid = true;

        return $boolIsValid;
	}

    public function valCreatedBy() {
    	$boolIsValid = true;

        return $boolIsValid;
	}

    public function valCreatedOn() {
    	$boolIsValid = true;
      	return $boolIsValid;
	}

    public function validate( $strAction ) {
    	$boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmailBlockTypeId();
            	$boolIsValid &= $this->valEmailAddress();
            	// Todo:- We need to validate scenario of duplicate email address
				break;

			case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
		}

        return $boolIsValid;
	}

	public function determineMassEmailTypeId() {

		switch( $this->getEmailBlockTypeId() ) {
			case CEmailBlockType::PS_NEWSLETTER;
				return [ CMassEmailType::MONTHLY_NEWSLETTER, CMassEmailType::INTERNAL_NEWSLETTER ];
				break;

			case CEmailBlockType::SCHEDULED_DOWN_TIME;
				return [ CMassEmailType::SCHEDULED_DOWN_TIME ];
				break;

			case CEmailBlockType::NEW_PRODUCT_ANNOUNCEMENT;
				return [ CMassEmailType::SALES_EMAIL, CMassEmailType::NEW_PRODUCT_ANNOUNCEMENT ];
				break;

			case CEmailBlockType::OTHER_MASS_EMAIL;
				return [ CMassEmailType::OTHER ];
				break;

			case CEmailBlockType::PRODUCT_UPDATE_EMAIL;
				return [ CMassEmailType::PRODUCT_UPDATE_EMAIL ];
				break;

			case CEmailBlockType::PRODUCT_PROMOTION;
				return [ CMassEmailType::PRODUCT_PROMOTION ];
				break;

			default:
				return NULL;
				break;
		}
	}

}

?>