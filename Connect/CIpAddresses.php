<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CIpAddresses
 * Do not add any new functions to this class.
 */

class CIpAddresses extends CBaseIpAddresses {

	public static function fetchIpAddressesByIds( $arrintIpAddressesIds, $objConnectDatabase ) {

		if( false == valArr( $arrintIpAddressesIds ) ) return NULL;

		return self::fetchIpAddresses( sprintf( 'SELECT * FROM %s WHERE id IN( %s )', 'ip_addresses', implode( ',', $arrintIpAddressesIds ) ), $objConnectDatabase );
	}

	public static function fetchAllPaginatedIpAddresses( $intPageNo, $intPageSize, $arrstrFilteredExplodedSearch, $objConnectDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT
						ia.id,
						ia.name,
						iat.name AS ip_address_type_name,
						ia.ip_address,
						ia.is_published
					FROM
						ip_addresses ia
						JOIN ip_address_types iat ON (ia.ip_address_type_id = iat.id)
					WHERE
						( ia.name ILIKE \'%' . implode( '%\' AND ia.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ia.ip_address LIKE \'%' . implode( '%\' AND ia.ip_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR iat.name ILIKE \'%' . implode( '%\' AND iat.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					ORDER BY
						' . ' ' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET
						' . ( int ) $intOffset . '
					LIMIT
						' . $intLimit;

		return self::fetchIpAddresses( $strSql, $objConnectDatabase );
	}

	public static function fetchPaginatedIpAddressesCount( $arrstrFilteredExplodedSearch, $objConnectDatabase ) {

		$strSql = 'SELECT
						count( * )
					FROM
						ip_addresses ia
						JOIN ip_address_types iat ON (ia.ip_address_type_id = iat.id)
					WHERE
						( ia.name ILIKE \'%' . implode( '%\' AND ia.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ia.ip_address LIKE \'%' . implode( '%\' AND ia.ip_address ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR iat.name ILIKE \'%' . implode( '%\' AND iat.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )';

		$arrintResponse = fetchData( $strSql, $objConnectDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchIpAddressesByIPAddressTypIds( $arrintIpAddressTypeIds, $objConnectDatabase ) {

		if( false == valArr( $arrintIpAddressTypeIds ) ) return NULL;

		return self::fetchIpAddresses( 'SELECT * FROM ip_addresses WHERE ip_address_type_id IN( ' . implode( ',', $arrintIpAddressTypeIds ) . ' )', $objConnectDatabase );
	}
}
?>