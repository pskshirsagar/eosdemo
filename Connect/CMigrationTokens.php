<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CMigrationTokens
 * Do not add any new functions to this class.
 */

class CMigrationTokens extends CBaseMigrationTokens {

	public static function fetchMigrationTokensByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_tokens WHERE cid = ' . ( int ) $intCid . ' AND is_expired IS NULL AND deleted_on IS NULL;';
		return self::fetchMigrationTokens( $strSql, $objDatabase );
	}

	public static function fetchMigrationTokenByToken( $strToken, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_tokens WHERE token = \'' . $strToken . '\' AND is_expired IS NULL AND deleted_on IS NULL;';
		return self::fetchMigrationToken( $strSql, $objDatabase );
	}

	public static function fetchAllMigrationTokenByIdByCid( $intId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_tokens WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid . ';';
		return self::fetchMigrationToken( $strSql, $objDatabase );
	}

	public static function fetchAllMigrationTokensByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM migration_tokens WHERE ( cid = ' . ( int ) $intCid . ' OR destination_cid = ' . ( int ) $intCid . ' ) AND deleted_on IS NULL ORDER BY created_on DESC;';
		return self::fetchMigrationTokens( $strSql, $objDatabase );
	}

	public static function fetchTokensByDatabaseIdByCid( $intDatabaseId, $intCid = NULL, $objDatabase ) {

		$strSql = 'SELECT
						mt.*
					FROM
						migration_tokens mt
						JOIN directives d ON ( mt.destination_cid = d.cid )
					WHERE
						mt.is_expired IS NULL
						AND mt.deleted_on IS NULL
						AND d.database_id = ' . ( int ) $intDatabaseId;

		if( false == is_null( $intCid ) ) {
			$strSql .= ' AND d.cid = ' . ( int ) $intCid;
		}

		return self::fetchMigrationTokens( $strSql, $objDatabase );
	}

	public static function fetchMigrationTokenByIdByDestinationCid( $intId, $intDestinationCid, $objDatabase ) {

		if( true == is_null( $intId ) || true == is_null( $intDestinationCid ) ) return NULL;

		$strSql = 'SELECT * FROM migration_tokens WHERE id = ' . ( int ) $intId . ' AND destination_cid = ' . ( int ) $intDestinationCid . ';';
		return self::fetchMigrationToken( $strSql, $objDatabase );
	}

}
?>