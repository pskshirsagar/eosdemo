<?php

class CIpAddressType extends CBaseIpAddressType {

	const OFFICE		= 1;
	const DOMAIN_NAME_SERVER = 3;
	const EMAIL_DNS_SERVER = 4;
	const PROSPECT_PORTAL_DNS = 5;

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = false;
				break;
        }

        return $boolIsValid;
    }
}
?>