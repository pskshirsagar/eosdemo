<?php

class CTableStat extends CBaseTableStat {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTotalCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastCreatedOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        		$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>