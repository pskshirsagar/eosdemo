<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDocumentRecipients
 * Do not add any new functions to this class.
 */

class CBaseDocumentRecipients extends CEosPluralBase {

    const TABLE_DOCUMENT_RECIPIENTS = 'public.document_recipients';

    public static function fetchDocumentRecipients( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CDocumentRecipient', $objDatabase );
    }

    public static function fetchDocumentRecipient( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CDocumentRecipient', $objDatabase );
    }

    public static function fetchDocumentRecipientCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'document_recipients', $objDatabase );
    }

    public static function fetchDocumentRecipientById( $intId, $objDatabase ) {
        return self::fetchDocumentRecipient( sprintf( 'SELECT * FROM document_recipients WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>