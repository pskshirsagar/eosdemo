<?php

class CBaseTableStat extends CEosSingularBase {

	const TABLE_NAME = 'public.table_stats';

	protected $m_intId;
	protected $m_intTableId;
	protected $m_intDatabaseId;
	protected $m_intTotalCount;
	protected $m_strLastCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['table_id'] ) && $boolDirectSet ) $this->set( 'm_intTableId', trim( $arrValues['table_id'] ) ); elseif( isset( $arrValues['table_id'] ) ) $this->setTableId( $arrValues['table_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['total_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalCount', trim( $arrValues['total_count'] ) ); elseif( isset( $arrValues['total_count'] ) ) $this->setTotalCount( $arrValues['total_count'] );
		if( isset( $arrValues['last_created_on'] ) && $boolDirectSet ) $this->set( 'm_strLastCreatedOn', trim( $arrValues['last_created_on'] ) ); elseif( isset( $arrValues['last_created_on'] ) ) $this->setLastCreatedOn( $arrValues['last_created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTableId( $intTableId ) {
		$this->set( 'm_intTableId', CStrings::strToIntDef( $intTableId, NULL, false ) );
	}

	public function getTableId() {
		return $this->m_intTableId;
	}

	public function sqlTableId() {
		return ( true == isset( $this->m_intTableId ) ) ? ( string ) $this->m_intTableId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setTotalCount( $intTotalCount ) {
		$this->set( 'm_intTotalCount', CStrings::strToIntDef( $intTotalCount, NULL, false ) );
	}

	public function getTotalCount() {
		return $this->m_intTotalCount;
	}

	public function sqlTotalCount() {
		return ( true == isset( $this->m_intTotalCount ) ) ? ( string ) $this->m_intTotalCount : 'NULL';
	}

	public function setLastCreatedOn( $strLastCreatedOn ) {
		$this->set( 'm_strLastCreatedOn', CStrings::strTrimDef( $strLastCreatedOn, -1, NULL, true ) );
	}

	public function getLastCreatedOn() {
		return $this->m_strLastCreatedOn;
	}

	public function sqlLastCreatedOn() {
		return ( true == isset( $this->m_strLastCreatedOn ) ) ? '\'' . $this->m_strLastCreatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, table_id, database_id, total_count, last_created_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTableId() . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlTotalCount() . ', ' .
 						$this->sqlLastCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' table_id = ' . $this->sqlTableId() . ','; } elseif( true == array_key_exists( 'TableId', $this->getChangedColumns() ) ) { $strSql .= ' table_id = ' . $this->sqlTableId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_count = ' . $this->sqlTotalCount() . ','; } elseif( true == array_key_exists( 'TotalCount', $this->getChangedColumns() ) ) { $strSql .= ' total_count = ' . $this->sqlTotalCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_created_on = ' . $this->sqlLastCreatedOn() . ','; } elseif( true == array_key_exists( 'LastCreatedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_created_on = ' . $this->sqlLastCreatedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'table_id' => $this->getTableId(),
			'database_id' => $this->getDatabaseId(),
			'total_count' => $this->getTotalCount(),
			'last_created_on' => $this->getLastCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>