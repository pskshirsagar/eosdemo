<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUserTypes
 * Do not add any new functions to this class.
 */

class CBaseGlobalUserTypes extends CEosPluralBase {

	/**
	 * @return CGlobalUserType[]
	 */
	public static function fetchGlobalUserTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGlobalUserType', $objDatabase );
	}

	/**
	 * @return CGlobalUserType
	 */
	public static function fetchGlobalUserType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlobalUserType', $objDatabase );
	}

	public static function fetchGlobalUserTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'global_user_types', $objDatabase );
	}

	public static function fetchGlobalUserTypeById( $intId, $objDatabase ) {
		return self::fetchGlobalUserType( sprintf( 'SELECT * FROM global_user_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>