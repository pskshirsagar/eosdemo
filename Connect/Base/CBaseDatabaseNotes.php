<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseNotes
 * Do not add any new functions to this class.
 */

class CBaseDatabaseNotes extends CEosPluralBase {

	/**
	 * @return CDatabaseNote[]
	 */
	public static function fetchDatabaseNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseNote', $objDatabase );
	}

	/**
	 * @return CDatabaseNote
	 */
	public static function fetchDatabaseNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseNote', $objDatabase );
	}

	public static function fetchDatabaseNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_notes', $objDatabase );
	}

	public static function fetchDatabaseNoteById( $intId, $objDatabase ) {
		return self::fetchDatabaseNote( sprintf( 'SELECT * FROM database_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDatabaseNotesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDatabaseNotes( sprintf( 'SELECT * FROM database_notes WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>