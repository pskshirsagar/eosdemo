<?php

class CBaseDirective extends CEosSingularBase {

	const TABLE_NAME = 'public.directives';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDatabaseId;
	protected $m_intDirectiveTypeId;
	protected $m_intDirectiveStatusTypeId;
	protected $m_intClusterId;
	protected $m_intWebsiteId;
	protected $m_intWebsiteDomainId;
	protected $m_intPropertyId;
	protected $m_strDomain;
	protected $m_strDnsInfo;
	protected $m_strHostedAt;
	protected $m_intIsPsiHosted;
	protected $m_strLastSyncedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDirectiveStatusTypeId = '1';
		$this->m_intClusterId = '1';
		$this->m_intIsPsiHosted = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['directive_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDirectiveTypeId', trim( $arrValues['directive_type_id'] ) ); elseif( isset( $arrValues['directive_type_id'] ) ) $this->setDirectiveTypeId( $arrValues['directive_type_id'] );
		if( isset( $arrValues['directive_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDirectiveStatusTypeId', trim( $arrValues['directive_status_type_id'] ) ); elseif( isset( $arrValues['directive_status_type_id'] ) ) $this->setDirectiveStatusTypeId( $arrValues['directive_status_type_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['website_domain_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteDomainId', trim( $arrValues['website_domain_id'] ) ); elseif( isset( $arrValues['website_domain_id'] ) ) $this->setWebsiteDomainId( $arrValues['website_domain_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['dns_info'] ) && $boolDirectSet ) $this->set( 'm_strDnsInfo', trim( stripcslashes( $arrValues['dns_info'] ) ) ); elseif( isset( $arrValues['dns_info'] ) ) $this->setDnsInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dns_info'] ) : $arrValues['dns_info'] );
		if( isset( $arrValues['hosted_at'] ) && $boolDirectSet ) $this->set( 'm_strHostedAt', trim( stripcslashes( $arrValues['hosted_at'] ) ) ); elseif( isset( $arrValues['hosted_at'] ) ) $this->setHostedAt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hosted_at'] ) : $arrValues['hosted_at'] );
		if( isset( $arrValues['is_psi_hosted'] ) && $boolDirectSet ) $this->set( 'm_intIsPsiHosted', trim( $arrValues['is_psi_hosted'] ) ); elseif( isset( $arrValues['is_psi_hosted'] ) ) $this->setIsPsiHosted( $arrValues['is_psi_hosted'] );
		if( isset( $arrValues['last_synced_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSyncedOn', trim( $arrValues['last_synced_on'] ) ); elseif( isset( $arrValues['last_synced_on'] ) ) $this->setLastSyncedOn( $arrValues['last_synced_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setDirectiveTypeId( $intDirectiveTypeId ) {
		$this->set( 'm_intDirectiveTypeId', CStrings::strToIntDef( $intDirectiveTypeId, NULL, false ) );
	}

	public function getDirectiveTypeId() {
		return $this->m_intDirectiveTypeId;
	}

	public function sqlDirectiveTypeId() {
		return ( true == isset( $this->m_intDirectiveTypeId ) ) ? ( string ) $this->m_intDirectiveTypeId : 'NULL';
	}

	public function setDirectiveStatusTypeId( $intDirectiveStatusTypeId ) {
		$this->set( 'm_intDirectiveStatusTypeId', CStrings::strToIntDef( $intDirectiveStatusTypeId, NULL, false ) );
	}

	public function getDirectiveStatusTypeId() {
		return $this->m_intDirectiveStatusTypeId;
	}

	public function sqlDirectiveStatusTypeId() {
		return ( true == isset( $this->m_intDirectiveStatusTypeId ) ) ? ( string ) $this->m_intDirectiveStatusTypeId : '1';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setWebsiteDomainId( $intWebsiteDomainId ) {
		$this->set( 'm_intWebsiteDomainId', CStrings::strToIntDef( $intWebsiteDomainId, NULL, false ) );
	}

	public function getWebsiteDomainId() {
		return $this->m_intWebsiteDomainId;
	}

	public function sqlWebsiteDomainId() {
		return ( true == isset( $this->m_intWebsiteDomainId ) ) ? ( string ) $this->m_intWebsiteDomainId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 240, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setDnsInfo( $strDnsInfo ) {
		$this->set( 'm_strDnsInfo', CStrings::strTrimDef( $strDnsInfo, 240, NULL, true ) );
	}

	public function getDnsInfo() {
		return $this->m_strDnsInfo;
	}

	public function sqlDnsInfo() {
		return ( true == isset( $this->m_strDnsInfo ) ) ? '\'' . addslashes( $this->m_strDnsInfo ) . '\'' : 'NULL';
	}

	public function setHostedAt( $strHostedAt ) {
		$this->set( 'm_strHostedAt', CStrings::strTrimDef( $strHostedAt, 240, NULL, true ) );
	}

	public function getHostedAt() {
		return $this->m_strHostedAt;
	}

	public function sqlHostedAt() {
		return ( true == isset( $this->m_strHostedAt ) ) ? '\'' . addslashes( $this->m_strHostedAt ) . '\'' : 'NULL';
	}

	public function setIsPsiHosted( $intIsPsiHosted ) {
		$this->set( 'm_intIsPsiHosted', CStrings::strToIntDef( $intIsPsiHosted, NULL, false ) );
	}

	public function getIsPsiHosted() {
		return $this->m_intIsPsiHosted;
	}

	public function sqlIsPsiHosted() {
		return ( true == isset( $this->m_intIsPsiHosted ) ) ? ( string ) $this->m_intIsPsiHosted : '1';
	}

	public function setLastSyncedOn( $strLastSyncedOn ) {
		$this->set( 'm_strLastSyncedOn', CStrings::strTrimDef( $strLastSyncedOn, -1, NULL, true ) );
	}

	public function getLastSyncedOn() {
		return $this->m_strLastSyncedOn;
	}

	public function sqlLastSyncedOn() {
		return ( true == isset( $this->m_strLastSyncedOn ) ) ? '\'' . $this->m_strLastSyncedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, database_id, directive_type_id, directive_status_type_id, cluster_id, website_id, website_domain_id, property_id, domain, dns_info, hosted_at, is_psi_hosted, last_synced_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlDirectiveTypeId() . ', ' .
 						$this->sqlDirectiveStatusTypeId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlWebsiteDomainId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlDomain() . ', ' .
 						$this->sqlDnsInfo() . ', ' .
 						$this->sqlHostedAt() . ', ' .
 						$this->sqlIsPsiHosted() . ', ' .
 						$this->sqlLastSyncedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' directive_type_id = ' . $this->sqlDirectiveTypeId() . ','; } elseif( true == array_key_exists( 'DirectiveTypeId', $this->getChangedColumns() ) ) { $strSql .= ' directive_type_id = ' . $this->sqlDirectiveTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' directive_status_type_id = ' . $this->sqlDirectiveStatusTypeId() . ','; } elseif( true == array_key_exists( 'DirectiveStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' directive_status_type_id = ' . $this->sqlDirectiveStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_domain_id = ' . $this->sqlWebsiteDomainId() . ','; } elseif( true == array_key_exists( 'WebsiteDomainId', $this->getChangedColumns() ) ) { $strSql .= ' website_domain_id = ' . $this->sqlWebsiteDomainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_info = ' . $this->sqlDnsInfo() . ','; } elseif( true == array_key_exists( 'DnsInfo', $this->getChangedColumns() ) ) { $strSql .= ' dns_info = ' . $this->sqlDnsInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hosted_at = ' . $this->sqlHostedAt() . ','; } elseif( true == array_key_exists( 'HostedAt', $this->getChangedColumns() ) ) { $strSql .= ' hosted_at = ' . $this->sqlHostedAt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_psi_hosted = ' . $this->sqlIsPsiHosted() . ','; } elseif( true == array_key_exists( 'IsPsiHosted', $this->getChangedColumns() ) ) { $strSql .= ' is_psi_hosted = ' . $this->sqlIsPsiHosted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn() . ','; } elseif( true == array_key_exists( 'LastSyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_synced_on = ' . $this->sqlLastSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'database_id' => $this->getDatabaseId(),
			'directive_type_id' => $this->getDirectiveTypeId(),
			'directive_status_type_id' => $this->getDirectiveStatusTypeId(),
			'cluster_id' => $this->getClusterId(),
			'website_id' => $this->getWebsiteId(),
			'website_domain_id' => $this->getWebsiteDomainId(),
			'property_id' => $this->getPropertyId(),
			'domain' => $this->getDomain(),
			'dns_info' => $this->getDnsInfo(),
			'hosted_at' => $this->getHostedAt(),
			'is_psi_hosted' => $this->getIsPsiHosted(),
			'last_synced_on' => $this->getLastSyncedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>