<?php

class CBaseRecipientFileBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.recipient_file_batches';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDocumentRecipientId;
	protected $m_intFileBatchId;
	protected $m_strArchiveFileName;
	protected $m_strExpiresOn;
	protected $m_intIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['document_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentRecipientId', trim( $arrValues['document_recipient_id'] ) ); elseif( isset( $arrValues['document_recipient_id'] ) ) $this->setDocumentRecipientId( $arrValues['document_recipient_id'] );
		if( isset( $arrValues['file_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intFileBatchId', trim( $arrValues['file_batch_id'] ) ); elseif( isset( $arrValues['file_batch_id'] ) ) $this->setFileBatchId( $arrValues['file_batch_id'] );
		if( isset( $arrValues['archive_file_name'] ) && $boolDirectSet ) $this->set( 'm_strArchiveFileName', trim( stripcslashes( $arrValues['archive_file_name'] ) ) ); elseif( isset( $arrValues['archive_file_name'] ) ) $this->setArchiveFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['archive_file_name'] ) : $arrValues['archive_file_name'] );
		if( isset( $arrValues['expires_on'] ) && $boolDirectSet ) $this->set( 'm_strExpiresOn', trim( $arrValues['expires_on'] ) ); elseif( isset( $arrValues['expires_on'] ) ) $this->setExpiresOn( $arrValues['expires_on'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDocumentRecipientId( $intDocumentRecipientId ) {
		$this->set( 'm_intDocumentRecipientId', CStrings::strToIntDef( $intDocumentRecipientId, NULL, false ) );
	}

	public function getDocumentRecipientId() {
		return $this->m_intDocumentRecipientId;
	}

	public function sqlDocumentRecipientId() {
		return ( true == isset( $this->m_intDocumentRecipientId ) ) ? ( string ) $this->m_intDocumentRecipientId : 'NULL';
	}

	public function setFileBatchId( $intFileBatchId ) {
		$this->set( 'm_intFileBatchId', CStrings::strToIntDef( $intFileBatchId, NULL, false ) );
	}

	public function getFileBatchId() {
		return $this->m_intFileBatchId;
	}

	public function sqlFileBatchId() {
		return ( true == isset( $this->m_intFileBatchId ) ) ? ( string ) $this->m_intFileBatchId : 'NULL';
	}

	public function setArchiveFileName( $strArchiveFileName ) {
		$this->set( 'm_strArchiveFileName', CStrings::strTrimDef( $strArchiveFileName, 240, NULL, true ) );
	}

	public function getArchiveFileName() {
		return $this->m_strArchiveFileName;
	}

	public function sqlArchiveFileName() {
		return ( true == isset( $this->m_strArchiveFileName ) ) ? '\'' . addslashes( $this->m_strArchiveFileName ) . '\'' : 'NULL';
	}

	public function setExpiresOn( $strExpiresOn ) {
		$this->set( 'm_strExpiresOn', CStrings::strTrimDef( $strExpiresOn, -1, NULL, true ) );
	}

	public function getExpiresOn() {
		return $this->m_strExpiresOn;
	}

	public function sqlExpiresOn() {
		return ( true == isset( $this->m_strExpiresOn ) ) ? '\'' . $this->m_strExpiresOn . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'document_recipient_id' => $this->getDocumentRecipientId(),
			'file_batch_id' => $this->getFileBatchId(),
			'archive_file_name' => $this->getArchiveFileName(),
			'expires_on' => $this->getExpiresOn(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>