<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CMigrationTokens
 * Do not add any new functions to this class.
 */

class CBaseMigrationTokens extends CEosPluralBase {

    const TABLE_MIGRATION_TOKENS = 'public.migration_tokens';

    public static function fetchMigrationTokens( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CMigrationToken', $objDatabase );
    }

    public static function fetchMigrationToken( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CMigrationToken', $objDatabase );
    }

    public static function fetchMigrationTokenCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'migration_tokens', $objDatabase );
    }

    public static function fetchMigrationTokenById( $intId, $objDatabase ) {
        return self::fetchMigrationToken( sprintf( 'SELECT * FROM migration_tokens WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchMigrationTokensByCid( $intCid, $objDatabase ) {
        return self::fetchMigrationTokens( sprintf( 'SELECT * FROM migration_tokens WHERE cid = %d ', (int) $intCid ), $objDatabase );
    }

    public static function fetchMigrationTokensByDestinationCid( $intDestinationCid, $objDatabase ) {
        return self::fetchMigrationTokens( sprintf( 'SELECT * FROM migration_tokens WHERE destination_cid = %d ', (int) $intDestinationCid ), $objDatabase );
    }
}
?>