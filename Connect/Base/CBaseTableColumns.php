<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CTableColumns
 * Do not add any new functions to this class.
 */

class CBaseTableColumns extends CEosPluralBase {

	/**
	 * @return CTableColumn[]
	 */
	public static function fetchTableColumns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTableColumn', $objDatabase );
	}

	/**
	 * @return CTableColumn
	 */
	public static function fetchTableColumn( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTableColumn', $objDatabase );
	}

	public static function fetchTableColumnCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'table_columns', $objDatabase );
	}

	public static function fetchTableColumnById( $intId, $objDatabase ) {
		return self::fetchTableColumn( sprintf( 'SELECT * FROM table_columns WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTableColumnsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTableColumns( sprintf( 'SELECT * FROM table_columns WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTableColumnsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTableColumns( sprintf( 'SELECT * FROM table_columns WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchTableColumnsByTableId( $intTableId, $objDatabase ) {
		return self::fetchTableColumns( sprintf( 'SELECT * FROM table_columns WHERE table_id = %d', ( int ) $intTableId ), $objDatabase );
	}

}
?>