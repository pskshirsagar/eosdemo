<?php

class CBaseDatabase extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.databases';

	protected $m_intId;
	protected $m_intDatabaseServerTypeId;
	protected $m_intDatabaseTypeId;
	protected $m_intClusterId;
	protected $m_intHardwareId;
	protected $m_intScriptServerId;
	protected $m_intDatabaseId;
	protected $m_intReplicationRoleId;
	protected $m_intCid;
	protected $m_strDnsHandle;
	protected $m_strMachineName;
	protected $m_strIpAddress;
	protected $m_intPort;
	protected $m_intLdapPort;
	protected $m_strDatabaseName;
	protected $m_strVersion;
	protected $m_strShortDescription;
	protected $m_strFullDescription;
	protected $m_intCompanyCount;
	protected $m_intLoadScore;
	protected $m_boolIsTemplate;
	protected $m_intIsTestDatabase;
	protected $m_intIsPrivate;
	protected $m_strInitializedOn;
	protected $m_strLastBackupOn;
	protected $m_strBackupVerifiedOn;
	protected $m_intOrderNum;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCloudId;
	protected $m_boolIsRestricted;
	protected $m_boolIsActive;

	public function __construct() {
		parent::__construct();

		$this->m_intDatabaseServerTypeId = '1';
		$this->m_intDatabaseTypeId = '2';
		$this->m_intClusterId = '1';
		$this->m_boolIsTemplate = false;
		$this->m_intIsTestDatabase = '0';
		$this->m_intIsPrivate = '0';
		$this->m_intOrderNum = '0';
		$this->m_intCloudId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_server_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseServerTypeId', trim( $arrValues['database_server_type_id'] ) ); elseif( isset( $arrValues['database_server_type_id'] ) ) $this->setDatabaseServerTypeId( $arrValues['database_server_type_id'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['hardware_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareId', trim( $arrValues['hardware_id'] ) ); elseif( isset( $arrValues['hardware_id'] ) ) $this->setHardwareId( $arrValues['hardware_id'] );
		if( isset( $arrValues['script_server_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptServerId', trim( $arrValues['script_server_id'] ) ); elseif( isset( $arrValues['script_server_id'] ) ) $this->setScriptServerId( $arrValues['script_server_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['replication_role_id'] ) && $boolDirectSet ) $this->set( 'm_intReplicationRoleId', trim( $arrValues['replication_role_id'] ) ); elseif( isset( $arrValues['replication_role_id'] ) ) $this->setReplicationRoleId( $arrValues['replication_role_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['dns_handle'] ) && $boolDirectSet ) $this->set( 'm_strDnsHandle', trim( $arrValues['dns_handle'] ) ); elseif( isset( $arrValues['dns_handle'] ) ) $this->setDnsHandle( $arrValues['dns_handle'] );
		if( isset( $arrValues['machine_name'] ) && $boolDirectSet ) $this->set( 'm_strMachineName', trim( $arrValues['machine_name'] ) ); elseif( isset( $arrValues['machine_name'] ) ) $this->setMachineName( $arrValues['machine_name'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['port'] ) && $boolDirectSet ) $this->set( 'm_intPort', trim( $arrValues['port'] ) ); elseif( isset( $arrValues['port'] ) ) $this->setPort( $arrValues['port'] );
		if( isset( $arrValues['ldap_port'] ) && $boolDirectSet ) $this->set( 'm_intLdapPort', trim( $arrValues['ldap_port'] ) ); elseif( isset( $arrValues['ldap_port'] ) ) $this->setLdapPort( $arrValues['ldap_port'] );
		if( isset( $arrValues['database_name'] ) && $boolDirectSet ) $this->set( 'm_strDatabaseName', trim( $arrValues['database_name'] ) ); elseif( isset( $arrValues['database_name'] ) ) $this->setDatabaseName( $arrValues['database_name'] );
		if( isset( $arrValues['version'] ) && $boolDirectSet ) $this->set( 'm_strVersion', trim( $arrValues['version'] ) ); elseif( isset( $arrValues['version'] ) ) $this->setVersion( $arrValues['version'] );
		if( isset( $arrValues['short_description'] ) && $boolDirectSet ) $this->set( 'm_strShortDescription', trim( $arrValues['short_description'] ) ); elseif( isset( $arrValues['short_description'] ) ) $this->setShortDescription( $arrValues['short_description'] );
		if( isset( $arrValues['full_description'] ) && $boolDirectSet ) $this->set( 'm_strFullDescription', trim( $arrValues['full_description'] ) ); elseif( isset( $arrValues['full_description'] ) ) $this->setFullDescription( $arrValues['full_description'] );
		if( isset( $arrValues['company_count'] ) && $boolDirectSet ) $this->set( 'm_intCompanyCount', trim( $arrValues['company_count'] ) ); elseif( isset( $arrValues['company_count'] ) ) $this->setCompanyCount( $arrValues['company_count'] );
		if( isset( $arrValues['load_score'] ) && $boolDirectSet ) $this->set( 'm_intLoadScore', trim( $arrValues['load_score'] ) ); elseif( isset( $arrValues['load_score'] ) ) $this->setLoadScore( $arrValues['load_score'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_boolIsTemplate', trim( stripcslashes( $arrValues['is_template'] ) ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_template'] ) : $arrValues['is_template'] );
		if( isset( $arrValues['is_test_database'] ) && $boolDirectSet ) $this->set( 'm_intIsTestDatabase', trim( $arrValues['is_test_database'] ) ); elseif( isset( $arrValues['is_test_database'] ) ) $this->setIsTestDatabase( $arrValues['is_test_database'] );
		if( isset( $arrValues['is_private'] ) && $boolDirectSet ) $this->set( 'm_intIsPrivate', trim( $arrValues['is_private'] ) ); elseif( isset( $arrValues['is_private'] ) ) $this->setIsPrivate( $arrValues['is_private'] );
		if( isset( $arrValues['initialized_on'] ) && $boolDirectSet ) $this->set( 'm_strInitializedOn', trim( $arrValues['initialized_on'] ) ); elseif( isset( $arrValues['initialized_on'] ) ) $this->setInitializedOn( $arrValues['initialized_on'] );
		if( isset( $arrValues['last_backup_on'] ) && $boolDirectSet ) $this->set( 'm_strLastBackupOn', trim( $arrValues['last_backup_on'] ) ); elseif( isset( $arrValues['last_backup_on'] ) ) $this->setLastBackupOn( $arrValues['last_backup_on'] );
		if( isset( $arrValues['backup_verified_on'] ) && $boolDirectSet ) $this->set( 'm_strBackupVerifiedOn', trim( $arrValues['backup_verified_on'] ) ); elseif( isset( $arrValues['backup_verified_on'] ) ) $this->setBackupVerifiedOn( $arrValues['backup_verified_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['cloud_id'] ) && $boolDirectSet ) $this->set( 'm_intCloudId', trim( $arrValues['cloud_id'] ) ); elseif( isset( $arrValues['cloud_id'] ) ) $this->setCloudId( $arrValues['cloud_id'] );
		if( isset( $arrValues['is_restricted'] ) && $boolDirectSet ) $this->set( 'm_boolIsRestricted', trim( stripcslashes( $arrValues['is_restricted'] ) ) ); elseif( isset( $arrValues['is_restricted'] ) ) $this->setIsRestricted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_restricted'] ) : $arrValues['is_restricted'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseServerTypeId( $intDatabaseServerTypeId ) {
		$this->set( 'm_intDatabaseServerTypeId', CStrings::strToIntDef( $intDatabaseServerTypeId, NULL, false ) );
	}

	public function getDatabaseServerTypeId() {
		return $this->m_intDatabaseServerTypeId;
	}

	public function sqlDatabaseServerTypeId() {
		return ( true == isset( $this->m_intDatabaseServerTypeId ) ) ? ( string ) $this->m_intDatabaseServerTypeId : '1';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : '2';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setHardwareId( $intHardwareId ) {
		$this->set( 'm_intHardwareId', CStrings::strToIntDef( $intHardwareId, NULL, false ) );
	}

	public function getHardwareId() {
		return $this->m_intHardwareId;
	}

	public function sqlHardwareId() {
		return ( true == isset( $this->m_intHardwareId ) ) ? ( string ) $this->m_intHardwareId : 'NULL';
	}

	public function setScriptServerId( $intScriptServerId ) {
		$this->set( 'm_intScriptServerId', CStrings::strToIntDef( $intScriptServerId, NULL, false ) );
	}

	public function getScriptServerId() {
		return $this->m_intScriptServerId;
	}

	public function sqlScriptServerId() {
		return ( true == isset( $this->m_intScriptServerId ) ) ? ( string ) $this->m_intScriptServerId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setReplicationRoleId( $intReplicationRoleId ) {
		$this->set( 'm_intReplicationRoleId', CStrings::strToIntDef( $intReplicationRoleId, NULL, false ) );
	}

	public function getReplicationRoleId() {
		return $this->m_intReplicationRoleId;
	}

	public function sqlReplicationRoleId() {
		return ( true == isset( $this->m_intReplicationRoleId ) ) ? ( string ) $this->m_intReplicationRoleId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDnsHandle( $strDnsHandle ) {
		$this->set( 'm_strDnsHandle', CStrings::strTrimDef( $strDnsHandle, 250, NULL, true ) );
	}

	public function getDnsHandle() {
		return $this->m_strDnsHandle;
	}

	public function sqlDnsHandle() {
		return ( true == isset( $this->m_strDnsHandle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDnsHandle ) : '\'' . addslashes( $this->m_strDnsHandle ) . '\'' ) : 'NULL';
	}

	public function setMachineName( $strMachineName ) {
		$this->set( 'm_strMachineName', CStrings::strTrimDef( $strMachineName, 240, NULL, true ) );
	}

	public function getMachineName() {
		return $this->m_strMachineName;
	}

	public function sqlMachineName() {
		return ( true == isset( $this->m_strMachineName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMachineName ) : '\'' . addslashes( $this->m_strMachineName ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setPort( $intPort ) {
		$this->set( 'm_intPort', CStrings::strToIntDef( $intPort, NULL, false ) );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function sqlPort() {
		return ( true == isset( $this->m_intPort ) ) ? ( string ) $this->m_intPort : 'NULL';
	}

	public function setLdapPort( $intLdapPort ) {
		$this->set( 'm_intLdapPort', CStrings::strToIntDef( $intLdapPort, NULL, false ) );
	}

	public function getLdapPort() {
		return $this->m_intLdapPort;
	}

	public function sqlLdapPort() {
		return ( true == isset( $this->m_intLdapPort ) ) ? ( string ) $this->m_intLdapPort : 'NULL';
	}

	public function setDatabaseName( $strDatabaseName ) {
		$this->set( 'm_strDatabaseName', CStrings::strTrimDef( $strDatabaseName, 64, NULL, true ) );
	}

	public function getDatabaseName() {
		return $this->m_strDatabaseName;
	}

	public function sqlDatabaseName() {
		return ( true == isset( $this->m_strDatabaseName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDatabaseName ) : '\'' . addslashes( $this->m_strDatabaseName ) . '\'' ) : 'NULL';
	}

	public function setVersion( $strVersion ) {
		$this->set( 'm_strVersion', CStrings::strTrimDef( $strVersion, 50, NULL, true ) );
	}

	public function getVersion() {
		return $this->m_strVersion;
	}

	public function sqlVersion() {
		return ( true == isset( $this->m_strVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVersion ) : '\'' . addslashes( $this->m_strVersion ) . '\'' ) : 'NULL';
	}

	public function setShortDescription( $strShortDescription ) {
		$this->set( 'm_strShortDescription', CStrings::strTrimDef( $strShortDescription, 240, NULL, true ) );
	}

	public function getShortDescription() {
		return $this->m_strShortDescription;
	}

	public function sqlShortDescription() {
		return ( true == isset( $this->m_strShortDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strShortDescription ) : '\'' . addslashes( $this->m_strShortDescription ) . '\'' ) : 'NULL';
	}

	public function setFullDescription( $strFullDescription ) {
		$this->set( 'm_strFullDescription', CStrings::strTrimDef( $strFullDescription, -1, NULL, true ) );
	}

	public function getFullDescription() {
		return $this->m_strFullDescription;
	}

	public function sqlFullDescription() {
		return ( true == isset( $this->m_strFullDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFullDescription ) : '\'' . addslashes( $this->m_strFullDescription ) . '\'' ) : 'NULL';
	}

	public function setCompanyCount( $intCompanyCount ) {
		$this->set( 'm_intCompanyCount', CStrings::strToIntDef( $intCompanyCount, NULL, false ) );
	}

	public function getCompanyCount() {
		return $this->m_intCompanyCount;
	}

	public function sqlCompanyCount() {
		return ( true == isset( $this->m_intCompanyCount ) ) ? ( string ) $this->m_intCompanyCount : 'NULL';
	}

	public function setLoadScore( $intLoadScore ) {
		$this->set( 'm_intLoadScore', CStrings::strToIntDef( $intLoadScore, NULL, false ) );
	}

	public function getLoadScore() {
		return $this->m_intLoadScore;
	}

	public function sqlLoadScore() {
		return ( true == isset( $this->m_intLoadScore ) ) ? ( string ) $this->m_intLoadScore : 'NULL';
	}

	public function setIsTemplate( $boolIsTemplate ) {
		$this->set( 'm_boolIsTemplate', CStrings::strToBool( $boolIsTemplate ) );
	}

	public function getIsTemplate() {
		return $this->m_boolIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_boolIsTemplate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTemplate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTestDatabase( $intIsTestDatabase ) {
		$this->set( 'm_intIsTestDatabase', CStrings::strToIntDef( $intIsTestDatabase, NULL, false ) );
	}

	public function getIsTestDatabase() {
		return $this->m_intIsTestDatabase;
	}

	public function sqlIsTestDatabase() {
		return ( true == isset( $this->m_intIsTestDatabase ) ) ? ( string ) $this->m_intIsTestDatabase : '0';
	}

	public function setIsPrivate( $intIsPrivate ) {
		$this->set( 'm_intIsPrivate', CStrings::strToIntDef( $intIsPrivate, NULL, false ) );
	}

	public function getIsPrivate() {
		return $this->m_intIsPrivate;
	}

	public function sqlIsPrivate() {
		return ( true == isset( $this->m_intIsPrivate ) ) ? ( string ) $this->m_intIsPrivate : '0';
	}

	public function setInitializedOn( $strInitializedOn ) {
		$this->set( 'm_strInitializedOn', CStrings::strTrimDef( $strInitializedOn, -1, NULL, true ) );
	}

	public function getInitializedOn() {
		return $this->m_strInitializedOn;
	}

	public function sqlInitializedOn() {
		return ( true == isset( $this->m_strInitializedOn ) ) ? '\'' . $this->m_strInitializedOn . '\'' : 'NULL';
	}

	public function setLastBackupOn( $strLastBackupOn ) {
		$this->set( 'm_strLastBackupOn', CStrings::strTrimDef( $strLastBackupOn, -1, NULL, true ) );
	}

	public function getLastBackupOn() {
		return $this->m_strLastBackupOn;
	}

	public function sqlLastBackupOn() {
		return ( true == isset( $this->m_strLastBackupOn ) ) ? '\'' . $this->m_strLastBackupOn . '\'' : 'NULL';
	}

	public function setBackupVerifiedOn( $strBackupVerifiedOn ) {
		$this->set( 'm_strBackupVerifiedOn', CStrings::strTrimDef( $strBackupVerifiedOn, -1, NULL, true ) );
	}

	public function getBackupVerifiedOn() {
		return $this->m_strBackupVerifiedOn;
	}

	public function sqlBackupVerifiedOn() {
		return ( true == isset( $this->m_strBackupVerifiedOn ) ) ? '\'' . $this->m_strBackupVerifiedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCloudId( $intCloudId ) {
		$this->set( 'm_intCloudId', CStrings::strToIntDef( $intCloudId, NULL, false ) );
	}

	public function getCloudId() {
		return $this->m_intCloudId;
	}

	public function sqlCloudId() {
		return ( true == isset( $this->m_intCloudId ) ) ? ( string ) $this->m_intCloudId : '1';
	}

	public function setIsRestricted( $boolIsRestricted ) {
		$this->set( 'm_boolIsRestricted', CStrings::strToBool( $boolIsRestricted ) );
	}

	public function getIsRestricted() {
		return $this->m_boolIsRestricted;
	}

	public function sqlIsRestricted() {
		return ( true == isset( $this->m_boolIsRestricted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRestricted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_server_type_id, database_type_id, cluster_id, hardware_id, script_server_id, database_id, replication_role_id, cid, dns_handle, machine_name, ip_address, port, ldap_port, database_name, version, short_description, full_description, company_count, load_score, is_template, is_test_database, is_private, initialized_on, last_backup_on, backup_verified_on, order_num, deleted_on, deleted_by, updated_by, updated_on, created_by, created_on, details, cloud_id, is_restricted, is_active )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDatabaseServerTypeId() . ', ' .
						$this->sqlDatabaseTypeId() . ', ' .
						$this->sqlClusterId() . ', ' .
						$this->sqlHardwareId() . ', ' .
						$this->sqlScriptServerId() . ', ' .
						$this->sqlDatabaseId() . ', ' .
						$this->sqlReplicationRoleId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlDnsHandle() . ', ' .
						$this->sqlMachineName() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlPort() . ', ' .
						$this->sqlLdapPort() . ', ' .
						$this->sqlDatabaseName() . ', ' .
						$this->sqlVersion() . ', ' .
						$this->sqlShortDescription() . ', ' .
						$this->sqlFullDescription() . ', ' .
						$this->sqlCompanyCount() . ', ' .
						$this->sqlLoadScore() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlIsTestDatabase() . ', ' .
						$this->sqlIsPrivate() . ', ' .
						$this->sqlInitializedOn() . ', ' .
						$this->sqlLastBackupOn() . ', ' .
						$this->sqlBackupVerifiedOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCloudId() . ', ' .
						$this->sqlIsRestricted() . ', ' .
						$this->sqlIsActive() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_server_type_id = ' . $this->sqlDatabaseServerTypeId(). ',' ; } elseif( true == array_key_exists( 'DatabaseServerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_server_type_id = ' . $this->sqlDatabaseServerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId(). ',' ; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId(). ',' ; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId(). ',' ; } elseif( true == array_key_exists( 'HardwareId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_id = ' . $this->sqlHardwareId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_server_id = ' . $this->sqlScriptServerId(). ',' ; } elseif( true == array_key_exists( 'ScriptServerId', $this->getChangedColumns() ) ) { $strSql .= ' script_server_id = ' . $this->sqlScriptServerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId(). ',' ; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replication_role_id = ' . $this->sqlReplicationRoleId(). ',' ; } elseif( true == array_key_exists( 'ReplicationRoleId', $this->getChangedColumns() ) ) { $strSql .= ' replication_role_id = ' . $this->sqlReplicationRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle(). ',' ; } elseif( true == array_key_exists( 'DnsHandle', $this->getChangedColumns() ) ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' machine_name = ' . $this->sqlMachineName(). ',' ; } elseif( true == array_key_exists( 'MachineName', $this->getChangedColumns() ) ) { $strSql .= ' machine_name = ' . $this->sqlMachineName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port = ' . $this->sqlPort(). ',' ; } elseif( true == array_key_exists( 'Port', $this->getChangedColumns() ) ) { $strSql .= ' port = ' . $this->sqlPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ldap_port = ' . $this->sqlLdapPort(). ',' ; } elseif( true == array_key_exists( 'LdapPort', $this->getChangedColumns() ) ) { $strSql .= ' ldap_port = ' . $this->sqlLdapPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName(). ',' ; } elseif( true == array_key_exists( 'DatabaseName', $this->getChangedColumns() ) ) { $strSql .= ' database_name = ' . $this->sqlDatabaseName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version = ' . $this->sqlVersion(). ',' ; } elseif( true == array_key_exists( 'Version', $this->getChangedColumns() ) ) { $strSql .= ' version = ' . $this->sqlVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_description = ' . $this->sqlShortDescription(). ',' ; } elseif( true == array_key_exists( 'ShortDescription', $this->getChangedColumns() ) ) { $strSql .= ' short_description = ' . $this->sqlShortDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' full_description = ' . $this->sqlFullDescription(). ',' ; } elseif( true == array_key_exists( 'FullDescription', $this->getChangedColumns() ) ) { $strSql .= ' full_description = ' . $this->sqlFullDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_count = ' . $this->sqlCompanyCount(). ',' ; } elseif( true == array_key_exists( 'CompanyCount', $this->getChangedColumns() ) ) { $strSql .= ' company_count = ' . $this->sqlCompanyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_score = ' . $this->sqlLoadScore(). ',' ; } elseif( true == array_key_exists( 'LoadScore', $this->getChangedColumns() ) ) { $strSql .= ' load_score = ' . $this->sqlLoadScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test_database = ' . $this->sqlIsTestDatabase(). ',' ; } elseif( true == array_key_exists( 'IsTestDatabase', $this->getChangedColumns() ) ) { $strSql .= ' is_test_database = ' . $this->sqlIsTestDatabase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate(). ',' ; } elseif( true == array_key_exists( 'IsPrivate', $this->getChangedColumns() ) ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initialized_on = ' . $this->sqlInitializedOn(). ',' ; } elseif( true == array_key_exists( 'InitializedOn', $this->getChangedColumns() ) ) { $strSql .= ' initialized_on = ' . $this->sqlInitializedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_backup_on = ' . $this->sqlLastBackupOn(). ',' ; } elseif( true == array_key_exists( 'LastBackupOn', $this->getChangedColumns() ) ) { $strSql .= ' last_backup_on = ' . $this->sqlLastBackupOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_verified_on = ' . $this->sqlBackupVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'BackupVerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' backup_verified_on = ' . $this->sqlBackupVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cloud_id = ' . $this->sqlCloudId(). ',' ; } elseif( true == array_key_exists( 'CloudId', $this->getChangedColumns() ) ) { $strSql .= ' cloud_id = ' . $this->sqlCloudId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_restricted = ' . $this->sqlIsRestricted(). ',' ; } elseif( true == array_key_exists( 'IsRestricted', $this->getChangedColumns() ) ) { $strSql .= ' is_restricted = ' . $this->sqlIsRestricted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_server_type_id' => $this->getDatabaseServerTypeId(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'cluster_id' => $this->getClusterId(),
			'hardware_id' => $this->getHardwareId(),
			'script_server_id' => $this->getScriptServerId(),
			'database_id' => $this->getDatabaseId(),
			'replication_role_id' => $this->getReplicationRoleId(),
			'cid' => $this->getCid(),
			'dns_handle' => $this->getDnsHandle(),
			'machine_name' => $this->getMachineName(),
			'ip_address' => $this->getIpAddress(),
			'port' => $this->getPort(),
			'ldap_port' => $this->getLdapPort(),
			'database_name' => $this->getDatabaseName(),
			'version' => $this->getVersion(),
			'short_description' => $this->getShortDescription(),
			'full_description' => $this->getFullDescription(),
			'company_count' => $this->getCompanyCount(),
			'load_score' => $this->getLoadScore(),
			'is_template' => $this->getIsTemplate(),
			'is_test_database' => $this->getIsTestDatabase(),
			'is_private' => $this->getIsPrivate(),
			'initialized_on' => $this->getInitializedOn(),
			'last_backup_on' => $this->getLastBackupOn(),
			'backup_verified_on' => $this->getBackupVerifiedOn(),
			'order_num' => $this->getOrderNum(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'cloud_id' => $this->getCloudId(),
			'is_restricted' => $this->getIsRestricted(),
			'is_active' => $this->getIsActive()
		);
	}

}
?>