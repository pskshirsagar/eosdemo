<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseUserTypes
 * Do not add any new functions to this class.
 */

class CBaseDatabaseUserTypes extends CEosPluralBase {

	/**
	 * @return CDatabaseUserType[]
	 */
	public static function fetchDatabaseUserTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseUserType', $objDatabase );
	}

	/**
	 * @return CDatabaseUserType
	 */
	public static function fetchDatabaseUserType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseUserType', $objDatabase );
	}

	public static function fetchDatabaseUserTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_user_types', $objDatabase );
	}

	public static function fetchDatabaseUserTypeById( $intId, $objDatabase ) {
		return self::fetchDatabaseUserType( sprintf( 'SELECT * FROM database_user_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>