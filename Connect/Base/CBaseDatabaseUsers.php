<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseUsers
 * Do not add any new functions to this class.
 */

class CBaseDatabaseUsers extends CEosPluralBase {

	/**
	 * @return CDatabaseUser[]
	 */
	public static function fetchDatabaseUsers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseUser', $objDatabase );
	}

	/**
	 * @return CDatabaseUser
	 */
	public static function fetchDatabaseUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseUser', $objDatabase );
	}

	public static function fetchDatabaseUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_users', $objDatabase );
	}

	public static function fetchDatabaseUserById( $intId, $objDatabase ) {
		return self::fetchDatabaseUser( sprintf( 'SELECT * FROM database_users WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDatabaseUsersByDatabaseUserTypeId( $intDatabaseUserTypeId, $objDatabase ) {
		return self::fetchDatabaseUsers( sprintf( 'SELECT * FROM database_users WHERE database_user_type_id = %d', ( int ) $intDatabaseUserTypeId ), $objDatabase );
	}

	public static function fetchDatabaseUsersByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDatabaseUsers( sprintf( 'SELECT * FROM database_users WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>