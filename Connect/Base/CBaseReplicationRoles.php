<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CReplicationRoles
 * Do not add any new functions to this class.
 */

class CBaseReplicationRoles extends CEosPluralBase {

	/**
	 * @return CReplicationRole[]
	 */
	public static function fetchReplicationRoles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReplicationRole::class, $objDatabase );
	}

	/**
	 * @return CReplicationRole
	 */
	public static function fetchReplicationRole( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReplicationRole::class, $objDatabase );
	}

	public static function fetchReplicationRoleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'replication_roles', $objDatabase );
	}

	public static function fetchReplicationRoleById( $intId, $objDatabase ) {
		return self::fetchReplicationRole( sprintf( 'SELECT * FROM replication_roles WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>