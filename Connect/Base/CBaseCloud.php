<?php

class CBaseCloud extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.clouds';

	protected $m_intId;
	protected $m_strName;
	protected $m_strProvider;
	protected $m_strAccountId;
	protected $m_strRegion;
	protected $m_strDomain;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strReferenceId;
	protected $m_strTimezone;
	protected $m_intTimezoneDifference;
	protected $m_strDeploymentTime;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['provider'] ) && $boolDirectSet ) $this->set( 'm_strProvider', trim( stripcslashes( $arrValues['provider'] ) ) ); elseif( isset( $arrValues['provider'] ) ) $this->setProvider( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['provider'] ) : $arrValues['provider'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_strAccountId', trim( stripcslashes( $arrValues['account_id'] ) ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_id'] ) : $arrValues['account_id'] );
		if( isset( $arrValues['region'] ) && $boolDirectSet ) $this->set( 'm_strRegion', trim( stripcslashes( $arrValues['region'] ) ) ); elseif( isset( $arrValues['region'] ) ) $this->setRegion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['region'] ) : $arrValues['region'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_strReferenceId', trim( stripcslashes( $arrValues['reference_id'] ) ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_id'] ) : $arrValues['reference_id'] );
		if( isset( $arrValues['timezone'] ) && $boolDirectSet ) $this->set( 'm_strTimezone', trim( stripcslashes( $arrValues['timezone'] ) ) ); elseif( isset( $arrValues['timezone'] ) ) $this->setTimezone( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['timezone'] ) : $arrValues['timezone'] );
		if( isset( $arrValues['timezone_difference'] ) && $boolDirectSet ) $this->set( 'm_intTimezoneDifference', trim( $arrValues['timezone_difference'] ) ); elseif( isset( $arrValues['timezone_difference'] ) ) $this->setTimezoneDifference( $arrValues['timezone_difference'] );
		if( isset( $arrValues['deployment_time'] ) && $boolDirectSet ) $this->set( 'm_strDeploymentTime', trim( $arrValues['deployment_time'] ) ); elseif( isset( $arrValues['deployment_time'] ) ) $this->setDeploymentTime( $arrValues['deployment_time'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setProvider( $strProvider ) {
		$this->set( 'm_strProvider', CStrings::strTrimDef( $strProvider, 255, NULL, true ) );
	}

	public function getProvider() {
		return $this->m_strProvider;
	}

	public function sqlProvider() {
		return ( true == isset( $this->m_strProvider ) ) ? '\'' . addslashes( $this->m_strProvider ) . '\'' : 'NULL';
	}

	public function setAccountId( $strAccountId ) {
		$this->set( 'm_strAccountId', CStrings::strTrimDef( $strAccountId, 255, NULL, true ) );
	}

	public function getAccountId() {
		return $this->m_strAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_strAccountId ) ) ? '\'' . addslashes( $this->m_strAccountId ) . '\'' : 'NULL';
	}

	public function setRegion( $strRegion ) {
		$this->set( 'm_strRegion', CStrings::strTrimDef( $strRegion, 255, NULL, true ) );
	}

	public function getRegion() {
		return $this->m_strRegion;
	}

	public function sqlRegion() {
		return ( true == isset( $this->m_strRegion ) ) ? '\'' . addslashes( $this->m_strRegion ) . '\'' : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 255, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setReferenceId( $strReferenceId ) {
		$this->set( 'm_strReferenceId', CStrings::strTrimDef( $strReferenceId, 255, NULL, true ) );
	}

	public function getReferenceId() {
		return $this->m_strReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_strReferenceId ) ) ? '\'' . addslashes( $this->m_strReferenceId ) . '\'' : 'NULL';
	}

	public function setTimezone( $strTimezone ) {
		$this->set( 'm_strTimezone', CStrings::strTrimDef( $strTimezone, 255, NULL, true ) );
	}

	public function getTimezone() {
		return $this->m_strTimezone;
	}

	public function sqlTimezone() {
		return ( true == isset( $this->m_strTimezone ) ) ? '\'' . addslashes( $this->m_strTimezone ) . '\'' : 'NULL';
	}

	public function setTimezoneDifference( $intTimezoneDifference ) {
		$this->set( 'm_intTimezoneDifference', CStrings::strToIntDef( $intTimezoneDifference, NULL, false ) );
	}

	public function getTimezoneDifference() {
		return $this->m_intTimezoneDifference;
	}

	public function sqlTimezoneDifference() {
		return ( true == isset( $this->m_intTimezoneDifference ) ) ? ( string ) $this->m_intTimezoneDifference : 'NULL';
	}

	public function setDeploymentTime( $strDeploymentTime ) {
		$this->set( 'm_strDeploymentTime', CStrings::strTrimDef( $strDeploymentTime, -1, NULL, true ) );
	}

	public function getDeploymentTime() {
		return $this->m_strDeploymentTime;
	}

	public function sqlDeploymentTime() {
		return ( true == isset( $this->m_strDeploymentTime ) ) ? '\'' . $this->m_strDeploymentTime . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, provider, account_id, region, domain, details, reference_id, timezone, timezone_difference, deployment_time, description, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlProvider() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlRegion() . ', ' .
						$this->sqlDomain() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlTimezone() . ', ' .
						$this->sqlTimezoneDifference() . ', ' .
						$this->sqlDeploymentTime() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' provider = ' . $this->sqlProvider(). ',' ; } elseif( true == array_key_exists( 'Provider', $this->getChangedColumns() ) ) { $strSql .= ' provider = ' . $this->sqlProvider() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' region = ' . $this->sqlRegion(). ',' ; } elseif( true == array_key_exists( 'Region', $this->getChangedColumns() ) ) { $strSql .= ' region = ' . $this->sqlRegion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain(). ',' ; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' timezone = ' . $this->sqlTimezone(). ',' ; } elseif( true == array_key_exists( 'Timezone', $this->getChangedColumns() ) ) { $strSql .= ' timezone = ' . $this->sqlTimezone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' timezone_difference = ' . $this->sqlTimezoneDifference(). ',' ; } elseif( true == array_key_exists( 'TimezoneDifference', $this->getChangedColumns() ) ) { $strSql .= ' timezone_difference = ' . $this->sqlTimezoneDifference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_time = ' . $this->sqlDeploymentTime(). ',' ; } elseif( true == array_key_exists( 'DeploymentTime', $this->getChangedColumns() ) ) { $strSql .= ' deployment_time = ' . $this->sqlDeploymentTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'provider' => $this->getProvider(),
			'account_id' => $this->getAccountId(),
			'region' => $this->getRegion(),
			'domain' => $this->getDomain(),
			'details' => $this->getDetails(),
			'reference_id' => $this->getReferenceId(),
			'timezone' => $this->getTimezone(),
			'timezone_difference' => $this->getTimezoneDifference(),
			'deployment_time' => $this->getDeploymentTime(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>