<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CIpAddressTypes
 * Do not add any new functions to this class.
 */

class CBaseIpAddressTypes extends CEosPluralBase {

	/**
	 * @return CIpAddressType[]
	 */
	public static function fetchIpAddressTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIpAddressType', $objDatabase );
	}

	/**
	 * @return CIpAddressType
	 */
	public static function fetchIpAddressType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIpAddressType', $objDatabase );
	}

	public static function fetchIpAddressTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ip_address_types', $objDatabase );
	}

	public static function fetchIpAddressTypeById( $intId, $objDatabase ) {
		return self::fetchIpAddressType( sprintf( 'SELECT * FROM ip_address_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>