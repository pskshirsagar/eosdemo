<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseTypes
 * Do not add any new functions to this class.
 */

class CBaseDatabaseTypes extends CEosPluralBase {

	/**
	 * @return CDatabaseType[]
	 */
	public static function fetchDatabaseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseType', $objDatabase );
	}

	/**
	 * @return CDatabaseType
	 */
	public static function fetchDatabaseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseType', $objDatabase );
	}

	public static function fetchDatabaseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_types', $objDatabase );
	}

	public static function fetchDatabaseTypeById( $intId, $objDatabase ) {
		return self::fetchDatabaseType( sprintf( 'SELECT * FROM database_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>