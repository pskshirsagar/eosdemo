<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseServerTypes
 * Do not add any new functions to this class.
 */

class CBaseDatabaseServerTypes extends CEosPluralBase {

	/**
	 * @return CDatabaseServerType[]
	 */
	public static function fetchDatabaseServerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDatabaseServerType', $objDatabase );
	}

	/**
	 * @return CDatabaseServerType
	 */
	public static function fetchDatabaseServerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDatabaseServerType', $objDatabase );
	}

	public static function fetchDatabaseServerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'database_server_types', $objDatabase );
	}

	public static function fetchDatabaseServerTypeById( $intId, $objDatabase ) {
		return self::fetchDatabaseServerType( sprintf( 'SELECT * FROM database_server_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>