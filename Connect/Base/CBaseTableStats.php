<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CTableStats
 * Do not add any new functions to this class.
 */

class CBaseTableStats extends CEosPluralBase {

	/**
	 * @return CTableStat[]
	 */
	public static function fetchTableStats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTableStat', $objDatabase );
	}

	/**
	 * @return CTableStat
	 */
	public static function fetchTableStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTableStat', $objDatabase );
	}

	public static function fetchTableStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'table_stats', $objDatabase );
	}

	public static function fetchTableStatById( $intId, $objDatabase ) {
		return self::fetchTableStat( sprintf( 'SELECT * FROM table_stats WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTableStatsByTableId( $intTableId, $objDatabase ) {
		return self::fetchTableStats( sprintf( 'SELECT * FROM table_stats WHERE table_id = %d', ( int ) $intTableId ), $objDatabase );
	}

	public static function fetchTableStatsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchTableStats( sprintf( 'SELECT * FROM table_stats WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>