<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUsers
 * Do not add any new functions to this class.
 */

class CBaseGlobalUsers extends CEosPluralBase {

    const TABLE_GLOBAL_USERS = 'public.global_users';

    public static function fetchGlobalUsers( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CGlobalUser', $objDatabase );
    }

    public static function fetchGlobalUser( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CGlobalUser', $objDatabase );
    }

    public static function fetchGlobalUserCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'global_users', $objDatabase );
    }

    public static function fetchGlobalUserById( $intId, $objDatabase ) {
        return self::fetchGlobalUser( sprintf( 'SELECT * FROM global_users WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchGlobalUsersByCidByGlobalUserTypeId( $intCid, $intGlobalUserTypeId, $objDatabase ) {
        return self::fetchGlobalUsers( sprintf( 'SELECT * FROM global_users WHERE cid = %d AND global_user_type_id = %d', (int) $intCid, (int) $intGlobalUserTypeId ), $objDatabase );
    }

    public static function fetchGlobalUsersByWorksId( $intWorksId, $objDatabase ) {
        return self::fetchGlobalUsers( sprintf( 'SELECT * FROM global_users WHERE works_id = %d', (int) $intWorksId ), $objDatabase );
    }

}
?>