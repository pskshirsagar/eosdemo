<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectives
 * Do not add any new functions to this class.
 */

class CBaseDirectives extends CEosPluralBase {

	/**
	 * @return CDirective[]
	 */
	public static function fetchDirectives( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDirective', $objDatabase );
	}

	/**
	 * @return CDirective
	 */
	public static function fetchDirective( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDirective', $objDatabase );
	}

	public static function fetchDirectiveCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'directives', $objDatabase );
	}

	public static function fetchDirectiveById( $intId, $objDatabase ) {
		return self::fetchDirective( sprintf( 'SELECT * FROM directives WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDirectivesByCid( $intCid, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDirectivesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchDirectivesByDirectiveTypeId( $intDirectiveTypeId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE directive_type_id = %d', ( int ) $intDirectiveTypeId ), $objDatabase );
	}

	public static function fetchDirectivesByDirectiveStatusTypeId( $intDirectiveStatusTypeId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE directive_status_type_id = %d', ( int ) $intDirectiveStatusTypeId ), $objDatabase );
	}

	public static function fetchDirectivesByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchDirectivesByWebsiteId( $intWebsiteId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE website_id = %d', ( int ) $intWebsiteId ), $objDatabase );
	}

	public static function fetchDirectivesByWebsiteDomainId( $intWebsiteDomainId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE website_domain_id = %d', ( int ) $intWebsiteDomainId ), $objDatabase );
	}

	public static function fetchDirectivesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchDirectives( sprintf( 'SELECT * FROM directives WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>