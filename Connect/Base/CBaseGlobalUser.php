<?php

class CBaseGlobalUser extends CEosSingularBase {

	const TABLE_NAME = 'public.global_users';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intWorksId;
	protected $m_intGlobalUserTypeId;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strBluemoonEmailAddress;
	protected $m_strBluemoonPasswordEncrypted;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_intIsAdministrator;
	protected $m_intIsSystemUser;
	protected $m_intIsDisabled;
	protected $m_intIsActiveDirectoryUser;
	protected $m_strLastLogin;
	protected $m_strLastAccess;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsAdministrator = '0';
		$this->m_intIsSystemUser = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsActiveDirectoryUser = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['works_id'] ) && $boolDirectSet ) $this->set( 'm_intWorksId', trim( $arrValues['works_id'] ) ); elseif( isset( $arrValues['works_id'] ) ) $this->setWorksId( $arrValues['works_id'] );
		if( isset( $arrValues['global_user_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGlobalUserTypeId', trim( $arrValues['global_user_type_id'] ) ); elseif( isset( $arrValues['global_user_type_id'] ) ) $this->setGlobalUserTypeId( $arrValues['global_user_type_id'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['bluemoon_email_address'] ) && $boolDirectSet ) $this->set( 'm_strBluemoonEmailAddress', trim( stripcslashes( $arrValues['bluemoon_email_address'] ) ) ); elseif( isset( $arrValues['bluemoon_email_address'] ) ) $this->setBluemoonEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bluemoon_email_address'] ) : $arrValues['bluemoon_email_address'] );
		if( isset( $arrValues['bluemoon_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBluemoonPasswordEncrypted', trim( stripcslashes( $arrValues['bluemoon_password_encrypted'] ) ) ); elseif( isset( $arrValues['bluemoon_password_encrypted'] ) ) $this->setBluemoonPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bluemoon_password_encrypted'] ) : $arrValues['bluemoon_password_encrypted'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['is_administrator'] ) && $boolDirectSet ) $this->set( 'm_intIsAdministrator', trim( $arrValues['is_administrator'] ) ); elseif( isset( $arrValues['is_administrator'] ) ) $this->setIsAdministrator( $arrValues['is_administrator'] );
		if( isset( $arrValues['is_system_user'] ) && $boolDirectSet ) $this->set( 'm_intIsSystemUser', trim( $arrValues['is_system_user'] ) ); elseif( isset( $arrValues['is_system_user'] ) ) $this->setIsSystemUser( $arrValues['is_system_user'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_active_directory_user'] ) && $boolDirectSet ) $this->set( 'm_intIsActiveDirectoryUser', trim( $arrValues['is_active_directory_user'] ) ); elseif( isset( $arrValues['is_active_directory_user'] ) ) $this->setIsActiveDirectoryUser( $arrValues['is_active_directory_user'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['last_access'] ) && $boolDirectSet ) $this->set( 'm_strLastAccess', trim( $arrValues['last_access'] ) ); elseif( isset( $arrValues['last_access'] ) ) $this->setLastAccess( $arrValues['last_access'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setWorksId( $intWorksId ) {
		$this->set( 'm_intWorksId', CStrings::strToIntDef( $intWorksId, NULL, false ) );
	}

	public function getWorksId() {
		return $this->m_intWorksId;
	}

	public function sqlWorksId() {
		return ( true == isset( $this->m_intWorksId ) ) ? ( string ) $this->m_intWorksId : 'NULL';
	}

	public function setGlobalUserTypeId( $intGlobalUserTypeId ) {
		$this->set( 'm_intGlobalUserTypeId', CStrings::strToIntDef( $intGlobalUserTypeId, NULL, false ) );
	}

	public function getGlobalUserTypeId() {
		return $this->m_intGlobalUserTypeId;
	}

	public function sqlGlobalUserTypeId() {
		return ( true == isset( $this->m_intGlobalUserTypeId ) ) ? ( string ) $this->m_intGlobalUserTypeId : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setBluemoonEmailAddress( $strBluemoonEmailAddress ) {
		$this->set( 'm_strBluemoonEmailAddress', CStrings::strTrimDef( $strBluemoonEmailAddress, 240, NULL, true ) );
	}

	public function getBluemoonEmailAddress() {
		return $this->m_strBluemoonEmailAddress;
	}

	public function sqlBluemoonEmailAddress() {
		return ( true == isset( $this->m_strBluemoonEmailAddress ) ) ? '\'' . addslashes( $this->m_strBluemoonEmailAddress ) . '\'' : 'NULL';
	}

	public function setBluemoonPasswordEncrypted( $strBluemoonPasswordEncrypted ) {
		$this->set( 'm_strBluemoonPasswordEncrypted', CStrings::strTrimDef( $strBluemoonPasswordEncrypted, 500, NULL, true ) );
	}

	public function getBluemoonPasswordEncrypted() {
		return $this->m_strBluemoonPasswordEncrypted;
	}

	public function sqlBluemoonPasswordEncrypted() {
		return ( true == isset( $this->m_strBluemoonPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strBluemoonPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setIsAdministrator( $intIsAdministrator ) {
		$this->set( 'm_intIsAdministrator', CStrings::strToIntDef( $intIsAdministrator, NULL, false ) );
	}

	public function getIsAdministrator() {
		return $this->m_intIsAdministrator;
	}

	public function sqlIsAdministrator() {
		return ( true == isset( $this->m_intIsAdministrator ) ) ? ( string ) $this->m_intIsAdministrator : '0';
	}

	public function setIsSystemUser( $intIsSystemUser ) {
		$this->set( 'm_intIsSystemUser', CStrings::strToIntDef( $intIsSystemUser, NULL, false ) );
	}

	public function getIsSystemUser() {
		return $this->m_intIsSystemUser;
	}

	public function sqlIsSystemUser() {
		return ( true == isset( $this->m_intIsSystemUser ) ) ? ( string ) $this->m_intIsSystemUser : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsActiveDirectoryUser( $intIsActiveDirectoryUser ) {
		$this->set( 'm_intIsActiveDirectoryUser', CStrings::strToIntDef( $intIsActiveDirectoryUser, NULL, false ) );
	}

	public function getIsActiveDirectoryUser() {
		return $this->m_intIsActiveDirectoryUser;
	}

	public function sqlIsActiveDirectoryUser() {
		return ( true == isset( $this->m_intIsActiveDirectoryUser ) ) ? ( string ) $this->m_intIsActiveDirectoryUser : '0';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setLastAccess( $strLastAccess ) {
		$this->set( 'm_strLastAccess', CStrings::strTrimDef( $strLastAccess, -1, NULL, true ) );
	}

	public function getLastAccess() {
		return $this->m_strLastAccess;
	}

	public function sqlLastAccess() {
		return ( true == isset( $this->m_strLastAccess ) ) ? '\'' . $this->m_strLastAccess . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, works_id, global_user_type_id, username, password_encrypted, bluemoon_email_address, bluemoon_password_encrypted, email_address, phone_number, is_administrator, is_system_user, is_disabled, is_active_directory_user, last_login, last_access, login_attempt_count, last_login_attempt_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlWorksId() . ', ' .
 						$this->sqlGlobalUserTypeId() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlBluemoonEmailAddress() . ', ' .
 						$this->sqlBluemoonPasswordEncrypted() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlIsAdministrator() . ', ' .
 						$this->sqlIsSystemUser() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlIsActiveDirectoryUser() . ', ' .
 						$this->sqlLastLogin() . ', ' .
 						$this->sqlLastAccess() . ', ' .
 						$this->sqlLoginAttemptCount() . ', ' .
 						$this->sqlLastLoginAttemptOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' works_id = ' . $this->sqlWorksId() . ','; } elseif( true == array_key_exists( 'WorksId', $this->getChangedColumns() ) ) { $strSql .= ' works_id = ' . $this->sqlWorksId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' global_user_type_id = ' . $this->sqlGlobalUserTypeId() . ','; } elseif( true == array_key_exists( 'GlobalUserTypeId', $this->getChangedColumns() ) ) { $strSql .= ' global_user_type_id = ' . $this->sqlGlobalUserTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bluemoon_email_address = ' . $this->sqlBluemoonEmailAddress() . ','; } elseif( true == array_key_exists( 'BluemoonEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' bluemoon_email_address = ' . $this->sqlBluemoonEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bluemoon_password_encrypted = ' . $this->sqlBluemoonPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'BluemoonPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bluemoon_password_encrypted = ' . $this->sqlBluemoonPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; } elseif( true == array_key_exists( 'IsAdministrator', $this->getChangedColumns() ) ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system_user = ' . $this->sqlIsSystemUser() . ','; } elseif( true == array_key_exists( 'IsSystemUser', $this->getChangedColumns() ) ) { $strSql .= ' is_system_user = ' . $this->sqlIsSystemUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active_directory_user = ' . $this->sqlIsActiveDirectoryUser() . ','; } elseif( true == array_key_exists( 'IsActiveDirectoryUser', $this->getChangedColumns() ) ) { $strSql .= ' is_active_directory_user = ' . $this->sqlIsActiveDirectoryUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_access = ' . $this->sqlLastAccess() . ','; } elseif( true == array_key_exists( 'LastAccess', $this->getChangedColumns() ) ) { $strSql .= ' last_access = ' . $this->sqlLastAccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'works_id' => $this->getWorksId(),
			'global_user_type_id' => $this->getGlobalUserTypeId(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'bluemoon_email_address' => $this->getBluemoonEmailAddress(),
			'bluemoon_password_encrypted' => $this->getBluemoonPasswordEncrypted(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'is_administrator' => $this->getIsAdministrator(),
			'is_system_user' => $this->getIsSystemUser(),
			'is_disabled' => $this->getIsDisabled(),
			'is_active_directory_user' => $this->getIsActiveDirectoryUser(),
			'last_login' => $this->getLastLogin(),
			'last_access' => $this->getLastAccess(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>