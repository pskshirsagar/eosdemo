<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CIpAddresses
 * Do not add any new functions to this class.
 */

class CBaseIpAddresses extends CEosPluralBase {

	/**
	 * @return CIpAddress[]
	 */
	public static function fetchIpAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIpAddress', $objDatabase );
	}

	/**
	 * @return CIpAddress
	 */
	public static function fetchIpAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIpAddress', $objDatabase );
	}

	public static function fetchIpAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ip_addresses', $objDatabase );
	}

	public static function fetchIpAddressById( $intId, $objDatabase ) {
		return self::fetchIpAddress( sprintf( 'SELECT * FROM ip_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIpAddressesByIpAddressTypeId( $intIpAddressTypeId, $objDatabase ) {
		return self::fetchIpAddresses( sprintf( 'SELECT * FROM ip_addresses WHERE ip_address_type_id = %d', ( int ) $intIpAddressTypeId ), $objDatabase );
	}

}
?>