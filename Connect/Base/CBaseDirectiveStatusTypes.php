<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectiveStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseDirectiveStatusTypes extends CEosPluralBase {

	/**
	 * @return CDirectiveStatusType[]
	 */
	public static function fetchDirectiveStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDirectiveStatusType', $objDatabase );
	}

	/**
	 * @return CDirectiveStatusType
	 */
	public static function fetchDirectiveStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDirectiveStatusType', $objDatabase );
	}

	public static function fetchDirectiveStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'directive_status_types', $objDatabase );
	}

	public static function fetchDirectiveStatusTypeById( $intId, $objDatabase ) {
		return self::fetchDirectiveStatusType( sprintf( 'SELECT * FROM directive_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>