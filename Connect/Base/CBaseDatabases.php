<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabases
 * Do not add any new functions to this class.
 */

class CBaseDatabases extends CEosPluralBase {

	/**
	 * @return CDatabase[]
	 */
	public static function fetchDatabases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDatabase::class, $objDatabase );
	}

	/**
	 * @return CDatabase
	 */
	public static function fetchDatabase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDatabase::class, $objDatabase );
	}

	public static function fetchDatabaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'databases', $objDatabase );
	}

	public static function fetchDatabaseById( $intId, $objDatabase ) {
		return self::fetchDatabase( sprintf( 'SELECT * FROM databases WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDatabasesByDatabaseServerTypeId( $intDatabaseServerTypeId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE database_server_type_id = %d', $intDatabaseServerTypeId ), $objDatabase );
	}

	public static function fetchDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE database_type_id = %d', $intDatabaseTypeId ), $objDatabase );
	}

	public static function fetchDatabasesByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE cluster_id = %d', $intClusterId ), $objDatabase );
	}

	public static function fetchDatabasesByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE hardware_id = %d', $intHardwareId ), $objDatabase );
	}

	public static function fetchDatabasesByScriptServerId( $intScriptServerId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE script_server_id = %d', $intScriptServerId ), $objDatabase );
	}

	public static function fetchDatabasesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE database_id = %d', $intDatabaseId ), $objDatabase );
	}

	public static function fetchDatabasesByReplicationRoleId( $intReplicationRoleId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE replication_role_id = %d', $intReplicationRoleId ), $objDatabase );
	}

	public static function fetchDatabasesByCid( $intCid, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchDatabasesByCloudId( $intCloudId, $objDatabase ) {
		return self::fetchDatabases( sprintf( 'SELECT * FROM databases WHERE cloud_id = %d', $intCloudId ), $objDatabase );
	}

}
?>