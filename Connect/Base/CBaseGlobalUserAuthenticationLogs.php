<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CBaseGlobalUserAuthenticationLogs extends CEosPluralBase {

	/**
	 * @return CGlobalUserAuthenticationLog[]
	 */
	public static function fetchGlobalUserAuthenticationLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGlobalUserAuthenticationLog', $objDatabase );
	}

	/**
	 * @return CGlobalUserAuthenticationLog
	 */
	public static function fetchGlobalUserAuthenticationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGlobalUserAuthenticationLog', $objDatabase );
	}

	public static function fetchGlobalUserAuthenticationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'global_user_authentication_logs', $objDatabase );
	}

	public static function fetchGlobalUserAuthenticationLogById( $intId, $objDatabase ) {
		return self::fetchGlobalUserAuthenticationLog( sprintf( 'SELECT * FROM global_user_authentication_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGlobalUserAuthenticationLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchGlobalUserAuthenticationLogs( sprintf( 'SELECT * FROM global_user_authentication_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>