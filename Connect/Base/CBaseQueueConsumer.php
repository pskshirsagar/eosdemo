<?php

class CBaseQueueConsumer extends CEosSingularBase {

	protected $m_intId;
	protected $m_strBasicQueueName;
	protected $m_strConsumerClassName;
	protected $m_intMaxExecutionSeconds;
	protected $m_strSchedule;
	protected $m_boolIsClusterSpecific;
	protected $m_boolIsPaymentsSpecific;
	protected $m_boolIsLogging;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClusterSpecific = false;
		$this->m_boolIsPaymentsSpecific = false;
		$this->m_boolIsLogging = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['basic_queue_name'] ) && $boolDirectSet ) $this->m_strBasicQueueName = trim( stripcslashes( $arrValues['basic_queue_name'] ) ); else if( isset( $arrValues['basic_queue_name'] ) ) $this->setBasicQueueName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['basic_queue_name'] ) : $arrValues['basic_queue_name'] );
		if( isset( $arrValues['consumer_class_name'] ) && $boolDirectSet ) $this->m_strConsumerClassName = trim( stripcslashes( $arrValues['consumer_class_name'] ) ); else if( isset( $arrValues['consumer_class_name'] ) ) $this->setConsumerClassName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['consumer_class_name'] ) : $arrValues['consumer_class_name'] );
		if( isset( $arrValues['max_execution_seconds'] ) && $boolDirectSet ) $this->m_intMaxExecutionSeconds = trim( $arrValues['max_execution_seconds'] ); else if( isset( $arrValues['max_execution_seconds'] ) ) $this->setMaxExecutionSeconds( $arrValues['max_execution_seconds'] );
		if( isset( $arrValues['schedule'] ) && $boolDirectSet ) $this->m_strSchedule = trim( stripcslashes( $arrValues['schedule'] ) ); else if( isset( $arrValues['schedule'] ) ) $this->setSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['schedule'] ) : $arrValues['schedule'] );
		if( isset( $arrValues['is_cluster_specific'] ) && $boolDirectSet ) $this->m_boolIsClusterSpecific = trim( stripcslashes( $arrValues['is_cluster_specific'] ) ); else if( isset( $arrValues['is_cluster_specific'] ) ) $this->setIsClusterSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cluster_specific'] ) : $arrValues['is_cluster_specific'] );
		if( isset( $arrValues['is_payments_specific'] ) && $boolDirectSet ) $this->m_boolIsPaymentsSpecific = trim( stripcslashes( $arrValues['is_payments_specific'] ) ); else if( isset( $arrValues['is_payments_specific'] ) ) $this->setIsPaymentsSpecific( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_payments_specific'] ) : $arrValues['is_payments_specific'] );
		if( isset( $arrValues['is_logging'] ) && $boolDirectSet ) $this->m_boolIsLogging = trim( stripcslashes( $arrValues['is_logging'] ) ); else if( isset( $arrValues['is_logging'] ) ) $this->setIsLogging( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_logging'] ) : $arrValues['is_logging'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBasicQueueName( $strBasicQueueName ) {
		$this->m_strBasicQueueName = CStrings::strTrimDef( $strBasicQueueName, 150, NULL, true );
	}

	public function getBasicQueueName() {
		return $this->m_strBasicQueueName;
	}

	public function sqlBasicQueueName() {
		return ( true == isset( $this->m_strBasicQueueName ) ) ? '\'' . addslashes( $this->m_strBasicQueueName ) . '\'' : 'NULL';
	}

	public function setConsumerClassName( $strConsumerClassName ) {
		$this->m_strConsumerClassName = CStrings::strTrimDef( $strConsumerClassName, 250, NULL, true );
	}

	public function getConsumerClassName() {
		return $this->m_strConsumerClassName;
	}

	public function sqlConsumerClassName() {
		return ( true == isset( $this->m_strConsumerClassName ) ) ? '\'' . addslashes( $this->m_strConsumerClassName ) . '\'' : 'NULL';
	}

	public function setMaxExecutionSeconds( $intMaxExecutionSeconds ) {
		$this->m_intMaxExecutionSeconds = CStrings::strToIntDef( $intMaxExecutionSeconds, NULL, false );
	}

	public function getMaxExecutionSeconds() {
		return $this->m_intMaxExecutionSeconds;
	}

	public function sqlMaxExecutionSeconds() {
		return ( true == isset( $this->m_intMaxExecutionSeconds ) ) ? ( string ) $this->m_intMaxExecutionSeconds : 'NULL';
	}

	public function setSchedule( $strSchedule ) {
		$this->m_strSchedule = CStrings::strTrimDef( $strSchedule, 250, NULL, true );
	}

	public function getSchedule() {
		return $this->m_strSchedule;
	}

	public function sqlSchedule() {
		return ( true == isset( $this->m_strSchedule ) ) ? '\'' . addslashes( $this->m_strSchedule ) . '\'' : 'NULL';
	}

	public function setIsClusterSpecific( $boolIsClusterSpecific ) {
		$this->m_boolIsClusterSpecific = CStrings::strToBool( $boolIsClusterSpecific );
	}

	public function getIsClusterSpecific() {
		return $this->m_boolIsClusterSpecific;
	}

	public function sqlIsClusterSpecific() {
		return ( true == isset( $this->m_boolIsClusterSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClusterSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPaymentsSpecific( $boolIsPaymentsSpecific ) {
		$this->m_boolIsPaymentsSpecific = CStrings::strToBool( $boolIsPaymentsSpecific );
	}

	public function getIsPaymentsSpecific() {
		return $this->m_boolIsPaymentsSpecific;
	}

	public function sqlIsPaymentsSpecific() {
		return ( true == isset( $this->m_boolIsPaymentsSpecific ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaymentsSpecific ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLogging( $boolIsLogging ) {
		$this->m_boolIsLogging = CStrings::strToBool( $boolIsLogging );
	}

	public function getIsLogging() {
		return $this->m_boolIsLogging;
	}

	public function sqlIsLogging() {
		return ( true == isset( $this->m_boolIsLogging ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLogging ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.queue_consumers_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.queue_consumers
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBasicQueueName() . ', ' .
 						$this->sqlConsumerClassName() . ', ' .
 						$this->sqlMaxExecutionSeconds() . ', ' .
 						$this->sqlSchedule() . ', ' .
 						$this->sqlIsClusterSpecific() . ', ' .
 						$this->sqlIsPaymentsSpecific() . ', ' .
 						$this->sqlIsLogging() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.queue_consumers
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' basic_queue_name = ' . $this->sqlBasicQueueName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlBasicQueueName() ) != $this->getOriginalValueByFieldName ( 'basic_queue_name' ) ) { $arrstrOriginalValueChanges['basic_queue_name'] = $this->sqlBasicQueueName(); $strSql .= ' basic_queue_name = ' . $this->sqlBasicQueueName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' consumer_class_name = ' . $this->sqlConsumerClassName() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlConsumerClassName() ) != $this->getOriginalValueByFieldName ( 'consumer_class_name' ) ) { $arrstrOriginalValueChanges['consumer_class_name'] = $this->sqlConsumerClassName(); $strSql .= ' consumer_class_name = ' . $this->sqlConsumerClassName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_execution_seconds = ' . $this->sqlMaxExecutionSeconds() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMaxExecutionSeconds() ) != $this->getOriginalValueByFieldName ( 'max_execution_seconds' ) ) { $arrstrOriginalValueChanges['max_execution_seconds'] = $this->sqlMaxExecutionSeconds(); $strSql .= ' max_execution_seconds = ' . $this->sqlMaxExecutionSeconds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSchedule() ) != $this->getOriginalValueByFieldName ( 'schedule' ) ) { $arrstrOriginalValueChanges['schedule'] = $this->sqlSchedule(); $strSql .= ' schedule = ' . $this->sqlSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cluster_specific = ' . $this->sqlIsClusterSpecific() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsClusterSpecific() ) != $this->getOriginalValueByFieldName ( 'is_cluster_specific' ) ) { $arrstrOriginalValueChanges['is_cluster_specific'] = $this->sqlIsClusterSpecific(); $strSql .= ' is_cluster_specific = ' . $this->sqlIsClusterSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_payments_specific = ' . $this->sqlIsPaymentsSpecific() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsPaymentsSpecific() ) != $this->getOriginalValueByFieldName ( 'is_payments_specific' ) ) { $arrstrOriginalValueChanges['is_payments_specific'] = $this->sqlIsPaymentsSpecific(); $strSql .= ' is_payments_specific = ' . $this->sqlIsPaymentsSpecific() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_logging = ' . $this->sqlIsLogging() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlIsLogging() ) != $this->getOriginalValueByFieldName ( 'is_logging' ) ) { $arrstrOriginalValueChanges['is_logging'] = $this->sqlIsLogging(); $strSql .= ' is_logging = ' . $this->sqlIsLogging() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.queue_consumers WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.queue_consumers_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'basic_queue_name' => $this->getBasicQueueName(),
			'consumer_class_name' => $this->getConsumerClassName(),
			'max_execution_seconds' => $this->getMaxExecutionSeconds(),
			'schedule' => $this->getSchedule(),
			'is_cluster_specific' => $this->getIsClusterSpecific(),
			'is_payments_specific' => $this->getIsPaymentsSpecific(),
			'is_logging' => $this->getIsLogging(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>