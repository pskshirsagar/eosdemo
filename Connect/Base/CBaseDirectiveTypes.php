<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectiveTypes
 * Do not add any new functions to this class.
 */

class CBaseDirectiveTypes extends CEosPluralBase {

	/**
	 * @return CDirectiveType[]
	 */
	public static function fetchDirectiveTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDirectiveType', $objDatabase );
	}

	/**
	 * @return CDirectiveType
	 */
	public static function fetchDirectiveType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDirectiveType', $objDatabase );
	}

	public static function fetchDirectiveTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'directive_types', $objDatabase );
	}

	public static function fetchDirectiveTypeById( $intId, $objDatabase ) {
		return self::fetchDirectiveType( sprintf( 'SELECT * FROM directive_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>