<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CRecipientFileBatches
 * Do not add any new functions to this class.
 */

class CBaseRecipientFileBatches extends CEosPluralBase {

    const TABLE_RECIPIENT_FILE_BATCHES = 'public.recipient_file_batches';

    public static function fetchRecipientFileBatches( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CRecipientFileBatch', $objDatabase );
    }

    public static function fetchRecipientFileBatch( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CRecipientFileBatch', $objDatabase );
    }

    public static function fetchRecipientFileBatchCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'recipient_file_batches', $objDatabase );
    }

    public static function fetchRecipientFileBatchById( $intId, $objDatabase ) {
        return self::fetchRecipientFileBatch( sprintf( 'SELECT * FROM recipient_file_batches WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchRecipientFileBatchesByCid( $intCid, $objDatabase ) {
        return self::fetchRecipientFileBatches( sprintf( 'SELECT * FROM recipient_file_batches WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchRecipientFileBatchesByDocumentRecipientId( $intDocumentRecipientId, $objDatabase ) {
        return self::fetchRecipientFileBatches( sprintf( 'SELECT * FROM recipient_file_batches WHERE document_recipient_id = %d', (int) $intDocumentRecipientId ), $objDatabase );
    }

    public static function fetchRecipientFileBatchesByFileBatchId( $intFileBatchId, $objDatabase ) {
        return self::fetchRecipientFileBatches( sprintf( 'SELECT * FROM recipient_file_batches WHERE file_batch_id = %d', (int) $intFileBatchId ), $objDatabase );
    }

}
?>