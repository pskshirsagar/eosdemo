<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CClouds
 * Do not add any new functions to this class.
 */

class CBaseClouds extends CEosPluralBase {

	/**
	 * @return CCloud[]
	 */
	public static function fetchClouds( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCloud::class, $objDatabase );
	}

	/**
	 * @return CCloud
	 */
	public static function fetchCloud( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCloud::class, $objDatabase );
	}

	public static function fetchCloudCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'clouds', $objDatabase );
	}

	public static function fetchCloudById( $intId, $objDatabase ) {
		return self::fetchCloud( sprintf( 'SELECT * FROM clouds WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCloudsByAccountId( $strAccountId, $objDatabase ) {
		return self::fetchClouds( sprintf( 'SELECT * FROM clouds WHERE account_id = \'%s\'', $strAccountId ), $objDatabase );
	}

	public static function fetchCloudsByReferenceId( $strReferenceId, $objDatabase ) {
		return self::fetchClouds( sprintf( 'SELECT * FROM clouds WHERE reference_id = \'%s\'', $strReferenceId ), $objDatabase );
	}

}
?>