<?php

class CBaseDocumentRecipient extends CEosSingularBase {

	const TABLE_NAME = 'public.document_recipients';

	protected $m_intId;
	protected $m_strEmailAddress;
	protected $m_strPasswordEncrypted;
	protected $m_strLastLogin;
	protected $m_strLastLoginAttemptOn;
	protected $m_intLoginAttemptCount;
	protected $m_strPasswordResetToken;
	protected $m_intIsDisabled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['password_reset_token'] ) && $boolDirectSet ) $this->set( 'm_strPasswordResetToken', trim( stripcslashes( $arrValues['password_reset_token'] ) ) ); elseif( isset( $arrValues['password_reset_token'] ) ) $this->setPasswordResetToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_reset_token'] ) : $arrValues['password_reset_token'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setPasswordResetToken( $strPasswordResetToken ) {
		$this->set( 'm_strPasswordResetToken', CStrings::strTrimDef( $strPasswordResetToken, 240, NULL, true ) );
	}

	public function getPasswordResetToken() {
		return $this->m_strPasswordResetToken;
	}

	public function sqlPasswordResetToken() {
		return ( true == isset( $this->m_strPasswordResetToken ) ) ? '\'' . addslashes( $this->m_strPasswordResetToken ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, email_address, password_encrypted, last_login, last_login_attempt_on, login_attempt_count, password_reset_token, is_disabled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlLastLogin() . ', ' .
 						$this->sqlLastLoginAttemptOn() . ', ' .
 						$this->sqlLoginAttemptCount() . ', ' .
 						$this->sqlPasswordResetToken() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_reset_token = ' . $this->sqlPasswordResetToken() . ','; } elseif( true == array_key_exists( 'PasswordResetToken', $this->getChangedColumns() ) ) { $strSql .= ' password_reset_token = ' . $this->sqlPasswordResetToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'email_address' => $this->getEmailAddress(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'last_login' => $this->getLastLogin(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'password_reset_token' => $this->getPasswordResetToken(),
			'is_disabled' => $this->getIsDisabled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>