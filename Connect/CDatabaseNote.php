<?php

class CDatabaseNote extends CBaseDatabaseNote {

    public function valNote() {
        $boolIsValid = true;

        if( true == is_null( $this->getNote() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
        }

        return $boolIsValid;
    }

    public function valNoteDatetime() {
        $boolIsValid = true;

        if( true == is_null( $this->getNoteDatetime() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note_datetime', 'Date is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valNote();
            	$boolIsValid &= $this->valNoteDatetime();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>