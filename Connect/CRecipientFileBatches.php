<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CRecipientFileBatches
 * Do not add any new functions to this class.
 */

class CRecipientFileBatches extends CBaseRecipientFileBatches {

    public static function fetchRecipientFileBatchesAvailableByDocumentRecipientId( $intDocumentRecipientId, $intPage, $intOffset, $objDatabase ) {
    	$strSql = 'SELECT
    					rfb.*
    				FROM
    					recipient_file_batches rfb
    				WHERE
    					rfb.document_recipient_id = ' . ( int ) $intDocumentRecipientId . '
    					AND ( ( rfb.expires_on IS NULL ) OR ( rfb.expires_on >= now() ) )
    					AND rfb.is_published = 1
    					ORDER BY expires_on ';

    	if( true == is_numeric( $intPage ) && true == is_numeric( $intOffset ) ) {
    		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPage;
    	}

    	return self::fetchRecipientFileBatches( $strSql, $objDatabase );
    }

    public static function fetchRecipientFileBatchesExpiredByDocumentRecipientId( $intDocumentRecipientId, $intPage, $intOffset, $objDatabase ) {
    	$strSql = 'SELECT
    					rfb.*
    				FROM
    					recipient_file_batches rfb
    				WHERE
    					rfb.document_recipient_id = ' . ( int ) $intDocumentRecipientId . '
    					AND  rfb.expires_on < now()
    					AND rfb.is_published = 1
    					ORDER BY expires_on DESC ';

    	if( true == is_numeric( $intPage ) && true == is_numeric( $intOffset ) ) {
    		$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPage;
    	}

    	return self::fetchRecipientFileBatches( $strSql, $objDatabase );
    }

    public static function fetchRecipientFileBatchByCidByFileBatchIdByDocumentRecipientId( $intCid, $intFileBatchId, $intDocumentRecipientId, $objDatabase ) {
    	$strSql = 'SELECT
    					rfb.*
    				FROM
    					recipient_file_batches rfb
    				WHERE
    					rfb.cid = ' . ( int ) $intCid . '
						AND rfb.file_batch_id = ' . ( int ) $intFileBatchId . '
    					AND rfb.document_recipient_id = ' . ( int ) $intDocumentRecipientId . '';

    	return self::fetchRecipientFileBatch( $strSql, $objDatabase );
    }

}
?>