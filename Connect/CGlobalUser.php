<?php

use Psi\Libraries\UtilHash\CHash;

class CGlobalUser extends CBaseGlobalUser {

	const TOTAL_LOGIN_ATTEMPTS 		 	= 5;
	const LOGIN_ATTEMPT_WAITING_TIME 	= 1200;	// 20 minutes
	const LOGIN_ATTEMPT_PERIOD			= 300;	// 5 minutes
	const PASSWORD_ROTATION_DAYS  	 	= 180;
	const PASSWORD_ROTATION_NUMBER 	 	= 3;
	const USER_ID_ADMIN					= 2;

	const TOKEN_EXPIRATION = 259200; // 3 days -> 3 * 72 * 60 * 60
	const TOKEN_CID_INDEX = 'cid';
	const TOKEN_ENTRATA_ID_INDEX = 'entrataId';
	const TOKEN_DEVICE_ID_INDEX = 'deviceId';
	const TOKEN_TIMESTAMP_INDEX = 'expiration';
	const TOKEN_GLOBAL_ID_INDEX = 'id';
	const TOKEN_LEASE_ID_INDEX = 'leaseId';
	const TOKEN_PROPERTY_ID_INDEX = 'propertyId';

	const TOKEN_ERROR_CANT_FIND_RESIDENT_USING_TOKEN = 'Unable to find resident with current resident token.';
	const TOKEN_ERROR_CANT_FIND_RESIDENT = 'Invalid username or password.';
	const TOKEN_ERROR_EXPIRED_TOKEN = 'Resident token has expired.';
	const TOKEN_ERROR_INVALID_DEVICE_ID = 'Unable to authenticate resident.';
	const TOKEN_ERROR_INVALID_TOKEN = 'Invalid resident token.';

	protected $m_strEncryptedPassword;
	protected $m_boolIsLoggedIn;
	protected $m_intGlobalUserAuthenticationLogId;
	protected $m_strUnencryptedPassword;

	protected $m_strConfirmPassword;
	protected $m_strCurrentPassword;

    /**
     * Create Functions
     *
     */

    public function createUserAuthenticationLog() {

    	$objGlobalUserAuthenticationLog = new CGlobalUserAuthenticationLog();
    	$objGlobalUserAuthenticationLog->setUserId( $this->getWorksId() );

    	return $objGlobalUserAuthenticationLog;
    }

    /**
     * Set Functions
     *
     */

    public function setIsLoggedIn( $boolIsLoggedIn ) {
    	$this->m_boolIsLoggedIn = ( bool ) $boolIsLoggedIn;
    }

    public function setUnencryptedPassword( $strUnencryptedPassword ) {

    	$this->m_strUnencryptedPassword = trim( $strUnencryptedPassword );
    	$this->m_strPasswordEncrypted 	= CHash::createService()->hashUserOld( trim( $strUnencryptedPassword ) );
    }

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
    	if( true == isset( $arrmixValues['unencrypted_password'] ) ) $this->setUnencryptedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unencrypted_password'] ) : $arrmixValues['unencrypted_password'] );
    	if( true == isset( $arrmixValues['confirm_password'] ) ) $this->setConfirmPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['confirm_password'] ) : $arrmixValues['confirm_password'] );
    	if( true == isset( $arrmixValues['current_password'] ) ) $this->setCurrentPassword( $arrmixValues['current_password'] );

    	return;
    }

    public function setDefaults() {

    	$this->m_intIsSystemUser = 0;
    	$this->m_intIsDisabled = 0;
    }

    public function setSessionExpirationTime( $intSessionExpirationTime ) {
    	$this->m_intSessionExpirationTime = $intSessionExpirationTime;
    }

    public function setGlobalUserAuthenticationLogId( $intGlobalUserAuthenticationLogId ) {
    	$this->m_intGlobalUserAuthenticationLogId = ( int ) $intGlobalUserAuthenticationLogId;
    }

    public function setLoginAttemptData( $boolIsLoggedIn = false ) {

    	if( true == $boolIsLoggedIn ) {
    		$this->setLoginAttemptCount( 0 );
    	} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {
    		$this->setLoginAttemptCount( 1 );
    	} else {
    		$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
    	}

    	$this->setLastLoginAttemptOn( date( 'c', time() ) );

    }

	public function setBluemoonPassword( $strPlainBluemoonPassword ) {
		if( true == valStr( $strPlainBluemoonPassword ) ) {
			$this->setBluemoonPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPlainBluemoonPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

    public function setConfirmPassword( $strConfirmPassword ) {
    	$this->m_strConfirmPassword = CStrings::strTrimDef( $strConfirmPassword, 240, NULL, true );
    }

    public function setCurrentPassword( $strCurrentPassword ) {
    	$this->m_strCurrentPassword = $strCurrentPassword;
    }

	public static function getEncryptedPassword( $strClearTextPassword ) {
		return CHash::createService()->hashCustomer( $strClearTextPassword );
	}

	public static function encryptResidentToken( $intId, $intTimestamp, $strDeviceId, $intWorksId, $intCid, $intLeaseId = NULL, $intPropertyId = NULL ) {

		$arrmixToken = [
			'prefix' => mt_rand( 1000, mt_getrandmax() ),
			'id' => $intId,
			'expiration' => ( $intTimestamp + self::TOKEN_EXPIRATION ),
			'deviceId' => $strDeviceId,
			'entrataId' => $intWorksId,
			'cid' => $intCid,
			'leaseId' => $intLeaseId,
			'propertyId' => $intPropertyId,
			'suffix' => mt_rand( 1000, mt_getrandmax() )
		];

		$strText = json_encode( $arrmixToken );

		return CEncryption::encryptText( $strText, CONFIG_KEY_LOGIN_PASSWORD );
 	}

	public static function decryptResidentToken( $strEncryptedResidentToken ) {

		$strResidentToken = CEncryption::decryptText( $strEncryptedResidentToken, CONFIG_KEY_LOGIN_PASSWORD );

		return json_decode( $strResidentToken, true );
	}

    /**     * Get Functions
     *
     */

    public function getIsLoggedIn() {
    	return $this->m_boolIsLoggedIn;
    }

    public function getGlobalUserAuthenticationLogId() {
    	return $this->m_intGlobalUserAuthenticationLogId;
    }

    public function getSessionExpirationTime() {
    	return $this->m_intSessionExpirationTime;
    }

	public function getBluemoonPassword() {
		if( false == valStr( $this->getBluemoonPasswordEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBluemoonPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

    public function getConfirmPassword() {
    	return $this->m_strConfirmPassword;
    }

    public function getCurrentPassword() {
    	return $this->m_strCurrentPassword;
    }

    /**
     * Validation Functions
     *
     */

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valWorksId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUsername() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogin() {
    	$boolIsValid = true;

    	if( false == valStr( $this->m_strUsername ) || false == valStr( $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unencrypted_password', 'Username and/or password is required.' ) );
    	}

    	return $boolIsValid;
    }

    public function valConfirmPassword() {
    	$boolIsValid = true;

    	if( true == isset( $this->m_strUnencryptedPassword ) && false == isset( $this->m_strConfirmPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Confirmation password is required.' ) );
    		return $boolIsValid;
    	}

    	if( true == isset( $this->m_strUnencryptedPassword ) && 8 > strlen( $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password cannot be less than 8 characters.' ) );
    		return $boolIsValid;
    	}

    	if( true == isset( $this->m_strUnencryptedPassword ) && false == preg_match( '/(?=^.{8,}$)(?=.*[\d|!@#$%^&*]+)(?=.*[A-Z]).*$/', $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must contain at least 1 capital letter AND 1 number or special character.' ) );
    		return $boolIsValid;
    	}

    	if( 0 != strcmp( $this->m_strUnencryptedPassword, $this->m_strConfirmPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Passwords do not match.' ) );
    	}

    	return $boolIsValid;
    }

    public function valCurrentPassword( $objDatabase = NULL ) {
    	$boolIsValid = true;

    	if( false == is_null( $this->getCurrentPassword() ) && 0 == strlen( $this->getCurrentPassword() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', 'Current password is required.' ) );
    	} elseif( true == valStr( $this->getCurrentPassword() ) && 6 > strlen( $this->getCurrentPassword() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', 'Current Password cannot be less than 6 characters.' ) );
    	} else {
    		$objGlobalUser = CGlobalUsers::fetchNaaGlobalUserByUsernameByPasswordEncrypted( $this->m_strUsername, CHash::createService()->hashUserOld( $this->getCurrentPassword() ), $objDatabase );
    		if( false == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
    			$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_password', 'Current password is not correct.' ) );
    		}
    	}

    	return $boolIsValid;
    }

    public function valPassword() {

    	$boolIsValid = true;

    	if( false == is_null( $this->getConfirmPassword() ) && 0 == strlen( $this->getConfirmPassword() ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Password is required.' ) );
    	}

    	if( false == isset( $this->m_strUnencryptedPassword ) || 0 == strlen( $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
    	}

    	if( true == valStr( $this->m_strUnencryptedPassword ) && 8 > strlen( $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password cannot be less than 8 characters.' ) );

    	} elseif( true == valStr( $this->m_strUnencryptedPassword ) && false == preg_match( '/(?=^.{8,}$)(?=.*[\d|!@#$%^&*]+)(?=.*[A-Z]).*$/', $this->m_strUnencryptedPassword ) ) {
    		$boolIsValid = false;
    		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must contain at least 1 capital letter AND 1 number or special character.' ) );
    	}

    	return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
                break;

            case 'validate_login':
            	$boolIsValid = $this->valLogin();
            	break;

            case 'reset_password':
            	$objConnectDatabase = CDatabases::createConnectDatabase( false );
           		$boolIsValid &= $this->valCurrentPassword( $objConnectDatabase );
           		$boolIsValid &= $this->valPassword();
           		if( true == $boolIsValid ) {
           			$boolIsValid &= $this->valConfirmPassword();
           		}
           		break;

           	case 'update_new_password':
				$objConnectDatabase = CDatabases::createConnectDatabase( false );
				$boolIsValid &= $this->valPassword();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valConfirmPassword();
				}
				break;

            default:
           		$boolIsValid = true;
                break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     *
     */

    public function logout() {
    	$this->setIsLoggedIn( false );

     	return true;
    }

    public function mapUserData( $objUser ) {
    	$this->setWorksId( $objUser->getId() );
    	$this->setUsername( $objUser->getUsername() );
    	$this->setPasswordEncrypted( $objUser->getPasswordEncrypted() );
    	$this->setEmailAddress( $objUser->getEmailAddress() );
    	$this->setPhoneNumber( $objUser->getPhoneNumber() );
    	$this->setIsAdministrator( $objUser->getIsAdministrator() );
    	$this->setIsSystemUser( 1 );
    	$this->setIsActiveDirectoryUser( $objUser->getIsActiveDirectory() );
    	$this->setUpdatedBy( $objUser->getUpdatedBy() );
    	$this->setUpdatedOn( $objUser->getUpdatedOn() );
    	$this->setCreatedBy( $objUser->getCreatedBy() );
    	$this->setCreatedOn( $objUser->getCreatedOn() );
    }

    public function systemLogin( $objConnectDatabase, $boolIsFromClientAdmin = false ) {

    	$objGlobalUser = CGlobalUsers::fetchActiveGlobalUserByUsername( $this->getUsername(), $objConnectDatabase );

    	if( false == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
    		$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
    		return false;
    	}

    	// Validate Login with Active Directory
		if( 'production' == CONFIG_ENVIRONMENT && false == $boolIsFromClientAdmin && true == $objGlobalUser->getIsActiveDirectoryUser() ) {

			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );
			$objClientAdminLdap = new CClientAdminLdap();

			$boolIsValid = $objClientAdminLdap->authenticate( $objGlobalUser->getUsername(), $this->m_strUnencryptedPassword );

			if( false == $boolIsValid && -1 == $objClientAdminLdap->getErrorNumber() && $this->getPasswordEncrypted() != $objGlobalUser->getPasswordEncrypted() ) {

				// Unable to connect Active Directory server, so check password from database
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
				return false;

			} elseif( false == $boolIsValid && -1 != $objClientAdminLdap->getErrorNumber() ) {
				// Invalid credentials
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
				return false;
			}

		} elseif( $this->getPasswordEncrypted() != $objGlobalUser->getPasswordEncrypted() ) {

			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
			return false;
		}

		$objGlobalUser->setIsLoggedIn( true );

		$this->m_intId = $objGlobalUser->getId();

		$objGlobalUser->logGlobalUserAuthentication( $objConnectDatabase );

		// $objGlobalUser->checkIsPasswordRotation( $objConnectDatabase );

		$objGlobalUser->setLoginAttemptData( true );

		$objGlobalUser->setSessionExpirationTime( time() + ( 24 * 3600 ) );

		$objGlobalUser->setLastLogin( 'NOW()' );
		$objGlobalUser->setLastAccess( 'NOW()' );

		if( false == $objGlobalUser->update( $this->getId(), $objConnectDatabase ) ) {
			trigger_error( 'User record failed to update for User ID: ' . $this->getId(), E_USER_WARNING );
		}

		return $objGlobalUser;

    }

    public function getOrFetchAssociatedRoleIds( $objDatabase ) {

    	if( false == valArr( $this->m_arrintAssociatedRoleIds ) ) {

    		$arrobjUserRoles = $this->fetchUserRoles( $objDatabase );

    		if( true == valArr( $arrobjUserRoles ) ) {
    			$this->m_arrintAssociatedRoleIds = array_keys( rekeyObjects( 'RoleId', $arrobjUserRoles ) );
    		}
    	}

    	return $this->m_arrintAssociatedRoleIds;
    }

    public function logGlobalUserAuthentication( $objConnectDatabase ) {

    	if( false == is_null( $this->getGlobalUserAuthenticationLogId() ) && true == is_numeric( $this->getGlobalUserAuthenticationLogId() ) ) {
    		$objGlobalUserAuthenticationLog = CGlobalUserAuthenticationLogs::fetchGlobalUserAuthenticationLogByIdByUserId( $this->getGlobalUserAuthenticationLogId(), $this->getWorksId(), $objConnectDatabase );

    		if( true == valObj( $objGlobalUserAuthenticationLog, 'CGlobalUserAuthenticationLog' ) ) {
    			$objGlobalUserAuthenticationLog->setLogoutDateTime( 'NOW()' );
    		}
    	} else {
    		$objGlobalUserAuthenticationLog = $this->createUserAuthenticationLog();
    		$objGlobalUserAuthenticationLog->setLoginDatetime( 'NOW()' );
    	}

    	if( false == is_null( $objGlobalUserAuthenticationLog ) ) {

    		if( false == $objGlobalUserAuthenticationLog->validate( VALIDATE_INSERT ) || false == $objGlobalUserAuthenticationLog->insertOrUpdate( $this->getId(), $objConnectDatabase ) ) {
    			trigger_error( 'Failed to insert or update authentication log for the User : ' . $this->m_intId, E_USER_WARNING );
    		}
    		$this->setGlobalUserAuthenticationLogId( $objGlobalUserAuthenticationLog->getId() );
    	}

    	$objLastUserAuthenticationLog = CGlobalUserAuthenticationLogs::fetchLastGlobalUserAuthenticationLogByUserId( $this->getId(), $objConnectDatabase );

    	if( true == valObj( $objLastUserAuthenticationLog, 'CGlobalUserAuthenticationLog' ) ) {
    		$arrmixParameters = array(
    			'url' => 'http://api.ipinfodb.com/v3/ip-city/?key=' . CConfig::get( 'ipinfodb_api_key' ) . '&ip=' . $objLastUserAuthenticationLog->getIpAddress(),
    			'execution_timeout' => 5
    		);

    		$objExternalRequest = new CExternalRequest();
    		$objExternalRequest->setParameters( $arrmixParameters );

    		unset( $objExternalRequest );
    	}

    	return true;
    }

}
?>