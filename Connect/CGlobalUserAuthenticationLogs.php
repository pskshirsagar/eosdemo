<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CGlobalUserAuthenticationLogs extends CBaseGlobalUserAuthenticationLogs {

	public static function fetchLastGlobalUserAuthenticationLogByUserId( $intUserId, $objConnectDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						global_user_authentication_logs
					WHERE
						user_id = ' . ( int ) $intUserId . '
					ORDER BY
						id DESC
					OFFSET 1 LIMIT 1';

		return self::fetchGlobalUserAuthenticationLog( $strSql, $objConnectDatabase );
	}

	public static function fetchGlobalUserAuthenticationLogByIdByUserId( $intId, $intUserId, $objConnectDatabase ) {
		return self::fetchGlobalUserAuthenticationLog( sprintf( 'SELECT * FROM global_user_authentication_logs WHERE id = %d AND user_id = %d', ( int ) $intId, ( int ) $intUserId ), $objConnectDatabase );
	}

}
?>