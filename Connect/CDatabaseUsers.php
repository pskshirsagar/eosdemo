<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseUsers
 * Do not add any new functions to this class.
 */

class CDatabaseUsers extends CBaseDatabaseUsers {

	public static function fetchPaginatedDatabasesUsersCount( $objDatabase ) {
		$strWhere = ' WHERE deleted_by IS NULL AND deleted_on IS NULL';
		return self::fetchDatabaseUserCount( $strWhere, $objDatabase );
	}

	public static function fetchDatabaseUsersByIds( $arrintDatabaseUserIds, $objDatabase ) {
		return self::fetchDatabaseUsers( sprintf( 'SELECT * FROM %s WHERE deleted_by Is NULL AND deleted_on Is NULL AND id IN( %s )', 'database_users', implode( ',', $arrintDatabaseUserIds ) ), $objDatabase );
	}

	public static function fetchDatabaseUsersById( $intDatabaseUserId, $objDatabase ) {
		return self::fetchDatabaseUser( sprintf( 'SELECT * FROM %s WHERE deleted_by Is NULL AND deleted_on Is NULL AND id = %d', 'database_users', ( int ) $intDatabaseUserId ), $objDatabase );
	}

	public static function fetchDatabaseUserByDatabaseUserTypeId( $intUserTypeId, $objDatabase ) {
		$strSql = ' SELECT * FROM database_users WHERE database_user_type_id = ' . ( int ) $intUserTypeId . ' AND deleted_by IS NULL AND deleted_on IS NULL LIMIT 1';
		return self::fetchDatabaseUser( $strSql, $objDatabase );
	}

	public static function fetchSearchedDatabaseUsers( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (du.id) du.id,
						du.*,
						d.database_name,
						dut.name
					FROM
						database_users AS du
						JOIN database_user_types AS dut ON ( du.database_user_type_id = dut.id )
						LEFT JOIN databases AS d ON ( du.database_id = d.id OR d.database_type_id = any( du.database_type_ids ) )
					WHERE
						du.deleted_by IS NULL
						AND du.deleted_on IS NULL
						AND (
							d.database_name ILIKE \'%' . implode( '%\' AND dut.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							OR dut.name ILIKE \'%' . implode( '%\' AND d.database_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
							)
						AND d.database_name IS NOT NULL
						AND d.deleted_by IS NULL 
						AND d.deleted_on IS NULL
					ORDER BY 
						du.id,
						CASE WHEN database_type_ids IS NULL THEN 1 ELSE 2 END
					LIMIT 15';

		return self::fetchDatabaseUsers( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedDatabaseUsers( $intPageNo = NULL, $intPageSize = NULL, $objConnectDatabase,  $strOrderByField = NULL, $strOrderByType = NULL ) {

		if( true != is_null( $intPageNo ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strPageCondition = ' OFFSET ' . ( int ) $intOffset;
		} else {
			$strPageCondition = '';
		}

		if( true != is_null( $intPageSize ) ) {
			$strPageCondition .= ' LIMIT ' . ( int ) $intPageSize;
		}

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		} else {
			$strOrderBy = ' ORDER BY du.updated_on DESC';
		}

		$strSql = 'SELECT
						du.*,
						d.database_name AS database_name,
						dut.name AS database_user_type_name
					FROM
						database_users AS du
						JOIN database_user_types AS dut ON ( du.database_user_type_id = dut.id )
						LEFT JOIN databases AS d ON ( du.database_id = d.id )
					WHERE
						du.deleted_by IS NULL
						AND du.deleted_on IS NULL
						AND d.database_name IS NOT NULL
						AND d.deleted_by IS NULL 
						AND d.deleted_on IS NULL ' . $strOrderBy . $strPageCondition;

		return self::fetchDatabaseUsers( $strSql, $objConnectDatabase );
	}

}
?>