<?php

class CGlobalUserType extends CBaseGlobalUserType {

	const USER 				= 1;
	const COMPANY_USER 		= 2;
	const BLUEMOON_USER		= 3;
	const CUSTOMER		 	= 4;
	const NAA_USER			= 5;
	const MARKETING_USER	= 6;
	const HA_VENDOR     	= 7;

	// static array of published global user types
	public static $c_arrstrGlobalUserTypes = array(
		self::USER 				=> 'User',
		self::COMPANY_USER 		=> 'Company User',
		self::BLUEMOON_USER		=> 'Bluemoon User',
		self::CUSTOMER		 	=> 'Customer'
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>