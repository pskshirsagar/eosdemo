<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CTableColumns
 * Do not add any new functions to this class.
 */

class CTableColumns extends CBaseTableColumns {

	public static function fetchTableColumnsByTableId( $intTableId, $objConnectDatabase ) {

		$strSql = 'SELECT * FROM table_columns WHERE table_id = ' . ( int ) $intTableId . ' ORDER BY id';
		return self::fetchTableColumns( $strSql, $objConnectDatabase );
	}

	public static function fetchTableColumnsByIds( $arrintTableColumnIds, $objConnectDatabase ) {

		$strSql = 'SELECT * FROM table_columns WHERE id IN ( ' . implode( ', ', $arrintTableColumnIds ) . ' ) ORDER BY id';
		return self::fetchTableColumns( $strSql, $objConnectDatabase );
	}

	public static function fetchTableColumnsByTableIds( $arrintTableIds, $objConnectDatabase ) {

		$strSql = 'SELECT * FROM table_columns WHERE table_id IN (' . implode( ', ', $arrintTableIds ) . ') ORDER BY id ASC ';

		return self::fetchTableColumns( $strSql, $objConnectDatabase );
	}

	public static function fetchPaginatedTableColumnsByTableIdsByColumnName( $intPageNo, $intPageSize, $arrintTableIds, $strTableColumn, $objConnectDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT *	FROM table_columns WHERE table_id IN ( ' . implode( ', ', $arrintTableIds ) . ') AND name = \'' . trim( $strTableColumn ) . '\' ORDER BY id ASC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		return self::fetchTableColumns( $strSql, $objConnectDatabase );
	}

	public static function fetchPaginatedTableColumnsCountByTableIdsByColumnName( $arrintTableIds, $strTableColumn, $objConnectDatabase ) {

		$strWhere = 'WHERE table_id IN (' . implode( ', ', $arrintTableIds ) . ') AND name = \'' . $strTableColumn . '\'';
		return self::fetchTableColumnCount( $strWhere, $objConnectDatabase );
	}

}
?>