<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUsers
 * Do not add any new functions to this class.
 */

class CGlobalUsers extends CBaseGlobalUsers {

	public static function fetchGlobalUserByCidByWorksIdByGlobalUserTypeId( $intCid, $intWorksId, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM global_users WHERE cid = ' . ( int ) $intCid . ' AND works_id = ' . ( int ) $intWorksId . ' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;
		return self::fetchGlobalUser( $strSql, $objDatabase );
	}

	public static function fetchGlobalUsersByCidByWorksIdByGlobalUserTypeId( $intCid, $intWorksId, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM global_users WHERE cid = ' . ( int ) $intCid . ' AND works_id = ' . ( int ) $intWorksId . ' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;
		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchCompetingGlobalUsersCountByUsernameByNonCompetingCompanyUserIdByGlobalUserTypeId( $strUsername, $intNonCompetingCompanyUserId, $intGlobalUserTypeId, $objDatabase ) {

		$strWhereSql  = 'WHERE lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( trim( $strUsername ) ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;
		$strWhereSql .= ( true == is_numeric( $intNonCompetingCompanyUserId ) ) ? ' AND works_id <> ' . ( int ) $intNonCompetingCompanyUserId : '';

		return self::fetchGlobalUserCount( $strWhereSql, $objDatabase );
	}

	public static function fetchGlobalUsersCountByUsernameByNonCompetingGlobalUserIdByGlobalUserTypeId( $strUsername, $intNonCompetingGlobalUserId, $intGlobalUserTypeId, $objDatabase ) {
		$strWhereSql  = 'WHERE lower( username ) = \'' . addslashes( $strUsername ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;
		$strWhereSql .= ( true == is_numeric( $intNonCompetingGlobalUserId ) ) ? ' AND id <> ' . ( int ) $intNonCompetingGlobalUserId : '';

		return self::fetchGlobalUserCount( $strWhereSql, $objDatabase );
	}

	public static function fetchGlobalUsersCountByCidByPropertyIdByUsernameByGlobalUserTypeId( $intCid, $intPropertyId, $strUsername, $intGlobalUserTypeId, $objDatabase ) {
		$strWhereSql  = 'WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strUsername ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;

		return self::fetchGlobalUserCount( $strWhereSql, $objDatabase );
	}

	public static function fetchGlobalUserByCidByPropertyIdByWorksIdByGlobalUserTypeId( $intCid, $intPropertyId, $intWorksId, $intGlobalUserTypeId, $objDatabase ) {
		return self::fetchGlobalUser( 'SELECT * FROM global_users WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND works_id = ' . ( int ) $intWorksId . ' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId, $objDatabase );
	}

	public static function fetchGlobalUserByUsernameByGlobalUserTypeId( $strUsername, $intGlobalUserTypeId, $objDatabase ) {
		return self::fetchGlobalUser( 'SELECT * FROM global_users WHERE lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId, $objDatabase );
	}

	public static function fetchGlobalUserByWorksIdByGlobalUserTypeId( $intWorksId, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM global_users WHERE works_id = ' . ( int ) $intWorksId . ' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;
		return self::fetchGlobalUser( $strSql, $objDatabase );
	}

	public static function fetchPermissionedModuleIds( $objSystemUser, $objDatabase ) {

		if( 1 == $objSystemUser->getIsAdministrator() ) {
			$strSql = 'SELECT id AS system_module_id FROM system_modules WHERE is_published = 1';
		} else {
			// user permissioned modules
			$strSql = '	SELECT
							system_module_id
						FROM
							system_user_permissions
						WHERE
							is_allowed = 1
							AND user_id = ' . ( int ) $objSystemUser->getUserId() . '';
			// public modules
			$strSql .= 'UNION
							SELECT
								id
							FROM
								system_modules
							WHERE
								is_public = 1';
		}

		$arrintResultData = fetchData( $strSql, $objDatabase );

		$arrintModuleIds = array();

		if( true == valArr( $arrintResultData ) ) {
			foreach( $arrintResultData as $arrmixModuleIds ) {
				$arrintModuleIds[$arrmixModuleIds['system_module_id']] = $arrmixModuleIds['system_module_id'];
			}
		}

		return $arrintModuleIds;
	}

	public static function fetchGlobalUsersByUsernameByGlobalUserTypeId( $strUsername, $intGlobalUserTypeId, $objDatabase, $boolIsTenant = false ) {
		return self::fetchGlobalUsers( 'SELECT * FROM global_users WHERE ' . ( ( false === $boolIsTenant ) ? ' property_id IS NOT NULL AND' : '' ) . '  lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId . ' ORDER BY id DESC', $objDatabase );
	}

	public static function fetchGlobalUsersByUsernameByGlobalUserTypeIdEnrolledPassword( $strUsername, $intGlobalUserTypeId, $objDatabase ) {
		return self::fetchGlobalUsers( 'SELECT * FROM global_users WHERE property_id IS NOT NULL AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId . ' AND password_encrypted IS NOT NULL ORDER BY id DESC limit 1', $objDatabase );
	}

	public static function fetchEligibleForgotPasswordGlobalUsersByUsernameByGlobalUserTypeId( $strUsername, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = '
				SELECT * FROM global_users 
				WHERE property_id IS NOT NULL 
				AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\' 
				AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId . ' 
				AND ( password_encrypted IS NOT NULL OR password_encrypted <> \'\' )
				AND is_disabled = 0
				ORDER BY id DESC';

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchGlobalUsersByUsernameByPasswordEncryptedByGlobalUserTypeId( $strUsername, $strPassword, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = '
				SELECT * FROM global_users
				WHERE property_id IS NOT NULL
				AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
				AND password_encrypted = \'' . addslashes( ( string ) $strPassword ) . '\'
				AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchNonEnrolledGlobalUsersByUsernameByGlobalUserTypeId( $strUsername, $intGlobalUserTypeId, $objDatabase ) {
		$strSql = '
				SELECT * FROM global_users
				WHERE property_id IS NOT NULL
				AND lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
				AND ( password_encrypted IS NULL OR password_encrypted = \'\' )
				AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchGlobalUserCountByUserName( $strUserName, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						count(id)
					FROM
						global_users
					WHERE
						lower(username) LIKE  \'%' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strUserName ) ) ) . '\' AND global_user_type_id != ' . CGlobalUserType::CUSTOMER;
		if( $intCid ) {
			$strSql .= '  AND cid IS NOT NULL AND cid != ' . ( int ) $intCid;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );
		return $arrintResponse[0]['count'];
	}

	public static function fetchActiveGlobalUserByUsername( $strUsername, $objConnectDatabase ) {

		$strSql = 'SELECT
						 *
					FROM
						global_users
					WHERE
						(
							(
								is_active_directory_user = 0
								AND username = \'' . addslashes( ( string ) $strUsername ) . '\'
							)
							OR
							(
								is_active_directory_user = 1
								AND LOWER( username ) = LOWER( \'' . addslashes( ( string ) $strUsername ) . '\' )
							)
						)
						AND is_disabled = 0
						AND global_user_type_id != ' . CGlobalUserType::CUSTOMER;

		return self::fetchGlobalUser( $strSql, $objConnectDatabase );
	}

	public static function fetchGlobalUserByIsSystemUserByWorksId( $intWorksId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						global_users
					WHERE
						works_id = ' . ( int ) $intWorksId . '
						AND is_disabled = 0
						AND is_system_user = 1
						AND cid IS NULL
						AND global_user_type_id = ' . CGlobalUserType::USER;

		return self::fetchGlobalUser( $strSql, $objDatabase );
	}

	public static function fetchGlobalUsersByCidByIsAdministrator( $intCid, $boolIsAdministrator, $objConnectDatabase ) {

		$strSql	= 'SELECT
						*
					FROM
						global_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND is_disabled = 0
						AND is_administrator = ' . ( true == $boolIsAdministrator ? '1' : '0' ) . '
						AND global_user_type_id = ' . CGlobalUserType::COMPANY_USER . '
					ORDER BY
					   updated_on DESC';

		return self::fetchGlobalUsers( $strSql, $objConnectDatabase );
	}

	public static function fetchBluemoonPropertyUsersByCidByRwxDomainByLimit( $intCid, $strRwxDomain, $intLimit, $objConnectDatabase ) {
		$strSql = 'SELECT
						username,
						bluemoon_password_encrypted
					FROM
						global_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND bluemoon_password_encrypted is not null
						AND username ilike \'%' . $strRwxDomain . '.com\'
						AND global_user_type_id = ' . CGlobalUserType::COMPANY_USER . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchGlobalUsers( $strSql, $objConnectDatabase );
	}

	public static function fetchBluemoonEnterpriseUsersByCid( $intCid, $objConnectDatabase ) {
		$strSql = 'SELECT
						bluemoon_email_address,
						bluemoon_password_encrypted
					FROM
						global_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND global_user_type_id = ' . CGlobalUserType::BLUEMOON_USER . '
						AND bluemoon_password_encrypted is not null';

		return self::fetchGlobalUsers( $strSql, $objConnectDatabase );
	}

	public static function fetchNaaGlobalUserByUsernameByPasswordEncrypted( $strUsername, $strPassword, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						global_users
					WHERE
						lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
						AND password_encrypted = \'' . addslashes( ( string ) $strPassword ) . '\'
						AND global_user_type_id = ' . CGlobalUserType::NAA_USER;

		return self::fetchGlobalUser( $strSql, $objDatabase );
	}

	public static function fetchNaaGlobalUserByUsername( $strUsername, $objDatabase ) {

		if( false == valStr( $strUsername ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						global_users
					WHERE
						lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
						AND global_user_type_id = ' . CGlobalUserType::NAA_USER;

		return self::fetchGlobalUser( $strSql, $objDatabase );
	}

	public static function fetchGlobalUsersByCidByWorksIdByGlobalUserTypeIds( $intCid, $intWorksId, $arrintGlobalUserTypeIds, $objDatabase ) {

		if( false == valArr( $arrintGlobalUserTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						global_users
					WHERE
						cid = ' . ( int ) $intCid . '
						AND works_id = ' . ( int ) $intWorksId . '
						AND global_user_type_id IN (' . implode( ',', $arrintGlobalUserTypeIds ) . ' )';

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchCountGlobalUserByBluemoonEmailAddressByGlobalUserTypeIdExcludingId( $strBluemoonEmailAddress, $intGlobalUserTypeId, $intId = NULL, $objDatabase ) {

		$strWhereSql = ' WHERE
								bluemoon_email_address = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strBluemoonEmailAddress ) ) . '\'
								AND bluemoon_password_encrypted IS NOT NULL
								AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;

		if( NULL != $intId ) {
			$strWhereSql .= ' AND id <>' . ( int ) $intId;
		}

		return self::fetchGlobalUserCount( $strWhereSql, $objDatabase );
	}

	public static function updateGlobalUsersByCancelledCustomerPortalSettings( $arrstrCancelledCustomerPortalSettings, $objDatabase ) {
		$arrstrGlobalUsersToUpdate = [];
		foreach( $arrstrCancelledCustomerPortalSettings as $arrstrCancelledCustomerPortalSetting ) {
			array_push( $arrstrGlobalUsersToUpdate, pg_escape_string( $arrstrCancelledCustomerPortalSetting['user_data'] ) );
		}

		$strSql = '
				WITH customers AS (
					SELECT
						(string_to_array(myarray.t, \',\'))[1]::integer AS cid,
						(string_to_array(myarray.t, \',\'))[2]::integer AS customer_id,
						(string_to_array(myarray.t, \',\'))[3]::integer AS property_id,
						(string_to_array(myarray.t, \',\'))[4] AS username
					FROM (
						SELECT unnest(
							ARRAY[' .
								'\'' . implode( '\',\'', $arrstrGlobalUsersToUpdate ) . '\'
							]
						) AS t
					) AS myarray
				)
				UPDATE global_users gu
				SET is_disabled = 1
				FROM customers c
				WHERE c.cid = gu.cid
				AND c.customer_id = gu.works_id
				AND c.property_id = gu.property_id
				AND c.username = gu.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function disableGlobalUsersByCidByPropertyIdByCustomerIdAsCommaSeparatedStrings( $arrstrCustomers, $objDatabase ) {
		$arrstrGlobalUsersToDisable = [];
		foreach( $arrstrCustomers as $arrstrCustomer ) {
			array_push( $arrstrGlobalUsersToDisable, pg_escape_string( $arrstrCustomer['user_data'] ) );
		}

		$strSql = '
				WITH customers AS (
					SELECT
						(string_to_array(myarray.t, \',\'))[1]::integer AS cid,
						(string_to_array(myarray.t, \',\'))[2]::integer AS property_id,
						(string_to_array(myarray.t, \',\'))[3]::integer AS customer_id
					FROM (
						SELECT unnest(
							ARRAY[' .
								'\'' . implode( '\',\'', $arrstrGlobalUsersToDisable ) . '\'
							]
						) AS t
					) AS myarray
				)
				UPDATE global_users gu
				SET is_disabled = 1
				FROM customers c
				WHERE c.cid = gu.cid
				AND c.customer_id = gu.works_id
				AND c.property_id = gu.property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteGlobalUsersByCancelledCustomerPortalSettings( $arrstrCancelledCustomerPortalSettings, $objDatabase ) {
		$arrstrGlobalUsersToDelete = [];
		foreach( $arrstrCancelledCustomerPortalSettings as $arrstrCancelledCustomerPortalSetting ) {
			array_push( $arrstrGlobalUsersToDelete, pg_escape_string( $arrstrCancelledCustomerPortalSetting['user_data'] ) );
		}

		$strSql = '
				WITH customers AS (
					SELECT
						(string_to_array(myarray.t, \',\'))[1]::integer AS cid,
						(string_to_array(myarray.t, \',\'))[2]::integer AS customer_id,
						(string_to_array(myarray.t, \',\'))[3]::integer AS property_id,
						(string_to_array(myarray.t, \',\'))[4] AS username
					FROM (
						SELECT unnest(
							ARRAY[' .
			'\'' . implode( '\',\'', $arrstrGlobalUsersToDelete ) . '\'
							]
						) AS t
					) AS myarray
				)
				DELETE FROM global_users gu
				USING customers c
				WHERE c.cid = gu.cid
				AND c.customer_id = gu.works_id
				AND c.property_id = gu.property_id
				AND c.username = gu.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMultiplePropertyGlobalUsersByCids( $objDatabase, $arrintCids, $intLimit = 5000, $intOffset = 0 ) {
		$strSql = '
				SELECT
					cid || \',\' || works_id || \',\' || unnest(property_ids) || \',\' || username AS user_data
				FROM (
					SELECT
						cid, works_id, username, count(distinct property_id), array_agg(property_id) AS property_ids
					FROM global_users
					WHERE global_user_type_id = ' . CGlobalUserType::CUSTOMER . '
					AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
					GROUP BY cid, works_id, username
					HAVING count(distinct property_id) > 1
				) AS t
				LIMIT ' . ( int ) $intLimit . ' OFFSET ' . ( int ) $intOffset;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomGlobalUsersByUsernameByGlobalUserTypeId( $strEmailAddress, $intGlobalUserTypeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						global_users
					WHERE
						lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strEmailAddress ) ) . '\'
						AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId;

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public function fetchGlobalUserIdsByIdByCid( $arrintIds, $intCid, $objDatabase ) {
		// We can change the below query as $intCid might not be required in future implementation if token does not carries cid.
		if( false == valArr( $arrintIds ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						works_id
					FROM 
						global_users
					WHERE 
						id IN( ' . implode( ',', $arrintIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		$arrintWorksId = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintWorksId ) ) {
			return $arrintWorksId[0]['works_id'];
		} else {
			return NULL;
		}
	}

	public static function fetchActiveGlobalUserCidByUserNameByGlobalUserTypeId( $strUsername, $intCustomerId = NULL, $objDatabase, $arrintCids = NULL ) {
		$strCondition = '';
		if( false == valStr( $strUsername ) ) {
			return false;
		}
		if( true == valArr( $arrintCids ) ) {
			$strCondition .= ' AND gu.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';
		}

		$strSql = 'SELECT
						gu.cid,
						gu.works_id
					FROM
						global_users gu
					WHERE
						gu.global_user_type_id = ' . CGlobalUserType::CUSTOMER . '
						AND lower( gu.username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\'
						AND gu.password_encrypted IS NOT NULL
						AND gu.is_disabled = 0' . $strCondition;

		if( true == valId( $intCustomerId ) ) {
			$strSql .= ' AND works_id <> ' . $intCustomerId . '';
		}

		return self::fetchGlobalUsers( $strSql, $objDatabase );
	}

	public static function fetchGlobalUsersByUsernameByGlobalUserTypeIdByCid( $strUsername, $intGlobalUserTypeId, $intCid, $objDatabase , $boolIsTenant = false ) {
		return self::fetchGlobalUsers( 'SELECT * FROM global_users WHERE ' . ( ( false === $boolIsTenant ) ? ' property_id IS NOT NULL AND' : '' ) . ' lower( username ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( ( string ) $strUsername ) ) . '\' AND global_user_type_id = ' . ( int ) $intGlobalUserTypeId . ' AND cid = ' . ( int ) $intCid . ' ORDER BY id DESC', $objDatabase );
	}

}
?>