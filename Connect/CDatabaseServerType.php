<?php

class CDatabaseServerType extends CBaseDatabaseServerType {

	const POSTGRES	= 1;
	const MYSQL		= 2;
	const MSSQL    	= 3;

	public static function getDatabaseServerTypeNameByDatabaseServerTypeId( $intDatabaseServerTypeId ) {

		$strDatabaseServerTypeName = NULL;

		switch( $intDatabaseServerTypeId ) {

			case self::POSTGRES:
				$strDatabaseServerTypeName = 'Postgres';
				break;

			case self::MYSQL:
				$strDatabaseServerTypeName = 'MySql';
				break;

			case self::MSSQL:
				$strDatabaseServerTypeName = 'Microsoft SQL Server';
				break;

			default:
				// default case
				$strDatabaseServerTypeName = 'Postgres';
				break;
		}

		return $strDatabaseServerTypeName;
	}

}
?>