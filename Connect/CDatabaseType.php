<?php

class CDatabaseType extends CBaseDatabaseType {

	const ADMIN                       = 1;
	const CLIENT                      = 2;
	const EMAIL                       = 3;
	const VOIP                        = 4;
	const DNS                         = 5;
	const DBMAIL                      = 6;
	const LIVESUPPORT                 = 8;
	const SMS                         = 9;
	const CONNECT                     = 10;
	const CHAT                        = 11;
	const UTILITIES                   = 12;
	const INSURANCE                   = 13;
	const LOGS                        = 14;
	const BACKUP                      = 15;
	const PAYMENT                     = 16;
	const ZEND_QUEUE                  = 17;
	const COMPETITOR                  = 18;
	const REVENUE                     = 19;
	const SCREENING                   = 21;
	const FREESWITCH                  = 22;
	const VENDORS                     = 23;
	const TEST_CASE                   = 26;
	const INTERNALTECHNICALASSESSMENT = 27;
	const BEACONBOX                   = 28;
	const MESSAGING                   = 29;
	const BIXPRESS                    = 30;
	const SSISLOG                     = 31;
	const DEPLOY                      = 32;
	const HA                          = 33;
	const ANALYTIC                    = 34;
	const DB_COMMON                   = 35;
	const INSTITUTION                 = 36;
	const MOODLE                      = 37;
	const ENTRATAFI                   = 38;
	const INSIGHTS                    = 39;
	const SSO                         = 40;

	public static $c_arrintNotRequiredDatabaseTypesForDatabaseDeployments = [
		self::MESSAGING                   => 'Messaging',
		self::SSISLOG                     => 'Ssislog',
		self::BIXPRESS                    => 'BixPress',
		self::BACKUP                      => 'Backup',
		self::INTERNALTECHNICALASSESSMENT => 'InternalTechnicalAssessment',
		self::REVENUE                     => 'Revenue',
		self::DBMAIL                      => 'DbMail',
		self::ZEND_QUEUE                  => 'ZendQueue',
		self::LIVESUPPORT                 => 'LiveSupport',
		self::VENDORS                     => 'Vendors'
	];

	public static $c_arrintNotRequiredDatabaseTypesForDbCommon = [
		self::MESSAGING                   => 'Messaging',
		self::BEACONBOX                   => 'Beaconbox',
		self::SSISLOG                     => 'Ssislog',
		self::BIXPRESS                    => 'BixPress',
		self::BACKUP                      => 'Backup',
		self::INTERNALTECHNICALASSESSMENT => 'InternalTechnicalAssessment',
		self::REVENUE                     => 'Revenue',
		self::DBMAIL                      => 'DbMail',
		self::ZEND_QUEUE                  => 'ZendQueue',
		self::LIVESUPPORT                 => 'LiveSupport',
		self::VENDORS                     => 'Vendors',
		self::PAYMENT                     => 'Payment',
		self::COMPETITOR                  => 'Competitor',
		self::TEST_CASE                   => 'TestCase',
		self::ANALYTIC                    => 'Analytic',
		self::INSTITUTION                 => 'Institution',
		self::INSIGHTS                    => 'Insights',
		self::SSO                         => 'Sso'
	];

	public static $c_arrintCheckSimultaneousConnectionDatabaseTypes = [
		CDatabaseType::EMAIL
	];

	public static function getDatabaseNames( $boolIsName = true, $boolIsIncludeDbCommon = false ) {
		$arrstrDatabaseNames[self::ADMIN]       = ( false !== $boolIsName ) ? 'Admin' : 'admin';
		$arrstrDatabaseNames[self::CLIENT]      = ( false !== $boolIsName ) ? 'Entrata' : 'entrata';
		$arrstrDatabaseNames[self::CONNECT]     = ( false !== $boolIsName ) ? 'Connect' : 'connect';
		$arrstrDatabaseNames[self::CHAT]        = ( false !== $boolIsName ) ? 'Chat' : 'chats';
		$arrstrDatabaseNames[self::COMPETITOR]  = ( false !== $boolIsName ) ? 'Competitor' : 'competitor';
		$arrstrDatabaseNames[self::DEPLOY]      = ( false !== $boolIsName ) ? 'Deploy' : 'deploy';
		$arrstrDatabaseNames[self::DNS]         = ( false !== $boolIsName ) ? 'Dns' : 'dns';
		$arrstrDatabaseNames[self::EMAIL]       = ( false !== $boolIsName ) ? 'Emails' : 'emails';
		$arrstrDatabaseNames[self::FREESWITCH]  = ( false !== $boolIsName ) ? 'Freeswitch' : 'freeswitch';
		$arrstrDatabaseNames[self::INSURANCE]   = ( false !== $boolIsName ) ? 'Insurance' : 'insurance';
		$arrstrDatabaseNames[self::LOGS]        = ( false !== $boolIsName ) ? 'Logs' : 'logs';
		$arrstrDatabaseNames[self::MESSAGING]   = ( false !== $boolIsName ) ? 'Messaging' : 'messaging';
		$arrstrDatabaseNames[self::PAYMENT]     = ( false !== $boolIsName ) ? 'Payments' : 'payments';
		$arrstrDatabaseNames[self::REVENUE]     = ( false !== $boolIsName ) ? 'Revenue' : 'dm_revenue_management';
		$arrstrDatabaseNames[self::BIXPRESS]    = ( false !== $boolIsName ) ? 'BIX' : 'BIxPress';
		$arrstrDatabaseNames[self::SSISLOG]     = ( false !== $boolIsName ) ? 'SSIS' : 'ssis_log';
		$arrstrDatabaseNames[self::SMS]         = ( false !== $boolIsName ) ? 'Sms' : 'sms';
		$arrstrDatabaseNames[self::SCREENING]   = ( false !== $boolIsName ) ? 'Screening' : 'screening';
		$arrstrDatabaseNames[self::TEST_CASE]   = ( false !== $boolIsName ) ? 'TestCase' : 'test_cases';
		$arrstrDatabaseNames[self::UTILITIES]   = ( false !== $boolIsName ) ? 'Utilities' : 'utilities';
		$arrstrDatabaseNames[self::VOIP]        = ( false !== $boolIsName ) ? 'Voip' : 'voip';
		$arrstrDatabaseNames[self::ZEND_QUEUE]  = ( false !== $boolIsName ) ? 'ZendQueue' : 'zend_queue';
		$arrstrDatabaseNames[self::HA]          = ( false !== $boolIsName ) ? 'HA' : 'ha';
		$arrstrDatabaseNames[self::BEACONBOX]   = ( false !== $boolIsName ) ? 'BeaconBox' : 'beaconbox';
		$arrstrDatabaseNames[self::ANALYTIC]    = ( false !== $boolIsName ) ? 'Analytic' : 'analytics';
		$arrstrDatabaseNames[self::INSTITUTION] = ( false !== $boolIsName ) ? 'Institution' : 'institution';
		$arrstrDatabaseNames[self::MOODLE]      = ( false !== $boolIsName ) ? 'Moodle' : 'moodle';
		$arrstrDatabaseNames[self::ENTRATAFI]   = ( false !== $boolIsName ) ? 'Entratafi' : 'entratafi';
		$arrstrDatabaseNames[self::INSIGHTS]    = ( false !== $boolIsName ) ? 'Insights' : 'insights';
		$arrstrDatabaseNames[self::SSO]         = ( false !== $boolIsName ) ? 'Sso' : 'sso';

		if( true == $boolIsName && true == $boolIsIncludeDbCommon ) {
			$arrstrDatabaseNames[CDatabaseType::DB_COMMON] = 'DbCommon';
		}

		return $arrstrDatabaseNames;
	}

	public static function getDatabaseEosFolderNames() {
		$arrstrDatabaseNames[self::ADMIN]       = 'Admin';
		$arrstrDatabaseNames[self::CLIENT]      = 'Entrata';
		$arrstrDatabaseNames[self::CONNECT]     = 'Connect';
		$arrstrDatabaseNames[self::CHAT]        = 'Chats';
		$arrstrDatabaseNames[self::COMPETITOR]  = 'Competitor';
		$arrstrDatabaseNames[self::DEPLOY]      = 'Deploy';
		$arrstrDatabaseNames[self::EMAIL]       = 'Email';
		$arrstrDatabaseNames[self::FREESWITCH]  = 'Freeswitch';
		$arrstrDatabaseNames[self::INSURANCE]   = 'Insurance';
		$arrstrDatabaseNames[self::LOGS]        = 'Logs';
		$arrstrDatabaseNames[self::MESSAGING]   = 'Messaging';
		$arrstrDatabaseNames[self::PAYMENT]     = 'Payment';
		$arrstrDatabaseNames[self::SMS]         = 'Sms';
		$arrstrDatabaseNames[self::SCREENING]   = 'Screening';
		$arrstrDatabaseNames[self::TEST_CASE]   = 'TestCase';
		$arrstrDatabaseNames[self::UTILITIES]   = 'Utilities';
		$arrstrDatabaseNames[self::VOIP]        = 'Voip';
		$arrstrDatabaseNames[self::HA]          = 'HA';
		$arrstrDatabaseNames[self::INSTITUTION] = 'Institution';
		$arrstrDatabaseNames[self::MOODLE]      = 'Moodle';
		$arrstrDatabaseNames[self::SSO]         = 'Sso';
        $arrstrDatabaseNames[self::INSIGHTS]    = 'Insights';

		return $arrstrDatabaseNames;
	}

}
?>