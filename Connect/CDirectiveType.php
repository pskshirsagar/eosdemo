<?php

class CDirectiveType extends CBaseDirectiveType {

	const CLIENT 				= 1;
	const WEBSITE_SUB_DOMAIN 	= 2;
	const WEBSITE_FULL_DOMAIN 	= 3;
}
?>