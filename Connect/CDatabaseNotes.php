<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseNotes
 * Do not add any new functions to this class.
 */

class CDatabaseNotes extends CBaseDatabaseNotes {

	public static function fetchPaginatedDatabaseNotesByDatabaseId( $intPageNo, $intPageSize, $intDatabaseId, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit = ( int ) $intPageSize;

    	$strSql = ' SELECT
    					*
    				FROM
    					database_notes
    				WHERE
						database_id = ' . $intDatabaseId . '
					ORDER BY id DESC
    			   	OFFSET ' . ( int ) $intOffset . '
    			   	LIMIT ' . $intLimit;

    	return self::fetchDatabaseNotes( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDatabasesNotesCountByDatabaseId( $intDatabaseId, $objDatabase ) {
		$strWhere = 'WHERE database_id = ' . ( int ) $intDatabaseId;
    	return self::fetchDatabaseNoteCount( $strWhere, $objDatabase );
	}
}
?>