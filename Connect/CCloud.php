<?php

class CCloud extends CBaseCloud {

	const REFERENCE_ID_LINDON = '1';
	const REFERENCE_ID_IRELAND = 'vpc-0c8836da8bf1fbe1a';
	const REFERENCE_ID_CHINA = 'vpc-uf6o5flirrzdu9acat4sk';  // Change this constant as per production clouds table value.

	const LINDON_ID = 1;
	const IRELAND_ID = 2;
	const CHINA_ID = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProvider() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRegion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDomain() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTimezone() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTimezoneDifference() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeploymentTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>