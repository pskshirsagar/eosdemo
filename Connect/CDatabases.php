<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabases
 * Do not add any new functions to this class.
 */

class CDatabases extends CBaseDatabases {

	const ENTRATA_DATABASE_PREFIX = 'entrata';

	// CONFIG_DB_DNS_FAILOVER : Load Databases through DNS or IP address (0 from DNS and 1 from IP address)

	/**
	 * @return CDatabase[]
	 */
	public static function fetchDatabases( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, \CDatabase::class, $objDatabase, DATA_CACHE_MEMORY, 3600, true, true, true );
	}

	/**
	 * @return CDatabase
	 */
	public static function fetchDatabase( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, \CDatabase::class, $objDatabase, DATA_CACHE_MEMORY, 3600, true, true );
	}

	public static function createConnectDatabase( $boolRandomizeConnections = true, $boolOpenConnection = true ) {
		$strDbHost		= CONFIG_DB_HOST_CONNECT;
		$strDnsHandle	= NULL;

		if( true == defined( 'CONFIG_DB_DNS_HANDLE_CONNECT' ) && false == is_null( CONFIG_DB_DNS_HANDLE_CONNECT ) ) {
			$strDnsHandle = CONFIG_DB_DNS_HANDLE_CONNECT;
		}

		if( true == defined( 'CONFIG_DB_DNS_FAILOVER' ) && 0 == CONFIG_DB_DNS_FAILOVER && true == $boolRandomizeConnections && true == defined( 'CONFIG_DB_DNS_HANDLE_ALTERNATES_CONNECT' ) && false == is_null( CONFIG_DB_DNS_HANDLE_ALTERNATES_CONNECT ) ) {

			$arrstrAlternateDnsHandles = explode( ',', CONFIG_DB_DNS_HANDLE_ALTERNATES_CONNECT );

			foreach( $arrstrAlternateDnsHandles as $intKey => $strAlternateDnsHandle ) {
				$arrstrAlternateDnsHandles[$intKey] = trim( $strAlternateDnsHandle );
			}
			// $strDnsHandle = $arrstrAlternateDnsHandles[rand( 0, ( count( $arrstrAlternateDnsHandles ) - 1 ) )];
		} elseif( true == defined( 'CONFIG_DB_DNS_FAILOVER' ) && 1 == CONFIG_DB_DNS_FAILOVER && true == $boolRandomizeConnections && true == defined( 'CONFIG_DB_HOST_ALTERNATES' ) && false == is_null( CONFIG_DB_HOST_ALTERNATES ) ) {

			$arrstrAlternateDbHosts = explode( ',', CONFIG_DB_HOST_ALTERNATES );

			foreach( $arrstrAlternateDbHosts as $intKey => $strAlternateDbHost ) {
				$arrstrAlternateDbHosts[$intKey] = trim( $strAlternateDbHost );
			}

			// $strDbHost = $arrstrAlternateDbHosts[rand( 0, ( count( $arrstrAlternateDbHosts ) - 1 ) )];
		}

		$objDatabase = new CDatabase();
		$objDatabase->setId( CDatabase::CONNECT );
		$objDatabase->setDatabaseTypeId( CDatabaseType::CONNECT );
		$objDatabase->setDatabaseServerTypeId( CDatabaseServerType::POSTGRES );
		$objDatabase->setMachineName( $strDbHost );
		$objDatabase->setIpAddress( $strDbHost );
		$objDatabase->setDnsHandle( $strDnsHandle );
		$objDatabase->setPort( CONFIG_DB_PORT_CONNECT );
		$objDatabase->setDatabaseName( CONFIG_DB_NAME_CONNECT );
		$objDatabase->setUsernameEncrypted( CONFIG_DB_USER_CONNECT );
		$objDatabase->setPasswordEncrypted( CONFIG_DB_PASS_CONNECT );

		if( true == $objDatabase->validate( 'validate_open' ) ) {
			if( true == $boolOpenConnection ) {
				if( true == is_null( $objDatabase->open() ) ) {
					return NULL;
				}
			}

			return $objDatabase;
		} else {
			throw new ErrorException( 'Database Error: Invalid connect database credentials.', E_USER_ERROR );
			return false;
		}
	}

	public static function createIrenlandConnectDatabase( $objDatabase, $boolOpenConnection = true ) {

		$strSql = 'SELECT 
						*
					FROM
						databases 
					WHERE 
						cloud_id = ' . ( int ) CCloud::IRELAND_ID . '
					AND database_type_id = ' . ( int ) CDatabaseType::CONNECT . ' LIMIT 1';

		$objConnectDatabase = self::fetchDatabase( $strSql, $objDatabase );

		$objConnectDatabase->setUsernameEncrypted( CONFIG_DB_USER_CONNECT );
		$objConnectDatabase->setPasswordEncrypted( CONFIG_DB_PASS_CONNECT );

		if( true == $objConnectDatabase->validate( 'validate_open' ) ) {
			if( true == $boolOpenConnection ) {
				if( true == is_null( $objConnectDatabase->open() ) ) {
					return NULL;
				}
			}

			return $objConnectDatabase;
		} else {
			trigger_error( 'Database Error: Invalid connect database credentials.', E_USER_ERROR );
			return false;
		}
	}

	public static function createDeployDatabase( $boolRandomizeConnections = true, $boolOpenConnection = true ) {

		$strDbHost		= CONFIG_DB_HOST_DEPLOY;
		$strDnsHandle	= CONFIG_DB_DNS_HANDLE_DEPLOY;

		if( true == defined( 'CONFIG_DB_DNS_FAILOVER' ) && 0 == CONFIG_DB_DNS_FAILOVER && true == $boolRandomizeConnections && true == defined( 'CONFIG_DB_DNS_HANDLE_ALTERNATES_DEPLOY' ) && false == is_null( CONFIG_DB_DNS_HANDLE_ALTERNATES_DEPLOY ) ) {

			$arrstrAlternateDnsHandles = explode( ',', CONFIG_DB_DNS_HANDLE_ALTERNATES_DEPLOY );

			foreach( $arrstrAlternateDnsHandles as $intKey => $strAlternateDnsHandle ) {
				$arrstrAlternateDnsHandles[$intKey] = trim( $strAlternateDnsHandle );
			}
			$strDnsHandle = $arrstrAlternateDnsHandles[rand( 0, ( \Psi\Libraries\UtilFunctions\count( $arrstrAlternateDnsHandles ) - 1 ) )];
		} elseif( true == defined( 'CONFIG_DB_DNS_FAILOVER' ) && 1 == CONFIG_DB_DNS_FAILOVER && true == $boolRandomizeConnections && true == defined( 'CONFIG_DB_HOST_ALTERNATES_DEPLOY' ) && false == is_null( CONFIG_DB_HOST_ALTERNATES_DEPLOY ) ) {

			$arrstrAlternateDbHosts = explode( ',', CONFIG_DB_HOST_ALTERNATES_DEPLOY );

			foreach( $arrstrAlternateDbHosts as $intKey => $strAlternateDbHost ) {
				$arrstrAlternateDbHosts[$intKey] = trim( $strAlternateDbHost );
			}

			$strDbHost = $arrstrAlternateDbHosts[rand( 0, ( \Psi\Libraries\UtilFunctions\count( $arrstrAlternateDbHosts ) - 1 ) )];
		}

		$objDatabase = new CDatabase();
		$objDatabase->setId( CDatabase::DEPLOY );
		$objDatabase->setDatabaseTypeId( CDatabaseType::DEPLOY );
		$objDatabase->setDatabaseServerTypeId( CDatabaseServerType::POSTGRES );
		$objDatabase->setMachineName( $strDbHost );
		$objDatabase->setIpAddress( $strDbHost );
		$objDatabase->setDnsHandle( $strDnsHandle );
		$objDatabase->setPort( CONFIG_DB_PORT_DEPLOY );
		$objDatabase->setDatabaseName( CONFIG_DB_NAME_DEPLOY );
		$objDatabase->setUsernameEncrypted( CONFIG_DB_USER_DEPLOY );
		$objDatabase->setPasswordEncrypted( CONFIG_DB_PASS_DEPLOY );

		if( true == $objDatabase->validate( 'validate_open' ) ) {
			if( true == $boolOpenConnection ) {
				if( true == is_null( $objDatabase->open() ) ) {
					return NULL;
				}
			}

			return $objDatabase;
		} else {
			trigger_error( 'Database Error: Invalid deploy database credentials.', E_USER_ERROR );
			return false;
		}
	}

	public static function createGlobalDeployDatabase() {

		$objDatabase = new CDatabase();
		$objDatabase->setId( CDatabase::DEPLOY );
		$objDatabase->setDatabaseTypeId( CDatabaseType::DEPLOY );
		$objDatabase->setDatabaseServerTypeId( CDatabaseServerType::POSTGRES );
		$objDatabase->setMachineName( CONFIG_DB_HOST_GLOBAL_DEPLOY );
		$objDatabase->setIpAddress( CONFIG_DB_HOST_GLOBAL_DEPLOY );
		$objDatabase->setPort( CONFIG_DB_PORT_GLOBAL_DEPLOY );
		$objDatabase->setDatabaseName( CONFIG_DB_NAME_GLOBAL_DEPLOY );
		$objDatabase->setUsernameEncrypted( CONFIG_DB_USER_GLOBAL_DEPLOY );
		$objDatabase->setPasswordEncrypted( CONFIG_DB_PASS_GLOBAL_DEPLOY );

		if( true == $objDatabase->validate( 'validate_open' ) ) {
			if( true == is_null( $objDatabase->open() ) ) {
				return NULL;
			}

			return $objDatabase;
		} else {
			trigger_error( 'Database Error: Invalid connect database credentials.', E_USER_ERROR );
			return false;
		}
	}

	public static function createSlaveDeployDatabase() {

		$objDatabase = new CDatabase();
		$objDatabase->setId( CDatabase::DEPLOY );
		$objDatabase->setDatabaseTypeId( CDatabaseType::DEPLOY );
		$objDatabase->setDatabaseServerTypeId( CDatabaseServerType::POSTGRES );
		$objDatabase->setMachineName( CONFIG_DB_HOST_SLAVE_DEPLOY );
		$objDatabase->setIpAddress( CONFIG_DB_HOST_SLAVE_DEPLOY );
		$objDatabase->setDnsHandle( CONFIG_DB_DNS_HANDLE_SLAVE_DEPLOY );
		$objDatabase->setPort( CONFIG_DB_PORT_DEPLOY );
		$objDatabase->setDatabaseName( CONFIG_DB_NAME_DEPLOY );
		$objDatabase->setUsernameEncrypted( CONFIG_DB_USER_DEPLOY );
		$objDatabase->setPasswordEncrypted( CONFIG_DB_PASS_DEPLOY );

		if( true == $objDatabase->validate( 'validate_open' ) ) {

			if( true == is_null( $objDatabase->open() ) ) {
				return NULL;
			}

			return $objDatabase;
		} else {
			trigger_error( 'Database Error: Invalid deploy database credentials.', E_USER_ERROR );
			return false;
		}
	}

	// This function is created to load all the master databases ( non client ) databases to get loaded from config file instead of connect database.
	// This will only apply to production as we will still use connect database for local and stagging.

	public static function loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( $intDatabaseTypeId, $intDatabaseUserTypeId, $intDatabaseServerTypeId = CDatabaseServerType::POSTGRES, $boolOpenConnection = true, $intConnectionTimeout = 0, $boolThrowException = false ) {
		return \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( $intDatabaseTypeId, $intDatabaseUserTypeId, $intDatabaseServerTypeId, $boolOpenConnection, $intConnectionTimeout, $boolThrowException );
	}

	public static function fetchCachedDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $intDatabaseServerTypeId = 1, $boolOpenConnection = true, $intConnectionTimeout = 0 ) {
		$objDatabase = CCache::fetchObject( 'application_databases_' . $intDatabaseTypeId . '_' . $intDatabaseUserTypeId . '_' . $intDatabaseServerTypeId );

		if( false === $objDatabase || false == valObj( $objDatabase, 'CDatabase' ) ) {

			$objConnectDatabase = self::createConnectDatabase();
			$objDatabase = self::fetchDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $intDatabaseServerTypeId );
			$objConnectDatabase->close();

			CCache::storeObject( 'application_databases_' . $intDatabaseTypeId . '_' . $intDatabaseUserTypeId . '_' . $intDatabaseServerTypeId, $objDatabase, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		$objDatabase->setConnectionTimeout( $intConnectionTimeout );

		if( true == $boolOpenConnection ) {
			if( false == $objDatabase->validateAndOpen() ) {
				return NULL;
			}
		}

		return $objDatabase;
	}

	public static function fetchCachedSlaveDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $intDatabaseServerTypeId = 1, $boolOpenConnection = false, $intConnectionTimeout = 0 ) {

		$objDatabase = CCache::fetchObject( 'application_slave_databases_' . $intDatabaseTypeId . '_' . $intDatabaseUserTypeId . '_' . $intDatabaseServerTypeId );

		if( false === $objDatabase || false == valObj( $objDatabase, 'CDatabase' ) ) {

			$objConnectDatabase = self::createConnectDatabase();
			$objDatabase = self::fetchDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $intDatabaseServerTypeId, false );
			$objConnectDatabase->close();

			CCache::storeObject( 'application_slave_databases_' . $intDatabaseTypeId . '_' . $intDatabaseUserTypeId . '_' . $intDatabaseServerTypeId, $objDatabase, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$objDatabase->setConnectionTimeout( $intConnectionTimeout );
		}

		if( true == $boolOpenConnection ) {
			if( false == $objDatabase->validateAndOpen() ) {
				return NULL;
			}
		}

		return $objDatabase;
	}

	public static function loadCachedDatabaseLagTime( $objDatabase, $intSpecificLifetime = 120 ) {
		$intLagTime = NULL;

		if( !$objDatabase || false == valObj( $objDatabase, 'CDatabase' ) ) {
			return NULL;
		}

		$intLagTime = CCache::fetchObject( 'db_lag_' . $objDatabase->getDatabaseName() );

		if( $intLagTime ) {
			return $intLagTime;
		}

		$strSql = ' SELECT
						CASE 
							WHEN pg_last_wal_receive_lsn() = pg_last_wal_replay_lsn() THEN 0 
							ELSE ROUND((EXTRACT (EPOCH FROM NOW() - lagtime))/60)
						END AS lag_delay
					FROM 
						lagcheck;';

		if( !$objDatabase->getIsOpen() ) {
			$objDatabase->open();
		}
		$arrmixDatabaseLagTime = fetchData( $strSql, $objDatabase );

		if( false == empty( $arrmixDatabaseLagTime[0]['lag_delay'] ) ) {
			$intLagTime = $arrmixDatabaseLagTime[0]['lag_delay'];
		}

		CCache::storeObject( 'db_lag_' . $objDatabase->getDatabaseName(), $intLagTime, $intSpecificLifetime, $arrstrTags = [] );

		return $intLagTime;
	}

	public static function fetchCachedClientDatabaseByDatabaseId( $intDatabaseId, $intDefaultDatabaseUserTypeId = NULL ) {

		$objDatabase = NULL;
		if( is_null( $intDefaultDatabaseUserTypeId ) ) $intDefaultDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER;
		$objDatabase = CCache::fetchObject( 'CDatabase_' . $intDatabaseId . '_' . $intDefaultDatabaseUserTypeId );

		if( false === $objDatabase || false == valObj( $objDatabase, 'CDatabase' ) ) {

			$objConnectDatabase	= self::createConnectDatabase();
			$objDatabase		= self::fetchDatabaseByDatabaseIdByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseId, $intDefaultDatabaseUserTypeId, CDatabaseType::CLIENT, $objConnectDatabase );
			$objConnectDatabase->close();

			CCache::storeObject( 'CDatabase_' . $intDatabaseId . '_' . $intDefaultDatabaseUserTypeId, $objDatabase, $intSpecificLifetime = 3600, $arrstrTags = [] );
		}

		return $objDatabase;
	}

	public static function createDatabase( $intDatabaseUserTypeId, $intDatabaseTypeId, $intDatabaseServerTypeId = CDatabaseServerType::POSTGRES ) {

		$objConnectDatabase = self::createConnectDatabase();

		$objDatabase = self::fetchDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $intDatabaseServerTypeId );

		$objConnectDatabase->close();

		if( false == is_null( $objDatabase ) && true == $objDatabase->validate( 'validate_open' ) ) {
			if( false == $objDatabase->open() ) {
				return NULL;
			}
			return $objDatabase;
		} else {
			trigger_error( 'Database Error: Invalid database credentials for database type ' . ( int ) $intDatabaseTypeId, E_USER_ERROR );
			return false;
		}
	}

	public static function fetchCommonClientDatabaseByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT DISTINCT(du.database_user_type_id) as database_user_type_id, ds.*, du.username_encrypted, du.password_encrypted, du.database_id FROM databases ds, database_users du WHERE ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) ) AND ds.deleted_on IS NULL AND du.deleted_on IS NULL AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . ' AND ds.is_test_database = 0 AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true, $strOrderBy = 'id', $boolFetchTestDatabases = false, $boolFetchTemplateDatabases = false ) {

		$strSql = 'SELECT
                        DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( false == $boolFetchTestDatabases || 'production' != CONFIG_ENVIRONMENT ) {
			$strSql .= ' AND ds.is_test_database = 0 ';
		}
		if( false == $boolFetchTemplateDatabases ) {
			$strSql .= ' AND ds.is_template = false ';
		}
		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( true == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ds.id ';
		} elseif( 'database_name' == $strOrderBy ) {
			$strSql .= ' ORDER BY ds.id,
								NULLIF( regexp_replace ( ds.database_name, E\'\\\\D\', \'\', \'g\' ), \'\' )::BIGINT,
								ds.database_name';
		} elseif( 'id' != $strOrderBy ) {
			$strSql .= ' ORDER BY ds.id, ds.' . $strOrderBy;
		} else {
			$strSql .= ' ORDER BY ds.id';
		}

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseIds( $intDatabaseUserTypeId, $arrintDatabaseIds, $objConnectDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)  d.id,
						d.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases d
						JOIN database_users du ON ( d.id = du.database_id OR d.database_type_id = any( du.database_type_ids ) )
					WHERE
						d.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.is_test_database = 0
						AND d.database_id IS NULL
						AND d.id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					ORDER BY
						d.id, du.database_id NULLS last ';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseIds( $arrintDatabaseIds, $objConnectDatabase ) {
		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						d.*
					FROM
						databases d
					WHERE
						d.deleted_on IS NULL
						AND d.is_test_database = 0
						AND d.database_id IS NULL
						AND d.id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					ORDER BY
						d.id';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseByDatabaseIdByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseId, $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true, $intClusterId = NULL ) {

		$strSql = 'SELECT DISTINCT ON (ds.id)  ds.id, ds.*, du.database_user_type_id, du.username_encrypted, du.password_encrypted FROM databases ds, database_users du WHERE ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) ) AND ds.deleted_on IS NULL AND du.deleted_on IS NULL AND ds.id = ' . ( int ) $intDatabaseId . ' AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . ' AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}
		if( false == is_null( $intClusterId ) ) {
			$strSql .= ' AND cluster_id = ' . ( int ) $intClusterId;
		}
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $intDatabaseServerTypeId = 1, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT ds.*, du.database_user_type_id, du.username_encrypted, du.password_encrypted, du.database_id FROM databases ds, database_users du WHERE ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) ) AND ds.deleted_on IS NULL AND du.deleted_on IS NULL AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . ' AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . ' AND ds.database_server_type_id =' . ( int ) $intDatabaseServerTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY du.database_id NULLS last LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseTypeId, $intDatabaseUserTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true, $boolFetchTestDatabases = true ) {
		$strSql = '	SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( false === $boolFetchTestDatabases ) {
			$strSql .= ' AND ds.is_test_database = 0 ';
		}

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last ';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchPaginatedDatabases( $intPageNo, $intPageSize, $objConnectDatabase, $boolFetchMasterDatabases = true ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = ' SELECT
						*
					FROM
						databases
					WHERE
						deleted_by IS NULL AND
						deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY order_num
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchPaginatedDatabasesCount( $objConnectDatabase, $boolFetchMasterDatabases = true ) {

		$strWhere = 'WHERE deleted_by IS NULL AND deleted_on IS NULL';

		if( true == $boolFetchMasterDatabases ) {
			$strWhere .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strWhere .= ' AND database_id IS NOT NULL ';
		}

		return self::fetchDatabaseCount( $strWhere, $objConnectDatabase );
	}

	public static function fetchAllDatabases( $objConnectDatabase, $boolIncludingDeleted = false ) {

		if( false == $boolIncludingDeleted ) {
			$strSql = 'SELECT * FROM databases WHERE deleted_on IS NULL AND deleted_by IS NULL AND database_id IS NULL ORDER BY database_name ';
		} else {
			$strSql = 'SELECT * FROM databases WHERE database_id IS NULL ORDER BY database_name ';
		}

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchAllMasterSlaveDatabases( $objConnectDatabase ) {

		$strSql = 'SELECT * FROM databases WHERE deleted_on IS NULL AND deleted_by IS NULL ORDER BY database_name ';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseByIds( $arrintDatabaseIds, $objDatabase ) {
		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT * FROM databases WHERE id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByIdsByDatabaseUserTypeIdByDatabaseTypeId( $arrintDatabaseIds, $intDatabaseUserTypeId, $intDatabaseTypeId, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
						ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByIdsByDatabaseUserTypeIdByDatabaseTypeIdByClusterId( $arrintDatabaseIds, $intDatabaseUserTypeId, $intDatabaseTypeId, $intClusterId, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
						AND ds.cluster_id = ' . ( int ) $intClusterId;
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByIdByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseId, $intDatabaseUserTypeId, $intDatabaseTypeId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.id =' . ( int ) $intDatabaseId;
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabase( $strSql, $objDatabase );
	}

	public static function fetchClientDatabasesByCIdByDatabaseUserTypeIdByDatabaseTypeId( $intCid, $intDatabaseUserTypeId, $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true ) {

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						INNER JOIN directives d ON ( d.database_id = ds.id )
					WHERE
						d.cid = ' . ( int ) $intCid . '
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last ';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchClientDatabasesByCIdsByDatabaseUserTypeIdByDatabaseTypeId( $arrintCid, $intDatabaseUserTypeId, $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true, $boolIsRequestFromAPI = false ) {

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted,
						d.cid as cid
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						INNER JOIN directives d ON ( d.database_id = ds.id )
					WHERE
						d.cid IN (' . ( implode( ',', $arrintCid ) ) . ')
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last';

		if( true == $boolIsRequestFromAPI ) {
			return parent::fetchObjects( $strSql, 'CDatabase', $objDatabase, false );
		}

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllOrderedDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
						database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND deleted_on IS NULL';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}

		$strSql .= 'ORDER BY id ';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true ) {

		$strSql = 'SELECT * FROM databases WHERE database_type_id = ' . ( int ) $intDatabaseTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY database_name';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabasesByDatabaseUserTypeIdByDatabaseIds( $intDatabaseUserTypeId, $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND ds.database_id IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchSlaveDatabasesByDatabaseUserTypeIdByDatabaseIds( $intDatabaseUserTypeId, $arrintDatabaseIds, $objDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) return NULL;
		// Priorty 1st: RO, 2nd: DR, 3rd Master

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						LEFT JOIN replication_roles rr ON( ds.replication_role_id = rr.id )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND ds.database_id IS NOT NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
						AND ds.replication_role_id NOT IN ( ' . CReplicationRole::CLIENT . ' )
					ORDER BY ds.id, du.database_id NULLS last, rr.order_num ASC';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchSlaveDatabaseByDatabaseIds( $arrintDatabaseIds, $objDatabase ) {

		$strSql = 'SELECT
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND ds.database_id IS NOT NULL
						AND du.deleted_on IS NULL
						AND ds.database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					ORDER BY id ';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllDatabasesByDatabaseTypeIdsByClusterId( $arrintDatabaseTypeIds, $intClusterId, $objDatabase, $boolFetchMasterDatabases = true, $intCloudId = NULL ) {

		$strSql = 'SELECT * FROM databases WHERE database_type_id IN( ' . implode( ',', $arrintDatabaseTypeIds ) . ') AND deleted_on IS NULL';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}

		if( false == is_null( $intClusterId ) ) {
			$strSql .= ' AND cluster_id = ' . ( int ) $intClusterId;
		}

		if( false == is_null( $intCloudId ) ) {
			$strSql .= ' AND cloud_id = ' . ( int ) $intCloudId;
		}

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	// PsVoip

	/**
	 * @param $intCid
	 * @param $intDatabaseUserTypeId
	 * @param bool $boolIsDbConnectionOpen
	 * @return CDatabase|null
	 */
	public static function createDatabaseByCid( $intCid, $intDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER, $boolIsDbConnectionOpen = true ) {

		$objConnectDatabase = self::createConnectDatabase();

		$strSql = 'SELECT
						ds.*,
						d.cid,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.cid = ' . ( int ) $intCid . ' ORDER BY du.database_id NULLS last LIMIT 1';

		$objDatabase = self::fetchDatabase( $strSql, $objConnectDatabase );

		$objConnectDatabase->close();

		if( false == is_null( $objDatabase ) && true == $boolIsDbConnectionOpen ) {
			if( false == getIsDbOnMaintenance( $objDatabase->getId() ) ) {
				$objDatabase->open();
			}
		}

		return $objDatabase;
	}

	public static function loadAlternateConnectDatabases( $objMasterConnectDatabase = NULL ) {
		$arrobjConnectDatabases = [];

		if( false == valObj( $objMasterConnectDatabase, 'CDatabase' ) ) {
			$objMasterConnectDatabase = self::createConnectDatabase( $boolRandomizeConnections = false );
		}

		// [SG] - WE WANT TO USE CONFIG_DB_HOST_ALTERNATES JUST FOR READ AND NOT FOR WRITE SO RETURNING ONLY MASTER COPY OF CONNECT FOR WRITE
		$arrobjConnectDatabases[$objMasterConnectDatabase->getIpAddress()] = $objMasterConnectDatabase;
		return $arrobjConnectDatabases;

		// [SG] - WE MIGHT NEED THIS CODE LATER ON FOR EMERGANCY
		if( true == defined( 'CONFIG_DB_HOST_ALTERNATES' ) && false == is_null( CONFIG_DB_HOST_ALTERNATES ) ) {

			$arrstrAlternateDbHosts = explode( ',', CONFIG_DB_HOST_ALTERNATES );

			if( true == valArr( $arrstrAlternateDbHosts ) ) {
				foreach( $arrstrAlternateDbHosts as $strAlternateDbHost ) {
					$strAlternateDbHost = trim( $strAlternateDbHost );

					if( $strAlternateDbHost == trim( $objMasterConnectDatabase->getIpAddress() ) ) continue;

					$objDatabase = clone $objMasterConnectDatabase;

					$objDatabase->setHandle( NULL );
					$objDatabase->setMachineName( $strAlternateDbHost );
					$objDatabase->setIpAddress( $strAlternateDbHost );

					$objDatabase->open();

					$arrobjConnectDatabases[$strAlternateDbHost] = $objDatabase;
				}
			}
		}

		return $arrobjConnectDatabases;
	}

	public static function fetchDatabaseByDomainByDirectiveTypeIdByDatabaseTypeIdByDatabaseUserTypeId( $strDomain, $intDirectiveTypeId, $intDatabaseTypeId, $intDatabaseUserTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true, $intCid = 0, $boolOpenConnection = true ) {

		if( true == $boolFetchMasterDatabases ) {
			$strJoinOnDirectives = 'di.database_id = d.id';
			$strJoinOnDatabaseUsers = '( d.id = du.database_id )';

		} else {
			$strJoinOnDirectives = 'di.database_id = d.database_id';
			$strJoinOnDatabaseUsers = '( d.database_id = du.database_id )';
		}
		$strSql = 'SELECT
						d.*,
						di.cid,
						di.website_id,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted,
						du.database_id,
						di.property_id as property_id,
						du.database_user_type_id
					FROM
						directives di
						JOIN databases d ON ( ' . $strJoinOnDirectives . ' )
						JOIN database_users du ON ( ' . $strJoinOnDatabaseUsers . ' )
					WHERE
						du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND di.directive_type_id = ' . ( int ) $intDirectiveTypeId;
		if( true == valId( $intCid ) ) {
			$strSql .= ' AND di.cid = ' . ( int ) $intCid;
		} else {
			$strSql .= ' AND di.domain = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( CStrings::strTrimDef( $strDomain, 240 ) ) ) . '\'';
		}

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NOT NULL ';
		}
		$strSql .= ' AND d.deleted_by IS NULL AND d.deleted_on IS NULL LIMIT 1';

		$objDatabase = self::fetchObject( $strSql, 'CDatabase', $objConnectDatabase );
		// If slave database object is Null then it will return master database object
		if( false == is_null( $objDatabase ) ) {
			if( false == getIsDbOnMaintenance( $objDatabase->getId() ) ) {
				if( true == $boolOpenConnection ) {
					$objDatabase->open();
				}
			}
			return $objDatabase;
		} else {
			// If Master database object is Null then it returns Null(To prevent infinite recursive call)
			if( false == $boolFetchMasterDatabases ) {
				return self::fetchDatabaseByDomainByDirectiveTypeIdByDatabaseTypeIdByDatabaseUserTypeId( $strDomain, $intDirectiveTypeId, $intDatabaseTypeId, $intDatabaseUserTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true, $intCid, $boolOpenConnection );
			} else {
				return NULL;
			}
		}
	}

	public static function fetchDatabaseByCidByDirectiveTypeIdByDatabaseTypeIdByDatabaseUserTypeId( $intCid, $intDirectiveTypeId, $intDatabaseTypeId, $intDatabaseUserTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true ) {

		$strSql = 'SELECT
						DISTINCT ON (d.id)  d.id,
						d.*,
						di.cid,
						di.website_id,
						du.username_encrypted,
						du.password_encrypted,
						du.database_id
					FROM
						directives di,
						databases d,
						database_users du
					WHERE
						di.database_id = d.id
						AND ( d.id = du.database_id OR d.database_type_id = any( du.database_type_ids ) )
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND di.directive_type_id = ' . ( int ) $intDirectiveTypeId . '
						AND di.cid = ' . ( int ) $intCid;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY d.id, du.database_id NULLS last LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchPortalDatabaseByHttpHost( $strHttpHost, $objConnectDatabase, $intDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {
		// Here we strip out everything after the subdomain if the website is being accessed by a sub domain.
		if( true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.residentportal.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.residentportaldev.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.prospectportal.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.prospectportaldev.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.employeeportal.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.prospectportalpreview.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.residentportalpreview.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.propertysolutionspreview.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.reservationhub.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.reservationhubdev.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.tenantaccess.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.greystar-residents.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.greystar-reservationhub.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.greystar-tenants.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.greystar-property.' ) ) {
			$strDomainName = str_replace( '.' . CONFIG_SUBDOMAIN_EXTENSION, '', \Psi\CStringService::singleton()->strtolower( $strHttpHost ) );
		} else {
			$strDomainName = $strHttpHost;
		}

		$strDomainName = str_replace( 'www.', '', $strDomainName );
		$arrstrDomainName = explode( '.', $strDomainName );

		// The following url types might come through.

		// 1. subdomain
		// 3. omniapts.com (or some other fully qualified domain)
		// 4. property.omniapts.com (full domain with sub domain)

		if( true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.reservationhub.' ) || true == \Psi\CStringService::singleton()->stristr( $strHttpHost, '.reservationhubdev.' ) ) {
			// Reservation Hub needs to be treated the same as Entrata
			$intDirectiveTypeId = CDirectiveType::CLIENT;
		} else if( 1 == \Psi\Libraries\UtilFunctions\count( $arrstrDomainName ) ) {
			$intDirectiveTypeId = CDirectiveType::WEBSITE_SUB_DOMAIN;

		} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrstrDomainName ) ) {
			$intDirectiveTypeId = CDirectiveType::WEBSITE_FULL_DOMAIN;
		}

		return self::fetchDatabaseByDomainByDirectiveTypeIdByDatabaseTypeIdByDatabaseUserTypeId( $strDomainName, $intDirectiveTypeId, CDatabaseType::CLIENT, $intDatabaseUserTypeId, $objConnectDatabase );
	}

	public static function fetchDatabasesByIds( $arrintDatabaseIds, $objDatabase ) {
		if( false == valArr( $arrintDatabaseIds ) ) return NULL;
		$strSql = 'SELECT 
						* 
					FROM 
						databases 
					WHERE 
						deleted_on IS NULL 
						AND deleted_by IS NULL 
						AND database_id IS NULL 
						AND id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' ) 
					ORDER BY id';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchMasterDatabases( $objConnectDatabase ) {

		$strSql = 'SELECT * FROM databases WHERE deleted_on IS NULL AND deleted_by IS NULL AND database_id IS NULL ORDER BY database_name ';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchSimpleDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT id, database_name FROM databases WHERE database_type_id = ' . ( int ) $intDatabaseTypeId . ' AND deleted_by IS NULL AND deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}
		$arrstrMixData = fetchData( $strSql, $objDatabase );

		$arrstrDatabase = [];

		foreach( $arrstrMixData as $arrstrData ) {
			$arrstrDatabase[$arrstrData['id']] = $arrstrData['database_name'];
		}

		return $arrstrDatabase;
	}

	public static function fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeIds( $intDatabaseUserTypeId, $arrintDatabaseTypeIds, $objDatabase, $boolFetchMasterDatabases = true ) {

		if( false == valArr( $arrintDatabaseTypeIds ) ) return false;

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND ds.is_test_database = 0
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id IN ( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last ';

		return self::fetchDatabases( $strSql, $objDatabase );

	}

	public static function fetchAllDatabasesByDatabaseServerTypeIdExcludingDatabaseTypeIds( $intDatabaseServerTypeId, $arrintDatabaseTypeIds, $objDatabase, $boolFetchMasterDatabases = true ) {
		if( false == valArr( $arrintDatabaseTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM databases WHERE database_server_type_id = ' . ( int ) $intDatabaseServerTypeId . ' AND database_type_id NOT IN (' . implode( ',', $arrintDatabaseTypeIds ) . ') AND deleted_on IS NULL';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabasesWithDatabaseTypes( $objDatabase, $boolFetchMasterDatabases = true ) {

		$strSql = 'SELECT
						d.id,
						dt.name AS database_type_name
					FROM
						databases AS d
						JOIN database_types AS dt ON ( dt.id = d.database_type_id )
					WHERE
						d.deleted_by IS NULL
						AND d.deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NOT NULL ';
		}
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllSlaveDatabases( $objDatabase ) {
		$strSql = 'SELECT * FROM databases WHERE deleted_by IS NULL AND database_id IS NOT NULL ORDER BY database_name ';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchMasterDatabasesByDatabaseTypeIdByIsTestDatabase( $intDatabaseTypeId, $objDatabase, $boolIsTestDatabase = false, $boolAllDatabases = false, $boolIsRestricted = NULL ) {

		$strSqlCondition = '';
		$strIsRestricted = '';

		if( false == is_null( $boolIsRestricted ) ) {
			$strIsRestricted = ( true == $boolIsRestricted ) ? ' AND is_restricted = true' : '  AND is_restricted = false';
		}

		if( true == $boolIsTestDatabase ) {
			$strSqlCondition = ' AND is_test_database = 1';
		} else {
			$strSqlCondition = ' AND is_test_database = 0';
		}

		$strSqlCondition = ( false == $boolAllDatabases ) ? $strSqlCondition : '';

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$strSqlCondition = ' AND is_test_database = 0';
		}

		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
						database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND database_id IS NULL
						 ' . $strIsRestricted . $strSqlCondition . ' ORDER BY id';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllMasterDatabasesByDatabaseTypeIdByIsTestDatabase( $intDatabaseTypeId, $objDatabase, $boolIsTestDatabase = false ) {
		$strSqlCondition = '';

		if( true == $boolIsTestDatabase ) {
			$strSqlCondition = ' AND is_test_database = 1';
		} else {
			$strSqlCondition = ' AND is_test_database = 0';
		}

		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
						database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND database_id IS NULL' . $strSqlCondition . ' ORDER BY id';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchMasterDatabasesByClusterIdByDatabaseTypeIdByIsTestDatabase( $intClusterId, $intDatabaseTypeId, $objDatabase, $boolIsTestDatabase = false ) {
		$strSqlCondition = '';

		if( true == $boolIsTestDatabase ) {
			$strSqlCondition = ' AND is_test_database = 1';
		} else {
			$strSqlCondition = ' AND is_test_database = 0';
		}

//		if( 'production' != CONFIG_ENVIRONMENT ) {
//			$strSqlCondition = ' AND is_test_database = 0';
//		}

		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
						database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND cluster_id = ' . ( int ) $intClusterId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND database_id IS NULL' . $strSqlCondition . ' AND is_template = false ORDER BY id';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchTemplateDatabasesByClusterIds( $arrintClusterIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
						cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ')
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						AND database_id IS NULL
						AND is_template = true
					ORDER BY
						id;';
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchTemplateDatabaseByClusterIdByUserTypeId( $intClusterId, $intDatabaseUserTypeId, $objConnectDatabase ) {

		$strSql = 'SELECT
						d.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases d,
						database_users du
					WHERE
						d.cluster_id = ' . ( int ) $intClusterId . '
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND d.database_id IS NULL
						AND d.is_template = true';
		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchTemplateDatabasesByDatabaseUserTypeIdByClusterIds( $intDatabaseUserTypeId, $arrintClusterIds, $objConnectDatabase ) {

		$strSql = 'SELECT
                        DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND du.deleted_on IS NULL
						AND ds.database_id IS NULL
						AND ds.is_template = true
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')';
		$strSql .= ' ORDER BY ds.id,du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchSimpleMasterDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase, $strDatabase = NULL ) {

		if( true == is_null( $intDatabaseTypeId ) || false == is_numeric( $intDatabaseTypeId ) ) return NULL;

		$strSqlCondition = '';

		if( 'works' == $strDatabase ) {
			$strSqlCondition = ' AND cid IS NULL';
		} elseif( 'split' == $strDatabase ) {
			$strSqlCondition = ' AND cid IS NOT NULL';
		}

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$strSqlCondition = ' AND cid IS NULL';
		}

		$arrmixDatabases = [];

		$strSql = 'SELECT
						db.id,
						db.database_name
					FROM
						databases db
					WHERE
						db.database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND db.deleted_on IS NULL
						AND db.deleted_by IS NULL
						AND db.database_id IS NULL' . $strSqlCondition . ' ORDER BY id';

		$arrmixResponses = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResponses ) ) {
			foreach( $arrmixResponses as $arrmixResponse ) {
				$arrmixDatabases[$arrmixResponse['id']] = $arrmixResponse;
			}
		}

		return $arrmixDatabases;
	}

	// overriding base class functions to fetch master databases

	public static function fetchDatabasesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true, $boolOrderByCluster = false ) {
		$strSql = 'SELECT * FROM databases WHERE database_type_id = ' . ( int ) $intDatabaseTypeId . ' AND deleted_by IS NULL AND deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}
		if( true == $boolOrderByCluster ) {
			$strSql .= ' ORDER BY cluster_id, id ASC';
		}
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabasesByClusterIdByDatabaseTypeId( $intClusterId, $intDatabaseTypeId, $objDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT * FROM databases WHERE database_type_id = ' . ( int ) $intDatabaseTypeId . ' AND cluster_id = ' . ( int ) $intClusterId . ' AND deleted_by IS NULL AND deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByDatabaseName( $strDatabaseName, $objDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT * FROM databases WHERE database_name like \'' . $strDatabaseName . '\'';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL ';
		}
		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedDatabases( $intPageNo, $intPageSize, $objConnectDatabase, $objDatabasesFilter, $strOrderByField = NULL, $strOrderByType = NULL, $intReplicationRoleId = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType ) . ', d.database_name';
			} else {
				$strOrderBy .= ' ASC, d.database_name';
			}
		} else {
			$strOrderBy = ' ORDER BY d.updated_on DESC';
		}

		switch( $intReplicationRoleId ) {

			case CReplicationRole::MASTER:
				$strDatabaseType = ' AND d.deleted_by IS NULL AND d.database_id IS NULL ';
				break;

			case CReplicationRole::SLAVE:
				$strDatabaseType = ' AND d.deleted_by IS NULL AND d.database_id IS NOT NULL AND d.replication_role_id NOT IN ( ' . CReplicationRole::RO . ', ' . CReplicationRole::CLIENT . ' )';
				break;

			case CReplicationRole::CLIENT:
				$strDatabaseType = ' AND d.deleted_by IS NULL AND d.replication_role_id = ' . ( int ) CReplicationRole::CLIENT;
				break;

			case CReplicationRole::RO:
				$strDatabaseType = ' AND d.deleted_by IS NULL AND d.replication_role_id = ' . ( int ) CReplicationRole::RO;
				break;

			default:
				$strDatabaseType = ' AND d.deleted_by IS NULL ';
				break;
		}

		$strSql = 'SELECT
						d.*,
						dt.name AS database_type_name,
						dst.name AS database_server_type_name
					FROM
						databases AS d
						JOIN database_types AS dt ON ( d.database_type_id = dt.id )
						LEFT JOIN database_server_types AS dst ON ( d.database_server_type_id = dst.id )
					WHERE
						d.deleted_on IS NULL ' . $strDatabaseType;

		if( false == is_null( $objDatabasesFilter->getClusterIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getClusterIds() ) ) {
			$strSql .= 'AND d.cluster_id IN ( ' . implode( ', ', $objDatabasesFilter->getClusterIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseTypeIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseTypeIds() ) ) {
			$strSql .= 'AND d.database_type_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseTypeIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseServerTypeIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseServerTypeIds() ) ) {
			$strSql .= 'AND d.database_server_type_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseServerTypeIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseServerIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseServerIds() ) ) {
			$strSql .= 'AND d.hardware_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseServerIds() ) . ' )';
		}
		if( false == is_null( $objDatabasesFilter->getDatabaseName() ) ) {
			$strSql .= 'AND d.database_name ILIKE \'%' . $objDatabasesFilter->getDatabaseName() . '%\'';
		}

		$strSql	.= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchAllPaginatedDatabasesCount( $objDatabasesFilter, $objConnectDatabase, $intReplicationRoleId = NULL ) {

		switch( $intReplicationRoleId ) {

			case CReplicationRole::MASTER:
				$strWhere = 'WHERE deleted_on IS NULL AND deleted_by IS NULL AND database_id IS NULL ';
				break;

			case CReplicationRole::SLAVE:
				$strWhere = 'WHERE deleted_on IS NULL AND deleted_by IS NULL AND database_id IS NOT NULL AND replication_role_id NOT IN ( ' . CReplicationRole::RO . ', ' . CReplicationRole::CLIENT . ' )';
				break;

			case CReplicationRole::CLIENT:
				$strWhere = 'WHERE deleted_on IS NULL AND deleted_by IS NULL AND replication_role_id = ' . ( int ) CReplicationRole::CLIENT;
				break;

			case CReplicationRole::RO:
				$strWhere = 'WHERE deleted_on IS NULL AND deleted_by IS NULL AND replication_role_id = ' . ( int ) CReplicationRole::RO;
				break;

			default:
				$strWhere = 'WHERE deleted_on IS NULL AND deleted_by IS NULL ';
				break;
		}

		if( false == is_null( $objDatabasesFilter->getClusterIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getClusterIds() ) ) {
			$strWhere .= 'AND cluster_id IN ( ' . implode( ', ', $objDatabasesFilter->getClusterIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseTypeIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseTypeIds() ) ) {
			$strWhere .= 'AND database_type_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseTypeIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseServerTypeIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseServerTypeIds() ) ) {
			$strWhere .= 'AND database_server_type_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseServerTypeIds() ) . ' )';
		}

		if( false == is_null( $objDatabasesFilter->getDatabaseServerIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objDatabasesFilter->getDatabaseServerIds() ) ) {
			$strWhere .= 'AND hardware_id IN ( ' . implode( ', ', $objDatabasesFilter->getDatabaseServerIds() ) . ' )';
		}
		if( false == is_null( $objDatabasesFilter->getDatabaseName() ) ) {
			$strWhere .= 'AND database_name ILIKE \'%' . $objDatabasesFilter->getDatabaseName() . '%\'';
		}

		return self::fetchDatabaseCount( $strWhere, $objConnectDatabase );
	}

	public static function fetchSearchedDatabases( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						d.*,
						dt.name AS database_type_name
					FROM
						databases AS d
						JOIN database_types AS dt ON ( d.database_type_id = dt.id )
					WHERE
						d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND ( d.database_name ILIKE \'%' . implode( '%\' AND dt.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR dt.name ILIKE \'%' . implode( '%\' AND d.database_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ) order by d.id
					LIMIT 15';

		return self::fetchDatabases( $strSql, $objDatabase );

	}

	public static function fetchDatabasesByDatabaseTypeIds( $arrintDatabaseTypeIds, $objDatabase, $boolFetchMasterDatabases = true, $boolIncludeTestDatabases = true ) {

		$strSql = 'SELECT * FROM databases WHERE database_type_id IN ( ' . implode( ',', $arrintDatabaseTypeIds ) . ' ) AND deleted_by IS NULL AND deleted_on IS NULL ';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NOT NULL';
		}
		if( false == $boolIncludeTestDatabases ) {
			$strSql .= ' AND is_test_database = 0';
		}

		$strSql .= ' ORDER BY database_type_id, order_num, id';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchLatestDatabaseId( $objDatabase ) {

		$strSql = 'SELECT MAX(id)+1 as max_id FROM databases';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['max_id'] ) ) {
			return $arrintResponse[0]['max_id'];
		}
	}

	public static function fetchDatabasesByDatabaseUserTypeIdByDatabaseServerTypeId( $intDatabaseUserTypeId, $intDatabaseServerTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT DISTINCT ON (ds.id)  ds.id, ds.*, du.username_encrypted, du.password_encrypted FROM databases ds, database_users du WHERE ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) ) AND ds.deleted_on IS NULL AND du.deleted_on IS NULL AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . ' AND ds.database_server_type_id =' . ( int ) $intDatabaseServerTypeId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last ';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterId( $intDatabaseTypeId, $intDatabaseUserTypeId, $intClusterId, $objConnectDatabase, $boolFetchMasterDatabases = true, $boolIncludeTestDatabases = false ) {
		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id =' . ( int ) $intClusterId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( false == $boolIncludeTestDatabases ) {
			$strSql .= ' AND ds.is_test_database = 0';
		}
		$strSql .= ' ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterId( $intDatabaseUserTypeId, $intDatabaseTypeId, $intClusterId, $objConnectDatabase, $boolFetchMasterDatabases = true, $strOrderBy = NULL, $boolGetPrivateDatabases = false, $boolRestricted = false ) {

		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.is_test_database = 0
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id =' . ( int ) $intClusterId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( true == $boolGetPrivateDatabases ) {
			$strSql .= ' AND ds.is_private = 1';
		}

		if( true == $boolRestricted ) {
			$strSql .= ' AND is_restricted IS FALSE ';
		}

		if( true == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ds.id, CASE WHEN ds.id = ' . CDatabase::ENTRATA44 . ' THEN 0 ELSE 1 END ';
		} elseif ( 'id' != $strOrderBy ) {
			$strSql .= ' ORDER BY ds.id, ds.' . $strOrderBy;
		} else {
			$strSql .= ' ORDER BY ds.id';
		}

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchCommonClientDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByClusterId( $intDatabaseUserTypeId, $intDatabaseTypeId, $intClusterId, $objConnectDatabase, $boolFetchMasterDatabases = true ) {
		$strSql = 'SELECT
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						ds.id = du.database_id
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.cid IS NULL
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id =' . ( int ) $intClusterId;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY CASE WHEN ds.id = ' . CDatabase::ENTRATA44 . ' THEN 0 ELSE 1 END, updated_on, id LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterIdsByIsTestDatabase( $intDatabaseUserTypeId, $intDatabaseTypeId, $arrintClusterIds, $objConnectDatabase, $boolIsTestDatabase = false, $boolFetchMasterDatabases = true, $strOrderBy = NULL, $boolRestricted = false ) {

		$strSql = 'SELECT
						DISTINCT ON ( ds.id )  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')';

		if( true == $boolIsTestDatabase && 'production' == CONFIG_ENVIRONMENT ) {
			$strSql .= ' AND is_test_database = 1';
		} else {
			$strSql .= ' AND is_test_database = 0';
		}

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( true == $boolRestricted ) {
			$strSql .= ' AND is_restricted IS FALSE ';
		}

		if( true == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY id ';
		} else {
			$strSql .= ' ORDER BY id, ds.' . $strOrderBy;
		}

		$strSql .= ', du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterIdsByDatabaseIds( $intDatabaseUserTypeId, $intDatabaseTypeId, $arrintClusterIds, $arrstrDatabaseIds, $objConnectDatabase, $boolFetchMasterDatabases = true, $strOrderBy = NULL ) {

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')
						AND ds.id IN ( ' . implode( ',', $arrstrDatabaseIds ) . ')';

		if( 'production' == CONFIG_ENVIRONMENT ) {
			$strSql .= ' AND is_test_database = 1';
		} else {
			$strSql .= ' AND is_test_database = 0';
		}

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( true == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY id ';
		} else {
			$strSql .= ' ORDER BY id, ds.' . $strOrderBy;
		}

		$strSql .= ', du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseClusterNameByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						c.name AS cluster_name
					FROM
						databases d
						LEFT JOIN clusters c ON ( c.id = d.cluster_id )
					WHERE
						d.database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL';

		$arrstrMixData = fetchData( $strSql, $objDatabase );

		$arrstrDatabase = [];

		foreach( $arrstrMixData as $arrstrData ) {
			$arrstrDatabase[$arrstrData['id']] = $arrstrData['cluster_name'];
		}

		return $arrstrDatabase;
	}

	public static function fetchDatabaseIdByClusterId( $intClusterId, $objConnectDatabase, $intCloudId = NULL ) {

		$strWhereCondition = '';
		if( empty( $intCloudId ) || $intCloudId == CCloud::LINDON_ID ) {
			$strWhereCondition = 'AND is_test_database = 1';
		}
		$strSql = 'SELECT
						id
					FROM
						databases
					WHERE
						database_name LIKE \'entrata%\'
						AND database_name NOT LIKE \'entrata_split%\'
						AND cluster_id = ' . ( int ) $intClusterId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL
						'.$strWhereCondition.'
					ORDER BY created_on LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseIdsByClusterId( $intClusterId, $objDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						databases
					WHERE
						cluster_id = ' . ( int ) $intClusterId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND database_type_id = ' . CDatabaseType::CLIENT;

		$arrmixResponseData = fetchData( $strSql, $objDatabase );

		$arrintDatabaseIds = [];

		if( true == valArr( $arrmixResponseData ) ) {
			foreach( $arrmixResponseData as $arrintResponseData ) {
				$arrintDatabaseIds[] = $arrintResponseData['id'];
			}
		}

		return $arrintDatabaseIds;
	}

	public static function fetchDatabaseByCid( $intCid, $objConnectDatabase, $intDatabaseUserTypeId = CDatabaseUserType::PS_PROPERTYMANAGER ) {

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND ds.deleted_on IS NULL
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.cid = ' . ( int ) $intCid;

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabasesByDatabaseTypeIdsByDefaultUserTypeIds( $arrintDatabaseInformations, $objConnectDatabase, $boolFetchMasterDatabases = true ) {
		if( false == valArr( $arrintDatabaseInformations ) ) return NULL;

		$strWhereClause = '';
		$strInCondition = '';
		foreach( $arrintDatabaseInformations as $arrintDatabaseInformation ) {
			$strInCondition .= '( ' . ( int ) $arrintDatabaseInformation['type_id'] . ', ' . ( int ) $arrintDatabaseInformation['user_id'] . ' ), ';
		}
		$strInCondition = trim( $strInCondition, ', ' );

		if( true == $boolFetchMasterDatabases ) {
			$strWhereClause = ' AND ds.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strWhereClause = ' AND ds.database_id IS NOT NULL ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.database_type_id, du.database_user_type_id ) IN ( ' . $strInCondition . ' )
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						' . $strWhereClause;

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST';

		$arrobjDatabases = self::fetchDatabases( $strSql, $objConnectDatabase );

		$arrobjAllDatabases = [];

		foreach( $arrobjDatabases as $objDatabase ) {
			if( true == $objDatabase->validate( 'validate_open' ) ) {
				if( true == is_null( $objDatabase->open() ) ) {
					return NULL;
				}
				$arrobjAllDatabases[$objDatabase->getDatabaseTypeId()] = $objDatabase;
			} else {
				$arrstrDatabaseNames = CDatabaseType::getDatabaseNames();
				trigger_error( 'Database Error: Invalid ' . $arrstrDatabaseNames[$objDatabase->getDatabaseTypeId()] . ' database credentials.', E_USER_ERROR );
				return false;
			}
		}
		return $arrobjAllDatabases;
	}

	public static function fetchAllMasterDatabases( $objDatabase ) {
		$strSql = 'SELECT
						ds.*
					FROM
						databases ds
					WHERE
						ds.deleted_on IS NULL
						AND ds.database_id IS NULL
						AND ds.is_test_database = 0
						AND ds.database_server_type_id = ' . ( int ) CDatabaseServerType::POSTGRES . '
						AND ds.id NOT IN( 3, 5, 6, 11, 15, 17, 23, 24, 77, 151, 250, 411, 482, 495, 511, 596 )
					ORDER BY
						ds.database_name';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabasesByCidsByClusterId( $arrintIds, $intClusterId, $objConnectDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = ' SELECT
                        DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted,
						d.cid
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND d.cid IN ( ' . implode( ',', $arrintIds ) . ' )
						AND ds.cluster_id = ' . ( int ) $intClusterId . '
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND ds.database_id IS NULL
						AND ds.is_test_database = 0
						AND du.database_user_type_id = ' . ( int ) CDatabaseUserType::PS_PROPERTYMANAGER . '
						AND ds.database_type_id = ' . ( int ) CDatabaseType::CLIENT;

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseTypeIdsByClusterId( $intClusterId, $objDatabase ) {

		$strSql = 'SELECT DISTINCT( database_type_id ) FROM databases WHERE cluster_id = ' . ( int ) $intClusterId;

		$arrintDatabaseTypeIds = rekeyArray( 'database_type_id', fetchData( $strSql, $objDatabase ) );

		return array_keys( $arrintDatabaseTypeIds );
	}

	public static function fetchAllTestDatabasesByClusterIdsByDatabaseUserTypeId( $arrintClusterIds, $intDatabaseUserTypeId, $objConnectDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( d.id ) d.id,
						d.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases d
						JOIN database_users du ON ( d.id = du.database_id OR ( d.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						d.is_test_database = 1
						AND d.cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' )
						AND d.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId;

		$strSql .= ' ORDER BY d.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseIdsByClusterIds( $arrintClusterIds, $objConnectDatabase, $boolIsReturnObjects = false, $boolFetchMasterDatabases = false ) {

		$strWhere		= '';
		$strColumnName = '';
		$strOrderBy		= '';

		if( true == valIntArr( $arrintClusterIds ) ) {
			$strWhere = 'cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' ) AND';
		}

		if( true == $boolFetchMasterDatabases ) {
			$strWhere		.= ' database_id IS NULL AND';
			$strColumnName	= ', is_test_database, cluster_id';
			$strOrderBy		= ' ORDER BY SUBSTRING(database_name FROM \'' . self::ENTRATA_DATABASE_PREFIX . '([0-9]+)\')::BIGINT,database_name';
		}

		$strSql = 'SELECT
						id,
						database_name' .
						$strColumnName . '
					FROM
						databases
					WHERE
						' . $strWhere . '
						deleted_by IS NULL
						AND deleted_on IS NULL
						AND database_type_id = ' . ( int ) CDatabaseType::CLIENT . $strOrderBy;

		if( true == $boolIsReturnObjects ) {
			return self::fetchDatabases( $strSql, $objConnectDatabase );
		} else {
			$arrmixResponseData = fetchData( $strSql, $objConnectDatabase );

			$arrintDatabaseIds = [];

			if( true == valArr( $arrmixResponseData ) ) {
				foreach( $arrmixResponseData as $arrintResponseData ) {
					$arrintDatabaseIds[] = $arrintResponseData['id'];
				}
			}

			return $arrintDatabaseIds;
		}
	}

	public static function fetchDatabasesByCids( $arrintCids, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return;

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.cluster_id,
						d.cid
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND d.cid IN ( ' . implode( ',', $arrintCids ) . ' )
					GROUP BY
						ds.id,
						d.cid,
						du.database_id';

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllDatabasesByDatabaseUserTypeIdByDatabaseTypeIds( $intDatabaseUserTypeId, $arrintDatabaseTypeIds, $objConnectDatabase ) {

		if( false == valArr( $arrintDatabaseTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.id ) d.id,
						d.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases d
						JOIN database_users AS du ON ( d.id = du.database_id OR ( d.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						d.deleted_on IS NULL
						AND d.deleted_by IS NULL
						AND d.database_id IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.database_type_id IN ( ' . implode( ',', $arrintDatabaseTypeIds ) . ' )
					ORDER BY
						d.id,
						d.database_name,
						du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchClientDatabasesByDatabaseUserTypeIdByDatabaseTypeIdByClusterIds( $intDatabaseUserTypeId, $intDatabaseTypeId, $arrintClusterIds, $objConnectDatabase, $boolFetchMasterDatabases = true, $strOrderBy = NULL, $boolGetPrivateDatabases = false ) {

		$strSelect = ( true == is_null( $strOrderBy ) ) ? 'CASE WHEN ds.id = ' . CDatabase::ENTRATA44 . ' THEN 0 ELSE 1 END' : 'DISTINCT ON ( ds.id ) ds.id';

		$strSql = ' SELECT ' .
						$strSelect . ',
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du
					WHERE
						( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.is_test_database = 0
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId . '
						AND ds.cluster_id IN (' . implode( ',', $arrintClusterIds ) . ')';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NULL ';

		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND ds.database_id IS NOT NULL ';
		}

		if( true == $boolGetPrivateDatabases ) {
			$strSql .= ' AND ds.is_private = 1';
		}

		if( true == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY CASE WHEN ds.id = ' . CDatabase::ENTRATA44 . ' THEN 0 ELSE 1 END';
		} else {
			$strSql .= ' ORDER BY ds.' . $strOrderBy;
		}

		$strSql .= ' , du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchAllClientDatabasesByDatabaseUserTypeIdByClusterIds( $intDatabaseUserTypeId, $arrintClusterIds, $objConnectDatabase, $boolIsIncludeRoDbs = false ) {

		$strDatabaseReplicationRoleCondition = ( false == $boolIsIncludeRoDbs ) ? ' AND ds.database_id IS NULL ' : ' AND ( ds.database_id IS NULL OR ds.replication_role_id = ' . ( int ) CReplicationRole::RO . ' ) ';
		$strSql = 'SELECT
						DISTINCT ON (ds.id)  ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ds.database_type_id = any( du.database_type_ids ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL ';

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$strSql .= ' AND ds.is_test_database = 0 ';
		}

		$strSql .= ' AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id = ' . ( int ) CDatabaseType::CLIENT . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')
						' . $strDatabaseReplicationRoleCondition . '
					ORDER BY ds.id, du.database_id NULLS last';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchAllClientDatabasesByDatabaseUserTypeIdByClusterIdsByIsTestDatabase( $intDatabaseUserTypeId, $arrintClusterIds, $objConnectDatabase, $boolIsTestDatabase = false, $boolIsTemplate = false ) {

		$strWhere = '';

		if( false == $boolIsTestDatabase || 'production' != CONFIG_ENVIRONMENT ) {
			$strWhere .= ' AND ds.is_test_database = 0 ';
		}

		if( false == $boolIsTemplate ) {
			$strWhere .= ' AND ds.is_template = false ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id = ' . ( int ) CDatabaseType::CLIENT . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')
						AND ds.database_id IS NULL
						' . $strWhere . ' ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchAllClientDatabasesByDatabaseUserTypeIdByClusterIdsByDatabaseIds( $intDatabaseUserTypeId, $arrintClusterIds, $arrstrDatabaseIds, $objConnectDatabase ) {

		$strWhere = '';

		if( 'production' != CONFIG_ENVIRONMENT ) {
			$strWhere .= ' AND ds.is_test_database = 0 ';
		}

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						LEFT JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						LEFT JOIN clusters AS c ON ( ds.cluster_id = c.id )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id = ' . ( int ) CDatabaseType::CLIENT . '
						AND ds.cluster_id IN( ' . implode( ',', $arrintClusterIds ) . ')
						AND ds.database_id IS NULL
						AND ds.id IN ( ' . implode( ',', $arrstrDatabaseIds ) . ')
						' . $strWhere . ' ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchDatabaseCountByDatabaseTypeIdByClusterId( $intDatabaseTypeId, $intClusterId, $objConnectDatabase ) {

		$strWhere = 'WHERE database_type_id =' . ( int ) $intDatabaseTypeId . ' AND cluster_id = ' . ( int ) $intClusterId . ' AND database_id IS NULL AND is_template = false';

		return self::fetchDatabaseCount( $strWhere, $objConnectDatabase );

	}

	public static function fetchAllDatabasesByDatabaseServerTypeId( $intDatabaseServerTypeId, $objDatabase, $strDatabaseIds = NULL ) {

		$strCondition = '';
		if( true == valStr( $strDatabaseIds ) ) {
			$strCondition = ' AND ds.id IN ( ' . $strDatabaseIds . ' ) ';
		}
		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . CDatabaseUserType::PS_PROPERTYMANAGER . '
						AND ds.database_server_type_id = ' . ( int ) $intDatabaseServerTypeId . '
						AND ds.database_id IS NULL ' . $strCondition . '
					ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objDatabase );

	}

	public static function fetchDatabasesByClusterIds( $arrintClusterId, $objDatabase, $boolFetchMasterDatabases = false ) {

		if( false == valArr( $arrintClusterId ) ) return false;

		$strSql = 'SELECT * FROM databases WHERE deleted_on IS NULL AND deleted_by IS NULL AND database_server_type_id = ' . CDatabaseServerType::POSTGRES . ' AND cluster_id IN(' . implode( ',', $arrintClusterId ) . ')';

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND database_id IS NULL ';

		} else {
			$strSql .= ' AND database_id IS NOT NULL ';
		}

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchClientHardwareDetails( $objConnectDatabase ) {

		$strSql = 'SELECT
						id,
						hardware_id,
						cid
					FROM
						databases
					WHERE
						cid IS NOT NULL
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND is_test_database = 0
						AND database_type_id = ' . ( int ) CDatabaseType::CLIENT;

		return fetchData( $strSql, $objConnectDatabase );

	}

	public static function fetchDatabaseByDatabaseId( $intDatabaseId, $objDatabase ) {

		$strSql = 'SELECT * FROM databases WHERE database_id = ' . ( int ) $intDatabaseId . ' AND deleted_by IS NULL AND replication_role_id = ' . CReplicationRole::DR . ' LIMIT 1';
		return self::fetchDatabase( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByCidByDatabaseTypeIdByDatabaseUserTypeId( $intCid, $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase, $boolFetchMasterDatabases = true ) {

		if( false == valId( $intCid ) || false == valId( $intDatabaseUserTypeId ) || false == valId( $intDatabaseTypeId ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( d.id ) d.id,
						d.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						directives di,
						databases d,
						database_users du
					WHERE
						di.database_id = d.id
						AND ( d.id = du.database_id OR ( d.database_type_id = any( du.database_type_ids ) ) )
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND d.database_type_id = ' . ( int ) $intDatabaseTypeId . '
						AND di.cid = ' . ( int ) $intCid;

		if( true == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NULL ';
		} elseif( false == $boolFetchMasterDatabases ) {
			$strSql .= ' AND d.database_id IS NOT NULL ';
		}

		$strSql .= ' ORDER BY d.id, du.database_id NULLS LAST';

		$strSql .= ' LIMIT 1';

		return self::fetchDatabase( $strSql, $objConnectDatabase );
	}

	public static function fetchAllServiceDatabasesByDatabaseServerTypeId( $intDatabaseServerTypeId, $objConnectDatabase, $arrintExcludeDatabaseIds = NULL ) {
		$strWhereClause = '';

		if( true == valArr( $arrintExcludeDatabaseIds ) && true == valArr( array_filter( $arrintExcludeDatabaseIds ) ) ) {
			$strWhereClause = 'AND d.id NOT IN ( ' . implode( ',', $arrintExcludeDatabaseIds ) . ') ';
		}

		$strSql = 'SELECT
						d.*
					FROM
						databases d
					WHERE
						d.deleted_on IS NULL
						AND d.database_id IS NULL
						AND d.database_type_id <> ' . CDatabaseType::CLIENT . '
						AND d.database_server_type_id =' . ( int ) $intDatabaseServerTypeId . $strWhereClause;

		return self::fetchDatabases( $strSql, $objConnectDatabase );
	}

	public static function fetchPrimaryKeyDiagnosticData( $objDatabase ) {
		$fltPrimaryKeyThresholdValue = 0.20;

		$strTempFunctionSql = ' CREATE OR REPLACE FUNCTION pg_temp.get_sequence_last_value( sequence_name text )
									RETURNS bigint AS
										$body$
											DECLARE
												lTableName ALIAS FOR $1;
												lr_record RECORD;
												li_return bigint;

											BEGIN
												FOR lr_record IN EXECUTE \'SELECT last_value AS value FROM \' || lTableName

												LOOP
													li_return := lr_record.value;
												END LOOP;

												RETURN li_return;
											END;
										$body$
									LANGUAGE \'plpgsql\' VOLATILE;';

		fetchData( $strTempFunctionSql, $objDatabase );

		$strPkeyMaxValueSql = ' SELECT
									subq.constraint_name,
									subq.table_name,
									subq.column_name,
									CASE WHEN ( ' . $fltPrimaryKeyThresholdValue . ' * subq.column_max_range ) > ( subq.column_max_range - subq.column_max_value ) THEN 1 ELSE NULL END AS need_attention,
									subq.column_max_range - subq.column_max_value AS records_left
								FROM (
									SELECT
										DISTINCT tc.constraint_name,
										kcu.table_name,
										kcu.column_name,
										pa.atttypid,
										CASE
											WHEN 23 = pa.atttypid THEN -(((2^(8*pg_column_size(1::int)-2))::int << 1)+1)
											WHEN 20 = pa.atttypid THEN -(((2^(8*pg_column_size(1::bigint)-2))::bigint << 1)+1)
										END as column_max_range,
										pg_temp.get_sequence_last_value( s.relname ) AS column_max_value
									FROM
										information_schema.table_constraints tc
										JOIN information_schema.key_column_usage kcu ON ( tc.constraint_name = kcu.constraint_name AND tc.table_name = kcu.table_name )
										JOIN pg_attribute pa ON ( kcu.column_name = pa.attname )
										JOIN pg_class pc ON ( kcu.table_name = pc.relname AND pa.attrelid = pc.oid AND pc.relkind = \'r\' )
										JOIN pg_depend d ON ( d.refobjid = pc.oid AND d.refobjsubid = pa.attnum )
										JOIN pg_class s ON ( d.objid = s.oid AND s.relkind = \'S\' )
										JOIN information_schema.sequences se on ( se.sequence_name = s.relname and se.sequence_schema = \'public\' )
									WHERE
										tc.constraint_type = \'PRIMARY KEY\'
										AND pa.atttypid IN ( 20,23 )
										AND tc.constraint_schema = \'public\'
									) as subq
									WHERE
										( CASE WHEN ( ' . $fltPrimaryKeyThresholdValue . ' * subq.column_max_range ) > ( subq.column_max_range - subq.column_max_value ) THEN 1 ELSE NULL END ) IS NOT NULL';

		return fetchData( $strPkeyMaxValueSql, $objDatabase );
	}

	public static function fetchDatabaseDetailsByDatabaseIds( $arrintDatabaseIds, $objConnectDatabase ) {

		if( false == valArr( $arrintDatabaseIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						d.id As database_id,
						d.database_name,
						c.name AS cluster_name,
						c.id As cluster_id
					FROM
						databases d
						LEFT JOIN clusters c ON ( c.id = d.cluster_id )
					WHERE
						d.id IN( ' . implode( ', ', $arrintDatabaseIds ) . ' )
						AND d.deleted_by IS NULL
						AND d.deleted_on IS NULL';

		$arrstrData = fetchData( $strSql, $objConnectDatabase );
		return $arrstrData;
	}

	public static function fetchTestDatabasesByDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseUserTypeId, $intDatabaseTypeId, $objConnectDatabase ) {

		if( false == valId( $intDatabaseUserTypeId ) || false == valId( $intDatabaseTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id
					FROM
						databases AS ds
						JOIN database_users AS du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND ds.is_test_database = 1
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_type_id =' . ( int ) $intDatabaseTypeId;

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST';

		return fetchData( $strSql, $objConnectDatabase );
	}

	public static function fetchAllClientDatabasesByCidsByDatabaseUserTypeId( $arrintCids, $intDatabaseUserTypeId, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return;

		$strSql = 'SELECT 
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND d.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId;

		$strSql .= ' ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByDatabaseIdByDatabaseUserTypeId( $intDatabaseId, $intDatabaseUserTypeId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND ds.database_id IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.id = ' . ( int ) $intDatabaseId . '
					ORDER BY ds.id, du.database_id NULLS LAST';

		return self::fetchDatabase( $strSql, $objDatabase );
	}

	public static function fetchReplicaDatabaseByDatabaseIdByDatabaseUserTypeId( $intDatabaseId, $intDatabaseUserTypeId, $objDatabase, $arrintReplicaDatabaseIds = [] ) {

		$strWhere = ( !empty( $arrintReplicaDatabaseIds ) ) ? ' AND ds.id IN ( ' . implode( ',', $arrintReplicaDatabaseIds ) . ' )' : '';
		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds
						INNER JOIN database_users du ON ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
					WHERE
						ds.deleted_on IS NULL
						AND ds.deleted_by IS NULL
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
						AND ds.database_id = ' . ( int ) $intDatabaseId . '
						AND ds.replication_role_id = ' . ( int ) CReplicationRole::RO . '
						' . $strWhere . '
					ORDER BY ds.id, RANDOM(), du.database_id NULLS LAST 
					LIMIT 1';

		return self::fetchDatabase( $strSql, $objDatabase );
	}

	public static function fetchAllClientDatabasesObjectsByCidsByDatabaseUserTypeId( $arrintCids, $intDatabaseUserTypeId, $objDatabase ) {

		if( false == valArr( $arrintCids ) ) return;

		$strSql = 'SELECT
						DISTINCT ON ( ds.id ) ds.id,
						ds.*,
						ds.cluster_id,
						d.cid,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases ds,
						database_users du,
						directives d
					WHERE
						d.database_id = ds.id
						AND ( ds.id = du.database_id OR ( ds.database_type_id = any( du.database_type_ids ) ) )
						AND ds.deleted_on IS NULL
						AND du.deleted_on IS NULL
						AND d.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND du.database_user_type_id = ' . ( int ) $intDatabaseUserTypeId . '
					GROUP BY
						ds.id,
						d.cid,
						du.database_user_type_id,
						du.username_encrypted,
						du.password_encrypted';

		return parent::fetchObjects( $strSql, CDatabase::class, $objDatabase, false );
	}

	public static function fetchDatabaseTypeIdsByReplicationRoleId( $intReplicationRoleId, $objDatabase ) {

		$strSql = 'SELECT 
						DISTINCT database_type_id 
					FROM
						databases 
					WHERE
						deleted_on IS NULL
						AND replication_role_id = ' . ( int ) $intReplicationRoleId;

		return rekeyArray( 'database_type_id', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchDatabasesByClusterIdByDatabaseTypeIdByRegionId( $intClusterId, $intDatabaseTypeId, $intRegionId, $objDatabase, $boolIsFetchRoDatabases = true ) {

		$strRoCondition = ( true == $boolIsFetchRoDatabases ) ? ' AND replication_role_id = ' . CReplicationRole::RO : ' AND database_id IS NULL';
		$strSql = 'SELECT 
						* 
					FROM 
						databases 
					WHERE 
						database_type_id = ' . ( int ) $intDatabaseTypeId . ' 
					AND cluster_id = ' . ( int ) $intClusterId . '
					AND deleted_by IS NULL 
					AND deleted_on IS NULL
					AND cloud_id = ' . ( int ) $intRegionId . $strRoCondition;

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function fetchDatabaseByDatabaseUserTypeIdByDatabaseTypeIdByCloudId( $intDatabaseUserTypeId, $intDatabaseTypeId, $strCountryCode, $objDatabase, $boolslave = false ) {
		if( empty( $intDatabaseUserTypeId ) || empty( $intDatabaseTypeId ) || empty( $strCountryCode ) ) return NULL;
		$strCondition = ( false == $boolslave ) ? 'd.database_id IS NULL ' : 'd.database_id IS NOT NULL';
		// FixMe: d.deleted_on IS NULL is not added in the query for temporary, once DevOps fix the dependence on deleted_on add the same.
		$strSql = sprintf( '
					SELECT
						DISTINCT ON ( d.id ) d.id,
						d.*,
						du.username_encrypted,
						du.password_encrypted
					FROM
						databases d
						JOIN database_users du ON ( d.id = du.database_id OR ( d.database_type_id = any( du.database_type_ids ) ) )
						JOIN clouds c ON ( d.cloud_id = c.id  )
					WHERE
						' . $strCondition . '
						AND du.deleted_on IS NULL
						AND du.database_user_type_id = %d
						AND d.database_type_id = %d
						AND c.details#>\'{country_codes}\' ? %s
					ORDER BY d.id, du.database_id NULLS LAST
					LIMIT 1',
				$intDatabaseUserTypeId,
				$intDatabaseTypeId,
				pg_escape_literal( $strCountryCode ) );

		return self::fetchDatabase( $strSql, $objDatabase );

	}

	public static function fetchDatabasesByReplicationRoleIdByDatabaseIds( $intReplicationRoleId, $arrintDatabaseIds, $intClusterId, $objDatabase, $boolIsCheckDeletedOn = true, $strCloudIds = NULL ) {

		if( false == valArr( $arrintDatabaseIds ) ) return false;

		$strDeletedOnCondition = ( true == $boolIsCheckDeletedOn ) ? ' AND deleted_on IS NULL AND deleted_by IS NULL ' : '';

		$strCloudCondition = ( true == valStr( $strCloudIds ) ) ? ' AND cloud_id IN ( ' . $strCloudIds . ' )' : '';

		$strSql = 'SELECT
						*
					FROM
						databases
					WHERE
					database_id IN ( ' . implode( ',', $arrintDatabaseIds ) . ' )
					AND cluster_id = ' . ( int ) $intClusterId . '
					AND replication_role_id = ' . ( int ) $intReplicationRoleId . $strDeletedOnCondition . $strCloudCondition;

		return self::fetchDatabases( $strSql, $objDatabase );
	}

	public static function loadRegionConnectDatabases( $arrintCloudIds, $objDatabase ) {

		$strSql = 'SELECT 
						*
					FROM
						databases
					WHERE
					database_type_id = ' . CDatabaseType::CONNECT . '
					AND cloud_id IN ( ' . implode( ',', $arrintCloudIds ) . ')
					AND database_id IS NULL';

		return self::fetchDatabases( $strSql, $objDatabase );
	}

}
?>
