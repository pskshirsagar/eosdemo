<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectiveStatusTypes
 * Do not add any new functions to this class.
 */

class CDirectiveStatusTypes extends CBaseDirectiveStatusTypes {

	public static function fetchDirectiveStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDirectiveStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDirectiveStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDirectiveStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDirectiveStatusType( $objDatabase ) {
		return self::fetchDirectiveStatusTypes( 'SELECT * FROM directive_status_types ORDER BY id', $objDatabase );
	}

}
?>