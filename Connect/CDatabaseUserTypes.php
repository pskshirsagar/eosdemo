<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseUserTypes
 * Do not add any new functions to this class.
 */

class CDatabaseUserTypes extends CBaseDatabaseUserTypes {

	public static function fetchDatabaseUserTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDatabaseUserType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDatabaseUserType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDatabaseUserType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDatabaseUserTypes( $objDatabase ) {
		return self::fetchDatabaseUserTypes( 'SELECT * FROM database_user_types WHERE is_published = 1  ORDER BY order_num', $objDatabase );
	}
}
?>