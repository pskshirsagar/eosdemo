<?php

class CMigrationToken extends CBaseMigrationToken {

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->m_strPassword = ( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function getPassword() {
		if( false == valStr( $this->m_strPassword ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strPassword, CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['password'] ) ) $this->setPassword( $arrmixValues['password'] );
	}

	/**
	 * Create Functions
	 */

	public function createPropertyTransfer() {

		$objPropertyTransfer = new CPropertyTransfer();
		$objPropertyTransfer->setCid( $this->getCid() );
		$objPropertyTransfer->setMigrationTokenId( $this->getId() );
		$objPropertyTransfer->setPropertyTransferStatusTypeId( CPropertyTransferStatusType::INITIALIZED );

		return $objPropertyTransfer;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valToken() {
		$boolIsValid = true;

		if( false == valStr( $this->getToken() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token name is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;

		if( false == valStr( $this->getPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
		} elseif( 6 > \Psi\CStringService::singleton()->strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password cannot be less than 6 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valIsExpired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valToken();
				$boolIsValid &= $this->valPassword();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>