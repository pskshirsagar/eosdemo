<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectives
 * Do not add any new functions to this class.
 */

class CDirectives extends CBaseDirectives {

	public static function fetchDirectiveByCidByDirectiveTypeId( $intCid, $intDirectiveTypeId, $objDatabase ) {
		return self::fetchDirective( 'SELECT * FROM directives WHERE website_id IS NULL AND cid = ' . ( int ) $intCid . ' AND directive_type_id = ' . ( int ) $intDirectiveTypeId, $objDatabase );
	}

	public static function fetchDirectiveByWebsiteIdByCidByDirectiveTypeId( $intWebsiteId, $intCid, $intDirectiveTypeId, $objDatabase ) {
		return self::fetchDirective( 'SELECT * FROM directives WHERE website_id = ' . ( int ) $intWebsiteId . ' AND cid = ' . ( int ) $intCid . ' AND directive_type_id = ' . ( int ) $intDirectiveTypeId, $objDatabase );
	}

	public static function fetchDirectiveByWebsiteDomainIdByCidByDirectiveTypeId( $intWebsiteDomainId, $intCid, $intDirectiveTypeId, $objDatabase ) {
		return self::fetchDirective( 'SELECT * FROM directives WHERE website_domain_id = ' . ( int ) $intWebsiteDomainId . ' AND cid = ' . ( int ) $intCid . ' AND directive_type_id = ' . ( int ) $intDirectiveTypeId, $objDatabase );
	}

	public static function fetchClientDirectivesByCids( $objDatabase, $arrintCids ) {

		$strSql = 'SELECT
						DISTINCT ON ( di.cid )
						di.id,
						di.cid,
						di.database_id,
						di.cluster_id
					FROM
						directives di
					WHERE
						di.directive_type_id = ' . ( int ) CDirectiveType::CLIENT . '
						AND di.cid IN ( ' . implode( ',', $arrintCids ) . ' )
					ORDER BY
						di.cid';

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByCid( $strDomain, $intDirectiveTypeId, $intCid, $objDatabase ) {
		$strWhereSql = ' WHERE deleted_by IS NULL AND deleted_on IS NULL AND domain = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) . '\'  AND directive_type_id = ' . ( int ) $intDirectiveTypeId . 'AND cid IS NOT NULL AND cid != ' . ( int ) $intCid;
		return self::fetchRowCount( $strWhereSql, 'directives', $objDatabase );
	}

	public static function fetchCompetingDirectiveCountByDomainByDirectiveTypeIdByWebsiteId( $strDomain, $intDirectiveTypeId, $intWebsiteId, $objDatabase ) {
		$strWhereSql = 'WHERE domain = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) . '\'  AND directive_type_id = ' . ( int ) $intDirectiveTypeId . ' AND website_id IS NOT NULL AND website_id != ' . ( int ) $intWebsiteId;
		return self::fetchRowCount( $strWhereSql, 'directives', $objDatabase );
	}

	public static function fetchDirectiveByDomainByDirectiveTypeId( $strDomain, $intDirectiveTypeId, $objDatabase ) {
		$strWhereSql = 'WHERE domain = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) . '\'  AND directive_type_id = ' . ( int ) $intDirectiveTypeId;
		return self::fetchRowCount( $strWhereSql, 'directives', $objDatabase );
	}

	public static function fetchPaginatedDirectives( $intPageNo, $intPageSize, $objDirectiveFilter, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {

			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' DESC';
			}
		} else {
			$strOrderBy = ' ORDER BY d.domain ASC';
		}

		$arrstrWhere = array();

		if( true == valArr( $objDirectiveFilter->getDirectiveTypeIds() ) ) {
			$arrstrWhere[] = 'd.directive_type_id IN( ' . implode( ', ', $objDirectiveFilter->getDirectiveTypeIds() ) . ' )';
		}

		$strSql = 'SELECT
					 *
					FROM
						directives d';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$strSql .= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchPaginatedDirectivesCount( $objDirectiveFilter, $objDatabase ) {

		$arrstrWhere = array();

		if( true == valArr( $objDirectiveFilter->getDirectiveTypeIds() ) ) {
			$arrstrWhere[] = 'directive_type_id IN( ' . implode( ', ', $objDirectiveFilter->getDirectiveTypeIds() ) . ' )';
		}

		$strSql = NULL;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql = ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		return self::fetchDirectiveCount( $strSql, $objDatabase );
	}

	public static function fetchDirectivesByKeyword( $strKeyword, $objDatabase ) {
		if( true == empty( $strKeyword ) ) return NULL;

		$strSql = 'SELECT
					 d.*
					FROM
						directives d
					WHERE
						lower( d.domain ) LIKE\'%' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strKeyword ) ) ) . '%\' limit 15';

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchCompanyDomainDetails( $arrmixCompanyDomainFilter, $objDatabase ) {

		if( false == valArr( $arrmixCompanyDomainFilter ) ) return NULL;

		$strWhereCondition = '';

		if( true == valStr( $arrmixCompanyDomainFilter['domain_name'] ) ) {
			$strWhereCondition .= 'AND lower( d.domain ) LIKE\'%' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $arrmixCompanyDomainFilter['domain_name'] ) ) ) . '%\'';
		}

		if( true == valArr( $arrmixCompanyDomainFilter['client_ids'] ) ) {
			$strWhereCondition .= 'AND d.cid IN ( ' . implode( ', ', $arrmixCompanyDomainFilter['client_ids'] ) . ')';
		} elseif( true == valStr( $arrmixCompanyDomainFilter['client_ids'] ) ) {
			$strWhereCondition .= 'AND d.cid IN ( ' . $arrmixCompanyDomainFilter['client_ids'] . ')';
		}

		if( true == valArr( $arrmixCompanyDomainFilter['domain_type_ids'] ) ) {
			$strWhereCondition .= 'AND d.directive_type_id IN ( ' . implode( ', ', $arrmixCompanyDomainFilter['domain_type_ids'] ) . ')';
		} elseif( true == valStr( $arrmixCompanyDomainFilter['domain_type_ids'] ) ) {
			$strWhereCondition .= 'AND d.directive_type_id IN ( ' . $arrmixCompanyDomainFilter['domain_type_ids'] . ')';
		}

		if( true == valArr( $arrmixCompanyDomainFilter['domain_status'] ) ) {

			$strWhereCondition .= 'AND d.directive_status_type_id IN ( ' . implode( ', ', $arrmixCompanyDomainFilter['domain_status'] ) . ')';

			if( true == in_array( CDirectiveStatusType::ACTIVE, $arrmixCompanyDomainFilter['domain_status'], true ) ) {
				$strWhereCondition .= ' AND d.deleted_on IS NULL';
			}
		} elseif( true == valStr( $arrmixCompanyDomainFilter['domain_status'] ) ) {

			$strWhereCondition .= 'AND d.directive_status_type_id IN ( ' . $arrmixCompanyDomainFilter['domain_status'] . ')';

			if( true == in_array( CDirectiveStatusType::ACTIVE, explode( ',', $arrmixCompanyDomainFilter['domain_status'] ) ) ) {
				$strWhereCondition .= ' AND d.deleted_on IS NULL';
			}
		}

		$strSql = 'SELECT
						d.id,
						d.cid,
						d.directive_type_id,
						d.domain
					FROM
						directives d
					WHERE
						d.domain IS NOT NULL 
						AND d.directive_status_type_id IS NOT NULL ' . $strWhereCondition;

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchDownloadDirectiveData( $objDirectiveFilter, $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						d.cid,
						d.domain,
						db.database_name,
						cl.name as cluster_name,
						dt.name as directive_type_name
					FROM
						directives d
						JOIN databases AS db ON ( d.database_id = db.id )
						JOIN clusters AS cl ON ( d.cluster_id = cl.id )
						JOIN directive_types AS dt ON ( d.directive_type_id = dt.id )
						WHERE dt.id IN ( ' . implode( ', ', $objDirectiveFilter->getDirectiveTypeIds() ) . ' ) ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDirectivesByGlobalUser( $strGlobalUser, $objDatabase ) {

		$strSql = 'SELECT
						di.*
					FROM
						directives di
						JOIN global_users gu ON ( gu.cid = di.cid )
					WHERE
						lower( gu.username ) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $strGlobalUser ) ) ) . '\' AND gu.global_user_type_id = ' . CGlobalUserType::COMPANY_USER . ' LIMIT 1';

		return self::fetchDirective( $strSql, $objDatabase );
	}

	public static function fetchDirectiveByCid( $intCid, $objDatabase ) {

		$strSql = 'SELECT * FROM directives WHERE cid = ' . ( int ) $intCid . ' LIMIT 1';
		return self::fetchDirective( $strSql, $objDatabase );
	}

	public static function fetchDirectiveByWebsiteDomainIdByCid( $intWebsiteDomainId, $intCid, $objDatabase ) {
		return self::fetchDirective( 'SELECT * FROM directives WHERE website_domain_id = ' . ( int ) $intWebsiteDomainId . ' AND cid = ' . ( int ) $intCid, $objDatabase );
	}

	public static function fetchFullDomainDirectivesByDirectiveTypeId( $intDirectiveTypeId, $objDatabase ) {
		return self::fetchDirectives( 'SELECT * FROM directives WHERE directive_type_id = ' . ( int ) $intDirectiveTypeId . ' AND domain NOT LIKE \'%.%.%\'  ORDER BY domain;', $objDatabase );
	}

	public static function fetchDirectivesDomainsByClusterIdByDirectiveTypeIds( $intClusterId, $arrintDirectiveTypeIds, $objDatabase ) {
		$strSql = 'SELECT directive_type_id, domain FROM directives WHERE cluster_id = ' . ( int ) $intClusterId . ' AND directive_type_id in ( ' . implode( ',', $arrintDirectiveTypeIds ) . ' ) GROUP BY directive_type_id, domain;';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectivesByClusterIds( $arrintClusterIds, $objDatabase ) {
		return self::fetchDirectives( 'SELECT * FROM directives WHERE cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ' ) AND domain IS NOT NULL AND deleted_on IS NULL;', $objDatabase );
	}

	public static function fetchDirectivesDomainsByCid( $intCid, $objDatabase, $boolIsFetchData = false ) {

		if( false == is_numeric( $intCid ) ) return NULL;
		$strSql = 'SELECT
						id,
						domain
					FROM
						directives
					WHERE
						cid = ' . ( int ) $intCid . '
						AND domain NOT LIKE \'%deleted_website%\'
					ORDER BY domain ASC';

		if( true == $boolIsFetchData ) return fetchData( $strSql, $objDatabase );

		return array_map( 'array_pop', fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchCustomDirectiveByDomainByDirectiveTypeId( $strDomain, $intDirectiveTypeId, $objDatabase ) {
		$strSql = 'SELECT * FROM directives WHERE domain = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomain ) ) . '\'  AND directive_type_id = ' . ( int ) $intDirectiveTypeId;
		return self::fetchDirective( $strSql, $objDatabase );
	}

	public static function fetchDirectiveDetailsById( $intDomainId, $objDatabase ) {
		if( false == is_numeric( $intDomainId ) ) return NULL;

		$strSql = '	SELECT
						d.id,
						d.cid,
						d.domain,
						d.website_domain_id,
						d.database_id,
						d.hosted_at,
						d.website_id,
						db.database_name,
						cl.name as cluster_name,
						dt.name as directive_type_name,
						CASE
							WHEN d.website_domain_id IS NULL AND d.deleted_on IS NOT NULL AND d.deleted_by IS NOT NULL
							THEN  \'Deleted\'
							ELSE dst.name
						END as domain_status_type_name
					FROM
						directives d
						JOIN databases AS db ON ( d.database_id = db.id )
						JOIN clusters AS cl ON ( d.cluster_id = cl.id )
						JOIN directive_types AS dt ON ( d.directive_type_id = dt.id )
						LEFT JOIN directive_status_types dst ON ( dst.id = d.directive_status_type_id )
					WHERE
						d.id = ' . ( int ) $intDomainId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectiveByWebsiteIdsByCid( $arrintWebsiteIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintWebsiteIds ) || false == is_numeric( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						directives
					WHERE
						website_id IN ( ' . implode( ',', $arrintWebsiteIds ) . ' )
						AND cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectiveByWebsiteDomainIdByDirectiveTypeIdByByCid( $intWebsiteDomainid, $intDirectiveTypeId, $intCid, $objDatabase ) {
		$strSql = 'SELECT * FROM directives WHERE deleted_by IS NULL AND deleted_on IS NULL AND website_domain_id = ' . ( int ) $intWebsiteDomainid . ' AND directive_type_id = ' . ( int ) $intDirectiveTypeId . ' AND cid IS NOT NULL AND cid = ' . ( int ) $intCid;
		return self::fetchDirective( $strSql, $objDatabase );
	}

	public static function fetchAllActiveDirectiviesByCidsByDomainName( $arrintCids, $strDomainName, $objDatabase ) {

		if( false == valArr( $arrintCids ) || false == valStr( $strDomainName ) ) return NULL;

		$strSql = 'SELECT
						d.id,
						d.cid,
						d.directive_type_id,
						d.domain
					FROM
						directives d
					WHERE
						d.domain IS NOT NULL 
						AND d.deleted_on IS  NULL
						AND d.domain LIKE \'%' . addslashes( \Psi\CStringService::singleton()->strtolower( $strDomainName ) ) . '%\'
						AND cid IN ( ' . implode( ',', $arrintCids ) . ' )
						ORDER BY domain ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectivesByIds( $arrintDirectiveds, $objDatabase ) {

		if( false == valArr( $arrintDirectiveds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						directives di
					WHERE
						di.id IN ( ' . implode( ',', $arrintDirectiveds ) . ' )
					ORDER BY
						di.id';

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchClientDomainByWebsiteDomain( $strDomain, $objDatabase ) {

		$strSql = ' SELECT
					    domain
					FROM
					    directives
					WHERE
					    CID = 
					    (
					      SELECT
					          CID
					      FROM
					          directives
					      WHERE
					          domain = ' . '\'' . $strDomain . '\'
					          AND deleted_on IS NULL
					          AND deleted_by IS NULL
					       LIMIT 1
					    )
					    AND website_id IS NULL
					    AND website_domain_id IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectivesByCidByTypeId( $intCid, $intDirectiveTypeId, $objDatabase ) {

		if( false == valId( $intCid ) || false == valId( $intDirectiveTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						di.domain
					FROM
						directives di
					WHERE
						di.directive_type_id = ' . ( int ) $intDirectiveTypeId . '
						AND di.cid = ' . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDirectivesByWebsiteDomainIdsByDirectiveTypeIdByByCidByPropertyId( $arrintWebsiteDomainids, $intDirectiveTypeId, $intCid, $intPropertyId, $objDatabase ) {

		if( false == valArr( $arrintWebsiteDomainids ) && false == valId( intDirectiveTypeId ) && false == valId( $intCid ) && false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT 
						*
					FROM
						directives
					WHERE
						website_domain_id IN ( ' . implode( ',', $arrintWebsiteDomainids ) . ' )
						AND directive_type_id = ' . ( int ) $intDirectiveTypeId . '
						AND cid IS NOT NULL
						AND property_id = ' . ( int ) $intPropertyId . '
						AND cid = ' . ( int ) $intCid;

		return self::fetchDirectives( $strSql, $objDatabase );
	}

	public static function fetchExceptDomainsByCid( $intCid, $arrstrWebsiteSubDomains, $objDatabase ) {
		$strSql = "SELECT 
						DISTINCT d.domain 
					FROM 
						( VALUES ('" . $arrstrWebsiteSubDomains . "') )  d(domain) 
					EXCEPT 
					SELECT 
						domain 
					FROM 
						directives 
					WHERE 
						cid = " . ( int ) $intCid;

		return fetchData( $strSql, $objDatabase );

	}

}
?>