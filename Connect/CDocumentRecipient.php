<?php

class CDocumentRecipient extends CBaseDocumentRecipient {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valEmailAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastLogin() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLastLoginAttemptOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLoginAttemptCount() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPasswordResetToken() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsDisabled() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>