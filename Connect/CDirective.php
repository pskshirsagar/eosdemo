<?php

class CDirective extends CBaseDirective {

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolReturnSqlOnly ) {
			return $boolIsValid;
		}

		// insert deployment for rebuild dns for standard
		if( CCluster::STANDARD != $this->getClusterId() && CCloud::REFERENCE_ID_LINDON == CONFIG_CLOUD_REFERENCE_ID ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		// insert deployment for rebuild nginx for development environment
		if( CCluster::STANDARD == $this->getClusterId() && 'production' != CONFIG_ENVIRONMENT ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$strOldDomainName	= '';
		$boolIsValid		= true;

		if( false == is_null( $this->getId() ) ) {
			$arrstrData	= fetchData( 'SELECT domain FROM directives WHERE id = ' . $this->getId(), $objDatabase );

			if( true == valArr( $arrstrData ) ) {
				$strOldDomainName = $arrstrData[0]['domain'];
			}
		}

		$boolIsValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolReturnSqlOnly ) {
			return $boolIsValid;
		}

		if( '' != $strOldDomainName && $strOldDomainName == $this->getDomain() ) {
			return $boolIsValid;
		}

		// insert deployment for rebuild dns for standard
		if( CCluster::STANDARD != $this->getClusterId() ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		// insert deployment for rebuild nginx for development environment
		if( CCluster::STANDARD == $this->getClusterId() && 'production' != CONFIG_ENVIRONMENT ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = true ) {
		$boolIsValid = false;

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );
			$this->setDirectiveStatusTypeId( CDirectiveStatusType::AVAILABLE );
			$boolIsValid = parent::update( $intCurrentUserId, $objDatabase );
		} else {
			$boolIsValid = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( true == $boolReturnSqlOnly ) {
			return $boolIsValid;
		}

		// insert deployment for rebuild dns for standard
		if( CCluster::STANDARD != $this->getClusterId() ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		// insert deployment for rebuild nginx for development environment
		if( CCluster::STANDARD == $this->getClusterId() && 'production' != CONFIG_ENVIRONMENT ) {
			$boolIsValid = $this->insertDeploymentRequest( $this->getClusterId(), $intCurrentUserId );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insertDeploymentRequest( $intClusterId, $intCurrentUserId ) {
		$objDeployment = new CDeployment();
		$objDeployment->setDeploymentTypeId( CDeploymentType::RELOAD_F5_CONFIGURATION );
		$objDeployment->setDescription( 'Deployment for reload F5 configuration for standard track' );
		$objDeployment->setClusterId( $intClusterId );
		$objDeployment->setExecCommand( CDeploymentType::RELOAD_F5_CONFIGURATION_COMMAND );
		$objDeployment->setScheduledDatetime( 'NOW()' );
		$objDeployment->setDeploymentDatetime( 'NOW()' );

		return $objDeployment->insert( $intCurrentUserId, CDatabases::createDeployDatabase( $boolRandomizeConnections = false ) );
	}

}
?>