<?php

class CTableColumn extends CBaseTableColumn {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valTableId() {
        $boolIsValid = true;
        if( true == is_null( $this->getTableId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Table id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valKeywords() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsMissingFkey() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
    	$boolIsValid = true;

    	switch( $strAction ) {
    		case VALIDATE_INSERT:
    		case VALIDATE_UPDATE:
    			$boolIsValid &= $this->valTableId();
    			break;

    		case VALIDATE_DELETE:
    			break;

    		default:
    			// default case
    			break;
    	}

    	return $boolIsValid;
    }

}
?>