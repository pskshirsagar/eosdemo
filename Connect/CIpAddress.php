<?php

class CIpAddress extends CBaseIpAddress {

	protected $m_strIpAddressTypeName;

    /**
     * Set Functions
     *
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['ip_address_type_name'] ) && true == $boolDirectSet ) {
			$this->m_strIpAddressTypeName = trim( stripcslashes( $arrValues['ip_address_type_name'] ) );
		} elseif( true == isset( $arrValues['ip_address_type_name'] ) ) {
			$this->setIpAddressTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address_type_name'] ) : $arrValues['ip_address_type_name'] );
		}

		return;
    }

    public function setIpAddressTypeName( $strName ) {
    	$this->m_strIpAddressTypeName = CStrings::strTrimDef( $strName, 50, NULL, true );
    }

    /**
     * Get Functions
     *
     */

    public function getIpAddressTypeName() {
    	return $this->m_strIpAddressTypeName;
    }

    /**
     * Validate Functions
     *
     */

    public function valId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'IP address id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valIpAddressTypeId() {
        $boolIsValid = true;

        if( false == isset( $this->m_intIpAddressTypeId ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address_type_id', 'IP address type is required.' ) );
        }

        return $boolIsValid;
    }

  	public function valName() {
        $boolIsValid = true;

        if( false == isset( $this->m_strName ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
        }

        return $boolIsValid;
    }

	public function valIpAddress( $objConnectDatabase ) {
        $boolIsValid = true;

        if( false == isset( $this->m_strIpAddress ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );
        }

        if( true == valStr( $this->getIpAddress() ) && false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP address.' ) );
        } elseif( true == valStr( $this->getIpAddress() ) && 0 < CIpAddresses::fetchRowCount( ' WHERE ip_address=\'' . $this->getIpAddress() . '\' ' . ( 0 < $this->getId() ? ' AND id <> ' . $this->getId() : '' ), 'ip_addresses', $objConnectDatabase ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address' . $this->getIpAddress() . ' already exists. ' ) );
        }

        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction, $objConnectDatabase ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valIpAddressTypeId();
            	$boolIsValid &= $this->valName();
            	$boolIsValid &= $this->valIpAddress( $objConnectDatabase );
            	break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>