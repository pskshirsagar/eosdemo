<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseServerTypes
 * Do not add any new functions to this class.
 */

class CDatabaseServerTypes extends CBaseDatabaseServerTypes {

	public static function fetchDatabaseServerTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDatabaseServerType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDatabaseServerType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDatabaseServerType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDatabaseServerTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM database_server_types WHERE is_published = 1';

		return parent::fetchDatabaseServerTypes( $strSql, $objDatabase );
    }
}
?>