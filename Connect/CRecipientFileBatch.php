<?php

class CRecipientFileBatch extends CBaseRecipientFileBatch {

	protected $m_arrstrChunkFilesList;
	protected $m_strChunkFileName;
	protected $m_strBatchFileSize = 0;

    public function getFormatedExpiredOn() {
    	if( false == valStr( $this->getExpiresOn() ) ) return 'Never';

    	$strTodayDate = date( 'M d, Y' );
    	$strExpiredOn = date( 'M d, Y', strtotime( $this->getExpiresOn() ) );

    	if( $strTodayDate == $strExpiredOn ) {
    		return 'Today';
    	} elseif( date( 'M d, Y', strtotime( 'yesterday' ) ) == $strExpiredOn ) {
    		return 'Yesterday';
    	} elseif( date( 'M d, Y', strtotime( '+1 day' ) ) == $strExpiredOn ) {
    		return 'Tomorrow';
    	} else {
    		return $strExpiredOn;
    	}

   		return $this->getExpiresOn();
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDocumentRecipientId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valFileBatchId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valArchiveFileName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valExpiresOn() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function setChunkFilesList( $arrstrChunkFilesList ) {
	    $this->m_arrstrChunkFilesList = $arrstrChunkFilesList;
    }

    public function setBatchFileSize( $strBatchFileSize ) {
	    $this->m_strBatchFileSize = $strBatchFileSize;
    }

    public function setChunkFileName( $strChunkFileName ) {
	    $this->m_strChunkFileName = $strChunkFileName;
    }

	public function getChunkFilesList() {
		return $this->m_arrstrChunkFilesList;
	}

	public function getBatchFileSize() {
		return $this->m_strBatchFileSize;
	}

	public function getChunkFileName() {
		return $this->m_strChunkFileName;
	}

	public function insert( $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
					  ' . static::TABLE_NAME . '
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlDocumentRecipientId() . ', ' .
		          $this->sqlFileBatchId() . ', ' .
		          $this->sqlArchiveFileName() . ', ' .
		          $this->sqlExpiresOn() . ', ' .
		          $this->sqlIsPublished() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}
		$strSql = 'UPDATE
                      ' . static::TABLE_NAME . '
                    SET ';
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' cid = ' . $this->sqlCid() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName( 'cid' ) ) {
			$arrstrOriginalValueChanges['cid'] = $this->sqlCid();
		$strSql .= ' cid = ' . $this->sqlCid() . ',';
		$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' document_recipient_id = ' . $this->sqlDocumentRecipientId() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlDocumentRecipientId() ) != $this->getOriginalValueByFieldName( 'document_recipient_id' ) ) {
			$arrstrOriginalValueChanges['document_recipient_id'] = $this->sqlDocumentRecipientId();
			$strSql .= ' document_recipient_id = ' . $this->sqlDocumentRecipientId() . ',';
			$boolUpdate = true;
		}

		 if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' file_batch_id = ' . $this->sqlFileBatchId() . ',';
		 } elseif( CStrings::reverseSqlFormat( $this->sqlFileBatchId() ) != $this->getOriginalValueByFieldName( 'file_batch_id' ) ) {
			$arrstrOriginalValueChanges['file_batch_id'] = $this->sqlFileBatchId();
			$strSql .= ' file_batch_id = ' . $this->sqlFileBatchId() . ',';
			$boolUpdate = true;
		 }
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' archive_file_name = ' . $this->sqlArchiveFileName() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlArchiveFileName() ) != $this->getOriginalValueByFieldName( 'archive_file_name' ) ) {
			$arrstrOriginalValueChanges['archive_file_name'] = $this->sqlArchiveFileName();
			$strSql .= ' archive_file_name = ' . $this->sqlArchiveFileName() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlExpiresOn() ) != $this->getOriginalValueByFieldName( 'expires_on' ) ) {
			$arrstrOriginalValueChanges['expires_on'] = $this->sqlExpiresOn();
			$strSql .= ' expires_on = ' . $this->sqlExpiresOn() . ',';
			$boolUpdate = true;
		}
		if( false == $this->getAllowDifferentialUpdate() ) {
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
		} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
			$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
			$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
			$boolUpdate = true;
		}

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>