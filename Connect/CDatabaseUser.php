<?php

class CDatabaseUser extends CBaseDatabaseUser {

    /**
     * Get Functions
     */

    public function getUsername() {
    	if( false == valStr( $this->getUsernameEncrypted() ) ) {
    		return NULL;
	    }
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getUsernameEncrypted(), CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getPassword() {
		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function getMaskPassword() {
		return preg_replace( '/[[A-Za-z0-9-+=-_)(*&^%$#@!`~?[{|}]/', '*', $this->getPassword(), strlen( $this->getPassword() ) - 2 );
	}

    /**
     * Set Functions
     */

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

        if( true == isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
        if( true == isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );

        return;
    }

    public function setUsername( $strUsernameEncrypted ) {
    	if( true == valStr( $strUsernameEncrypted ) ) {
		    $this->setUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strUsernameEncrypted ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
	    }
	}

	public function setPassword( $strPasswordEncrypted ) {
    	if( true == valStr( $strPasswordEncrypted ) ) {
		    $this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPasswordEncrypted ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
	    }
	}

	/**
	 * Validation Functions
	 */

    public function valDatabaseUserTypeId() {

        $boolIsValid = true;

        if( true == is_null( $this->getDatabaseUserTypeId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_user_type_id', 'User type is required.' ) );
        }

        return $boolIsValid;
    }

    public function valDatabaseId() {
        $boolIsValid = true;

        if( true == is_null( $this->getDatabaseId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_id', 'Database is required.' ) );
        }

        return $boolIsValid;
    }

    public function valUsernameEncrypted( $objDatabase=NULL ) {
        $boolIsValid = true;

        if( NULL == $this->getUsername() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is required.' ) );
            return $boolIsValid;
        }

        if( NULL == $this->getDatabaseId() ) {
        	$boolIsValid = false;
        	return $boolIsValid;
        }
		$intCountConflictingDatabaseUserName = \Psi\Eos\Connect\CDatabaseUsers::createService()->fetchConflictingDatabaseUserName( $this->getUsername(), $this->getDatabaseId(), $this->getId(), $objDatabase );

		if( 0 < $intCountConflictingDatabaseUserName ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Username already exists for this Database.' ) );
			$boolIsValid = false;
		}

        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;

        if( NULL == $this->getPassword() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
            return $boolIsValid;
        }

        if( strlen( $this->getPassword() ) < 6 || strlen( $this->getPassword() ) > 30 ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must be 6-30 characters in length.' ) );

		}
        return $boolIsValid;
    }

    public function validate( $strAction, $objDatabase = NULL ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valDatabaseUserTypeId();
            	$boolIsValid &= $this->valDatabaseId();
            	$boolIsValid &= $this->valUsernameEncrypted( $objDatabase );
            	$boolIsValid &= $this->valPasswordEncrypted();
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    /**
     * Other Functions
     */

    public static function getDefaultDatabaseUserTypeIdByDatabaseTypeId( $intDatabaseTypeId ) {
    	$intDefaultUserTypeId = CDatabaseUserType::ENTRATA_HELP_SETTINGS;

    	$arrintPsDevelopers 	= [ CDatabaseType::ADMIN, CDatabaseType::PAYMENT ];

    	if( true == in_array( $intDatabaseTypeId, $arrintPsDevelopers ) ) {
    		$intDefaultUserTypeId = CDatabaseUserType::PS_DEVELOPER;

    	} elseif( CDatabaseType::VOIP == $intDatabaseTypeId ) {
    		$intDefaultUserTypeId = CDatabaseUserType::PS_VOIP;

    	} elseif( CDatabaseType::FREESWITCH == $intDatabaseTypeId ) {
    		$intDefaultUserTypeId = CDatabaseUserType::PS_FREESWITCH;

    	}

    	return $intDefaultUserTypeId;
    }

    public function delete( $intUserId, $objConnectDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objConnectDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>