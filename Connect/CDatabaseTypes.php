<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDatabaseTypes
 * Do not add any new functions to this class.
 */

class CDatabaseTypes extends CBaseDatabaseTypes {

	public static function fetchDatabaseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDatabaseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDatabaseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDatabaseType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDatabaseTypes( $objDatabase, $boolIsExcludeDbCommon = true ) {
		$strExcludeDbCommon = ( true == $boolIsExcludeDbCommon ) ? ' AND id NOT IN ( ' . CDatabaseType::DB_COMMON . ' )' : '';

		return self::fetchDatabaseTypes( 'SELECT * FROM database_types WHERE is_published = 1 ' . $strExcludeDbCommon, $objDatabase );
	}

    public static function fetchDatabaseTypesByIds( $arrintDatabaseTypeIds, $objDatabase ) {
    	if( false == valArr( $arrintDatabaseTypeIds ) ) return NULL;
    	return self::fetchDatabaseTypes( 'SELECT * FROM database_types WHERE id IN ( ' . implode( ',', $arrintDatabaseTypeIds ) . ')', $objDatabase );
    }

    public static function fetchAllDatabaseTypesByClusterIds( $arrintClusterIds, $objDatabase ) {

    	if( true == empty( $arrintClusterIds ) ) return NULL;

    	$strSql = 'SELECT
					    *
					FROM
					    database_types
					WHERE
					    id IN (
					            SELECT
					                DISTINCT ( d.database_type_id )
					            FROM
					                databases d
					            WHERE
					                d.deleted_on IS NULL
					                AND d.cluster_id IN ( ' . implode( ',', $arrintClusterIds ) . ')
					 );';

    	return self::fetchDatabaseTypes( $strSql, $objDatabase );
    }

	public static function fetchAllDatabaseTypesName( $objDatabase ) {
		return fetchData( 'SELECT name FROM database_types WHERE is_published = 1', $objDatabase );
	}

}
?>