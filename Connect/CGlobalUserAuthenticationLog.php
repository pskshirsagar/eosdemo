<?php

class CGlobalUserAuthenticationLog extends CBaseGlobalUserAuthenticationLog {

    public function __construct() {
        parent::__construct();

        $this->setIpAddress( getRemoteIpAddress() );

        return;
    }

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUserId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPreviousPassword() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLoginDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valLogoutDatetime() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIpAddress() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

    // other function

    public function calculateTimeSpent( $intLogOutTime, $intLoginTime ) {

    	$intdiff		= abs( $intLogOutTime - $intLoginTime );
    	$inthours 		= floor( $intdiff / ( 60 * 60 ) );
    	$intmins 		= floor( ( $intdiff - ( $inthours * 60 * 60 ) ) / ( 60 ) );
    	$intsecs 		= floor( ( $intdiff - ( ( $inthours * 60 * 60 ) + ( $intmins * 60 ) ) ) );
    	$intTotalTime 	= sprintf( '%02d', $inthours ) . ':' . sprintf( '%02d', $intmins ) . ':' . sprintf( '%02d', $intsecs );

    	return $intTotalTime;
    }
}
?>