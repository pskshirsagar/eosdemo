<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDirectiveTypes
 * Do not add any new functions to this class.
 */

class CDirectiveTypes extends CBaseDirectiveTypes {

	public static function fetchDirectiveTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDirectiveType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchDirectiveType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDirectiveType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllDirectiveTypes( $objDatabase ) {
		return self::fetchDirectiveTypes( 'SELECT * FROM directive_types', $objDatabase );
	}
}
?>