<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CTableStats
 * Do not add any new functions to this class.
 */

class CTableStats extends CBaseTableStats {

    public static function fetchTableStatsByTableIdsByCreatedOn( $arrintTableIds, $strCreatedOnDate, $objDatabase ) {

    	if( false == valArr( $arrintTableIds ) ) return NULL;

    	$strSql = 'SELECT * FROM table_stats WHERE table_id IN ( ' . implode( ', ', $arrintTableIds ) . ') AND to_char( created_on::DATE, \'YYYY-MM-DD\' ) LIKE to_char(\'' . $strCreatedOnDate . '\'::DATE, \'YYYY-MM-DD\' )';

    	return self::fetchObjects( $strSql, 'CTableStat', $objDatabase );

    }

    public static function fetchTableStatsByTableIdByCreatedOn( $intTableId, $strCreatedOnDate, $objDatabase ) {

    	$strSql = 'SELECT * FROM table_stats WHERE table_id = ' . ( int ) $intTableId . ' AND to_char( created_on::DATE, \'YYYY-MM-DD\' ) LIKE to_char(\'' . $strCreatedOnDate . '\'::DATE, \'YYYY-MM-DD\' )';

    	return self::fetchObjects( $strSql, 'CTableStat', $objDatabase );
    }

}
?>