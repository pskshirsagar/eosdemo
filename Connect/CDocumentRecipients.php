<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CDocumentRecipients
 * Do not add any new functions to this class.
 */

class CDocumentRecipients extends CBaseDocumentRecipients {

	public static function fetchDocumentRecipientByEmailAddress( $strEmailAddress, $objDatabase ) {
		if( false == valStr( $strEmailAddress ) ) return;

		$strSql = 'SELECT * FROM document_recipients WHERE email_address = \'' . $strEmailAddress . '\'';
		return self::fetchDocumentRecipient( $strSql, $objDatabase );
	}

	public static function fetchDocumentRecipientsEmailAddresses( $arrstrEmailAddresses, $objDatabase ) {
		if( false == valArr( $arrstrEmailAddresses ) ) return NULL;

		$strSql = 'SELECT id, email_address FROM document_recipients WHERE email_address IN ( \'' . implode( '\',\'', $arrstrEmailAddresses ) . '\' )';
		return self::fetchDocumentRecipients( $strSql, $objDatabase );
	}

}
?>