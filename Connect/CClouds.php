<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CClouds
 * Do not add any new functions to this class.
 */

class CClouds extends CBaseClouds {

	public static function fetchCountryCodes( $objDatabase ) {

		$arrstrCountryCodes = [];
		$strSql = 'SELECT
						jsonb_array_elements_text ( details #> \'{country_codes}\' ) as country_codes
					FROM
						clouds
					WHERE
						is_published = TRUE;';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			foreach( $arrmixResult as $arrstrCountry ) {
				$arrstrCountryCodes[] = $arrstrCountry['country_codes'];
			}
		}
		return $arrstrCountryCodes;
	}

	public static function fetchDomainByCountryCode( $strCountryCode, $objDatabase ) {
		if( false == valStr( $strCountryCode ) || false == valObj( $objDatabase, CDatabase::class ) ) {
			return NULL;
		}

		$strSql = sprintf(
			'SELECT
						domain
					FROM
						clouds
					WHERE
						is_published = TRUE
						AND details #> \'{country_codes}\' ? %s',
			pg_escape_literal( $objDatabase->getHandle(), $strCountryCode ) );

		return self::fetchColumn( $strSql, 'domain', $objDatabase );
	}

	public static function fetchAllPublishedClouds( $objDatabase, $boolIsArray = false ) {

		$strSql = 'SELECT
						id,
						name,
						region,
						domain
					FROM
						clouds
					WHERE
						is_published = true';
		if( true == $boolIsArray ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchClouds( $strSql, $objDatabase );
		}
	}

	public static function getCloudIdByReferenceId( $strReferenceId, $objDatabase, $boolIsObject = false ) {
		$objCloud = self::fetchCloud( 'SELECT * FROM clouds WHERE reference_id = \'' . $strReferenceId . '\'', $objDatabase );
		if( true == valObj( $objCloud, 'CCloud' ) ) {
			if( true == $boolIsObject ) {
				return $objCloud;
			} else {
				return $objCloud->getId();
			}
		}

		return false;
	}

	public static function getCountryCodesByReferenceId( $strReferenceId, $objDatabase ) {

		$arrstrCountryCodes = [];
		$strSql = 'SELECT
						jsonb_array_elements_text ( details #> \'{country_codes}\' ) as country_codes
					FROM
						clouds
					WHERE
						is_published = TRUE AND reference_id = \'' . $strReferenceId . '\';';

		$arrmixResult = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixResult ) ) {
			foreach( $arrmixResult as $arrstrCountry ) {
				$arrstrCountryCodes[] = $arrstrCountry['country_codes'];
			}
		}
		return $arrstrCountryCodes;
	}

}
?>
