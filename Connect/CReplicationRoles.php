<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CReplicationRoles
 * Do not add any new functions to this class.
 */

class CReplicationRoles extends CBaseReplicationRoles {

	public static function fetchReplicationRoles( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CReplicationRole::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReplicationRole( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CReplicationRole::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchReplicationRolesByIds( $arrintReplicationRolesIds, $objDatabase ) {

		$strSql = 'SELECT * FROM replication_roles WHERE id IN ( ' . implode( ',', $arrintReplicationRolesIds ) . ' ) AND is_published = true ORDER BY id';
		return self::fetchReplicationRoles( $strSql, $objDatabase );
	}

}
?>