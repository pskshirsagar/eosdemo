<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CIpAddressTypes
 * Do not add any new functions to this class.
 */

class CIpAddressTypes extends CBaseIpAddressTypes {

	public static function fetchAllIpAddressTypes( $objDatabase ) {
		return self::fetchIpAddressTypes( 'SELECT * FROM ip_address_types ', $objDatabase );
	}

    public static function fetchIpAddressTypes( $strSql, $objDatabase ) {
        return parent::fetchCachedObjects( $strSql, 'CIpAddressType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchIpAddressType( $strSql, $objDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CIpAddressType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }
}
?>