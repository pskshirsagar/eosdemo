<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Connect\CGlobalUserTypes
 * Do not add any new functions to this class.
 */

class CGlobalUserTypes extends CBaseGlobalUserTypes {

	public static function fetchGlobalUserTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGlobalUserType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGlobalUserType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGlobalUserType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>