<?php

require_once( PATH_EOS_DEFINES . 'ValidateActions.defines.php' );

class CDatabase extends CBaseDatabase {

	const ADMIN 			= 1;
	const ENTRATA1 			= 2;
	const EMAIL 			= 3;
	const VOIP 				= 4;
	const DNS 				= 5;
	const DDBMAIL			= 6;
	const WEBSITE_STAT		= 7;
	const LIVE_SUPPORT		= 8;
	const ENTRATA2 			= 9;
	const ENTRATA3 			= 10;
	const SMS	 			= 11;
	const ENTRATA4 			= 12;
	const ENTRATA5 			= 13;
	const CONNECT 			= 14;
	const CHAT 				= 15;
	const UTILITIES			= 16;
	const INSURANCE			= 17;
	const LOGS 				= 18;
	const ENTRATA6 			= 19;
	const ENTRATA7 			= 20;
	const ENTRATA8 			= 21;
	const ENTRATA9 			= 22;
	const PAYMENTS 			= 23;
	const COMPETITOR 		= 24;
	const REVENUE			= 25;
	const ENTRATA10			= 26;
	const ENTRATA11			= 27;
	const ENTRATA12			= 28;
	const ENTRATA7_BASE		= 48;
	const SCREENING			= 76;
	const FREESWITCH		= 77;
	const ENTRATA13 		= 126;
	const ENTRATA14 		= 127;
	const ENTRATA15 		= 134;
	const TEST_CASE			= 151;
	const ENTRATA16 		= 158;
	const ENTRATA17 		= 187;
	const ENTRATA18			= 236;
	const ZEND_QUEUE		= 250;
	const ENTRATA19			= 256;
	const ENTRATA20			= 278;
	const ENTRATA21			= 279;
	const ENTRATA22			= 314;
	const ENTRATA23			= 376;
	const ENTRATA24			= 409;
	const ENTRATA25			= 410;
	const ENTRATA_DOC		= 411;
	const ENTRATA27			= 417;
	const ENTRATA28			= 419;
	const ENTRATA29			= 421;
	const ENTRATA30			= 423;
	const ENTRATA32			= 427;
	const ENTRATA31 		= 425;
	const ENTRATA33			= 428;
	const ENTRATA34			= 429;
	const ENTRATA35			= 430;
	const ENTRATA36			= 435;
	const ENTRATA39			= 445;
	const ENTRATA40			= 438;
	const ENTRATA41			= 475;
	const ENTRATA42			= 477;
	const ENTRATA46			= 486;
	const ENTRATA44			= 480;
	const ENTRATA53			= 504;
	const ENTRATA54			= 508;
	const ENTRATA55			= 516;
	const ENTRATA56			= 520;
	const ENTRATA50			= 499;
	const ENTRATA52			= 502;
	const ENTRATA58			= 535;
	const ENTRATA59			= 539;
	const ENTRATA60			= 547;
	const ENTRATA61			= 549;
	const ENTRATA62			= 551;
	const ENTRATA63			= 557;
	const ENTRATA64			= 559;
	const ENTRATA65			= 561;
	const ENTRATA69			= 624;
	const ENTRATA70			= 625;
	const ENTRATA71			= 640;
	const ENTRATA72			= 645;
	const ENTRATA75			= 695;
	const ENTRATA76			= 688;
	const ENTRATA77			= 696;
	const ENTRATA78			= 701;
	const ENTRATA79			= 723;
	const ENTRATA80			= 774;
	const ENTRATA81			= 773;
	const ENTRATA82			= 776;
	const ENTRATA83			= 785;
	const ENTRATA84			= 789;
	const ENTRATA87			= 812;
	const ENTRATA90			= 844;
	const ENTRATA92			= 857;
	const ENTRATA95         = 887;
	const ENTRATA97         = 968;
	const ENTRATA98         = 1036;
	const ENTRATA99         = 1082;
	const ENTRATA100        = 1110;
	const ENTRATA101        = 1149;
	const ENTRATA103        = 1199;
	const ENTRATA108        = 1650;
	const ENTRATA109        = 1433;
	const MESSAGING			= 511;
	const DEPLOY			= 529;
	const BIXPRESS			= 513;
	const SSISLOG			= 514;
	const ENTRATA_STANDARD_TERMINATED	= 415;
	const ENTRATA_STANDARD_TERMINATED01	= 769;
	const ENTRATA_STANDARD_TERMINATED02	= 777;
	const ENTRATA_RAPID_TERMINATED		= 742;
	const ENTRATA_RAPID_TEMPLATE		= 436;
	const ENTRATA_STANDARD_TEMPLATE		= 437;
	const ENTRATA_TRANSITION_TEMPLATE	= 438;
	const INTERNAL_TECHNICAL_ASSESSMENT = 482;
	const HA				= 596;
	const BEACONBOX			= 495;
	const ANALYTICS			= 700;

	// define constant to set sql timeout value, should not be greater than 600 seconds
	const MAX_SQL_TIMEOUT = 600;
	private static $c_arrintConnectionCountByTypes = [];
	private static $c_arrintQueryCountByTypes = [];
	private static $c_arrintWriteQueryCountByTypes = [];
	private static $c_intOpenConnectionCount = 0;
	private static $c_intRollbackCount = 0;

	protected $m_resHandle;
	protected $m_strPdoDsn;

	private $m_boolIsOpen;
	private $m_boolIsConnectionInitialized;
	private $m_boolIsDieOnConnectionError;
	private $m_boolIsApplicationNameAdded;
	private $m_boolUseReplica;
	private $m_boolUseMasterDatabaseForWrite = true;

	// Following variable is needed for mysql Dataset. PBY
	protected $m_objResult;
	protected $m_strUsernameEncrypted;
	protected $m_strPasswordEncrypted;
	protected $m_boolIsTransaction;
	protected $m_intCid;
	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_intDatabaseUserTypeId;
	protected $m_arrintCids;
	private $m_intConnectionTimeout;

	private $m_arrintReplicaDatabaseIds;

	protected $m_strHardwareName;
	protected $m_strHardwareIpAddress;
	protected $m_strDatabseType;
	protected $m_strHardwareDellServiceTag;
	protected $m_strShippedOn;
	protected $m_strDatabaseTypeName;

	private $m_objReplicaDatabase;
	private $m_objMasterDatabase;

	public static $c_arrintEntrataDatabaseIds = array(
		CDatabase::ENTRATA1,
		CDatabase::ENTRATA2,
		CDatabase::ENTRATA3,
		CDatabase::ENTRATA4,
		CDatabase::ENTRATA5,
		CDatabase::ENTRATA6,
		CDatabase::ENTRATA7,
		CDatabase::ENTRATA8,
		CDatabase::ENTRATA9,
		CDatabase::ENTRATA10,
		CDatabase::ENTRATA11,
		CDatabase::ENTRATA12,
		CDatabase::ENTRATA13,
		CDatabase::ENTRATA14,
		CDatabase::ENTRATA15,
		CDatabase::ENTRATA16,
		CDatabase::ENTRATA17,
		CDatabase::ENTRATA18,
		CDatabase::ENTRATA20,
		CDatabase::ENTRATA22,
	    CDatabase::ENTRATA23,
	    CDatabase::ENTRATA24,
		CDatabase::ENTRATA25,
	    CDatabase::ENTRATA27,
		CDatabase::ENTRATA28,
		CDatabase::ENTRATA32,
		CDatabase::ENTRATA39,
		CDatabase::ENTRATA40,
	    CDatabase::ENTRATA41,
		CDatabase::ENTRATA53,
		CDatabase::ENTRATA54,
		CDatabase::ENTRATA55,
		CDatabase::ENTRATA56,
		CDatabase::ENTRATA50,
		CDatabase::ENTRATA52,
		CDatabase::ENTRATA58,
		CDatabase::ENTRATA59,
		CDatabase::ENTRATA60,
		CDatabase::ENTRATA61,
		CDatabase::ENTRATA62,
		CDatabase::ENTRATA63,
		CDatabase::ENTRATA64,
		CDatabase::ENTRATA65,
		CDatabase::ENTRATA69,
		CDatabase::ENTRATA70,
		CDatabase::ENTRATA71,
		CDatabase::ENTRATA72,
		CDatabase::ENTRATA75,
		CDatabase::ENTRATA76,
		CDatabase::ENTRATA77,
		CDatabase::ENTRATA78,
		CDatabase::ENTRATA79,
		CDatabase::ENTRATA80,
		CDatabase::ENTRATA81,
		CDatabase::ENTRATA82,
		CDatabase::ENTRATA83,
		CDatabase::ENTRATA84,
		CDatabase::ENTRATA87,
		CDatabase::ENTRATA92,
		CDatabase::ENTRATA95,
		CDatabase::ENTRATA97,
		CDatabase::ENTRATA98,
		CDatabase::ENTRATA100,
		CDatabase::ENTRATA_RAPID_TERMINATED,
		CDatabase::ENTRATA_STANDARD_TERMINATED,
		CDatabase::ENTRATA_STANDARD_TERMINATED01,
		CDatabase::ENTRATA_STANDARD_TERMINATED02
	);

	public static $c_arrintServiceDatabases = array(
		CDatabase::ADMIN,
		CDatabase::EMAIL,
		CDatabase::VOIP,
		CDatabase::DNS,
		CDatabase::DDBMAIL,
		CDatabase::WEBSITE_STAT,
		CDatabase::LIVE_SUPPORT,
		CDatabase::SMS,
		CDatabase::CHAT,
		CDatabase::CONNECT,
		CDatabase::UTILITIES,
		CDatabase::INSURANCE,
		CDatabase::LOGS,
		CDatabase::PAYMENTS,
		CDatabase::COMPETITOR,
		CDatabase::REVENUE,
		CDatabase::SCREENING,
		CDatabase::FREESWITCH,
		CDatabase::TEST_CASE,
		CDatabase::ZEND_QUEUE,
		CDatabase::MESSAGING,
		CDatabase::DEPLOY,
		CDatabase::BIXPRESS,
		CDatabase::SSISLOG,
		CDatabase::HA
	);

	public static $c_arrintDebugDatabaseIds;

	public function __construct() {
		parent::__construct();

		$this->m_resHandle            = NULL;
		$this->m_boolIsTransaction 	  = false;
		$this->m_boolIsOpen 	      = false;
		$this->m_boolIsConnectionInitialized = false;
		$this->m_boolIsDieOnConnectionError = true;
		$this->m_boolIsApplicationNameAdded = false;
		$this->m_intConnectionTimeout = 0;
		$this->m_boolUseReplica = false;
		self::$c_arrintDebugDatabaseIds = [];
		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getHandle() {
		return $this->m_resHandle;
	}

	public function getResult() {
		return $this->m_objResult;
	}

	public function getUsername() {
		if( false == valStr( $this->getUsernameEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getUsernameEncrypted(), CONFIG_SODIUM_KEY_LOGIN_USERNAME, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_USERNAME ] );
	}

	public function getPassword() {
		if( false == valStr( $this->getPasswordEncrypted() ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getPasswordEncrypted(), CONFIG_SODIUM_KEY_LOGIN_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_LOGIN_PASSWORD ] );
	}

	public function getUsernameEncrypted() {
		return $this->m_strUsernameEncrypted;
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function getIsTransaction() {
		return $this->m_boolIsTransaction;
	}

	public function getCids() {
		return $this->m_arrintCids;
	}

	public function getHardwareName() {
		return $this->m_strHardwareName;
	}

	public function getHardwareIpAddress() {
		return $this->m_strHardwareIpAddress;
	}

	public function getDatabaseType() {
		return $this->m_strDatabseType;
	}

	public function getHardwareDellServiceTag() {
		return $this->m_strHardwareDellServiceTag;
	}

	public function getShippedOn() {
		return $this->m_strShippedOn;
	}

	public function getDatabaseTypeName() {
		return $this->m_strDatabaseTypeName;
	}

	public function getDatabaseUserTypeId() {
		return $this->m_intDatabaseUserTypeId;
	}

	public function getIsDieOnConnectionError() {
		return $this->m_boolIsDieOnConnectionError;
	}

	public function getIsOpen() {
		return $this->m_boolIsOpen;
	}

	public function getUseReplica() {
		return $this->m_boolUseReplica;
	}

	public function getReplicaDatabaseIds() {
		return $this->m_arrintReplicaDatabaseIds;
	}

	public function getUseMasterDatabaseForWrite() {
		return $this->m_boolUseMasterDatabaseForWrite;
	}

	public function getIsConnectionInitialized() {
		return $this->m_boolIsConnectionInitialized;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->m_intWebsiteId = CStrings::strToIntDef( $intWebsiteId, NULL, false );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setHandle( $resHandle ) {
		$this->m_resHandle = $resHandle;

		return true;
	}

	public function setResult( $resResult ) {
		$this->m_objResult = $resResult;

		return true;
	}

	public function setUsername( $strUsername ) {
		if( true == valStr( $strUsername ) ) {
			$this->setUsernameEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strUsername ), CONFIG_SODIUM_KEY_LOGIN_USERNAME ) );
		}
	}

	public function setPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPasswordEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( trim( $strPassword ), CONFIG_SODIUM_KEY_LOGIN_PASSWORD ) );
		}
	}

	public function setUsernameEncrypted( $strUsernameEncrypted ) {
		$this->m_strUsernameEncrypted = CStrings::strTrimDef( $strUsernameEncrypted, 240, NULL, true );
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->m_strPasswordEncrypted = CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['username_encrypted'] ) ) 	$this->setUsernameEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['username_encrypted'] ) : $arrmixValues['username_encrypted'] );
		if( true == isset( $arrmixValues['password_encrypted'] ) ) 	$this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['password_encrypted'] ) : $arrmixValues['password_encrypted'] );
		if( true == isset( $arrmixValues['cid'] ) ) 				$this->setCid( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['cid'] ) : $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['website_id'] ) ) 			$this->setWebsiteId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['website_id'] ) : $arrmixValues['website_id'] );
		if( true == isset( $arrmixValues['property_id'] ) ) 		$this->setPropertyId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_id'] ) : $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['database_user_type_id'] ) ) 	$this->setDatabaseUserTypeId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['database_user_type_id'] ) : $arrmixValues['database_user_type_id'] );

		if( true == isset( $arrmixValues['hardware_name'] ) ) 		$this->setHardwareName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hardware_name'] ) : $arrmixValues['hardware_name'] );
		if( true == isset( $arrmixValues['hardware_ip_address'] ) ) $this->setHardwareIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['hardware_ip_address'] ) : $arrmixValues['hardware_ip_address'] );
		if( true == isset( $arrmixValues['databse_type'] ) ) 		$this->setDatabseType( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['databse_type'] ) : $arrmixValues['databse_type'] );
		if( true == isset( $arrmixValues['dell_service_tag'] ) ) 	$this->setHardwareDellServiceTag( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['dell_service_tag'] ) : $arrmixValues['dell_service_tag'] );
		if( true == isset( $arrmixValues['shipped_on'] ) ) 			$this->setShippedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['shipped_on'] ) : $arrmixValues['shipped_on'] );

		if( true == isset( $arrmixValues['database_type_name'] ) ) 	$this->setDatabaseTypeName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['database_type_name'] ) : $arrmixValues['database_type_name'] );
		return;
	}

	public function setCids( $arrintCids ) {
		$this->m_arrintCids = $arrintCids;
	}

	public function setHardwareName( $strHardwareName ) {
		$this->m_strHardwareName = $strHardwareName;
	}

	public function setHardwareIpAddress( $strHardwareIpAddress ) {
		$this->m_strHardwareIpAddress = $strHardwareIpAddress;
	}

	public function setDatabseType( $strDatabseType ) {
		$this->m_strDatabseType = $strDatabseType;
	}

	public function setHardwareDellServiceTag( $strHardwareDellServiceTag ) {
		$this->m_strHardwareDellServiceTag = $strHardwareDellServiceTag;
	}

	public function setShippedOn( $strShippedOn ) {
		$this->m_strShippedOn	= $strShippedOn;
	}

	public function setDatabaseTypeName( $strDatabaseTypeName ) {
		$this->m_strDatabaseTypeName = $strDatabaseTypeName;
	}

	public function setDatabaseUserTypeId( $intDatabaseUserTypeId ) {
		$this->m_intDatabaseUserTypeId = $intDatabaseUserTypeId;
	}

	public function setConnectionTimeout( $intConnectionTimeout ) {
		$this->m_intConnectionTimeout = $intConnectionTimeout;
	}

	public function setIsDieOnConnectionError( $boolIsDieOnConnectionError ) {
		$this->m_boolIsDieOnConnectionError = $boolIsDieOnConnectionError;
	}

	public function setLocaleCode( $strLocaleCode, $strDefaultLocaleCode, $strTargetLocaleCode = NULL, $boolAcceptMachineTranslated = false ) {
		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return $this->execute( sprintf( 'SELECT util_set_locale( %s, %s, %s, %s::bool );',
													pg_escape_literal( $this->getHandle(), $strLocaleCode ),
													pg_escape_literal( $this->getHandle(), $strDefaultLocaleCode ),
													pg_escape_literal( $this->getHandle(), $strTargetLocaleCode ),
													pg_escape_literal( $this->getHandle(), ( int ) $boolAcceptMachineTranslated ) ) );
				break;

			case CDatabaseServerType::MYSQL:
				return true;
				break;

			case CDatabaseServerType::MSSQL:
				return true;
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function setUseReplica( $boolUseReplica, $intAcceptedReplicaDatabaseLagTime = NULL ) {
		$this->m_boolUseReplica = $this->validateUseReplicaDatabase( $boolUseReplica, $intAcceptedReplicaDatabaseLagTime );
		if( $this->m_boolUseReplica ) $this->setUseMasterDatabaseForWrite( false );
	}

	private function validateUseReplicaDatabase( $boolUseReplica, $intAcceptedReplicaDatabaseLagTime = NULL ) {

		if( $boolUseReplica ) {
			$intId = ( CReplicationRole::RO == $this->getReplicationRoleId() ) ? $this->getDatabaseId() : $this->getId();
			$arrmixReplicaConfigs = CCache::fetchObject( 'database-replica-config-' . $intId );
			if( \valArr( $arrmixReplicaConfigs ) ) {
				foreach( $arrmixReplicaConfigs as $arrmixReplicaConfig ) {
					if( -1 != $arrmixReplicaConfig['lag_time'] && ( is_null( $intAcceptedReplicaDatabaseLagTime ) || $intAcceptedReplicaDatabaseLagTime >= $arrmixReplicaConfig['lag_time'] ) ) {
						$this->m_arrintReplicaDatabaseIds[] = $arrmixReplicaConfig['database_id'];
					}
				}
				if( false == \valArr( $this->m_arrintReplicaDatabaseIds ) ) {
					$boolUseReplica = false;
				}
			}
		}
		return $boolUseReplica;
	}

	public function setUseMasterDatabaseForWrite( $boolUseMasterDatabaseForWrite ) {
		if( !$boolUseMasterDatabaseForWrite && !$this->getUseReplica() ) throw new \Exception( 'You can only disable master database for writes when using RO database.' );
		$this->m_boolUseMasterDatabaseForWrite = $boolUseMasterDatabaseForWrite;
	}

	public function setBoolIsApplicationNameAdded( $boolValue ) {
		$this->m_boolIsApplicationNameAdded = $boolValue;
	}

	// function used to bind member variable of database from array.

	public function mapValues( $arrmixValues ) {
		if( true == isset( $arrmixValues['cid'] ) )  			$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['website_id'] ) )		$this->setWebsiteId( $arrmixValues['website_id'] );
		if( true == isset( $arrmixValues['property_id'] ) )		$this->setPropertyId( $arrmixValues['property_id'] );
	}

	/**
	 * Add Functions
	 *
	 */

	public function addCid( $intCid ) {
		$this->m_arrintCids[$intCid] = $intCid;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valDatabaseServerTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseServerTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_server_type_id', 'Database Server type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHardwareId() {
		$boolIsValid = true;

		if( true == is_null( $this->getHardwareId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'Database server is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMasterDatabase() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_id', 'Master database is required.' ) );
		}
		return $boolIsValid;
	}

	public function valReplicationRoleId() {
		$boolIsValid = true;

		if( true == is_null( $this->getReplicationRoleId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'replication_role_id', 'Replication role is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMaintenanceMode() {

		$boolIsDbOnMaintenance       = false;
		$intPrivateShowMaintenance	= ( int ) trim( file_get_contents( PATH_CONFIG . 'server_maintenance_private.txt' ) );

		if( $this->getDatabaseTypeId() == CDatabaseType::CLIENT ) {
			if( true == getIsDbOnMaintenance( $this->getId() ) || 1 == $intPrivateShowMaintenance ) {
				$boolIsDbOnMaintenance = true;
			}
		}

		// show maintenance page if CA is up, maintenance is going on and used different database rather that connect and admin
		if( false === defined( 'CONFIG_PS_PRODUCT_ID' ) || CPsProduct::CLIENT_ADMIN != CONFIG_PS_PRODUCT_ID ) {
			return true;
		}

		if( false == $boolIsDbOnMaintenance ) {
			return true;
		}

		$strRedirectUrl = 'https://' . basename( $_SERVER['HTTP_HOST'] );

		if( 'stage' == CConfig::get( 'environment' ) || 'production' == CConfig::get( 'environment' ) ) {
			$strRedirectUrl 	= 'https://' . basename( $_SERVER['HTTP_HOST'] );

		} elseif( 'development' == CConfig::get( 'environment' ) ) {
			$strRedirectUrl 	= 'http://' . basename( $_SERVER['HTTP_HOST'] );
		}

		$arrstrErrorMsgs = array();

		if( true == valArr( $_COOKIE['error_msgs'] ) ) {
			$arrstrErrorMsgs = $_COOKIE['error_msgs'];
		}

		array_push( $arrstrErrorMsgs, 'Due to ongoing system maintenance, only limited modules are accessible.' );

		setcookie( 'error_msgs', json_encode( $arrstrErrorMsgs ), 0, $strPath = '/' );

		echo '<script type="text/javascript">' . "\n";
		echo 'window.location="' . $strRedirectUrl . '";';
		echo '</script>';
		exit;

		return true;
	}

	public function valDatabaseTypeId( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_type_id', 'Database type is required.' ) );
		} elseif( true == isset( $objDatabase ) ) {

			if( $this->m_intDatabaseTypeId == CDatabaseType::CLIENT && false == is_null( $this->m_intCid ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_type_id', 'You do not have the permission to delete client database.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valDatabaseClusterId() {
		$boolIsValid = true;

		if( CDatabaseType::CLIENT == $this->getDatabaseTypeId() ) {
			if( true == is_null( $this->getClusterId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Cluster is required for this Database Type' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == is_null( $this->getCid() ) ) {
			if( false == preg_match( '/^[1-9][0-9]*$/', $this->getCid() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Cid field should be numeric.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valMachineName() {
		$boolIsValid = true;

		if( true == is_null( $this->getMachineName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'machine_name', 'Machine name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;

		// Temporary skipping IP & DNS validation for RDS databases
		if( 'production' == CONFIG_ENVIRONMENT && '.com' != CONFIG_TLD ) {
			return $boolIsValid;
		}

        // Temporary skipping IP & DNS validation for training databases
		if( true == defined( 'CONFIG_TRAINING_MODE' ) && true == CONFIG_TRAINING_MODE ) {
            return $boolIsValid;
        }

		if( true == is_null( $this->getIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP address is required.' ) );
		} else {

			if( false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP address entered.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valDnsHandle() {
		$boolIsValid = true;
		// Temporary skipping IP & DNS validation for RDS databases
		return true;

		// Temporary skipping IP & DNS validation for RDS databases
		if( 'production' == CONFIG_ENVIRONMENT && '.com' != CONFIG_TLD ) {
			return $boolIsValid;
		}

		if( false == is_null( $this->getDnsHandle() ) ) {

			$strDnsCheck = '/^([a-zA-Z0-9]+([\_\-]{1})?)*([a-zA-Z0-9]+)\.([a-zA-Z0-9]+([\_\-]{1})?)*([a-zA-Z0-9]+)\.([\w]+\.)?[a-zA-Z0-9]+([a-zA-Z0-9]+)*\.[a-zA-Z]{2,4}+$/';

			if( 1 !== preg_match( $strDnsCheck, $this->getDnsHandle() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dns_handle', 'DNS Handle is invalid.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valPort() {
		$boolIsValid = true;

			if( true == is_null( $this->getPort() ) || 0 == $this->getPort() || true == $this->validatePort( $this->getPort() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'port', 'Port is required.' ) );
			}

		return $boolIsValid;
	}

	public function valDatabaseName( $objDatabase=NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_name', 'Database name is required.' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) && false == is_null( $this->getHardwareId() ) && false == is_null( $this->getDatabaseTypeId() ) && false == is_null( $this->getPort() ) ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$strWhere = 'WHERE hardware_id = ' . trim( $this->getHardwareId() ) . ' AND port = ' . trim( $this->getPort() ) . ' AND database_type_id =' . trim( $this->getDatabaseTypeId() ) . ' AND database_name = \'' . trim( $this->getDatabaseName() ) . '\'' . $strSqlCondition;
			$intCount = CDatabases::fetchDatabaseCount( $strWhere, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_name', 'Database name already in use by this database type for this database server .' ) );

			}
		}
		return $boolIsValid;
	}

	public function valUsernameEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getUsernameEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username_encrypted', 'User name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getPasswordEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password_encrypted', 'Password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsSlave( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getDatabaseId() ) ) {
			$arrobjDatabases     = CDatabases::fetchDatabasesByDatabaseId( $this->getId(), $objDatabase );
			$objDatabase 	 	 = CDatabases::fetchDatabaseById( $this->getDatabaseId(), $objDatabase );

			if( true == valArr( $arrobjDatabases ) && false == is_null( $this->getDatabaseId() ) ) {

				$boolIsValid = false;
    			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Database', 'Unable to update database, database is associated with slave database(s).' ) );
    		} else {
    				if( false == is_null( $objDatabase ) ) {
    					if( false == is_null( $this->getDatabaseId() ) ) {
						    if( CReplicationRole::RO != $this->getReplicationRoleId() && $objDatabase->getDatabaseName() != $this->getDatabaseName() ) {
							    $boolIsValid = false;
							    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'database_name', '<br/>Slave database name should be same as its master database.' ) );
						    }
					    }

    					if( $objDatabase->getDatabaseServerTypeId() != $this->getDatabaseServerTypeId() ) {
    						$boolIsValid = false;
    						$strMessage .= '<br/>Database Server Type should be same as associated master database.';
    					}

    					if( $objDatabase->getDatabaseTypeId() != $this->getDatabaseTypeId() ) {
    						$boolIsValid = false;
    						$strMessage .= '<br/> Database Type should be same as associated master database.';
    					}

    					if( $objDatabase->getClusterId() != $this->getClusterId() ) {
    						$boolIsValid = false;
    						$strMessage .= ' <br/>Cluster should be same as associated master database.';
    					}

    					if( false == $boolIsValid ) {
    						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Database', $strMessage ) );
    					}
    				}
			}
		}

    	return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsSlave = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				// SRS $boolIsValid &= $this->valDatabaseClusterId();

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDatabaseServerTypeId();
				$boolIsValid &= $this->valDatabaseTypeId();
				// SRS $boolIsValid &= $this->valDatabaseClusterId();
				$boolIsValid &= $this->valHardwareId();
				$boolIsValid &= $this->valIsSlave( $objDatabase );
				if( true == $boolIsSlave ) {
					$boolIsValid &= $this->valMasterDatabase();
					$boolIsValid &= $this->valReplicationRoleId();
				}

				$boolIsValid &= $this->valDatabaseName( $objDatabase );
				$boolIsValid &= $this->valIpAddress();
				if( false == is_null( $this->getDnsHandle() ) && CDatabaseServerType::POSTGRES == $this->getDatabaseServerTypeId() ) $boolIsValid &= $this->valDnsHandle();
				$boolIsValid &= $this->valPort();
				$boolIsValid &= $this->valCid();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDatabaseTypeId( $objDatabase );
				break;

			case 'validate_open':
				$boolIsValid &= $this->valMaintenanceMode();
				$boolIsValid &= $this->valDatabaseTypeId();
				$boolIsValid &= $this->valMachineName();
				$boolIsValid &= $this->valDatabaseName();

				if( true == defined( 'CONFIG_DB_DNS_FAILOVER' ) && 0 == CONFIG_DB_DNS_FAILOVER && false == is_null( $this->getDnsHandle() ) && CDatabaseServerType::POSTGRES == $this->getDatabaseServerTypeId() ) {
					$boolIsValid &= $this->valDnsHandle();
				} else {
					$boolIsValid &= $this->valIpAddress();
				}
				$boolIsValid &= $this->valPort();
				$boolIsValid &= $this->valUsernameEncrypted();
				$boolIsValid &= $this->valPasswordEncrypted();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
	  	}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function checkHandle() {
		if( false == isset( $this->m_resHandle ) || !$this->getIsOpen() ) {

			if( false == is_null( $this->validateAndOpen() ) ) {
				return true;
			}

			if( true == $this->getIsDieOnConnectionError() ) {
				trigger_error( 'Database Error: Invalid database handle - CDatabase::checkHandle()', E_USER_ERROR );
			}
			return false;
		}

		return true;
	}

	public function getPdoDsn() {
		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$this->m_strPdoDsn = 'pgsql:host=' . $this->getIpAddress() . ';port=' . $this->getPort() . ';dbname=' . $this->getDatabaseName() . ';user=' . $this->getUsername() . ';password=' . $this->getPassword();
				break;

			case CDatabaseServerType::MYSQL:
				$this->m_strPdoDsn = 'mysql:host=' . $this->getIpAddress() . ';port=' . $this->getPort() . ';dbname=' . $this->getDatabaseName();
				break;

			case CDatabaseServerType::MSSQL:
				if( true == extension_loaded( 'MSSQL' ) ) {
					$this->m_strPdoDsn = 'sqlsrv:Server=' . $this->getIpAddress() . ',' . $this->getPort() . ';Database=' . $this->getDatabaseName();
				} elseif( \Psi\CStringService::singleton()->strtoupper( \Psi\CStringService::singleton()->substr( PHP_OS, 0, 3 ) ) !== 'WIN' && true == \Psi\CStringService::singleton()->strstr( phpversion(), '7.0' ) ) {
					// $this->m_strPdoDsn = 'dblib:host=' . $this->getIpAddress() . ':' . $this->getPort() . ';dbname=' . $this->getDatabaseName();
					$this->m_strPdoDsn = 'odbc:DRIVER=FreeTDS;SERVERNAME=MSSQL';
				} elseif( true == extension_loaded( 'PDO_ODBC' ) ) {
					$this->m_strPdoDsn = 'odbc:Driver={SQL Server};Server=' . $this->getIpAddress() . ';Database=' . $this->getDatabaseName() . ';Uid=' . $this->getUsername() . ';Pwd=' . $this->getPassword();
				} else {
					trigger_error( 'PHP Extension Error: ODBC / DBLIB not loaded - CDatabase::getPdoDsn()', E_USER_ERROR );
				}
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::getPdoDsn()', E_USER_ERROR );
				return false;
		}

		return $this->m_strPdoDsn;
	}

	public static function generateApplicationName() {

		$strModuleName = substr( preg_replace( '/[^A-Za-z0-9_\-]/', '', $_REQUEST['module'] ?? $_REQUEST['group_type'] ?? '' ), 0, 20 );
		$strActionName = substr( preg_replace( '/[^A-Za-z0-9_\-]/', '', $_REQUEST['action'] ?? $_REQUEST['group'] ?? '' ), 0, 20 );

		if( empty( $strModuleName ) ) {
			$strModuleName = substr( $_SERVER['REQUEST_URI'] ?? 'no-module', 0, 10 );
			$strModuleName .= '|' . getRemoteIpAddress();
		}
		$strReportNameAndVersion = $GLOBALS['debug_info']['data']['reportName'] ?? '';
		if( isset( $GLOBALS['debug_info']['data']['reportVersion'] ) && valStr( $GLOBALS['debug_info']['data']['reportVersion'] ) ) {
			$strReportNameAndVersion .= '(' . $GLOBALS['debug_info']['data']['reportVersion'] . ')';
		}

		// keys sorted in order of their importance while debugging from a log line
		$arrstrQueryData = [
			'product_id'		=> ( true == defined( 'CONFIG_PS_PRODUCT_ID' ) ) ? CONFIG_PS_PRODUCT_ID : '',
			'module'			=> $strModuleName,
			'action'			=> $strActionName,
			'report_name'		=> $strReportNameAndVersion
		];

		$strApplicationName = '';
		array_walk( $arrstrQueryData, function ( $strValue, $strKey ) use ( &$strApplicationName ) {
			if( !empty( $strValue ) ) {
				$strApplicationName .= ( empty( $strApplicationName ) ? '' : '|' ) . $strKey . ':' . $strValue . '';
			}
		} );

		return $strApplicationName;
	}

	/**
	 * @return string
	 */
	private function generateDebugInfoString() {

		// keys sorted in order of their importance while debugging from a log line
		$arrstrQueryData = [
			'cid'		=> $GLOBALS['debug_info']['cid'] ?? '',
			'user'		=> $GLOBALS['debug_info']['user_id'] ?? '',
			'req_id'	=> $GLOBALS['debug_info']['unique_id'] ?? ''
		];

		$strDebugInfo = '';
		array_walk( $arrstrQueryData, function ( $strValue, $strKey ) use ( &$strDebugInfo ) {
			if( !empty( $strValue ) ) {
				$strDebugInfo .= ( empty( $strDebugInfo ) ? '' : '|' ) . $strKey . ':' . $strValue;
			}
		} );

		$strDebugInfo = '/*' . $strDebugInfo . '*/';
		return $strDebugInfo;
	}

	public function open( $boolForceOpen = true ) {
		if( false == $boolForceOpen && true == is_resource( $this->getHandle() ) && PGSQL_CONNECTION_OK === pg_connection_status( $this->getHandle() ) ) {
			return $this->m_resHandle;
		}

		$this->validateMultipleSimultaneousConnections();

		if( true == $boolForceOpen && true == $this->m_boolIsApplicationNameAdded ) {
			$this->m_boolIsApplicationNameAdded = false;
		}

		// TODO This is added for the keepmyvoice database connection as right now we are not able to connect using IP address - [mnarole].
		$strHost = $this->getIpAddress();

		if( ( false == defined( 'CONFIG_DB_DNS_FAILOVER' ) || 0 == CONFIG_DB_DNS_FAILOVER ) && false == is_null( $this->getDnsHandle() ) && CDatabaseServerType::POSTGRES == $this->getDatabaseServerTypeId() ) {
			$strHost = $this->getDnsHandle();
		}

		if( 'ebdb' == $this->getDatabaseName() ) {
			$strHost = $this->getMachineName();
		}

		$this->m_boolIsConnectionInitialized = true;

		$strLogString = 'host=' . $strHost . ' port=' . $this->getPort() . ' dbname=' . $this->getDatabaseName() . ' user=' . $this->getUsername();
		if( 0 < $this->m_intConnectionTimeout ) {
			$strLogString .= ' connect_timeout=' . $this->m_intConnectionTimeout;
		}

		self::$c_arrintDebugDatabaseIds[$this->getId()] = $this->getId();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'query' => $strLogString, 'database_id' => $this->getId(), 'database_name' => $this->getDatabaseName(), 'database_type_name' => $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() ), 'backtrace' => '-' ) );
					$fltStartTime = microtime( true );
				}

				$strConnectionString = 'host=' . $strHost . ' port=' . $this->getPort() . ' dbname=' . $this->getDatabaseName() . ' user=' . $this->getUsername() . ' password=' . $this->getPassword();
				if( 0 < $this->m_intConnectionTimeout ) {
					$strConnectionString .= ' connect_timeout=' . $this->m_intConnectionTimeout;
				}

				if( true == $boolForceOpen ) {
					$this->m_resHandle = pg_connect( $strConnectionString, PGSQL_CONNECT_FORCE_NEW );
				} else {
					$this->m_resHandle = pg_connect( $strConnectionString );
				}

				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'start_time' => $fltStartTime ) );
				}

				if( false !== $this->m_resHandle ) {
					$this->m_boolIsOpen = true;
					if( CCluster::STANDARD == $this->getClusterId() ) {
						$this->execute( 'SET DATESTYLE TO \'SQL\';' );
					}
				} else {
					$this->m_resHandle = NULL;
				}
				break;

			case CDatabaseServerType::MYSQL:
				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'query' => $strLogString, 'database_id' => $this->getId(), 'database_name' => $this->getDatabaseName(), 'database_type_name' => $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() ), 'backtrace' => '-' ) );
					$fltStartTime = microtime( true );
				}

				$this->m_resHandle = mysqli_connect( $this->getIpAddress(), $this->getUsername(), $this->getPassword(), $this->getDatabaseName(), $this->getPort() );

				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'start_time' => $fltStartTime ) );
				}

				if( false === $this->m_resHandle ) {
					$this->m_resHandle = NULL;
				}
				break;

			case CDatabaseServerType::MSSQL:
				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'query' => $strLogString, 'database_id' => $this->getId(), 'database_name' => $this->getDatabaseName(), 'database_type_name' => $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() ), 'backtrace' => '-' ) );
					$fltStartTime = microtime( true );
				}

				if( true == checkDisplayDbInfoProcess() ) {
					storeDbInfo( array( 'start_time' => $fltStartTime ) );
				}

				if( true == extension_loaded( 'MSSQL' ) ) {
					$this->m_resHandle = mssql_connect( $this->getIpAddress(), $this->getUsername(), $this->getPassword() );
					if( false !== $this->m_resHandle ) {
						mssql_select_db( $this->getDatabaseName(), $this->m_resHandle );
					}

				} elseif( true == \Psi\CStringService::singleton()->strstr( phpversion(), '7.0' ) ) {
					try {
						$this->m_resHandle = new PDO( $this->getPdoDsn( CDatabaseServerType::MSSQL ), $this->getUsername(), $this->getPassword() );

					} catch( PDOException $objException ) {
						$this->m_resHandle = NULL;
					}
				} else {
					$this->m_resHandle = NULL;
				}
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}

		if( false == is_null( $this->m_resHandle ) ) {
		    $this->m_boolIsOpen = true;
		}

		$this->logConnectionCount();

		return $this->m_resHandle;
	}

	public function close() {

		$this->m_boolIsOpen = false;

		if( true == $this->m_boolIsApplicationNameAdded ) {
			$this->m_boolIsApplicationNameAdded = false;
		}

		$strKey = $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() );
		if( true == isset( self::$c_arrintConnectionCountByTypes[$strKey] ) ) {
			self::$c_arrintConnectionCountByTypes[$strKey]--;
		}

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				if( true == is_resource( $this->getHandle() ) && PGSQL_CONNECTION_OK === pg_connection_status( $this->getHandle() ) ) {
					self::$c_intOpenConnectionCount--;
					return pg_close( $this->getHandle() );
				} else {
					return true;
				}
				break;

			case CDatabaseServerType::MYSQL:
				self::$c_intOpenConnectionCount--;
				return mysqli_close( $this->getHandle() );
				break;

			case CDatabaseServerType::MSSQL:
				if( true == extension_loaded( 'MSSQL' ) ) {
					self::$c_intOpenConnectionCount--;
					return mssql_close( $this->getHandle() );
				} else {
					$this->m_resHandle = NULL;
					return true;
				}
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function execute( $strQuery, $boolIsLogging = false ) {

		$this->checkHandle();
		global $_objException;

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				// Composite key SQL check
                if( NULL == CConfig::get( 'ignore_composite_key_validation' ) && ( ( 'production' != CConfig::get( 'environment' ) && $this->getDatabaseTypeId() == CDatabaseType::CLIENT ) ) ) {
					$boolIsCompositeValidationOn = true;
					$objCCompositeKeyValidator = new CCompositeKeyValidator();
					$objCCompositeKeyValidator->validateRealTimeSql( $this->getHandle(), $strQuery );
				} else {
					$boolIsCompositeValidationOn = false;
				}

				if( false == $this->m_boolIsApplicationNameAdded ) {
					$strQuery = 'SET application_name = \'' . pg_escape_string( \CDatabase::generateApplicationName() ) . '\';' . $strQuery;
					$this->m_boolIsApplicationNameAdded = true;
					if( CDatabaseType::CLIENT == $this->getDatabaseTypeId() ) {
						CLocaleContainer::createService()->initializeDatabase( $this );
					}
                }

				$strQuery = $this->generateDebugInfoString() . $strQuery;

				$strRealStartTime = microtime( true );
				$strQuery = $this->setQueryTimeOut( $strQuery );
				if( true == valObj( $_objException, 'CException' ) && true == $_objException->getStoreData() ) {
					$_objException->setStoreData( false );
					$objResHandle = @pg_query( $this->getHandle(), $strQuery );
					$_objException->setStoreData( true );
				} else {
					$objResHandle = @pg_query( $this->getHandle(), $strQuery );
				}
				$this->updateQueryTimeOut();
				$this->logQueriesCount();

				if( false !== $objResHandle ) {

					// Composite key data check
					if( true == isset( $boolIsCompositeValidationOn ) && true == $boolIsCompositeValidationOn && true == in_array( CConfig::get( 'ps_product_id' ), array( CPsProduct::ENTRATA, CPsProduct::PROSPECT_PORTAL, CPsProduct::RESIDENT_PORTAL, CPsProduct::OWNER_PORTAL ) ) ) {
						$objCCompositeKeyValidator->validateRealTimeData( $objResHandle );
						unset( $objCCompositeKeyValidator );
					}
					if( true == $boolIsLogging && true == defined( 'CONFIG_PS_PRODUCT_ID' ) && CONFIG_PS_PRODUCT_ID != CPsProduct::JOBS ) {
						// database logging in logstash for web ONLY
						CPsLogger::logToLogstash( CPsLogger::LOG_TYPE_DATABASE, array( 'database' => $this, 'query' => $strQuery, 'exec_time' => microtime( true ) - $strRealStartTime, 'status' => 'OK' ), true );
					}
					return $objResHandle;
				} else {
					if( true == $boolIsLogging && true == defined( 'CONFIG_PS_PRODUCT_ID' ) && CONFIG_PS_PRODUCT_ID != CPsProduct::JOBS ) {
						// database logging in logstash for web ONLY
						CPsLogger::logToLogstash( CPsLogger::LOG_TYPE_DATABASE, array( 'database' => $this, 'query' => $strQuery, 'exec_time' => microtime( true ) - $strRealStartTime, 'status' => 'FAIL' ), true );
					}

					trigger_error( "execute_db\n" . pg_last_error( $this->getHandle() ) . "\n\n" . $strQuery, E_USER_WARNING );
					return NULL;
				}
				break;

			case CDatabaseServerType::MYSQL:
				$objResult = @mysqli_query( $this->m_resHandle, $strQuery );

				if( false !== $objResult ) {
					$this->m_objResult = $objResult;
					return $this->m_objResult;
				} else {
					 trigger_error( "execute_db\n" . mysqli_error( $this->getHandle() ) . "\n\n" . $strQuery, E_USER_WARNING );
					 return false;
				}
				break;

			case CDatabaseServerType::MSSQL:
				if( true == extension_loaded( 'MSSQL' ) ) {
					$resResult = @mssql_query( $strQuery, $this->m_resHandle );
					if( false !== $resResult ) {
						return $resResult;
					} else {
						trigger_error( "execute_db\n" . mssql_get_last_message() . "\n\n" . $strQuery, E_USER_WARNING );
						return false;
					}
				} elseif( true == \Psi\CStringService::singleton()->strstr( phpversion(), '7.0' ) ) {
					$resResult = $this->m_resHandle->prepare( $strQuery, array( PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ) );
					if( false !== $resResult ) {
						return $resResult;
					} else {
						trigger_error( "execute_db\n" . $this->m_resHandle() . "\n\n" . $strQuery, E_USER_WARNING );
						return false;
					}
				}
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function begin() {

		if( !$this->getUseMasterDatabaseForWrite() && ( $this->getUseReplica() || CReplicationRole::RO == $this->getReplicationRoleId() ) ) {
			throw new \Exception( 'Write operation not allowed on replica database.' );
		}

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$this->m_boolIsTransaction = true;
				return $this->execute( 'BEGIN' );
				break;

			case CDatabaseServerType::MYSQL:
				return true;
				break;

			case CDatabaseServerType::MSSQL:
				$this->m_boolIsTransaction = true;
				return $this->execute( 'BEGIN TRAN' );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function commit() {

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$this->m_boolIsTransaction = false;
				return $this->execute( 'COMMIT' );
				break;

			case CDatabaseServerType::MYSQL:
				return true;
				break;

			case CDatabaseServerType::MSSQL:
				$this->m_boolIsTransaction = false;
				return $this->execute( 'COMMIT' );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function createSavePoint( $strSavePointName ) {

		if( false == $this->getIsTransaction() ) {
			trigger_error( 'Database Error: SAVEPOINT can only be used in transaction blocks - CDatabase::createSavePoint()', E_USER_ERROR );
			return false;
		}

		if( false == valStr( $strSavePointName ) ) return false;

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return $this->execute( 'SAVEPOINT ' . $strSavePointName );
				break;

			case CDatabaseServerType::MYSQL:
				return $this->execute( 'SAVEPOINT ' . $strSavePointName );
				break;

			case CDatabaseServerType::MSSQL:
				return $this->execute( 'SAVE TRANSACTION ' . $strSavePointName );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function rollbackToSavePoint( $strSavePointName ) {

		if( false == $this->getIsTransaction() ) {
			trigger_error( 'Database Error: SAVEPOINT can only be used in transaction blocks - CDatabase::rollbackToSavePoint()', E_USER_ERROR );
			return false;
		}

		if( false == valStr( $strSavePointName ) ) return false;

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return $this->execute( 'ROLLBACK TO SAVEPOINT ' . $strSavePointName );
				break;

			case CDatabaseServerType::MYSQL:
				return $this->execute( 'ROLLBACK TO SAVEPOINT ' . $strSavePointName );
				break;

			case CDatabaseServerType::MSSQL:
				return $this->execute( 'ROLLBACK TRANSACTION ' . $strSavePointName );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function releaseSavePoint( $strSavePointName ) {

		if( false == $this->getIsTransaction() ) {
			trigger_error( 'Database Error: SAVEPOINT can only be used in transaction blocks - CDatabase::releaseSavePoint()', E_USER_ERROR );
			return false;
		}

		if( false == valStr( $strSavePointName ) ) return false;

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return $this->execute( 'RELEASE SAVEPOINT ' . $strSavePointName );
				break;

			case CDatabaseServerType::MYSQL:
				return $this->execute( 'RELEASE SAVEPOINT ' . $strSavePointName );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function rollback() {

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$this->m_boolIsTransaction = false;
				self::$c_intRollbackCount++;
				return $this->execute( 'ROLLBACK' );
				break;

			case CDatabaseServerType::MYSQL:
				self::$c_intRollbackCount++;
				return true;
				break;

			case CDatabaseServerType::MSSQL:
				$this->m_boolIsTransaction = false;
				self::$c_intRollbackCount++;
				return $this->execute( 'ROLLBACK TRAN' );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	public function checkTransactionStatus() {

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return pg_transaction_status( $this->m_resHandle );

			case CDatabaseServerType::MYSQL:
				return false;

			case CDatabaseServerType::MSSQL:
				return false;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::checkTransactionStatus()', E_USER_ERROR );
				return false;
		}
	}

	public function checkAndReOpen() {

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$intStatus = pg_connection_status( $this->m_resHandle );
				if( $intStatus === PGSQL_CONNECTION_OK ) {
					return true;
				} else {
					$this->open();
					return true;
				}
				break;

			case CDatabaseServerType::MYSQL:
				return true;
				break;

			case CDatabaseServerType::MSSQL:
				return true;
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::checkAndReOpen()', E_USER_ERROR );
				return false;
		}
	}

	public function checkAndReOpenBrokenConnection() {

        if( false == $this->m_boolIsOpen ) {
            if( false == is_null( $this->open() ) ) {
				return true;
            } else {
				return false;
            }
        }

	    $this->checkHandle();

	    switch( $this->getDatabaseServerTypeId() ) {
	        case CDatabaseServerType::POSTGRES:
	            $boolStatus = pg_ping( $this->m_resHandle );
	            if( true === $boolStatus ) {
                    $this->m_boolIsOpen = true;
	                return true;
	            } else {
	                return false;
	            }
	            break;

	        case CDatabaseServerType::MYSQL:
	            return true;
	            break;

	        case CDatabaseServerType::MSSQL:
	            return true;
	            break;

	        default:
	            trigger_error( 'Database Error: Invalid database server type - CDatabase::checkAndReOpenBrokenConnection()', E_USER_ERROR );
	            return false;
	    }
	}

	public function errorMsg() {
		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				return @pg_last_error( $this->getHandle() );
				break;

			case CDatabaseServerType::MYSQL:
				return @mysqli_error( $this->getHandle() );
				break;

			case CDatabaseServerType::MSSQL:
				return @mssql_get_last_message();
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}
	}

	/**
	 * Create Functions
	 *
	 */

	public function createDataset() {

		$this->checkHandle();

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$objDataset = new CPgSqlDs( $this );
				break;

			case CDatabaseServerType::MYSQL:
				 $objDataset = new CMySqlDs( $this );
				break;

			case CDatabaseServerType::MSSQL:
				$objDataset = new CMsSqlDs( $this );
				break;

			default:
				trigger_error( 'Database Error: Invalid database server type - CDatabase::open()', E_USER_ERROR );
				return false;
		}

		return $objDataset;
	}

	public function createDatabaseTransaction( $intScriptId ) {

		$objDatabaseTransaction = new CDatabaseTransaction();
		$objDatabaseTransaction->setDatabaseId( $this->getId() );
		$objDatabaseTransaction->setScriptId( $intScriptId );
		$objDatabaseTransaction->setBeginDatetime( date( 'm/d/Y H:i:s' ) );

		return $objDatabaseTransaction;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function validateAndOpen( $boolForceOpen = true ) {

		if( true == $this->validate( 'validate_open' ) ) {
			if( false == is_null( $this->open( $boolForceOpen ) ) ) {
				return true;
			} else {
				return false;
			}
		} else {
			trigger_error( 'Database Error: Invalid database credentials.', E_USER_ERROR );
			return false;
		}
	}

	public function validatePort( $strPort ) {
		if( true == preg_match( '(([0-9]{5}))', $strPort ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Used to set query time out value on base of php execution time. For now we have set sql timeour for only CA.
	 * @param $strQuery
	 * @return string
	 */
	private function setQueryTimeOut( $strQuery ) {

		if( false == preg_match( '/STATEMENT_TIMEOUT|DATESTYLE|BEGIN|COMMIT|ROLLBACK/', $strQuery ) && false !== $intTimeout = ( CGlobalTimeout::createService()->getRemainingTimeout() ) ) {
			$strQuery = 'SET STATEMENT_TIMEOUT = \'' . $intTimeout . 's\'; ' . $strQuery;
		}
		return $strQuery;
	}

	private function updateQueryTimeOut() {
		CGlobalTimeout::createService()->updateRemainingTimeout();
	}

	private function logConnectionCount() {

		$strKey = $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() );

		if( false == isset( self::$c_arrintConnectionCountByTypes[$strKey] ) ) {
			self::$c_arrintConnectionCountByTypes[$strKey] = 0;
		}

		self::$c_arrintConnectionCountByTypes[$strKey]++;

		if( true == $this->m_boolIsOpen ) {
			self::$c_intOpenConnectionCount++;
		}
	}

	private function logQueriesCount() {

		$strKey = $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() );

		if( false == isset( self::$c_arrintQueryCountByTypes[$strKey] ) ) {
			self::$c_arrintQueryCountByTypes[$strKey] = 0;
		}

		self::$c_arrintQueryCountByTypes[$strKey]++;
	}

	public function logWriteQueriesCount() {

		$strKey = $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() );

		if( false == isset( self::$c_arrintWriteQueryCountByTypes[$strKey] ) ) {
			self::$c_arrintWriteQueryCountByTypes[$strKey] = 0;
		}

		self::$c_arrintWriteQueryCountByTypes[$strKey]++;
	}

	public function validateMultipleSimultaneousConnections() {

		if( false == in_array( $this->getDatabaseTypeId(), CDatabaseType::$c_arrintCheckSimultaneousConnectionDatabaseTypes ) ) {
			return;
		}

		$strKey = $this->getDatabaseTypeNameById( $this->getDatabaseTypeId() );

		if( ( true == defined( 'CONFIG_ENVIRONMENT' ) && 'development' == CONFIG_ENVIRONMENT ) && ( true == isset( self::$c_arrintConnectionCountByTypes[$strKey] ) && self::$c_arrintConnectionCountByTypes[$strKey] > 0 ) ) {
			throw new \Exception( sprintf( 'Simultaneous connections are restricted to the same database. [%s]', $this->getDatabaseName() ) );
		}

	}

	public static function getOpenConnectionCount() {
		return self::$c_intOpenConnectionCount;
	}

	public static function getConnectionCountByTypes() {
		return self::$c_arrintConnectionCountByTypes;
	}

	public static function getQueriesCountByTypes() {
		return self::$c_arrintQueryCountByTypes;
	}

	public static function getWriteQueriesCount() {
		return self::$c_arrintWriteQueryCountByTypes;
	}

	public static function getRollbackCount() {
		return self::$c_intRollbackCount;
	}

	public static function resetConnectionCountAndCountByTypes() {
		self::$c_intOpenConnectionCount = 0;
		self::$c_arrintConnectionCountByTypes = [];
		self::$c_arrintQueryCountByTypes = [];
		self::$c_arrintWriteQueryCountByTypes = [];
		self::$c_intRollbackCount = 0;
	}

	private function getDatabaseTypeNameById( $intDatabaseTypeId ) {
		switch( $intDatabaseTypeId ) {
			case CDatabaseType::ADMIN:
				$strKey = 'admin';
				break;

			case CDatabaseType::EMAIL:
				$strKey = 'email';
				break;

			case CDatabaseType::CLIENT:
				$strKey = 'entrata';
				break;

			case CDatabaseType::LOGS:
				$strKey = 'logs';
				break;

			case CDatabaseType::PAYMENT:
				$strKey = 'payment';
				break;

			case CDatabaseType::CONNECT:
				$strKey = 'connect';
				break;

			case CDatabaseType::VOIP:
				$strKey = 'voip';
				break;

			case CDatabaseType::SMS:
				$strKey = 'sms';
				break;

			case CDatabaseType::SCREENING:
				$strKey = 'screening';
				break;

			default:
				$strKey = 'others';

		}
		return $strKey;
	}

	public function getReplicaDatabase() {
		if( true == $this->getUseReplica() && false == $this->getIsTransaction() ) {
			if( false == is_null( $this->m_objReplicaDatabase ) ) return $this->m_objReplicaDatabase;

			if( CReplicationRole::RO == $this->getReplicationRoleId() ) return $this;

			$objConnectDatabase = $this->getConnectDatabase();

			$objConnectDatabase->open();
			$objDatabase = \Psi\Eos\Connect\CDatabases::createService()->fetchReplicaDatabaseByDatabaseIdByDatabaseUserTypeId( $this->getId(), $this->getDatabaseUserTypeId(), $objConnectDatabase, $this->getReplicaDatabaseIds() );
			$objConnectDatabase->close();

			if( true == valObj( $objDatabase, 'CDatabase' ) ) {
				$this->setReplicaDatabase( $objDatabase );
				$this->m_objReplicaDatabase->setMasterDatabase( $this );
				$this->m_objReplicaDatabase->setUseReplica( true );
				$this->m_objReplicaDatabase->setUseMasterDatabaseForWrite( $this->getUseMasterDatabaseForWrite() );
				return $this->m_objReplicaDatabase;
			} else {
				$this->setUseReplica( false );
			}
		}
		return $this;
	}

	public function getMasterDatabase() {
		if( CReplicationRole::RO != $this->getReplicationRoleId() ) return $this;

		if( !$this->getUseMasterDatabaseForWrite() && ( $this->getUseReplica() || CReplicationRole::RO == $this->getReplicationRoleId() ) ) {
			throw new \Exception( 'Write operation not allowed on replica database.' );
		}

		if( false == is_null( $this->m_objMasterDatabase ) ) return $this->m_objMasterDatabase;

		$objConnectDatabase = $this->getConnectDatabase();

		$objConnectDatabase->open();
		$objDatabase = CDatabases::fetchDatabaseByDatabaseIdByDatabaseUserTypeId( $this->getDatabaseId(), $this->getDatabaseUserTypeId(), $objConnectDatabase );
		$objConnectDatabase->close();

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$this->setMasterDatabase( $objDatabase );
			return $this->m_objMasterDatabase;
		} else {
			return $this;
		}
	}

	public function setMasterDatabase( $objDatabase ) {
		$this->m_objMasterDatabase = $objDatabase;
	}

	public function setReplicaDatabase( $objDatabase ) {
		$this->m_objReplicaDatabase = $objDatabase;
	}

	private function getConnectDatabase() {
		$objConnectDatabase = $this->getDatabase();
		if( false == valObj( $objConnectDatabase, 'CDatabase' ) || CDatabaseType::CONNECT != $objConnectDatabase->getDatabaseTypeId() ) {
			$objConnectDatabase = CDatabases::createConnectDatabase();
		}
		return $objConnectDatabase;
	}

	public function getCollateSort( $strColumnName, $boolIsTranslatedColumn = false, $boolIsSystemTable = false ) {

		switch( $this->getDatabaseServerTypeId() ) {
			case CDatabaseServerType::POSTGRES:
				$strLocaleCode = CLocaleContainer::createService()->getLocaleCode();
				if( CLocale::DEFAULT_LOCALE != $strLocaleCode ) {
					if( true == $boolIsTranslatedColumn ) {
						if( false !== \Psi\CStringService::singleton()->strpos( $strColumnName, '.' ) ) {
							list( $strAlias, $strColumnNameWithoutAlias ) = ( false !== \Psi\CStringService::singleton()->strpos( $strColumnName, '.' ) ) ? explode( '.', $strColumnName ) : [];
							$strAlias = $strAlias . '.';
						} else {
							$strAlias = '';
							$strColumnNameWithoutAlias = $strColumnName;
						}
						if( true == $boolIsSystemTable ) {
							$strColumnName = 'util_get_system_translated( \'' . $strColumnNameWithoutAlias . '\', ' . $strColumnName . ', ' . $strAlias . 'details, \'' . $strLocaleCode . '\' )';
						} else {
							$strColumnName = 'util_get_translated( \'' . $strColumnNameWithoutAlias . '\', ' . $strColumnName . ', ' . $strAlias . 'details, \'' . $strLocaleCode . '\' )';
						}
					}
					return $strColumnName . ' COLLATE "' . $strLocaleCode . '"';
				} else {
					return $strColumnName;
				}
				break;

			default:
				return $strColumnName;
		}

	}

}
