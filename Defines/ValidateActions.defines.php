<?php
define( 'VALIDATE_CREATE', 'create' );
define( 'VALIDATE_INSERT', 'insert' );
define( 'VALIDATE_UPDATE', 'update' );
define( 'VALIDATE_DELETE', 'delete' );
define( 'VALIDATE_IMPORT', 'validate_import' );
define( 'VALIDATE_REFRRER_TYPE_CUSTOMER', 'validate_referrer_type_customer' );
define( 'VALIDATE_COMPANY_DOMAIN_INSERT', 'validate_company_domain_insert' );
define( 'VALIDATE_COMPANY_DOMAIN_TASK_INSERT', 'validate_company_domain_task_insert' );
define( 'VALIDATE_INSERT_HELPDESK_TASK', 'validate_insert_helpdesk_task' );
define( 'VALIDATE_UPDATE_BULK_UPDATE_SCHEDULED_CHARGES', 'validate_update_bulk_update_scheduled_charges' );
define( 'VALIDATE_SHARED_FILTER_UPDATE', 'validate_shared_filter_update' );

?>