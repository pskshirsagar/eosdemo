<?php

define( 'TRACE_CODE_CUSTOMER_PAYMENT',			'cust_payment_id' );
define( 'TRACE_CODE_CUSTOMER_PAYMENT_REVERSAL',	'cust_reverse_id' );
define( 'TRACE_CODE_COMPANY_PAYMENT',			'comp_payment_id' );
define( 'TRACE_CODE_SETTLEMENT_DISTRIBUTION',	'distribution_id' );

class CTraceCodes {
	const CUSTOMER_PAYMENT			= TRACE_CODE_CUSTOMER_PAYMENT;
	const CUSTOMER_PAYMENT_REVERSAL	= TRACE_CODE_CUSTOMER_PAYMENT_REVERSAL;
	const COMPANY_PAYMENT			= TRACE_CODE_COMPANY_PAYMENT;
	const SETTLEMENT_DISTRIBUTION	= TRACE_CODE_SETTLEMENT_DISTRIBUTION;
}

$GLOBALS['_arrstrOldTraceCodes'] = array(
	'ccustomerpayment_id'			=> TRACE_CODE_CUSTOMER_PAYMENT,
	'ccompany_payment_id'			=> TRACE_CODE_COMPANY_PAYMENT,
	'ar_payment_id'					=> TRACE_CODE_CUSTOMER_PAYMENT,
	'settlement_distribution_id'	=> TRACE_CODE_SETTLEMENT_DISTRIBUTION
);

?>