<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgBlacklistedDomains
 * Do not add any new functions to this class.
 */

class CMktgBlacklistedDomains extends CBaseMktgBlacklistedDomains {

	public static function fetchActiveMktgBlacklistedDomains( $objDatabase ) {
		$strSql = 'SELECT domain FROM mktg_blacklisted_domains WHERE is_active = 1';
		return self::fetchMktgBlacklistedDomains( $strSql, $objDatabase );
	}
}
?>