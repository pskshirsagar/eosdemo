<?php

class CMktgState extends CBaseMktgState {

	public function valCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $objDatabase ) {
		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( code, name )
					VALUES ( ' .
					$this->sqlCode() . ', ' .
					$this->sqlName() . ' ) ';

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>