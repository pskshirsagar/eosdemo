<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyInsights
 * Do not add any new functions to this class.
 */

class CMktgPropertyInsights extends CBaseMktgPropertyInsights {

	public static function fetchActiveMktgPropertyInsightsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = sprintf( 'SELECT 
						mpi.* 
			       FROM mktg_property_insights mpi
				   WHERE 
						mpi.property_id = %d
						AND mpi.cid = %d
						AND mpi.deleted_on IS NULL
						AND COALESCE(mpi.is_competitor, TRUE) = TRUE', $intPropertyId, $intCid );
		return parent::fetchMktgPropertyInsights( $strSql, $objDatabase );
	}

	public static function fetchMarketInsightsAssociationByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strOccupancyTypeStudent = '%Student%';
		$strSql = sprintf( "SELECT DISTINCT
					scp.remote_primary_key as competitor_id,
					mpc.priority as rank,
					cc.remote_primary_key as cid,
					cc.company_name as company,
					cp.property_name as name,
					concat(mpa.street_line1,', ',mpa.city,', ',mpa.state_code,' ', mpa.postal_code) as address,
					CASE 
						WHEN mpc.drive_distance IS NOT NULL THEN round( mpc.drive_distance, 1) 
					    ELSE mpc.drive_distance
					END as drive_distance,
					CASE 
						WHEN mpc.drive_time IS NOT NULL THEN
					      CASE 
					        WHEN mpc.drive_time::INTEGER > 60 THEN  concat( (mpc.drive_time::INTEGER / 60)::TEXT, ' hours ', (mpc.drive_time::INTEGER %% 60)::TEXT, ' mins' )::TEXT
					        WHEN mpc.drive_time::INTEGER > 0 AND mpc.drive_time::INTEGER < 60  THEN concat( round(mpc.drive_time)::TEXT, ' mins') 
					        ELSE round(mpc.drive_time)::TEXT
					      END
					    ELSE round(mpc.drive_time)::TEXT
					END as drive_time,
					CASE 
						WHEN scp.remote_primary_key::INTEGER > 0 THEN 'white'
					    ELSE 'grey'
					END AS display_type,
					CASE 
						WHEN mpi.id IS NOT NULL AND mpi.deleted_on IS NULL THEN 'Yes'
					    ELSE 'No'
					END AS is_checked,
					CASE 
						WHEN mpi.locked_on IS NOT NULL AND mpi.deleted_on IS NULL AND (  ( mpi.locked_on + '60 days'::INTERVAL )::DATE > CURRENT_DATE ) THEN 'Yes' 
					    ELSE 'No'
					END AS is_locked,
					CASE 
						WHEN mpi.locked_on IS NOT NULL AND mpi.deleted_on IS NULL AND (  ( mpi.locked_on + '60 days'::INTERVAL )::DATE > CURRENT_DATE ) THEN  to_char(( mpi.locked_on + '60 days'::INTERVAL )::DATE, 'mm/dd/yy')  
					    ELSE NULL
					END AS unlock_date
					FROM
					mktg_companies mc 
					   JOIN mktg_properties mp ON (mp.company_id = mc.id  AND mp.remote_primary_key::INTEGER = %d
					                                                          AND mc.remote_primary_key::INTEGER = %d
					                                                              AND mp.is_disabled = 'N')
					   JOIN mktg_property_competitors mpc ON mpc.company_id = mp.company_id AND mpc.property_id = mp.id AND mpc.deleted_on IS NULL
					   JOIN mktg_properties cp ON mpc.competitor_property_id = cp.id 
					   AND cp.property_occupancy_types ILIKE ( '%s' ) AND cp.property_occupancy_types IS NOT NULL
					   JOIN mktg_properties scp ON cp.source_property_id = scp.id
					   JOIN mktg_companies cc ON scp.company_id = cc.id
					   LEFT JOIN mktg_property_addresses mpa ON( mpa.property_id = mpc.competitor_property_id AND mpa.address_type = 'Primary' )
					   LEFT JOIN mktg_property_insights mpi ON mp.remote_primary_key::INTEGER = mpi.property_id
					                                           AND mc.remote_primary_key::INTEGER = mpi.cid
					                                           AND scp.remote_primary_key::INTEGER = mpi.competitor_property_id
					                                           AND cc.remote_primary_key::INTEGER = mpi.competitor_cid
					                                           AND mpi.deleted_on IS NULL
					                                           AND COALESCE(mpi.is_competitor, TRUE) = TRUE
					ORDER BY mpc.priority", $intPropertyId, $intCid, $strOccupancyTypeStudent );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllCompetitorsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = sprintf( 'SELECT 
					mpi.*
				FROM mktg_property_insights mpi
				WHERE
					mpi.cid = %d
					AND mpi.property_id = %d 
					AND mpi.deleted_on IS NULL
					AND mpi.deleted_by IS NULL', $intCid, $intPropertyId );

		return parent::fetchMktgPropertyInsights( $strSql, $objDatabase );
	}

	public static function fetchExecutedRentOccupancyByPropertyIdByCid( $objRentInsights, $intCid, $objDatabase, $objCompetitorDatabase, $strInterval = 'MONTH', $boolIsMarket = false, $boolIsPortfolioView = false ) {

		if( false == valId( $objRentInsights->property_id ) ) {
			return [];
		}

		$strCnd = '';
		if( false == $boolIsMarket ) {
			$strCnd = 'AND mpi.is_competitor = TRUE';
		}

		if( 'MONTH' == $strInterval ) {
			$arrmixIntervalDates       = CCachedApplications::fetchStudentMonthIntervalDates( $objRentInsights->property_id, $intCid, $objDatabase );
			$strEffectiveDateCondition = 'AND EXTRACT ( MONTH FROM cutp.effective_date ) = ds.interval_num ';
		} else {
			$arrmixIntervalDates       = CCachedApplications::fetchStudentWeeklyIntervalDates( $objRentInsights->property_id, $intCid, $objDatabase );
			$strEffectiveDateCondition = 'AND cutp.effective_date BETWEEN interval_key - INTERVAL \'6 DAYS\' AND interval_key';
		}

		$strBedroomCondition  = 'AND cutp.bedroom_count IS NOT NULL';
		$strUnitTypeCondition = '';
		$strAVGSpaceCount     = 'AVG ( cutp.total_unit_space_count )';
		if( false != valArr( $objRentInsights->selected_bedrooms ) && 4 > count( $objRentInsights->selected_bedrooms ) ) {
			$strUnitTypeCondition = ', cutp.unit_type_id';
			$strAVGSpaceCount     = 'AVG ( cutp.unit_space_count )';
			if( false == array_search( 4, $objRentInsights->selected_bedrooms ) ) {
				$strBedroomCondition = 'AND cutp.bedroom_count IN ( ' . sqlIntImplode( $objRentInsights->selected_bedrooms ) . ' )';
			} else {
				$strBedroomCondition = 'AND ( cutp.bedroom_count IN ( ' . sqlIntImplode( $objRentInsights->selected_bedrooms ) . ' ) OR 4 < cutp.bedroom_count )';
			}
		}

		$strSelect               = '';
		$strSelectBedroomCount   = '';
		$strBedCountCondition    = '';
		$strUsSelectBedroomCount = '';
		if( true == $boolIsPortfolioView ) {
			$strSelect = ', mpi.cid, mpi.property_id';

			if( true == $objRentInsights->boolUnitTypeLevel ) {
				$strUnitTypeCondition    = ', cutp.unit_type_id';
				$strAVGSpaceCount        = 'AVG ( cutp.unit_space_count )';
				$strSelectBedroomCount   = ',bedroom_count';
				$strUsSelectBedroomCount = ',us.bedroom_count';
				$strBedCountCondition    = 'AND ro.bedroom_count = us.bedroom_count';
			}
		}

		$strSql = sprintf( 'with date_series AS (
										SELECT
											interval_key,
											EXTRACT ( %1$s FROM interval_key ) AS interval_num,
											EXTRACT ( YEAR FROM interval_key ) AS year_num
										FROM
											(
											  SELECT
												  \'%2$s\'::DATE AS start_date,
												  \'%3$s\'::DATE AS end_date
											) academic_dates,
											generate_series ( academic_dates.start_date, academic_dates.end_date, \'1 %1$s\'::INTERVAL ) AS interval_key
									),
									lease_date AS (
										SELECT ( \'10/01/\' || CASE
																	WHEN EXTRACT( MONTH FROM CURRENT_DATE) >=8
																	THEN EXTRACT( YEAR FROM CURRENT_DATE + INTERVAL \'1 Year\')
																	ELSE EXTRACT( YEAR FROM CURRENT_DATE )
																END )::DATE AS prelease_date
									),
									space_count AS (
										SELECT
											CID,
											property_id,
											sum ( space_count ) AS spaces
											%10$s
										FROM
											(
												SELECT
													cutp.CID,
													cutp.property_id,
													%13$s AS space_count,
													RANK ( ) over ( PARTITION BY cutp.CID, cutp.property_id %12$s ORDER BY cutp.created_on DESC ) AS rank
													%12$s
													%10$s
												FROM
													cached_new_unit_type_periods cutp
													JOIN mktg_property_insights mpi ON mpi.cid = %4$d AND mpi.property_id = %5$d AND mpi.deleted_on IS NULL %6$s AND cutp.cid = mpi.competitor_cid AND mpi.competitor_property_id = cutp.property_id
												WHERE
													cutp.CID = mpi.competitor_cid
													AND cutp.property_id = mpi.competitor_property_id
													AND cutp.unit_space_count > 0
													%8$s
												GROUP BY
													cutp.CID,
													cutp.property_id,
													cutp.created_on
													%12$s
													%10$s
											) AS sub
										WHERE
											sub.rank = 1
										GROUP BY
											1,
											2
											%10$s
									),
									rent_occupancy AS (
										SELECT
											ds.interval_key::DATE AS post_month,
											cutp.cid,
											cutp.property_id,
											AVG ( 
												CASE
													WHEN
														CASE
															WHEN \'%11$s\' = \'application_start\' AND ( details -> \'application_counts\' ->> \'started\' )::int > 0 THEN TRUE
															WHEN \'%11$s\' = \'app_partially_completed\' AND ( details -> \'application_counts\' ->> \'partially_completed\' )::int > 0 THEN TRUE
															WHEN \'%11$s\' = \'application_complete\' AND ( details -> \'application_counts\' ->> \'completed\' )::int > 0 THEN TRUE
															WHEN \'%11$s\' = \'lease_partially_completed\' AND ( details -> \'lease_counts\' ->> \'partially_completed\' )::int > 0 THEN TRUE
															WHEN \'%11$s\' = \'lease_complete\' AND ( details -> \'lease_counts\' ->> \'completed\' )::int > 0 THEN TRUE
															ELSE \'%11$s\' = \'lease_approve\' AND ( details -> \'lease_counts\' ->> \'approved\' )::int - ( details -> \'lease_counts\' ->> \'approved_ended\' )::int > 0
														END THEN cutp.base_rent + cutp.amenity_rent + cutp.special_rent
													ELSE NULL
												END ) AS rent,
											SUM (
												CASE
													WHEN \'%11$s\' = \'application_start\' THEN ( details -> \'application_counts\' ->> \'started\' )::int
													WHEN \'%11$s\' = \'app_partially_completed\' THEN  ( details -> \'application_counts\' ->> \'partially_completed\' )::int 
													WHEN \'%11$s\' = \'application_complete\' THEN  ( details -> \'application_counts\' ->> \'completed\' )::int 
													WHEN \'%11$s\' = \'lease_partially_completed\' THEN ( details -> \'lease_counts\' ->> \'partially_completed\' )::int 
													WHEN \'%11$s\' = \'lease_complete\' THEN ( details -> \'lease_counts\' ->> \'completed\' )::int 
													ELSE ( details -> \'lease_counts\' ->> \'approved\' )::int - ( details -> \'lease_counts\' ->> \'approved_ended\' )::int
												END ) AS applications
											%10$s
										FROM
											cached_new_unit_type_periods cutp
											LEFT JOIN mktg_property_insights mpi ON mpi.cid = %4$d AND mpi.property_id = %5$d AND mpi.deleted_on IS NULL %6$s AND cutp.cid = mpi.competitor_cid AND mpi.competitor_property_id = cutp.property_id
											JOIN lease_date ON TRUE
											JOIN date_series AS ds ON TRUE
										WHERE
											cutp.cid = mpi.competitor_cid
											AND cutp.property_id = mpi.competitor_property_id
											AND cutp.unit_type_id IS NOT NULL
											%7$s
											AND EXTRACT ( YEAR FROM cutp.effective_date ) = ds.year_num
											%8$s
											AND ( cutp.lease_start_date <= lease_date.prelease_date AND ( cutp.lease_end_date >= lease_date.prelease_date OR cutp.lease_end_date IS NULL ) )
										GROUP BY
											1,
											cutp.cid,
											cutp.property_id
											%10$s
									)
									SELECT
										ds.interval_key::DATE AS post_month,
										count ( DISTINCT mpi.competitor_cid ) AS company_count,
										count ( DISTINCT mpi.competitor_property_id ) AS property_count,
										round ( avg ( ro.rent ) ) AS rent,
										round ( sum ( ro.applications ) / sum ( us.spaces ) * 100, 2 ) AS occupancy
										%9$s
										%15$s
									FROM
										date_series AS ds
										LEFT JOIN mktg_property_insights mpi ON mpi.cid = %4$d AND mpi.property_id = %5$d AND mpi.deleted_on IS NULL %6$s
										LEFT JOIN space_count AS us ON us.cid = mpi.competitor_cid AND us.property_id = mpi.competitor_property_id
										LEFT JOIN rent_occupancy AS ro ON ro.cid = mpi.competitor_cid AND ro.property_id = mpi.competitor_property_id AND ds.interval_key::DATE = ro.post_month %14$s
									GROUP BY
										1
										%9$s
										%15$s
									ORDER BY
										1', $strInterval, $arrmixIntervalDates[0]['start_date'], $arrmixIntervalDates[0]['end_date'], $intCid, $objRentInsights->property_id, $strCnd, $strEffectiveDateCondition, $strBedroomCondition, $strSelect, $strSelectBedroomCount, $objRentInsights->recognize_lease_at, $strUnitTypeCondition, $strAVGSpaceCount, $strBedCountCondition, $strUsSelectBedroomCount );

		if( false == $boolIsPortfolioView ) {
			return fetchData( $strSql, $objCompetitorDatabase );
		}

		$strPortfolioViewSQL = sprintf( 'SELECT
											sub.cid,
											sub.property_id,
											SUM( sub.occupancy ) as competitor_pre_lease_velocity,
											ROUND( AVG( rent ) ) as rent
											%2$s 
										FROM 
											(
												%1$s 
											) sub
										GROUP BY 
											sub.cid,
											sub.property_id 
											%2$s', $strSql, $strSelectBedroomCount );

		return fetchData( $strPortfolioViewSQL, $objCompetitorDatabase );
	}

	public static function fetchLockedMarketPropertyInsightsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = sprintf( ' SELECT 
						mpi.* 
			       FROM mktg_property_insights mpi
				   WHERE 
						mpi.property_id = %d
						AND mpi.cid = %d
						AND mpi.locked_by IS NOT NULL
						AND mpi.locked_on IS NOT NULL', $intPropertyId, $intCid );

		return parent::fetchMktgPropertyInsights( $strSql, $objDatabase );
	}

}
?>