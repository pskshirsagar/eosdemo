<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgProperties
 * Do not add any new functions to this class.
 */

class CMktgProperties extends CBaseMktgProperties {

	public static function fetchMktgProperties( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgProperty::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgProperty( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgProperty::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSimpleAssociatedMktgPropertiesByMktgPropertyId( $intMktgPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						mp.id,
						mp.company_id,
						mp.property_name,
						mp.property_type,
						mp.remote_primary_key AS rpk_property_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mc.company_name,
						mc.remote_primary_key AS rpk_cid
					FROM
						mktg_properties mp
						JOIN mktg_companies mc ON( mc.id = mp.company_id )
						JOIN mktg_property_competitors mpc ON(
							mpc.competitor_property_id = ' . ( int ) $intMktgPropertyId . '
							AND mpc.property_id = mp.id
							AND mpc.deleted_on is NULL
						)
						JOIN mktg_property_addresses mpa ON(
							mpa.property_id = mp.id
							AND mpa.address_type = \'Primary\'
						)
					ORDER BY
					' . $objDatabase->getCollateSort( 'mp.property_name' ) . ' ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleMktgPropertiesByWorksPropertyIdByLookUpCodeByIds( $intSubjectMktgPropertyId, $arrintCompetitorMktgPropertyIds, $objDatabase ) {

		$strSql = 'SELECT
						mp.id AS lookup_id,
						mp.property_name AS lookup_code,
						mp.source,
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						ROUND( ( SUM( mpf.min_rent ) + SUM( mpf.max_rent ) ) / ( COUNT( mpf.min_rent ) + COUNT( mpf.max_rent ) ) ) AS average_rent,
						mp1.property_name
					FROM
						mktg_properties mp
						LEFT JOIN mktg_floorplan_unit_types mfut ON (
							mfut.property_id = ' . ( int ) $intSubjectMktgPropertyId . '
							AND mfut.competitor_property_id = mp.id
							AND mfut.property_unit_type_id <> 0
						)
						LEFT JOIN mktg_property_floorplans mpf ON(
							mpf.property_id = mp.id
							AND mpf.id = mfut.competitor_property_floorplan_id
						)
						LEFT JOIN mktg_property_unit_types mput ON (
							mput.property_id = mfut.property_id
							AND mput.id = mfut.property_unit_type_id
							AND mput.is_disabled = \'N\'
						)
						JOIN  mktg_properties mp1 ON mfut.property_id = mp1.id
					WHERE
						mp.id IN ( ' . implode( ',', $arrintCompetitorMktgPropertyIds ) . ' )
						AND mp.is_disabled = \'N\'
					GROUP BY
						mp.id,
						mp.property_name,
						mp.source,
						mp1.property_name';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleMktgPropertiesWithLookupCodeByWorksPropertyIdByLookUpCodeById( $intPropertyId, $arrintCompetitorPropertyIds, $strCondition, $objDatabase ) {
		$strSql = 'SELECT
						mfut.competitor_property_id AS lookup_id,
						mp.property_name AS lookup_code,
						mp.source,
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						ROUND( ( SUM( mpf.min_rent ) + SUM( mpf.max_rent ) ) / ( COUNT( mpf.min_rent ) + COUNT( mpf.max_rent ) ) ) AS average_rent,
                        mp1.property_name
					FROM
						mktg_property_floorplans mpf
						JOIN mktg_floorplan_unit_types mfut ON (
							mfut.competitor_property_floorplan_id = mpf.id
							AND mfut.property_id = ' . ( int ) $intPropertyId . '
							AND mfut.property_unit_type_id <> 0
						)
						JOIN mktg_properties mp1 ON mp1.id = mfut.property_id
						JOIN mktg_properties mp ON (
							mp.id = mfut.competitor_property_id
							AND mp.id IN ( ' . implode( ',', $arrintCompetitorPropertyIds ) . ' )
						)
						JOIN mktg_property_unit_types mput ON (
							mput.id = mfut.property_unit_type_id
							AND mput.is_disabled = \'N\'
							' . $strCondition . '
						)
					GROUP BY
						mfut.competitor_property_id,
						mp.property_name,
						mp.source,
						mp1.property_name';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompetitorsByCompetitorPropertyIdByPropertyId( $intCompetitorPropertyId, $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.source,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						mpf.id AS mktg_property_floorplan_id,
						mpf.number_of_bedrooms,
						mpf.number_of_bathrooms,
						ROUND( mpf.min_rent ) AS min_rent,
						ROUND( mpf.max_rent ) AS max_rent,
						ROUND( mpf.min_square_feet ) AS min_square_feet,
						ROUND( mpf.max_square_feet ) AS max_square_feet,
						COUNT( mput.id ) AS mktg_property_unit_type_count
					FROM
						mktg_properties p
						JOIN mktg_property_addresses pa ON( pa.property_id = p.id AND pa.address_type = \'Primary\' )
						LEFT JOIN mktg_property_floorplans mpf ON( mpf.property_id = p.id )
						LEFT JOIN mktg_floorplan_unit_types mfut ON( mfut.property_id = ' . ( int ) $intPropertyId . ' AND mfut.competitor_property_floorplan_id = mpf.id )
						LEFT JOIN mktg_property_unit_types mput ON( mput.id = mfut.property_unit_type_id AND UPPER( mput.lookup_code ) != \'NONE\' )
					WHERE
						p.id = ' . ( int ) $intCompetitorPropertyId . '
					GROUP BY
						p.id,
						p.property_name,
						p.source,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						mpf.id,
						mpf.number_of_bedrooms,
						mpf.number_of_bathrooms,
						ROUND( mpf.min_rent ),
						ROUND( mpf.max_rent ),
						ROUND( mpf.min_square_feet ),
						ROUND( mpf.max_square_feet )
					ORDER BY
						mpf.number_of_bedrooms,
						mpf.number_of_bathrooms,
						ROUND( mpf.min_rent ),
						ROUND( mpf.max_rent ),
						ROUND( mpf.min_square_feet ),
						ROUND( mpf.max_square_feet )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompetitorPropertiesByCompetitorPropertyId( $intPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						p.id,
						p.property_name,
						p.source,
						pa.id AS property_address_id,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						pa.longitude,
						pa.latitude,
						MIN( mpf.min_rent ) AS min_rent,
						MAX( mpf.max_rent ) AS max_rent
					FROM
						mktg_properties p
						JOIN mktg_property_addresses pa ON( pa.property_id = p.id )
						LEFT JOIN mktg_property_floorplans mpf ON ( mpf.property_id = p.id )
					WHERE
						p.id = ' . ( int ) $intPropertyId . '
					GROUP BY
						p.id,
						p.property_name,
						p.source,
						pa.id,
						pa.street_line1,
						pa.street_line2,
						pa.city,
						pa.state_code,
						pa.postal_code,
						pa.longitude,
						pa.latitude
					ORDER BY
						p.id
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleAssociatedCompetitorsByCompetitorNameByStreetLine( $strCompetitorName, $strStreetLine, $objDatabase ) {

		$strSql = 'SELECT
						mp.id,
						mp.remote_primary_key,
						mp.property_name,
						mpa.street_line1,
						mpa.city,
						mpa.state_code,
						mpa.postal_code
					FROM
						mktg_properties mp
						JOIN mktg_property_addresses mpa ON ( mp.id = mpa.property_id AND mpa.address_type = \'Primary\' )
					WHERE
						( SIMILARITY ( mp.property_name, \'' . $strCompetitorName . '\' ) > 0.5 )
						AND ( SIMILARITY ( mpa.street_line1, \'' . $strStreetLine . '\' ) > 0.3 )
						AND mp.is_disabled = \'N\'
						AND mp.source NOT IN ( \'manual\' )
					ORDER BY
						SIMILARITY ( mp.property_name, \'' . $strCompetitorName . '\' ) DESC,
						mp.property_name,
						mpa.street_line1
					LIMIT 5';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyIdsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						mp.id
					FROM
						mktg_properties mp
						JOIN mktg_companies mc ON( mc.id = mp.company_id )
					WHERE
						mp.remote_primary_key = \'' . $intPropertyId . '\'
						AND mc.remote_primary_key = \'' . $intCid . '\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleRecommendedCompetitorPropertiesByMktgPropertyIdByCompetitorPropertyIdsByPropertyAddress( $intPropertyId, $strPropertyIds, $objPropertyAddress, $intCompititorsDistanceLimit, $intCompetitorsLimit, $objDatabase ) {

		$strMileSelect    = '';
		$strMileCondition = '';
		$strStateCondition = '';

		if( true == valObj( $objPropertyAddress, 'CMktgPropertyAddress' ) ) {
			$strStateCondition = ' AND mpa.state_code = \'' . $objPropertyAddress->getStateCode() . '\'';

			if( '' != $objPropertyAddress->getLatitude() && false == is_null( $objPropertyAddress->getLatitude() ) && '' != $objPropertyAddress->getLongitude() && false == is_null( $objPropertyAddress->getLongitude() ) ) {
				$strMileSelect    = 'ROUND( func_calculate_miles( ' . $objPropertyAddress->getLatitude() . ', ' . $objPropertyAddress->getLongitude() . ', CAST( mpa.latitude AS NUMERIC ), CAST( mpa.longitude AS NUMERIC ) ) ) AS distance_in_miles,';
				$strMileCondition = 'AND func_calculate_miles( ' . $objPropertyAddress->getLatitude() . ', ' . $objPropertyAddress->getLongitude() . ', CAST( mpa.latitude AS NUMERIC ), CAST( mpa.longitude AS NUMERIC ) ) <= ' . ( int ) $intCompititorsDistanceLimit . '';
			}
		}

		if( true == empty( $strMileSelect ) ) return NULL;

		$strSql = 'SELECT
						mp.id,
						mp.property_name,
						mp.source,
						mpa.id AS property_address_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
							ELSE NULL
						END AS longitude,
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
							ELSE NULL
						END AS latitude,
						' . $strMileSelect . '
						ROUND( MIN( mpf1.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf1.max_rent ) ) AS max_rent,
						COUNT( mp.id )
					FROM
						mktg_properties mp
						JOIN mktg_property_addresses mpa ON (
							mpa.property_id = mp.id
							AND ( address_type = \'Primary\' OR mpa.address_type IS NULL )
							AND CASE
									WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
									ELSE NULL
								END IS NOT NULL
							AND CASE
									WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
									ELSE NULL
								END IS NOT NULL
							' . $strMileCondition . '
							' . $strStateCondition . '
						)
						JOIN mktg_property_floorplans mpf1 ON ( mpf1.property_id = mp.id )
						JOIN mktg_property_floorplans mpf2 ON (
							mpf2.property_id = ' . ( int ) $intPropertyId . '
							AND mpf1.number_of_bedrooms = mpf2.number_of_bedrooms
							AND mpf1.number_of_bathrooms = mpf2.number_of_bathrooms
							AND mpf2.property_id <> mpf1.property_id
							AND ( ( mpf1.min_square_feet = mpf2.min_square_feet AND mpf1.max_square_feet = mpf2.max_square_feet )
								OR ( mpf1.min_square_feet = mpf2.min_square_feet AND mpf1.max_square_feet IS NULL )
								OR ( mpf1.max_square_feet = mpf2.max_square_feet AND mpf1.min_square_feet IS NULL )
								OR ( mpf1.min_square_feet = mpf2.max_square_feet AND mpf1.max_square_feet IS NULL )
								OR ( mpf1.max_square_feet = mpf2.min_square_feet AND mpf1.min_square_feet IS NULL ) )
						)
					WHERE
						mp.source NOT IN ( \'manual\' )
						AND mp.id NOT IN ( ' . $strPropertyIds . ' )
						AND mp.id <> ' . ( int ) $intPropertyId . '
						AND mp.is_disabled = \'N\'
					GROUP BY
						mp.id,
						mp.property_name,
						mp.source,
						mpa.id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mpa.longitude,
						mpa.latitude
					ORDER BY
						COUNT( mp.id ) DESC,
						distance_in_miles ASC
					LIMIT ' . ( int ) $intCompetitorsLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompetitorPropertiesByCompetitorPropertyIds( $arrintPropertyIds, $objDatabase ) {

		$strSql = 'SELECT
						mp.id,
						mp.property_name,
						mp.source_property_id,
						mp.source,
						mp.property_occupancy_types,
						mpa.id AS property_address_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						/*mpa.longitude,
						mpa.latitude,*/
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
							ELSE NULL
						END AS longitude,
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
							ELSE NULL
						END AS latitude,
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						mc.company_name
					FROM
						mktg_properties mp
						JOIN mktg_property_addresses mpa ON( mpa.property_id = mp.id AND mpa.address_type = \'Primary\' )
						LEFT JOIN mktg_property_floorplans mpf ON ( mpf.property_id = mp.id )
						LEFT JOIN mktg_companies mc ON ( mc.id = mp.company_id AND mc.company_status_type = \'Client\' )
					WHERE
						mp.id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						mp.id,
						mp.property_name,
						mp.source_property_id,
						mp.source,
						mpa.id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mpa.longitude,
						mpa.latitude,
						mc.company_name
					ORDER BY
						' . $objDatabase->getCollateSort( 'mp.property_type' ) . ' ASC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertiesAssociatedToMaximumPropertiesByLimit( $objDatabase ) {

		$strSql = 'SELECT
						mpc.competitor_property_id,
						mp.property_name,
						mp.source,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						COUNT( mpc.id ) AS property_count
					FROM
						mktg_properties mp
						JOIN mktg_property_competitors mpc ON( mpc.competitor_property_id = mp.id AND mpc.deleted_on IS NULL )
						LEFT JOIN mktg_property_addresses mpa ON ( mpa.property_id = mp.id AND ( mpa.address_type = \'Primary\' OR mpa.address_type IS NULL ) )
					WHERE
						mp.is_disabled = \'N\'
					GROUP BY
						mpc.competitor_property_id,
						mp.property_name,
						mp.source,
						mpa.city,
						mpa.state_code,
						mpa.postal_code
					ORDER BY
						property_count DESC
					';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertiesWithMultipleEnabledUrls( $objDatabase ) {

		$strSql = 'SELECT
						mp.id,
						mp.property_name,
						COUNT( mpu.id ) AS mktg_url_count
					FROM
						mktg_properties mp
						JOIN mktg_property_urls mpu ON( mp.id = mpu.property_id AND mpu.is_extract_ready = 1 AND mpu.is_rating = 0 )
					GROUP BY
						mp.id,
						mp.property_name
					HAVING
						COUNT( mpu.is_extract_ready ) > 1
					ORDER BY
						mktg_url_count DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMktgPropertiesCount( $strGroupByClause, $objDatabase ) {

		$strGroupByCondition = ' ';
		if( 'source' == $strGroupByClause || 'property_type' == $strGroupByClause ) {
			$strGroupByCondition = ' WHERE is_disabled = \'N\'';
		}

		$strSql = 'SELECT
						' . $strGroupByClause . ',
						COUNT(id)
					FROM
						mktg_properties
						' . $strGroupByCondition . '
					GROUP BY
						' . $strGroupByClause . '
					ORDER BY
						COUNT DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchIdByRemotePrimaryKey( $strRemotePrimaryKey, $objDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						mktg_properties
					WHERE
						remote_primary_key = \'' . $strRemotePrimaryKey . '\'';

		$arrintIds = fetchData( $strSql, $objDatabase );

		return $arrintIds[0]['id'];
	}

	public static function fetchMktgPropertyDetailsByMktgPropertyIds( $arrintCompetitorPropertyIds, $objDatabase, $intCompetitorHistoricalTrendScreen = 0 ) {

		if( 1 == $intCompetitorHistoricalTrendScreen ) {
			$strSqlOrderBy = $objDatabase->getCollateSort( 'mp.property_name' ) . ' ASC';
		} else {
			$strSqlOrderBy = ' mpf.number_of_bedrooms,
			mpf.number_of_bathrooms,
			mpf.min_rent';
		}

		$strSql = 'SELECT
					   mp.id,
					   mp.property_name,
					   mp.source,
					   mpa.street_line1,
					   mpa.street_line2,
					   mpa.city,
					   mpa.state_code,
					   mpa.postal_code,
					   mpf.id AS floorplan_id,
					   mpf.number_of_bedrooms,
					   mpf.number_of_bathrooms,
					   mpf.min_rent,
					   mpf.max_rent,
					   string_agg( mput.lookup_code, \', \' ) AS lookup_codes,
					   mpc.details->\'competitor_weight\' -> \'weight\' as weight
					FROM
					    mktg_properties mp
						JOIN mktg_property_addresses mpa ON( mpa.property_id = mp.id AND mpa.address_type = \'Primary\' )
						JOIN mktg_property_competitors mpc ON mp.id = mpc.competitor_property_id
						LEFT JOIN mktg_property_floorplans mpf ON ( mpf.property_id = mp.id )
					    LEFT JOIN mktg_floorplan_unit_types mfut ON ( mfut.competitor_property_floorplan_id = mpf.id )
					    LEFT JOIN mktg_property_unit_types mput ON ( mput.id = mfut.property_unit_type_id AND mput.is_disabled = \'N\' )
					WHERE
					    mp.id IN ( ' . implode( ',', $arrintCompetitorPropertyIds ) . ' )
					    AND mp.is_disabled = \'N\'
					GROUP BY
					    mp.id,
					    mp.property_name,
					    mp.source,
					    mpa.street_line1,
					    mpa.street_line2,
					    mpa.city,
					    mpa.state_code,
					    mpa.postal_code,
					    mpf.id,
					    mpf.number_of_bedrooms,
					    mpf.number_of_bathrooms,
					    mpf.min_rent,
					    mpf.max_rent,
					    weight
					ORDER BY ' . $strSqlOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUpdatedMarketingPropertiesRecordsByAllowOverrideRentBySource( $intInterval, $strSource, $objDatabase ) {

		$strSql = 'SELECT
					    DISTINCT mp.id AS manual_property_id,
					    mp.source_property_id,
						mp.id,
						mp.company_id,
						mp2.source
					FROM
					    mktg_properties mp
					    JOIN mktg_property_competitors mpc ON ( mp.id = mpc.competitor_property_id AND mpc.deleted_on IS NULL )
					    JOIN mktg_properties mp2 ON ( mp2.id = mp.source_property_id )
					    JOIN mktg_property_floorplans mpf ON ( mpf.property_id = mp2.id AND EXTRACT ( DAY
					FROM
					    ( CURRENT_DATE - mpf.updated_on ) ) <= ' . ( int ) $intInterval . ' )
					WHERE
					    mp.source = \'' . $strSource . '\'
					    AND mp.is_disabled = \'N\'
					ORDER BY
					    mp.id DESC
						';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompetitorMktgPropertiesByRemotePrimaryKeys( $arrintPropertyIds, $objDatabase ) {

		$strPropertyIds = "'" . implode( "','", $arrintPropertyIds ) . "'";

		$strSql = 'SELECT
					  *
					FROM
					    mktg_properties
					WHERE
						remote_primary_key IN ( ' . $strPropertyIds . ' )
				   ';

		return self::fetchMktgProperties( $strSql, $objDatabase );
	}

	public static function fetchCompetitorMktgPropertiesByIds( $arrintPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}
		$strSql = 'SELECT
					  *
					FROM
					    mktg_properties
					WHERE
						id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				   ';

		return self::fetchMktgProperties( $strSql, $objDatabase );
	}

	public static function fetchChildMktgPropertiesByMktgPropertyId( $intMktgPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						mp.id,
						mp.company_id,
						mp.property_name,
						mp.property_type,
						mp.remote_primary_key AS rpk_property_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mc.company_name,
						mc.remote_primary_key AS rpk_cid
					FROM
						mktg_properties mp
						JOIN mktg_companies mc ON( mc.id = mp.company_id )
						JOIN mktg_property_addresses mpa ON(
							mpa.property_id = mp.id
							AND mpa.address_type = \'Primary\'
						)
					WHERE mp.source_property_id = ' . ( int ) $intMktgPropertyId . '
					ORDER BY
						' . $objDatabase->getCollateSort( 'mp.property_name' ) . ' ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssociatedMktgPropertiesByChildMktgPropertyIds( $arrintMktgPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintMktgPropertyIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						mp.id,
						mp.company_id,
						mp.property_name,
						mp.property_type,
						mp.remote_primary_key AS rpk_property_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mc.company_name,
						mc.remote_primary_key AS rpk_cid,
						mpc.competitor_property_id
					FROM
						mktg_properties mp
						JOIN mktg_companies mc ON( mc.id = mp.company_id )
						JOIN mktg_property_competitors mpc ON(
							mpc.competitor_property_id IN ( ' . implode( ',', $arrintMktgPropertyIds ) . ' )
							AND mpc.property_id = mp.id
							AND mpc.deleted_on IS NULL
						)
						JOIN mktg_property_addresses mpa ON(
							mpa.property_id = mp.id
							AND mpa.address_type = \'Primary\'
						)
					ORDER BY
						' . $objDatabase->getCollateSort( 'mp.property_name' ) . ' ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemMktgPropertiesWithDisabledUrls( $objDatabase ) {

		$strSql = 'SELECT
                       mp.id AS mktg_property_id,
                       mp.property_name,
                       mp.source_property_id,
                       mp.source,
                       mp2.property_name as system_property_name,
                       COUNT ( DISTINCT mpu.url ) AS url_count
                   FROM
                       mktg_properties mp
                       LEFT JOIN mktg_property_urls mpu ON ( mpu.property_id = mp.source_property_id AND mpu.is_rating = 0 )
                       JOIN mktg_property_competitors mpc ON ( mpc.competitor_property_id = mp.id)
                       JOIN mktg_properties mp2 ON ( mp2.id = mp.source_property_id )
                   GROUP BY
                       mp.id,
                       mp.property_name,
                       mp.source,
                       mpc.competitor_property_id,
                       mp2.property_name
                   HAVING
                       SUM ( COALESCE ( mpu.is_extract_ready, 0 ) ) = 0
                   ORDER BY
                       url_count DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemChildPropertyCount( $arrintSourcePropertyIds, $objDatabase ) {

		$strSourcePropertyIds = "'" . implode( "','", $arrintSourcePropertyIds ) . "'";

		$strSql = 'SELECT
						count( id ),
						source_property_id
				   FROM
						mktg_properties
				   WHERE
						source_property_id IN( ' . $strSourcePropertyIds . ' )
				   GROUP BY
						source_property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyByRemotePrimaryKeyByMktgCompanyId( $strRemotePrimaryKey, $intMktgCompanyId, $objDatabase ) {
		return self::fetchMktgProperty( sprintf( 'SELECT * FROM mktg_properties WHERE remote_primary_key = \'%s\' AND company_id = %d', ( string ) $strRemotePrimaryKey, ( int ) $intMktgCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertiesByIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
    					mp.*
    			   	FROM
    					mktg_properties mp
    			   	WHERE
    			   		mp.id IN( ' . implode( ', ', $arrintPropertyIds ) . ' )';

		return self::fetchMktgProperties( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertiesByMktgCompanyId( $intMktgCompanyId, $objDatabase ) {
		if( false == valId( $intMktgCompanyId ) ) return NULL;

		return parent::fetchMktgProperties( sprintf( 'SELECT * FROM mktg_properties WHERE is_disabled=\'N\' and company_id = %d', ( int ) $intMktgCompanyId ), $objDatabase );
	}

	public static function fetchSimilarMktgProperties( $objRentjungleProperty, $strFlagWhere, $objDatabase ) {
		if( true == is_null( $strFlagWhere ) ) {
			$strWhere = '( SIMILARITY ( mp.property_name, \'' . pg_escape_string( $objRentjungleProperty->getRjName() ) . '\' ) > 0.7 )
							AND ( SIMILARITY ( mpa.street_line1, \'' . $objRentjungleProperty->getRjAddress() . '\' ) > 0.6 )
							AND ( mpa.postal_code = \'' . $objRentjungleProperty->getRjZip() . '\' OR mpa.postal_code IS NULL )';
		} else {
			$strWhere = $strFlagWhere;
		}
		$strSql = 'SELECT
							mp.property_name,
							mpa.street_line1,
							mpa.city,
							mpa.postal_code
						FROM
							mktg_properties mp
							JOIN mktg_property_addresses mpa ON( mp.id = mpa.property_id )
						WHERE ' . $strWhere;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveSystemProperties( $strSource, $objDatabase ) {
		$strSql = 'SELECT
   						mp1.id as property_id,
   						mpu.is_extract_ready
					FROM
   						mktg_properties mp1
						LEFT JOIN mktg_property_urls mpu ON ( mpu.property_id = mp1.id AND mpu.is_rating != 1 )
					WHERE
   						mp1.source = \'' . $strSource . '\'
   						AND mp1.id IN ( SELECT DISTINCT mp.source_property_id
                   						FROM mktg_properties mp JOIN mktg_property_competitors mpc ON ( mp.id = mpc.competitor_property_id AND mpc.deleted_on IS NULL)
					) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCompetitorPropertiesBySourcePropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strRegExp = "E'^[\-\+]?[0-9]+[\.]{1}[0-9]+$'";
		$strStudentJoin = "JOIN mktg_properties s_mp ON mpc.competitor_property_id = s_mp.id AND s_mp.property_occupancy_types ILIKE ( '%Student%' )";
		$strSql = sprintf( '
						SELECT 
							DISTINCT mp.id,
							mp.remote_primary_key,
							mp.property_name,
							mp.source_property_id,
							mp.source,
							mc.company_name,
							mc.remote_primary_key as entrata_cid,
							mpa.id AS property_address_id,
							mpa.street_line1,
							mpa.street_line2,
							mpa.city,
							mpa.state_code,
							mpa.postal_code,
							CASE
								WHEN mpa.longitude ~ %1$s AND
									mpa.latitude ~ %1$s THEN
									mpa.longitude
								ELSE NULL
							END AS longitude,
							CASE
								WHEN mpa.longitude ~ %1$s AND
								  mpa.latitude ~ %1$s THEN
								  mpa.latitude
								ELSE NULL
							END AS latitude,
							CONCAT(mp.property_name, \'|\', mpa.latitude, \'|\', mpa.longitude) AS name_lat_long
						FROM mktg_properties mp
							JOIN mktg_property_competitors mpc ON mpc.competitor_property_id = mp.id
							JOIN mktg_companies mc ON (mc.id = mp.company_id)
							JOIN mktg_companies smc ON (smc.id = mpc.company_id AND smc.remote_primary_key = \'%2$d\')
							JOIN mktg_properties smp ON (smp.id = mpc.property_id AND smp.company_id = smc.id AND smp.remote_primary_key = \'%3$d\')
							%4$s
							JOIN mktg_property_addresses mpa ON (mpa.property_id = mp.id AND mpa.address_type = \'Primary\')
						WHERE smp.is_disabled = \'N\' AND
						mpc.deleted_on IS NULL', $strRegExp, $intCid, $intPropertyId, $strStudentJoin );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOtherCompetitorPropertiesBySourcePropertyIdByCid( $objPropertyAddress, $objDatabase, $arrmixFilter = [] ) {
		$strFilterCondition = CPricingWebReconLibrary::applyStudentFilter( $arrmixFilter );
		$strStudentJoin = "AND mp.property_occupancy_types ILIKE ( '%Student%' )";
		$strSql = sprintf( '
						SELECT 
						  DISTINCT mp.id,
			              mp.remote_primary_key,
			              mp.property_name,
			              mp.source,
			              mc.id as company_id,
			              mc.remote_primary_key as entrata_cid,
                          mc.company_name,
			              mpa.id AS property_address_id,
			              mpa.street_line1,
			              mpa.street_line2,
			              mpa.city,
			              mpa.state_code,
			              mpa.postal_code,
			              CASE
			                WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND
			                  mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN
			                  mpa.longitude
			                ELSE NULL
			              END AS longitude,
			              CASE
			                WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND
			                  mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN
			                  mpa.latitude
			                ELSE NULL
			              END AS latitude,
			              CONCAT(mp.property_name, \'|\', mpa.latitude, \'|\', mpa.longitude) AS
			                name_lat_long,
			                mpf_rent.min_rent,
			                mpf_rent.max_rent
			       FROM mktg_properties mp
			            JOIN mktg_companies mc ON mp.company_id = mc.id %1$s AND mc.company_status_type = \'Client\'
			            JOIN mktg_property_details mpd ON mpd.property_id = mp.id
			            JOIN LATERAL ( SELECT * FROM mktg_property_addresses mpa WHERE mpa.property_id = mp.id AND
			              mpa.address_type = \'Primary\' OR mpa.address_type IS NULL ORDER BY mpa.remote_primary_key LIMIT 1 ) AS mpa ON TRUE
			              LEFT JOIN 
			            (
			              SELECT property_id,
			                     ROUND(MIN(mpf_rent.min_rent)) AS min_rent,
			                     ROUND(MAX(mpf_rent.max_rent)) AS max_rent
			              FROM mktg_property_floorplans mpf_rent
			              GROUP BY property_id
			            ) mpf_rent ON mpf_rent.property_id = mp.id
			            LEFT JOIN mktg_property_floorplans mpf ON ( mpf.property_id = mp.id )
			       WHERE mp.is_disabled = \'N\' 
			             %7$s
			             AND (mp.remote_primary_key != \'%2$d\' OR
			             mp.remote_primary_key IS NULL) AND
			             CASE WHEN  mp.source = \'works\' AND mc.company_status_type = \'Client\' AND mpd.number_of_units >= \'%3$d\'  
			                THEN TRUE
			                ELSE FALSE
			             END AND
			             CASE
			               WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND
			             mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
			               ELSE NULL
			             END IS NOT NULL AND
			             CASE
			               WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND
			             mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
			               ELSE NULL
			             END IS NOT NULL AND
			             func_calculate_miles(%4$f::float, %5$f::float,
			               mpa.latitude::float, mpa.longitude::float) < %6$d AND
			               mp.source IN (\'works\', \'system\')
						  LIMIT 500', $strStudentJoin, $objPropertyAddress->getPropertyId(), CPricingWebReconLibrary::COMPETITOR_PROPERTY_MIN_UNIT_COUNT, $objPropertyAddress->getLatitude(), $objPropertyAddress->getLongitude(), CPricingWebReconLibrary::STUDENT_COMPETITORS_DISTANCE_LIMIT, $strFilterCondition );
	return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStudentPropertiesByDatabaseId( $intDatabaseId, $objDatabase ) {
		$strOccupancyTypeStudent = '%Student%';
		$strSql = sprintf( "SELECT
						mc.remote_primary_key as company_id,
						mp.remote_primary_key as source_property_id
					FROM 
						mktg_properties mp
						JOIN mktg_companies mc ON mc.id = mp.company_id
					WHERE
						source = 'works'
						AND is_disabled = 'N'
						AND mp.property_occupancy_types ILIKE '%s'
						AND mc.database_id = %d", $strOccupancyTypeStudent, ( int ) $intDatabaseId );

		return self::fetchMktgProperties( $strSql, $objDatabase );
	}

	public static function updateMktgPropertiesOccupancyTypesByPropertyIdsByCId( $arrintStudentProertyIds, $intCid, $objDatabase ) {
		$strSql = sprintf( "UPDATE
					mktg_properties AS mp
					SET property_occupancy_types = '%s'
					FROM mktg_companies AS mc
					WHERE
					mc.id = mp.company_id
					AND mp.remote_primary_key IN ('%s')
					AND mc.remote_primary_key = %d", CMktgProperty::PROPERTY_OCCUPANCY_TYPE_STUDENT, implode( "','", $arrintStudentProertyIds ), $intCid );

		return executeSql( $strSql, $objDatabase );

	}

}
?>