<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFrequencies
 * Do not add any new functions to this class.
 */

class CMktgFrequencies extends CBaseMktgFrequencies {

	public static function fetchMktgFrequencies( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgFrequency::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgFrequency( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgFrequency::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>