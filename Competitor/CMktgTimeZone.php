<?php

class CMktgTimeZone extends CBaseMktgTimeZone {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAbbr() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGmtOffset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDaylightSavings() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $objDatabase ) {
		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, abbr, gmt_offset, is_daylight_savings )
					VALUES ( ' .
		          $this->sqlId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlAbbr() . ', ' .
		          $this->sqlGmtOffset() . ', ' .
		          $this->sqlIsDaylightSavings() . ' ) ';

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>