<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanies
 * Do not add any new functions to this class.
 */
class CMktgCompanies extends CBaseMktgCompanies {

	public static function fetchMktgCompanyByRemotePrimaryKey( $intRemotePrimaryKey, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						mktg_companies
					WHERE
						remote_primary_key = ' . ( int ) $intRemotePrimaryKey;

		return self::fetchMktgCompany( $strSql, $objDatabase );
	}

	public static function fetchIdByRemotePrimaryKey( $intRemotePrimaryKey, $objDatabase ) {
		$strSql = 'SELECT
						id
					FROM
						mktg_companies
					WHERE
						remote_primary_key = ' . ( int ) $intRemotePrimaryKey;

		$arrintIds = fetchData( $strSql, $objDatabase );

		return $arrintIds[0]['id'];
	}
}
?>