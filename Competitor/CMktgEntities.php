<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgEntities
 * Do not add any new functions to this class.
 */

class CMktgEntities extends CBaseMktgEntities {

	public static function fetchMktgEntities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgEntity::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgEntity( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgEntity::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>