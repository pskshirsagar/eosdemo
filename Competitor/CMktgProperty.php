<?php

class CMktgProperty extends CBaseMktgProperty {

	const PROPERTY_OCCUPANCY_TYPE_STUDENT = 'Student';
	const PROPERTY_SOURCE_TYPE = 'works';
	const PROPERTY_IS_DISABLED = 'N';

    public function valId() {
        $boolIsValid = true;
    	if( true == is_null( $this->getId() ) || false == is_int( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Invalid Id' ) );
        }
        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
	    $boolIsValid = true;
	    return $boolIsValid;
    }

    public function valSource() {
	    $boolIsValid = true;
	    return $boolIsValid;
    }

    public function valCompanyId() {
        $boolIsValid = true;
    	if( true == is_null( $this->getCompanyId() ) || false == is_int( $this->getCompanyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', 'Invalid Company Id' ) );
        }
        return $boolIsValid;
    }

    public function valTimeZoneId() {
        $boolIsValid = true;
    	if( true == is_null( $this->getTimeZoneId() ) || false == is_int( $this->getTimeZoneId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_zone_id', 'Invalid Time Zone Id' ) );
        }
        return $boolIsValid;
    }

	public function valSourcePropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function valPropertyName() {
        $boolIsValid = true;
    	if( true == is_null( $this->getPropertyName() ) || 1 > \psi\CStringService::singleton()->strlen( $this->getPropertyName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_name', 'Competitor Property Name is required.' ) );
		}
        return $boolIsValid;
    }

	public function valPropertyType() {
		$boolIsValid = true;
		return $boolIsValid;
    }

    public function valIsDisabled() {
	    $boolIsValid = true;
	    return $boolIsValid;
    }

	public function valPropertyOccupancyTypes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valRemotePrimaryKey();
            	$boolIsValid &= $this->valSource();
            	$boolIsValid &= $this->valCompanyId();
            	$boolIsValid &= $this->valTimeZoneId();
            	$boolIsValid &= $this->valPropertyName();
				$boolIsValid &= $this->valPropertyType();
            	$boolIsValid &= $this->valIsDisabled();
            	$boolIsValid &= $this->valPropertyOccupancyTypes();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valRemotePrimaryKey();
            	$boolIsValid &= $this->valSource();
            	$boolIsValid &= $this->valCompanyId();
            	$boolIsValid &= $this->valTimeZoneId();
            	$boolIsValid &= $this->valPropertyName();
				$boolIsValid &= $this->valPropertyType();
            	$boolIsValid &= $this->valIsDisabled();
            	$boolIsValid &= $this->valPropertyOccupancyTypes();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>