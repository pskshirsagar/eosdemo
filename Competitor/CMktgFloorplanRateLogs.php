<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFloorplanRateLogs
 * Do not add any new functions to this class.
 */

class CMktgFloorplanRateLogs extends CBaseMktgFloorplanRateLogs {

	public static function fetchMktgFloorplanRateLogsByPropertyFloorplanId( $intPropertyFloorplanId, $objDatabase ) {
		return self::fetchMktgFloorplanRateLogs( sprintf( 'SELECT * FROM mktg_floorplan_rate_logs WHERE property_floorplan_id = %d ORDER BY created_on DESC', ( int ) $intPropertyFloorplanId ), $objDatabase );
	}

	public static function fetchCompetitiveRentByPropertyFloorplanId( $intCid, $intPropertyId, $intUnitTypeId, $boolPropertyLevel, $objDatabase ) {
		if( false == \valId( $intCid ) || false == \valId( $intPropertyId ) ) {
			return NULL;
		}

		$strUnitTypeCondition = '';
		$strUnitTypeSelect = '';
		if( false == $boolPropertyLevel ) {
			$strUnitTypeCondition = ' AND mput.remote_primary_key =  \'' . $intUnitTypeId . '\' ';
			$strUnitTypeSelect = ', unit_type_id';
		}

		$strSql = sprintf('WITH competitors as (
								SELECT
									mpc.property_id,
									mp1.remote_primary_key,
									mpc.competitor_property_id,
									mp.property_name AS competitor_property_name,
									mpc.company_id,
									mp.id,
									CASE
										WHEN ( ( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'data_source\' = \'0\' )
										THEN mp.source_property_id
										ELSE mp.id
									END AS mktg_property_id,
									( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'weight\' AS weight,
									( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'data_source\' AS is_manual,
									COALESCE( ( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'compare_advertised_rate\', \'avg\' ) AS compare_advertised_rate,
									MAX( mpf.updated_on::DATE ) AS updated_on
								FROM
									mktg_property_competitors mpc
									JOIN mktg_companies mc ON ( mc.id = mpc.company_id )
									JOIN mktg_properties mp ON mp.id = mpc.competitor_property_id
									JOIN mktg_properties mp1 ON ( mp1.id = mpc.property_id AND mp1.company_id = mc.id AND mp1.is_disabled ILIKE ( \'N\' ) )
									JOIN mktg_property_floorplans mpf ON mpf.property_id = CASE
																								WHEN ( ( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'data_source\' = \'0\' )
																								THEN mp.source_property_id
																								ELSE mp.id
																							END
								WHERE
								mc.remote_primary_key = %1$d
								AND mp1.remote_primary_key IN ( \'%2$d\' )
								AND mpc.deleted_on IS NULL
								AND ( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'data_source\' IS NOT NULL
								AND ( mpc.details ->> \'competitor_weight\' )::JSONB ->> \'weight\' <> \'\'
								GROUP BY
								mpc.property_id,
								mpc.company_id,
								mpc.details,
								mpc.competitor_property_id,
								mp1.remote_primary_key,
								mp.property_name,
								mp.id )
							SELECT
								subject_property_id,
								compare_advertised_rate,
								ROUND ( CASE
											WHEN \'min\' = compare_advertised_rate THEN avg( min_rent )
											WHEN \'max\' = compare_advertised_rate THEN avg( max_rent )
											WHEN \'avg\' = compare_advertised_rate THEN avg( avg_rent )
										END ) AS competitive_rent,
								month_num,
								year_num
								%4$s
							FROM
							(
								SELECT
									mpcw.remote_primary_key AS subject_property_id,
									mfut.property_id,
									mfut.competitor_property_id,
									mpcw.competitor_property_name,
									mput.remote_primary_key AS unit_type_id,
									mpcw.compare_advertised_rate,
									min( mfrl.min_rent ) AS min_rent,
									max( mfrl.max_rent ) AS max_rent,
									( min( mfrl.min_rent ) + max( mfrl.max_rent ) ) / 2 AS avg_rent,
									date_part( \'MONTH\', mfrl.created_on::DATE ) AS month_num,
									date_part( \'YEAR\', mfrl.created_on::DATE ) AS year_num,
									row_number() over( PARTITION BY mput.remote_primary_key, mfut.property_id, mfut.competitor_property_id, date_part ( \'MONTH\', mfrl.created_on::DATE ), date_part ( \'YEAR\', mfrl.created_on::DATE ) ORDER BY mfrl.created_on::DATE DESC ) AS row_num
								FROM
									competitors mpcw
									JOIN mktg_floorplan_unit_types mfut ON mpcw.property_id = mfut.property_id  AND mpcw.mktg_property_id = mfut.competitor_property_id
									JOIN mktg_property_unit_types mput ON mput.id = mfut.property_unit_type_id AND mpcw.company_id = mput.company_id AND mput.property_id = mfut.property_id
									JOIN mktg_floorplan_rate_logs mfrl ON mfut.competitor_property_floorplan_id = mfrl.property_floorplan_id
								WHERE
									mfrl.created_on >= date_trunc( \'MONTH\', now() )::DATE - INTERVAL \'12 MONTH\'
									%3$s
								GROUP BY
									mpcw.remote_primary_key,
									mpcw.competitor_property_name,
									mfut.property_id,
									mput.remote_primary_key,
									mfut.competitor_property_id,
									mpcw.compare_advertised_rate,
									mfrl.created_on::DATE
							) sub
						WHERE
							row_num = 1
						GROUP BY
							subject_property_id,
							compare_advertised_rate,
							month_num,
							year_num
							%4$s', ( int ) $intCid, ( int ) $intPropertyId, $strUnitTypeCondition, $strUnitTypeSelect );

		return fetchData( $strSql, $objDatabase );
	}

}
?>