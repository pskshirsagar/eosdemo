<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyFloorplans
 * Do not add any new functions to this class.
 */

class CMktgPropertyFloorplans extends CBaseMktgPropertyFloorplans {

	public static function fetchMktgPropertyFloorplansByPropertyIdByMktgPropertyId( $intPropertyId, $intMktgPropertyId, $objDatabase ) {
		$strSql = 'SELECT
						mpf.id,
						mpf.remote_primary_key,
						mpf.company_id,
						mpf.property_id,
						mpf.name,
						mpf.description,
						mpf.min_rent,
						mpf.max_rent,
						mpf.min_deposit,
						mpf.max_deposit,
						mpf.is_manual_range,
						mpf.min_square_feet,
						mpf.max_square_feet,
						mpf.number_of_units,
						mpf.number_of_floors,
						mpf.number_of_rooms,
						mpf.number_of_bedrooms,
						mpf.number_of_bathrooms,
						mpf.created_on,
						mpf.updated_on,
						mfut.property_unit_type_id
					FROM
						mktg_property_floorplans mpf
						LEFT JOIN mktg_floorplan_unit_types mfut ON(
							mfut.property_id = ' . ( int ) $intPropertyId . '
							AND mfut.competitor_property_id = mpf.property_id
							AND mfut.competitor_property_floorplan_id = mpf.id
						)
					WHERE
						mpf.property_id = ' . ( int ) $intMktgPropertyId . '
					ORDER BY
						mpf.number_of_bedrooms,
						mpf.number_of_bathrooms,
						mpf.min_rent,
						mpf.max_rent,
						mpf.min_square_feet,
						mpf.max_square_feet';

		return parent::fetchMktgPropertyFloorplans( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyFloorplansByPropertyId( $intPropertyId, $objDatabase ) {
		return parent::fetchMktgPropertyFloorplans( sprintf( 'SELECT * FROM mktg_property_floorplans WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorplansByMktgPropertyIds( $arrintPropertyIds, $objDatabase ) {

		$strSql = 'SELECT
						mpf.*
					FROM
						mktg_property_floorplans mpf
					WHERE
						mpf.property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' );';

		return self::fetchMktgPropertyFloorplans( $strSql, $objDatabase );
	}

	public static function fetchSimpleMktgPropertyFloorPlansByPropertIdByLookUpCode( $intMktgPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						UPPER( mput.lookup_code ) AS lookup_code,
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						ROUND( MIN( mpf.min_square_feet ) ) AS min_sqft,
						ROUND( MAX( mpf.max_square_feet ) ) AS max_sqft,
						ROUND( ( SUM( mpf.min_rent ) + SUM( mpf.max_rent ) ) / ( COUNT( mpf.min_rent ) + COUNT( mpf.max_rent ) ) ) AS average_rent,
						ROUND( ( SUM( mpf.min_square_feet ) + SUM( mpf.max_square_feet ) ) / ( COUNT( mpf.min_square_feet ) + COUNT( mpf.max_square_feet ) ) ) AS average_sqft,
						mp.property_name
					FROM
						mktg_property_unit_types mput
						JOIN mktg_properties mp ON mp.id = mput.property_id
						LEFT JOIN mktg_floorplan_unit_types mfut ON (
								mfut.property_id = mput.property_id
								AND mfut.property_unit_type_id = mput.id
						)
						LEFT JOIN mktg_property_floorplans mpf ON (
								mpf.property_id = mfut.competitor_property_id
								AND mpf.id = mfut.competitor_property_floorplan_id
						)
					WHERE
						mput.property_id = ' . ( int ) $intMktgPropertyId . '
						AND mput.is_disabled = \'N\'
					GROUP BY
						mput.lookup_code, mp.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRentRange( $objDatabase ) {

		$strSql = 'SELECT
						mpa.postal_code,
						ROUND( AVG ( mpf.min_rent ), 0 ) AS min_rent,
						ROUND( AVG ( mpf.max_rent ), 0 ) AS max_rent
					FROM
						mktg_property_floorplans mpf
						LEFT JOIN mktg_property_addresses mpa ON( mpa.property_id = mpf.property_id )
					GROUP BY
						mpa.postal_code';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSimpleMktgPropertyUnitTypeDetailsByPropertyIdByMktgPropertyFloorPlanIds( $intPropertyId, $arrintMktgPropertyFloorplanIds, $objDatabase ) {

		$strSql = 'SELECT
						mpf.id,
						mput.lookup_code
					FROM
						mktg_property_floorplans mpf
						JOIN mktg_floorplan_unit_types mfut ON(
							mpf.id = mfut.competitor_property_floorplan_id
							AND mfut.property_id = ' . ( int ) $intPropertyId . '
						)
						JOIN mktg_property_unit_types mput ON ( mput.id = mfut.property_unit_type_id )
					WHERE
						mpf.id IN( ' . implode( ',', $arrintMktgPropertyFloorplanIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchMktgPropertyFloorplansWithAssociatedUnitTypeCountByMktgPropertyId( $intMktgPropertyId, $intDuplicateFloorplanProperty = NULL, $objDatabase ) {

		if( 1 == $intDuplicateFloorplanProperty ) {
			$strSql = 'SELECT mpf2.*,
							  mpf1.associated_unit_type_count
						FROM mktg_property_floorplans mpf2
							 JOIN(
									SELECT mpf.min_rent,
											mpf.max_rent,
											mpf.min_square_feet,
											mpf.max_square_feet,
											mpf.number_of_bedrooms,
											mpf.number_of_bathrooms,
											COUNT(mfut.id) AS associated_unit_type_count
									FROM mktg_property_floorplans mpf
										 LEFT JOIN mktg_floorplan_unit_types mfut ON(
							mfut.competitor_property_floorplan_id = mpf.id
						)
						WHERE
							mpf.property_id = ' . ( int ) $intMktgPropertyId . '
						GROUP BY
							mpf.min_rent,
							mpf.max_rent,
							mpf.min_square_feet,
							mpf.max_square_feet,
							mpf.number_of_bedrooms,
							mpf.number_of_bathrooms
						HAVING count(*) > 1) AS mpf1 ON (
								mpf1.min_rent = mpf2.min_rent
 								AND mpf1.max_rent = mpf2.max_rent
 								AND mpf1.min_square_feet = mpf2.min_square_feet
								AND mpf1.max_square_feet = mpf2.max_square_feet
								AND mpf1.number_of_bedrooms = mpf2.number_of_bedrooms
 								AND mpf1.number_of_bathrooms = mpf2.number_of_bathrooms ) ';
		} else {
				$strSql = 'SELECT
								mpf.*,
								COUNT(mfut.id) AS associated_unit_type_count
							FROM
								mktg_property_floorplans mpf
								LEFT JOIN mktg_floorplan_unit_types mfut ON(
									mfut.competitor_property_floorplan_id = mpf.id
								)
							WHERE
								mpf.property_id = ' . ( int ) $intMktgPropertyId . '
							GROUP BY mpf.id';
		}

		return parent::fetchMktgPropertyFloorplans( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertiesWithDuplicateFloorplanCount( $objDatabase, $intOffset = NULL, $intPageSize = NULL ) {

		$strCondition = '';
		if( false == is_null( $intOffset ) && false == is_null( $intPageSize ) ) {
			$strOffsetCondition = ' OFFSET ' . ( int ) $intOffset . '
							  LIMIT ' . ( int ) $intPageSize;
		}
		$strSql = 'SELECT
						t.property_id,
						t.property_name,
						t.source,
						COUNT( t.cnt ) AS duplicate_floorplan_count
					FROM
						(
							SELECT
								mpf.property_id,
								mp.property_name,
								mp.source,
								COUNT ( 1 ) AS cnt
							FROM
								mktg_property_floorplans mpf
								JOIN mktg_properties mp ON ( mp.company_id = mpf.company_id AND mp.id = mpf.property_id )
							WHERE
								mpf.min_rent IS NOT NULL
								AND mpf.max_rent IS NOT NULL
								AND mpf.min_square_feet IS NOT NULL
								AND mpf.max_square_feet IS NOT NULL
								AND mpf.number_of_bedrooms IS NOT NULL
								AND mpf.number_of_bathrooms IS NOT NULL
							GROUP BY
								mpf.property_id,
								mp.property_name,
								mp.source,
								mpf.min_rent,
								mpf.max_rent,
								mpf.min_square_feet,
								mpf.max_square_feet,
								mpf.number_of_bedrooms,
								mpf.number_of_bathrooms
							HAVING
								COUNT ( 1 ) > 1
							ORDER BY
								mpf.property_id
						) AS t
					GROUP BY
						t.property_id,
						t.property_name,
						t.source
					ORDER BY
						COUNT( t.cnt ) DESC ' . $strOffsetCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgCompetitorPropertyUnitTypesByPropertyId( $intPropertyId, $objDatabase ) {
		$strSql = '
                    select DISTINCT mp.property_name,mpf.property_id,mfut.property_unit_type_id,round(min(mpf.min_rent)) as min_rent,round(max(mpf.max_rent)) as max_rent,mput.lookup_code,mput.name,round(min(mpf.min_square_feet)) as min_square_feet,round(max(mpf.max_square_feet)) as max_square_feet
					from 
					mktg_property_floorplans mpf
					INNER JOIN mktg_floorplan_unit_types mfut on mfut.competitor_property_id = mpf.property_id and mpf.id = mfut.competitor_property_floorplan_id
					INNER JOIN mktg_property_unit_types mput on mput.id = mfut.property_unit_type_id AND mput.is_disabled = \'N\'
					INNER JOIN mktg_properties mp on  mp.id = mpf.property_id AND mfut.competitor_property_id = mp.id
					where  mpf.property_id =' . ( int ) $intPropertyId . ' group by
                    mp.property_name,mpf.property_id,mfut.property_unit_type_id,mput.is_disabled,mput.lookup_code,mput.name ORDER BY mput.lookup_code';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleMktgPropertyFloorPlanByUnitTypeIdByPropertyId( $intUnitTypeId, $intMktgPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						mp.property_name,
						mfut.competitor_property_id AS competitor_id,
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						ROUND((SUM(mpf.min_rent) + SUM(mpf.max_rent)) /(COUNT(mpf.min_rent) + COUNT(mpf.max_rent))) AS average_rent
					FROM
						mktg_property_unit_types mput
						JOIN mktg_properties mp ON mp.id = mput.property_id
						LEFT JOIN mktg_floorplan_unit_types mfut ON (
								mfut.property_id = mput.property_id
								AND mfut.property_unit_type_id = mput.id
						)
						LEFT JOIN mktg_property_floorplans mpf ON (
								mpf.property_id = mfut.competitor_property_id
								AND mpf.id = mfut.competitor_property_floorplan_id
						)
					WHERE
						mput.property_id = ' . ( int ) $intMktgPropertyId . '
						AND mput.remote_primary_key = \'' . ( int ) $intUnitTypeId . '\'
						AND mput.is_disabled = \'N\'
					GROUP BY
						mp.property_name,mfut.competitor_property_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyFloorplanByPropertyIds( $arrintBathrooms, $arrintBedrooms, $arrintMktgPropertyIds, $boolIsReturnAllData = true, $objDatabase, $strRatesMonth = NULL, $strRatesYear = NULL ) {

		if( false == array_search( 4, $arrintBathrooms ) ) {
			$strBathroomCondition = ' mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' )';
		} else {
			$strBathroomCondition = ' ( mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' ) OR 4 < mpf.number_of_bathrooms )';
		}

		if( false == array_search( 4, $arrintBedrooms ) ) {
			$strBedroomCondition = ' mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' )';
		} else {
			$strBedroomCondition = ' ( mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' ) OR 4 < mpf.number_of_bedrooms )';
		}

		$strSqlAllData = 'SELECT 
						mpf.id,
						mfut.property_unit_type_id,
						mpf.property_id,
						mfrl.min_rent AS min_rent,
						mfrl.max_rent AS max_rent,
						mpf.created_on,
						mpf.updated_on,
						mfrl.created_on as daterate_log,
						date_part(\'MONTH\', mfrl.created_on) AS month_num,
						date_part(\'YEAR\', mfrl.created_on) AS year_num,
						to_char(mfrl.created_on, \'Mon\') as month_name
					FROM mktg_floorplan_rate_logs mfrl
						JOIN mktg_property_floorplans mpf on mfrl.property_floorplan_id = mpf.id
						JOIN mktg_floorplan_unit_types mfut on mfut.competitor_property_id = mpf.property_id AND mfut.competitor_property_floorplan_id = mpf.id
						JOIN mktg_property_unit_types mput on mput.id = mfut.property_unit_type_id
					WHERE mpf.property_id IN ( ' . sqlIntImplode( $arrintMktgPropertyIds ) . ' ) 
					AND' . $strBedroomCondition . '
					AND' . $strBathroomCondition . '
					AND mfrl.created_on >= date_trunc(\'MONTH\', now())::DATE - INTERVAL \'1 YEAR\' ';

		if( true == $boolIsReturnAllData ) {
			$strSql = ' SELECT 
						AVG(min_rent) as min,
						AVG(max_rent) as max,
						month_num,
						year_num,
						month_name
						FROM (
						' . $strSqlAllData . '
						)
						As competitive_data
						GROUP BY 
							month_name,
							month_num,
							year_num;';
		} else {
			$strSql = $strSqlAllData . ' AND EXTRACT ( \'Mon\' FROM mfrl.created_on ) = EXTRACT ( \'Mon\' FROM \'' . $strRatesMonth . '\'::DATE ) AND date_part(\'YEAR\', mfrl.created_on) = ' . $strRatesYear;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMonthlyFloorplanHistoricalByPropertyIds( $arrintBathrooms, $arrintBedrooms, $arrintMktgPropertyIds, $objDatabase ) : array {

			if( false == array_search( 4, $arrintBathrooms ) ) {
				$strBathroomCondition = ' mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' )';
			} else {
				$strBathroomCondition = ' ( mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' ) OR 4 < mpf.number_of_bathrooms )';
			}

			if( false == array_search( 4, $arrintBedrooms ) ) {
				$strBedroomCondition = ' mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' )';
			} else {
				$strBedroomCondition = ' ( mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' ) OR 4 < mpf.number_of_bedrooms )';
			}

		$strSql = sprintf( 'WITH date_series AS (
							        SELECT generate_series( (cast(date_trunc(\'month\', current_date) as date)) - INTERVAL \'12 MONTHS\', CURRENT_DATE, \'1 month\'::INTERVAL)::DATE AS effective_date
							          )
								 SELECT
								        effective_date,
								        AVG(min_rent) as min,
								        AVG(max_rent) as max,
								        date_part(\'MONTH\', ds.effective_date ) AS month_num,
								        date_part(\'YEAR\', ds.effective_date ) AS year_num,
								        to_char(ds.effective_date, \'Mon\') AS month_name
								 FROM date_series AS ds
								    LEFT JOIN (
								          SELECT mpf.id,
								                 mfut.property_unit_type_id,
								                 mpf.property_id,
								                 mfrl.min_rent AS min_rent,
								                 mfrl.max_rent AS max_rent,
								                 mpf.created_on,
								                 mfrl.created_on as daterate_log
								          FROM mktg_floorplan_rate_logs mfrl
								               JOIN mktg_property_floorplans mpf on mfrl.property_floorplan_id =
								                 mpf.id
								               JOIN mktg_floorplan_unit_types mfut on mfut.competitor_property_id =
								                 mpf.property_id AND mfut.competitor_property_floorplan_id = mpf.id
								               JOIN mktg_property_unit_types mput on mput.id = mfut.property_unit_type_id
								          WHERE mpf.property_id IN ( %s ) AND
                                                %s AND
                                                %s AND
								                mfrl.created_on >= date_trunc(\'MONTH\', now())::DATE - INTERVAL \'1 YEAR\'
								      ) As cd on date_part(\'MONTH\', cd.daterate_log) = date_part(\'MONTH\', ds.effective_date )
								        AND date_part(\'YEAR\', cd.daterate_log) = date_part(\'YEAR\', ds.effective_date )
								 GROUP BY month_name,
								          month_num,
								          effective_date,
								          year_num
								  ORDER BY
									effective_date ASC', sqlIntImplode( $arrintMktgPropertyIds ), $strBedroomCondition, $strBathroomCondition );

		return ( array ) fetchData( $strSql, $objDatabase );
	}

	public static function fetchDailyFloorplansByUnitTypeIdByPropertyIds( $arrintBathrooms, $arrintBedrooms, $arrintMktgPropertyIds, $objDatabase ) {

		if( false == array_search( 4, $arrintBathrooms ) ) {
			$strBathroomCondition = ' mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' )';
		} else {
			$strBathroomCondition = ' ( mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' ) OR 4 < mpf.number_of_bathrooms )';
		}

		if( false == array_search( 4, $arrintBedrooms ) ) {
			$strBedroomCondition = ' mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' )';
		} else {
			$strBedroomCondition = ' ( mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' ) OR 4 < mpf.number_of_bedrooms )';
		}

		$strSql = sprintf( 'WITH date_series AS (
										SELECT generate_series((cast(date_trunc(\'month\', current_date) as date)) - INTERVAL \'12 MONTHS\', CURRENT_DATE, \'1 day\'::INTERVAL)::DATE  AS effective_date
										)
									SELECT 
									min_rent,
									max_rent,
									ds.effective_date,
									date_part(\'MONTH\', ds.effective_date) AS month_num,
									date_part(\'YEAR\', ds.effective_date) AS year_num,
									to_char(ds.effective_date, \'Mon\') AS month_name
									FROM date_series AS ds
									LEFT JOIN (
										SELECT
											avg(mfrl.min_rent) AS min_rent,
											avg(mfrl.max_rent) AS max_rent,
											mfrl.created_on::DATE as created_on
										FROM mktg_floorplan_rate_logs mfrl
										JOIN mktg_property_floorplans mpf on mfrl.property_floorplan_id = mpf.id 
										JOIN mktg_floorplan_unit_types mfut on mfut.competitor_property_id =  mpf.property_id AND mfut.competitor_property_floorplan_id = mpf.id
										JOIN mktg_property_unit_types mput on mput.id = mfut.property_unit_type_id
										WHERE mfrl.created_on::date >= (cast(date_trunc(\'month\', current_date) as date)) - INTERVAL \'12 MONTHS\'
										AND mpf.property_id IN ( %s ) 
										AND %s 
										AND %s
										GROUP BY
											mfrl.created_on::DATE
									) AS dr ON ds.effective_date = dr.created_on', sqlIntImplode( $arrintMktgPropertyIds ), $strBedroomCondition, $strBathroomCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMonthlySystemFloorplansByManualPropertyIds( $arrintBathrooms, $arrintBedrooms, $arrintCompetitorPropertyIds, $objDatabase ) {

		if( false == array_search( 4, $arrintBathrooms ) ) {
			$strBathroomCondition = ' mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' )';
		} else {
			$strBathroomCondition = ' ( mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' ) OR 4 < mpf.number_of_bathrooms )';
		}

		if( false == array_search( 4, $arrintBedrooms ) ) {
			$strBedroomCondition = ' mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' )';
		} else {
			$strBedroomCondition = ' ( mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' ) OR 4 < mpf.number_of_bedrooms )';
		}

		$strSql = sprintf( 'WITH date_series AS (
							        SELECT generate_series( (cast(date_trunc(\'month\', current_date) as date)) - INTERVAL \'12 MONTHS\', CURRENT_DATE, \'1 month\'::INTERVAL)::DATE AS effective_date
						          )
								SELECT
									 ds.effective_date, 
									 AVG(min_rent) as MIN,
								      AVG(max_rent) as MAX,
									 date_part(\'MONTH\', ds.effective_date ) AS month_num,
								     date_part(\'YEAR\', ds.effective_date ) AS year_num,
								     to_char(ds.effective_date, \'Mon\') AS month_name
								FROM date_series AS ds
								LEFT JOIN(
								      SELECT mpf.id,
								             mpf.property_id,
								             mfrl.min_rent AS min_rent,
								             mfrl.max_rent AS max_rent,
								             mfrl.created_on
								      FROM mktg_floorplan_rate_logs mfrl
								           JOIN mktg_property_floorplans mpf on mfrl.property_floorplan_id =
								             mpf.id
								      WHERE mpf.property_id IN ( %s )
								            AND %s
								            AND %s
								            AND mfrl.created_on >= date_trunc(\'MONTH\', now())::DATE - INTERVAL \'1 YEAR\'
								    ) As cd on date_part(\'MONTH\', cd.created_on) = date_part(\'MONTH\', ds.effective_date )
								    AND date_part(\'YEAR\', cd.created_on) = date_part(\'YEAR\', ds.effective_date)
								GROUP BY 
										ds.effective_date,
										month_name,
								        month_num,
								        year_num,
								        month_name 
								        ORDER BY ds.effective_date ASC', sqlIntImplode( $arrintCompetitorPropertyIds ), $strBedroomCondition, $strBathroomCondition );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSystemDrilInDataByManualPropertyIds( $arrintBathrooms, $arrintBedrooms, $strDate, $strRatesYear, $arrintCompetitorPropertyIds, $objDatabase ) {

		if( false == array_search( 4, $arrintBathrooms ) ) {
			$strBathroomCondition = ' mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' )';
		} else {
			$strBathroomCondition = ' ( mpf.number_of_bathrooms IN ( ' . sqlIntImplode( $arrintBathrooms ) . ' ) OR 4 < mpf.number_of_bathrooms )';
		}

		if( false == array_search( 4, $arrintBedrooms ) ) {
			$strBedroomCondition = ' mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' )';
		} else {
			$strBedroomCondition = ' ( mpf.number_of_bedrooms IN ( ' . sqlIntImplode( $arrintBedrooms ) . ' ) OR 4 < mpf.number_of_bedrooms )';
		}

	$strSql = sprintf( ' SELECT mpf.id,
										mpf.property_id,
										mfrl.min_rent AS min_rent,
										mfrl.max_rent AS max_rent,
										mfrl.created_on as daterate_log
									FROM mktg_floorplan_rate_logs mfrl
									JOIN mktg_property_floorplans mpf on mfrl.property_floorplan_id = mpf.id
								      WHERE mpf.property_id IN (%s)
										 AND mfrl.min_rent IS NOT NULL
										 AND mfrl.max_rent IS NOT NULL
										 AND (%s)
										 AND (%s)
										 AND EXTRACT(\'Mon\' FROM mfrl.created_on) = EXTRACT(\'Mon\' FROM \'%s\'::DATE) AND date_part(\'YEAR\', mfrl.created_on) = \'%s\'', sqlIntImplode( $arrintCompetitorPropertyIds ), $strBathroomCondition, $strBedroomCondition, $strDate, $strRatesYear );

		return fetchData( $strSql, $objDatabase );
	}

}
?>