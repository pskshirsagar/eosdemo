<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgScraperServers
 * Do not add any new functions to this class.
 */

class CMktgScraperServers extends CBaseMktgScraperServers {

	public static function fetchMktgScraperServersOrderById( $objDatabase ) {
		$strSql = 'SELECT * FROM mktg_scraper_servers ORDER BY id';
		return parent::fetchMktgScraperServers( $strSql, $objDatabase );
	}

	public static function fetchMktgScraperServersByFailCountLimitByRandomOrder( $intFailCountLimit, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						mktg_scraper_servers
					WHERE
						fail_count < ' . ( int ) $intFailCountLimit . '
						AND is_active = 1
					ORDER BY RANDOM()';

		return parent::fetchMktgScraperServers( $strSql, $objDatabase );
	}

}
?>