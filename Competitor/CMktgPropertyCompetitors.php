<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyCompetitors
 * Do not add any new functions to this class.
 */

class CMktgPropertyCompetitors extends CBaseMktgPropertyCompetitors {

	public static function fetchActiveMktgPropertyCompetitorsByRemotePropertyKeyByRemoteCompanyKey( $strRemotePropertyKey, $intRemoteCompanyKey, $objDatabase, $boolIsStudentProperty = false ) {
		$strStudentJoin = '';
		$strIsDisabled = 'N';
		if( true == $boolIsStudentProperty ) {
			$strStudentJoin = "JOIN mktg_properties m_mp ON mpc.competitor_property_id = m_mp.id AND m_mp.property_occupancy_types ILIKE ( '%Student%' )";
		}
		$strSql = sprintf( "SELECT
						mpc.*
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON (
							mc.id = mpc.company_id
							AND mc.remote_primary_key = %d )
						JOIN mktg_properties mp ON (
							mp.id = mpc.property_id
							AND mp.company_id = mc.id
							AND mp.remote_primary_key = '%s'
							AND mp.is_disabled ilike '%s' )
						%s
					WHERE
						mpc.deleted_on IS NULL
					ORDER BY
						mpc.priority", ( int ) $intRemoteCompanyKey, $strRemotePropertyKey, $strIsDisabled, $strStudentJoin );

		return parent::fetchMktgPropertyCompetitors( $strSql, $objDatabase );
	}

	public static function fetchActiveMktgPropertyHavingValidAddressByRemotePropertyKeyByRemoteCompanyKey( $strRemotePropertyKey, $intRemoteCompanyKey, $objDatabase ) {
		$strSql = 'SELECT
						mpc.*,
						mp.property_name
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON (
							mc.id = mpc.company_id
							AND mc.remote_primary_key = ' . ( int ) $intRemoteCompanyKey . ' )
						JOIN mktg_properties mp ON (
							mp.id = mpc.property_id
							AND mp.company_id = mc.id
							AND mp.remote_primary_key = \'' . $strRemotePropertyKey . '\'
							AND mp.is_disabled = \'N\' )
						JOIN mktg_property_addresses mpa ON (
							mpa.property_id = mpc.competitor_property_id
							AND mpa.address_type = \'Primary\' )
					WHERE
						mpc.deleted_on IS NULL';

		return parent::fetchMktgPropertyCompetitors( $strSql, $objDatabase );
	}

    public static function fetchActivePropertyCompetitorByRemotePropertyKeyByCompetitorPropertyIdByRemoteCompanyKey( $strRemotePropertyKey, $intCompetitorPropertyId, $intRemoteCompanyKey, $objDatabase ) {
    	$strSql = 'SELECT
						mpc.*
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON (
							mc.id = mpc.company_id
							AND mc.remote_primary_key = ' . ( int ) $intRemoteCompanyKey . ' )
						JOIN mktg_properties mp ON (
							mp.id = mpc.property_id
							AND mp.company_id = mc.id
							AND mp.remote_primary_key = \'' . $strRemotePropertyKey . '\'
							AND mpc.competitor_property_id = ' . ( int ) $intCompetitorPropertyId . '
							AND mp.is_disabled = \'N\' )
					WHERE
						mpc.deleted_on IS NULL
						LIMIT 1 ';

    	return parent::fetchMktgPropertyCompetitor( $strSql, $objDatabase );
    }

    public static function fetchPropertyCompetitorByRemotePropertyKeyByCompetitorPropertyIdByRemoteCompanyKey( $strRemotePropertyKey, $intCompetitorPropertyId, $intRemoteCompanyKey, $objDatabase ) {
    	$strSql = 'SELECT
						mpc.*
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON (
							mc.id = mpc.company_id
							AND mc.remote_primary_key = ' . ( int ) $intRemoteCompanyKey . ' )
						JOIN mktg_properties mp ON (
							mp.id = mpc.property_id
							AND mp.company_id = mc.id
							AND mp.remote_primary_key = \'' . $strRemotePropertyKey . '\' )
					WHERE
						mpc.competitor_property_id = ' . ( int ) $intCompetitorPropertyId . '
					LIMIT 1 ';

    	return parent::fetchMktgPropertyCompetitor( $strSql, $objDatabase );
    }

	public static function fetchMaxPriorityByRemotePropertyKeyByRemoteCompanyKey( $strRemotePropertyKey, $intRemoteCompanyKey, $objDatabase ) {

		$strSql = 'SELECT
						MAX( priority ) as priority
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON ( mc.id = mpc.company_id 
						AND mc.remote_primary_key = ' . ( int ) $intRemoteCompanyKey . ' )
						JOIN mktg_properties mp ON ( mp.id = mpc.property_id AND mp.company_id = mc.id AND mp.remote_primary_key = \'' . $strRemotePropertyKey . '\' )
					WHERE
						mpc.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPropertyCompetitorByPropertyIdByCompetitorPropertyId( $intPropertyId, $intCompetitorPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						mpc.*
					FROM
						mktg_property_competitors mpc
						WHERE mpc.property_id = ' . ( int ) $intPropertyId . '
						AND mpc.competitor_property_id=' . ( int ) $intCompetitorPropertyId . '
					LIMIT 1';

		return parent::fetchMktgPropertyCompetitor( $strSql, $objDatabase );
	}

	public static function fetchDisabledMktgPropertyAssociations( $objDatabase ) {

		$strSql = 'SELECT
						mpc.property_id,
						mpc.competitor_property_id,
						mpc.id,
					    mp1.property_name,
					    mp1.is_disabled,
					    mp2.property_name as competitor_property_name,
					    mp2.is_disabled as competitor_property_is_disabled,
					    mp2.source
					FROM
					    mktg_property_competitors mpc
					    JOIN mktg_properties mp1 ON ( mp1.id = mpc.property_id )
					    JOIN mktg_properties mp2 ON ( mp2.id = mpc.competitor_property_id )
					WHERE
					    ( mp1.is_disabled = \'Y\'
					    OR mp2.is_disabled = \'Y\' )
					    AND mpc.deleted_on IS NULL
					ORDER BY mp1.id ASC
    			';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchLatestPropertyCompetitorsCountByIntervalGroupByStatus( $intIsChecked, $objDatabase ) {

		$strWhere = '';
		if( 1 == $intIsChecked ) {
			$strWhere = " AND mc.company_status_type = 'Client' ";
		}

		$strSql = 'SELECT
                        DISTINCT CASE
                            WHEN ( CURRENT_DATE - mpc.created_on::DATE ) / 7 = 0 THEN 0
                            WHEN ( CURRENT_DATE - mpc.created_on::DATE ) / 7 = 1 THEN 1
                            WHEN ( CURRENT_DATE - mpc.created_on::DATE ) / 7 = 2 THEN 2
                            WHEN ( CURRENT_DATE - mpc.created_on::DATE ) / 7 = 3 THEN 3
                            ELSE 4
                            END AS range,
                            COUNT ( mpc.id ) OVER ( PARTITION BY FLOOR ( ( CURRENT_DATE - mpc.created_on::DATE ) / 7 ) ) AS competitor_count
                          FROM
                            mktg_property_competitors mpc
		                    JOIN mktg_properties mp ON mpc.competitor_property_id = mp.id
		                    JOIN mktg_companies mc ON mp.company_id = mc.id
                          WHERE
                              mpc.created_on BETWEEN ( CURRENT_DATE - INTERVAL \'1 month\' )::DATE
                              AND ( CURRENT_DATE + 1 )::DATE
                              AND mpc.deleted_on IS NULL
				 			' . $strWhere . '
                          ORDER BY
                              1 ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRankedActiveMktgPropertyCompetitors( $intCurrentRankedId,$intUpdatedRankedId, $boolGreater = false, $strRemotePropertyKey, $intRemoteCompanyKey, $objDatabase ) {

		if( false == valId( $intCurrentRankedId ) || false == valId( $intUpdatedRankedId ) ) {
			return NULL;
		}

		if( true == $boolGreater ) {
			$strWhere = ' AND mpc.priority >= ' . ( int ) $intUpdatedRankedId . ' AND mpc.priority <= ' . ( int ) $intCurrentRankedId;
		} else {
			$strWhere = ' AND mpc.priority <= ' . ( int ) $intUpdatedRankedId . ' AND mpc.priority >= ' . ( int ) $intCurrentRankedId;
		}
		$strSql = 'SELECT
						mpc.*
					FROM
						mktg_property_competitors mpc
						JOIN mktg_companies mc ON (
							mc.id = mpc.company_id
							AND mc.remote_primary_key = ' . ( int ) $intRemoteCompanyKey . ' )
						JOIN mktg_properties mp ON (
							mp.id = mpc.property_id
							AND mp.company_id = mc.id
							AND mp.remote_primary_key = \'' . $strRemotePropertyKey . '\'
							AND mp.is_disabled = \'N\' )
					WHERE
						mpc.deleted_on IS NULL
						' . $strWhere . '
					ORDER BY
						mpc.priority ASC';

		return parent::fetchMktgPropertyCompetitors( $strSql, $objDatabase );
	}

}
?>