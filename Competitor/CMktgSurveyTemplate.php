<?php

class CMktgSurveyTemplate extends CBaseMktgSurveyTemplate {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyInterval() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCompanyId();
				$boolIsValid &= $this->valTemplateName();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>