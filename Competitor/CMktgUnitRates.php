<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgUnitRates
 * Do not add any new functions to this class.
 */

class CMktgUnitRates extends CBaseMktgUnitRates {

	public static function fetchMktgUnitRates( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgUnitRate::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgUnitRate( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgUnitRate::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>