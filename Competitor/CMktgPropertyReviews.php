<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyReviews
 * Do not add any new functions to this class.
 */

class CMktgPropertyReviews extends CBaseMktgPropertyReviews {

	public static function fetchMktgPropertyReviewsByPropertyIds( $arrintMktgPropertyIds, $objDatabase ) {
    	if( false == valArr( $arrintMktgPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
						*
					FROM
						public.mktg_property_reviews
					WHERE
						property_id IN( ' . implode( ', ', $arrintMktgPropertyIds ) . ' ) ';

    	return self::fetchMktgPropertyReviews( $strSql, $objDatabase );
    }

    public static function fetchMktgPropertyReviewsByMktgPropetyIdByScraperType( $intMktgPropertyId, $strScraperType, $objDatabase ) {

    	$strSql = 'SELECT
						*
					FROM
						public.mktg_property_reviews
					WHERE
						property_id = ' . ( int ) $intMktgPropertyId . '
						AND source = \'' . $strScraperType . '\'';

    	return self::fetchMktgPropertyReviews( $strSql, $objDatabase );
    }
}
?>