<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgUsers
 * Do not add any new functions to this class.
 */

class CMktgUsers extends CBaseMktgUsers {

	public static function fetchMktgUsers( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgUser::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgUser( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgUser::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>