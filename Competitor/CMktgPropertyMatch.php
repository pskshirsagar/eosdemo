<?php

class CMktgPropertyMatch extends CBaseMktgPropertyMatch {

	public function valExternalPrimaryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMatchId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMatchType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfidenceRating() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSimilarityRating() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateMatched() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>