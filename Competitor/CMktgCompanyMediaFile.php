<?php

class CMktgCompanyMediaFile extends CBaseMktgCompanyMediaFile {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyMediaFolder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaFileType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsHidden() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaCaption() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMediaAlt() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFullsizeUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valThumbnailUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExternalUri() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileSize() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>