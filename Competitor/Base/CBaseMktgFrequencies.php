<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFrequencies
 * Do not add any new functions to this class.
 */

class CBaseMktgFrequencies extends CEosPluralBase {

	/**
	 * @return CMktgFrequency[]
	 */
	public static function fetchMktgFrequencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgFrequency::class, $objDatabase );
	}

	/**
	 * @return CMktgFrequency
	 */
	public static function fetchMktgFrequency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgFrequency::class, $objDatabase );
	}

	public static function fetchMktgFrequencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_frequencies', $objDatabase );
	}

	public static function fetchMktgFrequencyById( $intId, $objDatabase ) {
		return self::fetchMktgFrequency( sprintf( 'SELECT * FROM mktg_frequencies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>