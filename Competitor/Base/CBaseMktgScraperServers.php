<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgScraperServers
 * Do not add any new functions to this class.
 */

class CBaseMktgScraperServers extends CEosPluralBase {

	/**
	 * @return CMktgScraperServer[]
	 */
	public static function fetchMktgScraperServers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgScraperServer', $objDatabase );
	}

	/**
	 * @return CMktgScraperServer
	 */
	public static function fetchMktgScraperServer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgScraperServer', $objDatabase );
	}

	public static function fetchMktgScraperServerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_scraper_servers', $objDatabase );
	}

	public static function fetchMktgScraperServerById( $intId, $objDatabase ) {
		return self::fetchMktgScraperServer( sprintf( 'SELECT * FROM mktg_scraper_servers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>