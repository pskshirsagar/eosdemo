<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUrls
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyUrls extends CEosPluralBase {

	/**
	 * @return CMktgPropertyUrl[]
	 */
	public static function fetchMktgPropertyUrls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyUrl', $objDatabase );
	}

	/**
	 * @return CMktgPropertyUrl
	 */
	public static function fetchMktgPropertyUrl( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyUrl', $objDatabase );
	}

	public static function fetchMktgPropertyUrlCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_urls', $objDatabase );
	}

	public static function fetchMktgPropertyUrlById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyUrl( sprintf( 'SELECT * FROM mktg_property_urls WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyUrlsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyUrls( sprintf( 'SELECT * FROM mktg_property_urls WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>