<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUnitTypes
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyUnitTypes extends CEosPluralBase {

	/**
	 * @return CMktgPropertyUnitType[]
	 */
	public static function fetchMktgPropertyUnitTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyUnitType', $objDatabase );
	}

	/**
	 * @return CMktgPropertyUnitType
	 */
	public static function fetchMktgPropertyUnitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyUnitType', $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_unit_types', $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypeById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyUnitType( sprintf( 'SELECT * FROM mktg_property_unit_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyUnitTypes( sprintf( 'SELECT * FROM mktg_property_unit_types WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyUnitTypes( sprintf( 'SELECT * FROM mktg_property_unit_types WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>