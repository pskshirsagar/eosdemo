<?php

class CBaseMktgSurveyRentConcession extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_survey_rent_concessions';

	protected $m_intId;
	protected $m_intSurveyId;
	protected $m_intCompetitorPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_strFloorplanName;
	protected $m_intBeds;
	protected $m_intBaths;
	protected $m_fltSquareFeet;
	protected $m_fltAdvertisedRent;
	protected $m_fltWebsiteRent;
	protected $m_intConcessionTypeId;
	protected $m_strConcessionExpiresOn;
	protected $m_strConcessionDescription;
	protected $m_strConcessionAmountType;
	protected $m_fltConcessionAmount;
	protected $m_strConcessionGiftCardCompany;
	protected $m_arrintAssociatedUnitTypeIds;
	protected $m_arrstrAssociatedUnitTypeNames;
	protected $m_fltNonRefundableDeposit;
	protected $m_fltRefundableDeposit;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyId', trim( $arrValues['survey_id'] ) ); elseif( isset( $arrValues['survey_id'] ) ) $this->setSurveyId( $arrValues['survey_id'] );
		if( isset( $arrValues['competitor_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyId', trim( $arrValues['competitor_property_id'] ) ); elseif( isset( $arrValues['competitor_property_id'] ) ) $this->setCompetitorPropertyId( $arrValues['competitor_property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['floorplan_name'] ) && $boolDirectSet ) $this->set( 'm_strFloorplanName', trim( $arrValues['floorplan_name'] ) ); elseif( isset( $arrValues['floorplan_name'] ) ) $this->setFloorplanName( $arrValues['floorplan_name'] );
		if( isset( $arrValues['beds'] ) && $boolDirectSet ) $this->set( 'm_intBeds', trim( $arrValues['beds'] ) ); elseif( isset( $arrValues['beds'] ) ) $this->setBeds( $arrValues['beds'] );
		if( isset( $arrValues['baths'] ) && $boolDirectSet ) $this->set( 'm_intBaths', trim( $arrValues['baths'] ) ); elseif( isset( $arrValues['baths'] ) ) $this->setBaths( $arrValues['baths'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['advertised_rent'] ) && $boolDirectSet ) $this->set( 'm_fltAdvertisedRent', trim( $arrValues['advertised_rent'] ) ); elseif( isset( $arrValues['advertised_rent'] ) ) $this->setAdvertisedRent( $arrValues['advertised_rent'] );
		if( isset( $arrValues['website_rent'] ) && $boolDirectSet ) $this->set( 'm_fltWebsiteRent', trim( $arrValues['website_rent'] ) ); elseif( isset( $arrValues['website_rent'] ) ) $this->setWebsiteRent( $arrValues['website_rent'] );
		if( isset( $arrValues['concession_type_id'] ) && $boolDirectSet ) $this->set( 'm_intConcessionTypeId', trim( $arrValues['concession_type_id'] ) ); elseif( isset( $arrValues['concession_type_id'] ) ) $this->setConcessionTypeId( $arrValues['concession_type_id'] );
		if( isset( $arrValues['concession_expires_on'] ) && $boolDirectSet ) $this->set( 'm_strConcessionExpiresOn', trim( $arrValues['concession_expires_on'] ) ); elseif( isset( $arrValues['concession_expires_on'] ) ) $this->setConcessionExpiresOn( $arrValues['concession_expires_on'] );
		if( isset( $arrValues['concession_description'] ) && $boolDirectSet ) $this->set( 'm_strConcessionDescription', trim( $arrValues['concession_description'] ) ); elseif( isset( $arrValues['concession_description'] ) ) $this->setConcessionDescription( $arrValues['concession_description'] );
		if( isset( $arrValues['concession_amount_type'] ) && $boolDirectSet ) $this->set( 'm_strConcessionAmountType', trim( stripcslashes( $arrValues['concession_amount_type'] ) ) ); elseif( isset( $arrValues['concession_amount_type'] ) ) $this->setConcessionAmountType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['concession_amount_type'] ) : $arrValues['concession_amount_type'] );
		if( isset( $arrValues['concession_amount'] ) && $boolDirectSet ) $this->set( 'm_fltConcessionAmount', trim( $arrValues['concession_amount'] ) ); elseif( isset( $arrValues['concession_amount'] ) ) $this->setConcessionAmount( $arrValues['concession_amount'] );
		if( isset( $arrValues['concession_gift_card_company'] ) && $boolDirectSet ) $this->set( 'm_strConcessionGiftCardCompany', trim( $arrValues['concession_gift_card_company'] ) ); elseif( isset( $arrValues['concession_gift_card_company'] ) ) $this->setConcessionGiftCardCompany( $arrValues['concession_gift_card_company'] );
		if( isset( $arrValues['associated_unit_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintAssociatedUnitTypeIds', trim( $arrValues['associated_unit_type_ids'] ) ); elseif( isset( $arrValues['associated_unit_type_ids'] ) ) $this->setAssociatedUnitTypeIds( $arrValues['associated_unit_type_ids'] );
		if( isset( $arrValues['associated_unit_type_names'] ) && $boolDirectSet ) $this->set( 'm_arrstrAssociatedUnitTypeNames', trim( $arrValues['associated_unit_type_names'] ) ); elseif( isset( $arrValues['associated_unit_type_names'] ) ) $this->setAssociatedUnitTypeNames( $arrValues['associated_unit_type_names'] );
		if( isset( $arrValues['non_refundable_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltNonRefundableDeposit', trim( $arrValues['non_refundable_deposit'] ) ); elseif( isset( $arrValues['non_refundable_deposit'] ) ) $this->setNonRefundableDeposit( $arrValues['non_refundable_deposit'] );
		if( isset( $arrValues['refundable_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltRefundableDeposit', trim( $arrValues['refundable_deposit'] ) ); elseif( isset( $arrValues['refundable_deposit'] ) ) $this->setRefundableDeposit( $arrValues['refundable_deposit'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyId( $intSurveyId ) {
		$this->set( 'm_intSurveyId', CStrings::strToIntDef( $intSurveyId, NULL, false ) );
	}

	public function getSurveyId() {
		return $this->m_intSurveyId;
	}

	public function sqlSurveyId() {
		return ( true == isset( $this->m_intSurveyId ) ) ? ( string ) $this->m_intSurveyId : 'NULL';
	}

	public function setCompetitorPropertyId( $intCompetitorPropertyId ) {
		$this->set( 'm_intCompetitorPropertyId', CStrings::strToIntDef( $intCompetitorPropertyId, NULL, false ) );
	}

	public function getCompetitorPropertyId() {
		return $this->m_intCompetitorPropertyId;
	}

	public function sqlCompetitorPropertyId() {
		return ( true == isset( $this->m_intCompetitorPropertyId ) ) ? ( string ) $this->m_intCompetitorPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setFloorplanName( $strFloorplanName ) {
		$this->set( 'm_strFloorplanName', CStrings::strTrimDef( $strFloorplanName, -1, NULL, true ) );
	}

	public function getFloorplanName() {
		return $this->m_strFloorplanName;
	}

	public function sqlFloorplanName() {
		return ( true == isset( $this->m_strFloorplanName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFloorplanName ) : '\'' . addslashes( $this->m_strFloorplanName ) . '\'' ) : 'NULL';
	}

	public function setBeds( $intBeds ) {
		$this->set( 'm_intBeds', CStrings::strToIntDef( $intBeds, NULL, false ) );
	}

	public function getBeds() {
		return $this->m_intBeds;
	}

	public function sqlBeds() {
		return ( true == isset( $this->m_intBeds ) ) ? ( string ) $this->m_intBeds : 'NULL';
	}

	public function setBaths( $intBaths ) {
		$this->set( 'm_intBaths', CStrings::strToIntDef( $intBaths, NULL, false ) );
	}

	public function getBaths() {
		return $this->m_intBaths;
	}

	public function sqlBaths() {
		return ( true == isset( $this->m_intBaths ) ) ? ( string ) $this->m_intBaths : 'NULL';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setAdvertisedRent( $fltAdvertisedRent ) {
		$this->set( 'm_fltAdvertisedRent', CStrings::strToFloatDef( $fltAdvertisedRent, NULL, false, 4 ) );
	}

	public function getAdvertisedRent() {
		return $this->m_fltAdvertisedRent;
	}

	public function sqlAdvertisedRent() {
		return ( true == isset( $this->m_fltAdvertisedRent ) ) ? ( string ) $this->m_fltAdvertisedRent : 'NULL';
	}

	public function setWebsiteRent( $fltWebsiteRent ) {
		$this->set( 'm_fltWebsiteRent', CStrings::strToFloatDef( $fltWebsiteRent, NULL, false, 4 ) );
	}

	public function getWebsiteRent() {
		return $this->m_fltWebsiteRent;
	}

	public function sqlWebsiteRent() {
		return ( true == isset( $this->m_fltWebsiteRent ) ) ? ( string ) $this->m_fltWebsiteRent : 'NULL';
	}

	public function setConcessionTypeId( $intConcessionTypeId ) {
		$this->set( 'm_intConcessionTypeId', CStrings::strToIntDef( $intConcessionTypeId, NULL, false ) );
	}

	public function getConcessionTypeId() {
		return $this->m_intConcessionTypeId;
	}

	public function sqlConcessionTypeId() {
		return ( true == isset( $this->m_intConcessionTypeId ) ) ? ( string ) $this->m_intConcessionTypeId : 'NULL';
	}

	public function setConcessionExpiresOn( $strConcessionExpiresOn ) {
		$this->set( 'm_strConcessionExpiresOn', CStrings::strTrimDef( $strConcessionExpiresOn, -1, NULL, true ) );
	}

	public function getConcessionExpiresOn() {
		return $this->m_strConcessionExpiresOn;
	}

	public function sqlConcessionExpiresOn() {
		return ( true == isset( $this->m_strConcessionExpiresOn ) ) ? '\'' . $this->m_strConcessionExpiresOn . '\'' : 'NULL';
	}

	public function setConcessionDescription( $strConcessionDescription ) {
		$this->set( 'm_strConcessionDescription', CStrings::strTrimDef( $strConcessionDescription, -1, NULL, true ) );
	}

	public function getConcessionDescription() {
		return $this->m_strConcessionDescription;
	}

	public function sqlConcessionDescription() {
		return ( true == isset( $this->m_strConcessionDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConcessionDescription ) : '\'' . addslashes( $this->m_strConcessionDescription ) . '\'' ) : 'NULL';
	}

	public function setConcessionAmountType( $strConcessionAmountType ) {
		$this->set( 'm_strConcessionAmountType', CStrings::strTrimDef( $strConcessionAmountType, -1, NULL, true ) );
	}

	public function getConcessionAmountType() {
		return $this->m_strConcessionAmountType;
	}

	public function sqlConcessionAmountType() {
		return ( true == isset( $this->m_strConcessionAmountType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConcessionAmountType ) : '\'' . addslashes( $this->m_strConcessionAmountType ) . '\'' ) : 'NULL';
	}

	public function setConcessionAmount( $fltConcessionAmount ) {
		$this->set( 'm_fltConcessionAmount', CStrings::strToFloatDef( $fltConcessionAmount, NULL, false, 4 ) );
	}

	public function getConcessionAmount() {
		return $this->m_fltConcessionAmount;
	}

	public function sqlConcessionAmount() {
		return ( true == isset( $this->m_fltConcessionAmount ) ) ? ( string ) $this->m_fltConcessionAmount : 'NULL';
	}

	public function setConcessionGiftCardCompany( $strConcessionGiftCardCompany ) {
		$this->set( 'm_strConcessionGiftCardCompany', CStrings::strTrimDef( $strConcessionGiftCardCompany, -1, NULL, true ) );
	}

	public function getConcessionGiftCardCompany() {
		return $this->m_strConcessionGiftCardCompany;
	}

	public function sqlConcessionGiftCardCompany() {
		return ( true == isset( $this->m_strConcessionGiftCardCompany ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConcessionGiftCardCompany ) : '\'' . addslashes( $this->m_strConcessionGiftCardCompany ) . '\'' ) : 'NULL';
	}

	public function setAssociatedUnitTypeIds( $arrintAssociatedUnitTypeIds ) {
		$this->set( 'm_arrintAssociatedUnitTypeIds', CStrings::strToArrIntDef( $arrintAssociatedUnitTypeIds, NULL ) );
	}

	public function getAssociatedUnitTypeIds() {
		return $this->m_arrintAssociatedUnitTypeIds;
	}

	public function sqlAssociatedUnitTypeIds() {
		return ( true == isset( $this->m_arrintAssociatedUnitTypeIds ) && true == valArr( $this->m_arrintAssociatedUnitTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintAssociatedUnitTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setAssociatedUnitTypeNames( $arrstrAssociatedUnitTypeNames ) {
		$this->set( 'm_arrstrAssociatedUnitTypeNames', CStrings::strToArrIntDef( $arrstrAssociatedUnitTypeNames, NULL ) );
	}

	public function getAssociatedUnitTypeNames() {
		return $this->m_arrstrAssociatedUnitTypeNames;
	}

	public function sqlAssociatedUnitTypeNames() {
		return ( true == isset( $this->m_arrstrAssociatedUnitTypeNames ) && true == valArr( $this->m_arrstrAssociatedUnitTypeNames ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrAssociatedUnitTypeNames, NULL ) . '\'' : 'NULL';
	}

	public function setNonRefundableDeposit( $fltNonRefundableDeposit ) {
		$this->set( 'm_fltNonRefundableDeposit', CStrings::strToFloatDef( $fltNonRefundableDeposit, NULL, false, 4 ) );
	}

	public function getNonRefundableDeposit() {
		return $this->m_fltNonRefundableDeposit;
	}

	public function sqlNonRefundableDeposit() {
		return ( true == isset( $this->m_fltNonRefundableDeposit ) ) ? ( string ) $this->m_fltNonRefundableDeposit : 'NULL';
	}

	public function setRefundableDeposit( $fltRefundableDeposit ) {
		$this->set( 'm_fltRefundableDeposit', CStrings::strToFloatDef( $fltRefundableDeposit, NULL, false, 4 ) );
	}

	public function getRefundableDeposit() {
		return $this->m_fltRefundableDeposit;
	}

	public function sqlRefundableDeposit() {
		return ( true == isset( $this->m_fltRefundableDeposit ) ) ? ( string ) $this->m_fltRefundableDeposit : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_id, competitor_property_id, property_floorplan_id, floorplan_name, beds, baths, square_feet, advertised_rent, website_rent, concession_type_id, concession_expires_on, concession_description, concession_amount_type, concession_amount, concession_gift_card_company, associated_unit_type_ids, associated_unit_type_names, non_refundable_deposit, refundable_deposit, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlSurveyId() . ', ' .
		          $this->sqlCompetitorPropertyId() . ', ' .
		          $this->sqlPropertyFloorplanId() . ', ' .
		          $this->sqlFloorplanName() . ', ' .
		          $this->sqlBeds() . ', ' .
		          $this->sqlBaths() . ', ' .
		          $this->sqlSquareFeet() . ', ' .
		          $this->sqlAdvertisedRent() . ', ' .
		          $this->sqlWebsiteRent() . ', ' .
		          $this->sqlConcessionTypeId() . ', ' .
		          $this->sqlConcessionExpiresOn() . ', ' .
		          $this->sqlConcessionDescription() . ', ' .
		          $this->sqlConcessionAmountType() . ', ' .
		          $this->sqlConcessionAmount() . ', ' .
		          $this->sqlConcessionGiftCardCompany() . ', ' .
		          $this->sqlAssociatedUnitTypeIds() . ', ' .
		          $this->sqlAssociatedUnitTypeNames() . ', ' .
		          $this->sqlNonRefundableDeposit() . ', ' .
		          $this->sqlRefundableDeposit() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId(). ',' ; } elseif( true == array_key_exists( 'SurveyId', $this->getChangedColumns() ) ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId(). ',' ; } elseif( true == array_key_exists( 'CompetitorPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId(). ',' ; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' floorplan_name = ' . $this->sqlFloorplanName(). ',' ; } elseif( true == array_key_exists( 'FloorplanName', $this->getChangedColumns() ) ) { $strSql .= ' floorplan_name = ' . $this->sqlFloorplanName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' beds = ' . $this->sqlBeds(). ',' ; } elseif( true == array_key_exists( 'Beds', $this->getChangedColumns() ) ) { $strSql .= ' beds = ' . $this->sqlBeds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' baths = ' . $this->sqlBaths(). ',' ; } elseif( true == array_key_exists( 'Baths', $this->getChangedColumns() ) ) { $strSql .= ' baths = ' . $this->sqlBaths() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet(). ',' ; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advertised_rent = ' . $this->sqlAdvertisedRent(). ',' ; } elseif( true == array_key_exists( 'AdvertisedRent', $this->getChangedColumns() ) ) { $strSql .= ' advertised_rent = ' . $this->sqlAdvertisedRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_rent = ' . $this->sqlWebsiteRent(). ',' ; } elseif( true == array_key_exists( 'WebsiteRent', $this->getChangedColumns() ) ) { $strSql .= ' website_rent = ' . $this->sqlWebsiteRent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_type_id = ' . $this->sqlConcessionTypeId(). ',' ; } elseif( true == array_key_exists( 'ConcessionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' concession_type_id = ' . $this->sqlConcessionTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_expires_on = ' . $this->sqlConcessionExpiresOn(). ',' ; } elseif( true == array_key_exists( 'ConcessionExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' concession_expires_on = ' . $this->sqlConcessionExpiresOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_description = ' . $this->sqlConcessionDescription(). ',' ; } elseif( true == array_key_exists( 'ConcessionDescription', $this->getChangedColumns() ) ) { $strSql .= ' concession_description = ' . $this->sqlConcessionDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_amount_type = ' . $this->sqlConcessionAmountType(). ',' ; } elseif( true == array_key_exists( 'ConcessionAmountType', $this->getChangedColumns() ) ) { $strSql .= ' concession_amount_type = ' . $this->sqlConcessionAmountType() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_amount = ' . $this->sqlConcessionAmount(). ',' ; } elseif( true == array_key_exists( 'ConcessionAmount', $this->getChangedColumns() ) ) { $strSql .= ' concession_amount = ' . $this->sqlConcessionAmount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_gift_card_company = ' . $this->sqlConcessionGiftCardCompany(). ',' ; } elseif( true == array_key_exists( 'ConcessionGiftCardCompany', $this->getChangedColumns() ) ) { $strSql .= ' concession_gift_card_company = ' . $this->sqlConcessionGiftCardCompany() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_unit_type_ids = ' . $this->sqlAssociatedUnitTypeIds(). ',' ; } elseif( true == array_key_exists( 'AssociatedUnitTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' associated_unit_type_ids = ' . $this->sqlAssociatedUnitTypeIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_unit_type_names = ' . $this->sqlAssociatedUnitTypeNames(). ',' ; } elseif( true == array_key_exists( 'AssociatedUnitTypeNames', $this->getChangedColumns() ) ) { $strSql .= ' associated_unit_type_names = ' . $this->sqlAssociatedUnitTypeNames() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_refundable_deposit = ' . $this->sqlNonRefundableDeposit(). ',' ; } elseif( true == array_key_exists( 'NonRefundableDeposit', $this->getChangedColumns() ) ) { $strSql .= ' non_refundable_deposit = ' . $this->sqlNonRefundableDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refundable_deposit = ' . $this->sqlRefundableDeposit(). ',' ; } elseif( true == array_key_exists( 'RefundableDeposit', $this->getChangedColumns() ) ) { $strSql .= ' refundable_deposit = ' . $this->sqlRefundableDeposit() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_id' => $this->getSurveyId(),
			'competitor_property_id' => $this->getCompetitorPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'floorplan_name' => $this->getFloorplanName(),
			'beds' => $this->getBeds(),
			'baths' => $this->getBaths(),
			'square_feet' => $this->getSquareFeet(),
			'advertised_rent' => $this->getAdvertisedRent(),
			'website_rent' => $this->getWebsiteRent(),
			'concession_type_id' => $this->getConcessionTypeId(),
			'concession_expires_on' => $this->getConcessionExpiresOn(),
			'concession_description' => $this->getConcessionDescription(),
			'concession_amount_type' => $this->getConcessionAmountType(),
			'concession_amount' => $this->getConcessionAmount(),
			'concession_gift_card_company' => $this->getConcessionGiftCardCompany(),
			'associated_unit_type_ids' => $this->getAssociatedUnitTypeIds(),
			'associated_unit_type_names' => $this->getAssociatedUnitTypeNames(),
			'non_refundable_deposit' => $this->getNonRefundableDeposit(),
			'refundable_deposit' => $this->getRefundableDeposit(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>