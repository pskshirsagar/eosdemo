<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CRentjungleProperties
 * Do not add any new functions to this class.
 */

class CBaseRentjungleProperties extends CEosPluralBase {

	/**
	 * @return CRentjungleProperty[]
	 */
	public static function fetchRentjungleProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRentjungleProperty', $objDatabase );
	}

	/**
	 * @return CRentjungleProperty
	 */
	public static function fetchRentjungleProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRentjungleProperty', $objDatabase );
	}

	public static function fetchRentjunglePropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rentjungle_properties', $objDatabase );
	}

	public static function fetchRentjunglePropertyById( $intId, $objDatabase ) {
		return self::fetchRentjungleProperty( sprintf( 'SELECT * FROM rentjungle_properties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRentjunglePropertiesByRjId( $strRjId, $objDatabase ) {
		return self::fetchRentjungleProperties( sprintf( 'SELECT * FROM rentjungle_properties WHERE rj_id = \'%s\'', $strRjId ), $objDatabase );
	}

}
?>