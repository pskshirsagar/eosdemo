<?php

class CBaseCachedNewUnitTypePeriod extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.cached_new_unit_type_periods';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strGroupHash;
	protected $m_strEffectiveDate;
	protected $m_strLastExtractRange;
	protected $m_arrintApplicationIds;
	protected $m_intTotalApplicationCount;
	protected $m_intBedroomCount;
	protected $m_intUnitSpaceCount;
	protected $m_intTotalUnitSpaceCount;
	protected $m_intRent;
	protected $m_intBaseRent;
	protected $m_intAmenityRent;
	protected $m_intSpecialRent;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalApplicationCount = '0';
		$this->m_intBedroomCount = '0';
		$this->m_intUnitSpaceCount = '0';
		$this->m_intTotalUnitSpaceCount = '0';
		$this->m_intRent = '0';
		$this->m_intBaseRent = '0';
		$this->m_intAmenityRent = '0';
		$this->m_intSpecialRent = '0';
		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['group_hash'] ) && $boolDirectSet ) $this->set( 'm_strGroupHash', trim( $arrValues['group_hash'] ) ); elseif( isset( $arrValues['group_hash'] ) ) $this->setGroupHash( $arrValues['group_hash'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['last_extract_range'] ) && $boolDirectSet ) $this->set( 'm_strLastExtractRange', trim( $arrValues['last_extract_range'] ) ); elseif( isset( $arrValues['last_extract_range'] ) ) $this->setLastExtractRange( $arrValues['last_extract_range'] );
		if( isset( $arrValues['application_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintApplicationIds', trim( $arrValues['application_ids'] ) ); elseif( isset( $arrValues['application_ids'] ) ) $this->setApplicationIds( $arrValues['application_ids'] );
		if( isset( $arrValues['total_application_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalApplicationCount', trim( $arrValues['total_application_count'] ) ); elseif( isset( $arrValues['total_application_count'] ) ) $this->setTotalApplicationCount( $arrValues['total_application_count'] );
		if( isset( $arrValues['bedroom_count'] ) && $boolDirectSet ) $this->set( 'm_intBedroomCount', trim( $arrValues['bedroom_count'] ) ); elseif( isset( $arrValues['bedroom_count'] ) ) $this->setBedroomCount( $arrValues['bedroom_count'] );
		if( isset( $arrValues['unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceCount', trim( $arrValues['unit_space_count'] ) ); elseif( isset( $arrValues['unit_space_count'] ) ) $this->setUnitSpaceCount( $arrValues['unit_space_count'] );
		if( isset( $arrValues['total_unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitSpaceCount', trim( $arrValues['total_unit_space_count'] ) ); elseif( isset( $arrValues['total_unit_space_count'] ) ) $this->setTotalUnitSpaceCount( $arrValues['total_unit_space_count'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_intRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_intBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_intAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_intSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setGroupHash( $strGroupHash ) {
		$this->set( 'm_strGroupHash', CStrings::strTrimDef( $strGroupHash, -1, NULL, true ) );
	}

	public function getGroupHash() {
		return $this->m_strGroupHash;
	}

	public function sqlGroupHash() {
		return ( true == isset( $this->m_strGroupHash ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGroupHash ) : '\'' . addslashes( $this->m_strGroupHash ) . '\'' ) : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setLastExtractRange( $strLastExtractRange ) {
		$this->set( 'm_strLastExtractRange', CStrings::strTrimDef( $strLastExtractRange, NULL, NULL, true ) );
	}

	public function getLastExtractRange() {
		return $this->m_strLastExtractRange;
	}

	public function sqlLastExtractRange() {
		return ( true == isset( $this->m_strLastExtractRange ) ) ? '\'' . addslashes( $this->m_strLastExtractRange ) . '\'' : 'NULL';
	}

	public function setApplicationIds( $arrintApplicationIds ) {
		$this->set( 'm_arrintApplicationIds', CStrings::strToArrIntDef( $arrintApplicationIds, NULL ) );
	}

	public function getApplicationIds() {
		return $this->m_arrintApplicationIds;
	}

	public function sqlApplicationIds() {
		return ( true == isset( $this->m_arrintApplicationIds ) && true == valArr( $this->m_arrintApplicationIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintApplicationIds, NULL ) . '\'' : 'NULL';
	}

	public function setTotalApplicationCount( $intTotalApplicationCount ) {
		$this->set( 'm_intTotalApplicationCount', CStrings::strToIntDef( $intTotalApplicationCount, NULL, false ) );
	}

	public function getTotalApplicationCount() {
		return $this->m_intTotalApplicationCount;
	}

	public function sqlTotalApplicationCount() {
		return ( true == isset( $this->m_intTotalApplicationCount ) ) ? ( string ) $this->m_intTotalApplicationCount : '0';
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->set( 'm_intBedroomCount', CStrings::strToIntDef( $intBedroomCount, NULL, false ) );
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function sqlBedroomCount() {
		return ( true == isset( $this->m_intBedroomCount ) ) ? ( string ) $this->m_intBedroomCount : '0';
	}

	public function setUnitSpaceCount( $intUnitSpaceCount ) {
		$this->set( 'm_intUnitSpaceCount', CStrings::strToIntDef( $intUnitSpaceCount, NULL, false ) );
	}

	public function getUnitSpaceCount() {
		return $this->m_intUnitSpaceCount;
	}

	public function sqlUnitSpaceCount() {
		return ( true == isset( $this->m_intUnitSpaceCount ) ) ? ( string ) $this->m_intUnitSpaceCount : '0';
	}

	public function setTotalUnitSpaceCount( $intTotalUnitSpaceCount ) {
		$this->set( 'm_intTotalUnitSpaceCount', CStrings::strToIntDef( $intTotalUnitSpaceCount, NULL, false ) );
	}

	public function getTotalUnitSpaceCount() {
		return $this->m_intTotalUnitSpaceCount;
	}

	public function sqlTotalUnitSpaceCount() {
		return ( true == isset( $this->m_intTotalUnitSpaceCount ) ) ? ( string ) $this->m_intTotalUnitSpaceCount : '0';
	}

	public function setRent( $intRent ) {
		$this->set( 'm_intRent', CStrings::strToIntDef( $intRent, NULL, false ) );
	}

	public function getRent() {
		return $this->m_intRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_intRent ) ) ? ( string ) $this->m_intRent : '0';
	}

	public function setBaseRent( $intBaseRent ) {
		$this->set( 'm_intBaseRent', CStrings::strToIntDef( $intBaseRent, NULL, false ) );
	}

	public function getBaseRent() {
		return $this->m_intBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_intBaseRent ) ) ? ( string ) $this->m_intBaseRent : '0';
	}

	public function setAmenityRent( $intAmenityRent ) {
		$this->set( 'm_intAmenityRent', CStrings::strToIntDef( $intAmenityRent, NULL, false ) );
	}

	public function getAmenityRent() {
		return $this->m_intAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_intAmenityRent ) ) ? ( string ) $this->m_intAmenityRent : '0';
	}

	public function setSpecialRent( $intSpecialRent ) {
		$this->set( 'm_intSpecialRent', CStrings::strToIntDef( $intSpecialRent, NULL, false ) );
	}

	public function getSpecialRent() {
		return $this->m_intSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_intSpecialRent ) ) ? ( string ) $this->m_intSpecialRent : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_id, lease_term_id, lease_start_window_id, lease_start_date, lease_end_date, group_hash, effective_date, last_extract_range, application_ids, total_application_count, bedroom_count, unit_space_count, total_unit_space_count, rent, base_rent, amenity_rent, special_rent, details, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlGroupHash() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlLastExtractRange() . ', ' .
						$this->sqlApplicationIds() . ', ' .
						$this->sqlTotalApplicationCount() . ', ' .
						$this->sqlBedroomCount() . ', ' .
						$this->sqlUnitSpaceCount() . ', ' .
						$this->sqlTotalUnitSpaceCount() . ', ' .
						$this->sqlRent() . ', ' .
						$this->sqlBaseRent() . ', ' .
						$this->sqlAmenityRent() . ', ' .
						$this->sqlSpecialRent() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_hash = ' . $this->sqlGroupHash(). ',' ; } elseif( true == array_key_exists( 'GroupHash', $this->getChangedColumns() ) ) { $strSql .= ' group_hash = ' . $this->sqlGroupHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_extract_range = ' . $this->sqlLastExtractRange(). ',' ; } elseif( true == array_key_exists( 'LastExtractRange', $this->getChangedColumns() ) ) { $strSql .= ' last_extract_range = ' . $this->sqlLastExtractRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_ids = ' . $this->sqlApplicationIds(). ',' ; } elseif( true == array_key_exists( 'ApplicationIds', $this->getChangedColumns() ) ) { $strSql .= ' application_ids = ' . $this->sqlApplicationIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_application_count = ' . $this->sqlTotalApplicationCount(). ',' ; } elseif( true == array_key_exists( 'TotalApplicationCount', $this->getChangedColumns() ) ) { $strSql .= ' total_application_count = ' . $this->sqlTotalApplicationCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount(). ',' ; } elseif( true == array_key_exists( 'BedroomCount', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_unit_space_count = ' . $this->sqlTotalUnitSpaceCount(). ',' ; } elseif( true == array_key_exists( 'TotalUnitSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' total_unit_space_count = ' . $this->sqlTotalUnitSpaceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent = ' . $this->sqlRent(). ',' ; } elseif( true == array_key_exists( 'Rent', $this->getChangedColumns() ) ) { $strSql .= ' rent = ' . $this->sqlRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent(). ',' ; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'group_hash' => $this->getGroupHash(),
			'effective_date' => $this->getEffectiveDate(),
			'last_extract_range' => $this->getLastExtractRange(),
			'application_ids' => $this->getApplicationIds(),
			'total_application_count' => $this->getTotalApplicationCount(),
			'bedroom_count' => $this->getBedroomCount(),
			'unit_space_count' => $this->getUnitSpaceCount(),
			'total_unit_space_count' => $this->getTotalUnitSpaceCount(),
			'rent' => $this->getRent(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>
