<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyLeaseTerms
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyLeaseTerms extends CEosPluralBase {

	/**
	 * @return CMktgPropertyLeaseTerm[]
	 */
	public static function fetchMktgPropertyLeaseTerms( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyLeaseTerm', $objDatabase );
	}

	/**
	 * @return CMktgPropertyLeaseTerm
	 */
	public static function fetchMktgPropertyLeaseTerm( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyLeaseTerm', $objDatabase );
	}

	public static function fetchMktgPropertyLeaseTermCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_lease_terms', $objDatabase );
	}

	public static function fetchMktgPropertyLeaseTermById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyLeaseTerm( sprintf( 'SELECT * FROM mktg_property_lease_terms WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyLeaseTermsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyLeaseTerms( sprintf( 'SELECT * FROM mktg_property_lease_terms WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyLeaseTermsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyLeaseTerms( sprintf( 'SELECT * FROM mktg_property_lease_terms WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>