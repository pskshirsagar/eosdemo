<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyCompetitors
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyCompetitors extends CEosPluralBase {

	/**
	 * @return CMktgPropertyCompetitor[]
	 */
	public static function fetchMktgPropertyCompetitors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyCompetitor', $objDatabase );
	}

	/**
	 * @return CMktgPropertyCompetitor
	 */
	public static function fetchMktgPropertyCompetitor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyCompetitor', $objDatabase );
	}

	public static function fetchMktgPropertyCompetitorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_competitors', $objDatabase );
	}

	public static function fetchMktgPropertyCompetitorById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyCompetitor( sprintf( 'SELECT * FROM mktg_property_competitors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyCompetitorsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyCompetitors( sprintf( 'SELECT * FROM mktg_property_competitors WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyCompetitorsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyCompetitors( sprintf( 'SELECT * FROM mktg_property_competitors WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyCompetitorsByCompetitorPropertyId( $intCompetitorPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyCompetitors( sprintf( 'SELECT * FROM mktg_property_competitors WHERE competitor_property_id = %d', ( int ) $intCompetitorPropertyId ), $objDatabase );
	}

}
?>