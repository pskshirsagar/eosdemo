<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyPhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CMktgPropertyPhoneNumber[]
	 */
	public static function fetchMktgPropertyPhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyPhoneNumber', $objDatabase );
	}

	/**
	 * @return CMktgPropertyPhoneNumber
	 */
	public static function fetchMktgPropertyPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyPhoneNumber', $objDatabase );
	}

	public static function fetchMktgPropertyPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_phone_numbers', $objDatabase );
	}

	public static function fetchMktgPropertyPhoneNumberById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyPhoneNumber( sprintf( 'SELECT * FROM mktg_property_phone_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyPhoneNumbersByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyPhoneNumbers( sprintf( 'SELECT * FROM mktg_property_phone_numbers WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyPhoneNumbersByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyPhoneNumbers( sprintf( 'SELECT * FROM mktg_property_phone_numbers WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>