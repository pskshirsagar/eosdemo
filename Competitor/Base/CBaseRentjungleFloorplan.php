<?php

class CBaseRentjungleFloorplan extends CEosSingularBase {

	const TABLE_NAME = 'public.rentjungle_floorplans';

	protected $m_intId;
	protected $m_strRjId;
	protected $m_intRjBeds;
	protected $m_fltRjPrice;
	protected $m_strRjBaths;
	protected $m_fltRjArea;
	protected $m_strRjLastseen;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['rj_id'] ) && $boolDirectSet ) $this->set( 'm_strRjId', trim( stripcslashes( $arrValues['rj_id'] ) ) ); elseif( isset( $arrValues['rj_id'] ) ) $this->setRjId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_id'] ) : $arrValues['rj_id'] );
		if( isset( $arrValues['rj_beds'] ) && $boolDirectSet ) $this->set( 'm_intRjBeds', trim( $arrValues['rj_beds'] ) ); elseif( isset( $arrValues['rj_beds'] ) ) $this->setRjBeds( $arrValues['rj_beds'] );
		if( isset( $arrValues['rj_price'] ) && $boolDirectSet ) $this->set( 'm_fltRjPrice', trim( $arrValues['rj_price'] ) ); elseif( isset( $arrValues['rj_price'] ) ) $this->setRjPrice( $arrValues['rj_price'] );
		if( isset( $arrValues['rj_baths'] ) && $boolDirectSet ) $this->set( 'm_strRjBaths', trim( stripcslashes( $arrValues['rj_baths'] ) ) ); elseif( isset( $arrValues['rj_baths'] ) ) $this->setRjBaths( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_baths'] ) : $arrValues['rj_baths'] );
		if( isset( $arrValues['rj_area'] ) && $boolDirectSet ) $this->set( 'm_fltRjArea', trim( $arrValues['rj_area'] ) ); elseif( isset( $arrValues['rj_area'] ) ) $this->setRjArea( $arrValues['rj_area'] );
		if( isset( $arrValues['rj_lastseen'] ) && $boolDirectSet ) $this->set( 'm_strRjLastseen', trim( stripcslashes( $arrValues['rj_lastseen'] ) ) ); elseif( isset( $arrValues['rj_lastseen'] ) ) $this->setRjLastseen( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_lastseen'] ) : $arrValues['rj_lastseen'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRjId( $strRjId ) {
		$this->set( 'm_strRjId', CStrings::strTrimDef( $strRjId, 32, NULL, true ) );
	}

	public function getRjId() {
		return $this->m_strRjId;
	}

	public function sqlRjId() {
		return ( true == isset( $this->m_strRjId ) ) ? '\'' . addslashes( $this->m_strRjId ) . '\'' : 'NULL';
	}

	public function setRjBeds( $intRjBeds ) {
		$this->set( 'm_intRjBeds', CStrings::strToIntDef( $intRjBeds, NULL, false ) );
	}

	public function getRjBeds() {
		return $this->m_intRjBeds;
	}

	public function sqlRjBeds() {
		return ( true == isset( $this->m_intRjBeds ) ) ? ( string ) $this->m_intRjBeds : 'NULL';
	}

	public function setRjPrice( $fltRjPrice ) {
		$this->set( 'm_fltRjPrice', CStrings::strToFloatDef( $fltRjPrice, NULL, false, 2 ) );
	}

	public function getRjPrice() {
		return $this->m_fltRjPrice;
	}

	public function sqlRjPrice() {
		return ( true == isset( $this->m_fltRjPrice ) ) ? ( string ) $this->m_fltRjPrice : 'NULL';
	}

	public function setRjBaths( $strRjBaths ) {
		$this->set( 'm_strRjBaths', CStrings::strTrimDef( $strRjBaths, 50, NULL, true ) );
	}

	public function getRjBaths() {
		return $this->m_strRjBaths;
	}

	public function sqlRjBaths() {
		return ( true == isset( $this->m_strRjBaths ) ) ? '\'' . addslashes( $this->m_strRjBaths ) . '\'' : 'NULL';
	}

	public function setRjArea( $fltRjArea ) {
		$this->set( 'm_fltRjArea', CStrings::strToFloatDef( $fltRjArea, NULL, false, 2 ) );
	}

	public function getRjArea() {
		return $this->m_fltRjArea;
	}

	public function sqlRjArea() {
		return ( true == isset( $this->m_fltRjArea ) ) ? ( string ) $this->m_fltRjArea : 'NULL';
	}

	public function setRjLastseen( $strRjLastseen ) {
		$this->set( 'm_strRjLastseen', CStrings::strTrimDef( $strRjLastseen, 10, NULL, true ) );
	}

	public function getRjLastseen() {
		return $this->m_strRjLastseen;
	}

	public function sqlRjLastseen() {
		return ( true == isset( $this->m_strRjLastseen ) ) ? '\'' . addslashes( $this->m_strRjLastseen ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, rj_id, rj_beds, rj_price, rj_baths, rj_area, rj_lastseen, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRjId() . ', ' .
 						$this->sqlRjBeds() . ', ' .
 						$this->sqlRjPrice() . ', ' .
 						$this->sqlRjBaths() . ', ' .
 						$this->sqlRjArea() . ', ' .
 						$this->sqlRjLastseen() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_id = ' . $this->sqlRjId() . ','; } elseif( true == array_key_exists( 'RjId', $this->getChangedColumns() ) ) { $strSql .= ' rj_id = ' . $this->sqlRjId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_beds = ' . $this->sqlRjBeds() . ','; } elseif( true == array_key_exists( 'RjBeds', $this->getChangedColumns() ) ) { $strSql .= ' rj_beds = ' . $this->sqlRjBeds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_price = ' . $this->sqlRjPrice() . ','; } elseif( true == array_key_exists( 'RjPrice', $this->getChangedColumns() ) ) { $strSql .= ' rj_price = ' . $this->sqlRjPrice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_baths = ' . $this->sqlRjBaths() . ','; } elseif( true == array_key_exists( 'RjBaths', $this->getChangedColumns() ) ) { $strSql .= ' rj_baths = ' . $this->sqlRjBaths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_area = ' . $this->sqlRjArea() . ','; } elseif( true == array_key_exists( 'RjArea', $this->getChangedColumns() ) ) { $strSql .= ' rj_area = ' . $this->sqlRjArea() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_lastseen = ' . $this->sqlRjLastseen() . ','; } elseif( true == array_key_exists( 'RjLastseen', $this->getChangedColumns() ) ) { $strSql .= ' rj_lastseen = ' . $this->sqlRjLastseen() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'rj_id' => $this->getRjId(),
			'rj_beds' => $this->getRjBeds(),
			'rj_price' => $this->getRjPrice(),
			'rj_baths' => $this->getRjBaths(),
			'rj_area' => $this->getRjArea(),
			'rj_lastseen' => $this->getRjLastseen(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>