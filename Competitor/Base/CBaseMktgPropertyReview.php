<?php

class CBaseMktgPropertyReview extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_reviews';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intPropertyId;
	protected $m_strName;
	protected $m_strEmail;
	protected $m_strTitle;
	protected $m_strReview;
	protected $m_strUrl;
	protected $m_strSource;
	protected $m_strReviewedOn;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['email'] ) && $boolDirectSet ) $this->set( 'm_strEmail', trim( stripcslashes( $arrValues['email'] ) ) ); elseif( isset( $arrValues['email'] ) ) $this->setEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email'] ) : $arrValues['email'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['review'] ) && $boolDirectSet ) $this->set( 'm_strReview', trim( stripcslashes( $arrValues['review'] ) ) ); elseif( isset( $arrValues['review'] ) ) $this->setReview( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['review'] ) : $arrValues['review'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['source'] ) && $boolDirectSet ) $this->set( 'm_strSource', trim( stripcslashes( $arrValues['source'] ) ) ); elseif( isset( $arrValues['source'] ) ) $this->setSource( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source'] ) : $arrValues['source'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setEmail( $strEmail ) {
		$this->set( 'm_strEmail', CStrings::strTrimDef( $strEmail, 240, NULL, true ) );
	}

	public function getEmail() {
		return $this->m_strEmail;
	}

	public function sqlEmail() {
		return ( true == isset( $this->m_strEmail ) ) ? '\'' . addslashes( $this->m_strEmail ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setReview( $strReview ) {
		$this->set( 'm_strReview', CStrings::strTrimDef( $strReview, -1, NULL, true ) );
	}

	public function getReview() {
		return $this->m_strReview;
	}

	public function sqlReview() {
		return ( true == isset( $this->m_strReview ) ) ? '\'' . addslashes( $this->m_strReview ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 2049, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setSource( $strSource ) {
		$this->set( 'm_strSource', CStrings::strTrimDef( $strSource, 255, NULL, true ) );
	}

	public function getSource() {
		return $this->m_strSource;
	}

	public function sqlSource() {
		return ( true == isset( $this->m_strSource ) ) ? '\'' . addslashes( $this->m_strSource ) . '\'' : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NOW()';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, property_id, name, email, title, review, url, source, reviewed_on, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlEmail() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlReview() . ', ' .
 						$this->sqlUrl() . ', ' .
 						$this->sqlSource() . ', ' .
 						$this->sqlReviewedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; } elseif( true == array_key_exists( 'Email', $this->getChangedColumns() ) ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review = ' . $this->sqlReview() . ','; } elseif( true == array_key_exists( 'Review', $this->getChangedColumns() ) ) { $strSql .= ' review = ' . $this->sqlReview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source = ' . $this->sqlSource() . ','; } elseif( true == array_key_exists( 'Source', $this->getChangedColumns() ) ) { $strSql .= ' source = ' . $this->sqlSource() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'property_id' => $this->getPropertyId(),
			'name' => $this->getName(),
			'email' => $this->getEmail(),
			'title' => $this->getTitle(),
			'review' => $this->getReview(),
			'url' => $this->getUrl(),
			'source' => $this->getSource(),
			'reviewed_on' => $this->getReviewedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>