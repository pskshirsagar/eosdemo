<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFloorplanRateLogs
 * Do not add any new functions to this class.
 */

class CBaseMktgFloorplanRateLogs extends CEosPluralBase {

	/**
	 * @return CMktgFloorplanRateLog[]
	 */
	public static function fetchMktgFloorplanRateLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgFloorplanRateLog', $objDatabase );
	}

	/**
	 * @return CMktgFloorplanRateLog
	 */
	public static function fetchMktgFloorplanRateLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgFloorplanRateLog', $objDatabase );
	}

	public static function fetchMktgFloorplanRateLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_floorplan_rate_logs', $objDatabase );
	}

	public static function fetchMktgFloorplanRateLogById( $intId, $objDatabase ) {
		return self::fetchMktgFloorplanRateLog( sprintf( 'SELECT * FROM mktg_floorplan_rate_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgFloorplanRateLogsByPropertyFloorplanId( $intPropertyFloorplanId, $objDatabase ) {
		return self::fetchMktgFloorplanRateLogs( sprintf( 'SELECT * FROM mktg_floorplan_rate_logs WHERE property_floorplan_id = %d', ( int ) $intPropertyFloorplanId ), $objDatabase );
	}

}
?>