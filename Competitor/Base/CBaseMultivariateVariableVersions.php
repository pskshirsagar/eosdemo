<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMultivariateVariableVersions
 * Do not add any new functions to this class.
 */

class CBaseMultivariateVariableVersions extends CEosPluralBase {

	/**
	 * @return CMultivariateVariableVersion[]
	 */
	public static function fetchMultivariateVariableVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMultivariateVariableVersion', $objDatabase );
	}

	/**
	 * @return CMultivariateVariableVersion
	 */
	public static function fetchMultivariateVariableVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMultivariateVariableVersion', $objDatabase );
	}

	public static function fetchMultivariateVariableVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'multivariate_variable_versions', $objDatabase );
	}

	public static function fetchMultivariateVariableVersionById( $intId, $objDatabase ) {
		return self::fetchMultivariateVariableVersion( sprintf( 'SELECT * FROM multivariate_variable_versions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMultivariateVariableVersionsByMultivariateVariablesId( $intMultivariateVariablesId, $objDatabase ) {
		return self::fetchMultivariateVariableVersions( sprintf( 'SELECT * FROM multivariate_variable_versions WHERE multivariate_variables_id = %d', ( int ) $intMultivariateVariablesId ), $objDatabase );
	}

}
?>