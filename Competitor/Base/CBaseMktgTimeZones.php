<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgTimeZones
 * Do not add any new functions to this class.
 */

class CBaseMktgTimeZones extends CEosPluralBase {

	/**
	 * @return CMktgTimeZone[]
	 */
	public static function fetchMktgTimeZones( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgTimeZone::class, $objDatabase );
	}

	/**
	 * @return CMktgTimeZone
	 */
	public static function fetchMktgTimeZone( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgTimeZone::class, $objDatabase );
	}

	public static function fetchMktgTimeZoneCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_time_zones', $objDatabase );
	}

	public static function fetchMktgTimeZoneById( $intId, $objDatabase ) {
		return self::fetchMktgTimeZone( sprintf( 'SELECT * FROM mktg_time_zones WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>