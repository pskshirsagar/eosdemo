<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgStates
 * Do not add any new functions to this class.
 */

class CBaseMktgStates extends CEosPluralBase {

	/**
	 * @return CMktgState[]
	 */
	public static function fetchMktgStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgState::class, $objDatabase );
	}

	/**
	 * @return CMktgState
	 */
	public static function fetchMktgState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgState::class, $objDatabase );
	}

	public static function fetchMktgStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_states', $objDatabase );
	}

	public static function fetchMktgStateById( $intId, $objDatabase ) {
		return self::fetchMktgState( sprintf( 'SELECT * FROM mktg_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>