<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyAddresses
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyAddresses extends CEosPluralBase {

	/**
	 * @return CMktgPropertyAddress[]
	 */
	public static function fetchMktgPropertyAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyAddress', $objDatabase );
	}

	/**
	 * @return CMktgPropertyAddress
	 */
	public static function fetchMktgPropertyAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyAddress', $objDatabase );
	}

	public static function fetchMktgPropertyAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_addresses', $objDatabase );
	}

	public static function fetchMktgPropertyAddressById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyAddress( sprintf( 'SELECT * FROM mktg_property_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyAddressesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyAddresses( sprintf( 'SELECT * FROM mktg_property_addresses WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAddressesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyAddresses( sprintf( 'SELECT * FROM mktg_property_addresses WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAddressesByTimeZoneId( $intTimeZoneId, $objDatabase ) {
		return self::fetchMktgPropertyAddresses( sprintf( 'SELECT * FROM mktg_property_addresses WHERE time_zone_id = %d', ( int ) $intTimeZoneId ), $objDatabase );
	}

}
?>