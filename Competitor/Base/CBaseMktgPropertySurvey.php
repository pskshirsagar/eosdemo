<?php

class CBaseMktgPropertySurvey extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.mktg_property_surveys';

	protected $m_intId;
	protected $m_intTemplateId;
	protected $m_intCompetitorPropertyId;
	protected $m_boolIsDraft;
	protected $m_strSurveySubmittedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDraft = false;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['template_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplateId', trim( $arrValues['template_id'] ) ); elseif( isset( $arrValues['template_id'] ) ) $this->setTemplateId( $arrValues['template_id'] );
		if( isset( $arrValues['competitor_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyId', trim( $arrValues['competitor_property_id'] ) ); elseif( isset( $arrValues['competitor_property_id'] ) ) $this->setCompetitorPropertyId( $arrValues['competitor_property_id'] );
		if( isset( $arrValues['is_draft'] ) && $boolDirectSet ) $this->set( 'm_boolIsDraft', trim( stripcslashes( $arrValues['is_draft'] ) ) ); elseif( isset( $arrValues['is_draft'] ) ) $this->setIsDraft( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_draft'] ) : $arrValues['is_draft'] );
		if( isset( $arrValues['survey_submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strSurveySubmittedOn', trim( $arrValues['survey_submitted_on'] ) ); elseif( isset( $arrValues['survey_submitted_on'] ) ) $this->setSurveySubmittedOn( $arrValues['survey_submitted_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTemplateId( $intTemplateId ) {
		$this->set( 'm_intTemplateId', CStrings::strToIntDef( $intTemplateId, NULL, false ) );
	}

	public function getTemplateId() {
		return $this->m_intTemplateId;
	}

	public function sqlTemplateId() {
		return ( true == isset( $this->m_intTemplateId ) ) ? ( string ) $this->m_intTemplateId : 'NULL';
	}

	public function setCompetitorPropertyId( $intCompetitorPropertyId ) {
		$this->set( 'm_intCompetitorPropertyId', CStrings::strToIntDef( $intCompetitorPropertyId, NULL, false ) );
	}

	public function getCompetitorPropertyId() {
		return $this->m_intCompetitorPropertyId;
	}

	public function sqlCompetitorPropertyId() {
		return ( true == isset( $this->m_intCompetitorPropertyId ) ) ? ( string ) $this->m_intCompetitorPropertyId : 'NULL';
	}

	public function setIsDraft( $boolIsDraft ) {
		$this->set( 'm_boolIsDraft', CStrings::strToBool( $boolIsDraft ) );
	}

	public function getIsDraft() {
		return $this->m_boolIsDraft;
	}

	public function sqlIsDraft() {
		return ( true == isset( $this->m_boolIsDraft ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDraft ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSurveySubmittedOn( $strSurveySubmittedOn ) {
		$this->set( 'm_strSurveySubmittedOn', CStrings::strTrimDef( $strSurveySubmittedOn, -1, NULL, true ) );
	}

	public function getSurveySubmittedOn() {
		return $this->m_strSurveySubmittedOn;
	}

	public function sqlSurveySubmittedOn() {
		return ( true == isset( $this->m_strSurveySubmittedOn ) ) ? '\'' . $this->m_strSurveySubmittedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, template_id, competitor_property_id, is_draft, survey_submitted_on, details, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTemplateId() . ', ' .
						$this->sqlCompetitorPropertyId() . ', ' .
						$this->sqlIsDraft() . ', ' .
						$this->sqlSurveySubmittedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_id = ' . $this->sqlTemplateId(). ',' ; } elseif( true == array_key_exists( 'TemplateId', $this->getChangedColumns() ) ) { $strSql .= ' template_id = ' . $this->sqlTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId(). ',' ; } elseif( true == array_key_exists( 'CompetitorPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_draft = ' . $this->sqlIsDraft(). ',' ; } elseif( true == array_key_exists( 'IsDraft', $this->getChangedColumns() ) ) { $strSql .= ' is_draft = ' . $this->sqlIsDraft() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_submitted_on = ' . $this->sqlSurveySubmittedOn(). ',' ; } elseif( true == array_key_exists( 'SurveySubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' survey_submitted_on = ' . $this->sqlSurveySubmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'template_id' => $this->getTemplateId(),
			'competitor_property_id' => $this->getCompetitorPropertyId(),
			'is_draft' => $this->getIsDraft(),
			'survey_submitted_on' => $this->getSurveySubmittedOn(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>