<?php

class CBaseMktgPropertyUnit extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_units';

	protected $m_intId;
	protected $m_strRemotePropertyUnitId;
	protected $m_strRemoteUnitSpaceId;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_intPropertyFloorplanId;
	protected $m_intPropertyUnitTypeId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyFloorId;
	protected $m_strUnitNumber;
	protected $m_fltSquareFeet;
	protected $m_intNumberOfRooms;
	protected $m_intNumberOfBedrooms;
	protected $m_fltNumberOfBathrooms;
	protected $m_strAppliancesDescription;
	protected $m_intMaxOccupants;
	protected $m_intDefaultLeaseTermMonths;
	protected $m_intIsFurnished;
	protected $m_strAvailableOn;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strIsDisabled;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFurnished = '0';
		$this->m_strIsDisabled = 'N';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_strRemotePropertyUnitId', trim( stripcslashes( $arrValues['remote_property_unit_id'] ) ) ); elseif( isset( $arrValues['remote_property_unit_id'] ) ) $this->setRemotePropertyUnitId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_property_unit_id'] ) : $arrValues['remote_property_unit_id'] );
		if( isset( $arrValues['remote_unit_space_id'] ) && $boolDirectSet ) $this->set( 'm_strRemoteUnitSpaceId', trim( stripcslashes( $arrValues['remote_unit_space_id'] ) ) ); elseif( isset( $arrValues['remote_unit_space_id'] ) ) $this->setRemoteUnitSpaceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_unit_space_id'] ) : $arrValues['remote_unit_space_id'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorplanId', trim( $arrValues['property_floorplan_id'] ) ); elseif( isset( $arrValues['property_floorplan_id'] ) ) $this->setPropertyFloorplanId( $arrValues['property_floorplan_id'] );
		if( isset( $arrValues['property_unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitTypeId', trim( $arrValues['property_unit_type_id'] ) ); elseif( isset( $arrValues['property_unit_type_id'] ) ) $this->setPropertyUnitTypeId( $arrValues['property_unit_type_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_floor_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyFloorId', trim( $arrValues['property_floor_id'] ) ); elseif( isset( $arrValues['property_floor_id'] ) ) $this->setPropertyFloorId( $arrValues['property_floor_id'] );
		if( isset( $arrValues['unit_number'] ) && $boolDirectSet ) $this->set( 'm_strUnitNumber', trim( stripcslashes( $arrValues['unit_number'] ) ) ); elseif( isset( $arrValues['unit_number'] ) ) $this->setUnitNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unit_number'] ) : $arrValues['unit_number'] );
		if( isset( $arrValues['square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltSquareFeet', trim( $arrValues['square_feet'] ) ); elseif( isset( $arrValues['square_feet'] ) ) $this->setSquareFeet( $arrValues['square_feet'] );
		if( isset( $arrValues['number_of_rooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfRooms', trim( $arrValues['number_of_rooms'] ) ); elseif( isset( $arrValues['number_of_rooms'] ) ) $this->setNumberOfRooms( $arrValues['number_of_rooms'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['appliances_description'] ) && $boolDirectSet ) $this->set( 'm_strAppliancesDescription', trim( stripcslashes( $arrValues['appliances_description'] ) ) ); elseif( isset( $arrValues['appliances_description'] ) ) $this->setAppliancesDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['appliances_description'] ) : $arrValues['appliances_description'] );
		if( isset( $arrValues['max_occupants'] ) && $boolDirectSet ) $this->set( 'm_intMaxOccupants', trim( $arrValues['max_occupants'] ) ); elseif( isset( $arrValues['max_occupants'] ) ) $this->setMaxOccupants( $arrValues['max_occupants'] );
		if( isset( $arrValues['default_lease_term_months'] ) && $boolDirectSet ) $this->set( 'm_intDefaultLeaseTermMonths', trim( $arrValues['default_lease_term_months'] ) ); elseif( isset( $arrValues['default_lease_term_months'] ) ) $this->setDefaultLeaseTermMonths( $arrValues['default_lease_term_months'] );
		if( isset( $arrValues['is_furnished'] ) && $boolDirectSet ) $this->set( 'm_intIsFurnished', trim( $arrValues['is_furnished'] ) ); elseif( isset( $arrValues['is_furnished'] ) ) $this->setIsFurnished( $arrValues['is_furnished'] );
		if( isset( $arrValues['available_on'] ) && $boolDirectSet ) $this->set( 'm_strAvailableOn', trim( $arrValues['available_on'] ) ); elseif( isset( $arrValues['available_on'] ) ) $this->setAvailableOn( $arrValues['available_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_strIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePropertyUnitId( $strRemotePropertyUnitId ) {
		$this->set( 'm_strRemotePropertyUnitId', CStrings::strTrimDef( $strRemotePropertyUnitId, 100, NULL, true ) );
	}

	public function getRemotePropertyUnitId() {
		return $this->m_strRemotePropertyUnitId;
	}

	public function sqlRemotePropertyUnitId() {
		return ( true == isset( $this->m_strRemotePropertyUnitId ) ) ? '\'' . addslashes( $this->m_strRemotePropertyUnitId ) . '\'' : 'NULL';
	}

	public function setRemoteUnitSpaceId( $strRemoteUnitSpaceId ) {
		$this->set( 'm_strRemoteUnitSpaceId', CStrings::strTrimDef( $strRemoteUnitSpaceId, 100, NULL, true ) );
	}

	public function getRemoteUnitSpaceId() {
		return $this->m_strRemoteUnitSpaceId;
	}

	public function sqlRemoteUnitSpaceId() {
		return ( true == isset( $this->m_strRemoteUnitSpaceId ) ) ? '\'' . addslashes( $this->m_strRemoteUnitSpaceId ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyFloorplanId( $intPropertyFloorplanId ) {
		$this->set( 'm_intPropertyFloorplanId', CStrings::strToIntDef( $intPropertyFloorplanId, NULL, false ) );
	}

	public function getPropertyFloorplanId() {
		return $this->m_intPropertyFloorplanId;
	}

	public function sqlPropertyFloorplanId() {
		return ( true == isset( $this->m_intPropertyFloorplanId ) ) ? ( string ) $this->m_intPropertyFloorplanId : 'NULL';
	}

	public function setPropertyUnitTypeId( $intPropertyUnitTypeId ) {
		$this->set( 'm_intPropertyUnitTypeId', CStrings::strToIntDef( $intPropertyUnitTypeId, NULL, false ) );
	}

	public function getPropertyUnitTypeId() {
		return $this->m_intPropertyUnitTypeId;
	}

	public function sqlPropertyUnitTypeId() {
		return ( true == isset( $this->m_intPropertyUnitTypeId ) ) ? ( string ) $this->m_intPropertyUnitTypeId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyFloorId( $intPropertyFloorId ) {
		$this->set( 'm_intPropertyFloorId', CStrings::strToIntDef( $intPropertyFloorId, NULL, false ) );
	}

	public function getPropertyFloorId() {
		return $this->m_intPropertyFloorId;
	}

	public function sqlPropertyFloorId() {
		return ( true == isset( $this->m_intPropertyFloorId ) ) ? ( string ) $this->m_intPropertyFloorId : 'NULL';
	}

	public function setUnitNumber( $strUnitNumber ) {
		$this->set( 'm_strUnitNumber', CStrings::strTrimDef( $strUnitNumber, 100, NULL, true ) );
	}

	public function getUnitNumber() {
		return $this->m_strUnitNumber;
	}

	public function sqlUnitNumber() {
		return ( true == isset( $this->m_strUnitNumber ) ) ? '\'' . addslashes( $this->m_strUnitNumber ) . '\'' : 'NULL';
	}

	public function setSquareFeet( $fltSquareFeet ) {
		$this->set( 'm_fltSquareFeet', CStrings::strToFloatDef( $fltSquareFeet, NULL, false, 4 ) );
	}

	public function getSquareFeet() {
		return $this->m_fltSquareFeet;
	}

	public function sqlSquareFeet() {
		return ( true == isset( $this->m_fltSquareFeet ) ) ? ( string ) $this->m_fltSquareFeet : 'NULL';
	}

	public function setNumberOfRooms( $intNumberOfRooms ) {
		$this->set( 'm_intNumberOfRooms', CStrings::strToIntDef( $intNumberOfRooms, NULL, false ) );
	}

	public function getNumberOfRooms() {
		return $this->m_intNumberOfRooms;
	}

	public function sqlNumberOfRooms() {
		return ( true == isset( $this->m_intNumberOfRooms ) ) ? ( string ) $this->m_intNumberOfRooms : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->set( 'm_fltNumberOfBathrooms', CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_fltNumberOfBathrooms ) ) ? ( string ) $this->m_fltNumberOfBathrooms : 'NULL';
	}

	public function setAppliancesDescription( $strAppliancesDescription ) {
		$this->set( 'm_strAppliancesDescription', CStrings::strTrimDef( $strAppliancesDescription, 240, NULL, true ) );
	}

	public function getAppliancesDescription() {
		return $this->m_strAppliancesDescription;
	}

	public function sqlAppliancesDescription() {
		return ( true == isset( $this->m_strAppliancesDescription ) ) ? '\'' . addslashes( $this->m_strAppliancesDescription ) . '\'' : 'NULL';
	}

	public function setMaxOccupants( $intMaxOccupants ) {
		$this->set( 'm_intMaxOccupants', CStrings::strToIntDef( $intMaxOccupants, NULL, false ) );
	}

	public function getMaxOccupants() {
		return $this->m_intMaxOccupants;
	}

	public function sqlMaxOccupants() {
		return ( true == isset( $this->m_intMaxOccupants ) ) ? ( string ) $this->m_intMaxOccupants : 'NULL';
	}

	public function setDefaultLeaseTermMonths( $intDefaultLeaseTermMonths ) {
		$this->set( 'm_intDefaultLeaseTermMonths', CStrings::strToIntDef( $intDefaultLeaseTermMonths, NULL, false ) );
	}

	public function getDefaultLeaseTermMonths() {
		return $this->m_intDefaultLeaseTermMonths;
	}

	public function sqlDefaultLeaseTermMonths() {
		return ( true == isset( $this->m_intDefaultLeaseTermMonths ) ) ? ( string ) $this->m_intDefaultLeaseTermMonths : 'NULL';
	}

	public function setIsFurnished( $intIsFurnished ) {
		$this->set( 'm_intIsFurnished', CStrings::strToIntDef( $intIsFurnished, NULL, false ) );
	}

	public function getIsFurnished() {
		return $this->m_intIsFurnished;
	}

	public function sqlIsFurnished() {
		return ( true == isset( $this->m_intIsFurnished ) ) ? ( string ) $this->m_intIsFurnished : '0';
	}

	public function setAvailableOn( $strAvailableOn ) {
		$this->set( 'm_strAvailableOn', CStrings::strTrimDef( $strAvailableOn, -1, NULL, true ) );
	}

	public function getAvailableOn() {
		return $this->m_strAvailableOn;
	}

	public function sqlAvailableOn() {
		return ( true == isset( $this->m_strAvailableOn ) ) ? '\'' . $this->m_strAvailableOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setIsDisabled( $strIsDisabled ) {
		$this->set( 'm_strIsDisabled', CStrings::strTrimDef( $strIsDisabled, 1, NULL, true ) );
	}

	public function getIsDisabled() {
		return $this->m_strIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_strIsDisabled ) ) ? '\'' . addslashes( $this->m_strIsDisabled ) . '\'' : '\'N\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_property_unit_id, remote_unit_space_id, company_id, property_id, property_floorplan_id, property_unit_type_id, property_building_id, property_floor_id, unit_number, square_feet, number_of_rooms, number_of_bedrooms, number_of_bathrooms, appliances_description, max_occupants, default_lease_term_months, is_furnished, available_on, created_on, updated_on, is_disabled )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePropertyUnitId() . ', ' .
 						$this->sqlRemoteUnitSpaceId() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyFloorplanId() . ', ' .
 						$this->sqlPropertyUnitTypeId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlPropertyFloorId() . ', ' .
 						$this->sqlUnitNumber() . ', ' .
 						$this->sqlSquareFeet() . ', ' .
 						$this->sqlNumberOfRooms() . ', ' .
 						$this->sqlNumberOfBedrooms() . ', ' .
 						$this->sqlNumberOfBathrooms() . ', ' .
 						$this->sqlAppliancesDescription() . ', ' .
 						$this->sqlMaxOccupants() . ', ' .
 						$this->sqlDefaultLeaseTermMonths() . ', ' .
 						$this->sqlIsFurnished() . ', ' .
 						$this->sqlAvailableOn() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlIsDisabled() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_property_unit_id = ' . $this->sqlRemotePropertyUnitId() . ','; } elseif( true == array_key_exists( 'RemotePropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' remote_property_unit_id = ' . $this->sqlRemotePropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_unit_space_id = ' . $this->sqlRemoteUnitSpaceId() . ','; } elseif( true == array_key_exists( 'RemoteUnitSpaceId', $this->getChangedColumns() ) ) { $strSql .= ' remote_unit_space_id = ' . $this->sqlRemoteUnitSpaceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'PropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' property_floorplan_id = ' . $this->sqlPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_type_id = ' . $this->sqlPropertyUnitTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_type_id = ' . $this->sqlPropertyUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; } elseif( true == array_key_exists( 'PropertyFloorId', $this->getChangedColumns() ) ) { $strSql .= ' property_floor_id = ' . $this->sqlPropertyFloorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; } elseif( true == array_key_exists( 'UnitNumber', $this->getChangedColumns() ) ) { $strSql .= ' unit_number = ' . $this->sqlUnitNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; } elseif( true == array_key_exists( 'SquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' square_feet = ' . $this->sqlSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; } elseif( true == array_key_exists( 'NumberOfRooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription() . ','; } elseif( true == array_key_exists( 'AppliancesDescription', $this->getChangedColumns() ) ) { $strSql .= ' appliances_description = ' . $this->sqlAppliancesDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_occupants = ' . $this->sqlMaxOccupants() . ','; } elseif( true == array_key_exists( 'MaxOccupants', $this->getChangedColumns() ) ) { $strSql .= ' max_occupants = ' . $this->sqlMaxOccupants() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths() . ','; } elseif( true == array_key_exists( 'DefaultLeaseTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' default_lease_term_months = ' . $this->sqlDefaultLeaseTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished() . ','; } elseif( true == array_key_exists( 'IsFurnished', $this->getChangedColumns() ) ) { $strSql .= ' is_furnished = ' . $this->sqlIsFurnished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; } elseif( true == array_key_exists( 'AvailableOn', $this->getChangedColumns() ) ) { $strSql .= ' available_on = ' . $this->sqlAvailableOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_property_unit_id' => $this->getRemotePropertyUnitId(),
			'remote_unit_space_id' => $this->getRemoteUnitSpaceId(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'property_floorplan_id' => $this->getPropertyFloorplanId(),
			'property_unit_type_id' => $this->getPropertyUnitTypeId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_floor_id' => $this->getPropertyFloorId(),
			'unit_number' => $this->getUnitNumber(),
			'square_feet' => $this->getSquareFeet(),
			'number_of_rooms' => $this->getNumberOfRooms(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'appliances_description' => $this->getAppliancesDescription(),
			'max_occupants' => $this->getMaxOccupants(),
			'default_lease_term_months' => $this->getDefaultLeaseTermMonths(),
			'is_furnished' => $this->getIsFurnished(),
			'available_on' => $this->getAvailableOn(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'is_disabled' => $this->getIsDisabled()
		);
	}

}
?>