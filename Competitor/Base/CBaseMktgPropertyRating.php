<?php

class CBaseMktgPropertyRating extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_ratings';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intPropertyId;
	protected $m_intPropertyReviewId;
	protected $m_strRatingType;
	protected $m_fltRatingValue;
	protected $m_strRatingMetric;
	protected $m_strRatingSource;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_review_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyReviewId', trim( $arrValues['property_review_id'] ) ); elseif( isset( $arrValues['property_review_id'] ) ) $this->setPropertyReviewId( $arrValues['property_review_id'] );
		if( isset( $arrValues['rating_type'] ) && $boolDirectSet ) $this->set( 'm_strRatingType', trim( stripcslashes( $arrValues['rating_type'] ) ) ); elseif( isset( $arrValues['rating_type'] ) ) $this->setRatingType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rating_type'] ) : $arrValues['rating_type'] );
		if( isset( $arrValues['rating_value'] ) && $boolDirectSet ) $this->set( 'm_fltRatingValue', trim( $arrValues['rating_value'] ) ); elseif( isset( $arrValues['rating_value'] ) ) $this->setRatingValue( $arrValues['rating_value'] );
		if( isset( $arrValues['rating_metric'] ) && $boolDirectSet ) $this->set( 'm_strRatingMetric', trim( stripcslashes( $arrValues['rating_metric'] ) ) ); elseif( isset( $arrValues['rating_metric'] ) ) $this->setRatingMetric( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rating_metric'] ) : $arrValues['rating_metric'] );
		if( isset( $arrValues['rating_source'] ) && $boolDirectSet ) $this->set( 'm_strRatingSource', trim( stripcslashes( $arrValues['rating_source'] ) ) ); elseif( isset( $arrValues['rating_source'] ) ) $this->setRatingSource( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rating_source'] ) : $arrValues['rating_source'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyReviewId( $intPropertyReviewId ) {
		$this->set( 'm_intPropertyReviewId', CStrings::strToIntDef( $intPropertyReviewId, NULL, false ) );
	}

	public function getPropertyReviewId() {
		return $this->m_intPropertyReviewId;
	}

	public function sqlPropertyReviewId() {
		return ( true == isset( $this->m_intPropertyReviewId ) ) ? ( string ) $this->m_intPropertyReviewId : 'NULL';
	}

	public function setRatingType( $strRatingType ) {
		$this->set( 'm_strRatingType', CStrings::strTrimDef( $strRatingType, 50, NULL, true ) );
	}

	public function getRatingType() {
		return $this->m_strRatingType;
	}

	public function sqlRatingType() {
		return ( true == isset( $this->m_strRatingType ) ) ? '\'' . addslashes( $this->m_strRatingType ) . '\'' : 'NULL';
	}

	public function setRatingValue( $fltRatingValue ) {
		$this->set( 'm_fltRatingValue', CStrings::strToFloatDef( $fltRatingValue, NULL, false, 2 ) );
	}

	public function getRatingValue() {
		return $this->m_fltRatingValue;
	}

	public function sqlRatingValue() {
		return ( true == isset( $this->m_fltRatingValue ) ) ? ( string ) $this->m_fltRatingValue : 'NULL';
	}

	public function setRatingMetric( $strRatingMetric ) {
		$this->set( 'm_strRatingMetric', CStrings::strTrimDef( $strRatingMetric, 20, NULL, true ) );
	}

	public function getRatingMetric() {
		return $this->m_strRatingMetric;
	}

	public function sqlRatingMetric() {
		return ( true == isset( $this->m_strRatingMetric ) ) ? '\'' . addslashes( $this->m_strRatingMetric ) . '\'' : 'NULL';
	}

	public function setRatingSource( $strRatingSource ) {
		$this->set( 'm_strRatingSource', CStrings::strTrimDef( $strRatingSource, 255, NULL, true ) );
	}

	public function getRatingSource() {
		return $this->m_strRatingSource;
	}

	public function sqlRatingSource() {
		return ( true == isset( $this->m_strRatingSource ) ) ? '\'' . addslashes( $this->m_strRatingSource ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, property_id, property_review_id, rating_type, rating_value, rating_metric, rating_source, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyReviewId() . ', ' .
 						$this->sqlRatingType() . ', ' .
 						$this->sqlRatingValue() . ', ' .
 						$this->sqlRatingMetric() . ', ' .
 						$this->sqlRatingSource() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_review_id = ' . $this->sqlPropertyReviewId() . ','; } elseif( true == array_key_exists( 'PropertyReviewId', $this->getChangedColumns() ) ) { $strSql .= ' property_review_id = ' . $this->sqlPropertyReviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rating_type = ' . $this->sqlRatingType() . ','; } elseif( true == array_key_exists( 'RatingType', $this->getChangedColumns() ) ) { $strSql .= ' rating_type = ' . $this->sqlRatingType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rating_value = ' . $this->sqlRatingValue() . ','; } elseif( true == array_key_exists( 'RatingValue', $this->getChangedColumns() ) ) { $strSql .= ' rating_value = ' . $this->sqlRatingValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rating_metric = ' . $this->sqlRatingMetric() . ','; } elseif( true == array_key_exists( 'RatingMetric', $this->getChangedColumns() ) ) { $strSql .= ' rating_metric = ' . $this->sqlRatingMetric() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rating_source = ' . $this->sqlRatingSource() . ','; } elseif( true == array_key_exists( 'RatingSource', $this->getChangedColumns() ) ) { $strSql .= ' rating_source = ' . $this->sqlRatingSource() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'property_id' => $this->getPropertyId(),
			'property_review_id' => $this->getPropertyReviewId(),
			'rating_type' => $this->getRatingType(),
			'rating_value' => $this->getRatingValue(),
			'rating_metric' => $this->getRatingMetric(),
			'rating_source' => $this->getRatingSource(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>