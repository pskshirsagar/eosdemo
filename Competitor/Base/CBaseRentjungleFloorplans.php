<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CRentjungleFloorplans
 * Do not add any new functions to this class.
 */

class CBaseRentjungleFloorplans extends CEosPluralBase {

	/**
	 * @return CRentjungleFloorplan[]
	 */
	public static function fetchRentjungleFloorplans( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRentjungleFloorplan', $objDatabase );
	}

	/**
	 * @return CRentjungleFloorplan
	 */
	public static function fetchRentjungleFloorplan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRentjungleFloorplan', $objDatabase );
	}

	public static function fetchRentjungleFloorplanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rentjungle_floorplans', $objDatabase );
	}

	public static function fetchRentjungleFloorplanById( $intId, $objDatabase ) {
		return self::fetchRentjungleFloorplan( sprintf( 'SELECT * FROM rentjungle_floorplans WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRentjungleFloorplansByRjId( $strRjId, $objDatabase ) {
		return self::fetchRentjungleFloorplans( sprintf( 'SELECT * FROM rentjungle_floorplans WHERE rj_id = \'%s\'', $strRjId ), $objDatabase );
	}

}
?>