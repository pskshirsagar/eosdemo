<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyDetails
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyDetails extends CEosPluralBase {

	/**
	 * @return CMktgPropertyDetail[]
	 */
	public static function fetchMktgPropertyDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgPropertyDetail::class, $objDatabase );
	}

	/**
	 * @return CMktgPropertyDetail
	 */
	public static function fetchMktgPropertyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgPropertyDetail::class, $objDatabase );
	}

	public static function fetchMktgPropertyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_details', $objDatabase );
	}

	public static function fetchMktgPropertyDetailById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyDetail( sprintf( 'SELECT * FROM mktg_property_details WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyDetailsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyDetails( sprintf( 'SELECT * FROM mktg_property_details WHERE company_id = %d', $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyDetails( sprintf( 'SELECT * FROM mktg_property_details WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

}
?>