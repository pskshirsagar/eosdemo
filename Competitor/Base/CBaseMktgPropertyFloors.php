<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyFloors
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyFloors extends CEosPluralBase {

	/**
	 * @return CMktgPropertyFloor[]
	 */
	public static function fetchMktgPropertyFloors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyFloor', $objDatabase );
	}

	/**
	 * @return CMktgPropertyFloor
	 */
	public static function fetchMktgPropertyFloor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyFloor', $objDatabase );
	}

	public static function fetchMktgPropertyFloorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_floors', $objDatabase );
	}

	public static function fetchMktgPropertyFloorById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyFloor( sprintf( 'SELECT * FROM mktg_property_floors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyFloors( sprintf( 'SELECT * FROM mktg_property_floors WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyFloors( sprintf( 'SELECT * FROM mktg_property_floors WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorsByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return self::fetchMktgPropertyFloors( sprintf( 'SELECT * FROM mktg_property_floors WHERE company_media_file_id = %d', ( int ) $intCompanyMediaFileId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorsByPropertyBuildingId( $intPropertyBuildingId, $objDatabase ) {
		return self::fetchMktgPropertyFloors( sprintf( 'SELECT * FROM mktg_property_floors WHERE property_building_id = %d', ( int ) $intPropertyBuildingId ), $objDatabase );
	}

}
?>