<?php

class CBaseMktgProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_properties';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSource;
	protected $m_intCompanyId;
	protected $m_intTimeZoneId;
	protected $m_intSourcePropertyId;
	protected $m_strPropertyName;
	protected $m_strPropertyType;
	protected $m_strIsDisabled;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strPropertyOccupancyTypes;
	protected $m_intParentPropertyId;
	protected $m_strParentRemotePrimaryKey;

	public function __construct() {
		parent::__construct();

		$this->m_strPropertyType = 'Unspecified';
		$this->m_strIsDisabled = 'N';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['source'] ) && $boolDirectSet ) $this->set( 'm_strSource', trim( $arrValues['source'] ) ); elseif( isset( $arrValues['source'] ) ) $this->setSource( $arrValues['source'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['source_property_id'] ) && $boolDirectSet ) $this->set( 'm_intSourcePropertyId', trim( $arrValues['source_property_id'] ) ); elseif( isset( $arrValues['source_property_id'] ) ) $this->setSourcePropertyId( $arrValues['source_property_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( $arrValues['property_name'] ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( $arrValues['property_name'] );
		if( isset( $arrValues['property_type'] ) && $boolDirectSet ) $this->set( 'm_strPropertyType', trim( $arrValues['property_type'] ) ); elseif( isset( $arrValues['property_type'] ) ) $this->setPropertyType( $arrValues['property_type'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_strIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['property_occupancy_types'] ) && $boolDirectSet ) $this->set( 'm_strPropertyOccupancyTypes', trim( $arrValues['property_occupancy_types'] ) ); elseif( isset( $arrValues['property_occupancy_types'] ) ) $this->setPropertyOccupancyTypes( $arrValues['property_occupancy_types'] );
		if( isset( $arrValues['parent_property_id'] ) && $boolDirectSet ) $this->set( 'm_intParentPropertyId', trim( $arrValues['parent_property_id'] ) ); elseif( isset( $arrValues['parent_property_id'] ) ) $this->setParentPropertyId( $arrValues['parent_property_id'] );
		if( isset( $arrValues['parent_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strParentRemotePrimaryKey', trim( $arrValues['parent_remote_primary_key'] ) ); elseif( isset( $arrValues['parent_remote_primary_key'] ) ) $this->setParentRemotePrimaryKey( $arrValues['parent_remote_primary_key'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setSource( $strSource ) {
		$this->set( 'm_strSource', CStrings::strTrimDef( $strSource, 50, NULL, true ) );
	}

	public function getSource() {
		return $this->m_strSource;
	}

	public function sqlSource() {
		return ( true == isset( $this->m_strSource ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSource ) : '\'' . addslashes( $this->m_strSource ) . '\'' ) : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : 'NULL';
	}

	public function setSourcePropertyId( $intSourcePropertyId ) {
		$this->set( 'm_intSourcePropertyId', CStrings::strToIntDef( $intSourcePropertyId, NULL, false ) );
	}

	public function getSourcePropertyId() {
		return $this->m_intSourcePropertyId;
	}

	public function sqlSourcePropertyId() {
		return ( true == isset( $this->m_intSourcePropertyId ) ) ? ( string ) $this->m_intSourcePropertyId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyName ) : '\'' . addslashes( $this->m_strPropertyName ) . '\'' ) : 'NULL';
	}

	public function setPropertyType( $strPropertyType ) {
		$this->set( 'm_strPropertyType', CStrings::strTrimDef( $strPropertyType, 100, NULL, true ) );
	}

	public function getPropertyType() {
		return $this->m_strPropertyType;
	}

	public function sqlPropertyType() {
		return ( true == isset( $this->m_strPropertyType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyType ) : '\'' . addslashes( $this->m_strPropertyType ) . '\'' ) : '\'Unspecified\'';
	}

	public function setIsDisabled( $strIsDisabled ) {
		$this->set( 'm_strIsDisabled', CStrings::strTrimDef( $strIsDisabled, 1, NULL, true ) );
	}

	public function getIsDisabled() {
		return $this->m_strIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_strIsDisabled ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIsDisabled ) : '\'' . addslashes( $this->m_strIsDisabled ) . '\'' ) : '\'N\'';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setPropertyOccupancyTypes( $strPropertyOccupancyTypes ) {
		$this->set( 'm_strPropertyOccupancyTypes', CStrings::strTrimDef( $strPropertyOccupancyTypes, -1, NULL, true ) );
	}

	public function getPropertyOccupancyTypes() {
		return $this->m_strPropertyOccupancyTypes;
	}

	public function sqlPropertyOccupancyTypes() {
		return ( true == isset( $this->m_strPropertyOccupancyTypes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPropertyOccupancyTypes ) : '\'' . addslashes( $this->m_strPropertyOccupancyTypes ) . '\'' ) : 'NULL';
	}

	public function setParentPropertyId( $intParentPropertyId ) {
		$this->set( 'm_intParentPropertyId', CStrings::strToIntDef( $intParentPropertyId, NULL, false ) );
	}

	public function getParentPropertyId() {
		return $this->m_intParentPropertyId;
	}

	public function sqlParentPropertyId() {
		return ( true == isset( $this->m_intParentPropertyId ) ) ? ( string ) $this->m_intParentPropertyId : 'NULL';
	}

	public function setParentRemotePrimaryKey( $strParentRemotePrimaryKey ) {
		$this->set( 'm_strParentRemotePrimaryKey', CStrings::strTrimDef( $strParentRemotePrimaryKey, -1, NULL, true ) );
	}

	public function getParentRemotePrimaryKey() {
		return $this->m_strParentRemotePrimaryKey;
	}

	public function sqlParentRemotePrimaryKey() {
		return ( true == isset( $this->m_strParentRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentRemotePrimaryKey ) : '\'' . addslashes( $this->m_strParentRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, source, company_id, time_zone_id, source_property_id, property_name, property_type, is_disabled, created_on, updated_on, property_occupancy_types, parent_property_id, parent_remote_primary_key )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlRemotePrimaryKey() . ', ' .
		          $this->sqlSource() . ', ' .
		          $this->sqlCompanyId() . ', ' .
		          $this->sqlTimeZoneId() . ', ' .
		          $this->sqlSourcePropertyId() . ', ' .
		          $this->sqlPropertyName() . ', ' .
		          $this->sqlPropertyType() . ', ' .
		          $this->sqlIsDisabled() . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          $this->sqlPropertyOccupancyTypes() . ', ' .
		          $this->sqlParentPropertyId() . ', ' .
		          $this->sqlParentRemotePrimaryKey() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source = ' . $this->sqlSource(). ',' ; } elseif( true == array_key_exists( 'Source', $this->getChangedColumns() ) ) { $strSql .= ' source = ' . $this->sqlSource() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId(). ',' ; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId(). ',' ; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId(). ',' ; } elseif( true == array_key_exists( 'SourcePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' source_property_id = ' . $this->sqlSourcePropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type = ' . $this->sqlPropertyType(). ',' ; } elseif( true == array_key_exists( 'PropertyType', $this->getChangedColumns() ) ) { $strSql .= ' property_type = ' . $this->sqlPropertyType() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_occupancy_types = ' . $this->sqlPropertyOccupancyTypes(). ',' ; } elseif( true == array_key_exists( 'PropertyOccupancyTypes', $this->getChangedColumns() ) ) { $strSql .= ' property_occupancy_types = ' . $this->sqlPropertyOccupancyTypes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_property_id = ' . $this->sqlParentPropertyId(). ',' ; } elseif( true == array_key_exists( 'ParentPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' parent_property_id = ' . $this->sqlParentPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_remote_primary_key = ' . $this->sqlParentRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'ParentRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' parent_remote_primary_key = ' . $this->sqlParentRemotePrimaryKey() . ','; $boolUpdate = true; }
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'source' => $this->getSource(),
			'company_id' => $this->getCompanyId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'source_property_id' => $this->getSourcePropertyId(),
			'property_name' => $this->getPropertyName(),
			'property_type' => $this->getPropertyType(),
			'is_disabled' => $this->getIsDisabled(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'property_occupancy_types' => $this->getPropertyOccupancyTypes(),
			'parent_property_id' => $this->getParentPropertyId(),
			'parent_remote_primary_key' => $this->getParentRemotePrimaryKey()
		);
	}

}
?>