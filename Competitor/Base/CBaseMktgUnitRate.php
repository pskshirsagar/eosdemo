<?php

class CBaseMktgUnitRate extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_unit_rates';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_intPropertyBuildingId;
	protected $m_intPropertyUnitId;
	protected $m_strEffectiveDate;
	protected $m_intLeaseTerm;
	protected $m_fltMarketRent;
	protected $m_fltBaseRent;
	protected $m_fltAmenitiesAdjustment;
	protected $m_fltSpecialsAdjustment;
	protected $m_fltEffectiveRent;
	protected $m_fltDepositAmount;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_building_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyBuildingId', trim( $arrValues['property_building_id'] ) ); elseif( isset( $arrValues['property_building_id'] ) ) $this->setPropertyBuildingId( $arrValues['property_building_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['lease_term'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTerm', trim( $arrValues['lease_term'] ) ); elseif( isset( $arrValues['lease_term'] ) ) $this->setLeaseTerm( $arrValues['lease_term'] );
		if( isset( $arrValues['market_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRent', trim( $arrValues['market_rent'] ) ); elseif( isset( $arrValues['market_rent'] ) ) $this->setMarketRent( $arrValues['market_rent'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_fltBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenities_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltAmenitiesAdjustment', trim( $arrValues['amenities_adjustment'] ) ); elseif( isset( $arrValues['amenities_adjustment'] ) ) $this->setAmenitiesAdjustment( $arrValues['amenities_adjustment'] );
		if( isset( $arrValues['specials_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltSpecialsAdjustment', trim( $arrValues['specials_adjustment'] ) ); elseif( isset( $arrValues['specials_adjustment'] ) ) $this->setSpecialsAdjustment( $arrValues['specials_adjustment'] );
		if( isset( $arrValues['effective_rent'] ) && $boolDirectSet ) $this->set( 'm_fltEffectiveRent', trim( $arrValues['effective_rent'] ) ); elseif( isset( $arrValues['effective_rent'] ) ) $this->setEffectiveRent( $arrValues['effective_rent'] );
		if( isset( $arrValues['deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDepositAmount', trim( $arrValues['deposit_amount'] ) ); elseif( isset( $arrValues['deposit_amount'] ) ) $this->setDepositAmount( $arrValues['deposit_amount'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyBuildingId( $intPropertyBuildingId ) {
		$this->set( 'm_intPropertyBuildingId', CStrings::strToIntDef( $intPropertyBuildingId, NULL, false ) );
	}

	public function getPropertyBuildingId() {
		return $this->m_intPropertyBuildingId;
	}

	public function sqlPropertyBuildingId() {
		return ( true == isset( $this->m_intPropertyBuildingId ) ) ? ( string ) $this->m_intPropertyBuildingId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function setLeaseTerm( $intLeaseTerm ) {
		$this->set( 'm_intLeaseTerm', CStrings::strToIntDef( $intLeaseTerm, NULL, false ) );
	}

	public function getLeaseTerm() {
		return $this->m_intLeaseTerm;
	}

	public function sqlLeaseTerm() {
		return ( true == isset( $this->m_intLeaseTerm ) ) ? ( string ) $this->m_intLeaseTerm : 'NULL';
	}

	public function setMarketRent( $fltMarketRent ) {
		$this->set( 'm_fltMarketRent', CStrings::strToFloatDef( $fltMarketRent, NULL, false, 2 ) );
	}

	public function getMarketRent() {
		return $this->m_fltMarketRent;
	}

	public function sqlMarketRent() {
		return ( true == isset( $this->m_fltMarketRent ) ) ? ( string ) $this->m_fltMarketRent : 'NULL';
	}

	public function setBaseRent( $fltBaseRent ) {
		$this->set( 'm_fltBaseRent', CStrings::strToFloatDef( $fltBaseRent, NULL, false, 2 ) );
	}

	public function getBaseRent() {
		return $this->m_fltBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_fltBaseRent ) ) ? ( string ) $this->m_fltBaseRent : 'NULL';
	}

	public function setAmenitiesAdjustment( $fltAmenitiesAdjustment ) {
		$this->set( 'm_fltAmenitiesAdjustment', CStrings::strToFloatDef( $fltAmenitiesAdjustment, NULL, false, 2 ) );
	}

	public function getAmenitiesAdjustment() {
		return $this->m_fltAmenitiesAdjustment;
	}

	public function sqlAmenitiesAdjustment() {
		return ( true == isset( $this->m_fltAmenitiesAdjustment ) ) ? ( string ) $this->m_fltAmenitiesAdjustment : 'NULL';
	}

	public function setSpecialsAdjustment( $fltSpecialsAdjustment ) {
		$this->set( 'm_fltSpecialsAdjustment', CStrings::strToFloatDef( $fltSpecialsAdjustment, NULL, false, 2 ) );
	}

	public function getSpecialsAdjustment() {
		return $this->m_fltSpecialsAdjustment;
	}

	public function sqlSpecialsAdjustment() {
		return ( true == isset( $this->m_fltSpecialsAdjustment ) ) ? ( string ) $this->m_fltSpecialsAdjustment : 'NULL';
	}

	public function setEffectiveRent( $fltEffectiveRent ) {
		$this->set( 'm_fltEffectiveRent', CStrings::strToFloatDef( $fltEffectiveRent, NULL, false, 2 ) );
	}

	public function getEffectiveRent() {
		return $this->m_fltEffectiveRent;
	}

	public function sqlEffectiveRent() {
		return ( true == isset( $this->m_fltEffectiveRent ) ) ? ( string ) $this->m_fltEffectiveRent : 'NULL';
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->set( 'm_fltDepositAmount', CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 2 ) );
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function sqlDepositAmount() {
		return ( true == isset( $this->m_fltDepositAmount ) ) ? ( string ) $this->m_fltDepositAmount : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, property_id, property_building_id, property_unit_id, effective_date, lease_term, market_rent, base_rent, amenities_adjustment, specials_adjustment, effective_rent, deposit_amount, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyBuildingId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlEffectiveDate() . ', ' .
 						$this->sqlLeaseTerm() . ', ' .
 						$this->sqlMarketRent() . ', ' .
 						$this->sqlBaseRent() . ', ' .
 						$this->sqlAmenitiesAdjustment() . ', ' .
 						$this->sqlSpecialsAdjustment() . ', ' .
 						$this->sqlEffectiveRent() . ', ' .
 						$this->sqlDepositAmount() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; } elseif( true == array_key_exists( 'PropertyBuildingId', $this->getChangedColumns() ) ) { $strSql .= ' property_building_id = ' . $this->sqlPropertyBuildingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term = ' . $this->sqlLeaseTerm() . ','; } elseif( true == array_key_exists( 'LeaseTerm', $this->getChangedColumns() ) ) { $strSql .= ' lease_term = ' . $this->sqlLeaseTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; } elseif( true == array_key_exists( 'MarketRent', $this->getChangedColumns() ) ) { $strSql .= ' market_rent = ' . $this->sqlMarketRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenities_adjustment = ' . $this->sqlAmenitiesAdjustment() . ','; } elseif( true == array_key_exists( 'AmenitiesAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' amenities_adjustment = ' . $this->sqlAmenitiesAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' specials_adjustment = ' . $this->sqlSpecialsAdjustment() . ','; } elseif( true == array_key_exists( 'SpecialsAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' specials_adjustment = ' . $this->sqlSpecialsAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_rent = ' . $this->sqlEffectiveRent() . ','; } elseif( true == array_key_exists( 'EffectiveRent', $this->getChangedColumns() ) ) { $strSql .= ' effective_rent = ' . $this->sqlEffectiveRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; } elseif( true == array_key_exists( 'DepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'property_building_id' => $this->getPropertyBuildingId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'effective_date' => $this->getEffectiveDate(),
			'lease_term' => $this->getLeaseTerm(),
			'market_rent' => $this->getMarketRent(),
			'base_rent' => $this->getBaseRent(),
			'amenities_adjustment' => $this->getAmenitiesAdjustment(),
			'specials_adjustment' => $this->getSpecialsAdjustment(),
			'effective_rent' => $this->getEffectiveRent(),
			'deposit_amount' => $this->getDepositAmount(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>