<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFloorplanUnitTypes
 * Do not add any new functions to this class.
 */

class CBaseMktgFloorplanUnitTypes extends CEosPluralBase {

	/**
	 * @return CMktgFloorplanUnitType[]
	 */
	public static function fetchMktgFloorplanUnitTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgFloorplanUnitType', $objDatabase );
	}

	/**
	 * @return CMktgFloorplanUnitType
	 */
	public static function fetchMktgFloorplanUnitType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgFloorplanUnitType', $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_floorplan_unit_types', $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypeById( $intId, $objDatabase ) {
		return self::fetchMktgFloorplanUnitType( sprintf( 'SELECT * FROM mktg_floorplan_unit_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgFloorplanUnitTypes( sprintf( 'SELECT * FROM mktg_floorplan_unit_types WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByCompetitorPropertyId( $intCompetitorPropertyId, $objDatabase ) {
		return self::fetchMktgFloorplanUnitTypes( sprintf( 'SELECT * FROM mktg_floorplan_unit_types WHERE competitor_property_id = %d', ( int ) $intCompetitorPropertyId ), $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByCompetitorPropertyFloorplanId( $intCompetitorPropertyFloorplanId, $objDatabase ) {
		return self::fetchMktgFloorplanUnitTypes( sprintf( 'SELECT * FROM mktg_floorplan_unit_types WHERE competitor_property_floorplan_id = %d', ( int ) $intCompetitorPropertyFloorplanId ), $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByPropertyUnitTypeId( $intPropertyUnitTypeId, $objDatabase ) {
		return self::fetchMktgFloorplanUnitTypes( sprintf( 'SELECT * FROM mktg_floorplan_unit_types WHERE property_unit_type_id = %d', ( int ) $intPropertyUnitTypeId ), $objDatabase );
	}

}
?>