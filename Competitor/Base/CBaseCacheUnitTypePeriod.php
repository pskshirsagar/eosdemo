<?php

class CBaseCacheUnitTypePeriod extends CEosSingularBase {

	const TABLE_NAME = 'public.cache_unit_type_periods';

	protected $m_strEffectiveDate;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_intTotalUnitSpaceCount;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intTotalApplicationCount;
	protected $m_intRent;
	protected $m_intBedroomCount;
	protected $m_intBaseRent;
	protected $m_intAmenityRent;
	protected $m_intSpecialRent;
	protected $m_strLeaseStartWindowStartDate;
	protected $m_strLeaseStartWindowEndDate;

	public function __construct() {
		parent::__construct();

		$this->m_intTotalUnitSpaceCount = '0';
		$this->m_intTotalApplicationCount = '0';
		$this->m_intRent = '0';
		$this->m_intBedroomCount = '0';
		$this->m_intBaseRent = '0';
		$this->m_intAmenityRent = '0';
		$this->m_intSpecialRent = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['total_unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitSpaceCount', trim( $arrValues['total_unit_space_count'] ) ); elseif( isset( $arrValues['total_unit_space_count'] ) ) $this->setTotalUnitSpaceCount( $arrValues['total_unit_space_count'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['total_application_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalApplicationCount', trim( $arrValues['total_application_count'] ) ); elseif( isset( $arrValues['total_application_count'] ) ) $this->setTotalApplicationCount( $arrValues['total_application_count'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_intRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['bedroom_count'] ) && $boolDirectSet ) $this->set( 'm_intBedroomCount', trim( $arrValues['bedroom_count'] ) ); elseif( isset( $arrValues['bedroom_count'] ) ) $this->setBedroomCount( $arrValues['bedroom_count'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_intBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_intAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_intSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['lease_start_window_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartWindowStartDate', trim( $arrValues['lease_start_window_start_date'] ) ); elseif( isset( $arrValues['lease_start_window_start_date'] ) ) $this->setLeaseStartWindowStartDate( $arrValues['lease_start_window_start_date'] );
		if( isset( $arrValues['lease_start_window_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartWindowEndDate', trim( $arrValues['lease_start_window_end_date'] ) ); elseif( isset( $arrValues['lease_start_window_end_date'] ) ) $this->setLeaseStartWindowEndDate( $arrValues['lease_start_window_end_date'] );
		$this->m_boolInitialized = true;
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setTotalUnitSpaceCount( $intTotalUnitSpaceCount ) {
		$this->set( 'm_intTotalUnitSpaceCount', CStrings::strToIntDef( $intTotalUnitSpaceCount, NULL, false ) );
	}

	public function getTotalUnitSpaceCount() {
		return $this->m_intTotalUnitSpaceCount;
	}

	public function sqlTotalUnitSpaceCount() {
		return ( true == isset( $this->m_intTotalUnitSpaceCount ) ) ? ( string ) $this->m_intTotalUnitSpaceCount : '0';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setTotalApplicationCount( $intTotalApplicationCount ) {
		$this->set( 'm_intTotalApplicationCount', CStrings::strToIntDef( $intTotalApplicationCount, NULL, false ) );
	}

	public function getTotalApplicationCount() {
		return $this->m_intTotalApplicationCount;
	}

	public function sqlTotalApplicationCount() {
		return ( true == isset( $this->m_intTotalApplicationCount ) ) ? ( string ) $this->m_intTotalApplicationCount : '0';
	}

	public function setRent( $intRent ) {
		$this->set( 'm_intRent', CStrings::strToIntDef( $intRent, NULL, false ) );
	}

	public function getRent() {
		return $this->m_intRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_intRent ) ) ? ( string ) $this->m_intRent : '0';
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->set( 'm_intBedroomCount', CStrings::strToIntDef( $intBedroomCount, NULL, false ) );
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function sqlBedroomCount() {
		return ( true == isset( $this->m_intBedroomCount ) ) ? ( string ) $this->m_intBedroomCount : '0';
	}

	public function setBaseRent( $intBaseRent ) {
		$this->set( 'm_intBaseRent', CStrings::strToIntDef( $intBaseRent, NULL, false ) );
	}

	public function getBaseRent() {
		return $this->m_intBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_intBaseRent ) ) ? ( string ) $this->m_intBaseRent : '0';
	}

	public function setAmenityRent( $intAmenityRent ) {
		$this->set( 'm_intAmenityRent', CStrings::strToIntDef( $intAmenityRent, NULL, false ) );
	}

	public function getAmenityRent() {
		return $this->m_intAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_intAmenityRent ) ) ? ( string ) $this->m_intAmenityRent : '0';
	}

	public function setSpecialRent( $intSpecialRent ) {
		$this->set( 'm_intSpecialRent', CStrings::strToIntDef( $intSpecialRent, NULL, false ) );
	}

	public function getSpecialRent() {
		return $this->m_intSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_intSpecialRent ) ) ? ( string ) $this->m_intSpecialRent : '0';
	}

	public function setLeaseStartWindowStartDate( $strLeaseStartWindowStartDate ) {
		$this->set( 'm_strLeaseStartWindowStartDate', CStrings::strTrimDef( $strLeaseStartWindowStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartWindowStartDate() {
		return $this->m_strLeaseStartWindowStartDate;
	}

	public function sqlLeaseStartWindowStartDate() {
		return ( true == isset( $this->m_strLeaseStartWindowStartDate ) ) ? '\'' . $this->m_strLeaseStartWindowStartDate . '\'' : 'NULL';
	}

	public function setLeaseStartWindowEndDate( $strLeaseStartWindowEndDate ) {
		$this->set( 'm_strLeaseStartWindowEndDate', CStrings::strTrimDef( $strLeaseStartWindowEndDate, -1, NULL, true ) );
	}

	public function getLeaseStartWindowEndDate() {
		return $this->m_strLeaseStartWindowEndDate;
	}

	public function sqlLeaseStartWindowEndDate() {
		return ( true == isset( $this->m_strLeaseStartWindowEndDate ) ) ? '\'' . $this->m_strLeaseStartWindowEndDate . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'effective_date' => $this->getEffectiveDate(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'total_unit_space_count' => $this->getTotalUnitSpaceCount(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'total_application_count' => $this->getTotalApplicationCount(),
			'rent' => $this->getRent(),
			'bedroom_count' => $this->getBedroomCount(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'lease_start_window_start_date' => $this->getLeaseStartWindowStartDate(),
			'lease_start_window_end_date' => $this->getLeaseStartWindowEndDate()
		);
	}

}
?>