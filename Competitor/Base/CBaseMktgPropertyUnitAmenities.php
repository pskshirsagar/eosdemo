<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUnitAmenities
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyUnitAmenities extends CEosPluralBase {

	/**
	 * @return CMktgPropertyUnitAmenity[]
	 */
	public static function fetchMktgPropertyUnitAmenities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyUnitAmenity', $objDatabase );
	}

	/**
	 * @return CMktgPropertyUnitAmenity
	 */
	public static function fetchMktgPropertyUnitAmenity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyUnitAmenity', $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_unit_amenities', $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenityById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyUnitAmenity( sprintf( 'SELECT * FROM mktg_property_unit_amenities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenitiesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyUnitAmenities( sprintf( 'SELECT * FROM mktg_property_unit_amenities WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenitiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyUnitAmenities( sprintf( 'SELECT * FROM mktg_property_unit_amenities WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenitiesByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return self::fetchMktgPropertyUnitAmenities( sprintf( 'SELECT * FROM mktg_property_unit_amenities WHERE property_unit_id = %d', ( int ) $intPropertyUnitId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitAmenitiesByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return self::fetchMktgPropertyUnitAmenities( sprintf( 'SELECT * FROM mktg_property_unit_amenities WHERE company_media_file_id = %d', ( int ) $intCompanyMediaFileId ), $objDatabase );
	}

}
?>