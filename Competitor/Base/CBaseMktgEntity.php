<?php

class CBaseMktgEntity extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_entities';

	protected $m_intId;
	protected $m_intLeaseId;
	protected $m_intCustomerId;
	protected $m_intApplicantId;
	protected $m_intApplicationId;
	protected $m_strCompanyName;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strProvince;
	protected $m_strStateCode;
	protected $m_intAreaCode;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strMailingStreetLine1;
	protected $m_strMailingStreetLine2;
	protected $m_strMailingStreetLine3;
	protected $m_strMailingCity;
	protected $m_strMailingStateCode;
	protected $m_strMailingProvince;
	protected $m_strMailingPostalCode;
	protected $m_strPhoneNumber;
	protected $m_strEmailAddress;
	protected $m_strTaxNumberEncrypted;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strPasswordQuestion;
	protected $m_strPasswordAnswerEncrypted;
	protected $m_intDontAllowLogin;
	protected $m_intDontAcceptPayments;
	protected $m_strApprovalIpAddress;
	protected $m_strTermsApprovalIp;
	protected $m_strTermsApprovedOn;
	protected $m_strNotes;
	protected $m_intIsDisabled;
	protected $m_intActivatedBy;
	protected $m_strActivatedOn;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDontAllowLogin = '0';
		$this->m_intDontAcceptPayments = '0';
		$this->m_intIsDisabled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['area_code'] ) && $boolDirectSet ) $this->set( 'm_intAreaCode', trim( $arrValues['area_code'] ) ); elseif( isset( $arrValues['area_code'] ) ) $this->setAreaCode( $arrValues['area_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['mailing_street_line1'] ) && $boolDirectSet ) $this->set( 'm_strMailingStreetLine1', trim( stripcslashes( $arrValues['mailing_street_line1'] ) ) ); elseif( isset( $arrValues['mailing_street_line1'] ) ) $this->setMailingStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_street_line1'] ) : $arrValues['mailing_street_line1'] );
		if( isset( $arrValues['mailing_street_line2'] ) && $boolDirectSet ) $this->set( 'm_strMailingStreetLine2', trim( stripcslashes( $arrValues['mailing_street_line2'] ) ) ); elseif( isset( $arrValues['mailing_street_line2'] ) ) $this->setMailingStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_street_line2'] ) : $arrValues['mailing_street_line2'] );
		if( isset( $arrValues['mailing_street_line3'] ) && $boolDirectSet ) $this->set( 'm_strMailingStreetLine3', trim( stripcslashes( $arrValues['mailing_street_line3'] ) ) ); elseif( isset( $arrValues['mailing_street_line3'] ) ) $this->setMailingStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_street_line3'] ) : $arrValues['mailing_street_line3'] );
		if( isset( $arrValues['mailing_city'] ) && $boolDirectSet ) $this->set( 'm_strMailingCity', trim( stripcslashes( $arrValues['mailing_city'] ) ) ); elseif( isset( $arrValues['mailing_city'] ) ) $this->setMailingCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_city'] ) : $arrValues['mailing_city'] );
		if( isset( $arrValues['mailing_state_code'] ) && $boolDirectSet ) $this->set( 'm_strMailingStateCode', trim( stripcslashes( $arrValues['mailing_state_code'] ) ) ); elseif( isset( $arrValues['mailing_state_code'] ) ) $this->setMailingStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_state_code'] ) : $arrValues['mailing_state_code'] );
		if( isset( $arrValues['mailing_province'] ) && $boolDirectSet ) $this->set( 'm_strMailingProvince', trim( stripcslashes( $arrValues['mailing_province'] ) ) ); elseif( isset( $arrValues['mailing_province'] ) ) $this->setMailingProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_province'] ) : $arrValues['mailing_province'] );
		if( isset( $arrValues['mailing_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strMailingPostalCode', trim( stripcslashes( $arrValues['mailing_postal_code'] ) ) ); elseif( isset( $arrValues['mailing_postal_code'] ) ) $this->setMailingPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mailing_postal_code'] ) : $arrValues['mailing_postal_code'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( stripcslashes( $arrValues['tax_number_encrypted'] ) ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_number_encrypted'] ) : $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['password_question'] ) && $boolDirectSet ) $this->set( 'm_strPasswordQuestion', trim( stripcslashes( $arrValues['password_question'] ) ) ); elseif( isset( $arrValues['password_question'] ) ) $this->setPasswordQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_question'] ) : $arrValues['password_question'] );
		if( isset( $arrValues['password_answer_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordAnswerEncrypted', trim( stripcslashes( $arrValues['password_answer_encrypted'] ) ) ); elseif( isset( $arrValues['password_answer_encrypted'] ) ) $this->setPasswordAnswerEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_answer_encrypted'] ) : $arrValues['password_answer_encrypted'] );
		if( isset( $arrValues['dont_allow_login'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowLogin', trim( $arrValues['dont_allow_login'] ) ); elseif( isset( $arrValues['dont_allow_login'] ) ) $this->setDontAllowLogin( $arrValues['dont_allow_login'] );
		if( isset( $arrValues['dont_accept_payments'] ) && $boolDirectSet ) $this->set( 'm_intDontAcceptPayments', trim( $arrValues['dont_accept_payments'] ) ); elseif( isset( $arrValues['dont_accept_payments'] ) ) $this->setDontAcceptPayments( $arrValues['dont_accept_payments'] );
		if( isset( $arrValues['approval_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strApprovalIpAddress', trim( stripcslashes( $arrValues['approval_ip_address'] ) ) ); elseif( isset( $arrValues['approval_ip_address'] ) ) $this->setApprovalIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['approval_ip_address'] ) : $arrValues['approval_ip_address'] );
		if( isset( $arrValues['terms_approval_ip'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovalIp', trim( stripcslashes( $arrValues['terms_approval_ip'] ) ) ); elseif( isset( $arrValues['terms_approval_ip'] ) ) $this->setTermsApprovalIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['terms_approval_ip'] ) : $arrValues['terms_approval_ip'] );
		if( isset( $arrValues['terms_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strTermsApprovedOn', trim( $arrValues['terms_approved_on'] ) ); elseif( isset( $arrValues['terms_approved_on'] ) ) $this->setTermsApprovedOn( $arrValues['terms_approved_on'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['activated_by'] ) && $boolDirectSet ) $this->set( 'm_intActivatedBy', trim( $arrValues['activated_by'] ) ); elseif( isset( $arrValues['activated_by'] ) ) $this->setActivatedBy( $arrValues['activated_by'] );
		if( isset( $arrValues['activated_on'] ) && $boolDirectSet ) $this->set( 'm_strActivatedOn', trim( $arrValues['activated_on'] ) ); elseif( isset( $arrValues['activated_on'] ) ) $this->setActivatedOn( $arrValues['activated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->set( 'm_strStreetLine3', CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) );
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setAreaCode( $intAreaCode ) {
		$this->set( 'm_intAreaCode', CStrings::strToIntDef( $intAreaCode, NULL, false ) );
	}

	public function getAreaCode() {
		return $this->m_intAreaCode;
	}

	public function sqlAreaCode() {
		return ( true == isset( $this->m_intAreaCode ) ) ? ( string ) $this->m_intAreaCode : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setMailingStreetLine1( $strMailingStreetLine1 ) {
		$this->set( 'm_strMailingStreetLine1', CStrings::strTrimDef( $strMailingStreetLine1, 100, NULL, true ) );
	}

	public function getMailingStreetLine1() {
		return $this->m_strMailingStreetLine1;
	}

	public function sqlMailingStreetLine1() {
		return ( true == isset( $this->m_strMailingStreetLine1 ) ) ? '\'' . addslashes( $this->m_strMailingStreetLine1 ) . '\'' : 'NULL';
	}

	public function setMailingStreetLine2( $strMailingStreetLine2 ) {
		$this->set( 'm_strMailingStreetLine2', CStrings::strTrimDef( $strMailingStreetLine2, 100, NULL, true ) );
	}

	public function getMailingStreetLine2() {
		return $this->m_strMailingStreetLine2;
	}

	public function sqlMailingStreetLine2() {
		return ( true == isset( $this->m_strMailingStreetLine2 ) ) ? '\'' . addslashes( $this->m_strMailingStreetLine2 ) . '\'' : 'NULL';
	}

	public function setMailingStreetLine3( $strMailingStreetLine3 ) {
		$this->set( 'm_strMailingStreetLine3', CStrings::strTrimDef( $strMailingStreetLine3, 100, NULL, true ) );
	}

	public function getMailingStreetLine3() {
		return $this->m_strMailingStreetLine3;
	}

	public function sqlMailingStreetLine3() {
		return ( true == isset( $this->m_strMailingStreetLine3 ) ) ? '\'' . addslashes( $this->m_strMailingStreetLine3 ) . '\'' : 'NULL';
	}

	public function setMailingCity( $strMailingCity ) {
		$this->set( 'm_strMailingCity', CStrings::strTrimDef( $strMailingCity, 50, NULL, true ) );
	}

	public function getMailingCity() {
		return $this->m_strMailingCity;
	}

	public function sqlMailingCity() {
		return ( true == isset( $this->m_strMailingCity ) ) ? '\'' . addslashes( $this->m_strMailingCity ) . '\'' : 'NULL';
	}

	public function setMailingStateCode( $strMailingStateCode ) {
		$this->set( 'm_strMailingStateCode', CStrings::strTrimDef( $strMailingStateCode, 2, NULL, true ) );
	}

	public function getMailingStateCode() {
		return $this->m_strMailingStateCode;
	}

	public function sqlMailingStateCode() {
		return ( true == isset( $this->m_strMailingStateCode ) ) ? '\'' . addslashes( $this->m_strMailingStateCode ) . '\'' : 'NULL';
	}

	public function setMailingProvince( $strMailingProvince ) {
		$this->set( 'm_strMailingProvince', CStrings::strTrimDef( $strMailingProvince, 50, NULL, true ) );
	}

	public function getMailingProvince() {
		return $this->m_strMailingProvince;
	}

	public function sqlMailingProvince() {
		return ( true == isset( $this->m_strMailingProvince ) ) ? '\'' . addslashes( $this->m_strMailingProvince ) . '\'' : 'NULL';
	}

	public function setMailingPostalCode( $strMailingPostalCode ) {
		$this->set( 'm_strMailingPostalCode', CStrings::strTrimDef( $strMailingPostalCode, 20, NULL, true ) );
	}

	public function getMailingPostalCode() {
		return $this->m_strMailingPostalCode;
	}

	public function sqlMailingPostalCode() {
		return ( true == isset( $this->m_strMailingPostalCode ) ) ? '\'' . addslashes( $this->m_strMailingPostalCode ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setPasswordQuestion( $strPasswordQuestion ) {
		$this->set( 'm_strPasswordQuestion', CStrings::strTrimDef( $strPasswordQuestion, 2000, NULL, true ) );
	}

	public function getPasswordQuestion() {
		return $this->m_strPasswordQuestion;
	}

	public function sqlPasswordQuestion() {
		return ( true == isset( $this->m_strPasswordQuestion ) ) ? '\'' . addslashes( $this->m_strPasswordQuestion ) . '\'' : 'NULL';
	}

	public function setPasswordAnswerEncrypted( $strPasswordAnswerEncrypted ) {
		$this->set( 'm_strPasswordAnswerEncrypted', CStrings::strTrimDef( $strPasswordAnswerEncrypted, 240, NULL, true ) );
	}

	public function getPasswordAnswerEncrypted() {
		return $this->m_strPasswordAnswerEncrypted;
	}

	public function sqlPasswordAnswerEncrypted() {
		return ( true == isset( $this->m_strPasswordAnswerEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordAnswerEncrypted ) . '\'' : 'NULL';
	}

	public function setDontAllowLogin( $intDontAllowLogin ) {
		$this->set( 'm_intDontAllowLogin', CStrings::strToIntDef( $intDontAllowLogin, NULL, false ) );
	}

	public function getDontAllowLogin() {
		return $this->m_intDontAllowLogin;
	}

	public function sqlDontAllowLogin() {
		return ( true == isset( $this->m_intDontAllowLogin ) ) ? ( string ) $this->m_intDontAllowLogin : '0';
	}

	public function setDontAcceptPayments( $intDontAcceptPayments ) {
		$this->set( 'm_intDontAcceptPayments', CStrings::strToIntDef( $intDontAcceptPayments, NULL, false ) );
	}

	public function getDontAcceptPayments() {
		return $this->m_intDontAcceptPayments;
	}

	public function sqlDontAcceptPayments() {
		return ( true == isset( $this->m_intDontAcceptPayments ) ) ? ( string ) $this->m_intDontAcceptPayments : '0';
	}

	public function setApprovalIpAddress( $strApprovalIpAddress ) {
		$this->set( 'm_strApprovalIpAddress', CStrings::strTrimDef( $strApprovalIpAddress, 23, NULL, true ) );
	}

	public function getApprovalIpAddress() {
		return $this->m_strApprovalIpAddress;
	}

	public function sqlApprovalIpAddress() {
		return ( true == isset( $this->m_strApprovalIpAddress ) ) ? '\'' . addslashes( $this->m_strApprovalIpAddress ) . '\'' : 'NULL';
	}

	public function setTermsApprovalIp( $strTermsApprovalIp ) {
		$this->set( 'm_strTermsApprovalIp', CStrings::strTrimDef( $strTermsApprovalIp, 23, NULL, true ) );
	}

	public function getTermsApprovalIp() {
		return $this->m_strTermsApprovalIp;
	}

	public function sqlTermsApprovalIp() {
		return ( true == isset( $this->m_strTermsApprovalIp ) ) ? '\'' . addslashes( $this->m_strTermsApprovalIp ) . '\'' : 'NULL';
	}

	public function setTermsApprovedOn( $strTermsApprovedOn ) {
		$this->set( 'm_strTermsApprovedOn', CStrings::strTrimDef( $strTermsApprovedOn, -1, NULL, true ) );
	}

	public function getTermsApprovedOn() {
		return $this->m_strTermsApprovedOn;
	}

	public function sqlTermsApprovedOn() {
		return ( true == isset( $this->m_strTermsApprovedOn ) ) ? '\'' . $this->m_strTermsApprovedOn . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setActivatedBy( $intActivatedBy ) {
		$this->set( 'm_intActivatedBy', CStrings::strToIntDef( $intActivatedBy, NULL, false ) );
	}

	public function getActivatedBy() {
		return $this->m_intActivatedBy;
	}

	public function sqlActivatedBy() {
		return ( true == isset( $this->m_intActivatedBy ) ) ? ( string ) $this->m_intActivatedBy : 'NULL';
	}

	public function setActivatedOn( $strActivatedOn ) {
		$this->set( 'm_strActivatedOn', CStrings::strTrimDef( $strActivatedOn, -1, NULL, true ) );
	}

	public function getActivatedOn() {
		return $this->m_strActivatedOn;
	}

	public function sqlActivatedOn() {
		return ( true == isset( $this->m_strActivatedOn ) ) ? '\'' . $this->m_strActivatedOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, lease_id, customer_id, applicant_id, application_id, company_name, name_first, name_middle, name_last, street_line1, street_line2, street_line3, city, province, state_code, area_code, postal_code, country_code, mailing_street_line1, mailing_street_line2, mailing_street_line3, mailing_city, mailing_state_code, mailing_province, mailing_postal_code, phone_number, email_address, tax_number_encrypted, username, password_encrypted, password_question, password_answer_encrypted, dont_allow_login, dont_accept_payments, approval_ip_address, terms_approval_ip, terms_approved_on, notes, is_disabled, activated_by, activated_on, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlApplicantId() . ', ' .
 						$this->sqlApplicationId() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameMiddle() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlStreetLine3() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlProvince() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlAreaCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlMailingStreetLine1() . ', ' .
 						$this->sqlMailingStreetLine2() . ', ' .
 						$this->sqlMailingStreetLine3() . ', ' .
 						$this->sqlMailingCity() . ', ' .
 						$this->sqlMailingStateCode() . ', ' .
 						$this->sqlMailingProvince() . ', ' .
 						$this->sqlMailingPostalCode() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlTaxNumberEncrypted() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlPasswordQuestion() . ', ' .
 						$this->sqlPasswordAnswerEncrypted() . ', ' .
 						$this->sqlDontAllowLogin() . ', ' .
 						$this->sqlDontAcceptPayments() . ', ' .
 						$this->sqlApprovalIpAddress() . ', ' .
 						$this->sqlTermsApprovalIp() . ', ' .
 						$this->sqlTermsApprovedOn() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlActivatedBy() . ', ' .
 						$this->sqlActivatedOn() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' area_code = ' . $this->sqlAreaCode() . ','; } elseif( true == array_key_exists( 'AreaCode', $this->getChangedColumns() ) ) { $strSql .= ' area_code = ' . $this->sqlAreaCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_street_line1 = ' . $this->sqlMailingStreetLine1() . ','; } elseif( true == array_key_exists( 'MailingStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' mailing_street_line1 = ' . $this->sqlMailingStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_street_line2 = ' . $this->sqlMailingStreetLine2() . ','; } elseif( true == array_key_exists( 'MailingStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' mailing_street_line2 = ' . $this->sqlMailingStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_street_line3 = ' . $this->sqlMailingStreetLine3() . ','; } elseif( true == array_key_exists( 'MailingStreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' mailing_street_line3 = ' . $this->sqlMailingStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_city = ' . $this->sqlMailingCity() . ','; } elseif( true == array_key_exists( 'MailingCity', $this->getChangedColumns() ) ) { $strSql .= ' mailing_city = ' . $this->sqlMailingCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_state_code = ' . $this->sqlMailingStateCode() . ','; } elseif( true == array_key_exists( 'MailingStateCode', $this->getChangedColumns() ) ) { $strSql .= ' mailing_state_code = ' . $this->sqlMailingStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_province = ' . $this->sqlMailingProvince() . ','; } elseif( true == array_key_exists( 'MailingProvince', $this->getChangedColumns() ) ) { $strSql .= ' mailing_province = ' . $this->sqlMailingProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailing_postal_code = ' . $this->sqlMailingPostalCode() . ','; } elseif( true == array_key_exists( 'MailingPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' mailing_postal_code = ' . $this->sqlMailingPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; } elseif( true == array_key_exists( 'PasswordQuestion', $this->getChangedColumns() ) ) { $strSql .= ' password_question = ' . $this->sqlPasswordQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordAnswerEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_answer_encrypted = ' . $this->sqlPasswordAnswerEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; } elseif( true == array_key_exists( 'DontAllowLogin', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_login = ' . $this->sqlDontAllowLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; } elseif( true == array_key_exists( 'DontAcceptPayments', $this->getChangedColumns() ) ) { $strSql .= ' dont_accept_payments = ' . $this->sqlDontAcceptPayments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; } elseif( true == array_key_exists( 'ApprovalIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' approval_ip_address = ' . $this->sqlApprovalIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terms_approval_ip = ' . $this->sqlTermsApprovalIp() . ','; } elseif( true == array_key_exists( 'TermsApprovalIp', $this->getChangedColumns() ) ) { $strSql .= ' terms_approval_ip = ' . $this->sqlTermsApprovalIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terms_approved_on = ' . $this->sqlTermsApprovedOn() . ','; } elseif( true == array_key_exists( 'TermsApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' terms_approved_on = ' . $this->sqlTermsApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activated_by = ' . $this->sqlActivatedBy() . ','; } elseif( true == array_key_exists( 'ActivatedBy', $this->getChangedColumns() ) ) { $strSql .= ' activated_by = ' . $this->sqlActivatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; } elseif( true == array_key_exists( 'ActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' activated_on = ' . $this->sqlActivatedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'lease_id' => $this->getLeaseId(),
			'customer_id' => $this->getCustomerId(),
			'applicant_id' => $this->getApplicantId(),
			'application_id' => $this->getApplicationId(),
			'company_name' => $this->getCompanyName(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'province' => $this->getProvince(),
			'state_code' => $this->getStateCode(),
			'area_code' => $this->getAreaCode(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'mailing_street_line1' => $this->getMailingStreetLine1(),
			'mailing_street_line2' => $this->getMailingStreetLine2(),
			'mailing_street_line3' => $this->getMailingStreetLine3(),
			'mailing_city' => $this->getMailingCity(),
			'mailing_state_code' => $this->getMailingStateCode(),
			'mailing_province' => $this->getMailingProvince(),
			'mailing_postal_code' => $this->getMailingPostalCode(),
			'phone_number' => $this->getPhoneNumber(),
			'email_address' => $this->getEmailAddress(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'password_question' => $this->getPasswordQuestion(),
			'password_answer_encrypted' => $this->getPasswordAnswerEncrypted(),
			'dont_allow_login' => $this->getDontAllowLogin(),
			'dont_accept_payments' => $this->getDontAcceptPayments(),
			'approval_ip_address' => $this->getApprovalIpAddress(),
			'terms_approval_ip' => $this->getTermsApprovalIp(),
			'terms_approved_on' => $this->getTermsApprovedOn(),
			'notes' => $this->getNotes(),
			'is_disabled' => $this->getIsDisabled(),
			'activated_by' => $this->getActivatedBy(),
			'activated_on' => $this->getActivatedOn(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>