<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgUnitRates
 * Do not add any new functions to this class.
 */

class CBaseMktgUnitRates extends CEosPluralBase {

	/**
	 * @return CMktgUnitRate[]
	 */
	public static function fetchMktgUnitRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgUnitRate::class, $objDatabase );
	}

	/**
	 * @return CMktgUnitRate
	 */
	public static function fetchMktgUnitRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgUnitRate::class, $objDatabase );
	}

	public static function fetchMktgUnitRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_unit_rates', $objDatabase );
	}

	public static function fetchMktgUnitRateById( $intId, $objDatabase ) {
		return self::fetchMktgUnitRate( sprintf( 'SELECT * FROM mktg_unit_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgUnitRatesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgUnitRates( sprintf( 'SELECT * FROM mktg_unit_rates WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgUnitRatesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgUnitRates( sprintf( 'SELECT * FROM mktg_unit_rates WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgUnitRatesByPropertyBuildingId( $intPropertyBuildingId, $objDatabase ) {
		return self::fetchMktgUnitRates( sprintf( 'SELECT * FROM mktg_unit_rates WHERE property_building_id = %d', ( int ) $intPropertyBuildingId ), $objDatabase );
	}

	public static function fetchMktgUnitRatesByPropertyUnitId( $intPropertyUnitId, $objDatabase ) {
		return self::fetchMktgUnitRates( sprintf( 'SELECT * FROM mktg_unit_rates WHERE property_unit_id = %d', ( int ) $intPropertyUnitId ), $objDatabase );
	}

}
?>