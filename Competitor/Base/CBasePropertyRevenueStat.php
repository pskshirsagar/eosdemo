<?php

class CBasePropertyRevenueStat extends CEosSingularBase {

	const TABLE_NAME = 'public.property_revenue_stats';

	protected $m_intId;
	protected $m_strEffectiveDate;
	protected $m_intEntrataCid;
	protected $m_strCompanyName;
	protected $m_intEntrataPropertyId;
	protected $m_strPropertyName;
	protected $m_arrstrOccupancyTypes;
	protected $m_intCompanyUnitCount;
	protected $m_intPropertyUnitCount;
	protected $m_intEntrataPricingUnitCount;
	protected $m_intLroIntegratedUnitCount;
	protected $m_intYieldstarIntegratedUnitCount;
	protected $m_intNewLeases;
	protected $m_intNewRenewalLeases;
	protected $m_intEffectiveIncome;
	protected $m_intBaseRent;
	protected $m_intAmenityRent;
	protected $m_intSpecialRent;
	protected $m_strPropertyLatitude;
	protected $m_strPropertyLongitude;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['entrata_cid'] ) && $boolDirectSet ) $this->set( 'm_intEntrataCid', trim( $arrValues['entrata_cid'] ) ); elseif( isset( $arrValues['entrata_cid'] ) ) $this->setEntrataCid( $arrValues['entrata_cid'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['entrata_property_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataPropertyId', trim( $arrValues['entrata_property_id'] ) ); elseif( isset( $arrValues['entrata_property_id'] ) ) $this->setEntrataPropertyId( $arrValues['entrata_property_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['occupancy_types'] ) && $boolDirectSet ) $this->set( 'm_arrstrOccupancyTypes', trim( $arrValues['occupancy_types'] ) ); elseif( isset( $arrValues['occupancy_types'] ) ) $this->setOccupancyTypes( $arrValues['occupancy_types'] );
		if( isset( $arrValues['company_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUnitCount', trim( $arrValues['company_unit_count'] ) ); elseif( isset( $arrValues['company_unit_count'] ) ) $this->setCompanyUnitCount( $arrValues['company_unit_count'] );
		if( isset( $arrValues['property_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitCount', trim( $arrValues['property_unit_count'] ) ); elseif( isset( $arrValues['property_unit_count'] ) ) $this->setPropertyUnitCount( $arrValues['property_unit_count'] );
		if( isset( $arrValues['entrata_pricing_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intEntrataPricingUnitCount', trim( $arrValues['entrata_pricing_unit_count'] ) ); elseif( isset( $arrValues['entrata_pricing_unit_count'] ) ) $this->setEntrataPricingUnitCount( $arrValues['entrata_pricing_unit_count'] );
		if( isset( $arrValues['lro_integrated_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intLroIntegratedUnitCount', trim( $arrValues['lro_integrated_unit_count'] ) ); elseif( isset( $arrValues['lro_integrated_unit_count'] ) ) $this->setLroIntegratedUnitCount( $arrValues['lro_integrated_unit_count'] );
		if( isset( $arrValues['yieldstar_integrated_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intYieldstarIntegratedUnitCount', trim( $arrValues['yieldstar_integrated_unit_count'] ) ); elseif( isset( $arrValues['yieldstar_integrated_unit_count'] ) ) $this->setYieldstarIntegratedUnitCount( $arrValues['yieldstar_integrated_unit_count'] );
		if( isset( $arrValues['new_leases'] ) && $boolDirectSet ) $this->set( 'm_intNewLeases', trim( $arrValues['new_leases'] ) ); elseif( isset( $arrValues['new_leases'] ) ) $this->setNewLeases( $arrValues['new_leases'] );
		if( isset( $arrValues['new_renewal_leases'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalLeases', trim( $arrValues['new_renewal_leases'] ) ); elseif( isset( $arrValues['new_renewal_leases'] ) ) $this->setNewRenewalLeases( $arrValues['new_renewal_leases'] );
		if( isset( $arrValues['effective_income'] ) && $boolDirectSet ) $this->set( 'm_intEffectiveIncome', trim( $arrValues['effective_income'] ) ); elseif( isset( $arrValues['effective_income'] ) ) $this->setEffectiveIncome( $arrValues['effective_income'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_intBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_intAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_intSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['property_latitude'] ) && $boolDirectSet ) $this->set( 'm_strPropertyLatitude', trim( stripcslashes( $arrValues['property_latitude'] ) ) ); elseif( isset( $arrValues['property_latitude'] ) ) $this->setPropertyLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_latitude'] ) : $arrValues['property_latitude'] );
		if( isset( $arrValues['property_longitude'] ) && $boolDirectSet ) $this->set( 'm_strPropertyLongitude', trim( stripcslashes( $arrValues['property_longitude'] ) ) ); elseif( isset( $arrValues['property_longitude'] ) ) $this->setPropertyLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_longitude'] ) : $arrValues['property_longitude'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setEntrataCid( $intEntrataCid ) {
		$this->set( 'm_intEntrataCid', CStrings::strToIntDef( $intEntrataCid, NULL, false ) );
	}

	public function getEntrataCid() {
		return $this->m_intEntrataCid;
	}

	public function sqlEntrataCid() {
		return ( true == isset( $this->m_intEntrataCid ) ) ? ( string ) $this->m_intEntrataCid : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setEntrataPropertyId( $intEntrataPropertyId ) {
		$this->set( 'm_intEntrataPropertyId', CStrings::strToIntDef( $intEntrataPropertyId, NULL, false ) );
	}

	public function getEntrataPropertyId() {
		return $this->m_intEntrataPropertyId;
	}

	public function sqlEntrataPropertyId() {
		return ( true == isset( $this->m_intEntrataPropertyId ) ) ? ( string ) $this->m_intEntrataPropertyId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setOccupancyTypes( $arrstrOccupancyTypes ) {
		$this->set( 'm_arrstrOccupancyTypes', CStrings::strToArrIntDef( $arrstrOccupancyTypes, NULL ) );
	}

	public function getOccupancyTypes() {
		return $this->m_arrstrOccupancyTypes;
	}

	public function sqlOccupancyTypes() {
		return ( true == isset( $this->m_arrstrOccupancyTypes ) && true == valArr( $this->m_arrstrOccupancyTypes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrOccupancyTypes, NULL ) . '\'' : 'NULL';
	}

	public function setCompanyUnitCount( $intCompanyUnitCount ) {
		$this->set( 'm_intCompanyUnitCount', CStrings::strToIntDef( $intCompanyUnitCount, NULL, false ) );
	}

	public function getCompanyUnitCount() {
		return $this->m_intCompanyUnitCount;
	}

	public function sqlCompanyUnitCount() {
		return ( true == isset( $this->m_intCompanyUnitCount ) ) ? ( string ) $this->m_intCompanyUnitCount : 'NULL';
	}

	public function setPropertyUnitCount( $intPropertyUnitCount ) {
		$this->set( 'm_intPropertyUnitCount', CStrings::strToIntDef( $intPropertyUnitCount, NULL, false ) );
	}

	public function getPropertyUnitCount() {
		return $this->m_intPropertyUnitCount;
	}

	public function sqlPropertyUnitCount() {
		return ( true == isset( $this->m_intPropertyUnitCount ) ) ? ( string ) $this->m_intPropertyUnitCount : 'NULL';
	}

	public function setEntrataPricingUnitCount( $intEntrataPricingUnitCount ) {
		$this->set( 'm_intEntrataPricingUnitCount', CStrings::strToIntDef( $intEntrataPricingUnitCount, NULL, false ) );
	}

	public function getEntrataPricingUnitCount() {
		return $this->m_intEntrataPricingUnitCount;
	}

	public function sqlEntrataPricingUnitCount() {
		return ( true == isset( $this->m_intEntrataPricingUnitCount ) ) ? ( string ) $this->m_intEntrataPricingUnitCount : 'NULL';
	}

	public function setLroIntegratedUnitCount( $intLroIntegratedUnitCount ) {
		$this->set( 'm_intLroIntegratedUnitCount', CStrings::strToIntDef( $intLroIntegratedUnitCount, NULL, false ) );
	}

	public function getLroIntegratedUnitCount() {
		return $this->m_intLroIntegratedUnitCount;
	}

	public function sqlLroIntegratedUnitCount() {
		return ( true == isset( $this->m_intLroIntegratedUnitCount ) ) ? ( string ) $this->m_intLroIntegratedUnitCount : 'NULL';
	}

	public function setYieldstarIntegratedUnitCount( $intYieldstarIntegratedUnitCount ) {
		$this->set( 'm_intYieldstarIntegratedUnitCount', CStrings::strToIntDef( $intYieldstarIntegratedUnitCount, NULL, false ) );
	}

	public function getYieldstarIntegratedUnitCount() {
		return $this->m_intYieldstarIntegratedUnitCount;
	}

	public function sqlYieldstarIntegratedUnitCount() {
		return ( true == isset( $this->m_intYieldstarIntegratedUnitCount ) ) ? ( string ) $this->m_intYieldstarIntegratedUnitCount : 'NULL';
	}

	public function setNewLeases( $intNewLeases ) {
		$this->set( 'm_intNewLeases', CStrings::strToIntDef( $intNewLeases, NULL, false ) );
	}

	public function getNewLeases() {
		return $this->m_intNewLeases;
	}

	public function sqlNewLeases() {
		return ( true == isset( $this->m_intNewLeases ) ) ? ( string ) $this->m_intNewLeases : 'NULL';
	}

	public function setNewRenewalLeases( $intNewRenewalLeases ) {
		$this->set( 'm_intNewRenewalLeases', CStrings::strToIntDef( $intNewRenewalLeases, NULL, false ) );
	}

	public function getNewRenewalLeases() {
		return $this->m_intNewRenewalLeases;
	}

	public function sqlNewRenewalLeases() {
		return ( true == isset( $this->m_intNewRenewalLeases ) ) ? ( string ) $this->m_intNewRenewalLeases : 'NULL';
	}

	public function setEffectiveIncome( $intEffectiveIncome ) {
		$this->set( 'm_intEffectiveIncome', CStrings::strToIntDef( $intEffectiveIncome, NULL, false ) );
	}

	public function getEffectiveIncome() {
		return $this->m_intEffectiveIncome;
	}

	public function sqlEffectiveIncome() {
		return ( true == isset( $this->m_intEffectiveIncome ) ) ? ( string ) $this->m_intEffectiveIncome : 'NULL';
	}

	public function setBaseRent( $intBaseRent ) {
		$this->set( 'm_intBaseRent', CStrings::strToIntDef( $intBaseRent, NULL, false ) );
	}

	public function getBaseRent() {
		return $this->m_intBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_intBaseRent ) ) ? ( string ) $this->m_intBaseRent : 'NULL';
	}

	public function setAmenityRent( $intAmenityRent ) {
		$this->set( 'm_intAmenityRent', CStrings::strToIntDef( $intAmenityRent, NULL, false ) );
	}

	public function getAmenityRent() {
		return $this->m_intAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_intAmenityRent ) ) ? ( string ) $this->m_intAmenityRent : 'NULL';
	}

	public function setSpecialRent( $intSpecialRent ) {
		$this->set( 'm_intSpecialRent', CStrings::strToIntDef( $intSpecialRent, NULL, false ) );
	}

	public function getSpecialRent() {
		return $this->m_intSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_intSpecialRent ) ) ? ( string ) $this->m_intSpecialRent : 'NULL';
	}

	public function setPropertyLatitude( $strPropertyLatitude ) {
		$this->set( 'm_strPropertyLatitude', CStrings::strTrimDef( $strPropertyLatitude, 25, NULL, true ) );
	}

	public function getPropertyLatitude() {
		return $this->m_strPropertyLatitude;
	}

	public function sqlPropertyLatitude() {
		return ( true == isset( $this->m_strPropertyLatitude ) ) ? '\'' . addslashes( $this->m_strPropertyLatitude ) . '\'' : 'NULL';
	}

	public function setPropertyLongitude( $strPropertyLongitude ) {
		$this->set( 'm_strPropertyLongitude', CStrings::strTrimDef( $strPropertyLongitude, 25, NULL, true ) );
	}

	public function getPropertyLongitude() {
		return $this->m_strPropertyLongitude;
	}

	public function sqlPropertyLongitude() {
		return ( true == isset( $this->m_strPropertyLongitude ) ) ? '\'' . addslashes( $this->m_strPropertyLongitude ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, effective_date, entrata_cid, company_name, entrata_property_id, property_name, occupancy_types, company_unit_count, property_unit_count, entrata_pricing_unit_count, lro_integrated_unit_count, yieldstar_integrated_unit_count, new_leases, new_renewal_leases, effective_income, base_rent, amenity_rent, special_rent, property_latitude, property_longitude, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlEntrataCid() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlEntrataPropertyId() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlOccupancyTypes() . ', ' .
						$this->sqlCompanyUnitCount() . ', ' .
						$this->sqlPropertyUnitCount() . ', ' .
						$this->sqlEntrataPricingUnitCount() . ', ' .
						$this->sqlLroIntegratedUnitCount() . ', ' .
						$this->sqlYieldstarIntegratedUnitCount() . ', ' .
						$this->sqlNewLeases() . ', ' .
						$this->sqlNewRenewalLeases() . ', ' .
						$this->sqlEffectiveIncome() . ', ' .
						$this->sqlBaseRent() . ', ' .
						$this->sqlAmenityRent() . ', ' .
						$this->sqlSpecialRent() . ', ' .
						$this->sqlPropertyLatitude() . ', ' .
						$this->sqlPropertyLongitude() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_cid = ' . $this->sqlEntrataCid(). ',' ; } elseif( true == array_key_exists( 'EntrataCid', $this->getChangedColumns() ) ) { $strSql .= ' entrata_cid = ' . $this->sqlEntrataCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_property_id = ' . $this->sqlEntrataPropertyId(). ',' ; } elseif( true == array_key_exists( 'EntrataPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' entrata_property_id = ' . $this->sqlEntrataPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_types = ' . $this->sqlOccupancyTypes(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypes', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_types = ' . $this->sqlOccupancyTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_unit_count = ' . $this->sqlCompanyUnitCount(). ',' ; } elseif( true == array_key_exists( 'CompanyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' company_unit_count = ' . $this->sqlCompanyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_pricing_unit_count = ' . $this->sqlEntrataPricingUnitCount(). ',' ; } elseif( true == array_key_exists( 'EntrataPricingUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_pricing_unit_count = ' . $this->sqlEntrataPricingUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lro_integrated_unit_count = ' . $this->sqlLroIntegratedUnitCount(). ',' ; } elseif( true == array_key_exists( 'LroIntegratedUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' lro_integrated_unit_count = ' . $this->sqlLroIntegratedUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' yieldstar_integrated_unit_count = ' . $this->sqlYieldstarIntegratedUnitCount(). ',' ; } elseif( true == array_key_exists( 'YieldstarIntegratedUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' yieldstar_integrated_unit_count = ' . $this->sqlYieldstarIntegratedUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_leases = ' . $this->sqlNewLeases(). ',' ; } elseif( true == array_key_exists( 'NewLeases', $this->getChangedColumns() ) ) { $strSql .= ' new_leases = ' . $this->sqlNewLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_leases = ' . $this->sqlNewRenewalLeases(). ',' ; } elseif( true == array_key_exists( 'NewRenewalLeases', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_leases = ' . $this->sqlNewRenewalLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_income = ' . $this->sqlEffectiveIncome(). ',' ; } elseif( true == array_key_exists( 'EffectiveIncome', $this->getChangedColumns() ) ) { $strSql .= ' effective_income = ' . $this->sqlEffectiveIncome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent(). ',' ; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_latitude = ' . $this->sqlPropertyLatitude(). ',' ; } elseif( true == array_key_exists( 'PropertyLatitude', $this->getChangedColumns() ) ) { $strSql .= ' property_latitude = ' . $this->sqlPropertyLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_longitude = ' . $this->sqlPropertyLongitude(). ',' ; } elseif( true == array_key_exists( 'PropertyLongitude', $this->getChangedColumns() ) ) { $strSql .= ' property_longitude = ' . $this->sqlPropertyLongitude() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'effective_date' => $this->getEffectiveDate(),
			'entrata_cid' => $this->getEntrataCid(),
			'company_name' => $this->getCompanyName(),
			'entrata_property_id' => $this->getEntrataPropertyId(),
			'property_name' => $this->getPropertyName(),
			'occupancy_types' => $this->getOccupancyTypes(),
			'company_unit_count' => $this->getCompanyUnitCount(),
			'property_unit_count' => $this->getPropertyUnitCount(),
			'entrata_pricing_unit_count' => $this->getEntrataPricingUnitCount(),
			'lro_integrated_unit_count' => $this->getLroIntegratedUnitCount(),
			'yieldstar_integrated_unit_count' => $this->getYieldstarIntegratedUnitCount(),
			'new_leases' => $this->getNewLeases(),
			'new_renewal_leases' => $this->getNewRenewalLeases(),
			'effective_income' => $this->getEffectiveIncome(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'property_latitude' => $this->getPropertyLatitude(),
			'property_longitude' => $this->getPropertyLongitude(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>