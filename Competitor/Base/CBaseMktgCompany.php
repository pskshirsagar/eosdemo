<?php

class CBaseMktgCompany extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_companies';

	protected $m_intId;
	protected $m_intRemotePrimaryKey;
	protected $m_intEntityId;
	protected $m_intDatabaseId;
	protected $m_strCompanyStatusType;
	protected $m_strCompanyName;
	protected $m_strCompanyNumber;
	protected $m_strStreetLine1;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strLongitude;
	protected $m_strLatitude;
	protected $m_strRwxDomain;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDatabaseId = '2';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_intRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['company_status_type'] ) && $boolDirectSet ) $this->set( 'm_strCompanyStatusType', trim( stripcslashes( $arrValues['company_status_type'] ) ) ); elseif( isset( $arrValues['company_status_type'] ) ) $this->setCompanyStatusType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_status_type'] ) : $arrValues['company_status_type'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['company_number'] ) && $boolDirectSet ) $this->set( 'm_strCompanyNumber', trim( stripcslashes( $arrValues['company_number'] ) ) ); elseif( isset( $arrValues['company_number'] ) ) $this->setCompanyNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_number'] ) : $arrValues['company_number'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( stripcslashes( $arrValues['longitude'] ) ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['longitude'] ) : $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( stripcslashes( $arrValues['latitude'] ) ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latitude'] ) : $arrValues['latitude'] );
		if( isset( $arrValues['rwx_domain'] ) && $boolDirectSet ) $this->set( 'm_strRwxDomain', trim( stripcslashes( $arrValues['rwx_domain'] ) ) ); elseif( isset( $arrValues['rwx_domain'] ) ) $this->setRwxDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rwx_domain'] ) : $arrValues['rwx_domain'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->set( 'm_intRemotePrimaryKey', CStrings::strToIntDef( $intRemotePrimaryKey, NULL, false ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_intRemotePrimaryKey ) ) ? ( string ) $this->m_intRemotePrimaryKey : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : '2';
	}

	public function setCompanyStatusType( $strCompanyStatusType ) {
		$this->set( 'm_strCompanyStatusType', CStrings::strTrimDef( $strCompanyStatusType, 50, NULL, true ) );
	}

	public function getCompanyStatusType() {
		return $this->m_strCompanyStatusType;
	}

	public function sqlCompanyStatusType() {
		return ( true == isset( $this->m_strCompanyStatusType ) ) ? '\'' . addslashes( $this->m_strCompanyStatusType ) . '\'' : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setCompanyNumber( $strCompanyNumber ) {
		$this->set( 'm_strCompanyNumber', CStrings::strTrimDef( $strCompanyNumber, 7, NULL, true ) );
	}

	public function getCompanyNumber() {
		return $this->m_strCompanyNumber;
	}

	public function sqlCompanyNumber() {
		return ( true == isset( $this->m_strCompanyNumber ) ) ? '\'' . addslashes( $this->m_strCompanyNumber ) . '\'' : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 255, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? '\'' . addslashes( $this->m_strLongitude ) . '\'' : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 255, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? '\'' . addslashes( $this->m_strLatitude ) . '\'' : 'NULL';
	}

	public function setRwxDomain( $strRwxDomain ) {
		$this->set( 'm_strRwxDomain', CStrings::strTrimDef( $strRwxDomain, 240, NULL, true ) );
	}

	public function getRwxDomain() {
		return $this->m_strRwxDomain;
	}

	public function sqlRwxDomain() {
		return ( true == isset( $this->m_strRwxDomain ) ) ? '\'' . addslashes( $this->m_strRwxDomain ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, entity_id, database_id, company_status_type, company_name, company_number, street_line1, city, state_code, postal_code, country_code, longitude, latitude, rwx_domain, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlEntityId() . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlCompanyStatusType() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlCompanyNumber() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlLongitude() . ', ' .
 						$this->sqlLatitude() . ', ' .
 						$this->sqlRwxDomain() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_type = ' . $this->sqlCompanyStatusType() . ','; } elseif( true == array_key_exists( 'CompanyStatusType', $this->getChangedColumns() ) ) { $strSql .= ' company_status_type = ' . $this->sqlCompanyStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_number = ' . $this->sqlCompanyNumber() . ','; } elseif( true == array_key_exists( 'CompanyNumber', $this->getChangedColumns() ) ) { $strSql .= ' company_number = ' . $this->sqlCompanyNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rwx_domain = ' . $this->sqlRwxDomain() . ','; } elseif( true == array_key_exists( 'RwxDomain', $this->getChangedColumns() ) ) { $strSql .= ' rwx_domain = ' . $this->sqlRwxDomain() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'entity_id' => $this->getEntityId(),
			'database_id' => $this->getDatabaseId(),
			'company_status_type' => $this->getCompanyStatusType(),
			'company_name' => $this->getCompanyName(),
			'company_number' => $this->getCompanyNumber(),
			'street_line1' => $this->getStreetLine1(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'rwx_domain' => $this->getRwxDomain(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>