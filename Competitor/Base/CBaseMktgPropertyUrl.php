<?php

class CBaseMktgPropertyUrl extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_urls';

	protected $m_intId;
	protected $m_intPropertyId;
	protected $m_strUrl;
	protected $m_intPriority;
	protected $m_strUrlType;
	protected $m_strScraperType;
	protected $m_intIsExtractReady;
	protected $m_intIsRating;
	protected $m_strLastExtractedOn;
	protected $m_intFailCount;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intPriority = '1';
		$this->m_strUrlType = 'unknown';
		$this->m_strScraperType = 'unknown';
		$this->m_intIsExtractReady = '0';
		$this->m_intIsRating = '0';
		$this->m_intFailCount = ( 0 );

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( $arrValues['url'] ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( $arrValues['url'] );
		if( isset( $arrValues['priority'] ) && $boolDirectSet ) $this->set( 'm_intPriority', trim( $arrValues['priority'] ) ); elseif( isset( $arrValues['priority'] ) ) $this->setPriority( $arrValues['priority'] );
		if( isset( $arrValues['url_type'] ) && $boolDirectSet ) $this->set( 'm_strUrlType', trim( $arrValues['url_type'] ) ); elseif( isset( $arrValues['url_type'] ) ) $this->setUrlType( $arrValues['url_type'] );
		if( isset( $arrValues['scraper_type'] ) && $boolDirectSet ) $this->set( 'm_strScraperType', trim( $arrValues['scraper_type'] ) ); elseif( isset( $arrValues['scraper_type'] ) ) $this->setScraperType( $arrValues['scraper_type'] );
		if( isset( $arrValues['is_extract_ready'] ) && $boolDirectSet ) $this->set( 'm_intIsExtractReady', trim( $arrValues['is_extract_ready'] ) ); elseif( isset( $arrValues['is_extract_ready'] ) ) $this->setIsExtractReady( $arrValues['is_extract_ready'] );
		if( isset( $arrValues['is_rating'] ) && $boolDirectSet ) $this->set( 'm_intIsRating', trim( $arrValues['is_rating'] ) ); elseif( isset( $arrValues['is_rating'] ) ) $this->setIsRating( $arrValues['is_rating'] );
		if( isset( $arrValues['last_extracted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastExtractedOn', trim( $arrValues['last_extracted_on'] ) ); elseif( isset( $arrValues['last_extracted_on'] ) ) $this->setLastExtractedOn( $arrValues['last_extracted_on'] );
		if( isset( $arrValues['fail_count'] ) && $boolDirectSet ) $this->set( 'm_intFailCount', trim( $arrValues['fail_count'] ) ); elseif( isset( $arrValues['fail_count'] ) ) $this->setFailCount( $arrValues['fail_count'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 2049, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrl ) : '\'' . addslashes( $this->m_strUrl ) . '\'' ) : 'NULL';
	}

	public function setPriority( $intPriority ) {
		$this->set( 'm_intPriority', CStrings::strToIntDef( $intPriority, NULL, false ) );
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function sqlPriority() {
		return ( true == isset( $this->m_intPriority ) ) ? ( string ) $this->m_intPriority : '1';
	}

	public function setUrlType( $strUrlType ) {
		$this->set( 'm_strUrlType', CStrings::strTrimDef( $strUrlType, 50, NULL, true ) );
	}

	public function getUrlType() {
		return $this->m_strUrlType;
	}

	public function sqlUrlType() {
		return ( true == isset( $this->m_strUrlType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrlType ) : '\'' . addslashes( $this->m_strUrlType ) . '\'' ) : '\'unknown\'';
	}

	public function setScraperType( $strScraperType ) {
		$this->set( 'm_strScraperType', CStrings::strTrimDef( $strScraperType, 50, NULL, true ) );
	}

	public function getScraperType() {
		return $this->m_strScraperType;
	}

	public function sqlScraperType() {
		return ( true == isset( $this->m_strScraperType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strScraperType ) : '\'' . addslashes( $this->m_strScraperType ) . '\'' ) : '\'unknown\'';
	}

	public function setIsExtractReady( $intIsExtractReady ) {
		$this->set( 'm_intIsExtractReady', CStrings::strToIntDef( $intIsExtractReady, NULL, false ) );
	}

	public function getIsExtractReady() {
		return $this->m_intIsExtractReady;
	}

	public function sqlIsExtractReady() {
		return ( true == isset( $this->m_intIsExtractReady ) ) ? ( string ) $this->m_intIsExtractReady : '0';
	}

	public function setIsRating( $intIsRating ) {
		$this->set( 'm_intIsRating', CStrings::strToIntDef( $intIsRating, NULL, false ) );
	}

	public function getIsRating() {
		return $this->m_intIsRating;
	}

	public function sqlIsRating() {
		return ( true == isset( $this->m_intIsRating ) ) ? ( string ) $this->m_intIsRating : '0';
	}

	public function setLastExtractedOn( $strLastExtractedOn ) {
		$this->set( 'm_strLastExtractedOn', CStrings::strTrimDef( $strLastExtractedOn, -1, NULL, true ) );
	}

	public function getLastExtractedOn() {
		return $this->m_strLastExtractedOn;
	}

	public function sqlLastExtractedOn() {
		return ( true == isset( $this->m_strLastExtractedOn ) ) ? '\'' . $this->m_strLastExtractedOn . '\'' : 'NULL';
	}

	public function setFailCount( $intFailCount ) {
		$this->set( 'm_intFailCount', CStrings::strToIntDef( $intFailCount, NULL, false ) );
	}

	public function getFailCount() {
		return $this->m_intFailCount;
	}

	public function sqlFailCount() {
		return ( true == isset( $this->m_intFailCount ) ) ? ( string ) $this->m_intFailCount : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_id, url, priority, url_type, scraper_type, is_extract_ready, is_rating, last_extracted_on, fail_count, updated_on, created_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlPriority() . ', ' .
						$this->sqlUrlType() . ', ' .
						$this->sqlScraperType() . ', ' .
						$this->sqlIsExtractReady() . ', ' .
						$this->sqlIsRating() . ', ' .
						$this->sqlLastExtractedOn() . ', ' .
						$this->sqlFailCount() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority = ' . $this->sqlPriority(). ',' ; } elseif( true == array_key_exists( 'Priority', $this->getChangedColumns() ) ) { $strSql .= ' priority = ' . $this->sqlPriority() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url_type = ' . $this->sqlUrlType(). ',' ; } elseif( true == array_key_exists( 'UrlType', $this->getChangedColumns() ) ) { $strSql .= ' url_type = ' . $this->sqlUrlType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scraper_type = ' . $this->sqlScraperType(). ',' ; } elseif( true == array_key_exists( 'ScraperType', $this->getChangedColumns() ) ) { $strSql .= ' scraper_type = ' . $this->sqlScraperType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_extract_ready = ' . $this->sqlIsExtractReady(). ',' ; } elseif( true == array_key_exists( 'IsExtractReady', $this->getChangedColumns() ) ) { $strSql .= ' is_extract_ready = ' . $this->sqlIsExtractReady() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_rating = ' . $this->sqlIsRating(). ',' ; } elseif( true == array_key_exists( 'IsRating', $this->getChangedColumns() ) ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_extracted_on = ' . $this->sqlLastExtractedOn(). ',' ; } elseif( true == array_key_exists( 'LastExtractedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_extracted_on = ' . $this->sqlLastExtractedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fail_count = ' . $this->sqlFailCount(). ',' ; } elseif( true == array_key_exists( 'FailCount', $this->getChangedColumns() ) ) { $strSql .= ' fail_count = ' . $this->sqlFailCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_id' => $this->getPropertyId(),
			'url' => $this->getUrl(),
			'priority' => $this->getPriority(),
			'url_type' => $this->getUrlType(),
			'scraper_type' => $this->getScraperType(),
			'is_extract_ready' => $this->getIsExtractReady(),
			'is_rating' => $this->getIsRating(),
			'last_extracted_on' => $this->getLastExtractedOn(),
			'fail_count' => $this->getFailCount(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>