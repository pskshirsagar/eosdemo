<?php

class CBaseMktgPropertyCompetitor extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.mktg_property_competitors';

	protected $m_intId;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_intCompetitorPropertyId;
	protected $m_fltDriveTime;
	protected $m_fltDriveDistance;
	protected $m_fltPreleaseOccupancy;
	protected $m_fltCurrentOccupancy;
	protected $m_intPriority;
	protected $m_strDeletedOn;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intPriority = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['competitor_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyId', trim( $arrValues['competitor_property_id'] ) ); elseif( isset( $arrValues['competitor_property_id'] ) ) $this->setCompetitorPropertyId( $arrValues['competitor_property_id'] );
		if( isset( $arrValues['drive_time'] ) && $boolDirectSet ) $this->set( 'm_fltDriveTime', trim( $arrValues['drive_time'] ) ); elseif( isset( $arrValues['drive_time'] ) ) $this->setDriveTime( $arrValues['drive_time'] );
		if( isset( $arrValues['drive_distance'] ) && $boolDirectSet ) $this->set( 'm_fltDriveDistance', trim( $arrValues['drive_distance'] ) ); elseif( isset( $arrValues['drive_distance'] ) ) $this->setDriveDistance( $arrValues['drive_distance'] );
		if( isset( $arrValues['prelease_occupancy'] ) && $boolDirectSet ) $this->set( 'm_fltPreleaseOccupancy', trim( $arrValues['prelease_occupancy'] ) ); elseif( isset( $arrValues['prelease_occupancy'] ) ) $this->setPreleaseOccupancy( $arrValues['prelease_occupancy'] );
		if( isset( $arrValues['current_occupancy'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentOccupancy', trim( $arrValues['current_occupancy'] ) ); elseif( isset( $arrValues['current_occupancy'] ) ) $this->setCurrentOccupancy( $arrValues['current_occupancy'] );
		if( isset( $arrValues['priority'] ) && $boolDirectSet ) $this->set( 'm_intPriority', trim( $arrValues['priority'] ) ); elseif( isset( $arrValues['priority'] ) ) $this->setPriority( $arrValues['priority'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompetitorPropertyId( $intCompetitorPropertyId ) {
		$this->set( 'm_intCompetitorPropertyId', CStrings::strToIntDef( $intCompetitorPropertyId, NULL, false ) );
	}

	public function getCompetitorPropertyId() {
		return $this->m_intCompetitorPropertyId;
	}

	public function sqlCompetitorPropertyId() {
		return ( true == isset( $this->m_intCompetitorPropertyId ) ) ? ( string ) $this->m_intCompetitorPropertyId : 'NULL';
	}

	public function setDriveTime( $fltDriveTime ) {
		$this->set( 'm_fltDriveTime', CStrings::strToFloatDef( $fltDriveTime, NULL, false, 4 ) );
	}

	public function getDriveTime() {
		return $this->m_fltDriveTime;
	}

	public function sqlDriveTime() {
		return ( true == isset( $this->m_fltDriveTime ) ) ? ( string ) $this->m_fltDriveTime : 'NULL';
	}

	public function setDriveDistance( $fltDriveDistance ) {
		$this->set( 'm_fltDriveDistance', CStrings::strToFloatDef( $fltDriveDistance, NULL, false, 4 ) );
	}

	public function getDriveDistance() {
		return $this->m_fltDriveDistance;
	}

	public function sqlDriveDistance() {
		return ( true == isset( $this->m_fltDriveDistance ) ) ? ( string ) $this->m_fltDriveDistance : 'NULL';
	}

	public function setPreleaseOccupancy( $fltPreleaseOccupancy ) {
		$this->set( 'm_fltPreleaseOccupancy', CStrings::strToFloatDef( $fltPreleaseOccupancy, NULL, false, 4 ) );
	}

	public function getPreleaseOccupancy() {
		return $this->m_fltPreleaseOccupancy;
	}

	public function sqlPreleaseOccupancy() {
		return ( true == isset( $this->m_fltPreleaseOccupancy ) ) ? ( string ) $this->m_fltPreleaseOccupancy : 'NULL';
	}

	public function setCurrentOccupancy( $fltCurrentOccupancy ) {
		$this->set( 'm_fltCurrentOccupancy', CStrings::strToFloatDef( $fltCurrentOccupancy, NULL, false, 4 ) );
	}

	public function getCurrentOccupancy() {
		return $this->m_fltCurrentOccupancy;
	}

	public function sqlCurrentOccupancy() {
		return ( true == isset( $this->m_fltCurrentOccupancy ) ) ? ( string ) $this->m_fltCurrentOccupancy : 'NULL';
	}

	public function setPriority( $intPriority ) {
		$this->set( 'm_intPriority', CStrings::strToIntDef( $intPriority, NULL, false ) );
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function sqlPriority() {
		return ( true == isset( $this->m_intPriority ) ) ? ( string ) $this->m_intPriority : '0';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_id, property_id, competitor_property_id, drive_time, drive_distance, prelease_occupancy, current_occupancy, priority, deleted_on, created_on, updated_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCompanyId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCompetitorPropertyId() . ', ' .
						$this->sqlDriveTime() . ', ' .
						$this->sqlDriveDistance() . ', ' .
						$this->sqlPreleaseOccupancy() . ', ' .
						$this->sqlCurrentOccupancy() . ', ' .
						$this->sqlPriority() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId(). ',' ; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId(). ',' ; } elseif( true == array_key_exists( 'CompetitorPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' drive_time = ' . $this->sqlDriveTime(). ',' ; } elseif( true == array_key_exists( 'DriveTime', $this->getChangedColumns() ) ) { $strSql .= ' drive_time = ' . $this->sqlDriveTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' drive_distance = ' . $this->sqlDriveDistance(). ',' ; } elseif( true == array_key_exists( 'DriveDistance', $this->getChangedColumns() ) ) { $strSql .= ' drive_distance = ' . $this->sqlDriveDistance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prelease_occupancy = ' . $this->sqlPreleaseOccupancy(). ',' ; } elseif( true == array_key_exists( 'PreleaseOccupancy', $this->getChangedColumns() ) ) { $strSql .= ' prelease_occupancy = ' . $this->sqlPreleaseOccupancy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_occupancy = ' . $this->sqlCurrentOccupancy(). ',' ; } elseif( true == array_key_exists( 'CurrentOccupancy', $this->getChangedColumns() ) ) { $strSql .= ' current_occupancy = ' . $this->sqlCurrentOccupancy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority = ' . $this->sqlPriority(). ',' ; } elseif( true == array_key_exists( 'Priority', $this->getChangedColumns() ) ) { $strSql .= ' priority = ' . $this->sqlPriority() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'competitor_property_id' => $this->getCompetitorPropertyId(),
			'drive_time' => $this->getDriveTime(),
			'drive_distance' => $this->getDriveDistance(),
			'prelease_occupancy' => $this->getPreleaseOccupancy(),
			'current_occupancy' => $this->getCurrentOccupancy(),
			'priority' => $this->getPriority(),
			'deleted_on' => $this->getDeletedOn(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>