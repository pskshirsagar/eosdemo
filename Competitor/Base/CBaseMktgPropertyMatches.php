<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyMatches
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyMatches extends CEosPluralBase {

	/**
	 * @return CMktgPropertyMatch[]
	 */
	public static function fetchMktgPropertyMatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgPropertyMatch::class, $objDatabase );
	}

	/**
	 * @return CMktgPropertyMatch
	 */
	public static function fetchMktgPropertyMatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgPropertyMatch::class, $objDatabase );
	}

	public static function fetchMktgPropertyMatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_matches', $objDatabase );
	}

	public static function fetchMktgPropertyMatchById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyMatch( sprintf( 'SELECT * FROM mktg_property_matches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyMatchesByExternalPrimaryId( $strExternalPrimaryId, $objDatabase ) {
		return self::fetchMktgPropertyMatches( sprintf( 'SELECT * FROM mktg_property_matches WHERE external_primary_id = \'%s\'', $strExternalPrimaryId ), $objDatabase );
	}

	public static function fetchMktgPropertyMatchesByMatchId( $intMatchId, $objDatabase ) {
		return self::fetchMktgPropertyMatches( sprintf( 'SELECT * FROM mktg_property_matches WHERE match_id = %d', ( int ) $intMatchId ), $objDatabase );
	}

}
?>