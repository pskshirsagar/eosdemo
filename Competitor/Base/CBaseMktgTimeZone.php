<?php

class CBaseMktgTimeZone extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_time_zones';

	protected $m_intId;
	protected $m_strName;
	protected $m_strAbbr;
	protected $m_intGmtOffset;
	protected $m_intIsDaylightSavings;

	public function __construct() {
		parent::__construct();

		$this->m_intIsDaylightSavings = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['abbr'] ) && $boolDirectSet ) $this->set( 'm_strAbbr', trim( stripcslashes( $arrValues['abbr'] ) ) ); elseif( isset( $arrValues['abbr'] ) ) $this->setAbbr( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['abbr'] ) : $arrValues['abbr'] );
		if( isset( $arrValues['gmt_offset'] ) && $boolDirectSet ) $this->set( 'm_intGmtOffset', trim( $arrValues['gmt_offset'] ) ); elseif( isset( $arrValues['gmt_offset'] ) ) $this->setGmtOffset( $arrValues['gmt_offset'] );
		if( isset( $arrValues['is_daylight_savings'] ) && $boolDirectSet ) $this->set( 'm_intIsDaylightSavings', trim( $arrValues['is_daylight_savings'] ) ); elseif( isset( $arrValues['is_daylight_savings'] ) ) $this->setIsDaylightSavings( $arrValues['is_daylight_savings'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setAbbr( $strAbbr ) {
		$this->set( 'm_strAbbr', CStrings::strTrimDef( $strAbbr, 4, NULL, true ) );
	}

	public function getAbbr() {
		return $this->m_strAbbr;
	}

	public function sqlAbbr() {
		return ( true == isset( $this->m_strAbbr ) ) ? '\'' . addslashes( $this->m_strAbbr ) . '\'' : 'NULL';
	}

	public function setGmtOffset( $intGmtOffset ) {
		$this->set( 'm_intGmtOffset', CStrings::strToIntDef( $intGmtOffset, NULL, false ) );
	}

	public function getGmtOffset() {
		return $this->m_intGmtOffset;
	}

	public function sqlGmtOffset() {
		return ( true == isset( $this->m_intGmtOffset ) ) ? ( string ) $this->m_intGmtOffset : 'NULL';
	}

	public function setIsDaylightSavings( $intIsDaylightSavings ) {
		$this->set( 'm_intIsDaylightSavings', CStrings::strToIntDef( $intIsDaylightSavings, NULL, false ) );
	}

	public function getIsDaylightSavings() {
		return $this->m_intIsDaylightSavings;
	}

	public function sqlIsDaylightSavings() {
		return ( true == isset( $this->m_intIsDaylightSavings ) ) ? ( string ) $this->m_intIsDaylightSavings : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'abbr' => $this->getAbbr(),
			'gmt_offset' => $this->getGmtOffset(),
			'is_daylight_savings' => $this->getIsDaylightSavings()
		);
	}

}
?>