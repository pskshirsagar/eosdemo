<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyRatings
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyRatings extends CEosPluralBase {

	/**
	 * @return CMktgPropertyRating[]
	 */
	public static function fetchMktgPropertyRatings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyRating', $objDatabase );
	}

	/**
	 * @return CMktgPropertyRating
	 */
	public static function fetchMktgPropertyRating( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyRating', $objDatabase );
	}

	public static function fetchMktgPropertyRatingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_ratings', $objDatabase );
	}

	public static function fetchMktgPropertyRatingById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyRating( sprintf( 'SELECT * FROM mktg_property_ratings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyRatingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyRatings( sprintf( 'SELECT * FROM mktg_property_ratings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyRatingsByPropertyReviewId( $intPropertyReviewId, $objDatabase ) {
		return self::fetchMktgPropertyRatings( sprintf( 'SELECT * FROM mktg_property_ratings WHERE property_review_id = %d', ( int ) $intPropertyReviewId ), $objDatabase );
	}

}
?>