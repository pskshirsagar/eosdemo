<?php

class CBaseMultivariateVariable extends CEosSingularBase {

	const TABLE_NAME = 'public.multivariate_variables';

	protected $m_intId;
	protected $m_strVariableName;
	protected $m_strVariableDescription;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['variable_name'] ) && $boolDirectSet ) $this->set( 'm_strVariableName', trim( stripcslashes( $arrValues['variable_name'] ) ) ); elseif( isset( $arrValues['variable_name'] ) ) $this->setVariableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['variable_name'] ) : $arrValues['variable_name'] );
		if( isset( $arrValues['variable_description'] ) && $boolDirectSet ) $this->set( 'm_strVariableDescription', trim( stripcslashes( $arrValues['variable_description'] ) ) ); elseif( isset( $arrValues['variable_description'] ) ) $this->setVariableDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['variable_description'] ) : $arrValues['variable_description'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVariableName( $strVariableName ) {
		$this->set( 'm_strVariableName', CStrings::strTrimDef( $strVariableName, 100, NULL, true ) );
	}

	public function getVariableName() {
		return $this->m_strVariableName;
	}

	public function sqlVariableName() {
		return ( true == isset( $this->m_strVariableName ) ) ? '\'' . addslashes( $this->m_strVariableName ) . '\'' : 'NULL';
	}

	public function setVariableDescription( $strVariableDescription ) {
		$this->set( 'm_strVariableDescription', CStrings::strTrimDef( $strVariableDescription, 250, NULL, true ) );
	}

	public function getVariableDescription() {
		return $this->m_strVariableDescription;
	}

	public function sqlVariableDescription() {
		return ( true == isset( $this->m_strVariableDescription ) ) ? '\'' . addslashes( $this->m_strVariableDescription ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'variable_name' => $this->getVariableName(),
			'variable_description' => $this->getVariableDescription()
		);
	}

}
?>