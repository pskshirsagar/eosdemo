<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCountries
 * Do not add any new functions to this class.
 */

class CBaseMktgCountries extends CEosPluralBase {

	/**
	 * @return CMktgCountry[]
	 */
	public static function fetchMktgCountries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgCountry::class, $objDatabase );
	}

	/**
	 * @return CMktgCountry
	 */
	public static function fetchMktgCountry( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgCountry::class, $objDatabase );
	}

	public static function fetchMktgCountryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_countries', $objDatabase );
	}

	public static function fetchMktgCountryById( $intId, $objDatabase ) {
		return self::fetchMktgCountry( sprintf( 'SELECT * FROM mktg_countries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>