<?php

class CBaseMktgPropertyUnitAmenity extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_unit_amenities';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_intPropertyUnitId;
	protected $m_strAmenityName;
	protected $m_strMitsAmenityName;
	protected $m_fltAmenityAmount;
	protected $m_intCompanyMediaFileId;
	protected $m_strExternalVideoUrl;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['property_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitId', trim( $arrValues['property_unit_id'] ) ); elseif( isset( $arrValues['property_unit_id'] ) ) $this->setPropertyUnitId( $arrValues['property_unit_id'] );
		if( isset( $arrValues['amenity_name'] ) && $boolDirectSet ) $this->set( 'm_strAmenityName', trim( stripcslashes( $arrValues['amenity_name'] ) ) ); elseif( isset( $arrValues['amenity_name'] ) ) $this->setAmenityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amenity_name'] ) : $arrValues['amenity_name'] );
		if( isset( $arrValues['mits_amenity_name'] ) && $boolDirectSet ) $this->set( 'm_strMitsAmenityName', trim( stripcslashes( $arrValues['mits_amenity_name'] ) ) ); elseif( isset( $arrValues['mits_amenity_name'] ) ) $this->setMitsAmenityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mits_amenity_name'] ) : $arrValues['mits_amenity_name'] );
		if( isset( $arrValues['amenity_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAmenityAmount', trim( $arrValues['amenity_amount'] ) ); elseif( isset( $arrValues['amenity_amount'] ) ) $this->setAmenityAmount( $arrValues['amenity_amount'] );
		if( isset( $arrValues['company_media_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMediaFileId', trim( $arrValues['company_media_file_id'] ) ); elseif( isset( $arrValues['company_media_file_id'] ) ) $this->setCompanyMediaFileId( $arrValues['company_media_file_id'] );
		if( isset( $arrValues['external_video_url'] ) && $boolDirectSet ) $this->set( 'm_strExternalVideoUrl', trim( stripcslashes( $arrValues['external_video_url'] ) ) ); elseif( isset( $arrValues['external_video_url'] ) ) $this->setExternalVideoUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_video_url'] ) : $arrValues['external_video_url'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPropertyUnitId( $intPropertyUnitId ) {
		$this->set( 'm_intPropertyUnitId', CStrings::strToIntDef( $intPropertyUnitId, NULL, false ) );
	}

	public function getPropertyUnitId() {
		return $this->m_intPropertyUnitId;
	}

	public function sqlPropertyUnitId() {
		return ( true == isset( $this->m_intPropertyUnitId ) ) ? ( string ) $this->m_intPropertyUnitId : 'NULL';
	}

	public function setAmenityName( $strAmenityName ) {
		$this->set( 'm_strAmenityName', CStrings::strTrimDef( $strAmenityName, 255, NULL, true ) );
	}

	public function getAmenityName() {
		return $this->m_strAmenityName;
	}

	public function sqlAmenityName() {
		return ( true == isset( $this->m_strAmenityName ) ) ? '\'' . addslashes( $this->m_strAmenityName ) . '\'' : 'NULL';
	}

	public function setMitsAmenityName( $strMitsAmenityName ) {
		$this->set( 'm_strMitsAmenityName', CStrings::strTrimDef( $strMitsAmenityName, 100, NULL, true ) );
	}

	public function getMitsAmenityName() {
		return $this->m_strMitsAmenityName;
	}

	public function sqlMitsAmenityName() {
		return ( true == isset( $this->m_strMitsAmenityName ) ) ? '\'' . addslashes( $this->m_strMitsAmenityName ) . '\'' : 'NULL';
	}

	public function setAmenityAmount( $fltAmenityAmount ) {
		$this->set( 'm_fltAmenityAmount', CStrings::strToFloatDef( $fltAmenityAmount, NULL, false, 4 ) );
	}

	public function getAmenityAmount() {
		return $this->m_fltAmenityAmount;
	}

	public function sqlAmenityAmount() {
		return ( true == isset( $this->m_fltAmenityAmount ) ) ? ( string ) $this->m_fltAmenityAmount : 'NULL';
	}

	public function setCompanyMediaFileId( $intCompanyMediaFileId ) {
		$this->set( 'm_intCompanyMediaFileId', CStrings::strToIntDef( $intCompanyMediaFileId, NULL, false ) );
	}

	public function getCompanyMediaFileId() {
		return $this->m_intCompanyMediaFileId;
	}

	public function sqlCompanyMediaFileId() {
		return ( true == isset( $this->m_intCompanyMediaFileId ) ) ? ( string ) $this->m_intCompanyMediaFileId : 'NULL';
	}

	public function setExternalVideoUrl( $strExternalVideoUrl ) {
		$this->set( 'm_strExternalVideoUrl', CStrings::strTrimDef( $strExternalVideoUrl, 4096, NULL, true ) );
	}

	public function getExternalVideoUrl() {
		return $this->m_strExternalVideoUrl;
	}

	public function sqlExternalVideoUrl() {
		return ( true == isset( $this->m_strExternalVideoUrl ) ) ? '\'' . addslashes( $this->m_strExternalVideoUrl ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, property_id, property_unit_id, amenity_name, mits_amenity_name, amenity_amount, company_media_file_id, external_video_url, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPropertyUnitId() . ', ' .
 						$this->sqlAmenityName() . ', ' .
 						$this->sqlMitsAmenityName() . ', ' .
 						$this->sqlAmenityAmount() . ', ' .
 						$this->sqlCompanyMediaFileId() . ', ' .
 						$this->sqlExternalVideoUrl() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; } elseif( true == array_key_exists( 'PropertyUnitId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_id = ' . $this->sqlPropertyUnitId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_name = ' . $this->sqlAmenityName() . ','; } elseif( true == array_key_exists( 'AmenityName', $this->getChangedColumns() ) ) { $strSql .= ' amenity_name = ' . $this->sqlAmenityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mits_amenity_name = ' . $this->sqlMitsAmenityName() . ','; } elseif( true == array_key_exists( 'MitsAmenityName', $this->getChangedColumns() ) ) { $strSql .= ' mits_amenity_name = ' . $this->sqlMitsAmenityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount() . ','; } elseif( true == array_key_exists( 'AmenityAmount', $this->getChangedColumns() ) ) { $strSql .= ' amenity_amount = ' . $this->sqlAmenityAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; } elseif( true == array_key_exists( 'CompanyMediaFileId', $this->getChangedColumns() ) ) { $strSql .= ' company_media_file_id = ' . $this->sqlCompanyMediaFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; } elseif( true == array_key_exists( 'ExternalVideoUrl', $this->getChangedColumns() ) ) { $strSql .= ' external_video_url = ' . $this->sqlExternalVideoUrl() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'property_unit_id' => $this->getPropertyUnitId(),
			'amenity_name' => $this->getAmenityName(),
			'mits_amenity_name' => $this->getMitsAmenityName(),
			'amenity_amount' => $this->getAmenityAmount(),
			'company_media_file_id' => $this->getCompanyMediaFileId(),
			'external_video_url' => $this->getExternalVideoUrl(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>