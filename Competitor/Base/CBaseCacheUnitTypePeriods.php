<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CCacheUnitTypePeriods
 * Do not add any new functions to this class.
 */

/**
 * Warning: This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseCacheUnitTypePeriods extends CEosPluralBase {

	/**
	 * @return CCacheUnitTypePeriod[]
	 */
	public static function fetchCacheUnitTypePeriods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCacheUnitTypePeriod::class, $objDatabase );
	}

	/**
	 * @return CCacheUnitTypePeriod
	 */
	public static function fetchCacheUnitTypePeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCacheUnitTypePeriod::class, $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cache_unit_type_periods', $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodByIdByCid( $intId, $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriod( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE id = %d AND cid = %d', $intId, $intCid ), $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodsByCid( $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriods( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodsByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriods( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE property_id = %d AND cid = %d', $intPropertyId, $intCid ), $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodsByUnitTypeIdByCid( $intUnitTypeId, $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriods( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE unit_type_id = %d AND cid = %d', $intUnitTypeId, $intCid ), $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodsByLeaseTermIdByCid( $intLeaseTermId, $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriods( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE lease_term_id = %d AND cid = %d', $intLeaseTermId, $intCid ), $objDatabase );
	}

	public static function fetchCacheUnitTypePeriodsByLeaseStartWindowIdByCid( $intLeaseStartWindowId, $intCid, $objDatabase ) {
		return self::fetchCacheUnitTypePeriods( sprintf( 'SELECT * FROM cache_unit_type_periods WHERE lease_start_window_id = %d AND cid = %d', $intLeaseStartWindowId, $intCid ), $objDatabase );
	}

}
?>