<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgEmployees
 * Do not add any new functions to this class.
 */

class CBaseMktgEmployees extends CEosPluralBase {

	/**
	 * @return CMktgEmployee[]
	 */
	public static function fetchMktgEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgEmployee::class, $objDatabase );
	}

	/**
	 * @return CMktgEmployee
	 */
	public static function fetchMktgEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgEmployee::class, $objDatabase );
	}

	public static function fetchMktgEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_employees', $objDatabase );
	}

	public static function fetchMktgEmployeeById( $intId, $objDatabase ) {
		return self::fetchMktgEmployee( sprintf( 'SELECT * FROM mktg_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgEmployeesByManagerId( $intManagerId, $objDatabase ) {
		return self::fetchMktgEmployees( sprintf( 'SELECT * FROM mktg_employees WHERE manager_id = %d', ( int ) $intManagerId ), $objDatabase );
	}

}
?>