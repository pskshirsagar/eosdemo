<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMultivariateVariables
 * Do not add any new functions to this class.
 */

class CBaseMultivariateVariables extends CEosPluralBase {

	/**
	 * @return CMultivariateVariable[]
	 */
	public static function fetchMultivariateVariables( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMultivariateVariable', $objDatabase );
	}

	/**
	 * @return CMultivariateVariable
	 */
	public static function fetchMultivariateVariable( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMultivariateVariable', $objDatabase );
	}

	public static function fetchMultivariateVariableCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'multivariate_variables', $objDatabase );
	}

	public static function fetchMultivariateVariableById( $intId, $objDatabase ) {
		return self::fetchMultivariateVariable( sprintf( 'SELECT * FROM multivariate_variables WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>