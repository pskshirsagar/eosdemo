<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgUsers
 * Do not add any new functions to this class.
 */

class CBaseMktgUsers extends CEosPluralBase {

	/**
	 * @return CMktgUser[]
	 */
	public static function fetchMktgUsers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgUser::class, $objDatabase );
	}

	/**
	 * @return CMktgUser
	 */
	public static function fetchMktgUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgUser::class, $objDatabase );
	}

	public static function fetchMktgUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_users', $objDatabase );
	}

	public static function fetchMktgUserById( $intId, $objDatabase ) {
		return self::fetchMktgUser( sprintf( 'SELECT * FROM mktg_users WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgUsersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchMktgUsers( sprintf( 'SELECT * FROM mktg_users WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>