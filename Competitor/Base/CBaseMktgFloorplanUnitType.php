<?php

class CBaseMktgFloorplanUnitType extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_floorplan_unit_types';

	protected $m_intId;
	protected $m_intPropertyId;
	protected $m_intCompetitorPropertyId;
	protected $m_intCompetitorPropertyFloorplanId;
	protected $m_intPropertyUnitTypeId;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['competitor_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyId', trim( $arrValues['competitor_property_id'] ) ); elseif( isset( $arrValues['competitor_property_id'] ) ) $this->setCompetitorPropertyId( $arrValues['competitor_property_id'] );
		if( isset( $arrValues['competitor_property_floorplan_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyFloorplanId', trim( $arrValues['competitor_property_floorplan_id'] ) ); elseif( isset( $arrValues['competitor_property_floorplan_id'] ) ) $this->setCompetitorPropertyFloorplanId( $arrValues['competitor_property_floorplan_id'] );
		if( isset( $arrValues['property_unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitTypeId', trim( $arrValues['property_unit_type_id'] ) ); elseif( isset( $arrValues['property_unit_type_id'] ) ) $this->setPropertyUnitTypeId( $arrValues['property_unit_type_id'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompetitorPropertyId( $intCompetitorPropertyId ) {
		$this->set( 'm_intCompetitorPropertyId', CStrings::strToIntDef( $intCompetitorPropertyId, NULL, false ) );
	}

	public function getCompetitorPropertyId() {
		return $this->m_intCompetitorPropertyId;
	}

	public function sqlCompetitorPropertyId() {
		return ( true == isset( $this->m_intCompetitorPropertyId ) ) ? ( string ) $this->m_intCompetitorPropertyId : 'NULL';
	}

	public function setCompetitorPropertyFloorplanId( $intCompetitorPropertyFloorplanId ) {
		$this->set( 'm_intCompetitorPropertyFloorplanId', CStrings::strToIntDef( $intCompetitorPropertyFloorplanId, NULL, false ) );
	}

	public function getCompetitorPropertyFloorplanId() {
		return $this->m_intCompetitorPropertyFloorplanId;
	}

	public function sqlCompetitorPropertyFloorplanId() {
		return ( true == isset( $this->m_intCompetitorPropertyFloorplanId ) ) ? ( string ) $this->m_intCompetitorPropertyFloorplanId : 'NULL';
	}

	public function setPropertyUnitTypeId( $intPropertyUnitTypeId ) {
		$this->set( 'm_intPropertyUnitTypeId', CStrings::strToIntDef( $intPropertyUnitTypeId, NULL, false ) );
	}

	public function getPropertyUnitTypeId() {
		return $this->m_intPropertyUnitTypeId;
	}

	public function sqlPropertyUnitTypeId() {
		return ( true == isset( $this->m_intPropertyUnitTypeId ) ) ? ( string ) $this->m_intPropertyUnitTypeId : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_id, competitor_property_id, competitor_property_floorplan_id, property_unit_type_id, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompetitorPropertyId() . ', ' .
 						$this->sqlCompetitorPropertyFloorplanId() . ', ' .
 						$this->sqlPropertyUnitTypeId() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; } elseif( true == array_key_exists( 'CompetitorPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_floorplan_id = ' . $this->sqlCompetitorPropertyFloorplanId() . ','; } elseif( true == array_key_exists( 'CompetitorPropertyFloorplanId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_floorplan_id = ' . $this->sqlCompetitorPropertyFloorplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_type_id = ' . $this->sqlPropertyUnitTypeId() . ','; } elseif( true == array_key_exists( 'PropertyUnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_type_id = ' . $this->sqlPropertyUnitTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_id' => $this->getPropertyId(),
			'competitor_property_id' => $this->getCompetitorPropertyId(),
			'competitor_property_floorplan_id' => $this->getCompetitorPropertyFloorplanId(),
			'property_unit_type_id' => $this->getPropertyUnitTypeId(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>