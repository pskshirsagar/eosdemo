<?php

class CBaseRentjungleProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.rentjungle_properties';

	protected $m_intId;
	protected $m_strRjId;
	protected $m_strRjName;
	protected $m_strRjAddress;
	protected $m_strRjCity;
	protected $m_strRjState;
	protected $m_strRjZip;
	protected $m_fltRjLatitude;
	protected $m_fltRjLongitude;
	protected $m_intRjIshouse;
	protected $m_fltRjLowprice;
	protected $m_fltRjHighprice;
	protected $m_intRjLowbed;
	protected $m_intRjHighbath;
	protected $m_fltRjHigharea;
	protected $m_fltRjLowarea;
	protected $m_intRjLowbath;
	protected $m_intRjHighbed;
	protected $m_strSuggestedPropertyName;
	protected $m_strStatus;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strStatus = 'new';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['rj_id'] ) && $boolDirectSet ) $this->set( 'm_strRjId', trim( stripcslashes( $arrValues['rj_id'] ) ) ); elseif( isset( $arrValues['rj_id'] ) ) $this->setRjId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_id'] ) : $arrValues['rj_id'] );
		if( isset( $arrValues['rj_name'] ) && $boolDirectSet ) $this->set( 'm_strRjName', trim( stripcslashes( $arrValues['rj_name'] ) ) ); elseif( isset( $arrValues['rj_name'] ) ) $this->setRjName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_name'] ) : $arrValues['rj_name'] );
		if( isset( $arrValues['rj_address'] ) && $boolDirectSet ) $this->set( 'm_strRjAddress', trim( stripcslashes( $arrValues['rj_address'] ) ) ); elseif( isset( $arrValues['rj_address'] ) ) $this->setRjAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_address'] ) : $arrValues['rj_address'] );
		if( isset( $arrValues['rj_city'] ) && $boolDirectSet ) $this->set( 'm_strRjCity', trim( stripcslashes( $arrValues['rj_city'] ) ) ); elseif( isset( $arrValues['rj_city'] ) ) $this->setRjCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_city'] ) : $arrValues['rj_city'] );
		if( isset( $arrValues['rj_state'] ) && $boolDirectSet ) $this->set( 'm_strRjState', trim( stripcslashes( $arrValues['rj_state'] ) ) ); elseif( isset( $arrValues['rj_state'] ) ) $this->setRjState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_state'] ) : $arrValues['rj_state'] );
		if( isset( $arrValues['rj_zip'] ) && $boolDirectSet ) $this->set( 'm_strRjZip', trim( stripcslashes( $arrValues['rj_zip'] ) ) ); elseif( isset( $arrValues['rj_zip'] ) ) $this->setRjZip( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['rj_zip'] ) : $arrValues['rj_zip'] );
		if( isset( $arrValues['rj_latitude'] ) && $boolDirectSet ) $this->set( 'm_fltRjLatitude', trim( $arrValues['rj_latitude'] ) ); elseif( isset( $arrValues['rj_latitude'] ) ) $this->setRjLatitude( $arrValues['rj_latitude'] );
		if( isset( $arrValues['rj_longitude'] ) && $boolDirectSet ) $this->set( 'm_fltRjLongitude', trim( $arrValues['rj_longitude'] ) ); elseif( isset( $arrValues['rj_longitude'] ) ) $this->setRjLongitude( $arrValues['rj_longitude'] );
		if( isset( $arrValues['rj_ishouse'] ) && $boolDirectSet ) $this->set( 'm_intRjIshouse', trim( $arrValues['rj_ishouse'] ) ); elseif( isset( $arrValues['rj_ishouse'] ) ) $this->setRjIshouse( $arrValues['rj_ishouse'] );
		if( isset( $arrValues['rj_lowprice'] ) && $boolDirectSet ) $this->set( 'm_fltRjLowprice', trim( $arrValues['rj_lowprice'] ) ); elseif( isset( $arrValues['rj_lowprice'] ) ) $this->setRjLowprice( $arrValues['rj_lowprice'] );
		if( isset( $arrValues['rj_highprice'] ) && $boolDirectSet ) $this->set( 'm_fltRjHighprice', trim( $arrValues['rj_highprice'] ) ); elseif( isset( $arrValues['rj_highprice'] ) ) $this->setRjHighprice( $arrValues['rj_highprice'] );
		if( isset( $arrValues['rj_lowbed'] ) && $boolDirectSet ) $this->set( 'm_intRjLowbed', trim( $arrValues['rj_lowbed'] ) ); elseif( isset( $arrValues['rj_lowbed'] ) ) $this->setRjLowbed( $arrValues['rj_lowbed'] );
		if( isset( $arrValues['rj_highbath'] ) && $boolDirectSet ) $this->set( 'm_intRjHighbath', trim( $arrValues['rj_highbath'] ) ); elseif( isset( $arrValues['rj_highbath'] ) ) $this->setRjHighbath( $arrValues['rj_highbath'] );
		if( isset( $arrValues['rj_higharea'] ) && $boolDirectSet ) $this->set( 'm_fltRjHigharea', trim( $arrValues['rj_higharea'] ) ); elseif( isset( $arrValues['rj_higharea'] ) ) $this->setRjHigharea( $arrValues['rj_higharea'] );
		if( isset( $arrValues['rj_lowarea'] ) && $boolDirectSet ) $this->set( 'm_fltRjLowarea', trim( $arrValues['rj_lowarea'] ) ); elseif( isset( $arrValues['rj_lowarea'] ) ) $this->setRjLowarea( $arrValues['rj_lowarea'] );
		if( isset( $arrValues['rj_lowbath'] ) && $boolDirectSet ) $this->set( 'm_intRjLowbath', trim( $arrValues['rj_lowbath'] ) ); elseif( isset( $arrValues['rj_lowbath'] ) ) $this->setRjLowbath( $arrValues['rj_lowbath'] );
		if( isset( $arrValues['rj_highbed'] ) && $boolDirectSet ) $this->set( 'm_intRjHighbed', trim( $arrValues['rj_highbed'] ) ); elseif( isset( $arrValues['rj_highbed'] ) ) $this->setRjHighbed( $arrValues['rj_highbed'] );
		if( isset( $arrValues['suggested_property_name'] ) && $boolDirectSet ) $this->set( 'm_strSuggestedPropertyName', trim( stripcslashes( $arrValues['suggested_property_name'] ) ) ); elseif( isset( $arrValues['suggested_property_name'] ) ) $this->setSuggestedPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['suggested_property_name'] ) : $arrValues['suggested_property_name'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRjId( $strRjId ) {
		$this->set( 'm_strRjId', CStrings::strTrimDef( $strRjId, 32, NULL, true ) );
	}

	public function getRjId() {
		return $this->m_strRjId;
	}

	public function sqlRjId() {
		return ( true == isset( $this->m_strRjId ) ) ? '\'' . addslashes( $this->m_strRjId ) . '\'' : 'NULL';
	}

	public function setRjName( $strRjName ) {
		$this->set( 'm_strRjName', CStrings::strTrimDef( $strRjName, 255, NULL, true ) );
	}

	public function getRjName() {
		return $this->m_strRjName;
	}

	public function sqlRjName() {
		return ( true == isset( $this->m_strRjName ) ) ? '\'' . addslashes( $this->m_strRjName ) . '\'' : 'NULL';
	}

	public function setRjAddress( $strRjAddress ) {
		$this->set( 'm_strRjAddress', CStrings::strTrimDef( $strRjAddress, 255, NULL, true ) );
	}

	public function getRjAddress() {
		return $this->m_strRjAddress;
	}

	public function sqlRjAddress() {
		return ( true == isset( $this->m_strRjAddress ) ) ? '\'' . addslashes( $this->m_strRjAddress ) . '\'' : 'NULL';
	}

	public function setRjCity( $strRjCity ) {
		$this->set( 'm_strRjCity', CStrings::strTrimDef( $strRjCity, 50, NULL, true ) );
	}

	public function getRjCity() {
		return $this->m_strRjCity;
	}

	public function sqlRjCity() {
		return ( true == isset( $this->m_strRjCity ) ) ? '\'' . addslashes( $this->m_strRjCity ) . '\'' : 'NULL';
	}

	public function setRjState( $strRjState ) {
		$this->set( 'm_strRjState', CStrings::strTrimDef( $strRjState, 40, NULL, true ) );
	}

	public function getRjState() {
		return $this->m_strRjState;
	}

	public function sqlRjState() {
		return ( true == isset( $this->m_strRjState ) ) ? '\'' . addslashes( $this->m_strRjState ) . '\'' : 'NULL';
	}

	public function setRjZip( $strRjZip ) {
		$this->set( 'm_strRjZip', CStrings::strTrimDef( $strRjZip, 10, NULL, true ) );
	}

	public function getRjZip() {
		return $this->m_strRjZip;
	}

	public function sqlRjZip() {
		return ( true == isset( $this->m_strRjZip ) ) ? '\'' . addslashes( $this->m_strRjZip ) . '\'' : 'NULL';
	}

	public function setRjLatitude( $fltRjLatitude ) {
		$this->set( 'm_fltRjLatitude', CStrings::strToFloatDef( $fltRjLatitude, NULL, false, 6 ) );
	}

	public function getRjLatitude() {
		return $this->m_fltRjLatitude;
	}

	public function sqlRjLatitude() {
		return ( true == isset( $this->m_fltRjLatitude ) ) ? ( string ) $this->m_fltRjLatitude : 'NULL';
	}

	public function setRjLongitude( $fltRjLongitude ) {
		$this->set( 'm_fltRjLongitude', CStrings::strToFloatDef( $fltRjLongitude, NULL, false, 6 ) );
	}

	public function getRjLongitude() {
		return $this->m_fltRjLongitude;
	}

	public function sqlRjLongitude() {
		return ( true == isset( $this->m_fltRjLongitude ) ) ? ( string ) $this->m_fltRjLongitude : 'NULL';
	}

	public function setRjIshouse( $intRjIshouse ) {
		$this->set( 'm_intRjIshouse', CStrings::strToIntDef( $intRjIshouse, NULL, false ) );
	}

	public function getRjIshouse() {
		return $this->m_intRjIshouse;
	}

	public function sqlRjIshouse() {
		return ( true == isset( $this->m_intRjIshouse ) ) ? ( string ) $this->m_intRjIshouse : 'NULL';
	}

	public function setRjLowprice( $fltRjLowprice ) {
		$this->set( 'm_fltRjLowprice', CStrings::strToFloatDef( $fltRjLowprice, NULL, false, 2 ) );
	}

	public function getRjLowprice() {
		return $this->m_fltRjLowprice;
	}

	public function sqlRjLowprice() {
		return ( true == isset( $this->m_fltRjLowprice ) ) ? ( string ) $this->m_fltRjLowprice : 'NULL';
	}

	public function setRjHighprice( $fltRjHighprice ) {
		$this->set( 'm_fltRjHighprice', CStrings::strToFloatDef( $fltRjHighprice, NULL, false, 2 ) );
	}

	public function getRjHighprice() {
		return $this->m_fltRjHighprice;
	}

	public function sqlRjHighprice() {
		return ( true == isset( $this->m_fltRjHighprice ) ) ? ( string ) $this->m_fltRjHighprice : 'NULL';
	}

	public function setRjLowbed( $intRjLowbed ) {
		$this->set( 'm_intRjLowbed', CStrings::strToIntDef( $intRjLowbed, NULL, false ) );
	}

	public function getRjLowbed() {
		return $this->m_intRjLowbed;
	}

	public function sqlRjLowbed() {
		return ( true == isset( $this->m_intRjLowbed ) ) ? ( string ) $this->m_intRjLowbed : 'NULL';
	}

	public function setRjHighbath( $intRjHighbath ) {
		$this->set( 'm_intRjHighbath', CStrings::strToIntDef( $intRjHighbath, NULL, false ) );
	}

	public function getRjHighbath() {
		return $this->m_intRjHighbath;
	}

	public function sqlRjHighbath() {
		return ( true == isset( $this->m_intRjHighbath ) ) ? ( string ) $this->m_intRjHighbath : 'NULL';
	}

	public function setRjHigharea( $fltRjHigharea ) {
		$this->set( 'm_fltRjHigharea', CStrings::strToFloatDef( $fltRjHigharea, NULL, false, 2 ) );
	}

	public function getRjHigharea() {
		return $this->m_fltRjHigharea;
	}

	public function sqlRjHigharea() {
		return ( true == isset( $this->m_fltRjHigharea ) ) ? ( string ) $this->m_fltRjHigharea : 'NULL';
	}

	public function setRjLowarea( $fltRjLowarea ) {
		$this->set( 'm_fltRjLowarea', CStrings::strToFloatDef( $fltRjLowarea, NULL, false, 2 ) );
	}

	public function getRjLowarea() {
		return $this->m_fltRjLowarea;
	}

	public function sqlRjLowarea() {
		return ( true == isset( $this->m_fltRjLowarea ) ) ? ( string ) $this->m_fltRjLowarea : 'NULL';
	}

	public function setRjLowbath( $intRjLowbath ) {
		$this->set( 'm_intRjLowbath', CStrings::strToIntDef( $intRjLowbath, NULL, false ) );
	}

	public function getRjLowbath() {
		return $this->m_intRjLowbath;
	}

	public function sqlRjLowbath() {
		return ( true == isset( $this->m_intRjLowbath ) ) ? ( string ) $this->m_intRjLowbath : 'NULL';
	}

	public function setRjHighbed( $intRjHighbed ) {
		$this->set( 'm_intRjHighbed', CStrings::strToIntDef( $intRjHighbed, NULL, false ) );
	}

	public function getRjHighbed() {
		return $this->m_intRjHighbed;
	}

	public function sqlRjHighbed() {
		return ( true == isset( $this->m_intRjHighbed ) ) ? ( string ) $this->m_intRjHighbed : 'NULL';
	}

	public function setSuggestedPropertyName( $strSuggestedPropertyName ) {
		$this->set( 'm_strSuggestedPropertyName', CStrings::strTrimDef( $strSuggestedPropertyName, 50, NULL, true ) );
	}

	public function getSuggestedPropertyName() {
		return $this->m_strSuggestedPropertyName;
	}

	public function sqlSuggestedPropertyName() {
		return ( true == isset( $this->m_strSuggestedPropertyName ) ) ? '\'' . addslashes( $this->m_strSuggestedPropertyName ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 10, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : '\'new\'';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, rj_id, rj_name, rj_address, rj_city, rj_state, rj_zip, rj_latitude, rj_longitude, rj_ishouse, rj_lowprice, rj_highprice, rj_lowbed, rj_highbath, rj_higharea, rj_lowarea, rj_lowbath, rj_highbed, suggested_property_name, status, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRjId() . ', ' .
 						$this->sqlRjName() . ', ' .
 						$this->sqlRjAddress() . ', ' .
 						$this->sqlRjCity() . ', ' .
 						$this->sqlRjState() . ', ' .
 						$this->sqlRjZip() . ', ' .
 						$this->sqlRjLatitude() . ', ' .
 						$this->sqlRjLongitude() . ', ' .
 						$this->sqlRjIshouse() . ', ' .
 						$this->sqlRjLowprice() . ', ' .
 						$this->sqlRjHighprice() . ', ' .
 						$this->sqlRjLowbed() . ', ' .
 						$this->sqlRjHighbath() . ', ' .
 						$this->sqlRjHigharea() . ', ' .
 						$this->sqlRjLowarea() . ', ' .
 						$this->sqlRjLowbath() . ', ' .
 						$this->sqlRjHighbed() . ', ' .
 						$this->sqlSuggestedPropertyName() . ', ' .
 						$this->sqlStatus() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_id = ' . $this->sqlRjId() . ','; } elseif( true == array_key_exists( 'RjId', $this->getChangedColumns() ) ) { $strSql .= ' rj_id = ' . $this->sqlRjId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_name = ' . $this->sqlRjName() . ','; } elseif( true == array_key_exists( 'RjName', $this->getChangedColumns() ) ) { $strSql .= ' rj_name = ' . $this->sqlRjName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_address = ' . $this->sqlRjAddress() . ','; } elseif( true == array_key_exists( 'RjAddress', $this->getChangedColumns() ) ) { $strSql .= ' rj_address = ' . $this->sqlRjAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_city = ' . $this->sqlRjCity() . ','; } elseif( true == array_key_exists( 'RjCity', $this->getChangedColumns() ) ) { $strSql .= ' rj_city = ' . $this->sqlRjCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_state = ' . $this->sqlRjState() . ','; } elseif( true == array_key_exists( 'RjState', $this->getChangedColumns() ) ) { $strSql .= ' rj_state = ' . $this->sqlRjState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_zip = ' . $this->sqlRjZip() . ','; } elseif( true == array_key_exists( 'RjZip', $this->getChangedColumns() ) ) { $strSql .= ' rj_zip = ' . $this->sqlRjZip() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_latitude = ' . $this->sqlRjLatitude() . ','; } elseif( true == array_key_exists( 'RjLatitude', $this->getChangedColumns() ) ) { $strSql .= ' rj_latitude = ' . $this->sqlRjLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_longitude = ' . $this->sqlRjLongitude() . ','; } elseif( true == array_key_exists( 'RjLongitude', $this->getChangedColumns() ) ) { $strSql .= ' rj_longitude = ' . $this->sqlRjLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_ishouse = ' . $this->sqlRjIshouse() . ','; } elseif( true == array_key_exists( 'RjIshouse', $this->getChangedColumns() ) ) { $strSql .= ' rj_ishouse = ' . $this->sqlRjIshouse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_lowprice = ' . $this->sqlRjLowprice() . ','; } elseif( true == array_key_exists( 'RjLowprice', $this->getChangedColumns() ) ) { $strSql .= ' rj_lowprice = ' . $this->sqlRjLowprice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_highprice = ' . $this->sqlRjHighprice() . ','; } elseif( true == array_key_exists( 'RjHighprice', $this->getChangedColumns() ) ) { $strSql .= ' rj_highprice = ' . $this->sqlRjHighprice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_lowbed = ' . $this->sqlRjLowbed() . ','; } elseif( true == array_key_exists( 'RjLowbed', $this->getChangedColumns() ) ) { $strSql .= ' rj_lowbed = ' . $this->sqlRjLowbed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_highbath = ' . $this->sqlRjHighbath() . ','; } elseif( true == array_key_exists( 'RjHighbath', $this->getChangedColumns() ) ) { $strSql .= ' rj_highbath = ' . $this->sqlRjHighbath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_higharea = ' . $this->sqlRjHigharea() . ','; } elseif( true == array_key_exists( 'RjHigharea', $this->getChangedColumns() ) ) { $strSql .= ' rj_higharea = ' . $this->sqlRjHigharea() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_lowarea = ' . $this->sqlRjLowarea() . ','; } elseif( true == array_key_exists( 'RjLowarea', $this->getChangedColumns() ) ) { $strSql .= ' rj_lowarea = ' . $this->sqlRjLowarea() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_lowbath = ' . $this->sqlRjLowbath() . ','; } elseif( true == array_key_exists( 'RjLowbath', $this->getChangedColumns() ) ) { $strSql .= ' rj_lowbath = ' . $this->sqlRjLowbath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rj_highbed = ' . $this->sqlRjHighbed() . ','; } elseif( true == array_key_exists( 'RjHighbed', $this->getChangedColumns() ) ) { $strSql .= ' rj_highbed = ' . $this->sqlRjHighbed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suggested_property_name = ' . $this->sqlSuggestedPropertyName() . ','; } elseif( true == array_key_exists( 'SuggestedPropertyName', $this->getChangedColumns() ) ) { $strSql .= ' suggested_property_name = ' . $this->sqlSuggestedPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'rj_id' => $this->getRjId(),
			'rj_name' => $this->getRjName(),
			'rj_address' => $this->getRjAddress(),
			'rj_city' => $this->getRjCity(),
			'rj_state' => $this->getRjState(),
			'rj_zip' => $this->getRjZip(),
			'rj_latitude' => $this->getRjLatitude(),
			'rj_longitude' => $this->getRjLongitude(),
			'rj_ishouse' => $this->getRjIshouse(),
			'rj_lowprice' => $this->getRjLowprice(),
			'rj_highprice' => $this->getRjHighprice(),
			'rj_lowbed' => $this->getRjLowbed(),
			'rj_highbath' => $this->getRjHighbath(),
			'rj_higharea' => $this->getRjHigharea(),
			'rj_lowarea' => $this->getRjLowarea(),
			'rj_lowbath' => $this->getRjLowbath(),
			'rj_highbed' => $this->getRjHighbed(),
			'suggested_property_name' => $this->getSuggestedPropertyName(),
			'status' => $this->getStatus(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>