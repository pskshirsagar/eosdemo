<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompetitiveAdjustmentTypes
 * Do not add any new functions to this class.
 */

class CBaseMktgCompetitiveAdjustmentTypes extends CEosPluralBase {

	/**
	 * @return CMktgCompetitiveAdjustmentType[]
	 */
	public static function fetchMktgCompetitiveAdjustmentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgCompetitiveAdjustmentType', $objDatabase );
	}

	/**
	 * @return CMktgCompetitiveAdjustmentType
	 */
	public static function fetchMktgCompetitiveAdjustmentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgCompetitiveAdjustmentType', $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_competitive_adjustment_types', $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentTypeById( $intId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustmentType( sprintf( 'SELECT * FROM mktg_competitive_adjustment_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>