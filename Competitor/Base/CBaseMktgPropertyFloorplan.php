<?php

class CBaseMktgPropertyFloorplan extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_floorplans';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltMinDeposit;
	protected $m_fltMaxDeposit;
	protected $m_intIsManualRange;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intNumberOfUnits;
	protected $m_intNumberOfFloors;
	protected $m_intNumberOfRooms;
	protected $m_intNumberOfBedrooms;
	protected $m_fltNumberOfBathrooms;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsManualRange = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['min_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinRent', trim( $arrValues['min_rent'] ) ); elseif( isset( $arrValues['min_rent'] ) ) $this->setMinRent( $arrValues['min_rent'] );
		if( isset( $arrValues['max_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxRent', trim( $arrValues['max_rent'] ) ); elseif( isset( $arrValues['max_rent'] ) ) $this->setMaxRent( $arrValues['max_rent'] );
		if( isset( $arrValues['min_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMinDeposit', trim( $arrValues['min_deposit'] ) ); elseif( isset( $arrValues['min_deposit'] ) ) $this->setMinDeposit( $arrValues['min_deposit'] );
		if( isset( $arrValues['max_deposit'] ) && $boolDirectSet ) $this->set( 'm_fltMaxDeposit', trim( $arrValues['max_deposit'] ) ); elseif( isset( $arrValues['max_deposit'] ) ) $this->setMaxDeposit( $arrValues['max_deposit'] );
		if( isset( $arrValues['is_manual_range'] ) && $boolDirectSet ) $this->set( 'm_intIsManualRange', trim( $arrValues['is_manual_range'] ) ); elseif( isset( $arrValues['is_manual_range'] ) ) $this->setIsManualRange( $arrValues['is_manual_range'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['number_of_floors'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfFloors', trim( $arrValues['number_of_floors'] ) ); elseif( isset( $arrValues['number_of_floors'] ) ) $this->setNumberOfFloors( $arrValues['number_of_floors'] );
		if( isset( $arrValues['number_of_rooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfRooms', trim( $arrValues['number_of_rooms'] ) ); elseif( isset( $arrValues['number_of_rooms'] ) ) $this->setNumberOfRooms( $arrValues['number_of_rooms'] );
		if( isset( $arrValues['number_of_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBedrooms', trim( $arrValues['number_of_bedrooms'] ) ); elseif( isset( $arrValues['number_of_bedrooms'] ) ) $this->setNumberOfBedrooms( $arrValues['number_of_bedrooms'] );
		if( isset( $arrValues['number_of_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltNumberOfBathrooms', trim( $arrValues['number_of_bathrooms'] ) ); elseif( isset( $arrValues['number_of_bathrooms'] ) ) $this->setNumberOfBathrooms( $arrValues['number_of_bathrooms'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setMinRent( $fltMinRent ) {
		$this->set( 'm_fltMinRent', CStrings::strToFloatDef( $fltMinRent, NULL, false, 4 ) );
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function sqlMinRent() {
		return ( true == isset( $this->m_fltMinRent ) ) ? ( string ) $this->m_fltMinRent : 'NULL';
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->set( 'm_fltMaxRent', CStrings::strToFloatDef( $fltMaxRent, NULL, false, 4 ) );
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function sqlMaxRent() {
		return ( true == isset( $this->m_fltMaxRent ) ) ? ( string ) $this->m_fltMaxRent : 'NULL';
	}

	public function setMinDeposit( $fltMinDeposit ) {
		$this->set( 'm_fltMinDeposit', CStrings::strToFloatDef( $fltMinDeposit, NULL, false, 4 ) );
	}

	public function getMinDeposit() {
		return $this->m_fltMinDeposit;
	}

	public function sqlMinDeposit() {
		return ( true == isset( $this->m_fltMinDeposit ) ) ? ( string ) $this->m_fltMinDeposit : 'NULL';
	}

	public function setMaxDeposit( $fltMaxDeposit ) {
		$this->set( 'm_fltMaxDeposit', CStrings::strToFloatDef( $fltMaxDeposit, NULL, false, 4 ) );
	}

	public function getMaxDeposit() {
		return $this->m_fltMaxDeposit;
	}

	public function sqlMaxDeposit() {
		return ( true == isset( $this->m_fltMaxDeposit ) ) ? ( string ) $this->m_fltMaxDeposit : 'NULL';
	}

	public function setIsManualRange( $intIsManualRange ) {
		$this->set( 'm_intIsManualRange', CStrings::strToIntDef( $intIsManualRange, NULL, false ) );
	}

	public function getIsManualRange() {
		return $this->m_intIsManualRange;
	}

	public function sqlIsManualRange() {
		return ( true == isset( $this->m_intIsManualRange ) ) ? ( string ) $this->m_intIsManualRange : '0';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setNumberOfFloors( $intNumberOfFloors ) {
		$this->set( 'm_intNumberOfFloors', CStrings::strToIntDef( $intNumberOfFloors, NULL, false ) );
	}

	public function getNumberOfFloors() {
		return $this->m_intNumberOfFloors;
	}

	public function sqlNumberOfFloors() {
		return ( true == isset( $this->m_intNumberOfFloors ) ) ? ( string ) $this->m_intNumberOfFloors : 'NULL';
	}

	public function setNumberOfRooms( $intNumberOfRooms ) {
		$this->set( 'm_intNumberOfRooms', CStrings::strToIntDef( $intNumberOfRooms, NULL, false ) );
	}

	public function getNumberOfRooms() {
		return $this->m_intNumberOfRooms;
	}

	public function sqlNumberOfRooms() {
		return ( true == isset( $this->m_intNumberOfRooms ) ) ? ( string ) $this->m_intNumberOfRooms : 'NULL';
	}

	public function setNumberOfBedrooms( $intNumberOfBedrooms ) {
		$this->set( 'm_intNumberOfBedrooms', CStrings::strToIntDef( $intNumberOfBedrooms, NULL, false ) );
	}

	public function getNumberOfBedrooms() {
		return $this->m_intNumberOfBedrooms;
	}

	public function sqlNumberOfBedrooms() {
		return ( true == isset( $this->m_intNumberOfBedrooms ) ) ? ( string ) $this->m_intNumberOfBedrooms : 'NULL';
	}

	public function setNumberOfBathrooms( $fltNumberOfBathrooms ) {
		$this->set( 'm_fltNumberOfBathrooms', CStrings::strToFloatDef( $fltNumberOfBathrooms, NULL, false, 4 ) );
	}

	public function getNumberOfBathrooms() {
		return $this->m_fltNumberOfBathrooms;
	}

	public function sqlNumberOfBathrooms() {
		return ( true == isset( $this->m_fltNumberOfBathrooms ) ) ? ( string ) $this->m_fltNumberOfBathrooms : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, property_id, name, description, min_rent, max_rent, min_deposit, max_deposit, is_manual_range, min_square_feet, max_square_feet, number_of_units, number_of_floors, number_of_rooms, number_of_bedrooms, number_of_bathrooms, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlMinRent() . ', ' .
 						$this->sqlMaxRent() . ', ' .
 						$this->sqlMinDeposit() . ', ' .
 						$this->sqlMaxDeposit() . ', ' .
 						$this->sqlIsManualRange() . ', ' .
 						$this->sqlMinSquareFeet() . ', ' .
 						$this->sqlMaxSquareFeet() . ', ' .
 						$this->sqlNumberOfUnits() . ', ' .
 						$this->sqlNumberOfFloors() . ', ' .
 						$this->sqlNumberOfRooms() . ', ' .
 						$this->sqlNumberOfBedrooms() . ', ' .
 						$this->sqlNumberOfBathrooms() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; } elseif( true == array_key_exists( 'MinRent', $this->getChangedColumns() ) ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; } elseif( true == array_key_exists( 'MaxRent', $this->getChangedColumns() ) ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit() . ','; } elseif( true == array_key_exists( 'MinDeposit', $this->getChangedColumns() ) ) { $strSql .= ' min_deposit = ' . $this->sqlMinDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit() . ','; } elseif( true == array_key_exists( 'MaxDeposit', $this->getChangedColumns() ) ) { $strSql .= ' max_deposit = ' . $this->sqlMaxDeposit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_range = ' . $this->sqlIsManualRange() . ','; } elseif( true == array_key_exists( 'IsManualRange', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_range = ' . $this->sqlIsManualRange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; } elseif( true == array_key_exists( 'NumberOfFloors', $this->getChangedColumns() ) ) { $strSql .= ' number_of_floors = ' . $this->sqlNumberOfFloors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; } elseif( true == array_key_exists( 'NumberOfRooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_rooms = ' . $this->sqlNumberOfRooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bedrooms = ' . $this->sqlNumberOfBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; } elseif( true == array_key_exists( 'NumberOfBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' number_of_bathrooms = ' . $this->sqlNumberOfBathrooms() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'min_rent' => $this->getMinRent(),
			'max_rent' => $this->getMaxRent(),
			'min_deposit' => $this->getMinDeposit(),
			'max_deposit' => $this->getMaxDeposit(),
			'is_manual_range' => $this->getIsManualRange(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'number_of_units' => $this->getNumberOfUnits(),
			'number_of_floors' => $this->getNumberOfFloors(),
			'number_of_rooms' => $this->getNumberOfRooms(),
			'number_of_bedrooms' => $this->getNumberOfBedrooms(),
			'number_of_bathrooms' => $this->getNumberOfBathrooms(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>