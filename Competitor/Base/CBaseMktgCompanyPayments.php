<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanyPayments
 * Do not add any new functions to this class.
 */

class CBaseMktgCompanyPayments extends CEosPluralBase {

	/**
	 * @return CMktgCompanyPayment[]
	 */
	public static function fetchMktgCompanyPayments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgCompanyPayment::class, $objDatabase );
	}

	/**
	 * @return CMktgCompanyPayment
	 */
	public static function fetchMktgCompanyPayment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgCompanyPayment::class, $objDatabase );
	}

	public static function fetchMktgCompanyPaymentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_company_payments', $objDatabase );
	}

	public static function fetchMktgCompanyPaymentById( $intId, $objDatabase ) {
		return self::fetchMktgCompanyPayment( sprintf( 'SELECT * FROM mktg_company_payments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgCompanyPaymentsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgCompanyPayments( sprintf( 'SELECT * FROM mktg_company_payments WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgCompanyPaymentsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchMktgCompanyPayments( sprintf( 'SELECT * FROM mktg_company_payments WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

}
?>