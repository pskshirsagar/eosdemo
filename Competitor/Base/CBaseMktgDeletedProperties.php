<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgDeletedProperties
 * Do not add any new functions to this class.
 */

class CBaseMktgDeletedProperties extends CEosPluralBase {

	/**
	 * @return CMktgDeletedProperty[]
	 */
	public static function fetchMktgDeletedProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgDeletedProperty', $objDatabase );
	}

	/**
	 * @return CMktgDeletedProperty
	 */
	public static function fetchMktgDeletedProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgDeletedProperty', $objDatabase );
	}

	public static function fetchMktgDeletedPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_deleted_properties', $objDatabase );
	}

	public static function fetchMktgDeletedPropertyById( $intId, $objDatabase ) {
		return self::fetchMktgDeletedProperty( sprintf( 'SELECT * FROM mktg_deleted_properties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgDeletedPropertiesByConsolidatedPropertyId( $intConsolidatedPropertyId, $objDatabase ) {
		return self::fetchMktgDeletedProperties( sprintf( 'SELECT * FROM mktg_deleted_properties WHERE consolidated_property_id = %d', ( int ) $intConsolidatedPropertyId ), $objDatabase );
	}

}
?>