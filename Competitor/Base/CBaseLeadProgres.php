<?php

class CBaseLeadProgres extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.lead_progress';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intUnitTypeId;
	protected $m_intLeaseTermId;
	protected $m_intLeaseStartWindowId;
	protected $m_intApplicationId;
	protected $m_strEffectiveDate;
	protected $m_intApplicationStartedCount;
	protected $m_intApplicationPartiallyCompletedCount;
	protected $m_intApplicationCompletedCount;
	protected $m_intApplicationApprovedCount;
	protected $m_intLeaseStartedCount;
	protected $m_intLeasePartiallyCompletedCount;
	protected $m_intLeaseCompletedCount;
	protected $m_intLeaseApprovedCount;
	protected $m_boolIsAbandoned;
	protected $m_strLoadedToCacheOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strLeaseStartDate;
	protected $m_strLeaseEndDate;
	protected $m_strGroupHash;
	protected $m_intCurrentCalId;
	protected $m_intBedroomCount;
	protected $m_intUnitSpaceCount;
	protected $m_intTotalUnitSpaceCount;
	protected $m_intRent;
	protected $m_intBaseRent;
	protected $m_intAmenityRent;
	protected $m_intSpecialRent;
	protected $m_intLeaseApprovedEndedCount;
	protected $m_intTotalNetCount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strExtractRange;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['unit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intUnitTypeId', trim( $arrValues['unit_type_id'] ) ); elseif( isset( $arrValues['unit_type_id'] ) ) $this->setUnitTypeId( $arrValues['unit_type_id'] );
		if( isset( $arrValues['lease_term_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseTermId', trim( $arrValues['lease_term_id'] ) ); elseif( isset( $arrValues['lease_term_id'] ) ) $this->setLeaseTermId( $arrValues['lease_term_id'] );
		if( isset( $arrValues['lease_start_window_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartWindowId', trim( $arrValues['lease_start_window_id'] ) ); elseif( isset( $arrValues['lease_start_window_id'] ) ) $this->setLeaseStartWindowId( $arrValues['lease_start_window_id'] );
		if( isset( $arrValues['application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicationId', trim( $arrValues['application_id'] ) ); elseif( isset( $arrValues['application_id'] ) ) $this->setApplicationId( $arrValues['application_id'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['application_started_count'] ) && $boolDirectSet ) $this->set( 'm_intApplicationStartedCount', trim( $arrValues['application_started_count'] ) ); elseif( isset( $arrValues['application_started_count'] ) ) $this->setApplicationStartedCount( $arrValues['application_started_count'] );
		if( isset( $arrValues['application_partially_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intApplicationPartiallyCompletedCount', trim( $arrValues['application_partially_completed_count'] ) ); elseif( isset( $arrValues['application_partially_completed_count'] ) ) $this->setApplicationPartiallyCompletedCount( $arrValues['application_partially_completed_count'] );
		if( isset( $arrValues['application_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intApplicationCompletedCount', trim( $arrValues['application_completed_count'] ) ); elseif( isset( $arrValues['application_completed_count'] ) ) $this->setApplicationCompletedCount( $arrValues['application_completed_count'] );
		if( isset( $arrValues['application_approved_count'] ) && $boolDirectSet ) $this->set( 'm_intApplicationApprovedCount', trim( $arrValues['application_approved_count'] ) ); elseif( isset( $arrValues['application_approved_count'] ) ) $this->setApplicationApprovedCount( $arrValues['application_approved_count'] );
		if( isset( $arrValues['lease_started_count'] ) && $boolDirectSet ) $this->set( 'm_intLeaseStartedCount', trim( $arrValues['lease_started_count'] ) ); elseif( isset( $arrValues['lease_started_count'] ) ) $this->setLeaseStartedCount( $arrValues['lease_started_count'] );
		if( isset( $arrValues['lease_partially_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intLeasePartiallyCompletedCount', trim( $arrValues['lease_partially_completed_count'] ) ); elseif( isset( $arrValues['lease_partially_completed_count'] ) ) $this->setLeasePartiallyCompletedCount( $arrValues['lease_partially_completed_count'] );
		if( isset( $arrValues['lease_completed_count'] ) && $boolDirectSet ) $this->set( 'm_intLeaseCompletedCount', trim( $arrValues['lease_completed_count'] ) ); elseif( isset( $arrValues['lease_completed_count'] ) ) $this->setLeaseCompletedCount( $arrValues['lease_completed_count'] );
		if( isset( $arrValues['lease_approved_count'] ) && $boolDirectSet ) $this->set( 'm_intLeaseApprovedCount', trim( $arrValues['lease_approved_count'] ) ); elseif( isset( $arrValues['lease_approved_count'] ) ) $this->setLeaseApprovedCount( $arrValues['lease_approved_count'] );
		if( isset( $arrValues['is_abandoned'] ) && $boolDirectSet ) $this->set( 'm_boolIsAbandoned', trim( stripcslashes( $arrValues['is_abandoned'] ) ) ); elseif( isset( $arrValues['is_abandoned'] ) ) $this->setIsAbandoned( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_abandoned'] ) : $arrValues['is_abandoned'] );
		if( isset( $arrValues['loaded_to_cache_on'] ) && $boolDirectSet ) $this->set( 'm_strLoadedToCacheOn', trim( $arrValues['loaded_to_cache_on'] ) ); elseif( isset( $arrValues['loaded_to_cache_on'] ) ) $this->setLoadedToCacheOn( $arrValues['loaded_to_cache_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['lease_start_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseStartDate', trim( $arrValues['lease_start_date'] ) ); elseif( isset( $arrValues['lease_start_date'] ) ) $this->setLeaseStartDate( $arrValues['lease_start_date'] );
		if( isset( $arrValues['lease_end_date'] ) && $boolDirectSet ) $this->set( 'm_strLeaseEndDate', trim( $arrValues['lease_end_date'] ) ); elseif( isset( $arrValues['lease_end_date'] ) ) $this->setLeaseEndDate( $arrValues['lease_end_date'] );
		if( isset( $arrValues['group_hash'] ) && $boolDirectSet ) $this->set( 'm_strGroupHash', trim( $arrValues['group_hash'] ) ); elseif( isset( $arrValues['group_hash'] ) ) $this->setGroupHash( $arrValues['group_hash'] );
		if( isset( $arrValues['current_cal_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrentCalId', trim( $arrValues['current_cal_id'] ) ); elseif( isset( $arrValues['current_cal_id'] ) ) $this->setCurrentCalId( $arrValues['current_cal_id'] );
		if( isset( $arrValues['bedroom_count'] ) && $boolDirectSet ) $this->set( 'm_intBedroomCount', trim( $arrValues['bedroom_count'] ) ); elseif( isset( $arrValues['bedroom_count'] ) ) $this->setBedroomCount( $arrValues['bedroom_count'] );
		if( isset( $arrValues['unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitSpaceCount', trim( $arrValues['unit_space_count'] ) ); elseif( isset( $arrValues['unit_space_count'] ) ) $this->setUnitSpaceCount( $arrValues['unit_space_count'] );
		if( isset( $arrValues['total_unit_space_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitSpaceCount', trim( $arrValues['total_unit_space_count'] ) ); elseif( isset( $arrValues['total_unit_space_count'] ) ) $this->setTotalUnitSpaceCount( $arrValues['total_unit_space_count'] );
		if( isset( $arrValues['rent'] ) && $boolDirectSet ) $this->set( 'm_intRent', trim( $arrValues['rent'] ) ); elseif( isset( $arrValues['rent'] ) ) $this->setRent( $arrValues['rent'] );
		if( isset( $arrValues['base_rent'] ) && $boolDirectSet ) $this->set( 'm_intBaseRent', trim( $arrValues['base_rent'] ) ); elseif( isset( $arrValues['base_rent'] ) ) $this->setBaseRent( $arrValues['base_rent'] );
		if( isset( $arrValues['amenity_rent'] ) && $boolDirectSet ) $this->set( 'm_intAmenityRent', trim( $arrValues['amenity_rent'] ) ); elseif( isset( $arrValues['amenity_rent'] ) ) $this->setAmenityRent( $arrValues['amenity_rent'] );
		if( isset( $arrValues['special_rent'] ) && $boolDirectSet ) $this->set( 'm_intSpecialRent', trim( $arrValues['special_rent'] ) ); elseif( isset( $arrValues['special_rent'] ) ) $this->setSpecialRent( $arrValues['special_rent'] );
		if( isset( $arrValues['lease_approved_ended_count'] ) && $boolDirectSet ) $this->set( 'm_intLeaseApprovedEndedCount', trim( $arrValues['lease_approved_ended_count'] ) ); elseif( isset( $arrValues['lease_approved_ended_count'] ) ) $this->setLeaseApprovedEndedCount( $arrValues['lease_approved_ended_count'] );
		if( isset( $arrValues['total_net_count'] ) && $boolDirectSet ) $this->set( 'm_intTotalNetCount', trim( $arrValues['total_net_count'] ) ); elseif( isset( $arrValues['total_net_count'] ) ) $this->setTotalNetCount( $arrValues['total_net_count'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['extract_range'] ) && $boolDirectSet ) $this->set( 'm_strExtractRange', trim( $arrValues['extract_range'] ) ); elseif( isset( $arrValues['extract_range'] ) ) $this->setExtractRange( $arrValues['extract_range'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setUnitTypeId( $intUnitTypeId ) {
		$this->set( 'm_intUnitTypeId', CStrings::strToIntDef( $intUnitTypeId, NULL, false ) );
	}

	public function getUnitTypeId() {
		return $this->m_intUnitTypeId;
	}

	public function sqlUnitTypeId() {
		return ( true == isset( $this->m_intUnitTypeId ) ) ? ( string ) $this->m_intUnitTypeId : 'NULL';
	}

	public function setLeaseTermId( $intLeaseTermId ) {
		$this->set( 'm_intLeaseTermId', CStrings::strToIntDef( $intLeaseTermId, NULL, false ) );
	}

	public function getLeaseTermId() {
		return $this->m_intLeaseTermId;
	}

	public function sqlLeaseTermId() {
		return ( true == isset( $this->m_intLeaseTermId ) ) ? ( string ) $this->m_intLeaseTermId : 'NULL';
	}

	public function setLeaseStartWindowId( $intLeaseStartWindowId ) {
		$this->set( 'm_intLeaseStartWindowId', CStrings::strToIntDef( $intLeaseStartWindowId, NULL, false ) );
	}

	public function getLeaseStartWindowId() {
		return $this->m_intLeaseStartWindowId;
	}

	public function sqlLeaseStartWindowId() {
		return ( true == isset( $this->m_intLeaseStartWindowId ) ) ? ( string ) $this->m_intLeaseStartWindowId : 'NULL';
	}

	public function setApplicationId( $intApplicationId ) {
		$this->set( 'm_intApplicationId', CStrings::strToIntDef( $intApplicationId, NULL, false ) );
	}

	public function getApplicationId() {
		return $this->m_intApplicationId;
	}

	public function sqlApplicationId() {
		return ( true == isset( $this->m_intApplicationId ) ) ? ( string ) $this->m_intApplicationId : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setApplicationStartedCount( $intApplicationStartedCount ) {
		$this->set( 'm_intApplicationStartedCount', CStrings::strToIntDef( $intApplicationStartedCount, NULL, false ) );
	}

	public function getApplicationStartedCount() {
		return $this->m_intApplicationStartedCount;
	}

	public function sqlApplicationStartedCount() {
		return ( true == isset( $this->m_intApplicationStartedCount ) ) ? ( string ) $this->m_intApplicationStartedCount : 'NULL';
	}

	public function setApplicationPartiallyCompletedCount( $intApplicationPartiallyCompletedCount ) {
		$this->set( 'm_intApplicationPartiallyCompletedCount', CStrings::strToIntDef( $intApplicationPartiallyCompletedCount, NULL, false ) );
	}

	public function getApplicationPartiallyCompletedCount() {
		return $this->m_intApplicationPartiallyCompletedCount;
	}

	public function sqlApplicationPartiallyCompletedCount() {
		return ( true == isset( $this->m_intApplicationPartiallyCompletedCount ) ) ? ( string ) $this->m_intApplicationPartiallyCompletedCount : 'NULL';
	}

	public function setApplicationCompletedCount( $intApplicationCompletedCount ) {
		$this->set( 'm_intApplicationCompletedCount', CStrings::strToIntDef( $intApplicationCompletedCount, NULL, false ) );
	}

	public function getApplicationCompletedCount() {
		return $this->m_intApplicationCompletedCount;
	}

	public function sqlApplicationCompletedCount() {
		return ( true == isset( $this->m_intApplicationCompletedCount ) ) ? ( string ) $this->m_intApplicationCompletedCount : 'NULL';
	}

	public function setApplicationApprovedCount( $intApplicationApprovedCount ) {
		$this->set( 'm_intApplicationApprovedCount', CStrings::strToIntDef( $intApplicationApprovedCount, NULL, false ) );
	}

	public function getApplicationApprovedCount() {
		return $this->m_intApplicationApprovedCount;
	}

	public function sqlApplicationApprovedCount() {
		return ( true == isset( $this->m_intApplicationApprovedCount ) ) ? ( string ) $this->m_intApplicationApprovedCount : 'NULL';
	}

	public function setLeaseStartedCount( $intLeaseStartedCount ) {
		$this->set( 'm_intLeaseStartedCount', CStrings::strToIntDef( $intLeaseStartedCount, NULL, false ) );
	}

	public function getLeaseStartedCount() {
		return $this->m_intLeaseStartedCount;
	}

	public function sqlLeaseStartedCount() {
		return ( true == isset( $this->m_intLeaseStartedCount ) ) ? ( string ) $this->m_intLeaseStartedCount : 'NULL';
	}

	public function setLeasePartiallyCompletedCount( $intLeasePartiallyCompletedCount ) {
		$this->set( 'm_intLeasePartiallyCompletedCount', CStrings::strToIntDef( $intLeasePartiallyCompletedCount, NULL, false ) );
	}

	public function getLeasePartiallyCompletedCount() {
		return $this->m_intLeasePartiallyCompletedCount;
	}

	public function sqlLeasePartiallyCompletedCount() {
		return ( true == isset( $this->m_intLeasePartiallyCompletedCount ) ) ? ( string ) $this->m_intLeasePartiallyCompletedCount : 'NULL';
	}

	public function setLeaseCompletedCount( $intLeaseCompletedCount ) {
		$this->set( 'm_intLeaseCompletedCount', CStrings::strToIntDef( $intLeaseCompletedCount, NULL, false ) );
	}

	public function getLeaseCompletedCount() {
		return $this->m_intLeaseCompletedCount;
	}

	public function sqlLeaseCompletedCount() {
		return ( true == isset( $this->m_intLeaseCompletedCount ) ) ? ( string ) $this->m_intLeaseCompletedCount : 'NULL';
	}

	public function setLeaseApprovedCount( $intLeaseApprovedCount ) {
		$this->set( 'm_intLeaseApprovedCount', CStrings::strToIntDef( $intLeaseApprovedCount, NULL, false ) );
	}

	public function getLeaseApprovedCount() {
		return $this->m_intLeaseApprovedCount;
	}

	public function sqlLeaseApprovedCount() {
		return ( true == isset( $this->m_intLeaseApprovedCount ) ) ? ( string ) $this->m_intLeaseApprovedCount : 'NULL';
	}

	public function setIsAbandoned( $boolIsAbandoned ) {
		$this->set( 'm_boolIsAbandoned', CStrings::strToBool( $boolIsAbandoned ) );
	}

	public function getIsAbandoned() {
		return $this->m_boolIsAbandoned;
	}

	public function sqlIsAbandoned() {
		return ( true == isset( $this->m_boolIsAbandoned ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAbandoned ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setLoadedToCacheOn( $strLoadedToCacheOn ) {
		$this->set( 'm_strLoadedToCacheOn', CStrings::strTrimDef( $strLoadedToCacheOn, -1, NULL, true ) );
	}

	public function getLoadedToCacheOn() {
		return $this->m_strLoadedToCacheOn;
	}

	public function sqlLoadedToCacheOn() {
		return ( true == isset( $this->m_strLoadedToCacheOn ) ) ? '\'' . $this->m_strLoadedToCacheOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setLeaseStartDate( $strLeaseStartDate ) {
		$this->set( 'm_strLeaseStartDate', CStrings::strTrimDef( $strLeaseStartDate, -1, NULL, true ) );
	}

	public function getLeaseStartDate() {
		return $this->m_strLeaseStartDate;
	}

	public function sqlLeaseStartDate() {
		return ( true == isset( $this->m_strLeaseStartDate ) ) ? '\'' . $this->m_strLeaseStartDate . '\'' : 'NULL';
	}

	public function setLeaseEndDate( $strLeaseEndDate ) {
		$this->set( 'm_strLeaseEndDate', CStrings::strTrimDef( $strLeaseEndDate, -1, NULL, true ) );
	}

	public function getLeaseEndDate() {
		return $this->m_strLeaseEndDate;
	}

	public function sqlLeaseEndDate() {
		return ( true == isset( $this->m_strLeaseEndDate ) ) ? '\'' . $this->m_strLeaseEndDate . '\'' : 'NULL';
	}

	public function setGroupHash( $strGroupHash ) {
		$this->set( 'm_strGroupHash', CStrings::strTrimDef( $strGroupHash, -1, NULL, true ) );
	}

	public function getGroupHash() {
		return $this->m_strGroupHash;
	}

	public function sqlGroupHash() {
		return ( true == isset( $this->m_strGroupHash ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGroupHash ) : '\'' . addslashes( $this->m_strGroupHash ) . '\'' ) : 'NULL';
	}

	public function setCurrentCalId( $intCurrentCalId ) {
		$this->set( 'm_intCurrentCalId', CStrings::strToIntDef( $intCurrentCalId, NULL, false ) );
	}

	public function getCurrentCalId() {
		return $this->m_intCurrentCalId;
	}

	public function sqlCurrentCalId() {
		return ( true == isset( $this->m_intCurrentCalId ) ) ? ( string ) $this->m_intCurrentCalId : 'NULL';
	}

	public function setBedroomCount( $intBedroomCount ) {
		$this->set( 'm_intBedroomCount', CStrings::strToIntDef( $intBedroomCount, NULL, false ) );
	}

	public function getBedroomCount() {
		return $this->m_intBedroomCount;
	}

	public function sqlBedroomCount() {
		return ( true == isset( $this->m_intBedroomCount ) ) ? ( string ) $this->m_intBedroomCount : 'NULL';
	}

	public function setUnitSpaceCount( $intUnitSpaceCount ) {
		$this->set( 'm_intUnitSpaceCount', CStrings::strToIntDef( $intUnitSpaceCount, NULL, false ) );
	}

	public function getUnitSpaceCount() {
		return $this->m_intUnitSpaceCount;
	}

	public function sqlUnitSpaceCount() {
		return ( true == isset( $this->m_intUnitSpaceCount ) ) ? ( string ) $this->m_intUnitSpaceCount : 'NULL';
	}

	public function setTotalUnitSpaceCount( $intTotalUnitSpaceCount ) {
		$this->set( 'm_intTotalUnitSpaceCount', CStrings::strToIntDef( $intTotalUnitSpaceCount, NULL, false ) );
	}

	public function getTotalUnitSpaceCount() {
		return $this->m_intTotalUnitSpaceCount;
	}

	public function sqlTotalUnitSpaceCount() {
		return ( true == isset( $this->m_intTotalUnitSpaceCount ) ) ? ( string ) $this->m_intTotalUnitSpaceCount : 'NULL';
	}

	public function setRent( $intRent ) {
		$this->set( 'm_intRent', CStrings::strToIntDef( $intRent, NULL, false ) );
	}

	public function getRent() {
		return $this->m_intRent;
	}

	public function sqlRent() {
		return ( true == isset( $this->m_intRent ) ) ? ( string ) $this->m_intRent : 'NULL';
	}

	public function setBaseRent( $intBaseRent ) {
		$this->set( 'm_intBaseRent', CStrings::strToIntDef( $intBaseRent, NULL, false ) );
	}

	public function getBaseRent() {
		return $this->m_intBaseRent;
	}

	public function sqlBaseRent() {
		return ( true == isset( $this->m_intBaseRent ) ) ? ( string ) $this->m_intBaseRent : 'NULL';
	}

	public function setAmenityRent( $intAmenityRent ) {
		$this->set( 'm_intAmenityRent', CStrings::strToIntDef( $intAmenityRent, NULL, false ) );
	}

	public function getAmenityRent() {
		return $this->m_intAmenityRent;
	}

	public function sqlAmenityRent() {
		return ( true == isset( $this->m_intAmenityRent ) ) ? ( string ) $this->m_intAmenityRent : 'NULL';
	}

	public function setSpecialRent( $intSpecialRent ) {
		$this->set( 'm_intSpecialRent', CStrings::strToIntDef( $intSpecialRent, NULL, false ) );
	}

	public function getSpecialRent() {
		return $this->m_intSpecialRent;
	}

	public function sqlSpecialRent() {
		return ( true == isset( $this->m_intSpecialRent ) ) ? ( string ) $this->m_intSpecialRent : 'NULL';
	}

	public function setLeaseApprovedEndedCount( $intLeaseApprovedEndedCount ) {
		$this->set( 'm_intLeaseApprovedEndedCount', CStrings::strToIntDef( $intLeaseApprovedEndedCount, NULL, false ) );
	}

	public function getLeaseApprovedEndedCount() {
		return $this->m_intLeaseApprovedEndedCount;
	}

	public function sqlLeaseApprovedEndedCount() {
		return ( true == isset( $this->m_intLeaseApprovedEndedCount ) ) ? ( string ) $this->m_intLeaseApprovedEndedCount : 'NULL';
	}

	public function setTotalNetCount( $intTotalNetCount ) {
		$this->set( 'm_intTotalNetCount', CStrings::strToIntDef( $intTotalNetCount, NULL, false ) );
	}

	public function getTotalNetCount() {
		return $this->m_intTotalNetCount;
	}

	public function sqlTotalNetCount() {
		return ( true == isset( $this->m_intTotalNetCount ) ) ? ( string ) $this->m_intTotalNetCount : 'NULL';
	}

	public function setExtractRange( $strExtractRange ) {
		$this->set( 'm_strExtractRange', CStrings::strTrimDef( $strExtractRange, NULL, NULL, true ) );
	}

	public function getExtractRange() {
		return $this->m_strExtractRange;
	}

	public function sqlExtractRange() {
		return ( true == isset( $this->m_strExtractRange ) ) ? '\'' . addslashes( $this->m_strExtractRange ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, unit_type_id, lease_term_id, lease_start_window_id, application_id, effective_date, application_started_count, application_partially_completed_count, application_completed_count, application_approved_count, lease_started_count, lease_partially_completed_count, lease_completed_count, lease_approved_count, is_abandoned, loaded_to_cache_on, created_by, created_on, updated_by, updated_on, lease_start_date, lease_end_date, group_hash, current_cal_id, bedroom_count, unit_space_count, total_unit_space_count, rent, base_rent, amenity_rent, special_rent, lease_approved_ended_count, total_net_count, details, extract_range )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlUnitTypeId() . ', ' .
						$this->sqlLeaseTermId() . ', ' .
						$this->sqlLeaseStartWindowId() . ', ' .
						$this->sqlApplicationId() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlApplicationStartedCount() . ', ' .
						$this->sqlApplicationPartiallyCompletedCount() . ', ' .
						$this->sqlApplicationCompletedCount() . ', ' .
						$this->sqlApplicationApprovedCount() . ', ' .
						$this->sqlLeaseStartedCount() . ', ' .
						$this->sqlLeasePartiallyCompletedCount() . ', ' .
						$this->sqlLeaseCompletedCount() . ', ' .
						$this->sqlLeaseApprovedCount() . ', ' .
						$this->sqlIsAbandoned() . ', ' .
						$this->sqlLoadedToCacheOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlLeaseStartDate() . ', ' .
						$this->sqlLeaseEndDate() . ', ' .
						$this->sqlGroupHash() . ', ' .
						$this->sqlCurrentCalId() . ', ' .
						$this->sqlBedroomCount() . ', ' .
						$this->sqlUnitSpaceCount() . ', ' .
						$this->sqlTotalUnitSpaceCount() . ', ' .
						$this->sqlRent() . ', ' .
						$this->sqlBaseRent() . ', ' .
						$this->sqlAmenityRent() . ', ' .
						$this->sqlSpecialRent() . ', ' .
						$this->sqlLeaseApprovedEndedCount() . ', ' .
						$this->sqlTotalNetCount() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlExtractRange() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId(). ',' ; } elseif( true == array_key_exists( 'UnitTypeId', $this->getChangedColumns() ) ) { $strSql .= ' unit_type_id = ' . $this->sqlUnitTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId(). ',' ; } elseif( true == array_key_exists( 'LeaseTermId', $this->getChangedColumns() ) ) { $strSql .= ' lease_term_id = ' . $this->sqlLeaseTermId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId(). ',' ; } elseif( true == array_key_exists( 'LeaseStartWindowId', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_window_id = ' . $this->sqlLeaseStartWindowId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_id = ' . $this->sqlApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' application_id = ' . $this->sqlApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_started_count = ' . $this->sqlApplicationStartedCount(). ',' ; } elseif( true == array_key_exists( 'ApplicationStartedCount', $this->getChangedColumns() ) ) { $strSql .= ' application_started_count = ' . $this->sqlApplicationStartedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_partially_completed_count = ' . $this->sqlApplicationPartiallyCompletedCount(). ',' ; } elseif( true == array_key_exists( 'ApplicationPartiallyCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' application_partially_completed_count = ' . $this->sqlApplicationPartiallyCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_completed_count = ' . $this->sqlApplicationCompletedCount(). ',' ; } elseif( true == array_key_exists( 'ApplicationCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' application_completed_count = ' . $this->sqlApplicationCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_approved_count = ' . $this->sqlApplicationApprovedCount(). ',' ; } elseif( true == array_key_exists( 'ApplicationApprovedCount', $this->getChangedColumns() ) ) { $strSql .= ' application_approved_count = ' . $this->sqlApplicationApprovedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_started_count = ' . $this->sqlLeaseStartedCount(). ',' ; } elseif( true == array_key_exists( 'LeaseStartedCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_started_count = ' . $this->sqlLeaseStartedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_partially_completed_count = ' . $this->sqlLeasePartiallyCompletedCount(). ',' ; } elseif( true == array_key_exists( 'LeasePartiallyCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_partially_completed_count = ' . $this->sqlLeasePartiallyCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_completed_count = ' . $this->sqlLeaseCompletedCount(). ',' ; } elseif( true == array_key_exists( 'LeaseCompletedCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_completed_count = ' . $this->sqlLeaseCompletedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_approved_count = ' . $this->sqlLeaseApprovedCount(). ',' ; } elseif( true == array_key_exists( 'LeaseApprovedCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_approved_count = ' . $this->sqlLeaseApprovedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_abandoned = ' . $this->sqlIsAbandoned(). ',' ; } elseif( true == array_key_exists( 'IsAbandoned', $this->getChangedColumns() ) ) { $strSql .= ' is_abandoned = ' . $this->sqlIsAbandoned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loaded_to_cache_on = ' . $this->sqlLoadedToCacheOn(). ',' ; } elseif( true == array_key_exists( 'LoadedToCacheOn', $this->getChangedColumns() ) ) { $strSql .= ' loaded_to_cache_on = ' . $this->sqlLoadedToCacheOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate(). ',' ; } elseif( true == array_key_exists( 'LeaseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_start_date = ' . $this->sqlLeaseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate(). ',' ; } elseif( true == array_key_exists( 'LeaseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' lease_end_date = ' . $this->sqlLeaseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_hash = ' . $this->sqlGroupHash(). ',' ; } elseif( true == array_key_exists( 'GroupHash', $this->getChangedColumns() ) ) { $strSql .= ' group_hash = ' . $this->sqlGroupHash() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_cal_id = ' . $this->sqlCurrentCalId(). ',' ; } elseif( true == array_key_exists( 'CurrentCalId', $this->getChangedColumns() ) ) { $strSql .= ' current_cal_id = ' . $this->sqlCurrentCalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount(). ',' ; } elseif( true == array_key_exists( 'BedroomCount', $this->getChangedColumns() ) ) { $strSql .= ' bedroom_count = ' . $this->sqlBedroomCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount(). ',' ; } elseif( true == array_key_exists( 'UnitSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_space_count = ' . $this->sqlUnitSpaceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_unit_space_count = ' . $this->sqlTotalUnitSpaceCount(). ',' ; } elseif( true == array_key_exists( 'TotalUnitSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' total_unit_space_count = ' . $this->sqlTotalUnitSpaceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rent = ' . $this->sqlRent(). ',' ; } elseif( true == array_key_exists( 'Rent', $this->getChangedColumns() ) ) { $strSql .= ' rent = ' . $this->sqlRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent(). ',' ; } elseif( true == array_key_exists( 'BaseRent', $this->getChangedColumns() ) ) { $strSql .= ' base_rent = ' . $this->sqlBaseRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent(). ',' ; } elseif( true == array_key_exists( 'AmenityRent', $this->getChangedColumns() ) ) { $strSql .= ' amenity_rent = ' . $this->sqlAmenityRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent(). ',' ; } elseif( true == array_key_exists( 'SpecialRent', $this->getChangedColumns() ) ) { $strSql .= ' special_rent = ' . $this->sqlSpecialRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_approved_ended_count = ' . $this->sqlLeaseApprovedEndedCount(). ',' ; } elseif( true == array_key_exists( 'LeaseApprovedEndedCount', $this->getChangedColumns() ) ) { $strSql .= ' lease_approved_ended_count = ' . $this->sqlLeaseApprovedEndedCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_net_count = ' . $this->sqlTotalNetCount(). ',' ; } elseif( true == array_key_exists( 'TotalNetCount', $this->getChangedColumns() ) ) { $strSql .= ' total_net_count = ' . $this->sqlTotalNetCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extract_range = ' . $this->sqlExtractRange(). ',' ; } elseif( true == array_key_exists( 'ExtractRange', $this->getChangedColumns() ) ) { $strSql .= ' extract_range = ' . $this->sqlExtractRange() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'unit_type_id' => $this->getUnitTypeId(),
			'lease_term_id' => $this->getLeaseTermId(),
			'lease_start_window_id' => $this->getLeaseStartWindowId(),
			'application_id' => $this->getApplicationId(),
			'effective_date' => $this->getEffectiveDate(),
			'application_started_count' => $this->getApplicationStartedCount(),
			'application_partially_completed_count' => $this->getApplicationPartiallyCompletedCount(),
			'application_completed_count' => $this->getApplicationCompletedCount(),
			'application_approved_count' => $this->getApplicationApprovedCount(),
			'lease_started_count' => $this->getLeaseStartedCount(),
			'lease_partially_completed_count' => $this->getLeasePartiallyCompletedCount(),
			'lease_completed_count' => $this->getLeaseCompletedCount(),
			'lease_approved_count' => $this->getLeaseApprovedCount(),
			'is_abandoned' => $this->getIsAbandoned(),
			'loaded_to_cache_on' => $this->getLoadedToCacheOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'lease_start_date' => $this->getLeaseStartDate(),
			'lease_end_date' => $this->getLeaseEndDate(),
			'group_hash' => $this->getGroupHash(),
			'current_cal_id' => $this->getCurrentCalId(),
			'bedroom_count' => $this->getBedroomCount(),
			'unit_space_count' => $this->getUnitSpaceCount(),
			'total_unit_space_count' => $this->getTotalUnitSpaceCount(),
			'rent' => $this->getRent(),
			'base_rent' => $this->getBaseRent(),
			'amenity_rent' => $this->getAmenityRent(),
			'special_rent' => $this->getSpecialRent(),
			'lease_approved_ended_count' => $this->getLeaseApprovedEndedCount(),
			'total_net_count' => $this->getTotalNetCount(),
			'details' => $this->getDetails(),
			'extract_range' => $this->getExtractRange()
		);
	}

}
?>
