<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgBlacklistedDomains
 * Do not add any new functions to this class.
 */

class CBaseMktgBlacklistedDomains extends CEosPluralBase {

	/**
	 * @return CMktgBlacklistedDomain[]
	 */
	public static function fetchMktgBlacklistedDomains( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgBlacklistedDomain', $objDatabase );
	}

	/**
	 * @return CMktgBlacklistedDomain
	 */
	public static function fetchMktgBlacklistedDomain( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgBlacklistedDomain', $objDatabase );
	}

	public static function fetchMktgBlacklistedDomainCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_blacklisted_domains', $objDatabase );
	}

	public static function fetchMktgBlacklistedDomainById( $intId, $objDatabase ) {
		return self::fetchMktgBlacklistedDomain( sprintf( 'SELECT * FROM mktg_blacklisted_domains WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>