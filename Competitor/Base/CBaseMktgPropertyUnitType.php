<?php

class CBaseMktgPropertyUnitType extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_unit_types';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_strLookupCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strIsDisabled;
	protected $m_intUnitMix;
	protected $m_fltSqft;
	protected $m_fltMarketRentTotal;

	public function __construct() {
		parent::__construct();

		$this->m_strIsDisabled = 'N';
		$this->m_intUnitMix = '0';
		$this->m_fltSqft = '0.0';
		$this->m_fltMarketRentTotal = '0.0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['lookup_code'] ) && $boolDirectSet ) $this->set( 'm_strLookupCode', trim( stripcslashes( $arrValues['lookup_code'] ) ) ); elseif( isset( $arrValues['lookup_code'] ) ) $this->setLookupCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lookup_code'] ) : $arrValues['lookup_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_strIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['unit_mix'] ) && $boolDirectSet ) $this->set( 'm_intUnitMix', trim( $arrValues['unit_mix'] ) ); elseif( isset( $arrValues['unit_mix'] ) ) $this->setUnitMix( $arrValues['unit_mix'] );
		if( isset( $arrValues['sqft'] ) && $boolDirectSet ) $this->set( 'm_fltSqft', trim( $arrValues['sqft'] ) ); elseif( isset( $arrValues['sqft'] ) ) $this->setSqft( $arrValues['sqft'] );
		if( isset( $arrValues['market_rent_total'] ) && $boolDirectSet ) $this->set( 'm_fltMarketRentTotal', trim( $arrValues['market_rent_total'] ) ); elseif( isset( $arrValues['market_rent_total'] ) ) $this->setMarketRentTotal( $arrValues['market_rent_total'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setLookupCode( $strLookupCode ) {
		$this->set( 'm_strLookupCode', CStrings::strTrimDef( $strLookupCode, 240, NULL, true ) );
	}

	public function getLookupCode() {
		return $this->m_strLookupCode;
	}

	public function sqlLookupCode() {
		return ( true == isset( $this->m_strLookupCode ) ) ? '\'' . addslashes( $this->m_strLookupCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setIsDisabled( $strIsDisabled ) {
		$this->set( 'm_strIsDisabled', CStrings::strTrimDef( $strIsDisabled, 1, NULL, true ) );
	}

	public function getIsDisabled() {
		return $this->m_strIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_strIsDisabled ) ) ? '\'' . addslashes( $this->m_strIsDisabled ) . '\'' : '\'N\'';
	}

	public function setUnitMix( $intUnitMix ) {
		$this->set( 'm_intUnitMix', CStrings::strToIntDef( $intUnitMix, NULL, false ) );
	}

	public function getUnitMix() {
		return $this->m_intUnitMix;
	}

	public function sqlUnitMix() {
		return ( true == isset( $this->m_intUnitMix ) ) ? ( string ) $this->m_intUnitMix : '0';
	}

	public function setSqft( $fltSqft ) {
		$this->set( 'm_fltSqft', CStrings::strToFloatDef( $fltSqft, NULL, false, 4 ) );
	}

	public function getSqft() {
		return $this->m_fltSqft;
	}

	public function sqlSqft() {
		return ( true == isset( $this->m_fltSqft ) ) ? ( string ) $this->m_fltSqft : '0.0';
	}

	public function setMarketRentTotal( $fltMarketRentTotal ) {
		$this->set( 'm_fltMarketRentTotal', CStrings::strToFloatDef( $fltMarketRentTotal, NULL, false, 2 ) );
	}

	public function getMarketRentTotal() {
		return $this->m_fltMarketRentTotal;
	}

	public function sqlMarketRentTotal() {
		return ( true == isset( $this->m_fltMarketRentTotal ) ) ? ( string ) $this->m_fltMarketRentTotal : '0.0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, property_id, lookup_code, name, description, created_on, updated_on, is_disabled, unit_mix, sqft, market_rent_total )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlLookupCode() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlUnitMix() . ', ' .
 						$this->sqlSqft() . ', ' .
 						$this->sqlMarketRentTotal() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; } elseif( true == array_key_exists( 'LookupCode', $this->getChangedColumns() ) ) { $strSql .= ' lookup_code = ' . $this->sqlLookupCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_mix = ' . $this->sqlUnitMix() . ','; } elseif( true == array_key_exists( 'UnitMix', $this->getChangedColumns() ) ) { $strSql .= ' unit_mix = ' . $this->sqlUnitMix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sqft = ' . $this->sqlSqft() . ','; } elseif( true == array_key_exists( 'Sqft', $this->getChangedColumns() ) ) { $strSql .= ' sqft = ' . $this->sqlSqft() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' market_rent_total = ' . $this->sqlMarketRentTotal() . ','; } elseif( true == array_key_exists( 'MarketRentTotal', $this->getChangedColumns() ) ) { $strSql .= ' market_rent_total = ' . $this->sqlMarketRentTotal() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'lookup_code' => $this->getLookupCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'is_disabled' => $this->getIsDisabled(),
			'unit_mix' => $this->getUnitMix(),
			'sqft' => $this->getSqft(),
			'market_rent_total' => $this->getMarketRentTotal()
		);
	}

}
?>