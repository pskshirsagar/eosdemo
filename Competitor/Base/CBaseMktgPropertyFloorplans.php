<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyFloorplans
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyFloorplans extends CEosPluralBase {

	/**
	 * @return CMktgPropertyFloorplan[]
	 */
	public static function fetchMktgPropertyFloorplans( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyFloorplan', $objDatabase );
	}

	/**
	 * @return CMktgPropertyFloorplan
	 */
	public static function fetchMktgPropertyFloorplan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyFloorplan', $objDatabase );
	}

	public static function fetchMktgPropertyFloorplanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_floorplans', $objDatabase );
	}

	public static function fetchMktgPropertyFloorplanById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyFloorplan( sprintf( 'SELECT * FROM mktg_property_floorplans WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorplansByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyFloorplans( sprintf( 'SELECT * FROM mktg_property_floorplans WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyFloorplansByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyFloorplans( sprintf( 'SELECT * FROM mktg_property_floorplans WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>