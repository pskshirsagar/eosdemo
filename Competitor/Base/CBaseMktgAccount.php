<?php

class CBaseMktgAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_accounts';

	protected $m_intId;
	protected $m_intEntityId;
	protected $m_intCompanyId;
	protected $m_intDefaultBillingAccountId;
	protected $m_intRelatedCompanyAccountId;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['default_billing_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultBillingAccountId', trim( $arrValues['default_billing_account_id'] ) ); elseif( isset( $arrValues['default_billing_account_id'] ) ) $this->setDefaultBillingAccountId( $arrValues['default_billing_account_id'] );
		if( isset( $arrValues['related_company_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRelatedCompanyAccountId', trim( $arrValues['related_company_account_id'] ) ); elseif( isset( $arrValues['related_company_account_id'] ) ) $this->setRelatedCompanyAccountId( $arrValues['related_company_account_id'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setDefaultBillingAccountId( $intDefaultBillingAccountId ) {
		$this->set( 'm_intDefaultBillingAccountId', CStrings::strToIntDef( $intDefaultBillingAccountId, NULL, false ) );
	}

	public function getDefaultBillingAccountId() {
		return $this->m_intDefaultBillingAccountId;
	}

	public function sqlDefaultBillingAccountId() {
		return ( true == isset( $this->m_intDefaultBillingAccountId ) ) ? ( string ) $this->m_intDefaultBillingAccountId : 'NULL';
	}

	public function setRelatedCompanyAccountId( $intRelatedCompanyAccountId ) {
		$this->set( 'm_intRelatedCompanyAccountId', CStrings::strToIntDef( $intRelatedCompanyAccountId, NULL, false ) );
	}

	public function getRelatedCompanyAccountId() {
		return $this->m_intRelatedCompanyAccountId;
	}

	public function sqlRelatedCompanyAccountId() {
		return ( true == isset( $this->m_intRelatedCompanyAccountId ) ) ? ( string ) $this->m_intRelatedCompanyAccountId : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_id, company_id, default_billing_account_id, related_company_account_id, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEntityId() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlDefaultBillingAccountId() . ', ' .
 						$this->sqlRelatedCompanyAccountId() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_billing_account_id = ' . $this->sqlDefaultBillingAccountId() . ','; } elseif( true == array_key_exists( 'DefaultBillingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' default_billing_account_id = ' . $this->sqlDefaultBillingAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' related_company_account_id = ' . $this->sqlRelatedCompanyAccountId() . ','; } elseif( true == array_key_exists( 'RelatedCompanyAccountId', $this->getChangedColumns() ) ) { $strSql .= ' related_company_account_id = ' . $this->sqlRelatedCompanyAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_id' => $this->getEntityId(),
			'company_id' => $this->getCompanyId(),
			'default_billing_account_id' => $this->getDefaultBillingAccountId(),
			'related_company_account_id' => $this->getRelatedCompanyAccountId(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>