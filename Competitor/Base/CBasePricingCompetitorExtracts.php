<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CPricingCompetitorExtracts
 * Do not add any new functions to this class.
 */

class CBasePricingCompetitorExtracts extends CEosPluralBase {

	/**
	 * @return CPricingCompetitorExtract[]
	 */
	public static function fetchPricingCompetitorExtracts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPricingCompetitorExtract', $objDatabase );
	}

	/**
	 * @return CPricingCompetitorExtract
	 */
	public static function fetchPricingCompetitorExtract( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPricingCompetitorExtract', $objDatabase );
	}

	public static function fetchPricingCompetitorExtractCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'pricing_competitor_extracts', $objDatabase );
	}

	public static function fetchPricingCompetitorExtractById( $intId, $objDatabase ) {
		return self::fetchPricingCompetitorExtract( sprintf( 'SELECT * FROM pricing_competitor_extracts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>