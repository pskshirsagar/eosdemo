<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompetitiveAdjustments
 * Do not add any new functions to this class.
 */

class CBaseMktgCompetitiveAdjustments extends CEosPluralBase {

	/**
	 * @return CMktgCompetitiveAdjustment[]
	 */
	public static function fetchMktgCompetitiveAdjustments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgCompetitiveAdjustment', $objDatabase );
	}

	/**
	 * @return CMktgCompetitiveAdjustment
	 */
	public static function fetchMktgCompetitiveAdjustment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgCompetitiveAdjustment', $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_competitive_adjustments', $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentById( $intId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustment( sprintf( 'SELECT * FROM mktg_competitive_adjustments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustments( sprintf( 'SELECT * FROM mktg_competitive_adjustments WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustments( sprintf( 'SELECT * FROM mktg_competitive_adjustments WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentsByCompetitorPropertyId( $intCompetitorPropertyId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustments( sprintf( 'SELECT * FROM mktg_competitive_adjustments WHERE competitor_property_id = %d', ( int ) $intCompetitorPropertyId ), $objDatabase );
	}

	public static function fetchMktgCompetitiveAdjustmentsByCompetitiveAdjustmentTypeId( $intCompetitiveAdjustmentTypeId, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustments( sprintf( 'SELECT * FROM mktg_competitive_adjustments WHERE competitive_adjustment_type_id = %d', ( int ) $intCompetitiveAdjustmentTypeId ), $objDatabase );
	}

}
?>