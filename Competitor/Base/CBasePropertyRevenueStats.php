<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CPropertyRevenueStats
 * Do not add any new functions to this class.
 */

class CBasePropertyRevenueStats extends CEosPluralBase {

	/**
	 * @return CPropertyRevenueStat[]
	 */
	public static function fetchPropertyRevenueStats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyRevenueStat::class, $objDatabase );
	}

	/**
	 * @return CPropertyRevenueStat
	 */
	public static function fetchPropertyRevenueStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyRevenueStat::class, $objDatabase );
	}

	public static function fetchPropertyRevenueStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_revenue_stats', $objDatabase );
	}

	public static function fetchPropertyRevenueStatById( $intId, $objDatabase ) {
		return self::fetchPropertyRevenueStat( sprintf( 'SELECT * FROM property_revenue_stats WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchPropertyRevenueStatsByEntrataCid( $intEntrataCid, $objDatabase ) {
		return self::fetchPropertyRevenueStats( sprintf( 'SELECT * FROM property_revenue_stats WHERE entrata_cid = %d', $intEntrataCid ), $objDatabase );
	}

	public static function fetchPropertyRevenueStatsByEntrataPropertyId( $intEntrataPropertyId, $objDatabase ) {
		return self::fetchPropertyRevenueStats( sprintf( 'SELECT * FROM property_revenue_stats WHERE entrata_property_id = %d', $intEntrataPropertyId ), $objDatabase );
	}

}
?>