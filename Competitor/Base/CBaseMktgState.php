<?php

class CBaseMktgState extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_states';

	protected $m_strCode;
	protected $m_strName;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['code'] ) && $boolDirectSet ) $this->set( 'm_strCode', trim( stripcslashes( $arrValues['code'] ) ) ); elseif( isset( $arrValues['code'] ) ) $this->setCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['code'] ) : $arrValues['code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		$this->m_boolInitialized = true;
	}

	public function setCode( $strCode ) {
		$this->set( 'm_strCode', CStrings::strTrimDef( $strCode, 2, NULL, true ) );
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function sqlCode() {
		return ( true == isset( $this->m_strCode ) ) ? '\'' . addslashes( $this->m_strCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'code' => $this->getCode(),
			'name' => $this->getName()
		);
	}

}
?>