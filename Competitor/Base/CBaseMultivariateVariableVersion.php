<?php

class CBaseMultivariateVariableVersion extends CEosSingularBase {

	const TABLE_NAME = 'public.multivariate_variable_versions';

	protected $m_intId;
	protected $m_intMultivariateVariablesId;
	protected $m_strVariableVersion;
	protected $m_strVariableValue;
	protected $m_fltVisitors;
	protected $m_fltActions;
	protected $m_intActive;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['multivariate_variables_id'] ) && $boolDirectSet ) $this->set( 'm_intMultivariateVariablesId', trim( $arrValues['multivariate_variables_id'] ) ); elseif( isset( $arrValues['multivariate_variables_id'] ) ) $this->setMultivariateVariablesId( $arrValues['multivariate_variables_id'] );
		if( isset( $arrValues['variable_version'] ) && $boolDirectSet ) $this->set( 'm_strVariableVersion', trim( stripcslashes( $arrValues['variable_version'] ) ) ); elseif( isset( $arrValues['variable_version'] ) ) $this->setVariableVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['variable_version'] ) : $arrValues['variable_version'] );
		if( isset( $arrValues['variable_value'] ) && $boolDirectSet ) $this->set( 'm_strVariableValue', trim( stripcslashes( $arrValues['variable_value'] ) ) ); elseif( isset( $arrValues['variable_value'] ) ) $this->setVariableValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['variable_value'] ) : $arrValues['variable_value'] );
		if( isset( $arrValues['visitors'] ) && $boolDirectSet ) $this->set( 'm_fltVisitors', trim( $arrValues['visitors'] ) ); elseif( isset( $arrValues['visitors'] ) ) $this->setVisitors( $arrValues['visitors'] );
		if( isset( $arrValues['actions'] ) && $boolDirectSet ) $this->set( 'm_fltActions', trim( $arrValues['actions'] ) ); elseif( isset( $arrValues['actions'] ) ) $this->setActions( $arrValues['actions'] );
		if( isset( $arrValues['active'] ) && $boolDirectSet ) $this->set( 'm_intActive', trim( $arrValues['active'] ) ); elseif( isset( $arrValues['active'] ) ) $this->setActive( $arrValues['active'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMultivariateVariablesId( $intMultivariateVariablesId ) {
		$this->set( 'm_intMultivariateVariablesId', CStrings::strToIntDef( $intMultivariateVariablesId, NULL, false ) );
	}

	public function getMultivariateVariablesId() {
		return $this->m_intMultivariateVariablesId;
	}

	public function sqlMultivariateVariablesId() {
		return ( true == isset( $this->m_intMultivariateVariablesId ) ) ? ( string ) $this->m_intMultivariateVariablesId : 'NULL';
	}

	public function setVariableVersion( $strVariableVersion ) {
		$this->set( 'm_strVariableVersion', CStrings::strTrimDef( $strVariableVersion, 100, NULL, true ) );
	}

	public function getVariableVersion() {
		return $this->m_strVariableVersion;
	}

	public function sqlVariableVersion() {
		return ( true == isset( $this->m_strVariableVersion ) ) ? '\'' . addslashes( $this->m_strVariableVersion ) . '\'' : 'NULL';
	}

	public function setVariableValue( $strVariableValue ) {
		$this->set( 'm_strVariableValue', CStrings::strTrimDef( $strVariableValue, -1, NULL, true ) );
	}

	public function getVariableValue() {
		return $this->m_strVariableValue;
	}

	public function sqlVariableValue() {
		return ( true == isset( $this->m_strVariableValue ) ) ? '\'' . addslashes( $this->m_strVariableValue ) . '\'' : 'NULL';
	}

	public function setVisitors( $fltVisitors ) {
		$this->set( 'm_fltVisitors', CStrings::strToFloatDef( $fltVisitors, NULL, false, 0 ) );
	}

	public function getVisitors() {
		return $this->m_fltVisitors;
	}

	public function sqlVisitors() {
		return ( true == isset( $this->m_fltVisitors ) ) ? ( string ) $this->m_fltVisitors : 'NULL';
	}

	public function setActions( $fltActions ) {
		$this->set( 'm_fltActions', CStrings::strToFloatDef( $fltActions, NULL, false, 0 ) );
	}

	public function getActions() {
		return $this->m_fltActions;
	}

	public function sqlActions() {
		return ( true == isset( $this->m_fltActions ) ) ? ( string ) $this->m_fltActions : 'NULL';
	}

	public function setActive( $intActive ) {
		$this->set( 'm_intActive', CStrings::strToIntDef( $intActive, NULL, false ) );
	}

	public function getActive() {
		return $this->m_intActive;
	}

	public function sqlActive() {
		return ( true == isset( $this->m_intActive ) ) ? ( string ) $this->m_intActive : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'multivariate_variables_id' => $this->getMultivariateVariablesId(),
			'variable_version' => $this->getVariableVersion(),
			'variable_value' => $this->getVariableValue(),
			'visitors' => $this->getVisitors(),
			'actions' => $this->getActions(),
			'active' => $this->getActive()
		);
	}

}
?>