<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgEntities
 * Do not add any new functions to this class.
 */

class CBaseMktgEntities extends CEosPluralBase {

	/**
	 * @return CMktgEntity[]
	 */
	public static function fetchMktgEntities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgEntity::class, $objDatabase );
	}

	/**
	 * @return CMktgEntity
	 */
	public static function fetchMktgEntity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgEntity::class, $objDatabase );
	}

	public static function fetchMktgEntityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_entities', $objDatabase );
	}

	public static function fetchMktgEntityById( $intId, $objDatabase ) {
		return self::fetchMktgEntity( sprintf( 'SELECT * FROM mktg_entities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgEntitiesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchMktgEntities( sprintf( 'SELECT * FROM mktg_entities WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchMktgEntitiesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchMktgEntities( sprintf( 'SELECT * FROM mktg_entities WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchMktgEntitiesByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchMktgEntities( sprintf( 'SELECT * FROM mktg_entities WHERE applicant_id = %d', ( int ) $intApplicantId ), $objDatabase );
	}

	public static function fetchMktgEntitiesByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchMktgEntities( sprintf( 'SELECT * FROM mktg_entities WHERE application_id = %d', ( int ) $intApplicationId ), $objDatabase );
	}

}
?>