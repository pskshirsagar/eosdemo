<?php

class CBaseMktgConcessionType extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_concession_types';

	protected $m_intId;
	protected $m_strConcessionType;
	protected $m_strConcessionFrequency;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['concession_type'] ) && $boolDirectSet ) $this->set( 'm_strConcessionType', trim( $arrValues['concession_type'] ) ); elseif( isset( $arrValues['concession_type'] ) ) $this->setConcessionType( $arrValues['concession_type'] );
		if( isset( $arrValues['concession_frequency'] ) && $boolDirectSet ) $this->set( 'm_strConcessionFrequency', trim( stripcslashes( $arrValues['concession_frequency'] ) ) ); elseif( isset( $arrValues['concession_frequency'] ) ) $this->setConcessionFrequency( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['concession_frequency'] ) : $arrValues['concession_frequency'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setConcessionType( $strConcessionType ) {
		$this->set( 'm_strConcessionType', CStrings::strTrimDef( $strConcessionType, -1, NULL, true ) );
	}

	public function getConcessionType() {
		return $this->m_strConcessionType;
	}

	public function sqlConcessionType() {
		return ( true == isset( $this->m_strConcessionType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConcessionType ) : '\'' . addslashes( $this->m_strConcessionType ) . '\'' ) : 'NULL';
	}

	public function setConcessionFrequency( $strConcessionFrequency ) {
		$this->set( 'm_strConcessionFrequency', CStrings::strTrimDef( $strConcessionFrequency, -1, NULL, true ) );
	}

	public function getConcessionFrequency() {
		return $this->m_strConcessionFrequency;
	}

	public function sqlConcessionFrequency() {
		return ( true == isset( $this->m_strConcessionFrequency ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strConcessionFrequency ) : '\'' . addslashes( $this->m_strConcessionFrequency ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, concession_type, concession_frequency, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlConcessionType() . ', ' .
						$this->sqlConcessionFrequency() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_type = ' . $this->sqlConcessionType(). ',' ; } elseif( true == array_key_exists( 'ConcessionType', $this->getChangedColumns() ) ) { $strSql .= ' concession_type = ' . $this->sqlConcessionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' concession_frequency = ' . $this->sqlConcessionFrequency() ; } elseif( true == array_key_exists( 'ConcessionFrequency', $this->getChangedColumns() ) ) { $strSql .= ' concession_frequency = ' . $this->sqlConcessionFrequency() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'concession_type' => $this->getConcessionType(),
			'concession_frequency' => $this->getConcessionFrequency(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>