<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgProperties
 * Do not add any new functions to this class.
 */

class CBaseMktgProperties extends CEosPluralBase {

	/**
	 * @return CMktgProperty[]
	 */
	public static function fetchMktgProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgProperty::class, $objDatabase );
	}

	/**
	 * @return CMktgProperty
	 */
	public static function fetchMktgProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgProperty::class, $objDatabase );
	}

	public static function fetchMktgPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_properties', $objDatabase );
	}

	public static function fetchMktgPropertyById( $intId, $objDatabase ) {
		return self::fetchMktgProperty( sprintf( 'SELECT * FROM mktg_properties WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMktgPropertiesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgProperties( sprintf( 'SELECT * FROM mktg_properties WHERE company_id = %d', $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertiesByTimeZoneId( $intTimeZoneId, $objDatabase ) {
		return self::fetchMktgProperties( sprintf( 'SELECT * FROM mktg_properties WHERE time_zone_id = %d', $intTimeZoneId ), $objDatabase );
	}

	public static function fetchMktgPropertiesBySourcePropertyId( $intSourcePropertyId, $objDatabase ) {
		return self::fetchMktgProperties( sprintf( 'SELECT * FROM mktg_properties WHERE source_property_id = %d', $intSourcePropertyId ), $objDatabase );
	}

}
?>