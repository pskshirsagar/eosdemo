<?php

class CBaseMktgEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_employees';

	protected $m_intId;
	protected $m_intManagerId;
	protected $m_strEmployeeStatusType;
	protected $m_strDepartment;
	protected $m_strNameFull;
	protected $m_strPhoneExtension;
	protected $m_strBirthDate;
	protected $m_strEmailAddress;
	protected $m_strPermanentEmailAddress;
	protected $m_strGmailUsername;
	protected $m_strGmailPasswordEncrypted;
	protected $m_strDateStarted;
	protected $m_strDateTerminated;
	protected $m_strMarketingBlurb;
	protected $m_intMaxTaskVotes;
	protected $m_strDnsHandle;
	protected $m_strWorkStationIpAddress;
	protected $m_strVpnIpAddress;
	protected $m_strQaIpAddress;
	protected $m_strNotes;
	protected $m_strEmailSignature;
	protected $m_intIsProjectManager;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strDesignation;

	public function __construct() {
		parent::__construct();

		$this->m_intMaxTaskVotes = '5';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['manager_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerId', trim( $arrValues['manager_id'] ) ); elseif( isset( $arrValues['manager_id'] ) ) $this->setManagerId( $arrValues['manager_id'] );
		if( isset( $arrValues['employee_status_type'] ) && $boolDirectSet ) $this->set( 'm_strEmployeeStatusType', trim( stripcslashes( $arrValues['employee_status_type'] ) ) ); elseif( isset( $arrValues['employee_status_type'] ) ) $this->setEmployeeStatusType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employee_status_type'] ) : $arrValues['employee_status_type'] );
		if( isset( $arrValues['department'] ) && $boolDirectSet ) $this->set( 'm_strDepartment', trim( stripcslashes( $arrValues['department'] ) ) ); elseif( isset( $arrValues['department'] ) ) $this->setDepartment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['department'] ) : $arrValues['department'] );
		if( isset( $arrValues['name_full'] ) && $boolDirectSet ) $this->set( 'm_strNameFull', trim( stripcslashes( $arrValues['name_full'] ) ) ); elseif( isset( $arrValues['name_full'] ) ) $this->setNameFull( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_full'] ) : $arrValues['name_full'] );
		if( isset( $arrValues['phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneExtension', trim( stripcslashes( $arrValues['phone_extension'] ) ) ); elseif( isset( $arrValues['phone_extension'] ) ) $this->setPhoneExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_extension'] ) : $arrValues['phone_extension'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['permanent_email_address'] ) && $boolDirectSet ) $this->set( 'm_strPermanentEmailAddress', trim( stripcslashes( $arrValues['permanent_email_address'] ) ) ); elseif( isset( $arrValues['permanent_email_address'] ) ) $this->setPermanentEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['permanent_email_address'] ) : $arrValues['permanent_email_address'] );
		if( isset( $arrValues['gmail_username'] ) && $boolDirectSet ) $this->set( 'm_strGmailUsername', trim( stripcslashes( $arrValues['gmail_username'] ) ) ); elseif( isset( $arrValues['gmail_username'] ) ) $this->setGmailUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gmail_username'] ) : $arrValues['gmail_username'] );
		if( isset( $arrValues['gmail_password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strGmailPasswordEncrypted', trim( stripcslashes( $arrValues['gmail_password_encrypted'] ) ) ); elseif( isset( $arrValues['gmail_password_encrypted'] ) ) $this->setGmailPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gmail_password_encrypted'] ) : $arrValues['gmail_password_encrypted'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['marketing_blurb'] ) && $boolDirectSet ) $this->set( 'm_strMarketingBlurb', trim( stripcslashes( $arrValues['marketing_blurb'] ) ) ); elseif( isset( $arrValues['marketing_blurb'] ) ) $this->setMarketingBlurb( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_blurb'] ) : $arrValues['marketing_blurb'] );
		if( isset( $arrValues['max_task_votes'] ) && $boolDirectSet ) $this->set( 'm_intMaxTaskVotes', trim( $arrValues['max_task_votes'] ) ); elseif( isset( $arrValues['max_task_votes'] ) ) $this->setMaxTaskVotes( $arrValues['max_task_votes'] );
		if( isset( $arrValues['dns_handle'] ) && $boolDirectSet ) $this->set( 'm_strDnsHandle', trim( stripcslashes( $arrValues['dns_handle'] ) ) ); elseif( isset( $arrValues['dns_handle'] ) ) $this->setDnsHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dns_handle'] ) : $arrValues['dns_handle'] );
		if( isset( $arrValues['work_station_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strWorkStationIpAddress', trim( stripcslashes( $arrValues['work_station_ip_address'] ) ) ); elseif( isset( $arrValues['work_station_ip_address'] ) ) $this->setWorkStationIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['work_station_ip_address'] ) : $arrValues['work_station_ip_address'] );
		if( isset( $arrValues['vpn_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strVpnIpAddress', trim( stripcslashes( $arrValues['vpn_ip_address'] ) ) ); elseif( isset( $arrValues['vpn_ip_address'] ) ) $this->setVpnIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vpn_ip_address'] ) : $arrValues['vpn_ip_address'] );
		if( isset( $arrValues['qa_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strQaIpAddress', trim( stripcslashes( $arrValues['qa_ip_address'] ) ) ); elseif( isset( $arrValues['qa_ip_address'] ) ) $this->setQaIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qa_ip_address'] ) : $arrValues['qa_ip_address'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['email_signature'] ) && $boolDirectSet ) $this->set( 'm_strEmailSignature', trim( stripcslashes( $arrValues['email_signature'] ) ) ); elseif( isset( $arrValues['email_signature'] ) ) $this->setEmailSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_signature'] ) : $arrValues['email_signature'] );
		if( isset( $arrValues['is_project_manager'] ) && $boolDirectSet ) $this->set( 'm_intIsProjectManager', trim( $arrValues['is_project_manager'] ) ); elseif( isset( $arrValues['is_project_manager'] ) ) $this->setIsProjectManager( $arrValues['is_project_manager'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['designation'] ) && $boolDirectSet ) $this->set( 'm_strDesignation', trim( stripcslashes( $arrValues['designation'] ) ) ); elseif( isset( $arrValues['designation'] ) ) $this->setDesignation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['designation'] ) : $arrValues['designation'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setManagerId( $intManagerId ) {
		$this->set( 'm_intManagerId', CStrings::strToIntDef( $intManagerId, NULL, false ) );
	}

	public function getManagerId() {
		return $this->m_intManagerId;
	}

	public function sqlManagerId() {
		return ( true == isset( $this->m_intManagerId ) ) ? ( string ) $this->m_intManagerId : 'NULL';
	}

	public function setEmployeeStatusType( $strEmployeeStatusType ) {
		$this->set( 'm_strEmployeeStatusType', CStrings::strTrimDef( $strEmployeeStatusType, 50, NULL, true ) );
	}

	public function getEmployeeStatusType() {
		return $this->m_strEmployeeStatusType;
	}

	public function sqlEmployeeStatusType() {
		return ( true == isset( $this->m_strEmployeeStatusType ) ) ? '\'' . addslashes( $this->m_strEmployeeStatusType ) . '\'' : 'NULL';
	}

	public function setDepartment( $strDepartment ) {
		$this->set( 'm_strDepartment', CStrings::strTrimDef( $strDepartment, 50, NULL, true ) );
	}

	public function getDepartment() {
		return $this->m_strDepartment;
	}

	public function sqlDepartment() {
		return ( true == isset( $this->m_strDepartment ) ) ? '\'' . addslashes( $this->m_strDepartment ) . '\'' : 'NULL';
	}

	public function setNameFull( $strNameFull ) {
		$this->set( 'm_strNameFull', CStrings::strTrimDef( $strNameFull, 100, NULL, true ) );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function sqlNameFull() {
		return ( true == isset( $this->m_strNameFull ) ) ? '\'' . addslashes( $this->m_strNameFull ) . '\'' : 'NULL';
	}

	public function setPhoneExtension( $strPhoneExtension ) {
		$this->set( 'm_strPhoneExtension', CStrings::strTrimDef( $strPhoneExtension, 10, NULL, true ) );
	}

	public function getPhoneExtension() {
		return $this->m_strPhoneExtension;
	}

	public function sqlPhoneExtension() {
		return ( true == isset( $this->m_strPhoneExtension ) ) ? '\'' . addslashes( $this->m_strPhoneExtension ) . '\'' : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPermanentEmailAddress( $strPermanentEmailAddress ) {
		$this->set( 'm_strPermanentEmailAddress', CStrings::strTrimDef( $strPermanentEmailAddress, 240, NULL, true ) );
	}

	public function getPermanentEmailAddress() {
		return $this->m_strPermanentEmailAddress;
	}

	public function sqlPermanentEmailAddress() {
		return ( true == isset( $this->m_strPermanentEmailAddress ) ) ? '\'' . addslashes( $this->m_strPermanentEmailAddress ) . '\'' : 'NULL';
	}

	public function setGmailUsername( $strGmailUsername ) {
		$this->set( 'm_strGmailUsername', CStrings::strTrimDef( $strGmailUsername, 240, NULL, true ) );
	}

	public function getGmailUsername() {
		return $this->m_strGmailUsername;
	}

	public function sqlGmailUsername() {
		return ( true == isset( $this->m_strGmailUsername ) ) ? '\'' . addslashes( $this->m_strGmailUsername ) . '\'' : 'NULL';
	}

	public function setGmailPasswordEncrypted( $strGmailPasswordEncrypted ) {
		$this->set( 'm_strGmailPasswordEncrypted', CStrings::strTrimDef( $strGmailPasswordEncrypted, 240, NULL, true ) );
	}

	public function getGmailPasswordEncrypted() {
		return $this->m_strGmailPasswordEncrypted;
	}

	public function sqlGmailPasswordEncrypted() {
		return ( true == isset( $this->m_strGmailPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strGmailPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setMarketingBlurb( $strMarketingBlurb ) {
		$this->set( 'm_strMarketingBlurb', CStrings::strTrimDef( $strMarketingBlurb, -1, NULL, true ) );
	}

	public function getMarketingBlurb() {
		return $this->m_strMarketingBlurb;
	}

	public function sqlMarketingBlurb() {
		return ( true == isset( $this->m_strMarketingBlurb ) ) ? '\'' . addslashes( $this->m_strMarketingBlurb ) . '\'' : 'NULL';
	}

	public function setMaxTaskVotes( $intMaxTaskVotes ) {
		$this->set( 'm_intMaxTaskVotes', CStrings::strToIntDef( $intMaxTaskVotes, NULL, false ) );
	}

	public function getMaxTaskVotes() {
		return $this->m_intMaxTaskVotes;
	}

	public function sqlMaxTaskVotes() {
		return ( true == isset( $this->m_intMaxTaskVotes ) ) ? ( string ) $this->m_intMaxTaskVotes : '5';
	}

	public function setDnsHandle( $strDnsHandle ) {
		$this->set( 'm_strDnsHandle', CStrings::strTrimDef( $strDnsHandle, 240, NULL, true ) );
	}

	public function getDnsHandle() {
		return $this->m_strDnsHandle;
	}

	public function sqlDnsHandle() {
		return ( true == isset( $this->m_strDnsHandle ) ) ? '\'' . addslashes( $this->m_strDnsHandle ) . '\'' : 'NULL';
	}

	public function setWorkStationIpAddress( $strWorkStationIpAddress ) {
		$this->set( 'm_strWorkStationIpAddress', CStrings::strTrimDef( $strWorkStationIpAddress, 23, NULL, true ) );
	}

	public function getWorkStationIpAddress() {
		return $this->m_strWorkStationIpAddress;
	}

	public function sqlWorkStationIpAddress() {
		return ( true == isset( $this->m_strWorkStationIpAddress ) ) ? '\'' . addslashes( $this->m_strWorkStationIpAddress ) . '\'' : 'NULL';
	}

	public function setVpnIpAddress( $strVpnIpAddress ) {
		$this->set( 'm_strVpnIpAddress', CStrings::strTrimDef( $strVpnIpAddress, 23, NULL, true ) );
	}

	public function getVpnIpAddress() {
		return $this->m_strVpnIpAddress;
	}

	public function sqlVpnIpAddress() {
		return ( true == isset( $this->m_strVpnIpAddress ) ) ? '\'' . addslashes( $this->m_strVpnIpAddress ) . '\'' : 'NULL';
	}

	public function setQaIpAddress( $strQaIpAddress ) {
		$this->set( 'm_strQaIpAddress', CStrings::strTrimDef( $strQaIpAddress, 23, NULL, true ) );
	}

	public function getQaIpAddress() {
		return $this->m_strQaIpAddress;
	}

	public function sqlQaIpAddress() {
		return ( true == isset( $this->m_strQaIpAddress ) ) ? '\'' . addslashes( $this->m_strQaIpAddress ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setEmailSignature( $strEmailSignature ) {
		$this->set( 'm_strEmailSignature', CStrings::strTrimDef( $strEmailSignature, 2000, NULL, true ) );
	}

	public function getEmailSignature() {
		return $this->m_strEmailSignature;
	}

	public function sqlEmailSignature() {
		return ( true == isset( $this->m_strEmailSignature ) ) ? '\'' . addslashes( $this->m_strEmailSignature ) . '\'' : 'NULL';
	}

	public function setIsProjectManager( $intIsProjectManager ) {
		$this->set( 'm_intIsProjectManager', CStrings::strToIntDef( $intIsProjectManager, NULL, false ) );
	}

	public function getIsProjectManager() {
		return $this->m_intIsProjectManager;
	}

	public function sqlIsProjectManager() {
		return ( true == isset( $this->m_intIsProjectManager ) ) ? ( string ) $this->m_intIsProjectManager : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDesignation( $strDesignation ) {
		$this->set( 'm_strDesignation', CStrings::strTrimDef( $strDesignation, 100, NULL, true ) );
	}

	public function getDesignation() {
		return $this->m_strDesignation;
	}

	public function sqlDesignation() {
		return ( true == isset( $this->m_strDesignation ) ) ? '\'' . addslashes( $this->m_strDesignation ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, manager_id, employee_status_type, department, name_full, phone_extension, birth_date, email_address, permanent_email_address, gmail_username, gmail_password_encrypted, date_started, date_terminated, marketing_blurb, max_task_votes, dns_handle, work_station_ip_address, vpn_ip_address, qa_ip_address, notes, email_signature, is_project_manager, created_on, updated_on, designation )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlManagerId() . ', ' .
 						$this->sqlEmployeeStatusType() . ', ' .
 						$this->sqlDepartment() . ', ' .
 						$this->sqlNameFull() . ', ' .
 						$this->sqlPhoneExtension() . ', ' .
 						$this->sqlBirthDate() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPermanentEmailAddress() . ', ' .
 						$this->sqlGmailUsername() . ', ' .
 						$this->sqlGmailPasswordEncrypted() . ', ' .
 						$this->sqlDateStarted() . ', ' .
 						$this->sqlDateTerminated() . ', ' .
 						$this->sqlMarketingBlurb() . ', ' .
 						$this->sqlMaxTaskVotes() . ', ' .
 						$this->sqlDnsHandle() . ', ' .
 						$this->sqlWorkStationIpAddress() . ', ' .
 						$this->sqlVpnIpAddress() . ', ' .
 						$this->sqlQaIpAddress() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlEmailSignature() . ', ' .
 						$this->sqlIsProjectManager() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlDesignation() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_id = ' . $this->sqlManagerId() . ','; } elseif( true == array_key_exists( 'ManagerId', $this->getChangedColumns() ) ) { $strSql .= ' manager_id = ' . $this->sqlManagerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_status_type = ' . $this->sqlEmployeeStatusType() . ','; } elseif( true == array_key_exists( 'EmployeeStatusType', $this->getChangedColumns() ) ) { $strSql .= ' employee_status_type = ' . $this->sqlEmployeeStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department = ' . $this->sqlDepartment() . ','; } elseif( true == array_key_exists( 'Department', $this->getChangedColumns() ) ) { $strSql .= ' department = ' . $this->sqlDepartment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_full = ' . $this->sqlNameFull() . ','; } elseif( true == array_key_exists( 'NameFull', $this->getChangedColumns() ) ) { $strSql .= ' name_full = ' . $this->sqlNameFull() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension() . ','; } elseif( true == array_key_exists( 'PhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permanent_email_address = ' . $this->sqlPermanentEmailAddress() . ','; } elseif( true == array_key_exists( 'PermanentEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' permanent_email_address = ' . $this->sqlPermanentEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gmail_username = ' . $this->sqlGmailUsername() . ','; } elseif( true == array_key_exists( 'GmailUsername', $this->getChangedColumns() ) ) { $strSql .= ' gmail_username = ' . $this->sqlGmailUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gmail_password_encrypted = ' . $this->sqlGmailPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'GmailPasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' gmail_password_encrypted = ' . $this->sqlGmailPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_blurb = ' . $this->sqlMarketingBlurb() . ','; } elseif( true == array_key_exists( 'MarketingBlurb', $this->getChangedColumns() ) ) { $strSql .= ' marketing_blurb = ' . $this->sqlMarketingBlurb() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_task_votes = ' . $this->sqlMaxTaskVotes() . ','; } elseif( true == array_key_exists( 'MaxTaskVotes', $this->getChangedColumns() ) ) { $strSql .= ' max_task_votes = ' . $this->sqlMaxTaskVotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle() . ','; } elseif( true == array_key_exists( 'DnsHandle', $this->getChangedColumns() ) ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_station_ip_address = ' . $this->sqlWorkStationIpAddress() . ','; } elseif( true == array_key_exists( 'WorkStationIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' work_station_ip_address = ' . $this->sqlWorkStationIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vpn_ip_address = ' . $this->sqlVpnIpAddress() . ','; } elseif( true == array_key_exists( 'VpnIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' vpn_ip_address = ' . $this->sqlVpnIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_ip_address = ' . $this->sqlQaIpAddress() . ','; } elseif( true == array_key_exists( 'QaIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' qa_ip_address = ' . $this->sqlQaIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_signature = ' . $this->sqlEmailSignature() . ','; } elseif( true == array_key_exists( 'EmailSignature', $this->getChangedColumns() ) ) { $strSql .= ' email_signature = ' . $this->sqlEmailSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_project_manager = ' . $this->sqlIsProjectManager() . ','; } elseif( true == array_key_exists( 'IsProjectManager', $this->getChangedColumns() ) ) { $strSql .= ' is_project_manager = ' . $this->sqlIsProjectManager() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation = ' . $this->sqlDesignation() . ','; } elseif( true == array_key_exists( 'Designation', $this->getChangedColumns() ) ) { $strSql .= ' designation = ' . $this->sqlDesignation() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'manager_id' => $this->getManagerId(),
			'employee_status_type' => $this->getEmployeeStatusType(),
			'department' => $this->getDepartment(),
			'name_full' => $this->getNameFull(),
			'phone_extension' => $this->getPhoneExtension(),
			'birth_date' => $this->getBirthDate(),
			'email_address' => $this->getEmailAddress(),
			'permanent_email_address' => $this->getPermanentEmailAddress(),
			'gmail_username' => $this->getGmailUsername(),
			'gmail_password_encrypted' => $this->getGmailPasswordEncrypted(),
			'date_started' => $this->getDateStarted(),
			'date_terminated' => $this->getDateTerminated(),
			'marketing_blurb' => $this->getMarketingBlurb(),
			'max_task_votes' => $this->getMaxTaskVotes(),
			'dns_handle' => $this->getDnsHandle(),
			'work_station_ip_address' => $this->getWorkStationIpAddress(),
			'vpn_ip_address' => $this->getVpnIpAddress(),
			'qa_ip_address' => $this->getQaIpAddress(),
			'notes' => $this->getNotes(),
			'email_signature' => $this->getEmailSignature(),
			'is_project_manager' => $this->getIsProjectManager(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'designation' => $this->getDesignation()
		);
	}

}
?>