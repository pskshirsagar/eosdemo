<?php

class CBaseMktgScraperServer extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_scraper_servers';

	protected $m_intId;
	protected $m_strHost;
	protected $m_intPort;
	protected $m_strPath;
	protected $m_strLastFailedOn;
	protected $m_intFailCount;
	protected $m_intIsActive;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intFailCount = '0';
		$this->m_intIsActive = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['host'] ) && $boolDirectSet ) $this->set( 'm_strHost', trim( stripcslashes( $arrValues['host'] ) ) ); elseif( isset( $arrValues['host'] ) ) $this->setHost( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['host'] ) : $arrValues['host'] );
		if( isset( $arrValues['port'] ) && $boolDirectSet ) $this->set( 'm_intPort', trim( $arrValues['port'] ) ); elseif( isset( $arrValues['port'] ) ) $this->setPort( $arrValues['port'] );
		if( isset( $arrValues['path'] ) && $boolDirectSet ) $this->set( 'm_strPath', trim( stripcslashes( $arrValues['path'] ) ) ); elseif( isset( $arrValues['path'] ) ) $this->setPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['path'] ) : $arrValues['path'] );
		if( isset( $arrValues['last_failed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastFailedOn', trim( $arrValues['last_failed_on'] ) ); elseif( isset( $arrValues['last_failed_on'] ) ) $this->setLastFailedOn( $arrValues['last_failed_on'] );
		if( isset( $arrValues['fail_count'] ) && $boolDirectSet ) $this->set( 'm_intFailCount', trim( $arrValues['fail_count'] ) ); elseif( isset( $arrValues['fail_count'] ) ) $this->setFailCount( $arrValues['fail_count'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHost( $strHost ) {
		$this->set( 'm_strHost', CStrings::strTrimDef( $strHost, 100, NULL, true ) );
	}

	public function getHost() {
		return $this->m_strHost;
	}

	public function sqlHost() {
		return ( true == isset( $this->m_strHost ) ) ? '\'' . addslashes( $this->m_strHost ) . '\'' : 'NULL';
	}

	public function setPort( $intPort ) {
		$this->set( 'm_intPort', CStrings::strToIntDef( $intPort, NULL, false ) );
	}

	public function getPort() {
		return $this->m_intPort;
	}

	public function sqlPort() {
		return ( true == isset( $this->m_intPort ) ) ? ( string ) $this->m_intPort : 'NULL';
	}

	public function setPath( $strPath ) {
		$this->set( 'm_strPath', CStrings::strTrimDef( $strPath, 500, NULL, true ) );
	}

	public function getPath() {
		return $this->m_strPath;
	}

	public function sqlPath() {
		return ( true == isset( $this->m_strPath ) ) ? '\'' . addslashes( $this->m_strPath ) . '\'' : 'NULL';
	}

	public function setLastFailedOn( $strLastFailedOn ) {
		$this->set( 'm_strLastFailedOn', CStrings::strTrimDef( $strLastFailedOn, -1, NULL, true ) );
	}

	public function getLastFailedOn() {
		return $this->m_strLastFailedOn;
	}

	public function sqlLastFailedOn() {
		return ( true == isset( $this->m_strLastFailedOn ) ) ? '\'' . $this->m_strLastFailedOn . '\'' : 'NULL';
	}

	public function setFailCount( $intFailCount ) {
		$this->set( 'm_intFailCount', CStrings::strToIntDef( $intFailCount, NULL, false ) );
	}

	public function getFailCount() {
		return $this->m_intFailCount;
	}

	public function sqlFailCount() {
		return ( true == isset( $this->m_intFailCount ) ) ? ( string ) $this->m_intFailCount : '0';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, host, port, path, last_failed_on, fail_count, is_active, created_on, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlHost() . ', ' .
 						$this->sqlPort() . ', ' .
 						$this->sqlPath() . ', ' .
 						$this->sqlLastFailedOn() . ', ' .
 						$this->sqlFailCount() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' host = ' . $this->sqlHost() . ','; } elseif( true == array_key_exists( 'Host', $this->getChangedColumns() ) ) { $strSql .= ' host = ' . $this->sqlHost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port = ' . $this->sqlPort() . ','; } elseif( true == array_key_exists( 'Port', $this->getChangedColumns() ) ) { $strSql .= ' port = ' . $this->sqlPort() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' path = ' . $this->sqlPath() . ','; } elseif( true == array_key_exists( 'Path', $this->getChangedColumns() ) ) { $strSql .= ' path = ' . $this->sqlPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn() . ','; } elseif( true == array_key_exists( 'LastFailedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_failed_on = ' . $this->sqlLastFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fail_count = ' . $this->sqlFailCount() . ','; } elseif( true == array_key_exists( 'FailCount', $this->getChangedColumns() ) ) { $strSql .= ' fail_count = ' . $this->sqlFailCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'host' => $this->getHost(),
			'port' => $this->getPort(),
			'path' => $this->getPath(),
			'last_failed_on' => $this->getLastFailedOn(),
			'fail_count' => $this->getFailCount(),
			'is_active' => $this->getIsActive(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>