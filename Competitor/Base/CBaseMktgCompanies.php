<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanies
 * Do not add any new functions to this class.
 */

class CBaseMktgCompanies extends CEosPluralBase {

	/**
	 * @return CMktgCompany[]
	 */
	public static function fetchMktgCompanies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgCompany', $objDatabase );
	}

	/**
	 * @return CMktgCompany
	 */
	public static function fetchMktgCompany( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgCompany', $objDatabase );
	}

	public static function fetchMktgCompanyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_companies', $objDatabase );
	}

	public static function fetchMktgCompanyById( $intId, $objDatabase ) {
		return self::fetchMktgCompany( sprintf( 'SELECT * FROM mktg_companies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgCompaniesByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchMktgCompanies( sprintf( 'SELECT * FROM mktg_companies WHERE entity_id = %d', ( int ) $intEntityId ), $objDatabase );
	}

	public static function fetchMktgCompaniesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchMktgCompanies( sprintf( 'SELECT * FROM mktg_companies WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>