<?php

class CBaseMktgCompanyMediaFile extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_company_media_files';

	protected $m_intId;
	protected $m_intRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_strCompanyMediaFolder;
	protected $m_strMediaFileType;
	protected $m_strMediaType;
	protected $m_intIsHidden;
	protected $m_strMediaCaption;
	protected $m_strMediaDescription;
	protected $m_strMediaData;
	protected $m_strMediaWidth;
	protected $m_strMediaHeight;
	protected $m_strMediaAlt;
	protected $m_strFullsizeUri;
	protected $m_strThumbnailUri;
	protected $m_strExternalUri;
	protected $m_strFileName;
	protected $m_intFileSize;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsHidden = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_intRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['company_media_folder'] ) && $boolDirectSet ) $this->set( 'm_strCompanyMediaFolder', trim( stripcslashes( $arrValues['company_media_folder'] ) ) ); elseif( isset( $arrValues['company_media_folder'] ) ) $this->setCompanyMediaFolder( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_media_folder'] ) : $arrValues['company_media_folder'] );
		if( isset( $arrValues['media_file_type'] ) && $boolDirectSet ) $this->set( 'm_strMediaFileType', trim( stripcslashes( $arrValues['media_file_type'] ) ) ); elseif( isset( $arrValues['media_file_type'] ) ) $this->setMediaFileType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_file_type'] ) : $arrValues['media_file_type'] );
		if( isset( $arrValues['media_type'] ) && $boolDirectSet ) $this->set( 'm_strMediaType', trim( stripcslashes( $arrValues['media_type'] ) ) ); elseif( isset( $arrValues['media_type'] ) ) $this->setMediaType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_type'] ) : $arrValues['media_type'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_intIsHidden', trim( $arrValues['is_hidden'] ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( $arrValues['is_hidden'] );
		if( isset( $arrValues['media_caption'] ) && $boolDirectSet ) $this->set( 'm_strMediaCaption', trim( stripcslashes( $arrValues['media_caption'] ) ) ); elseif( isset( $arrValues['media_caption'] ) ) $this->setMediaCaption( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_caption'] ) : $arrValues['media_caption'] );
		if( isset( $arrValues['media_description'] ) && $boolDirectSet ) $this->set( 'm_strMediaDescription', trim( stripcslashes( $arrValues['media_description'] ) ) ); elseif( isset( $arrValues['media_description'] ) ) $this->setMediaDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_description'] ) : $arrValues['media_description'] );
		if( isset( $arrValues['media_data'] ) && $boolDirectSet ) $this->set( 'm_strMediaData', trim( stripcslashes( $arrValues['media_data'] ) ) ); elseif( isset( $arrValues['media_data'] ) ) $this->setMediaData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_data'] ) : $arrValues['media_data'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_strMediaWidth', trim( stripcslashes( $arrValues['media_width'] ) ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_width'] ) : $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_strMediaHeight', trim( stripcslashes( $arrValues['media_height'] ) ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_height'] ) : $arrValues['media_height'] );
		if( isset( $arrValues['media_alt'] ) && $boolDirectSet ) $this->set( 'm_strMediaAlt', trim( stripcslashes( $arrValues['media_alt'] ) ) ); elseif( isset( $arrValues['media_alt'] ) ) $this->setMediaAlt( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['media_alt'] ) : $arrValues['media_alt'] );
		if( isset( $arrValues['fullsize_uri'] ) && $boolDirectSet ) $this->set( 'm_strFullsizeUri', trim( stripcslashes( $arrValues['fullsize_uri'] ) ) ); elseif( isset( $arrValues['fullsize_uri'] ) ) $this->setFullsizeUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fullsize_uri'] ) : $arrValues['fullsize_uri'] );
		if( isset( $arrValues['thumbnail_uri'] ) && $boolDirectSet ) $this->set( 'm_strThumbnailUri', trim( stripcslashes( $arrValues['thumbnail_uri'] ) ) ); elseif( isset( $arrValues['thumbnail_uri'] ) ) $this->setThumbnailUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['thumbnail_uri'] ) : $arrValues['thumbnail_uri'] );
		if( isset( $arrValues['external_uri'] ) && $boolDirectSet ) $this->set( 'm_strExternalUri', trim( stripcslashes( $arrValues['external_uri'] ) ) ); elseif( isset( $arrValues['external_uri'] ) ) $this->setExternalUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_uri'] ) : $arrValues['external_uri'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_intFileSize', trim( $arrValues['file_size'] ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( $arrValues['file_size'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->set( 'm_intRemotePrimaryKey', CStrings::strToIntDef( $intRemotePrimaryKey, NULL, false ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_intRemotePrimaryKey ) ) ? ( string ) $this->m_intRemotePrimaryKey : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setCompanyMediaFolder( $strCompanyMediaFolder ) {
		$this->set( 'm_strCompanyMediaFolder', CStrings::strTrimDef( $strCompanyMediaFolder, 50, NULL, true ) );
	}

	public function getCompanyMediaFolder() {
		return $this->m_strCompanyMediaFolder;
	}

	public function sqlCompanyMediaFolder() {
		return ( true == isset( $this->m_strCompanyMediaFolder ) ) ? '\'' . addslashes( $this->m_strCompanyMediaFolder ) . '\'' : 'NULL';
	}

	public function setMediaFileType( $strMediaFileType ) {
		$this->set( 'm_strMediaFileType', CStrings::strTrimDef( $strMediaFileType, 50, NULL, true ) );
	}

	public function getMediaFileType() {
		return $this->m_strMediaFileType;
	}

	public function sqlMediaFileType() {
		return ( true == isset( $this->m_strMediaFileType ) ) ? '\'' . addslashes( $this->m_strMediaFileType ) . '\'' : 'NULL';
	}

	public function setMediaType( $strMediaType ) {
		$this->set( 'm_strMediaType', CStrings::strTrimDef( $strMediaType, 50, NULL, true ) );
	}

	public function getMediaType() {
		return $this->m_strMediaType;
	}

	public function sqlMediaType() {
		return ( true == isset( $this->m_strMediaType ) ) ? '\'' . addslashes( $this->m_strMediaType ) . '\'' : 'NULL';
	}

	public function setIsHidden( $intIsHidden ) {
		$this->set( 'm_intIsHidden', CStrings::strToIntDef( $intIsHidden, NULL, false ) );
	}

	public function getIsHidden() {
		return $this->m_intIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_intIsHidden ) ) ? ( string ) $this->m_intIsHidden : '0';
	}

	public function setMediaCaption( $strMediaCaption ) {
		$this->set( 'm_strMediaCaption', CStrings::strTrimDef( $strMediaCaption, 240, NULL, true ) );
	}

	public function getMediaCaption() {
		return $this->m_strMediaCaption;
	}

	public function sqlMediaCaption() {
		return ( true == isset( $this->m_strMediaCaption ) ) ? '\'' . addslashes( $this->m_strMediaCaption ) . '\'' : 'NULL';
	}

	public function setMediaDescription( $strMediaDescription ) {
		$this->set( 'm_strMediaDescription', CStrings::strTrimDef( $strMediaDescription, 1000, NULL, true ) );
	}

	public function getMediaDescription() {
		return $this->m_strMediaDescription;
	}

	public function sqlMediaDescription() {
		return ( true == isset( $this->m_strMediaDescription ) ) ? '\'' . addslashes( $this->m_strMediaDescription ) . '\'' : 'NULL';
	}

	public function setMediaData( $strMediaData ) {
		$this->set( 'm_strMediaData', CStrings::strTrimDef( $strMediaData, -1, NULL, true ) );
	}

	public function getMediaData() {
		return $this->m_strMediaData;
	}

	public function sqlMediaData() {
		return ( true == isset( $this->m_strMediaData ) ) ? '\'' . addslashes( $this->m_strMediaData ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $strMediaWidth ) {
		$this->set( 'm_strMediaWidth', CStrings::strTrimDef( $strMediaWidth, 20, NULL, true ) );
	}

	public function getMediaWidth() {
		return $this->m_strMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_strMediaWidth ) ) ? '\'' . addslashes( $this->m_strMediaWidth ) . '\'' : 'NULL';
	}

	public function setMediaHeight( $strMediaHeight ) {
		$this->set( 'm_strMediaHeight', CStrings::strTrimDef( $strMediaHeight, 20, NULL, true ) );
	}

	public function getMediaHeight() {
		return $this->m_strMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_strMediaHeight ) ) ? '\'' . addslashes( $this->m_strMediaHeight ) . '\'' : 'NULL';
	}

	public function setMediaAlt( $strMediaAlt ) {
		$this->set( 'm_strMediaAlt', CStrings::strTrimDef( $strMediaAlt, 240, NULL, true ) );
	}

	public function getMediaAlt() {
		return $this->m_strMediaAlt;
	}

	public function sqlMediaAlt() {
		return ( true == isset( $this->m_strMediaAlt ) ) ? '\'' . addslashes( $this->m_strMediaAlt ) . '\'' : 'NULL';
	}

	public function setFullsizeUri( $strFullsizeUri ) {
		$this->set( 'm_strFullsizeUri', CStrings::strTrimDef( $strFullsizeUri, 4096, NULL, true ) );
	}

	public function getFullsizeUri() {
		return $this->m_strFullsizeUri;
	}

	public function sqlFullsizeUri() {
		return ( true == isset( $this->m_strFullsizeUri ) ) ? '\'' . addslashes( $this->m_strFullsizeUri ) . '\'' : 'NULL';
	}

	public function setThumbnailUri( $strThumbnailUri ) {
		$this->set( 'm_strThumbnailUri', CStrings::strTrimDef( $strThumbnailUri, 4096, NULL, true ) );
	}

	public function getThumbnailUri() {
		return $this->m_strThumbnailUri;
	}

	public function sqlThumbnailUri() {
		return ( true == isset( $this->m_strThumbnailUri ) ) ? '\'' . addslashes( $this->m_strThumbnailUri ) . '\'' : 'NULL';
	}

	public function setExternalUri( $strExternalUri ) {
		$this->set( 'm_strExternalUri', CStrings::strTrimDef( $strExternalUri, 4096, NULL, true ) );
	}

	public function getExternalUri() {
		return $this->m_strExternalUri;
	}

	public function sqlExternalUri() {
		return ( true == isset( $this->m_strExternalUri ) ) ? '\'' . addslashes( $this->m_strExternalUri ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFileSize( $intFileSize ) {
		$this->set( 'm_intFileSize', CStrings::strToIntDef( $intFileSize, NULL, false ) );
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_intFileSize ) ) ? ( string ) $this->m_intFileSize : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, company_media_folder, media_file_type, media_type, is_hidden, media_caption, media_description, media_data, media_width, media_height, media_alt, fullsize_uri, thumbnail_uri, external_uri, file_name, file_size, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlCompanyMediaFolder() . ', ' .
 						$this->sqlMediaFileType() . ', ' .
 						$this->sqlMediaType() . ', ' .
 						$this->sqlIsHidden() . ', ' .
 						$this->sqlMediaCaption() . ', ' .
 						$this->sqlMediaDescription() . ', ' .
 						$this->sqlMediaData() . ', ' .
 						$this->sqlMediaWidth() . ', ' .
 						$this->sqlMediaHeight() . ', ' .
 						$this->sqlMediaAlt() . ', ' .
 						$this->sqlFullsizeUri() . ', ' .
 						$this->sqlThumbnailUri() . ', ' .
 						$this->sqlExternalUri() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFileSize() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_media_folder = ' . $this->sqlCompanyMediaFolder() . ','; } elseif( true == array_key_exists( 'CompanyMediaFolder', $this->getChangedColumns() ) ) { $strSql .= ' company_media_folder = ' . $this->sqlCompanyMediaFolder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_file_type = ' . $this->sqlMediaFileType() . ','; } elseif( true == array_key_exists( 'MediaFileType', $this->getChangedColumns() ) ) { $strSql .= ' media_file_type = ' . $this->sqlMediaFileType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_type = ' . $this->sqlMediaType() . ','; } elseif( true == array_key_exists( 'MediaType', $this->getChangedColumns() ) ) { $strSql .= ' media_type = ' . $this->sqlMediaType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; } elseif( true == array_key_exists( 'IsHidden', $this->getChangedColumns() ) ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption() . ','; } elseif( true == array_key_exists( 'MediaCaption', $this->getChangedColumns() ) ) { $strSql .= ' media_caption = ' . $this->sqlMediaCaption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription() . ','; } elseif( true == array_key_exists( 'MediaDescription', $this->getChangedColumns() ) ) { $strSql .= ' media_description = ' . $this->sqlMediaDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_data = ' . $this->sqlMediaData() . ','; } elseif( true == array_key_exists( 'MediaData', $this->getChangedColumns() ) ) { $strSql .= ' media_data = ' . $this->sqlMediaData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; } elseif( true == array_key_exists( 'MediaWidth', $this->getChangedColumns() ) ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; } elseif( true == array_key_exists( 'MediaHeight', $this->getChangedColumns() ) ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt() . ','; } elseif( true == array_key_exists( 'MediaAlt', $this->getChangedColumns() ) ) { $strSql .= ' media_alt = ' . $this->sqlMediaAlt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fullsize_uri = ' . $this->sqlFullsizeUri() . ','; } elseif( true == array_key_exists( 'FullsizeUri', $this->getChangedColumns() ) ) { $strSql .= ' fullsize_uri = ' . $this->sqlFullsizeUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thumbnail_uri = ' . $this->sqlThumbnailUri() . ','; } elseif( true == array_key_exists( 'ThumbnailUri', $this->getChangedColumns() ) ) { $strSql .= ' thumbnail_uri = ' . $this->sqlThumbnailUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_uri = ' . $this->sqlExternalUri() . ','; } elseif( true == array_key_exists( 'ExternalUri', $this->getChangedColumns() ) ) { $strSql .= ' external_uri = ' . $this->sqlExternalUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'company_media_folder' => $this->getCompanyMediaFolder(),
			'media_file_type' => $this->getMediaFileType(),
			'media_type' => $this->getMediaType(),
			'is_hidden' => $this->getIsHidden(),
			'media_caption' => $this->getMediaCaption(),
			'media_description' => $this->getMediaDescription(),
			'media_data' => $this->getMediaData(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'media_alt' => $this->getMediaAlt(),
			'fullsize_uri' => $this->getFullsizeUri(),
			'thumbnail_uri' => $this->getThumbnailUri(),
			'external_uri' => $this->getExternalUri(),
			'file_name' => $this->getFileName(),
			'file_size' => $this->getFileSize(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>