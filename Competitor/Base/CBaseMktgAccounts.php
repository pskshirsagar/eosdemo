<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgAccounts
 * Do not add any new functions to this class.
 */

class CBaseMktgAccounts extends CEosPluralBase {

	/**
	 * @return CMktgAccount[]
	 */
	public static function fetchMktgAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgAccount::class, $objDatabase );
	}

	/**
	 * @return CMktgAccount
	 */
	public static function fetchMktgAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgAccount::class, $objDatabase );
	}

	public static function fetchMktgAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_accounts', $objDatabase );
	}

	public static function fetchMktgAccountById( $intId, $objDatabase ) {
		return self::fetchMktgAccount( sprintf( 'SELECT * FROM mktg_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgAccountsByEntityId( $intEntityId, $objDatabase ) {
		return self::fetchMktgAccounts( sprintf( 'SELECT * FROM mktg_accounts WHERE entity_id = %d', ( int ) $intEntityId ), $objDatabase );
	}

	public static function fetchMktgAccountsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgAccounts( sprintf( 'SELECT * FROM mktg_accounts WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgAccountsByDefaultBillingAccountId( $intDefaultBillingAccountId, $objDatabase ) {
		return self::fetchMktgAccounts( sprintf( 'SELECT * FROM mktg_accounts WHERE default_billing_account_id = %d', ( int ) $intDefaultBillingAccountId ), $objDatabase );
	}

	public static function fetchMktgAccountsByRelatedCompanyAccountId( $intRelatedCompanyAccountId, $objDatabase ) {
		return self::fetchMktgAccounts( sprintf( 'SELECT * FROM mktg_accounts WHERE related_company_account_id = %d', ( int ) $intRelatedCompanyAccountId ), $objDatabase );
	}

}
?>