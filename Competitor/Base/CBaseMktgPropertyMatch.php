<?php

class CBaseMktgPropertyMatch extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_property_matches';

	protected $m_strExternalPrimaryId;
	protected $m_intMatchId;
	protected $m_strMatchType;
	protected $m_fltConfidenceRating;
	protected $m_fltSimilarityRating;
	protected $m_strDateMatched;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['external_primary_id'] ) && $boolDirectSet ) $this->set( 'm_strExternalPrimaryId', trim( stripcslashes( $arrValues['external_primary_id'] ) ) ); elseif( isset( $arrValues['external_primary_id'] ) ) $this->setExternalPrimaryId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['external_primary_id'] ) : $arrValues['external_primary_id'] );
		if( isset( $arrValues['match_id'] ) && $boolDirectSet ) $this->set( 'm_intMatchId', trim( $arrValues['match_id'] ) ); elseif( isset( $arrValues['match_id'] ) ) $this->setMatchId( $arrValues['match_id'] );
		if( isset( $arrValues['match_type'] ) && $boolDirectSet ) $this->set( 'm_strMatchType', trim( stripcslashes( $arrValues['match_type'] ) ) ); elseif( isset( $arrValues['match_type'] ) ) $this->setMatchType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['match_type'] ) : $arrValues['match_type'] );
		if( isset( $arrValues['confidence_rating'] ) && $boolDirectSet ) $this->set( 'm_fltConfidenceRating', trim( $arrValues['confidence_rating'] ) ); elseif( isset( $arrValues['confidence_rating'] ) ) $this->setConfidenceRating( $arrValues['confidence_rating'] );
		if( isset( $arrValues['similarity_rating'] ) && $boolDirectSet ) $this->set( 'm_fltSimilarityRating', trim( $arrValues['similarity_rating'] ) ); elseif( isset( $arrValues['similarity_rating'] ) ) $this->setSimilarityRating( $arrValues['similarity_rating'] );
		if( isset( $arrValues['date_matched'] ) && $boolDirectSet ) $this->set( 'm_strDateMatched', trim( $arrValues['date_matched'] ) ); elseif( isset( $arrValues['date_matched'] ) ) $this->setDateMatched( $arrValues['date_matched'] );
		$this->m_boolInitialized = true;
	}

	public function setExternalPrimaryId( $strExternalPrimaryId ) {
		$this->set( 'm_strExternalPrimaryId', CStrings::strTrimDef( $strExternalPrimaryId, 100, NULL, true ) );
	}

	public function getExternalPrimaryId() {
		return $this->m_strExternalPrimaryId;
	}

	public function sqlExternalPrimaryId() {
		return ( true == isset( $this->m_strExternalPrimaryId ) ) ? '\'' . addslashes( $this->m_strExternalPrimaryId ) . '\'' : 'NULL';
	}

	public function setMatchId( $intMatchId ) {
		$this->set( 'm_intMatchId', CStrings::strToIntDef( $intMatchId, NULL, false ) );
	}

	public function getMatchId() {
		return $this->m_intMatchId;
	}

	public function sqlMatchId() {
		return ( true == isset( $this->m_intMatchId ) ) ? ( string ) $this->m_intMatchId : 'NULL';
	}

	public function setMatchType( $strMatchType ) {
		$this->set( 'm_strMatchType', CStrings::strTrimDef( $strMatchType, 50, NULL, true ) );
	}

	public function getMatchType() {
		return $this->m_strMatchType;
	}

	public function sqlMatchType() {
		return ( true == isset( $this->m_strMatchType ) ) ? '\'' . addslashes( $this->m_strMatchType ) . '\'' : 'NULL';
	}

	public function setConfidenceRating( $fltConfidenceRating ) {
		$this->set( 'm_fltConfidenceRating', CStrings::strToFloatDef( $fltConfidenceRating, NULL, false, 4 ) );
	}

	public function getConfidenceRating() {
		return $this->m_fltConfidenceRating;
	}

	public function sqlConfidenceRating() {
		return ( true == isset( $this->m_fltConfidenceRating ) ) ? ( string ) $this->m_fltConfidenceRating : 'NULL';
	}

	public function setSimilarityRating( $fltSimilarityRating ) {
		$this->set( 'm_fltSimilarityRating', CStrings::strToFloatDef( $fltSimilarityRating, NULL, false, 4 ) );
	}

	public function getSimilarityRating() {
		return $this->m_fltSimilarityRating;
	}

	public function sqlSimilarityRating() {
		return ( true == isset( $this->m_fltSimilarityRating ) ) ? ( string ) $this->m_fltSimilarityRating : 'NULL';
	}

	public function setDateMatched( $strDateMatched ) {
		$this->set( 'm_strDateMatched', CStrings::strTrimDef( $strDateMatched, -1, NULL, true ) );
	}

	public function getDateMatched() {
		return $this->m_strDateMatched;
	}

	public function sqlDateMatched() {
		return ( true == isset( $this->m_strDateMatched ) ) ? '\'' . $this->m_strDateMatched . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'external_primary_id' => $this->getExternalPrimaryId(),
			'match_id' => $this->getMatchId(),
			'match_type' => $this->getMatchType(),
			'confidence_rating' => $this->getConfidenceRating(),
			'similarity_rating' => $this->getSimilarityRating(),
			'date_matched' => $this->getDateMatched()
		);
	}

}
?>