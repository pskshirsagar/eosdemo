<?php

class CBaseMktgCompetitiveAdjustment extends CEosSingularBase {

	const TABLE_NAME = 'public.mktg_competitive_adjustments';

	protected $m_intId;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_intCompetitorPropertyId;
	protected $m_intCompetitiveAdjustmentTypeId;
	protected $m_fltAdjustmentAmount;
	protected $m_intIsFixedAmount;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAdjustmentAmount = '0';
		$this->m_intIsFixedAmount = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['competitor_property_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorPropertyId', trim( $arrValues['competitor_property_id'] ) ); elseif( isset( $arrValues['competitor_property_id'] ) ) $this->setCompetitorPropertyId( $arrValues['competitor_property_id'] );
		if( isset( $arrValues['competitive_adjustment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitiveAdjustmentTypeId', trim( $arrValues['competitive_adjustment_type_id'] ) ); elseif( isset( $arrValues['competitive_adjustment_type_id'] ) ) $this->setCompetitiveAdjustmentTypeId( $arrValues['competitive_adjustment_type_id'] );
		if( isset( $arrValues['adjustment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustmentAmount', trim( $arrValues['adjustment_amount'] ) ); elseif( isset( $arrValues['adjustment_amount'] ) ) $this->setAdjustmentAmount( $arrValues['adjustment_amount'] );
		if( isset( $arrValues['is_fixed_amount'] ) && $boolDirectSet ) $this->set( 'm_intIsFixedAmount', trim( $arrValues['is_fixed_amount'] ) ); elseif( isset( $arrValues['is_fixed_amount'] ) ) $this->setIsFixedAmount( $arrValues['is_fixed_amount'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompetitorPropertyId( $intCompetitorPropertyId ) {
		$this->set( 'm_intCompetitorPropertyId', CStrings::strToIntDef( $intCompetitorPropertyId, NULL, false ) );
	}

	public function getCompetitorPropertyId() {
		return $this->m_intCompetitorPropertyId;
	}

	public function sqlCompetitorPropertyId() {
		return ( true == isset( $this->m_intCompetitorPropertyId ) ) ? ( string ) $this->m_intCompetitorPropertyId : 'NULL';
	}

	public function setCompetitiveAdjustmentTypeId( $intCompetitiveAdjustmentTypeId ) {
		$this->set( 'm_intCompetitiveAdjustmentTypeId', CStrings::strToIntDef( $intCompetitiveAdjustmentTypeId, NULL, false ) );
	}

	public function getCompetitiveAdjustmentTypeId() {
		return $this->m_intCompetitiveAdjustmentTypeId;
	}

	public function sqlCompetitiveAdjustmentTypeId() {
		return ( true == isset( $this->m_intCompetitiveAdjustmentTypeId ) ) ? ( string ) $this->m_intCompetitiveAdjustmentTypeId : 'NULL';
	}

	public function setAdjustmentAmount( $fltAdjustmentAmount ) {
		$this->set( 'm_fltAdjustmentAmount', CStrings::strToFloatDef( $fltAdjustmentAmount, NULL, false, 6 ) );
	}

	public function getAdjustmentAmount() {
		return $this->m_fltAdjustmentAmount;
	}

	public function sqlAdjustmentAmount() {
		return ( true == isset( $this->m_fltAdjustmentAmount ) ) ? ( string ) $this->m_fltAdjustmentAmount : '0';
	}

	public function setIsFixedAmount( $intIsFixedAmount ) {
		$this->set( 'm_intIsFixedAmount', CStrings::strToIntDef( $intIsFixedAmount, NULL, false ) );
	}

	public function getIsFixedAmount() {
		return $this->m_intIsFixedAmount;
	}

	public function sqlIsFixedAmount() {
		return ( true == isset( $this->m_intIsFixedAmount ) ) ? ( string ) $this->m_intIsFixedAmount : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_id, property_id, competitor_property_id, competitive_adjustment_type_id, adjustment_amount, is_fixed_amount, updated_on, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompetitorPropertyId() . ', ' .
 						$this->sqlCompetitiveAdjustmentTypeId() . ', ' .
 						$this->sqlAdjustmentAmount() . ', ' .
 						$this->sqlIsFixedAmount() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; } elseif( true == array_key_exists( 'CompetitorPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_property_id = ' . $this->sqlCompetitorPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitive_adjustment_type_id = ' . $this->sqlCompetitiveAdjustmentTypeId() . ','; } elseif( true == array_key_exists( 'CompetitiveAdjustmentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' competitive_adjustment_type_id = ' . $this->sqlCompetitiveAdjustmentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_amount = ' . $this->sqlAdjustmentAmount() . ','; } elseif( true == array_key_exists( 'AdjustmentAmount', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_amount = ' . $this->sqlAdjustmentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_fixed_amount = ' . $this->sqlIsFixedAmount() . ','; } elseif( true == array_key_exists( 'IsFixedAmount', $this->getChangedColumns() ) ) { $strSql .= ' is_fixed_amount = ' . $this->sqlIsFixedAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'competitor_property_id' => $this->getCompetitorPropertyId(),
			'competitive_adjustment_type_id' => $this->getCompetitiveAdjustmentTypeId(),
			'adjustment_amount' => $this->getAdjustmentAmount(),
			'is_fixed_amount' => $this->getIsFixedAmount(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>