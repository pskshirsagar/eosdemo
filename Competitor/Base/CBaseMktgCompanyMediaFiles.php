<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanyMediaFiles
 * Do not add any new functions to this class.
 */

class CBaseMktgCompanyMediaFiles extends CEosPluralBase {

	/**
	 * @return CMktgCompanyMediaFile[]
	 */
	public static function fetchMktgCompanyMediaFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgCompanyMediaFile::class, $objDatabase );
	}

	/**
	 * @return CMktgCompanyMediaFile
	 */
	public static function fetchMktgCompanyMediaFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgCompanyMediaFile::class, $objDatabase );
	}

	public static function fetchMktgCompanyMediaFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_company_media_files', $objDatabase );
	}

	public static function fetchMktgCompanyMediaFileById( $intId, $objDatabase ) {
		return self::fetchMktgCompanyMediaFile( sprintf( 'SELECT * FROM mktg_company_media_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgCompanyMediaFilesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgCompanyMediaFiles( sprintf( 'SELECT * FROM mktg_company_media_files WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

}
?>