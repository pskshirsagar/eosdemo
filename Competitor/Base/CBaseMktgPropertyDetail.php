<?php

class CBaseMktgPropertyDetail extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.mktg_property_details';

	protected $m_intId;
	protected $m_strRemotePrimaryKey;
	protected $m_intCompanyId;
	protected $m_intPropertyId;
	protected $m_fltMinRent;
	protected $m_fltMaxRent;
	protected $m_fltMinSquareFeet;
	protected $m_fltMaxSquareFeet;
	protected $m_intMinBedrooms;
	protected $m_intMaxBedrooms;
	protected $m_fltMinBathrooms;
	protected $m_fltMaxBathrooms;
	protected $m_intNumberOfBuildings;
	protected $m_intNumberOfAcres;
	protected $m_intNumberOfUnits;
	protected $m_strYearBuilt;
	protected $m_strYearRemodeled;
	protected $m_strShortDescription;
	protected $m_intAllowsCats;
	protected $m_intAllowsDogs;
	protected $m_intHasAvailability;
	protected $m_intUpdateVacancyAmenities;
	protected $m_strVacancyRedirectUrl;
	protected $m_fltRewardAmount;
	protected $m_strRewardDescription;
	protected $m_intIsAutoPostTestimonials;
	protected $m_strOwnerName;
	protected $m_strWebsiteAddress;
	protected $m_strData;
	protected $m_strCreatedOn;
	protected $m_strUpdatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intAllowsCats = '0';
		$this->m_intAllowsDogs = '0';
		$this->m_intHasAvailability = '0';
		$this->m_intUpdateVacancyAmenities = '0';
		$this->m_intIsAutoPostTestimonials = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['company_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyId', trim( $arrValues['company_id'] ) ); elseif( isset( $arrValues['company_id'] ) ) $this->setCompanyId( $arrValues['company_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['min_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMinRent', trim( $arrValues['min_rent'] ) ); elseif( isset( $arrValues['min_rent'] ) ) $this->setMinRent( $arrValues['min_rent'] );
		if( isset( $arrValues['max_rent'] ) && $boolDirectSet ) $this->set( 'm_fltMaxRent', trim( $arrValues['max_rent'] ) ); elseif( isset( $arrValues['max_rent'] ) ) $this->setMaxRent( $arrValues['max_rent'] );
		if( isset( $arrValues['min_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMinSquareFeet', trim( $arrValues['min_square_feet'] ) ); elseif( isset( $arrValues['min_square_feet'] ) ) $this->setMinSquareFeet( $arrValues['min_square_feet'] );
		if( isset( $arrValues['max_square_feet'] ) && $boolDirectSet ) $this->set( 'm_fltMaxSquareFeet', trim( $arrValues['max_square_feet'] ) ); elseif( isset( $arrValues['max_square_feet'] ) ) $this->setMaxSquareFeet( $arrValues['max_square_feet'] );
		if( isset( $arrValues['min_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intMinBedrooms', trim( $arrValues['min_bedrooms'] ) ); elseif( isset( $arrValues['min_bedrooms'] ) ) $this->setMinBedrooms( $arrValues['min_bedrooms'] );
		if( isset( $arrValues['max_bedrooms'] ) && $boolDirectSet ) $this->set( 'm_intMaxBedrooms', trim( $arrValues['max_bedrooms'] ) ); elseif( isset( $arrValues['max_bedrooms'] ) ) $this->setMaxBedrooms( $arrValues['max_bedrooms'] );
		if( isset( $arrValues['min_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltMinBathrooms', trim( $arrValues['min_bathrooms'] ) ); elseif( isset( $arrValues['min_bathrooms'] ) ) $this->setMinBathrooms( $arrValues['min_bathrooms'] );
		if( isset( $arrValues['max_bathrooms'] ) && $boolDirectSet ) $this->set( 'm_fltMaxBathrooms', trim( $arrValues['max_bathrooms'] ) ); elseif( isset( $arrValues['max_bathrooms'] ) ) $this->setMaxBathrooms( $arrValues['max_bathrooms'] );
		if( isset( $arrValues['number_of_buildings'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfBuildings', trim( $arrValues['number_of_buildings'] ) ); elseif( isset( $arrValues['number_of_buildings'] ) ) $this->setNumberOfBuildings( $arrValues['number_of_buildings'] );
		if( isset( $arrValues['number_of_acres'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfAcres', trim( $arrValues['number_of_acres'] ) ); elseif( isset( $arrValues['number_of_acres'] ) ) $this->setNumberOfAcres( $arrValues['number_of_acres'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['year_built'] ) && $boolDirectSet ) $this->set( 'm_strYearBuilt', trim( $arrValues['year_built'] ) ); elseif( isset( $arrValues['year_built'] ) ) $this->setYearBuilt( $arrValues['year_built'] );
		if( isset( $arrValues['year_remodeled'] ) && $boolDirectSet ) $this->set( 'm_strYearRemodeled', trim( $arrValues['year_remodeled'] ) ); elseif( isset( $arrValues['year_remodeled'] ) ) $this->setYearRemodeled( $arrValues['year_remodeled'] );
		if( isset( $arrValues['short_description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strShortDescription', trim( stripcslashes( $arrValues['short_description'] ) ) ); elseif( isset( $arrValues['short_description'] ) ) $this->setShortDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['short_description'] ) : $arrValues['short_description'] );
		if( isset( $arrValues['allows_cats'] ) && $boolDirectSet ) $this->set( 'm_intAllowsCats', trim( $arrValues['allows_cats'] ) ); elseif( isset( $arrValues['allows_cats'] ) ) $this->setAllowsCats( $arrValues['allows_cats'] );
		if( isset( $arrValues['allows_dogs'] ) && $boolDirectSet ) $this->set( 'm_intAllowsDogs', trim( $arrValues['allows_dogs'] ) ); elseif( isset( $arrValues['allows_dogs'] ) ) $this->setAllowsDogs( $arrValues['allows_dogs'] );
		if( isset( $arrValues['has_availability'] ) && $boolDirectSet ) $this->set( 'm_intHasAvailability', trim( $arrValues['has_availability'] ) ); elseif( isset( $arrValues['has_availability'] ) ) $this->setHasAvailability( $arrValues['has_availability'] );
		if( isset( $arrValues['update_vacancy_amenities'] ) && $boolDirectSet ) $this->set( 'm_intUpdateVacancyAmenities', trim( $arrValues['update_vacancy_amenities'] ) ); elseif( isset( $arrValues['update_vacancy_amenities'] ) ) $this->setUpdateVacancyAmenities( $arrValues['update_vacancy_amenities'] );
		if( isset( $arrValues['vacancy_redirect_url'] ) && $boolDirectSet ) $this->set( 'm_strVacancyRedirectUrl', trim( stripcslashes( $arrValues['vacancy_redirect_url'] ) ) ); elseif( isset( $arrValues['vacancy_redirect_url'] ) ) $this->setVacancyRedirectUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vacancy_redirect_url'] ) : $arrValues['vacancy_redirect_url'] );
		if( isset( $arrValues['reward_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRewardAmount', trim( $arrValues['reward_amount'] ) ); elseif( isset( $arrValues['reward_amount'] ) ) $this->setRewardAmount( $arrValues['reward_amount'] );
		if( isset( $arrValues['reward_description'] ) && $boolDirectSet ) $this->set( 'm_strRewardDescription', trim( stripcslashes( $arrValues['reward_description'] ) ) ); elseif( isset( $arrValues['reward_description'] ) ) $this->setRewardDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reward_description'] ) : $arrValues['reward_description'] );
		if( isset( $arrValues['is_auto_post_testimonials'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoPostTestimonials', trim( $arrValues['is_auto_post_testimonials'] ) ); elseif( isset( $arrValues['is_auto_post_testimonials'] ) ) $this->setIsAutoPostTestimonials( $arrValues['is_auto_post_testimonials'] );
		if( isset( $arrValues['owner_name'] ) && $boolDirectSet ) $this->set( 'm_strOwnerName', trim( stripcslashes( $arrValues['owner_name'] ) ) ); elseif( isset( $arrValues['owner_name'] ) ) $this->setOwnerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['owner_name'] ) : $arrValues['owner_name'] );
		if( isset( $arrValues['website_address'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteAddress', trim( stripcslashes( $arrValues['website_address'] ) ) ); elseif( isset( $arrValues['website_address'] ) ) $this->setWebsiteAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['website_address'] ) : $arrValues['website_address'] );
		if( isset( $arrValues['data'] ) && $boolDirectSet ) $this->set( 'm_strData', trim( stripcslashes( $arrValues['data'] ) ) ); elseif( isset( $arrValues['data'] ) ) $this->setData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data'] ) : $arrValues['data'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 100, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setCompanyId( $intCompanyId ) {
		$this->set( 'm_intCompanyId', CStrings::strToIntDef( $intCompanyId, NULL, false ) );
	}

	public function getCompanyId() {
		return $this->m_intCompanyId;
	}

	public function sqlCompanyId() {
		return ( true == isset( $this->m_intCompanyId ) ) ? ( string ) $this->m_intCompanyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMinRent( $fltMinRent ) {
		$this->set( 'm_fltMinRent', CStrings::strToFloatDef( $fltMinRent, NULL, false, 4 ) );
	}

	public function getMinRent() {
		return $this->m_fltMinRent;
	}

	public function sqlMinRent() {
		return ( true == isset( $this->m_fltMinRent ) ) ? ( string ) $this->m_fltMinRent : 'NULL';
	}

	public function setMaxRent( $fltMaxRent ) {
		$this->set( 'm_fltMaxRent', CStrings::strToFloatDef( $fltMaxRent, NULL, false, 4 ) );
	}

	public function getMaxRent() {
		return $this->m_fltMaxRent;
	}

	public function sqlMaxRent() {
		return ( true == isset( $this->m_fltMaxRent ) ) ? ( string ) $this->m_fltMaxRent : 'NULL';
	}

	public function setMinSquareFeet( $fltMinSquareFeet ) {
		$this->set( 'm_fltMinSquareFeet', CStrings::strToFloatDef( $fltMinSquareFeet, NULL, false, 4 ) );
	}

	public function getMinSquareFeet() {
		return $this->m_fltMinSquareFeet;
	}

	public function sqlMinSquareFeet() {
		return ( true == isset( $this->m_fltMinSquareFeet ) ) ? ( string ) $this->m_fltMinSquareFeet : 'NULL';
	}

	public function setMaxSquareFeet( $fltMaxSquareFeet ) {
		$this->set( 'm_fltMaxSquareFeet', CStrings::strToFloatDef( $fltMaxSquareFeet, NULL, false, 4 ) );
	}

	public function getMaxSquareFeet() {
		return $this->m_fltMaxSquareFeet;
	}

	public function sqlMaxSquareFeet() {
		return ( true == isset( $this->m_fltMaxSquareFeet ) ) ? ( string ) $this->m_fltMaxSquareFeet : 'NULL';
	}

	public function setMinBedrooms( $intMinBedrooms ) {
		$this->set( 'm_intMinBedrooms', CStrings::strToIntDef( $intMinBedrooms, NULL, false ) );
	}

	public function getMinBedrooms() {
		return $this->m_intMinBedrooms;
	}

	public function sqlMinBedrooms() {
		return ( true == isset( $this->m_intMinBedrooms ) ) ? ( string ) $this->m_intMinBedrooms : 'NULL';
	}

	public function setMaxBedrooms( $intMaxBedrooms ) {
		$this->set( 'm_intMaxBedrooms', CStrings::strToIntDef( $intMaxBedrooms, NULL, false ) );
	}

	public function getMaxBedrooms() {
		return $this->m_intMaxBedrooms;
	}

	public function sqlMaxBedrooms() {
		return ( true == isset( $this->m_intMaxBedrooms ) ) ? ( string ) $this->m_intMaxBedrooms : 'NULL';
	}

	public function setMinBathrooms( $fltMinBathrooms ) {
		$this->set( 'm_fltMinBathrooms', CStrings::strToFloatDef( $fltMinBathrooms, NULL, false, 4 ) );
	}

	public function getMinBathrooms() {
		return $this->m_fltMinBathrooms;
	}

	public function sqlMinBathrooms() {
		return ( true == isset( $this->m_fltMinBathrooms ) ) ? ( string ) $this->m_fltMinBathrooms : 'NULL';
	}

	public function setMaxBathrooms( $fltMaxBathrooms ) {
		$this->set( 'm_fltMaxBathrooms', CStrings::strToFloatDef( $fltMaxBathrooms, NULL, false, 4 ) );
	}

	public function getMaxBathrooms() {
		return $this->m_fltMaxBathrooms;
	}

	public function sqlMaxBathrooms() {
		return ( true == isset( $this->m_fltMaxBathrooms ) ) ? ( string ) $this->m_fltMaxBathrooms : 'NULL';
	}

	public function setNumberOfBuildings( $intNumberOfBuildings ) {
		$this->set( 'm_intNumberOfBuildings', CStrings::strToIntDef( $intNumberOfBuildings, NULL, false ) );
	}

	public function getNumberOfBuildings() {
		return $this->m_intNumberOfBuildings;
	}

	public function sqlNumberOfBuildings() {
		return ( true == isset( $this->m_intNumberOfBuildings ) ) ? ( string ) $this->m_intNumberOfBuildings : 'NULL';
	}

	public function setNumberOfAcres( $intNumberOfAcres ) {
		$this->set( 'm_intNumberOfAcres', CStrings::strToIntDef( $intNumberOfAcres, NULL, false ) );
	}

	public function getNumberOfAcres() {
		return $this->m_intNumberOfAcres;
	}

	public function sqlNumberOfAcres() {
		return ( true == isset( $this->m_intNumberOfAcres ) ) ? ( string ) $this->m_intNumberOfAcres : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setYearBuilt( $strYearBuilt ) {
		$this->set( 'm_strYearBuilt', CStrings::strTrimDef( $strYearBuilt, -1, NULL, true ) );
	}

	public function getYearBuilt() {
		return $this->m_strYearBuilt;
	}

	public function sqlYearBuilt() {
		return ( true == isset( $this->m_strYearBuilt ) ) ? '\'' . $this->m_strYearBuilt . '\'' : 'NULL';
	}

	public function setYearRemodeled( $strYearRemodeled ) {
		$this->set( 'm_strYearRemodeled', CStrings::strTrimDef( $strYearRemodeled, -1, NULL, true ) );
	}

	public function getYearRemodeled() {
		return $this->m_strYearRemodeled;
	}

	public function sqlYearRemodeled() {
		return ( true == isset( $this->m_strYearRemodeled ) ) ? '\'' . $this->m_strYearRemodeled . '\'' : 'NULL';
	}

	public function setShortDescription( $strShortDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strShortDescription', CStrings::strTrimDef( $strShortDescription, 240, NULL, true ), $strLocaleCode );
	}

	public function getShortDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strShortDescription', $strLocaleCode );
	}

	public function sqlShortDescription() {
		return ( true == isset( $this->m_strShortDescription ) ) ? '\'' . addslashes( $this->m_strShortDescription ) . '\'' : 'NULL';
	}

	public function setAllowsCats( $intAllowsCats ) {
		$this->set( 'm_intAllowsCats', CStrings::strToIntDef( $intAllowsCats, NULL, false ) );
	}

	public function getAllowsCats() {
		return $this->m_intAllowsCats;
	}

	public function sqlAllowsCats() {
		return ( true == isset( $this->m_intAllowsCats ) ) ? ( string ) $this->m_intAllowsCats : '0';
	}

	public function setAllowsDogs( $intAllowsDogs ) {
		$this->set( 'm_intAllowsDogs', CStrings::strToIntDef( $intAllowsDogs, NULL, false ) );
	}

	public function getAllowsDogs() {
		return $this->m_intAllowsDogs;
	}

	public function sqlAllowsDogs() {
		return ( true == isset( $this->m_intAllowsDogs ) ) ? ( string ) $this->m_intAllowsDogs : '0';
	}

	public function setHasAvailability( $intHasAvailability ) {
		$this->set( 'm_intHasAvailability', CStrings::strToIntDef( $intHasAvailability, NULL, false ) );
	}

	public function getHasAvailability() {
		return $this->m_intHasAvailability;
	}

	public function sqlHasAvailability() {
		return ( true == isset( $this->m_intHasAvailability ) ) ? ( string ) $this->m_intHasAvailability : '0';
	}

	public function setUpdateVacancyAmenities( $intUpdateVacancyAmenities ) {
		$this->set( 'm_intUpdateVacancyAmenities', CStrings::strToIntDef( $intUpdateVacancyAmenities, NULL, false ) );
	}

	public function getUpdateVacancyAmenities() {
		return $this->m_intUpdateVacancyAmenities;
	}

	public function sqlUpdateVacancyAmenities() {
		return ( true == isset( $this->m_intUpdateVacancyAmenities ) ) ? ( string ) $this->m_intUpdateVacancyAmenities : '0';
	}

	public function setVacancyRedirectUrl( $strVacancyRedirectUrl ) {
		$this->set( 'm_strVacancyRedirectUrl', CStrings::strTrimDef( $strVacancyRedirectUrl, 4096, NULL, true ) );
	}

	public function getVacancyRedirectUrl() {
		return $this->m_strVacancyRedirectUrl;
	}

	public function sqlVacancyRedirectUrl() {
		return ( true == isset( $this->m_strVacancyRedirectUrl ) ) ? '\'' . addslashes( $this->m_strVacancyRedirectUrl ) . '\'' : 'NULL';
	}

	public function setRewardAmount( $fltRewardAmount ) {
		$this->set( 'm_fltRewardAmount', CStrings::strToFloatDef( $fltRewardAmount, NULL, false, 4 ) );
	}

	public function getRewardAmount() {
		return $this->m_fltRewardAmount;
	}

	public function sqlRewardAmount() {
		return ( true == isset( $this->m_fltRewardAmount ) ) ? ( string ) $this->m_fltRewardAmount : 'NULL';
	}

	public function setRewardDescription( $strRewardDescription ) {
		$this->set( 'm_strRewardDescription', CStrings::strTrimDef( $strRewardDescription, 2000, NULL, true ) );
	}

	public function getRewardDescription() {
		return $this->m_strRewardDescription;
	}

	public function sqlRewardDescription() {
		return ( true == isset( $this->m_strRewardDescription ) ) ? '\'' . addslashes( $this->m_strRewardDescription ) . '\'' : 'NULL';
	}

	public function setIsAutoPostTestimonials( $intIsAutoPostTestimonials ) {
		$this->set( 'm_intIsAutoPostTestimonials', CStrings::strToIntDef( $intIsAutoPostTestimonials, NULL, false ) );
	}

	public function getIsAutoPostTestimonials() {
		return $this->m_intIsAutoPostTestimonials;
	}

	public function sqlIsAutoPostTestimonials() {
		return ( true == isset( $this->m_intIsAutoPostTestimonials ) ) ? ( string ) $this->m_intIsAutoPostTestimonials : '1';
	}

	public function setOwnerName( $strOwnerName ) {
		$this->set( 'm_strOwnerName', CStrings::strTrimDef( $strOwnerName, 100, NULL, true ) );
	}

	public function getOwnerName() {
		return $this->m_strOwnerName;
	}

	public function sqlOwnerName() {
		return ( true == isset( $this->m_strOwnerName ) ) ? '\'' . addslashes( $this->m_strOwnerName ) . '\'' : 'NULL';
	}

	public function setWebsiteAddress( $strWebsiteAddress ) {
		$this->set( 'm_strWebsiteAddress', CStrings::strTrimDef( $strWebsiteAddress, 4096, NULL, true ) );
	}

	public function getWebsiteAddress() {
		return $this->m_strWebsiteAddress;
	}

	public function sqlWebsiteAddress() {
		return ( true == isset( $this->m_strWebsiteAddress ) ) ? '\'' . addslashes( $this->m_strWebsiteAddress ) . '\'' : 'NULL';
	}

	public function setData( $strData ) {
		$this->set( 'm_strData', CStrings::strTrimDef( $strData, -1, NULL, true ) );
	}

	public function getData() {
		return $this->m_strData;
	}

	public function sqlData() {
		return ( true == isset( $this->m_strData ) ) ? '\'' . addslashes( $this->m_strData ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, remote_primary_key, company_id, property_id, min_rent, max_rent, min_square_feet, max_square_feet, min_bedrooms, max_bedrooms, min_bathrooms, max_bathrooms, number_of_buildings, number_of_acres, number_of_units, year_built, year_remodeled, short_description, allows_cats, allows_dogs, has_availability, update_vacancy_amenities, vacancy_redirect_url, reward_amount, reward_description, is_auto_post_testimonials, owner_name, website_address, data, created_on, updated_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlCompanyId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMinRent() . ', ' .
						$this->sqlMaxRent() . ', ' .
						$this->sqlMinSquareFeet() . ', ' .
						$this->sqlMaxSquareFeet() . ', ' .
						$this->sqlMinBedrooms() . ', ' .
						$this->sqlMaxBedrooms() . ', ' .
						$this->sqlMinBathrooms() . ', ' .
						$this->sqlMaxBathrooms() . ', ' .
						$this->sqlNumberOfBuildings() . ', ' .
						$this->sqlNumberOfAcres() . ', ' .
						$this->sqlNumberOfUnits() . ', ' .
						$this->sqlYearBuilt() . ', ' .
						$this->sqlYearRemodeled() . ', ' .
						$this->sqlShortDescription() . ', ' .
						$this->sqlAllowsCats() . ', ' .
						$this->sqlAllowsDogs() . ', ' .
						$this->sqlHasAvailability() . ', ' .
						$this->sqlUpdateVacancyAmenities() . ', ' .
						$this->sqlVacancyRedirectUrl() . ', ' .
						$this->sqlRewardAmount() . ', ' .
						$this->sqlRewardDescription() . ', ' .
						$this->sqlIsAutoPostTestimonials() . ', ' .
						$this->sqlOwnerName() . ', ' .
						$this->sqlWebsiteAddress() . ', ' .
						$this->sqlData() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_id = ' . $this->sqlCompanyId(). ',' ; } elseif( true == array_key_exists( 'CompanyId', $this->getChangedColumns() ) ) { $strSql .= ' company_id = ' . $this->sqlCompanyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_rent = ' . $this->sqlMinRent(). ',' ; } elseif( true == array_key_exists( 'MinRent', $this->getChangedColumns() ) ) { $strSql .= ' min_rent = ' . $this->sqlMinRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent(). ',' ; } elseif( true == array_key_exists( 'MaxRent', $this->getChangedColumns() ) ) { $strSql .= ' max_rent = ' . $this->sqlMaxRent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MinSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' min_square_feet = ' . $this->sqlMinSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet(). ',' ; } elseif( true == array_key_exists( 'MaxSquareFeet', $this->getChangedColumns() ) ) { $strSql .= ' max_square_feet = ' . $this->sqlMaxSquareFeet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_bedrooms = ' . $this->sqlMinBedrooms(). ',' ; } elseif( true == array_key_exists( 'MinBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' min_bedrooms = ' . $this->sqlMinBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_bedrooms = ' . $this->sqlMaxBedrooms(). ',' ; } elseif( true == array_key_exists( 'MaxBedrooms', $this->getChangedColumns() ) ) { $strSql .= ' max_bedrooms = ' . $this->sqlMaxBedrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_bathrooms = ' . $this->sqlMinBathrooms(). ',' ; } elseif( true == array_key_exists( 'MinBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' min_bathrooms = ' . $this->sqlMinBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_bathrooms = ' . $this->sqlMaxBathrooms(). ',' ; } elseif( true == array_key_exists( 'MaxBathrooms', $this->getChangedColumns() ) ) { $strSql .= ' max_bathrooms = ' . $this->sqlMaxBathrooms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_buildings = ' . $this->sqlNumberOfBuildings(). ',' ; } elseif( true == array_key_exists( 'NumberOfBuildings', $this->getChangedColumns() ) ) { $strSql .= ' number_of_buildings = ' . $this->sqlNumberOfBuildings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_acres = ' . $this->sqlNumberOfAcres(). ',' ; } elseif( true == array_key_exists( 'NumberOfAcres', $this->getChangedColumns() ) ) { $strSql .= ' number_of_acres = ' . $this->sqlNumberOfAcres() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt(). ',' ; } elseif( true == array_key_exists( 'YearBuilt', $this->getChangedColumns() ) ) { $strSql .= ' year_built = ' . $this->sqlYearBuilt() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_remodeled = ' . $this->sqlYearRemodeled(). ',' ; } elseif( true == array_key_exists( 'YearRemodeled', $this->getChangedColumns() ) ) { $strSql .= ' year_remodeled = ' . $this->sqlYearRemodeled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_description = ' . $this->sqlShortDescription(). ',' ; } elseif( true == array_key_exists( 'ShortDescription', $this->getChangedColumns() ) ) { $strSql .= ' short_description = ' . $this->sqlShortDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats(). ',' ; } elseif( true == array_key_exists( 'AllowsCats', $this->getChangedColumns() ) ) { $strSql .= ' allows_cats = ' . $this->sqlAllowsCats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs(). ',' ; } elseif( true == array_key_exists( 'AllowsDogs', $this->getChangedColumns() ) ) { $strSql .= ' allows_dogs = ' . $this->sqlAllowsDogs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_availability = ' . $this->sqlHasAvailability(). ',' ; } elseif( true == array_key_exists( 'HasAvailability', $this->getChangedColumns() ) ) { $strSql .= ' has_availability = ' . $this->sqlHasAvailability() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' update_vacancy_amenities = ' . $this->sqlUpdateVacancyAmenities(). ',' ; } elseif( true == array_key_exists( 'UpdateVacancyAmenities', $this->getChangedColumns() ) ) { $strSql .= ' update_vacancy_amenities = ' . $this->sqlUpdateVacancyAmenities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_redirect_url = ' . $this->sqlVacancyRedirectUrl(). ',' ; } elseif( true == array_key_exists( 'VacancyRedirectUrl', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_redirect_url = ' . $this->sqlVacancyRedirectUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_amount = ' . $this->sqlRewardAmount(). ',' ; } elseif( true == array_key_exists( 'RewardAmount', $this->getChangedColumns() ) ) { $strSql .= ' reward_amount = ' . $this->sqlRewardAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_description = ' . $this->sqlRewardDescription(). ',' ; } elseif( true == array_key_exists( 'RewardDescription', $this->getChangedColumns() ) ) { $strSql .= ' reward_description = ' . $this->sqlRewardDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_post_testimonials = ' . $this->sqlIsAutoPostTestimonials(). ',' ; } elseif( true == array_key_exists( 'IsAutoPostTestimonials', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_post_testimonials = ' . $this->sqlIsAutoPostTestimonials() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName(). ',' ; } elseif( true == array_key_exists( 'OwnerName', $this->getChangedColumns() ) ) { $strSql .= ' owner_name = ' . $this->sqlOwnerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_address = ' . $this->sqlWebsiteAddress(). ',' ; } elseif( true == array_key_exists( 'WebsiteAddress', $this->getChangedColumns() ) ) { $strSql .= ' website_address = ' . $this->sqlWebsiteAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data = ' . $this->sqlData(). ',' ; } elseif( true == array_key_exists( 'Data', $this->getChangedColumns() ) ) { $strSql .= ' data = ' . $this->sqlData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'company_id' => $this->getCompanyId(),
			'property_id' => $this->getPropertyId(),
			'min_rent' => $this->getMinRent(),
			'max_rent' => $this->getMaxRent(),
			'min_square_feet' => $this->getMinSquareFeet(),
			'max_square_feet' => $this->getMaxSquareFeet(),
			'min_bedrooms' => $this->getMinBedrooms(),
			'max_bedrooms' => $this->getMaxBedrooms(),
			'min_bathrooms' => $this->getMinBathrooms(),
			'max_bathrooms' => $this->getMaxBathrooms(),
			'number_of_buildings' => $this->getNumberOfBuildings(),
			'number_of_acres' => $this->getNumberOfAcres(),
			'number_of_units' => $this->getNumberOfUnits(),
			'year_built' => $this->getYearBuilt(),
			'year_remodeled' => $this->getYearRemodeled(),
			'short_description' => $this->getShortDescription(),
			'allows_cats' => $this->getAllowsCats(),
			'allows_dogs' => $this->getAllowsDogs(),
			'has_availability' => $this->getHasAvailability(),
			'update_vacancy_amenities' => $this->getUpdateVacancyAmenities(),
			'vacancy_redirect_url' => $this->getVacancyRedirectUrl(),
			'reward_amount' => $this->getRewardAmount(),
			'reward_description' => $this->getRewardDescription(),
			'is_auto_post_testimonials' => $this->getIsAutoPostTestimonials(),
			'owner_name' => $this->getOwnerName(),
			'website_address' => $this->getWebsiteAddress(),
			'data' => $this->getData(),
			'created_on' => $this->getCreatedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>