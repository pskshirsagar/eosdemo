<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyAmenities
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyAmenities extends CEosPluralBase {

	/**
	 * @return CMktgPropertyAmenity[]
	 */
	public static function fetchMktgPropertyAmenities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyAmenity', $objDatabase );
	}

	/**
	 * @return CMktgPropertyAmenity
	 */
	public static function fetchMktgPropertyAmenity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyAmenity', $objDatabase );
	}

	public static function fetchMktgPropertyAmenityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_amenities', $objDatabase );
	}

	public static function fetchMktgPropertyAmenityById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyAmenity( sprintf( 'SELECT * FROM mktg_property_amenities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyAmenitiesByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyAmenities( sprintf( 'SELECT * FROM mktg_property_amenities WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAmenitiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyAmenities( sprintf( 'SELECT * FROM mktg_property_amenities WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAmenitiesByCompanyMediaFileId( $intCompanyMediaFileId, $objDatabase ) {
		return self::fetchMktgPropertyAmenities( sprintf( 'SELECT * FROM mktg_property_amenities WHERE company_media_file_id = %d', ( int ) $intCompanyMediaFileId ), $objDatabase );
	}

}
?>