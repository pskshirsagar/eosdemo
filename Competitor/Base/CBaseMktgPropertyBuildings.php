<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyBuildings
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyBuildings extends CEosPluralBase {

	/**
	 * @return CMktgPropertyBuilding[]
	 */
	public static function fetchMktgPropertyBuildings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyBuilding', $objDatabase );
	}

	/**
	 * @return CMktgPropertyBuilding
	 */
	public static function fetchMktgPropertyBuilding( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyBuilding', $objDatabase );
	}

	public static function fetchMktgPropertyBuildingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_buildings', $objDatabase );
	}

	public static function fetchMktgPropertyBuildingById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyBuilding( sprintf( 'SELECT * FROM mktg_property_buildings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyBuildingsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyBuildings( sprintf( 'SELECT * FROM mktg_property_buildings WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyBuildingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyBuildings( sprintf( 'SELECT * FROM mktg_property_buildings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>