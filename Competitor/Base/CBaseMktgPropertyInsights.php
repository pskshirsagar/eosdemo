<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyInsights
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyInsights extends CEosPluralBase {

	/**
	 * @return CMktgPropertyInsight[]
	 */
	public static function fetchMktgPropertyInsights( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMktgPropertyInsight::class, $objDatabase );
	}

	/**
	 * @return CMktgPropertyInsight
	 */
	public static function fetchMktgPropertyInsight( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMktgPropertyInsight::class, $objDatabase );
	}

	public static function fetchMktgPropertyInsightCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_insights', $objDatabase );
	}

	public static function fetchMktgPropertyInsightById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyInsight( sprintf( 'SELECT * FROM mktg_property_insights WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyInsightsByCid( $intCid, $objDatabase ) {
		return self::fetchMktgPropertyInsights( sprintf( 'SELECT * FROM mktg_property_insights WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMktgPropertyInsightsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyInsights( sprintf( 'SELECT * FROM mktg_property_insights WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyInsightsByCompetitorCid( $intCompetitorCid, $objDatabase ) {
		return self::fetchMktgPropertyInsights( sprintf( 'SELECT * FROM mktg_property_insights WHERE competitor_cid = %d', $intCompetitorCid ), $objDatabase );
	}

	public static function fetchMktgPropertyInsightsByCompetitorPropertyId( $intCompetitorPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyInsights( sprintf( 'SELECT * FROM mktg_property_insights WHERE competitor_property_id = %d', $intCompetitorPropertyId ), $objDatabase );
	}

}
?>