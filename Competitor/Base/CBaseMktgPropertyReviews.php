<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyReviews
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyReviews extends CEosPluralBase {

	/**
	 * @return CMktgPropertyReview[]
	 */
	public static function fetchMktgPropertyReviews( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyReview', $objDatabase );
	}

	/**
	 * @return CMktgPropertyReview
	 */
	public static function fetchMktgPropertyReview( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyReview', $objDatabase );
	}

	public static function fetchMktgPropertyReviewCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_reviews', $objDatabase );
	}

	public static function fetchMktgPropertyReviewById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyReview( sprintf( 'SELECT * FROM mktg_property_reviews WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyReviewsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyReviews( sprintf( 'SELECT * FROM mktg_property_reviews WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>