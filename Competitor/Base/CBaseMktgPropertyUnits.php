<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUnits
 * Do not add any new functions to this class.
 */

class CBaseMktgPropertyUnits extends CEosPluralBase {

	/**
	 * @return CMktgPropertyUnit[]
	 */
	public static function fetchMktgPropertyUnits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMktgPropertyUnit', $objDatabase );
	}

	/**
	 * @return CMktgPropertyUnit
	 */
	public static function fetchMktgPropertyUnit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMktgPropertyUnit', $objDatabase );
	}

	public static function fetchMktgPropertyUnitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mktg_property_units', $objDatabase );
	}

	public static function fetchMktgPropertyUnitById( $intId, $objDatabase ) {
		return self::fetchMktgPropertyUnit( sprintf( 'SELECT * FROM mktg_property_units WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByRemotePropertyUnitId( $strRemotePropertyUnitId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE remote_property_unit_id = \'%s\'', $strRemotePropertyUnitId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByRemoteUnitSpaceId( $strRemoteUnitSpaceId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE remote_unit_space_id = \'%s\'', $strRemoteUnitSpaceId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByCompanyId( $intCompanyId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE company_id = %d', ( int ) $intCompanyId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByPropertyFloorplanId( $intPropertyFloorplanId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE property_floorplan_id = %d', ( int ) $intPropertyFloorplanId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByPropertyUnitTypeId( $intPropertyUnitTypeId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE property_unit_type_id = %d', ( int ) $intPropertyUnitTypeId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByPropertyBuildingId( $intPropertyBuildingId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE property_building_id = %d', ( int ) $intPropertyBuildingId ), $objDatabase );
	}

	public static function fetchMktgPropertyUnitsByPropertyFloorId( $intPropertyFloorId, $objDatabase ) {
		return self::fetchMktgPropertyUnits( sprintf( 'SELECT * FROM mktg_property_units WHERE property_floor_id = %d', ( int ) $intPropertyFloorId ), $objDatabase );
	}

}
?>