<?php

class CPricingCompetitorExtract extends CBasePricingCompetitorExtract {

	const QUEUE_STATUS_NEW 			= 'NEW';
	const QUEUE_STATUS_RUNNING 		= 'RUNNING';
	const QUEUE_STATUS_READY 		= 'READY';
	const QUEUE_STATUS_COMPLETED 	= 'COMPLETED';
	const QUEUE_STATUS_FAILED 		= 'FAILED';

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>