<?php

class CCachedNewUnitTypePeriod extends CBaseCachedNewUnitTypePeriod {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupHash() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastExtractRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalApplicationCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBedroomCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnitSpaceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
