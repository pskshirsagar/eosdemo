<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompetitiveAdjustments
 * Do not add any new functions to this class.
 */

class CMktgCompetitiveAdjustments extends CBaseMktgCompetitiveAdjustments {

	public static function fetchMktgCompetitiveAdjustmentsByMktgPropertyIdByMktgCompetitorPropertyId( $intMktgPropertyId, $intMktgCompetitorPropertyId, $objDatabase ) {
		$strSql = 'SELECT
						mca.*
					FROM
						mktg_competitive_adjustments mca
					WHERE
						mca.property_id = ' . ( int ) $intMktgPropertyId . '
						AND mca.competitor_property_id = ' . ( int ) $intMktgCompetitorPropertyId;

		return parent::fetchMktgCompetitiveAdjustments( $strSql, $objDatabase );
	}

}
?>