<?php

class CMktgEntity extends CBaseMktgEntity {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCustomerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameMiddle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAreaCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingStreetLine1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingStreetLine2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingStreetLine3() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingStateCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingProvince() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMailingPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaxNumberEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPasswordQuestion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPasswordAnswerEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontAllowLogin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDontAcceptPayments() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovalIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTermsApprovalIp() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTermsApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivatedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>