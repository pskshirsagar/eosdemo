<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyAddresses
 * Do not add any new functions to this class.
 */

class CMktgPropertyAddresses extends CBaseMktgPropertyAddresses {

	public static function fetchMktgPropertyAddressesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyAddresses( sprintf( 'SELECT * FROM mktg_property_addresses WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAddressByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyAddress( sprintf( 'SELECT * FROM mktg_property_addresses WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSimpleNewOtherCompetitorPropertiesByCompetitorPropertyIdsByMktgPropertyIdByPropertyAddressByFilter( $strPropertyIds, $intPropertyId, $objPropertyAddress, $intCompititorsDistanceLimit, $arrmixFilter, $intCompetitorsCountLimit, $objDatabase ) {

		$strFilter = '';

		if( true == isset( $arrmixFilter['property_type'] ) && true == valArr( $arrmixFilter['property_type'] ) ) {
			$strFilter = $strFilter . " AND mp.property_type IN ( '" . implode( "','", $arrmixFilter['property_type'] ) . "' ) ";
		}

		if( true == isset( $arrmixFilter['bedroom_count'] ) && true == valArr( $arrmixFilter['bedroom_count'] ) ) {

			if( true == in_array( 4, $arrmixFilter['bedroom_count'] ) ) {
				$strFilter = $strFilter . ' AND ( ( mpf.number_of_bedrooms IN ( ' . implode( ',', $arrmixFilter['bedroom_count'] ) . ' ) ) OR ( mpf.number_of_bedrooms > 4 ) )';
			} else {
				$strFilter = $strFilter . ' AND mpf.number_of_bedrooms IN ( ' . implode( ',', $arrmixFilter['bedroom_count'] ) . ' ) ';
			}
		}

		if( true == isset( $arrmixFilter['bathroom_count'] ) && true == valArr( $arrmixFilter['bathroom_count'] ) ) {

			if( true == in_array( 3, $arrmixFilter['bathroom_count'] ) ) {
				$strFilter = $strFilter . ' AND ( ( mpf.number_of_bathrooms IN ( ' . implode( ',', $arrmixFilter['bathroom_count'] ) . ' ) ) OR ( mpf.number_of_bathrooms > 3 ) )';
			} else {
				$strFilter = $strFilter . ' AND mpf.number_of_bathrooms IN ( ' . implode( ',', $arrmixFilter['bathroom_count'] ) . ' ) ';
			}
		}

		$intMinRent = 0;
		$intMaxRent = 0;
		if( true == isset( $arrmixFilter['rent_range'] ) && true == valArr( $arrmixFilter['rent_range'] ) && ( '' != $arrmixFilter['rent_range'][0] || '' != $arrmixFilter['rent_range'][1] ) ) {

			$intMinRent = $arrmixFilter['rent_range'][0];
			$intMaxRent = $arrmixFilter['rent_range'][1];

			$strFilter = $strFilter . ' AND ( ( mpf.min_rent >= ' . ( int ) $intMinRent . ' AND mpf.max_rent <= ' . ( int ) $intMaxRent . ' )';
			$strFilter = $strFilter . ' OR ( mpf.min_rent >= ' . ( int ) $intMinRent . ' AND mpf.max_rent IS NULL )';
			$strFilter = $strFilter . ' OR ( mpf.max_rent <= ' . ( int ) $intMaxRent . ' AND mpf.min_rent IS NULL ) )';
		}

		$intMinSqft = 0;
		$intMaxSqft = 0;

		if( true == isset( $arrmixFilter['sqft_range'] ) && true == valArr( $arrmixFilter['sqft_range'] ) && ( '' != $arrmixFilter['sqft_range'][0] || '' != $arrmixFilter['sqft_range'][1] ) ) {

			$intMinSqft = $arrmixFilter['sqft_range'][0];
			$intMaxSqft = $arrmixFilter['sqft_range'][1];

			$strFilter = $strFilter . ' AND ( ( mpf.min_square_feet >= ' . ( int ) $intMinSqft . ' AND mpf.max_square_feet <= ' . ( int ) $intMaxSqft . ' )';
			$strFilter = $strFilter . ' OR ( mpf.min_square_feet >= ' . ( int ) $intMinSqft . ' AND mpf.max_square_feet IS NULL )';
			$strFilter = $strFilter . ' OR ( mpf.max_square_feet <= ' . ( int ) $intMaxSqft . ' AND mpf.min_square_feet IS NULL ) )';
		}

		$strMileSelect    = '';

		if( true == valObj( $objPropertyAddress, 'CMktgPropertyAddress' ) ) {

			if( '' != $objPropertyAddress->getLatitude() && false == is_null( $objPropertyAddress->getLatitude() ) && '' != $objPropertyAddress->getLongitude() && false == is_null( $objPropertyAddress->getLongitude() ) ) {
				$strMileSelect    = 'ROUND( func_calculate_miles( ' . $objPropertyAddress->getLatitude() . ', ' . $objPropertyAddress->getLongitude() . ', CAST( mpa.latitude AS NUMERIC ), CAST( mpa.longitude AS NUMERIC ) ) ) AS distance_in_miles,';
 			}
		}

		$strSql = 'SELECT
						mp.id,
						mp.property_name,
						mp.source,
						mpa.id AS property_address_id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
							ELSE NULL
						END AS longitude,
						CASE
							WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
							ELSE NULL
						END AS latitude,
						' . $strMileSelect . '
						ROUND( MIN( mpf.min_rent ) ) AS min_rent,
						ROUND( MAX( mpf.max_rent ) ) AS max_rent,
						CONCAT( mp.property_name, \'|\', mpa.latitude, \'|\', mpa.longitude ) AS name_lat_long,
						mc.company_name
					FROM
						mktg_properties mp
					    CROSS JOIN
					    (
					      SELECT
					          latitude,
					          longitude
					      FROM
					          mktg_property_addresses
					      WHERE
					          property_id = ' . ( int ) $intPropertyId . '
					          AND ( address_type = \'Primary\'
					          OR address_type IS NULL )
					    ) smpa
					    JOIN mktg_property_addresses mpa ON ( mpa.property_id = mp.id AND ( address_type = \'Primary\' OR mpa.address_type IS NULL ) )
					    LEFT JOIN mktg_property_floorplans mpf ON ( mpf.property_id = mp.id )
					    JOIN mktg_companies mc ON ( mc.id = mp.company_id AND mc.company_status_type = \'Client\' )
					WHERE
						mp.id NOT IN ( ' . $strPropertyIds . ' )
						AND mp.id <> ' . ( int ) $intPropertyId . '
						AND mpa.street_line1 NOT LIKE mp.property_name
						AND mp.is_disabled = \'N\'
						AND CASE WHEN  mp.source = \'works\' AND mc.company_status_type = \'Client\'  
			                THEN TRUE
			                ELSE FALSE
			             END
						AND CASE
								WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.latitude
								ELSE NULL
							END IS NOT NULL
						AND CASE
								WHEN mpa.longitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' AND mpa.latitude ~ E\'^[\-\+]?[0-9]+[\.]{1}[0-9]+$\' THEN mpa.longitude
								ELSE NULL
							END IS NOT NULL
 						AND func_calculate_miles ( smpa.latitude::float, smpa.longitude::float, mpa.latitude::float, mpa.longitude::float ) < ' . ( int ) $intCompititorsDistanceLimit . '
 						AND mp.source IN (\'works\', \'system\')
						' . $strFilter . '
					GROUP BY
						mp.id,
						mp.property_name,
						mp.source,
						mpa.id,
						mpa.street_line1,
						mpa.street_line2,
						mpa.city,
						mpa.state_code,
						mpa.postal_code,
						mpa.longitude,
						mpa.latitude,
						mc.company_name
					ORDER BY
					' . $objDatabase->getCollateSort( 'mp.property_name' ) . '
                    LIMIT ' . ( int ) $intCompetitorsCountLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPrimaryMktgPropertyAddressByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyAddress( sprintf( 'SELECT * FROM mktg_property_addresses WHERE address_type = \'Primary\' AND property_id = %d ORDER BY remote_primary_key DESC LIMIT 1', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMktgPropertyAddressesByRemotePrimaryKeyByCompanyId( $intRemotePrimaryKey, $intCompanyId, $objDatabase ) {
		if( true == empty( $intRemotePrimaryKey ) || true == empty( $intCompanyId ) ) {
			return false;
		}

		$strSql = sprintf( 'SELECT
								    mpa.*
								FROM
								    mktg_property_addresses mpa
								WHERE
									company_id = %1$d 
			                        AND remote_primary_key = \'%2$d\'', $intCompanyId, $intRemotePrimaryKey );

		return self::fetchMktgPropertyAddress( $strSql, $objDatabase );
	}

}
?>