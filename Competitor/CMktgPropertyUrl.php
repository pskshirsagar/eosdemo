<?php

class CMktgPropertyUrl extends CBaseMktgPropertyUrl {

	protected $m_strStatus;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function setStatus( $strStatus ) {
		$this->m_strStatus = trim( stripcslashes( $strStatus ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrmixValues['status'] ) && $boolDirectSet ) {
			$this->m_strStatus = trim( stripcslashes( $arrmixValues['status'] ) );
		} elseif( isset( $arrmixValues['status'] ) ) {
			$this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['status'] ) : $arrmixValues['status'] );
		}
	}

}
?>