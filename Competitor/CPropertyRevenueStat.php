<?php

class CPropertyRevenueStat extends CBasePropertyRevenueStat {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOccupancyTypes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntrataPricingUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLroIntegratedUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYieldstarIntegratedUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewLeases() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNewRenewalLeases() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveIncome() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyLatitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyLongitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>