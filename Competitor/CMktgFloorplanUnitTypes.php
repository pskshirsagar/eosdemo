<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgFloorplanUnitTypes
 * Do not add any new functions to this class.
 */

class CMktgFloorplanUnitTypes extends CBaseMktgFloorplanUnitTypes {

	public static function fetchMktgFloorplanUnitTypesByWorksPropertyIdByMktgPropertyFloorplanId( $intPropertyId, $intMktgPropertyFloorplanId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						mktg_floorplan_unit_types
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND competitor_property_floorplan_id = ' . ( int ) $intMktgPropertyFloorplanId;

		return parent::fetchMktgFloorplanUnitTypes( $strSql, $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByWorksPropertyIdByMktgPropertyId( $intPropertyId, $intMktgPropertyId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						mktg_floorplan_unit_types
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND competitor_property_id = ' . ( int ) $intMktgPropertyId;

		return parent::fetchMktgFloorplanUnitTypes( $strSql, $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypesByMktgPropertyFloorplanIds( $arrintMktgPropertyFloorplanIds, $objDatabase ) {
		if( false == valArr( $arrintMktgPropertyFloorplanIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						mktg_floorplan_unit_types
					WHERE
						competitor_property_floorplan_id IN ( ' . implode( ',', $arrintMktgPropertyFloorplanIds ) . ' )';

		return parent::fetchMktgFloorplanUnitTypes( $strSql, $objDatabase );
	}

	public static function fetchMktgFloorplanUnitTypeByCompetitorPropertyFloorplanIdByPropertyUnitTypeId( $intCompetitorPropertyFloorplanId, $intPropertyUnitTypeId, $objDatabase ) {
			$strSql = 'SELECT
							*
						FROM
							mktg_floorplan_unit_types
						WHERE
							competitor_property_floorplan_id = ' . ( int ) $intCompetitorPropertyFloorplanId . '
							AND property_unit_type_id = ' . ( int ) $intPropertyUnitTypeId;

			return parent::fetchMktgFloorplanUnitType( $strSql, $objDatabase );
	}

	public static function fetchFloorplanIdsByPropertyIdByCompetitorPropertyIds( $intMktgPropertyId, $arrintCompetitorsIds, $objDatabase ) {

		$strSql = 'SELECT
                        competitor_property_floorplan_id
				   FROM
						mktg_floorplan_unit_types
                   WHERE
                        property_id = ' . ( int ) $intMktgPropertyId . '
                        AND competitor_property_id IN ( ' . implode( ', ', $arrintCompetitorsIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

}
?>