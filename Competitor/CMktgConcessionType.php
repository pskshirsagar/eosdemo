<?php

class CMktgConcessionType extends CBaseMktgConcessionType {

	const  CUSTOM_INCENTIVE       = 1;
	const  GIFT_CARD              = 2;
	const  GIFT_INCENTIVE         = 3;
	const  RENT_DISCOUNT_MONTHLY  = 4;
	const  RENT_DISCOUNT_ONE_TIME = 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionFrequency() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>