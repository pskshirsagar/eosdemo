<?php

class CHtmlInputType {

	const TEXTBOX = 'textbox';
	const DROPDOWN = 'dropdown';
	const RADIO = 'radio';
	const CHECKBOX = 'checkbox';

}
?>