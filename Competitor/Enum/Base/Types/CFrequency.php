<?php

class CFrequency {

	const WEEKLY = 'weekly';
	const BIWEEKLY = 'biweekly';
	const MONTHLY = 'monthly';
	const QUARTERLY = 'quarterly';
	const ONETIME = 'onetime';
	const ADHOC = 'AdHoc';

}
?>