<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyAmenities
 * Do not add any new functions to this class.
 */

class CMktgPropertyAmenities extends CBaseMktgPropertyAmenities {

    public static function fetchMktgPropertyAmenities( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CMktgPropertyAmenity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchMktgPropertyAmenity( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CMktgPropertyAmenity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>