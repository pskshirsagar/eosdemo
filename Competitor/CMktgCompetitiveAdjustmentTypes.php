<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompetitiveAdjustmentTypes
 * Do not add any new functions to this class.
 */

class CMktgCompetitiveAdjustmentTypes extends CBaseMktgCompetitiveAdjustmentTypes {

	public static function fetchMktgCompetitiveAdjustmentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMktgCompetitiveAdjustmentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCompetitiveAdjustmentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMktgCompetitiveAdjustmentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCompetitiveAdjustmentTypesByIsPublished( $intIsPublished, $objDatabase ) {
		return self::fetchMktgCompetitiveAdjustmentTypes( sprintf( 'SELECT * FROM mktg_competitive_adjustment_types WHERE is_published = \'%s\'', $intIsPublished ), $objDatabase );
	}

}
?>