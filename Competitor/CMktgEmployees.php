<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgEmployees
 * Do not add any new functions to this class.
 */

class CMktgEmployees extends CBaseMktgEmployees {

	public static function fetchMktgEmployees( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgEmployee::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgEmployee( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgEmployee::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>