<?php

class CMktgAccount extends CBaseMktgAccount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDefaultBillingAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRelatedCompanyAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>