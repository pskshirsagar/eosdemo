<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgAccounts
 * Do not add any new functions to this class.
 */

class CMktgAccounts extends CBaseMktgAccounts {

	public static function fetchMktgAccounts( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgAccount::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgAccount( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgAccount::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>