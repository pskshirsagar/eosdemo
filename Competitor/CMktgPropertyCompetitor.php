<?php

class CMktgPropertyCompetitor extends CBaseMktgPropertyCompetitor {

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

    public function getCompetitorPropertyName() {
	    $objProperty = \Psi\Eos\Competitor\CMktgProperties::createService()->fetchMktgPropertyById( $this->getCompetitorPropertyId(), $this->getDatabase() );
	    if( true == valObj( $objProperty, 'CMktgProperty' ) ) {
		    return $objProperty->getPropertyName();
	    }
	    return false;
    }

}
?>