<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUnitTypes
 * Do not add any new functions to this class.
 */

class CMktgPropertyUnitTypes extends CBaseMktgPropertyUnitTypes {

	public static function fetchMktgPropertyUnitTypesByWorksCidByWorksPropertyId( $intEntrataCid, $intEntrataPropertyId, $objDatabase ) {
		$strSql = 'SELECT
						T1.*
					FROM(
						SELECT
							mput.id,
							mput.remote_primary_key,
							mput.company_id,
							mput.property_id,
							CASE
								WHEN mput.lookup_code IS NOT NULL AND mput.name IS NOT NULL
									THEN concat(mput.lookup_code,\'-\', mput.name)
								ELSE
									COALESCE(mput.lookup_code, mput.name)
							END AS lookup_code,
							mput.name,
							mput.description,
							mput.created_on,
							mput.updated_on,
							mput.is_disabled,
							mput.unit_mix,
							mput.sqft,
							mput.market_rent_total,
							rank() OVER( PARTITION BY mput.company_id, mput.property_id, mput.id, COALESCE( mput.lookup_code, mput.name ) ORDER BY mput.remote_primary_key DESC ) AS rank
						FROM
							mktg_property_unit_types mput
							JOIN mktg_companies mc ON (
								mc.remote_primary_key =  \'' . ( int ) $intEntrataCid . '\'
								AND mc.id = mput.company_id
							)
							JOIN mktg_properties mp ON (
								mp.remote_primary_key =  \'' . ( int ) $intEntrataPropertyId . '\'
								AND mp.company_id = mc.id
								AND mp.id = mput.property_id
							)
						WHERE
							COALESCE( BTRIM( mput.lookup_code ), BTRIM( mput.name ), \'\' ) != \'\'
							AND mput.is_disabled = \'N\'
							AND mput.name NOT ILIKE \'%' . CUnitType::CATCH_ALL_UNIT_TYPE . '%\'
						ORDER BY
							COALESCE(' . $objDatabase->getCollateSort( 'lookup_code' ) . ',' . $objDatabase->getCollateSort( 'mput.name' ) . ' )
					) AS T1
					WHERE T1.rank = 1
		';

		return self::fetchMktgPropertyUnitTypes( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypesByCompetitorPropertyFoorplanIdByCompetitorPropertyId( $intCompeitiorPropertyFloorplanId, $intCompetitorPropertyId, $objDatabase ) {

		$strSql = 'SELECT
						mput.*,
						mc.company_name,
						mp.property_name
					FROM
						mktg_property_unit_types mput
						JOIN mktg_floorplan_unit_types mfut ON(
							mfut.property_unit_type_id = mput.id
							AND mfut.competitor_property_floorplan_id = ' . ( int ) $intCompeitiorPropertyFloorplanId . '
							AND mfut.competitor_property_id = ' . ( int ) $intCompetitorPropertyId . '
						)
						JOIN mktg_companies mc ON(
							mc.id = mput.company_id
						)
						JOIN mktg_properties mp ON(
							mp.id = mput.property_id
						)
		';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypesByPropertyIds( $arrintMktgPropertyIds, $objDatabase ) {

		if( false == valArr( $arrintMktgPropertyIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						mput.*
					FROM
						mktg_property_unit_types mput
					WHERE
						mput.property_id IN ( ' . implode( ',', $arrintMktgPropertyIds ) . ' )';

		return parent::fetchMktgPropertyUnitTypes( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUnitTypeDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMktgPropertyUnitTypes( sprintf( 'SELECT * FROM mktg_property_unit_types WHERE property_id = %d AND is_disabled = \'N\'', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>