<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CPricingCompetitorExtracts
 * Do not add any new functions to this class.
 */

class CPricingCompetitorExtracts extends CBasePricingCompetitorExtracts {

	public static function fetchPricingCompetitorExtractsByStatus( $intIsRating = NULL, $strStatus, $objDatabase ) {

		if( 0 == $intIsRating ) {
			$strSql = 'SELECT * FROM pricing_competitor_extracts WHERE status = \'' . $strStatus . '\' AND is_rating = 0';
			return self::fetchPricingCompetitorExtracts( $strSql, $objDatabase );
		}
		if( 1 == $intIsRating ) {
			$strSql = ' SELECT
						pce.*,
						mpu.scraper_type
					FROM
						mktg_property_urls mpu
						JOIN pricing_competitor_extracts pce ON( mpu.id = pce.remote_primary_key )
					WHERE
						pce.status = \'' . $strStatus . '\' AND pce.is_rating = 1';

			return fetchData( $strSql, $objDatabase );
		}
		return [];
	}

	/**
	 * @param $arrstrStatuses
	 * @param $objDatabase
	 * @return \CPricingCompetitorExtract[]|null
	 */
	public static function fetchPricingCompetitorExtractsByStatuses( $arrstrStatuses, $objDatabase ) {
		$strSqlStatus = NULL;
		if( true == valArr( $arrstrStatuses ) ) {
			foreach( $arrstrStatuses as $strStatus ) {
				$strSqlStatus .= '\'' . $strStatus . '\',';
			}
			$strSqlStatus = rtrim( $strSqlStatus, ',' );
		} else {
			return NULL;
		}

		$strSql = 'SELECT * FROM pricing_competitor_extracts WHERE status IN ( ' . $strSqlStatus . ' ) ';

		return self::fetchPricingCompetitorExtracts( $strSql, $objDatabase );
	}

	public static function fetchPricingCompetitorExtractsByStatusByLimit( $strStatus, $intLimit, $objDatabase ) {

		$strSql = '(SELECT
						*
					FROM
						pricing_competitor_extracts pce
					WHERE
						pce.status = \'' . $strStatus . '\'
						AND pce.is_rating = 0
					ORDER BY
						pce.id
					LIMIT ' . ( int ) $intLimit . ')
					UNION ALL
					(SELECT
						*
					FROM
						pricing_competitor_extracts pce
					WHERE
						pce.status = \'' . $strStatus . '\'
						AND pce.is_rating = 1
					ORDER BY
						pce.id
					LIMIT ' . ( int ) $intLimit . ')';

		return self::fetchPricingCompetitorExtracts( $strSql, $objDatabase );
	}

	public static function fetchPropertiesScraperDetailsByCreatedOn( $intIsRating, $objDatabase ) {

		$strSql = 'SELECT
						pce.status,
						mpu.scraper_type,
						COUNT( mpu.id )
					FROM
						pricing_competitor_extracts pce
						JOIN mktg_property_urls mpu ON( mpu.id = pce.remote_primary_key AND mpu.scraper_type != \'unknown\' )
					WHERE
						pce.updated_on BETWEEN ( CURRENT_DATE - INTERVAL \'2 weeks\' )::DATE AND CURRENT_DATE
						AND pce.is_rating = ' . ( int ) $intIsRating . '
					GROUP BY
						mpu.scraper_type,
						pce.status
					ORDER BY
					' . $objDatabase->getCollateSort( 'mpu.scraper_type' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPricingCompetitorExtractsByInterval( $intInterval, $objDatabase ) {
		$strSql = 'SELECT *
					FROM pricing_competitor_extracts
					WHERE
						id IN(
							SELECT
								pce.id
							FROM
								pricing_competitor_extracts pce
								JOIN mktg_property_urls mpu ON(
									mpu.id = pce.remote_primary_key
									AND mpu.is_extract_ready = 1 )
							WHERE
								pce.status = \'' . CPricingWebReconLibrary::EXTRACT_QUEUE_STATUS_RUNNING . '\'
								AND pce.updated_on < ( CURRENT_TIMESTAMP - INTERVAL \'' . $intInterval . ' hours\' )
					)';

		return self::fetchPricingCompetitorExtracts( $strSql, $objDatabase );
	}

	public static function fetchPricingCompetitorExtractsByIds( $arrintIds, $objDatabase ) {
		$strSql = 'SELECT * FROM pricing_competitor_extracts WHERE id IN ( ' . implode( ', ', $arrintIds ) . ' )';
		return self::fetchPricingCompetitorExtracts( $strSql, $objDatabase );
	}

}
?>