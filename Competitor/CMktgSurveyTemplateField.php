<?php

class CMktgSurveyTemplateField extends CBaseMktgSurveyTemplateField {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFieldName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFieldType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsMandatory() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>