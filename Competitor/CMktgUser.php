<?php

class CMktgUser extends CBaseMktgUser {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSvnUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSvnPassword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAdministrator() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDisabled() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastLogin() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastAccess() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoginAttemptCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLastLoginAttemptOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>