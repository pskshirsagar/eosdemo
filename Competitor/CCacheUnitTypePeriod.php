<?php

class CCacheUnitTypePeriod extends CBaseCacheUnitTypePeriod {

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnitSpaceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalApplicationCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBedroomCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( effective_date, cid, property_id, unit_type_id, total_unit_space_count, lease_term_id, lease_start_window_id, total_application_count, rent, bedroom_count, base_rent, amenity_rent, special_rent, lease_start_window_start_date, lease_start_window_end_date )
					VALUES ( ' .
		          $this->sqlEffectiveDate() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlUnitTypeId() . ', ' .
		          $this->sqlTotalUnitSpaceCount() . ', ' .
		          $this->sqlLeaseTermId() . ', ' .
		          $this->sqlLeaseStartWindowId() . ', ' .
		          $this->sqlTotalApplicationCount() . ', ' .
		          $this->sqlRent() . ', ' .
		          $this->sqlBedroomCount() . ', ' .
		          $this->sqlBaseRent() . ', ' .
		          $this->sqlAmenityRent() . ', ' .
		          $this->sqlSpecialRent() . ', ' .
		          $this->sqlLeaseStartWindowStartDate() . ', ' .
		          $this->sqlLeaseStartWindowEndDate() . ' ) ';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>