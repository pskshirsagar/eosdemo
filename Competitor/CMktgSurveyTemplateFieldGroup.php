<?php

class CMktgSurveyTemplateFieldGroup extends CBaseMktgSurveyTemplateFieldGroup {

	const CONTACT_GROUP = 7;
	
	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>