<?php

class CLeadProgres extends CBaseLeadProgres {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseTermId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartWindowId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEffectiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationStartedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationPartiallyCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicationApprovedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeasePartiallyCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseCompletedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseApprovedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsAbandoned() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLoadedToCacheOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupHash() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentCalId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBedroomCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitSpaceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnitSpaceCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaseRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAmenityRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeaseApprovedEndedCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalNetCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExtractRange() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
