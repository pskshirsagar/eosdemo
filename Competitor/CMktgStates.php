<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgStates
 * Do not add any new functions to this class.
 */

class CMktgStates extends CBaseMktgStates {

	public static function fetchMktgStates( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgState::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgState( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgState::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgStateByCode( $strCode, $objDatabase ) {
		$strSql = 'SELECT * FROM mktg_states WHERE code = \'' . $strCode . '\'';
		return fetchData( $strSql, $objDatabase );
	}

}
?>