<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyLeaseTerms
 * Do not add any new functions to this class.
 */

class CMktgPropertyLeaseTerms extends CBaseMktgPropertyLeaseTerms {

    public static function fetchMktgPropertyLeaseTermsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
    	if( false == valArr( $arrintPropertyIds ) ) return NULL;

    	$strSql = 'SELECT
    					mplt.*
    			   	FROM
    					mktg_property_lease_terms mplt
    			   	WHERE
    			   		mplt.property_id IN( ' . implode( ', ', $arrintPropertyIds ) . ' )';

    	return self::fetchMktgPropertyLeaseTerms( $strSql, $objDatabase );
    }
}
?>