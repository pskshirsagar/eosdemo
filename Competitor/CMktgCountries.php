<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCountries
 * Do not add any new functions to this class.
 */

class CMktgCountries extends CBaseMktgCountries {

	public static function fetchMktgCountries( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgCountry::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCountry( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgCountry::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCountryByCode( $strCode, $objDatabase ) {
		$strSql = 'SELECT * FROM mktg_countries WHERE code = \'' . $strCode . '\'';
		return fetchData( $strSql, $objDatabase );
	}

}
?>