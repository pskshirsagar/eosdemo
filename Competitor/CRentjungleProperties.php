<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CRentjungleProperties
 * Do not add any new functions to this class.
 */

class CRentjungleProperties extends CBaseRentjungleProperties {

	public static function fetchRentjungleProperties( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRentjungleProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchRentjungleProperty( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRentjungleProperty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPaginatedRentjungleProperties( $strWhere, $strSqlOrder, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = '	SELECT
						*
					FROM
						rentjungle_properties '
					. $strWhere .
					' ORDER BY ' . $strSqlOrder . '
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $intLimit;

		return parent::fetchRentjungleProperties( $strSql, $objDatabase );
	}

	public static function fetchCustomRentjunglePropertyCount( $objDatabase ) {

		$strSql = 'SELECT
						LOWER(status) AS status,
						COUNT(*)
					FROM
						rentjungle_properties
					GROUP BY
						LOWER(status)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRentjunglePropertyByRjId( $strRjId, $objDatabase ) {
		return parent::fetchRentjungleProperty( sprintf( 'SELECT * FROM rentjungle_properties WHERE rj_id = \'%s\'', $strRjId ), $objDatabase );
	}

}
?>