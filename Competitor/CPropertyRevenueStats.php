<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CPropertyRevenueStats
 * Do not add any new functions to this class.
 */

class CPropertyRevenueStats extends CBasePropertyRevenueStats {

	public static function fetchPropertyRevenueStatsByEntrataCidByStartDateByEndDate( $intEntrataCid, $strStartDate, $strEndDate, $objDatabase ) {
		$strSql = sprintf( 'SELECT 
										* 
								  FROM property_revenue_stats 
								  WHERE 
								        entrata_cid = %1$d AND
								        effective_date BETWEEN DATE_TRUNC(\'month\',\'%2$s\'::DATE) 
								        AND DATE_TRUNC(\'month\',\'%3$s\'::DATE)', $intEntrataCid, $strStartDate, $strEndDate );
		return self::fetchPropertyRevenueStats( $strSql, $objDatabase );
	}

	public static function fetchPropertyGrowthByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) {
			return false;
		}
		$strSql = sprintf( 'SELECT
										prs.entrata_cid,
										prs.entrata_property_id,
										prs.property_name,
										prs.property_latitude,
										prs.property_longitude,
										SUM( CASE 
										        WHEN prs.effective_date::DATE BETWEEN ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'12 Months\' )::DATE AND CURRENT_DATE 
										        THEN prs.effective_income
										     END ) AS current_year_growth,
										SUM( CASE 
										        WHEN prs.effective_date::DATE BETWEEN ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'24 Months\' )::DATE AND ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'13 Months\' )
										        THEN prs.effective_income
										     END ) AS previous_year_growth,
										SUM(CASE
									            WHEN prs.effective_income IS NOT NULL AND prs.effective_income <> 0 AND
									            prs.effective_date::DATE BETWEEN ((CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL
									              \'24 Months\')::DATE AND ((CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'13 Months\') THEN
									             1
									            ELSE 0
								            END) AS previous_year_months_count
								    FROM property_revenue_stats  prs
									WHERE
										prs.entrata_cid = %d
										AND prs.entrata_property_id IN( %s ) 
									GROUP BY 
										prs.entrata_cid, 
										prs.entrata_property_id,
										prs.property_name,
										prs.property_latitude,
										prs.property_longitude', $intCid, implode( ',', $arrintPropertyIds ) );
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMarketGrowthByRadiusLatLong( $fltLatitudeNorth, $fltLatitudeSouth, $fltLongitudeEast, $fltLongitudeWest, $objDatabase ) {
		$strSql = sprintf( 'SELECT
									COUNT( DISTINCT prs.entrata_property_id ) AS properties_in_market,
										SUM( CASE 
										        WHEN prs.effective_date::DATE BETWEEN ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'12 Months\' )::DATE AND CURRENT_DATE 
										        THEN prs.effective_income
										     END ) AS current_year_growth,
										SUM( CASE 
										        WHEN prs.effective_date::DATE BETWEEN ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'24 Months\' )::DATE AND ( (CAST(DATE_TRUNC(\'month\', CURRENT_DATE) as date)) - INTERVAL \'13 Months\' )
										        THEN prs.effective_income
										     END ) AS previous_year_growth
								    FROM property_revenue_stats prs
									WHERE
										CASE 
											WHEN prs.property_latitude ~ \'^[-+]?[0-9]*\.?[0-9]+$\' AND prs.property_longitude ~ \'^[-+]?[0-9]*\.?[0-9]+$\' 
											THEN 
												prs.property_latitude::float <= %1$f 
												AND prs.property_latitude::float >= %2$f 
												AND prs.property_longitude::float <= %3$f
												AND prs.property_longitude::float >= %4$f
										ELSE 
											prs.property_latitude = \'1\'
										END', $fltLatitudeNorth, $fltLatitudeSouth, $fltLongitudeEast, $fltLongitudeWest );
		return fetchData( $strSql, $objDatabase );
	}

}
?>