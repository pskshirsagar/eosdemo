<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgTimeZones
 * Do not add any new functions to this class.
 */

class CMktgTimeZones extends CBaseMktgTimeZones {

	public static function fetchMktgTimeZones( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgTimeZone::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgTimeZone( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgTimeZone::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>