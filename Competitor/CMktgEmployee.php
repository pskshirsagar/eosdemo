<?php

class CMktgEmployee extends CBaseMktgEmployee {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeStatusType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartment() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNameFull() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneExtension() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBirthDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPermanentEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGmailUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGmailPasswordEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateStarted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDateTerminated() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMarketingBlurb() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxTaskVotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDnsHandle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkStationIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVpnIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQaIpAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailSignature() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsProjectManager() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDesignation() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>