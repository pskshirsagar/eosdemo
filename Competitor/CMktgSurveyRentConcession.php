<?php

class CMktgSurveyRentConcession extends CBaseMktgSurveyRentConcession {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSurveyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_id', 'Survey id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompetitorPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompetitorPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'competitor_property_id', 'Competitor property id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyFloorplanId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFloorplanName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBeds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBaths() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSquareFeet() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAdvertisedRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteRent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionExpiresOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionAmountType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConcessionGiftCardCompany() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedUnitTypeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedUnitTypeNames() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNonRefundableDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRefundableDeposit() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSurveyId();
				$boolIsValid &= $this->valCompetitorPropertyId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>