<?php

class CMktgPropertySurvey extends CBaseMktgPropertySurvey {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompetitorPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDraft() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveySubmittedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>