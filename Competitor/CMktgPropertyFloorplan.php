<?php

class CMktgPropertyFloorplan extends CBaseMktgPropertyFloorplan {

	protected $m_intMktgPropertyUnitTypeId;
	protected $m_intAssociatedUnitTypeCount;

    public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

    	if( isset( $arrmixValues['mktg_property_unit_type_id'] ) && $boolDirectSet ) $this->m_intMktgPropertyUnitTypeId = trim( $arrmixValues['mktg_property_unit_type_id'] );
    	elseif ( isset( $arrmixValues['mktg_property_unit_type_id'] ) ) $this->setMktgPropertyUnitTypeId( $arrmixValues['mktg_property_unit_type_id'] );

    	if( isset( $arrmixValues['associated_unit_type_count'] ) && $boolDirectSet )
    		$this->m_intAssociatedUnitTypeCount = trim( $arrmixValues['associated_unit_type_count'] );
    	elseif( isset( $arrmixValues['associated_unit_type_count'] ) )
    		$this->setAssociatedUnitTypeCount( $arrmixValues['associated_unit_type_count'] );

    	return;
    }

    public function setMktgPropertyUnitTypeId( $intMktgPropertyUnitTypeId ) {
    	$this->m_intMktgPropertyUnitTypeId = CStrings::strToIntDef( $intMktgPropertyUnitTypeId, NULL, false );
    }

    public function getMktgPropertyUnitTypeId() {
    	return $this->m_intMktgPropertyUnitTypeId;
    }

	public function setAssociatedUnitTypeCount( $intAssociatedUnitTypeCount ) {
		$this->m_intAssociatedUnitTypeCount = CStrings::strToIntDef( $intAssociatedUnitTypeCount, NULL, false );
	}

	public function getAssociatedUnitTypeCount() {
		return $this->m_intAssociatedUnitTypeCount;
	}

    public function valId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getId() ) || false == is_int( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Invalid Id' ) );
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        return true;
    }

    public function valCompanyId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getCompanyId() ) || false == is_int( $this->getCompanyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', 'Invalid Company Id' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getPropertyId() ) || false == is_int( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Invalid Property Id' ) );
        }

        return $boolIsValid;
    }

    public function valName() {
        return true;
    }

    public function valDescription() {
        return true;
    }

    public function valMinRent() {
        $boolIsValid = true;

    	if( false == is_null( $this->getMinRent() ) && false == is_float( $this->getMinRent() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'min_rent', 'Invalid Min Rent' ) );
        }

        return $boolIsValid;
    }

    public function valMaxRent() {
        $boolIsValid = true;

    	if( false == is_null( $this->getMaxRent() ) && false == is_float( $this->getMaxRent() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'max_rent', 'Invalid Max Rent' ) );
        }

   	 	if( false == is_null( $this->getMinRent() ) && false == is_null( $this->getMaxRent() ) && $this->getMaxRent() < $this->getMinRent() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', 'Max Rent should be greater than min rent' ) );
        }

        return $boolIsValid;
    }

    public function valMinDeposit() {
        return true;
    }

    public function valMaxDeposit() {
        return true;
    }

    public function valIsManualRange() {
        return true;
    }

    public function valMinSquareFeet() {
       $boolIsValid = true;

    	if( false == is_null( $this->getMinSquareFeet() ) && false == is_float( $this->getMinSquareFeet() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'min_square_feet', 'Invalid Min Square Feet' ) );
        }

        return $boolIsValid;
    }

    public function valMaxSquareFeet() {
       $boolIsValid = true;

    	if( false == is_null( $this->getMaxSquareFeet() ) && false == is_float( $this->getMaxSquareFeet() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', 'Invalid Max Square Feet' ) );
        }

        if( false == is_null( $this->getMinSquareFeet() ) && false == is_null( $this->getMaxSquareFeet() ) && $this->getMaxSquareFeet() < $this->getMinSquareFeet() ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'max_square_feet', 'Max Square Feet should be greater than min square feet' ) );
        }

        return $boolIsValid;
    }

    public function valNumberOfUnits() {
        return true;
    }

    public function valNumberOfFloors() {
        return true;
    }

    public function valNumberOfRooms() {
        return true;
    }

    public function valNumberOfBedrooms() {
        $boolIsValid = true;

    	if( true == is_null( $this->getNumberOfBedrooms() ) || false == is_int( $this->getNumberOfBedrooms() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bedrooms', 'Invalid Number of Bedrooms' ) );
        }

        return $boolIsValid;
    }

    public function valNumberOfBathrooms() {
        $boolIsValid = true;

    	if( true == is_null( $this->getNumberOfBathrooms() ) || false == is_float( $this->getNumberOfBathrooms() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'number_of_bathrooms', 'Invalid Number of Bathrooms' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCompanyId();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valName();
        		$boolIsValid &= $this->valDescription();
        		$boolIsValid &= $this->valMinRent();
        		$boolIsValid &= $this->valMaxRent();
        		$boolIsValid &= $this->valMinDeposit();
        		$boolIsValid &= $this->valMaxDeposit();
        		$boolIsValid &= $this->valIsManualRange();
        		$boolIsValid &= $this->valMinSquareFeet();
        		$boolIsValid &= $this->valMaxSquareFeet();
        		$boolIsValid &= $this->valNumberOfUnits();
        		$boolIsValid &= $this->valNumberOfFloors();
        		$boolIsValid &= $this->valNumberOfRooms();
        		$boolIsValid &= $this->valNumberOfBedrooms();
        		$boolIsValid &= $this->valNumberOfBathrooms();
        		break;

        	case VALIDATE_UPDATE:
        		$boolIsValid &= $this->valId();
        		$boolIsValid &= $this->valCompanyId();
        		$boolIsValid &= $this->valPropertyId();
        		$boolIsValid &= $this->valName();
        		$boolIsValid &= $this->valDescription();
        		$boolIsValid &= $this->valMinRent();
        		$boolIsValid &= $this->valMaxRent();
        		$boolIsValid &= $this->valMinDeposit();
        		$boolIsValid &= $this->valMaxDeposit();
        		$boolIsValid &= $this->valIsManualRange();
        		$boolIsValid &= $this->valMinSquareFeet();
        		$boolIsValid &= $this->valMaxSquareFeet();
        		$boolIsValid &= $this->valNumberOfUnits();
        		$boolIsValid &= $this->valNumberOfFloors();
        		$boolIsValid &= $this->valNumberOfRooms();
        		$boolIsValid &= $this->valNumberOfBedrooms();
        		$boolIsValid &= $this->valNumberOfBathrooms();
        		break;

        	case VALIDATE_DELETE:
        		$boolIsValid &= $this->valId();
        		break;

        	default:
        		// default case
        		$boolIsValid = true;
        		break;
        }

        return $boolIsValid;
    }

}
?>