<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanyMediaFiles
 * Do not add any new functions to this class.
 */

class CMktgCompanyMediaFiles extends CBaseMktgCompanyMediaFiles {

	public static function fetchMktgCompanyMediaFiles( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgCompanyMediaFile::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCompanyMediaFile( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgCompanyMediaFile::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>