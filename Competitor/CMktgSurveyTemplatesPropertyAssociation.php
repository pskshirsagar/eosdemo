<?php

class CMktgSurveyTemplatesPropertyAssociation extends CBaseMktgSurveyTemplatesPropertyAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>