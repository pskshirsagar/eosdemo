<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUrls
 * Do not add any new functions to this class.
 */

class CMktgPropertyUrls extends CBaseMktgPropertyUrls {

	public static function fetchExtractReadyMktgPropertyUrlsByExtractedOnInterval( $intExtractedOnInterval, $intFailCountLimit, $objDatabase ) {

		$strSql = 'SELECT
						mpu.*
					FROM
						mktg_property_urls mpu
						JOIN mktg_properties mp
							ON ( mpu.property_id = mp.id AND mp.source = \'system\' )
					WHERE
						mpu.is_extract_ready = 1
						AND mpu.fail_count < ' . ( int ) $intFailCountLimit . '
						AND ( ( mpu.last_extracted_on NOT BETWEEN ( CURRENT_DATE - INTERVAL \'' . $intExtractedOnInterval . ' day\' )::date AND ( CURRENT_DATE + 1 )::date )
								OR ( mpu.last_extracted_on IS NULL ) )';

		return self::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

	public static function fetchAllMktgPropertyUrls( $objDatabase ) {
		$strSql = 'SELECT * FROM mktg_property_urls where is_rating = 0 AND deleted_on IS NULL AND deleted_by IS NULL';
		return parent::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUrlsExtractedDataByMktgPropertyId( $intMktgPropertyId, $strOrderByField, $strOrderByType, $intIsRating, $objDatabase ) {

		$strSql = 'SELECT
						mpu.*,
						ed.*
					FROM
						mktg_property_urls mpu
						LEFT JOIN (
							SELECT
								pce.remote_primary_key,
								pce.status,
								pce.extracted_data,
								rank() OVER( PARTITION BY pce.remote_primary_key ORDER BY pce.updated_on DESC )
							FROM
								pricing_competitor_extracts pce
							WHERE
				                pce.is_rating = ' . ( int ) $intIsRating . '
						) AS ed ON( ed.remote_primary_key = mpu.id )
					WHERE
						mpu.is_rating = ' . ( int ) $intIsRating . '
						AND mpu.property_id = ' . ( int ) $intMktgPropertyId . '
						AND ( mpu.deleted_by IS NULL OR mpu.deleted_on IS NULL )
						AND ( ed.rank = 1 OR ed.rank IS NULL )
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . ',
						id ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomMktgPropertyUrls( $objDatabase ) {
		$strSql = ' SELECT
						mp.id,
						mp.property_name,
						mpu.id as property_url_id,
						mpu.url,
						mpu.fail_count,
						mpu.property_id,
						mpu.is_rating
					FROM
						mktg_property_urls mpu
						JOIN mktg_properties mp ON ( mp.id = mpu.property_id AND mp.is_disabled = \'N\' )
						JOIN mktg_properties mp2 ON ( mp2.source_property_id = mp.id AND mp2.is_disabled = \'N\')
						JOIN mktg_property_competitors mpc ON( mpc.competitor_property_id = mp2.id AND mpc.deleted_on IS NULL )
					WHERE
						mpu.is_rating = 0 
						AND mpu.is_extract_ready = 1
						AND mpu.deleted_on IS NULL 
						AND mpu.deleted_by IS NULL
						AND mpu.fail_count > 0
					ORDER BY
						mpu.fail_count DESC, mp.property_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyRatingUrlIds( $arrstrUrls, $objDatabase ) {
		$strSql = 'SELECT * FROM mktg_property_urls WHERE is_rating = 1 AND url IN( \'' . implode( '\',\'', $arrstrUrls ) . '\' )';
		return parent::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

	public static function fetchMarketingPropertyRatingUrl( $strUrl, $objDatabase ) {
		$strSql = "SELECT * FROM mktg_property_urls WHERE url = '" . $strUrl . "' AND is_rating = 1 ";
		return parent::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUrlDetails( $arrintMktgPropertyUrlIds, $intIsRating, $objDatabase ) {
			$strSql = 'SELECT
							mpu.property_id,
							mp.company_id,
							mpu.id
						FROM
							mktg_property_urls mpu
							JOIN mktg_properties mp ON( mp.id = mpu.property_id )
						WHERE
							mpu.id IN ( ' . $arrintMktgPropertyUrlIds . ' )
							AND mpu.is_rating = ' . ( int ) $intIsRating . '
							AND mp.source <> \'works\'';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMarketingPropertyUrlsByIds( $arrintMktgPropertyUrlIds, $objDatabase ) {
		if( false == valArr( $arrintMktgPropertyUrlIds ) ) return NULL;
		$strSql = 'SELECT * FROM mktg_property_urls WHERE id IN( ' . implode( ', ', $arrintMktgPropertyUrlIds ) . ' ) ';
		return parent::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

	public static function fetchMktgPropertyUrlsStatusByPropertyId( $intPropertyId, $objDatabase ) {
		$strSql = sprintf( "SELECT mpu.id,
						mpu.url,
						mpu.property_id,
						CASE 
							WHEN pce.status = 'FAILED' THEN 'FAILED'
							WHEN pce.status = 'COMPLETE' THEN 'COMPLETE'
							ELSE 'PROGRESS'
						END AS status 
					FROM mktg_property_urls mpu
 						LEFT JOIN pricing_competitor_extracts pce ON ( mpu.id = pce.remote_primary_key )
					WHERE mpu.property_id = %d
					ORDER BY pce.updated_on LIMIT 1", ( int ) $intPropertyId );

		return parent::fetchMktgPropertyUrls( $strSql, $objDatabase );
	}

}
?>