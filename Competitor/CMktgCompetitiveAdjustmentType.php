<?php

class CMktgCompetitiveAdjustmentType extends CBaseMktgCompetitiveAdjustmentType {

	const AMENITY_ADJUSTMENT 				= 1;
	const SPECIALS_CONCESSIONS_ADJUSTMENT	= 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>