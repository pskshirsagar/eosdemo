<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyUnitAmenities
 * Do not add any new functions to this class.
 */

class CMktgPropertyUnitAmenities extends CBaseMktgPropertyUnitAmenities {

    public static function fetchMktgPropertyUnitAmenities( $strSql, $objDatabase ) {
        return self::fetchCachedObjects( $strSql, 'CMktgPropertyUnitAmenity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

    public static function fetchMktgPropertyUnitAmenity( $strSql, $objDatabase ) {
        return self::fetchCachedObject( $strSql, 'CMktgPropertyUnitAmenity', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

}
?>