<?php

class CMktgPropertyInsight extends CBaseMktgPropertyInsight {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompetitorCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompetitorPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>