<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CCacheUnitTypePeriods
 * Do not add any new functions to this class.
 */

class CCacheUnitTypePeriods extends CBaseCacheUnitTypePeriods {

	public static function fetchCacheUnitTypePeriods( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCacheUnitTypePeriod::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCacheUnitTypePeriod( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCacheUnitTypePeriod::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMaxEffectiveDateByPropertyIdByCid( $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'SELECT max(effective_date) AS max_effective_date from cache_unit_type_periods WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function deleteCacheUnitTypePeriodsByEffectiveDateByPropertyIdByCid( $strEffectiveDate, $intPropertyId, $intCid, $objDatabase ) {
		$strSql = 'DELETE FROM cache_unit_type_periods where cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND effective_date >= ' . '\'' . $strEffectiveDate . '\'::DATE';
		return executeSql( $strSql, $objDatabase );
	}

}
?>