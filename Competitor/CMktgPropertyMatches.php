<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgPropertyMatches
 * Do not add any new functions to this class.
 */

class CMktgPropertyMatches extends CBaseMktgPropertyMatches {

	public static function fetchMktgPropertyMatches( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgPropertyMatch::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgPropertyMatch( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgPropertyMatch::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>