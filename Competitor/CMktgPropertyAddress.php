<?php

class CMktgPropertyAddress extends CBaseMktgPropertyAddress {

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) || false == is_int( $this->getId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Invalid Id' ) ) );
        }

        return $boolIsValid;
    }

    public function valRemotePrimaryKey() {
        return true;
    }

    public function valCompanyId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getCompanyId() ) || false == is_int( $this->getCompanyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'company_id', __( 'Invalid Company Id' ) ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) || false == is_int( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Invalid Property Id' ) ) );
        }

        return $boolIsValid;
    }

    public function valAddressType() {
        return true;
    }

    public function valTimeZoneId() {
        $boolIsValid = true;

    	if( true == is_null( $this->getTimeZoneId() ) || false == is_int( $this->getTimeZoneId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'time_zone_id', __( 'Invalid Time Zone Id' ) ) );
        }

        return $boolIsValid;
    }

    public function valMarketingName() {
        return true;
    }

    public function valStreetLine1() {
        $boolIsValid = true;

    	if( true == is_null( $this->getStreetLine1() ) || 1 > \psi\CStringService::singleton()->strlen( $this->getStreetLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', __( 'Address Line {%d, 0} is required.', [ 1 ] ) ) );
		}

        return $boolIsValid;
    }

    public function valStreetLine2() {
        return true;
    }

    public function valStreetLine3() {
        return true;
    }

    public function valCity() {
        $boolIsValid = true;

    	if( true == is_null( $this->getCity() ) || 1 > \psi\CStringService::singleton()->strlen( $this->getCity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'city', __( 'city is required.' ) ) );
		}

        return $boolIsValid;
    }

    public function valCounty() {
        return true;
    }

    public function valStateCode() {
        $boolIsValid = true;

    	if( true == is_null( $this->getStateCode() ) || 1 > \psi\CStringService::singleton()->strlen( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'state_code', __( 'State is required.' ) ) );
		}

        return $boolIsValid;
    }

    public function valProvince() {
        return true;
    }

    public function valPostalCode() {
        $boolIsValid = true;

    	if( true == is_null( $this->getPostalCode() ) || 0 > ( int ) $this->getPostalCode() || 99999 < ( int ) $this->getPostalCode() ) {
            $boolIsValid = false;
            $this->addErrorMsg( new \CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', __( 'Invalid Postal Code' ) ) );
        }

        return $boolIsValid;
    }

    public function valCountryCode() {
        return true;
    }

    public function valLongitude() {
        return true;
    }

    public function valLatitude() {
        return true;
    }

    public function valPlaceidKey() {
        return true;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            	$boolIsValid &= $this->valRemotePrimaryKey();
            	$boolIsValid &= $this->valCompanyId();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valAddressType();
            	$boolIsValid &= $this->valTimeZoneId();
            	$boolIsValid &= $this->valMarketingName();
            	$boolIsValid &= $this->valStreetLine1();
            	$boolIsValid &= $this->valStreetLine2();
            	$boolIsValid &= $this->valStreetLine3();
            	$boolIsValid &= $this->valCounty();
            	if( 'en_US' == CLocaleContainer::createService()->getLocaleCode() ) {
		            $boolIsValid &= $this->valCity();
					$boolIsValid &= $this->valStateCode();
		            $boolIsValid &= $this->valPostalCode();
	            }
            	$boolIsValid &= $this->valProvince();
            	$boolIsValid &= $this->valCountryCode();
            	$boolIsValid &= $this->valLongitude();
            	$boolIsValid &= $this->valLatitude();
            	break;

            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valId();
            	$boolIsValid &= $this->valRemotePrimaryKey();
            	$boolIsValid &= $this->valCompanyId();
            	$boolIsValid &= $this->valPropertyId();
            	$boolIsValid &= $this->valAddressType();
            	$boolIsValid &= $this->valTimeZoneId();
            	$boolIsValid &= $this->valMarketingName();
            	$boolIsValid &= $this->valStreetLine1();
            	$boolIsValid &= $this->valStreetLine2();
            	$boolIsValid &= $this->valStreetLine3();
            	$boolIsValid &= $this->valCounty();
	            if( 'en_US' == CLocaleContainer::createService()->getLocaleCode() ) {
		            $boolIsValid &= $this->valCity();
		            $boolIsValid &= $this->valStateCode();
		            $boolIsValid &= $this->valPostalCode();
	            }
            	$boolIsValid &= $this->valProvince();
            	$boolIsValid &= $this->valCountryCode();
            	$boolIsValid &= $this->valLongitude();
            	$boolIsValid &= $this->valLatitude();
            	$boolIsValid &= $this->valPlaceidKey();
            	break;

            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }

}
?>