<?php

class CMktgCompanyPayment extends CBaseMktgCompanyPayment {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAccountId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>