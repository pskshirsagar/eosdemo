<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Competitor\CMktgCompanyPayments
 * Do not add any new functions to this class.
 */

class CMktgCompanyPayments extends CBaseMktgCompanyPayments {

	public static function fetchMktgCompanyPayments( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMktgCompanyPayment::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMktgCompanyPayment( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMktgCompanyPayment::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>