<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Messaging\CQueueMessageStatuses
 * Do not add any new functions to this class.
 */

class CQueueMessageStatuses extends CBaseQueueMessageStatuses {

	public static function fetchQueueMessageStatuses( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CQueueMessageStatus', $objDatabase );
	}

	public static function fetchQueueMessageStatus( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CQueueMessageStatus', $objDatabase );
	}

	/**
	 * @param $strCorrelationId
	 * @param $objDatabase
	 * @return CQueueMessageStatus
	 */
	public static function fetchQueueMessageStatusByCorrelationId( $strCorrelationId, $objDatabase ) {
		return self::fetchQueueMessageStatus( sprintf( 'SELECT * FROM queue_message_statuses qms WHERE qms.correlation_id = \'%s\';', $strCorrelationId ), $objDatabase );
	}

	public static function fetchMessageCountByCorrelationIdByStatusByCreatedOn( $strCorrelationId, $objDatabase, $strMessageStatus = '', $strMessageQueuingStartTime = '' ) {

		if( false == valStr( $strCorrelationId ) ) return NULL;

		$strWhere = ( true == valStr( $strMessageStatus ) ) ? ' AND qms.status = \'' . $strMessageStatus . '\'': '';
		$strWhere .= ( true == valStr( $strMessageQueuingStartTime ) ) ? ' AND qms.created_on >= \'' . $strMessageQueuingStartTime . '\'' : '';

		$strSql = 'SELECT count( qms.id ) FROM queue_message_statuses qms WHERE qms.correlation_id LIKE \'%' . $strCorrelationId . '%\'' . $strWhere;

		$intMessageCount = 0;
		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset ( $arrintResponse[0]['count'] ) ) {
			$intMessageCount = $arrintResponse[0]['count'];
		}

		return $intMessageCount;
	}

	public static function fetchQueueMessageStatusesByMessageClassByCompanyUserIdByCid( $strMessageClass, $intCompanyUserId, $intCid, $objDatabase ) {
		$arrstrWhereConditions = [ 'TRUE' ];

		if( false == is_null( $intCid ) ) {
			$arrstrWhereConditions[] = 'qms.cid = ' . ( int ) $intCid;
		}

		if( false == is_null( $intCompanyUserId ) ) {
			$arrstrWhereConditions[] = 'qms.company_user_id = ' . ( int ) $intCompanyUserId;
		}

		if( false == is_null( $strMessageClass ) ) {
			$arrstrWhereConditions[] = 'qms.message_class = \'' . pg_escape_string( $strMessageClass ) . '\'';
		}

		$strSql = '
			SELECT
				*
			FROM
				queue_message_statuses qms
			WHERE
				' . implode( ' AND ', $arrstrWhereConditions ) . '
		';

		return self::fetchQueueMessageStatuses( $strSql, $objDatabase ) ?: [];
	}

	public static function fetchLeaseIdsByMessageClassByStatusesByLeaseIdsByCid( $strMessageClass, $arrstrMessageStatuses, $arrintLeaseIds, $intCid, $objDatabase, $boolIsFetchCount = false ) {

		// If no lease ids passed then fetch all messages where lease_ids not null along with other conditions satisfy in query.
		$strCondition = ' AND qms.message_body::JSONB->\'lease_ids\' IS NOT NULL ';
		if( true == valArr( $arrintLeaseIds ) ) {
			$strCondition = ' AND lease_ids <@ \'[ ' . implode( ',', $arrintLeaseIds ) . ' ]\' ';
		}

		$strSelectField = ( $boolIsFetchCount == true ) ? 'COUNT( lease_ids )' : 'lease_ids';

		$strSql = '
			SELECT
				' . $strSelectField . '
			FROM
				queue_message_statuses qms,
				jsonb_array_elements( message_body::JSONB->\'lease_ids\' ) AS lease_ids
			WHERE
				qms.cid = ' . ( int ) $intCid . '
				AND qms.message_class = \'' . $strMessageClass . '\'
				AND qms.status IN ( ' . implode( ',', $arrstrMessageStatuses ) . ' )
				AND log_datetime > NOW() - INTERVAL \'1 day\'
				' . $strCondition;

		if( true == $boolIsFetchCount ) {
			return fetchData( $strSql, $objDatabase );
		}

		return array_keys( rekeyArray( 'lease_ids', ( array ) fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchOrganizationContractUnitSpaceIdsByMessageClassByStatusesByOrganizationContractIdByCid( $strMessageClass, $arrstrMessageStatuses, $intOrganizationContractId, $intCid, $objDatabase ) {

		$strSql = 'SELECT
						jsonb_array_elements( message_body::JSONB->\'organization_contract_unit_space_ids\' ) as organization_contract_unit_space_ids
					FROM
						queue_message_statuses qms
					WHERE
						qms.cid = ' . ( int ) $intCid . '
						AND qms.message_class = \'' . $strMessageClass . '\'
						AND qms.status IN ( ' . implode( ',', $arrstrMessageStatuses ) . ' )
						AND log_datetime > NOW() - INTERVAL \'1 day\'
						AND message_body::JSONB->\'organization_contract_id\' = \'' . ( int ) $intOrganizationContractId . '\'';

		return array_keys( rekeyArray( 'organization_contract_unit_space_ids', ( array ) fetchData( $strSql, $objDatabase ) ) );
	}

	public static function fetchQueueMessageCountByMessageClassByMessageStatusesByPropertyIdByCompanyUserIdByCid( $strMessageClass, $arrstrMessageStatuses, $intPropertyId, $intCompanyUserId, $intCid, $objDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql = 'SELECT
						count( qms.id ) 
					FROM 
						queue_message_statuses qms
					WHERE
						qms.cid = ' . ( int ) $intCid . '
						AND qms.company_user_id = ' . ( int ) $intCompanyUserId . '
						AND qms.message_class = \'' . $strMessageClass . '\'
						AND qms.status IN ( ' . implode( ',', $arrstrMessageStatuses ) . ' )
						AND qms.log_datetime > NOW() - INTERVAL \'1 day\'
						AND qms.message_body::JSONB->>\'property_id\' =  \'' . $intPropertyId . '\'';

		$intMessageCount = 0;
		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset ( $arrintResponse[0]['count'] ) ) {
			$intMessageCount = $arrintResponse[0]['count'];
		}

		return $intMessageCount;
	}

	public static function fetchSubsidyCertificationIdsByMessageClassByStatusesBySubsidyCertificationIdsByCid( $strMessageClass, $arrstrMessageStatuses, $intCid, $objDatabase ) {

		$strSql = '
			SELECT
				CAST ( qms.message_body::JSONB ->> \'subsidy_certification_id\' as INTEGER ) AS subsidy_certification_id				
			FROM
				queue_message_statuses qms
			WHERE
				qms.cid = ' . ( int ) $intCid . '
				AND qms.message_body::JSONB->\'subsidy_certification_id\' IS NOT NULL
				AND qms.message_class = \'' . $strMessageClass . '\'
				AND qms.status IN ( ' . implode( ',', $arrstrMessageStatuses ) . ' )
				AND log_datetime > NOW() - INTERVAL \'1 day\'';

		return array_keys( rekeyArray( 'subsidy_certification_id', ( array ) fetchData( $strSql, $objDatabase ) ) );
	}
}