<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Messaging\CQueueMessageStatuses
 * Do not add any new functions to this class.
 */

class CBaseQueueMessageStatuses extends CEosPluralBase {

	/**
	 * @return CBaseQueueMessageStatuse[]
	 */
	public static function fetchQueueMessageStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CQueueMessageStatus', $objDatabase );
	}

	/**
	 * @return CBaseQueueMessageStatuse
	 */
	public static function fetchQueueMessageStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CQueueMessageStatus', $objDatabase );
	}

	public static function fetchQueueMessageStatusCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'queue_message_statuses', $objDatabase );
	}

	public static function fetchQueueMessageStatusById( $intId, $objDatabase ) {
		return self::fetchQueueMessageStatus( sprintf( 'SELECT * FROM queue_message_statuses WHERE id = %d', (int) $intId ), $objDatabase );
	}

	public static function fetchQueueMessageStatusesByCorrelationId( $strCorrelationId, $objDatabase ) {
		return self::fetchQueueMessageStatuses( sprintf( 'SELECT * FROM queue_message_statuses WHERE correlation_id = \'%s\'', $strCorrelationId ), $objDatabase );
	}

	public static function fetchQueueMessageStatusesByCid( $intCid, $objDatabase ) {
		return self::fetchQueueMessageStatuses( sprintf( 'SELECT * FROM queue_message_statuses WHERE cid = %d', (int) $intCid ), $objDatabase );
	}

	public static function fetchQueueMessageStatusesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchQueueMessageStatuses( sprintf( 'SELECT * FROM queue_message_statuses WHERE company_user_id = %d', (int) $intCompanyUserId ), $objDatabase );
	}

}
?>