<?php

class CBaseQueueMessageStatus extends CEosSingularBase {

	const TABLE_NAME = 'public.queue_message_statuses';

	protected $m_intId;
	protected $m_strCorrelationId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intStatusTtl;
	protected $m_strStatus;
	protected $m_strHandlerClass;
	protected $m_strMessageClass;
	protected $m_strMessageBody;
	protected $m_strLogDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['correlation_id'] ) && $boolDirectSet ) $this->set( 'm_strCorrelationId', trim( stripcslashes( $arrValues['correlation_id'] ) ) ); elseif( isset( $arrValues['correlation_id'] ) ) $this->setCorrelationId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['correlation_id'] ) : $arrValues['correlation_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['status_ttl'] ) && $boolDirectSet ) $this->set( 'm_intStatusTtl', trim( $arrValues['status_ttl'] ) ); elseif( isset( $arrValues['status_ttl'] ) ) $this->setStatusTtl( $arrValues['status_ttl'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['handler_class'] ) && $boolDirectSet ) $this->set( 'm_strHandlerClass', trim( stripcslashes( $arrValues['handler_class'] ) ) ); elseif( isset( $arrValues['handler_class'] ) ) $this->setHandlerClass( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['handler_class'] ) : $arrValues['handler_class'] );
		if( isset( $arrValues['message_class'] ) && $boolDirectSet ) $this->set( 'm_strMessageClass', trim( stripcslashes( $arrValues['message_class'] ) ) ); elseif( isset( $arrValues['message_class'] ) ) $this->setMessageClass( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message_class'] ) : $arrValues['message_class'] );
		if( isset( $arrValues['message_body'] ) && $boolDirectSet ) $this->set( 'm_strMessageBody', trim( stripcslashes( $arrValues['message_body'] ) ) ); elseif( isset( $arrValues['message_body'] ) ) $this->setMessageBody( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message_body'] ) : $arrValues['message_body'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCorrelationId( $strCorrelationId ) {
		$this->set( 'm_strCorrelationId', CStrings::strTrimDef( $strCorrelationId, 240, NULL, true ) );
	}

	public function getCorrelationId() {
		return $this->m_strCorrelationId;
	}

	public function sqlCorrelationId() {
		return ( true == isset( $this->m_strCorrelationId ) ) ? '\'' . addslashes( $this->m_strCorrelationId ) . '\'' : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setStatusTtl( $intStatusTtl ) {
		$this->set( 'm_intStatusTtl', CStrings::strToIntDef( $intStatusTtl, NULL, false ) );
	}

	public function getStatusTtl() {
		return $this->m_intStatusTtl;
	}

	public function sqlStatusTtl() {
		return ( true == isset( $this->m_intStatusTtl ) ) ? ( string ) $this->m_intStatusTtl : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 240, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setHandlerClass( $strHandlerClass ) {
		$this->set( 'm_strHandlerClass', CStrings::strTrimDef( $strHandlerClass, 240, NULL, true ) );
	}

	public function getHandlerClass() {
		return $this->m_strHandlerClass;
	}

	public function sqlHandlerClass() {
		return ( true == isset( $this->m_strHandlerClass ) ) ? '\'' . addslashes( $this->m_strHandlerClass ) . '\'' : 'NULL';
	}

	public function setMessageClass( $strMessageClass ) {
		$this->set( 'm_strMessageClass', CStrings::strTrimDef( $strMessageClass, 240, NULL, true ) );
	}

	public function getMessageClass() {
		return $this->m_strMessageClass;
	}

	public function sqlMessageClass() {
		return ( true == isset( $this->m_strMessageClass ) ) ? '\'' . addslashes( $this->m_strMessageClass ) . '\'' : 'NULL';
	}

	public function setMessageBody( $strMessageBody ) {
		$this->set( 'm_strMessageBody', CStrings::strTrimDef( $strMessageBody, -1, NULL, true ) );
	}

	public function getMessageBody() {
		return $this->m_strMessageBody;
	}

	public function sqlMessageBody() {
		return ( true == isset( $this->m_strMessageBody ) ) ? '\'' . addslashes( $this->m_strMessageBody ) . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	/* this message is overridden in custom class, please update the definition if needed when committing new base class */
	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, correlation_id, cid, company_user_id, status_ttl, status, handler_class, message_class, message_body, log_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCorrelationId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlStatusTtl() . ', ' .
 						$this->sqlStatus() . ', ' .
 						$this->sqlHandlerClass() . ', ' .
 						$this->sqlMessageClass() . ', ' .
 						$this->sqlMessageBody() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' correlation_id = ' . $this->sqlCorrelationId() . ','; } elseif( true == array_key_exists( 'CorrelationId', $this->getChangedColumns() ) ) { $strSql .= ' correlation_id = ' . $this->sqlCorrelationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_ttl = ' . $this->sqlStatusTtl() . ','; } elseif( true == array_key_exists( 'StatusTtl', $this->getChangedColumns() ) ) { $strSql .= ' status_ttl = ' . $this->sqlStatusTtl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' handler_class = ' . $this->sqlHandlerClass() . ','; } elseif( true == array_key_exists( 'HandlerClass', $this->getChangedColumns() ) ) { $strSql .= ' handler_class = ' . $this->sqlHandlerClass() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_class = ' . $this->sqlMessageClass() . ','; } elseif( true == array_key_exists( 'MessageClass', $this->getChangedColumns() ) ) { $strSql .= ' message_class = ' . $this->sqlMessageClass() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_body = ' . $this->sqlMessageBody() . ','; } elseif( true == array_key_exists( 'MessageBody', $this->getChangedColumns() ) ) { $strSql .= ' message_body = ' . $this->sqlMessageBody() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'correlation_id' => $this->getCorrelationId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'status_ttl' => $this->getStatusTtl(),
			'status' => $this->getStatus(),
			'handler_class' => $this->getHandlerClass(),
			'message_class' => $this->getMessageClass(),
			'message_body' => $this->getMessageBody(),
			'log_datetime' => $this->getLogDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>