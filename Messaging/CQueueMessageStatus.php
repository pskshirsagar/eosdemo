<?php

class CQueueMessageStatus extends CBaseQueueMessageStatus {

	public function valCorrelationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatusTtl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHandlerClass() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageClass() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageBody() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		// We need to handle the message body before the parent method, so that escape sequences are not removed from the JSON
		if( isset( $arrmixValues['message_body'] ) ) {
			if( true == $boolDirectSet ) {
				$this->m_strMessageBody = trim( $arrmixValues['message_body'] );
			} else {
				$this->setMessageBody( $arrmixValues['message_body'] );
			}
			unset( $arrmixValues['message_body'] );
		}

		return parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	public function applyStatusMessage( \Psi\Libraries\Queue\Message\CMessageStatusMessage $objStatusMessage ) {
		$objOriginalMessage = $objStatusMessage->getMessage();

		$this->setCorrelationId( $objStatusMessage->getCorrelationId() );
		$this->setStatusTtl( $objOriginalMessage->getStatusTtl() );
		$this->setStatus( $objStatusMessage->getStatus() );
		$this->setHandlerClass( $objOriginalMessage->getHandlerClass() );
		$this->setMessageClass( get_class( $objOriginalMessage ) );
		$this->setMessageBody( $objOriginalMessage->__toString() );
		$this->setLogDatetime( $objOriginalMessage->getLogDatetime()->format( 'c' ) );

		if( true == method_exists( $objOriginalMessage, 'getCid' ) ) {
			$this->setCid( $objOriginalMessage->getCid() );
		}

		if( true == method_exists( $objOriginalMessage, 'getCompanyUserId' ) ) {
			$this->setCompanyUserId( $objOriginalMessage->getCompanyUserId() );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = '
			INSERT INTO
				' . static::TABLE_NAME . '
				( id, correlation_id, cid, company_user_id, status_ttl, status, handler_class, message_class, message_body, log_datetime, updated_by, updated_on, created_by, created_on )
			VALUES (
				' . $strId . ',
				' . $this->sqlCorrelationId() . ',
				' . $this->sqlCid() . ',
				' . $this->sqlCompanyUserId() . ',
				' . $this->sqlStatusTtl() . ',
				' . $this->sqlStatus() . ',
				' . $this->sqlHandlerClass() . ',
				' . $this->sqlMessageClass() . ',
				' . $this->sqlMessageBody() . ',
				' . $this->sqlLogDatetime() . ',
				' . ( int ) $intCurrentUserId . ',
				' . $this->sqlUpdatedOn() . ',
				' . ( int ) $intCurrentUserId . ',
				' . $this->sqlCreatedOn() . '
			)
			ON CONFLICT ( correlation_id ) DO UPDATE
			SET
				status = ' . $this->sqlStatus() . ',
				message_body = ' . $this->sqlMessageBody() . ',
				log_datetime = ' . $this->sqlLogDatetime() . ',
				updated_by = ' . ( int ) $intCurrentUserId . ',
				updated_on = NOW()
			RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return executeSql( $strSql, $objDatabase );
		}
	}
}