<?php

class CIvrHoliday extends CBaseIvrHoliday {

	public function valTitle() {
		$boolIsValid = true;

		if( false == valStr( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Holiday title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHolidayDate( $objVoipDatabase ) {
		$boolIsValid	= true;
		$strWhere		= ' WHERE ivr_id = ' . ( int ) $this->getIvrId() . ' AND holiday_date = \'' . $this->getHolidayDate() . '\'';

		if( true == valId( $this->getId() ) ) {
			$strWhere .= ' AND id <> ' . ( int ) $this->getId();
		}

		switch( NULL ) {
			default:
				if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, 'Voip', 'VOIP service is currently unavailable, please try after some time.' ) );
					break;
				}

				if( false == valStr( $this->getHolidayDate() ) && false == CValidation::validateDate( $this->getHolidayDate() ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'holiday_date', 'Select valid holiday date.' ) );
					break;
				}

				$intIvrHolidayCount = CIvrHolidays::fetchIvrHolidayCount( $strWhere, $objVoipDatabase );

				if( true == valId( $intIvrHolidayCount ) ) {
					$boolIsValid &= false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'holiday_date', 'Duplicate holiday date has been assigned.' ) );
				}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case 'validate_ivr_holiday':
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valHolidayDate( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getMonthDay() {
		return date( 'd', strtotime( $this->getHolidayDate() ) );
	}

	public function getMonth() {
		return date( 'n', strtotime( $this->getHolidayDate() ) );
	}

	public function getYear() {
		return date( 'Y', strtotime( $this->getHolidayDate() ) );
	}
}
?>