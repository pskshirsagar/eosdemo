<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallResults
 * Do not add any new functions to this class.
 */

class COutboundCallResults extends CBaseOutboundCallResults {

	public static function fetchOutboundCallResultsByOutboundCallIds( $arrintOutboundCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallIds ) ) return NULL;

		$strSql = 'SELECT
						ocr.*,
						cr.name AS outbound_call_result_name
					FROM
						outbound_call_results ocr
						LEFT JOIN call_results cr ON ( ocr.call_result_id = cr.id )
					WHERE
						ocr.outbound_call_id IN ( ' . implode( ',', $arrintOutboundCallIds ) . ' )';

		return parent::fetchOutboundCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallResultByOutboundCallId( $intOutboundCallId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallId ) ) return NULL;

		$strSql = 'SELECT
						ocr.*,
						cr.name AS outbound_call_result_name
					FROM
						outbound_call_results ocr
						LEFT JOIN call_results cr ON ( ocr.call_result_id = cr.id )
					WHERE
						ocr.outbound_call_id = ' . ( int ) $intOutboundCallId;

		return parent::fetchOutboundCallResult( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsResultsCountByOutboundCallCampaignId( $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						COUNT( ocr.outbound_call_id )
					FROM
						outbound_call_results ocr
						JOIN outbound_calls oc ON( ocr.outbound_call_id = oc.id )
					WHERE
						oc.outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsResultsCountGroupByCallResultsByOutboundCallCampaignId( $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						cr.name,
						cr.ps_product_id,
						ocr.call_result_id,
						COUNT( ocr.outbound_call_id )
					FROM
						outbound_call_results ocr
						JOIN outbound_calls oc ON( ocr.outbound_call_id = oc.id )
						JOIN call_results cr ON( ocr.call_result_id = cr.id )
					WHERE
						oc.outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
					GROUP BY
						ocr.call_result_id,
						cr.name,
						cr.ps_product_id
					ORDER BY
						COUNT( ocr.outbound_call_id ) DESC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsResultsCountForOutboundCallCampaignsByOutboundCallCampaignIds( $arrintOutboundCallCampaignIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallCampaignIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( ocr.outbound_call_id ),
						occ.id AS outbound_call_campaign_id
					FROM
						outbound_call_results ocr
						JOIN outbound_calls oc ON( ocr.outbound_call_id = oc.id )
						JOIN outbound_call_campaigns occ ON( oc.outbound_call_campaign_id = occ.id )
					WHERE
						occ.id IN( ' . implode( ',', $arrintOutboundCallCampaignIds ) . ' )
					GROUP BY
						occ.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>