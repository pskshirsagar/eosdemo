<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenus
 * Do not add any new functions to this class.
 */

class CIvrMenus extends CBaseIvrMenus {

	public static function fetchIvrMenuByIdByCid( $intId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT * FROM ivr_menus WHERE id = ' . ( int ) $intId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchIvrMenu( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleIvrMenusByCidByIvrId( $intCid, $intIvrId, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intIvrId ) ) return NULL;

		$strSql = 'SELECT
						im.id,
						im.ivr_menu_type_id,
						im.ivr_menu_id,
						im.name,
						ima.id AS ivr_menu_action_id,
						ima.ivr_menu_id AS ivr_menu_action_ivr_menu_id,
						ima.sub_ivr_menu_id,
						ima.ivr_action_id,
						ima.greeting_id,
						ima.action_key,
						ima.name AS ivr_menu_action_name,
						ima.action_parameter,
						cf.id AS call_file_id
					FROM
						ivr_menus AS im
						JOIN ivr_menu_actions AS ima ON ( ( im.cid = ima.cid ) AND ( im.id = ima.ivr_menu_id ) )
						JOIN greetings AS g ON ( ima.cid = g.cid AND ima.greeting_id = g.id )
						JOIN call_files AS cf ON ( g.call_file_id = cf.id )
					WHERE
						im.cid = ' . ( int ) $intCid . '
						AND im.ivr_id = ' . ( int ) $intIvrId . '
					ORDER BY
						ima.action_key,im.ivr_menu_type_id ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchIvrMenusByIdsByCid( $arrintIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT * FROM ivr_menus WHERE id IN ( ' . implode( ',', $arrintIds ) . ' ) AND cid = ' . ( int ) $intCid;

		return self::fetchIvrMenus( $strSql, $objVoipDatabase );
	}

	public static function fetchIvrMenusByIvrIdByCid( $intIvrId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intIvrId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT * FROM ivr_menus WHERE ivr_id = ' . ( int ) $intIvrId . ' AND cid = ' . ( int ) $intCid;

		return self::fetchIvrMenus( $strSql, $objVoipDatabase );
	}

	public static function fetchIvrMenusByIvrIdByIvrMenuTypeIdByCidByIsOverrideAfterOfficeHours( $intIvrId, $intIvrMenuTypeId, $intCid, $boolIsOverrideAfterOfficeHours, $objVoipDatabase ) {
		if( true == $boolIsOverrideAfterOfficeHours ) {
			$intIvrMenuTypeId = CIvrMenuType::DURING_OFFICE_HOURS;
		}

		$strSql = 'SELECT *FROM ivr_menus WHERE ivr_id = ' . ( int ) $intIvrId . ' AND ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchIvrMenus( $strSql, $objVoipDatabase );
	}
}
?>