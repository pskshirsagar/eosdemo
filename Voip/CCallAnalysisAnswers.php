<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisAnswers
 * Do not add any new functions to this class.
 */

class CCallAnalysisAnswers extends CBaseCallAnalysisAnswers {

	public static function fetchCustomPossiblePointsByCallAnalysisCategoryIds( $arrintCallAnalysisCategoryIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAnalysisCategoryIds ) ) return NULL;

		$strSql = 'WITH question_possible_points AS ( 
						SELECT 
							caa.call_analysis_question_id, 
							caq.call_analysis_category_id,
							max(caa.points) AS points
						FROM 
							call_analysis_answers caa
							JOIN call_analysis_questions caq ON caq.id = caa.call_analysis_question_id
						WHERE 
							caq.call_analysis_category_id IN ( ' . implode( ',', $arrintCallAnalysisCategoryIds ) . ' )
							AND caa.deleted_on IS NULL
							AND caq.deleted_on IS NULL
						GROUP BY 
							caa.call_analysis_question_id, caq.call_analysis_category_id
					)
					SELECT 
						call_analysis_category_id,
						SUM(points) AS possible_points,
						count(call_analysis_question_id) AS question_counts
					FROM 
						question_possible_points
					GROUP BY 
						call_analysis_category_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisAbswersByIds( $arrintCallAnalysisAnswerIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAnalysisAnswerIds ) ) return NULL;

		$strSql = 'SELECT
						* 
					FROM 
						call_analysis_answers 
					WHERE 
						deleted_on IS NULL 
						AND id IN ( ' . implode( ',', $arrintCallAnalysisAnswerIds ) . ' ) 
					ORDER BY 
						order_num';

		return self::fetchCallAnalysisAnswers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisAnswersByCallAnalysisQuestionIds( $arrintCallAnalysisQuestionIds, $objVoipDatabase, $boolIsDeleted = true ) {
		if( false == valArr( $arrintCallAnalysisQuestionIds ) ) return false;

		$strSql = 'SELECT 
						* 
					FROM 
						' . CCallAnalysisAnswer::TABLE_NAME . ' 
					WHERE 
						call_analysis_question_id IN ( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
		if( false == $boolIsDeleted ) {
			$strSql .= ' AND deleted_on IS NULL';
		}
		$strSql .= ' ORDER BY order_num ASC';

		return self::fetchCallAnalysisAnswers( $strSql, $objVoipDatabase );
	}

}
?>