<?php

class CCallAnalysisCallDispute extends CBaseCallAnalysisCallDispute {

	protected $m_strCompanyName;
	protected $m_strAnalyzedByName;

	/**
	 * set functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )            $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['analyzed_by_name'] ) )          $this->setAnalyzedByName( $arrmixValues['analyzed_by_name'] );
		return true;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setAnalyzedByName( $strAnalyzedByName ) {
		$this->m_strAnalyzedByName = $strAnalyzedByName;
	}

	/**
	 * get functions
	 *
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getAnalyzedByName() {
		return $this->m_strAnalyzedByName;
	}

}
?>