<?php

class CCallDownloadLog extends CBaseCallDownloadLog {

	/**
	 * Validate Functions
	 *
	 */

	use Psi\Libraries\EosFoundation\TEosStoredObject;
	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getTitle() {
		return $this->m_strFileName;
	}

	/**
	 * Other Functions
	 *
	 */

	public function downloadCallFile( $strFullFileName, $objStorageGateway, $objDatabase ) {

		if( true == $this->downloadCallFileFromOSG( $objStorageGateway, $objDatabase ) ){
			return;
		}

		if( false == CFileIo::fileExists( $strFullFileName ) ) return;

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-Type: application/x-msexcel' );
		header( 'Content-Disposition: attachment; filename=' . $this->getFileName() );
		header( 'Content-Transfer-Encoding:binary' );

		echo file_get_contents( $strFullFileName );
		return;
	}

	public function downloadCallFileFromOSG( $objStorageGateway, $objDatabase ){
		$objStoredObject = $this->fetchStoredObject( $objDatabase );

		if( false == valObj( $objStoredObject, 'CStoredObject' ) ) {
			return false;
		}

		$objObjectStorageGatewayResponse = $objStorageGateway->getObject( $objStoredObject->createGatewayRequest() );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			return false;
		}

		header( 'Content-type: application/x-msexcel' );
		header( 'Content-Disposition: inline; filename="' . $this->getFileName() . '"' );
		header( 'Content-Transfer-Encoding: binary' );
		header( 'Accept-Ranges: bytes' );
		echo $objObjectStorageGatewayResponse['data'];
		return true;
	}

	public function calcStorageKey() {
		if( CClient::ID_DEFAULT == $this->getCid() ) {
			$strKey			= 'call_logs/'. CClient::ID_DEFAULT . '/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getFileName();
		}else{
			$strKey			= 'call_tracker_logs/'. $this->getCid() . '/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getFileName();
		}
		return $strKey;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CONFIG_OSG_BUCKET_VOIP;
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Voip\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}
?>