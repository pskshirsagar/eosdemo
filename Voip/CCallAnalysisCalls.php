<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCalls
 * Do not add any new functions to this class.
 */

class CCallAnalysisCalls extends CBaseCallAnalysisCalls {

	public static function fetchCallAnalysisCallByCallId( $intCallId, $objVoipDatabase ) {
		if( false == valId( $intCallId ) ) return false;
		$strSql = 'SELECT 
							* ,
							details->>\'analyzed_by\' AS analyzed_by,
							details->>\'analyzed_on\' AS analyzed_on,
							details->>\'call_type_id\' AS call_type_id,
							details->>\'call_datetime\' AS call_datetime,
							details->>\'property_name\' AS property_name,
							details->>\'call_result_id\' AS call_result_id,
							details->>\'lead_source_id\' AS lead_source_id,
							details->>\'call_duration_seconds\' AS call_duration_seconds,
							details->>\'lead_probability\' AS lead_probability
					FROM 
						call_analysis_calls 
					WHERE 
						call_id = ' . ( int ) $intCallId;
		return self::fetchCallAnalysisCall( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallAnalysisCallsByCallFilter( $objPagination, $objCallFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$arrstrWhere = [];

		if( true == valId( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = ' cac.call_id = ' . ( int ) $objCallFilter->getCallId();
		} elseif( true == valStr( $objCallFilter->getCallIds() ) ) {
			$arrstrWhere[] = 'cac.call_id IN ( ' . $objCallFilter->getCallIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$arrstrWhere[] = ' cac.cid IN ( ' . $objCallFilter->getCids() . ' ) ';
		} elseif( ( true == valId( $objCallFilter->getCid() ) ) ) {
			$arrstrWhere[] = ' cac.cid = ' . ( int ) $objCallFilter->getCid();
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'cac.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' ) ';
		}

		if( true == valId( $objCallFilter->getCallResultIds() ) ) {
			$arrstrWhere[] = 'CAST( cac.details->>\'call_result_id\' AS INTEGER ) IN ( ' . $objCallFilter->getCallResultIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCompanyEmployeeIds() ) ) {
			$arrstrWhere[] = ' cac.company_employee_id IN ( ' . $objCallFilter->getCompanyEmployeeIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCallAnalysisStatusTypes() ) ) {
			$strCallAnalysisStatusTypeIdsClause = ' ( cac.call_analysis_status_type_id IN ( ' . $objCallFilter->getCallAnalysisStatusTypes() . ' ) ';

			if( true == in_array( CCallAnalysisStatusType::UNEVALUATED, $objCallFilter->getCallAnalysisStatusTypesArray() ) ) {
				$strCallAnalysisStatusTypeIdsClause .= ' OR cac.call_analysis_status_type_id IS NULL )';
			} else {
				$strCallAnalysisStatusTypeIdsClause .= ' )';
			}

			$arrstrWhere[] = $strCallAnalysisStatusTypeIdsClause;
		}

		$arrstrOrderBy = [
			'call_id'			=> 'cac.call_id',
			'call_type'			=> 'cac.call_type_id',
			'status'			=> 'cac.call_status_type_id',
			'property'			=> 'cac.details->>\'property_name\'',
			'duration'			=> 'cac.call_duration_seconds',
			'time_to_listen'	=> 'cac.details->>\'call_datetime\'',
			'call_datetime'		=> 'cac.details->>\'call_datetime\'',
			'lead_probability'	=> 'cac.details->>\'lead_probability\''
		];

		if( false == is_null( $objCallFilter->getStartDate() ) && false == is_null( $objCallFilter->getEndDate() ) ) {
			$strStartDate	= $objCallFilter->getStartDate();
			$strEndDate		= $objCallFilter->getEndDate();

			if( 'Min' != $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'cac.details->>\'call_datetime\' BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'' . ' AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59\'';
			} elseif( 'Min' == $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'cac.details->>\'call_datetime\' <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59\'';
			} elseif( 'Min' != $strStartDate && 'Max' == $strEndDate ) {
				$arrstrWhere[] = 'cac.details->>\'call_datetime\' >= \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'';
			}
		}

		if( false == $boolIsCountOnly ) {
			$strSql = ' SELECT
							cac.*,
							cac.details->>\'analyzed_by\' AS analyzed_by,
							cac.details->>\'analyzed_on\' AS analyzed_on,
							cac.details->>\'call_type_id\' AS call_type_id,
							cac.details->>\'call_datetime\' AS call_datetime,
							cac.details->>\'property_name\' AS property_name,
							cac.details->>\'company_name\' AS company_name,
							cac.details->>\'call_result_id\' AS call_result_id,
							cac.details->>\'lead_source_id\' AS lead_source_id,
							cac.details->>\'call_duration_seconds\' AS call_duration_seconds,
							cast( cac.details->>\'lead_probability\' AS DOUBLE PRECISION ) * 100 AS lead_probability';
		} else {
			$strSql = ' SELECT COUNT( cac.id ) ';
		}

		$strSql .= ' FROM
						call_analysis_calls cac 
						LEFT JOIN call_analysis_call_sorts cacs ON ( cac.property_id = cacs.property_id AND cac.cid = cacs.cid ) LEFT JOIN clients cl ON ( cl.id = cac.cid )';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';

			if( false == valStr( $objCallFilter->getCids() ) ) {
				$strSql .= ' AND cl.company_status_type_id = ' . CCompanyStatusType:: CLIENT;
			}
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );

			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		} else {
			$strOrderBy			= ( true == isset( $arrstrOrderBy[$objCallFilter->getSortBy()] ) ? $arrstrOrderBy[$objCallFilter->getSortBy()] : 'cacs.delta DESC NULLS LAST, CAST ( cac.details->>\'lead_probability\' AS DOUBLE PRECISION ) ' );
			$strSortDirection	= ( false == is_null( $objCallFilter->getSortDirection() ) ) ? $objCallFilter->getSortDirection() : 'DESC NULLS LAST';

			if( false == is_null( $objCallFilter->getSortDirection() ) && 'lead_probability' == $objCallFilter->getSortBy() && 'DESC' == $objCallFilter->getSortDirection() ) {
				$strSortDirection = 'DESC NULLS LAST';
			}

			if( false == is_null( $strOrderBy ) ) {
				$strSql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;
				if( 'call_id' != $objCallFilter->getSortBy() ) {
					$strSql .= ' ,cac.call_id DESC ';
				}
			}

			if( false == $boolIsCountOnly && false == valId( $objCallFilter->getCallId() ) && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
				$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}
		return self::fetchCallAnalysisCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomUnanalyzedCallAnalysisCallsByDuration( $intDayDuration, $objVoipDatabase ) {
		$strSql = 'SELECT
							cac.id,
							cac.call_id,
							c.details
					FROM
						call_analysis_calls cac
						LEFT  JOIN calls c ON cac.call_id = c.id
					WHERE
						(cast( cac.details->>\'lead_probability\' AS DOUBLE PRECISION ) * 100) < 11
						OR cac.details->>\'lead_probability\' IS NULL
						OR (
							CAST ( cac.details->>\'call_datetime\' as timestamp ) < ( NOW() - INTERVAL \'' . $intDayDuration . '\' )
							AND (
								cac.call_analysis_status_type_id = ' . CCallAnalysisStatusType::UNEVALUATED . '
								OR cac.call_analysis_status_type_id IS NULL
							)
						) 
					ORDER BY cac.id';
		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>