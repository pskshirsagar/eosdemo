<?php

class CCallLog extends CBaseCallLog {

	const INSERT	= 'INSERT';
	const UPDATE	= 'UPDATE';
	const DELETE	= 'DELETE';

	const DATA_SEPERATOR	= '~^~';
	const KEY_VAL_SEPERATOR	= '::';

	protected $m_strCallAgentFullName;
	protected $m_strCallZoneName;
	protected $m_strCompanyName;
	protected $m_strPropertyName;

	/**
	 * set functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_agent_full_name'] ) )	$this->setCallAgentFullName( $arrmixValues['call_agent_full_name'] );
		if( true == isset( $arrmixValues['call_zone_name'] ) )			$this->setCallCallZoneName( $arrmixValues['call_zone_name'] );
		if( true == isset( $arrmixValues['company_name'] ) )			$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )			$this->setPropertyName( $arrmixValues['property_name'] );

		return;
	}

	public function setCallAgentFullName( $strCallAgentFullName ) {
		$this->m_strCallAgentFullName = $strCallAgentFullName;
	}

	public function setCallCallZoneName( $strCallZoneName ) {
		$this->m_strCallZoneName = $strCallZoneName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	/**
	 * get functions
	 *
	 */

	public function getCallAgentFullName() {
		return $this->m_strCallAgentFullName;
	}

	public function getCallZoneName() {
		return $this->m_strCallZoneName;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function updateCallLog( $strCurrentData, $strPreviousData, $strReferenceName, $intReferenceId, $strTableName, $strAction, $intUserId, $objVoipDatabase, $intCid = NULL, $intPropertyId = NULL ) {
		if( false == is_null( $intCid ) ) {
			$this->setCid( $intCid );
		}

		if( false == is_null( $intPropertyId ) ) {
			$this->setPropertyId( $intPropertyId );
		}

		$this->setCurrentData( $strCurrentData );
		$this->setPreviousData( $strPreviousData );
		$this->setCreatedBy( $intUserId );
		$this->setCreatedOn( 'now()' );
		$this->setTableName( $strTableName );
		$this->setReferenceId( $intReferenceId );
		$this->setReferenceName( $strReferenceName );
		$this->setAction( $strAction );

		if( false == $this->insert( $intUserId, $objVoipDatabase ) ) {
			return false;
		}
		return true;
	}

	public function parseCallLogData( $strCallLogData ) {
		if( false == valStr( $strCallLogData ) ) return NULL;

		$arrstrCallLogs			= explode( CCallLog::DATA_SEPERATOR, $strCallLogData );
		$arrstrParseCallLogData	= array();

		foreach( $arrstrCallLogs as $strCallLog ) {
			$arrstrCallLog = explode( CCallLog::KEY_VAL_SEPERATOR, $strCallLog );
			$arrstrParseCallLogData[$arrstrCallLog[0]] = $arrstrCallLog[1];
		}
		return $arrstrParseCallLogData;
	}

}
?>