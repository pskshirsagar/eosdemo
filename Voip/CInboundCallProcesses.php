<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCallProcesses
 * Do not add any new functions to this class.
 */

class CInboundCallProcesses extends CBaseInboundCallProcesses {

	/**
	 * Fetch Functions
	 */

	public static function fetchInboundCallProcessesByInboundCallIdsByCid( $arrintInboundCallIds, $intCid, $objVoipDatabase ) {

		if( false == valArr( $arrintInboundCallIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT icp.inbound_call_id, icp.id as id, icrt.name, icrt.description, icp.payment_activation_id, icp.process_datetime
					FROM inbound_calls ic,
						 inbound_call_processes icp,
						 inbound_call_response_types icrt
					WHERE ic.cid = ' . ( int ) $intCid . '
					AND ic.id IN ( ' . implode( ',', $arrintInboundCallIds ) . ' )
					AND ic.id = icp.inbound_call_id
					AND icp.inbound_call_response_type_id = icrt.id
					ORDER BY icp.id ASC';

		return self::fetchInboundCallProcesses( $strSql, $objVoipDatabase );
	}

	public static function fetchInboundCallProcessById( $intInboundCallProcessId, $objVoipDatabase ) {
		return self::fetchInboundCallProcess( 'SELECT *FROM inbound_call_processes WHERE id =' . ( int ) $intInboundCallProcessId, $objVoipDatabase );
	}

	public static function fetchInboundCallProcessesByInboundCallId( $intInboundCallId, $objVoipDatabase ) {
		return self::fetchInboundCallProcesses( 'SELECT *FROM inbound_call_processes WHERE inbound_call_id=' . ( int ) $intInboundCallId, $objVoipDatabase );
	}

}
?>