<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSources
 * Do not add any new functions to this class.
 */

class CCallSources extends CBaseCallSources {

	public static function fetchCallSources( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCallSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallSource( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCallSource::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCallSources( $objVoipDatabase ) {
		return self::fetchCallSources( 'SELECT * FROM call_sources ORDER BY name ASC', $objVoipDatabase );
	}

	public static function fetchCallSourcesByIds( $arrintCallSourceIds, $objVoipDatabase ) {
		return self::fetchCallSources( 'SELECT * FROM call_sources WHERE id IN ( ' . implode( ',', $arrintCallSourceIds ) . ') ORDER BY id ASC;', $objVoipDatabase );
	}

}
?>