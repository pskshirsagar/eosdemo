<?php

class CRewardPointSetting extends CBaseRewardPointSetting {

	const CALL_HANDLED_GREATER_THAN_60_SECONDS	= 1;
	const EVERY_ONE_HOUR_OF_AVAILABLE_TIME		= 2;
	const GRADED_CALL_GREATER_THAN_93_PERCENT	= 3;
	const GRADED_CALL_GREATER_THAN_95_PERCENT	= 4;
	const GRADED_CALL_100_PERCENT				= 5;
	const LEASE_GENERATED						= 6;
	const COACHING_GOAL_ACHIEVED				= 7;
	const RECRUITING_BONUS						= 8;
	const COACHING_MEETING_COMPLETION			= 9;
	const PERFECT_ATTENDANCE					= 10;
	const ADHERENCE_GREATER_THAN_93_PERCENT		= 11;
	const ADHERENCE_GREATER_THAN_95_PERCENT		= 12;
	const WRAP_UP_LESS_THAN_45_SECONDS			= 13;
	const WRAP_UP_LESS_THAN_30_SECONDS			= 14;
	const WORKED_GREATER_THAN_35_HOURS			= 15;
	const JONESES_CLUB							= 16;
	const SUPER_JONESES_CLUB					= 17;
	const MONTHLY_CHALLENGE						= 18;
	const JONESES_CLUB_TURKEY					= 19;
	const SUPER_JONESES_CLUB_TURKEY				= 20;
	const SIX_MONTH_ANNIVERSARY					= 21;
	const ANNUAL_ANNIVERSARY					= 22;
	const CUSTOM_MANUAL_ACHIEVEMENT				= 23;
	const CALL_HANDLED_DURATION					= 60;
	const GRADED_CALL_MIN_VALUE					= 93;
	const GRADED_CALL_MAX_VALUE					= 95;
	const GRADED_CALL_100						= 100;
	const ADHERENCE_MIN_VALUE					= 93;
	const ADHERENCE_MAX_VALUE					= 95;
	const WRAP_UP_MIN_VALUE						= 30;
	const WRAP_UP_MAX_VALUE						= 45;

	public static $c_arrintRewardPointSettingJonesesClubIds = [
		self::JONESES_CLUB,
		self::SUPER_JONESES_CLUB,
		self::JONESES_CLUB_TURKEY,
		self::SUPER_JONESES_CLUB_TURKEY
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRewardPointTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRewardPointCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getValue() ) || 0 >= $this->getValue() || false == valStr( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Please enter valid value.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_reward_point_setting':
				$boolIsValid &= $this->valValue();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>