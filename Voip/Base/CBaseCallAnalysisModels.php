<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisModels
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisModels extends CEosPluralBase {

	/**
	 * @return CCallAnalysisModel[]
	 */
	public static function fetchCallAnalysisModels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisModel::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisModel
	 */
	public static function fetchCallAnalysisModel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisModel::class, $objDatabase );
	}

	public static function fetchCallAnalysisModelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_models', $objDatabase );
	}

	public static function fetchCallAnalysisModelById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisModel( sprintf( 'SELECT * FROM call_analysis_models WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisModelsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisModels( sprintf( 'SELECT * FROM call_analysis_models WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisModelsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisModels( sprintf( 'SELECT * FROM call_analysis_models WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

}
?>