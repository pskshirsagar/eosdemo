<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallBlocks
 * Do not add any new functions to this class.
 */

class CBaseCallBlocks extends CEosPluralBase {

	/**
	 * @return CCallBlock[]
	 */
	public static function fetchCallBlocks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallBlock::class, $objDatabase );
	}

	/**
	 * @return CCallBlock
	 */
	public static function fetchCallBlock( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallBlock::class, $objDatabase );
	}

	public static function fetchCallBlockCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_blocks', $objDatabase );
	}

	public static function fetchCallBlockById( $intId, $objDatabase ) {
		return self::fetchCallBlock( sprintf( 'SELECT * FROM call_blocks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallBlocksByCid( $intCid, $objDatabase ) {
		return self::fetchCallBlocks( sprintf( 'SELECT * FROM call_blocks WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallBlocksByScheduledCallId( $intScheduledCallId, $objDatabase ) {
		return self::fetchCallBlocks( sprintf( 'SELECT * FROM call_blocks WHERE scheduled_call_id = %d', $intScheduledCallId ), $objDatabase );
	}

	public static function fetchCallBlocksByScheduledCallTypeId( $intScheduledCallTypeId, $objDatabase ) {
		return self::fetchCallBlocks( sprintf( 'SELECT * FROM call_blocks WHERE scheduled_call_type_id = %d', $intScheduledCallTypeId ), $objDatabase );
	}

}
?>