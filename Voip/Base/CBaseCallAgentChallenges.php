<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentChallenges
 * Do not add any new functions to this class.
 */

class CBaseCallAgentChallenges extends CEosPluralBase {

	/**
	 * @return CCallAgentChallenge[]
	 */
	public static function fetchCallAgentChallenges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentChallenge::class, $objDatabase );
	}

	/**
	 * @return CCallAgentChallenge
	 */
	public static function fetchCallAgentChallenge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentChallenge::class, $objDatabase );
	}

	public static function fetchCallAgentChallengeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_challenges', $objDatabase );
	}

	public static function fetchCallAgentChallengeById( $intId, $objDatabase ) {
		return self::fetchCallAgentChallenge( sprintf( 'SELECT * FROM call_agent_challenges WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentChallengesByCallAgentAchievementTypeId( $intCallAgentAchievementTypeId, $objDatabase ) {
		return self::fetchCallAgentChallenges( sprintf( 'SELECT * FROM call_agent_challenges WHERE call_agent_achievement_type_id = %d', $intCallAgentAchievementTypeId ), $objDatabase );
	}

	public static function fetchCallAgentChallengesByCallAgentChallengeScheduleId( $intCallAgentChallengeScheduleId, $objDatabase ) {
		return self::fetchCallAgentChallenges( sprintf( 'SELECT * FROM call_agent_challenges WHERE call_agent_challenge_schedule_id = %d', $intCallAgentChallengeScheduleId ), $objDatabase );
	}

	public static function fetchCallAgentChallengesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchCallAgentChallenges( sprintf( 'SELECT * FROM call_agent_challenges WHERE team_id = %d', $intTeamId ), $objDatabase );
	}

}
?>