<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCallResponseTypes
 * Do not add any new functions to this class.
 */

class CBaseInboundCallResponseTypes extends CEosPluralBase {

	/**
	 * @return CInboundCallResponseType[]
	 */
	public static function fetchInboundCallResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInboundCallResponseType::class, $objDatabase );
	}

	/**
	 * @return CInboundCallResponseType
	 */
	public static function fetchInboundCallResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInboundCallResponseType::class, $objDatabase );
	}

	public static function fetchInboundCallResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inbound_call_response_types', $objDatabase );
	}

	public static function fetchInboundCallResponseTypeById( $intId, $objDatabase ) {
		return self::fetchInboundCallResponseType( sprintf( 'SELECT * FROM inbound_call_response_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>