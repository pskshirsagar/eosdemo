<?php

class CBaseCallAgentScheduleDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_schedule_details';

	protected $m_intId;
	protected $m_intCallAgentScheduleId;
	protected $m_intCallAgentId;
	protected $m_intCallAgentStatusTypeId;
	protected $m_strIntervalStartTimestamp;
	protected $m_intIntervalDayOfWeek;
	protected $m_intIntervalStartEpochDelta;
	protected $m_intIntervalSequence;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentScheduleId', trim( $arrValues['call_agent_schedule_id'] ) ); elseif( isset( $arrValues['call_agent_schedule_id'] ) ) $this->setCallAgentScheduleId( $arrValues['call_agent_schedule_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_agent_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentStatusTypeId', trim( $arrValues['call_agent_status_type_id'] ) ); elseif( isset( $arrValues['call_agent_status_type_id'] ) ) $this->setCallAgentStatusTypeId( $arrValues['call_agent_status_type_id'] );
		if( isset( $arrValues['interval_start_timestamp'] ) && $boolDirectSet ) $this->set( 'm_strIntervalStartTimestamp', trim( $arrValues['interval_start_timestamp'] ) ); elseif( isset( $arrValues['interval_start_timestamp'] ) ) $this->setIntervalStartTimestamp( $arrValues['interval_start_timestamp'] );
		if( isset( $arrValues['interval_day_of_week'] ) && $boolDirectSet ) $this->set( 'm_intIntervalDayOfWeek', trim( $arrValues['interval_day_of_week'] ) ); elseif( isset( $arrValues['interval_day_of_week'] ) ) $this->setIntervalDayOfWeek( $arrValues['interval_day_of_week'] );
		if( isset( $arrValues['interval_start_epoch_delta'] ) && $boolDirectSet ) $this->set( 'm_intIntervalStartEpochDelta', trim( $arrValues['interval_start_epoch_delta'] ) ); elseif( isset( $arrValues['interval_start_epoch_delta'] ) ) $this->setIntervalStartEpochDelta( $arrValues['interval_start_epoch_delta'] );
		if( isset( $arrValues['interval_sequence'] ) && $boolDirectSet ) $this->set( 'm_intIntervalSequence', trim( $arrValues['interval_sequence'] ) ); elseif( isset( $arrValues['interval_sequence'] ) ) $this->setIntervalSequence( $arrValues['interval_sequence'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentScheduleId( $intCallAgentScheduleId ) {
		$this->set( 'm_intCallAgentScheduleId', CStrings::strToIntDef( $intCallAgentScheduleId, NULL, false ) );
	}

	public function getCallAgentScheduleId() {
		return $this->m_intCallAgentScheduleId;
	}

	public function sqlCallAgentScheduleId() {
		return ( true == isset( $this->m_intCallAgentScheduleId ) ) ? ( string ) $this->m_intCallAgentScheduleId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallAgentStatusTypeId( $intCallAgentStatusTypeId ) {
		$this->set( 'm_intCallAgentStatusTypeId', CStrings::strToIntDef( $intCallAgentStatusTypeId, NULL, false ) );
	}

	public function getCallAgentStatusTypeId() {
		return $this->m_intCallAgentStatusTypeId;
	}

	public function sqlCallAgentStatusTypeId() {
		return ( true == isset( $this->m_intCallAgentStatusTypeId ) ) ? ( string ) $this->m_intCallAgentStatusTypeId : 'NULL';
	}

	public function setIntervalStartTimestamp( $strIntervalStartTimestamp ) {
		$this->set( 'm_strIntervalStartTimestamp', CStrings::strTrimDef( $strIntervalStartTimestamp, -1, NULL, true ) );
	}

	public function getIntervalStartTimestamp() {
		return $this->m_strIntervalStartTimestamp;
	}

	public function sqlIntervalStartTimestamp() {
		return ( true == isset( $this->m_strIntervalStartTimestamp ) ) ? '\'' . $this->m_strIntervalStartTimestamp . '\'' : 'NOW()';
	}

	public function setIntervalDayOfWeek( $intIntervalDayOfWeek ) {
		$this->set( 'm_intIntervalDayOfWeek', CStrings::strToIntDef( $intIntervalDayOfWeek, NULL, false ) );
	}

	public function getIntervalDayOfWeek() {
		return $this->m_intIntervalDayOfWeek;
	}

	public function sqlIntervalDayOfWeek() {
		return ( true == isset( $this->m_intIntervalDayOfWeek ) ) ? ( string ) $this->m_intIntervalDayOfWeek : 'NULL';
	}

	public function setIntervalStartEpochDelta( $intIntervalStartEpochDelta ) {
		$this->set( 'm_intIntervalStartEpochDelta', CStrings::strToIntDef( $intIntervalStartEpochDelta, NULL, false ) );
	}

	public function getIntervalStartEpochDelta() {
		return $this->m_intIntervalStartEpochDelta;
	}

	public function sqlIntervalStartEpochDelta() {
		return ( true == isset( $this->m_intIntervalStartEpochDelta ) ) ? ( string ) $this->m_intIntervalStartEpochDelta : 'NULL';
	}

	public function setIntervalSequence( $intIntervalSequence ) {
		$this->set( 'm_intIntervalSequence', CStrings::strToIntDef( $intIntervalSequence, NULL, false ) );
	}

	public function getIntervalSequence() {
		return $this->m_intIntervalSequence;
	}

	public function sqlIntervalSequence() {
		return ( true == isset( $this->m_intIntervalSequence ) ) ? ( string ) $this->m_intIntervalSequence : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_schedule_id, call_agent_id, call_agent_status_type_id, interval_start_timestamp, interval_day_of_week, interval_start_epoch_delta, interval_sequence, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentScheduleId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallAgentStatusTypeId() . ', ' .
 						$this->sqlIntervalStartTimestamp() . ', ' .
 						$this->sqlIntervalDayOfWeek() . ', ' .
 						$this->sqlIntervalStartEpochDelta() . ', ' .
 						$this->sqlIntervalSequence() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_schedule_id = ' . $this->sqlCallAgentScheduleId() . ','; } elseif( true == array_key_exists( 'CallAgentScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_schedule_id = ' . $this->sqlCallAgentScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId() . ','; } elseif( true == array_key_exists( 'CallAgentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_start_timestamp = ' . $this->sqlIntervalStartTimestamp() . ','; } elseif( true == array_key_exists( 'IntervalStartTimestamp', $this->getChangedColumns() ) ) { $strSql .= ' interval_start_timestamp = ' . $this->sqlIntervalStartTimestamp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_day_of_week = ' . $this->sqlIntervalDayOfWeek() . ','; } elseif( true == array_key_exists( 'IntervalDayOfWeek', $this->getChangedColumns() ) ) { $strSql .= ' interval_day_of_week = ' . $this->sqlIntervalDayOfWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_start_epoch_delta = ' . $this->sqlIntervalStartEpochDelta() . ','; } elseif( true == array_key_exists( 'IntervalStartEpochDelta', $this->getChangedColumns() ) ) { $strSql .= ' interval_start_epoch_delta = ' . $this->sqlIntervalStartEpochDelta() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_sequence = ' . $this->sqlIntervalSequence() . ','; } elseif( true == array_key_exists( 'IntervalSequence', $this->getChangedColumns() ) ) { $strSql .= ' interval_sequence = ' . $this->sqlIntervalSequence() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_schedule_id' => $this->getCallAgentScheduleId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_agent_status_type_id' => $this->getCallAgentStatusTypeId(),
			'interval_start_timestamp' => $this->getIntervalStartTimestamp(),
			'interval_day_of_week' => $this->getIntervalDayOfWeek(),
			'interval_start_epoch_delta' => $this->getIntervalStartEpochDelta(),
			'interval_sequence' => $this->getIntervalSequence(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>