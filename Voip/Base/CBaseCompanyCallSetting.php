<?php

class CBaseCompanyCallSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.company_call_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCallCenterBaseScore;
	protected $m_intDefaultCallQueueId;
	protected $m_boolIsGooglePpcActivated;
	protected $m_boolIsAllowCallTranscription;
	protected $m_boolIsActivateCorporateCareLine;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intAgreementServiceLevel;
	protected $m_intCallAnalysisServiceLevel;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsGooglePpcActivated = false;
		$this->m_boolIsAllowCallTranscription = false;
		$this->m_boolIsActivateCorporateCareLine = false;
		$this->m_intAgreementServiceLevel = '0';
		$this->m_intCallAnalysisServiceLevel = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['call_center_base_score'] ) && $boolDirectSet ) $this->set( 'm_intCallCenterBaseScore', trim( $arrValues['call_center_base_score'] ) ); elseif( isset( $arrValues['call_center_base_score'] ) ) $this->setCallCenterBaseScore( $arrValues['call_center_base_score'] );
		if( isset( $arrValues['default_call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultCallQueueId', trim( $arrValues['default_call_queue_id'] ) ); elseif( isset( $arrValues['default_call_queue_id'] ) ) $this->setDefaultCallQueueId( $arrValues['default_call_queue_id'] );
		if( isset( $arrValues['is_google_ppc_activated'] ) && $boolDirectSet ) $this->set( 'm_boolIsGooglePpcActivated', trim( stripcslashes( $arrValues['is_google_ppc_activated'] ) ) ); elseif( isset( $arrValues['is_google_ppc_activated'] ) ) $this->setIsGooglePpcActivated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_google_ppc_activated'] ) : $arrValues['is_google_ppc_activated'] );
		if( isset( $arrValues['is_allow_call_transcription'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowCallTranscription', trim( stripcslashes( $arrValues['is_allow_call_transcription'] ) ) ); elseif( isset( $arrValues['is_allow_call_transcription'] ) ) $this->setIsAllowCallTranscription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allow_call_transcription'] ) : $arrValues['is_allow_call_transcription'] );
		if( isset( $arrValues['is_activate_corporate_care_line'] ) && $boolDirectSet ) $this->set( 'm_boolIsActivateCorporateCareLine', trim( stripcslashes( $arrValues['is_activate_corporate_care_line'] ) ) ); elseif( isset( $arrValues['is_activate_corporate_care_line'] ) ) $this->setIsActivateCorporateCareLine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_activate_corporate_care_line'] ) : $arrValues['is_activate_corporate_care_line'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['agreement_service_level'] ) && $boolDirectSet ) $this->set( 'm_intAgreementServiceLevel', trim( $arrValues['agreement_service_level'] ) ); elseif( isset( $arrValues['agreement_service_level'] ) ) $this->setAgreementServiceLevel( $arrValues['agreement_service_level'] );
		if( isset( $arrValues['call_analysis_service_level'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisServiceLevel', trim( $arrValues['call_analysis_service_level'] ) ); elseif( isset( $arrValues['call_analysis_service_level'] ) ) $this->setCallAnalysisServiceLevel( $arrValues['call_analysis_service_level'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCallCenterBaseScore( $intCallCenterBaseScore ) {
		$this->set( 'm_intCallCenterBaseScore', CStrings::strToIntDef( $intCallCenterBaseScore, NULL, false ) );
	}

	public function getCallCenterBaseScore() {
		return $this->m_intCallCenterBaseScore;
	}

	public function sqlCallCenterBaseScore() {
		return ( true == isset( $this->m_intCallCenterBaseScore ) ) ? ( string ) $this->m_intCallCenterBaseScore : 'NULL';
	}

	public function setDefaultCallQueueId( $intDefaultCallQueueId ) {
		$this->set( 'm_intDefaultCallQueueId', CStrings::strToIntDef( $intDefaultCallQueueId, NULL, false ) );
	}

	public function getDefaultCallQueueId() {
		return $this->m_intDefaultCallQueueId;
	}

	public function sqlDefaultCallQueueId() {
		return ( true == isset( $this->m_intDefaultCallQueueId ) ) ? ( string ) $this->m_intDefaultCallQueueId : 'NULL';
	}

	public function setIsGooglePpcActivated( $boolIsGooglePpcActivated ) {
		$this->set( 'm_boolIsGooglePpcActivated', CStrings::strToBool( $boolIsGooglePpcActivated ) );
	}

	public function getIsGooglePpcActivated() {
		return $this->m_boolIsGooglePpcActivated;
	}

	public function sqlIsGooglePpcActivated() {
		return ( true == isset( $this->m_boolIsGooglePpcActivated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGooglePpcActivated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAllowCallTranscription( $boolIsAllowCallTranscription ) {
		$this->set( 'm_boolIsAllowCallTranscription', CStrings::strToBool( $boolIsAllowCallTranscription ) );
	}

	public function getIsAllowCallTranscription() {
		return $this->m_boolIsAllowCallTranscription;
	}

	public function sqlIsAllowCallTranscription() {
		return ( true == isset( $this->m_boolIsAllowCallTranscription ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowCallTranscription ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActivateCorporateCareLine( $boolIsActivateCorporateCareLine ) {
		$this->set( 'm_boolIsActivateCorporateCareLine', CStrings::strToBool( $boolIsActivateCorporateCareLine ) );
	}

	public function getIsActivateCorporateCareLine() {
		return $this->m_boolIsActivateCorporateCareLine;
	}

	public function sqlIsActivateCorporateCareLine() {
		return ( true == isset( $this->m_boolIsActivateCorporateCareLine ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActivateCorporateCareLine ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAgreementServiceLevel( $intAgreementServiceLevel ) {
		$this->set( 'm_intAgreementServiceLevel', CStrings::strToIntDef( $intAgreementServiceLevel, NULL, false ) );
	}

	public function getAgreementServiceLevel() {
		return $this->m_intAgreementServiceLevel;
	}

	public function sqlAgreementServiceLevel() {
		return ( true == isset( $this->m_intAgreementServiceLevel ) ) ? ( string ) $this->m_intAgreementServiceLevel : '0';
	}

	public function setCallAnalysisServiceLevel( $intCallAnalysisServiceLevel ) {
		$this->set( 'm_intCallAnalysisServiceLevel', CStrings::strToIntDef( $intCallAnalysisServiceLevel, NULL, false ) );
	}

	public function getCallAnalysisServiceLevel() {
		return $this->m_intCallAnalysisServiceLevel;
	}

	public function sqlCallAnalysisServiceLevel() {
		return ( true == isset( $this->m_intCallAnalysisServiceLevel ) ) ? ( string ) $this->m_intCallAnalysisServiceLevel : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, call_center_base_score, default_call_queue_id, is_google_ppc_activated, is_allow_call_transcription, is_activate_corporate_care_line, updated_by, updated_on, created_by, created_on, agreement_service_level, call_analysis_service_level )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCallCenterBaseScore() . ', ' .
 						$this->sqlDefaultCallQueueId() . ', ' .
 						$this->sqlIsGooglePpcActivated() . ', ' .
 						$this->sqlIsAllowCallTranscription() . ', ' .
 						$this->sqlIsActivateCorporateCareLine() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlAgreementServiceLevel() . ', ' .
 						$this->sqlCallAnalysisServiceLevel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_center_base_score = ' . $this->sqlCallCenterBaseScore() . ','; } elseif( true == array_key_exists( 'CallCenterBaseScore', $this->getChangedColumns() ) ) { $strSql .= ' call_center_base_score = ' . $this->sqlCallCenterBaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_call_queue_id = ' . $this->sqlDefaultCallQueueId() . ','; } elseif( true == array_key_exists( 'DefaultCallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' default_call_queue_id = ' . $this->sqlDefaultCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_google_ppc_activated = ' . $this->sqlIsGooglePpcActivated() . ','; } elseif( true == array_key_exists( 'IsGooglePpcActivated', $this->getChangedColumns() ) ) { $strSql .= ' is_google_ppc_activated = ' . $this->sqlIsGooglePpcActivated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allow_call_transcription = ' . $this->sqlIsAllowCallTranscription() . ','; } elseif( true == array_key_exists( 'IsAllowCallTranscription', $this->getChangedColumns() ) ) { $strSql .= ' is_allow_call_transcription = ' . $this->sqlIsAllowCallTranscription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_activate_corporate_care_line = ' . $this->sqlIsActivateCorporateCareLine() . ','; } elseif( true == array_key_exists( 'IsActivateCorporateCareLine', $this->getChangedColumns() ) ) { $strSql .= ' is_activate_corporate_care_line = ' . $this->sqlIsActivateCorporateCareLine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_service_level = ' . $this->sqlAgreementServiceLevel() . ','; } elseif( true == array_key_exists( 'AgreementServiceLevel', $this->getChangedColumns() ) ) { $strSql .= ' agreement_service_level = ' . $this->sqlAgreementServiceLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_service_level = ' . $this->sqlCallAnalysisServiceLevel() . ','; } elseif( true == array_key_exists( 'CallAnalysisServiceLevel', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_service_level = ' . $this->sqlCallAnalysisServiceLevel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'call_center_base_score' => $this->getCallCenterBaseScore(),
			'default_call_queue_id' => $this->getDefaultCallQueueId(),
			'is_google_ppc_activated' => $this->getIsGooglePpcActivated(),
			'is_allow_call_transcription' => $this->getIsAllowCallTranscription(),
			'is_activate_corporate_care_line' => $this->getIsActivateCorporateCareLine(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'agreement_service_level' => $this->getAgreementServiceLevel(),
			'call_analysis_service_level' => $this->getCallAnalysisServiceLevel()
		);
	}

}
?>
