<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTypeProducts
 * Do not add any new functions to this class.
 */

class CBaseCallTypeProducts extends CEosPluralBase {

	/**
	 * @return CCallTypeProduct[]
	 */
	public static function fetchCallTypeProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallTypeProduct', $objDatabase );
	}

	/**
	 * @return CCallTypeProduct
	 */
	public static function fetchCallTypeProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallTypeProduct', $objDatabase );
	}

	public static function fetchCallTypeProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_type_products', $objDatabase );
	}

	public static function fetchCallTypeProductById( $intId, $objDatabase ) {
		return self::fetchCallTypeProduct( sprintf( 'SELECT * FROM call_type_products WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallTypeProductsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchCallTypeProducts( sprintf( 'SELECT * FROM call_type_products WHERE call_type_id = %d', ( int ) $intCallTypeId ), $objDatabase );
	}

	public static function fetchCallTypeProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchCallTypeProducts( sprintf( 'SELECT * FROM call_type_products WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>