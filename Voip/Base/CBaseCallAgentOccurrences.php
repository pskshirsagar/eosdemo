<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentOccurrences
 * Do not add any new functions to this class.
 */

class CBaseCallAgentOccurrences extends CEosPluralBase {

	/**
	 * @return CCallAgentOccurrence[]
	 */
	public static function fetchCallAgentOccurrences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentOccurrence', $objDatabase );
	}

	/**
	 * @return CCallAgentOccurrence
	 */
	public static function fetchCallAgentOccurrence( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentOccurrence', $objDatabase );
	}

	public static function fetchCallAgentOccurrenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_occurrences', $objDatabase );
	}

	public static function fetchCallAgentOccurrenceById( $intId, $objDatabase ) {
		return self::fetchCallAgentOccurrence( sprintf( 'SELECT * FROM call_agent_occurrences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentOccurrencesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentOccurrences( sprintf( 'SELECT * FROM call_agent_occurrences WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentOccurrencesByOccurrenceTypeId( $intOccurrenceTypeId, $objDatabase ) {
		return self::fetchCallAgentOccurrences( sprintf( 'SELECT * FROM call_agent_occurrences WHERE occurrence_type_id = %d', ( int ) $intOccurrenceTypeId ), $objDatabase );
	}

}
?>