<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTranscriptions
 * Do not add any new functions to this class.
 */

class CBaseCallTranscriptions extends CEosPluralBase {

	/**
	 * @return CCallTranscription[]
	 */
	public static function fetchCallTranscriptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallTranscription::class, $objDatabase );
	}

	/**
	 * @return CCallTranscription
	 */
	public static function fetchCallTranscription( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallTranscription::class, $objDatabase );
	}

	public static function fetchCallTranscriptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_transcriptions', $objDatabase );
	}

	public static function fetchCallTranscriptionById( $intId, $objDatabase ) {
		return self::fetchCallTranscription( sprintf( 'SELECT * FROM call_transcriptions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallTranscriptionsByCid( $intCid, $objDatabase ) {
		return self::fetchCallTranscriptions( sprintf( 'SELECT * FROM call_transcriptions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallTranscriptionsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallTranscriptions( sprintf( 'SELECT * FROM call_transcriptions WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchCallTranscriptionsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallTranscriptions( sprintf( 'SELECT * FROM call_transcriptions WHERE call_id = %d', $intCallId ), $objDatabase );
	}

}
?>