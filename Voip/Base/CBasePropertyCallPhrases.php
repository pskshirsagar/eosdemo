<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyCallPhrases
 * Do not add any new functions to this class.
 */

class CBasePropertyCallPhrases extends CEosPluralBase {

	/**
	 * @return CPropertyCallPhrase[]
	 */
	public static function fetchPropertyCallPhrases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyCallPhrase::class, $objDatabase );
	}

	/**
	 * @return CPropertyCallPhrase
	 */
	public static function fetchPropertyCallPhrase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyCallPhrase::class, $objDatabase );
	}

	public static function fetchPropertyCallPhraseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_call_phrases', $objDatabase );
	}

	public static function fetchPropertyCallPhraseById( $intId, $objDatabase ) {
		return self::fetchPropertyCallPhrase( sprintf( 'SELECT * FROM property_call_phrases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyCallPhrasesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCallPhrases( sprintf( 'SELECT * FROM property_call_phrases WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCallPhrasesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyCallPhrases( sprintf( 'SELECT * FROM property_call_phrases WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>