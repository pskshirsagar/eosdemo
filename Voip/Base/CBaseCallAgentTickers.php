<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentTickers
 * Do not add any new functions to this class.
 */

class CBaseCallAgentTickers extends CEosPluralBase {

	/**
	 * @return CCallAgentTicker[]
	 */
	public static function fetchCallAgentTickers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentTicker::class, $objDatabase );
	}

	/**
	 * @return CCallAgentTicker
	 */
	public static function fetchCallAgentTicker( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentTicker::class, $objDatabase );
	}

	public static function fetchCallAgentTickerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_tickers', $objDatabase );
	}

	public static function fetchCallAgentTickerById( $intId, $objDatabase ) {
		return self::fetchCallAgentTicker( sprintf( 'SELECT * FROM call_agent_tickers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentTickersByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentTickers( sprintf( 'SELECT * FROM call_agent_tickers WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentTickersByCallAgentTickerTypeId( $intCallAgentTickerTypeId, $objDatabase ) {
		return self::fetchCallAgentTickers( sprintf( 'SELECT * FROM call_agent_tickers WHERE call_agent_ticker_type_id = %d', ( int ) $intCallAgentTickerTypeId ), $objDatabase );
	}

	public static function fetchCallAgentTickersByDataReferenceId( $intDataReferenceId, $objDatabase ) {
		return self::fetchCallAgentTickers( sprintf( 'SELECT * FROM call_agent_tickers WHERE data_reference_id = %d', ( int ) $intDataReferenceId ), $objDatabase );
	}

}
?>