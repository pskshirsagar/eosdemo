<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallDownloadLogs
 * Do not add any new functions to this class.
 */

class CBaseCallDownloadLogs extends CEosPluralBase {

	/**
	 * @return CCallDownloadLog[]
	 */
	public static function fetchCallDownloadLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallDownloadLog', $objDatabase );
	}

	/**
	 * @return CCallDownloadLog
	 */
	public static function fetchCallDownloadLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallDownloadLog', $objDatabase );
	}

	public static function fetchCallDownloadLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_download_logs', $objDatabase );
	}

	public static function fetchCallDownloadLogById( $intId, $objDatabase ) {
		return self::fetchCallDownloadLog( sprintf( 'SELECT * FROM call_download_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallDownloadLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCallDownloadLogs( sprintf( 'SELECT * FROM call_download_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallDownloadLogsByCallFilterId( $intCallFilterId, $objDatabase ) {
		return self::fetchCallDownloadLogs( sprintf( 'SELECT * FROM call_download_logs WHERE call_filter_id = %d', ( int ) $intCallFilterId ), $objDatabase );
	}

}
?>