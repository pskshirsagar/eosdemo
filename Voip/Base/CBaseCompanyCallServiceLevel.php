<?php

class CBaseCompanyCallServiceLevel extends CEosSingularBase {

	const TABLE_NAME = 'public.company_call_service_levels';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTargetedServiceLevel;
	protected $m_intCurrentServiceLevel;
	protected $m_intAssignedBaseScore;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intOverrideServiceLevel;

	public function __construct() {
		parent::__construct();

		$this->m_intTargetedServiceLevel = '0';
		$this->m_intCurrentServiceLevel = '0';
		$this->m_intAssignedBaseScore = '100';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['targeted_service_level'] ) && $boolDirectSet ) $this->set( 'm_intTargetedServiceLevel', trim( $arrValues['targeted_service_level'] ) ); elseif( isset( $arrValues['targeted_service_level'] ) ) $this->setTargetedServiceLevel( $arrValues['targeted_service_level'] );
		if( isset( $arrValues['current_service_level'] ) && $boolDirectSet ) $this->set( 'm_intCurrentServiceLevel', trim( $arrValues['current_service_level'] ) ); elseif( isset( $arrValues['current_service_level'] ) ) $this->setCurrentServiceLevel( $arrValues['current_service_level'] );
		if( isset( $arrValues['assigned_base_score'] ) && $boolDirectSet ) $this->set( 'm_intAssignedBaseScore', trim( $arrValues['assigned_base_score'] ) ); elseif( isset( $arrValues['assigned_base_score'] ) ) $this->setAssignedBaseScore( $arrValues['assigned_base_score'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['override_service_level'] ) && $boolDirectSet ) $this->set( 'm_intOverrideServiceLevel', trim( $arrValues['override_service_level'] ) ); elseif( isset( $arrValues['override_service_level'] ) ) $this->setOverrideServiceLevel( $arrValues['override_service_level'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTargetedServiceLevel( $intTargetedServiceLevel ) {
		$this->set( 'm_intTargetedServiceLevel', CStrings::strToIntDef( $intTargetedServiceLevel, NULL, false ) );
	}

	public function getTargetedServiceLevel() {
		return $this->m_intTargetedServiceLevel;
	}

	public function sqlTargetedServiceLevel() {
		return ( true == isset( $this->m_intTargetedServiceLevel ) ) ? ( string ) $this->m_intTargetedServiceLevel : '0';
	}

	public function setCurrentServiceLevel( $intCurrentServiceLevel ) {
		$this->set( 'm_intCurrentServiceLevel', CStrings::strToIntDef( $intCurrentServiceLevel, NULL, false ) );
	}

	public function getCurrentServiceLevel() {
		return $this->m_intCurrentServiceLevel;
	}

	public function sqlCurrentServiceLevel() {
		return ( true == isset( $this->m_intCurrentServiceLevel ) ) ? ( string ) $this->m_intCurrentServiceLevel : '0';
	}

	public function setAssignedBaseScore( $intAssignedBaseScore ) {
		$this->set( 'm_intAssignedBaseScore', CStrings::strToIntDef( $intAssignedBaseScore, NULL, false ) );
	}

	public function getAssignedBaseScore() {
		return $this->m_intAssignedBaseScore;
	}

	public function sqlAssignedBaseScore() {
		return ( true == isset( $this->m_intAssignedBaseScore ) ) ? ( string ) $this->m_intAssignedBaseScore : '100';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setOverrideServiceLevel( $intOverrideServiceLevel ) {
		$this->set( 'm_intOverrideServiceLevel', CStrings::strToIntDef( $intOverrideServiceLevel, NULL, false ) );
	}

	public function getOverrideServiceLevel() {
		return $this->m_intOverrideServiceLevel;
	}

	public function sqlOverrideServiceLevel() {
		return ( true == isset( $this->m_intOverrideServiceLevel ) ) ? ( string ) $this->m_intOverrideServiceLevel : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, targeted_service_level, current_service_level, assigned_base_score, updated_by, updated_on, created_by, created_on, start_date, end_date, override_service_level )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTargetedServiceLevel() . ', ' .
 						$this->sqlCurrentServiceLevel() . ', ' .
 						$this->sqlAssignedBaseScore() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlOverrideServiceLevel() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' targeted_service_level = ' . $this->sqlTargetedServiceLevel() . ','; } elseif( true == array_key_exists( 'TargetedServiceLevel', $this->getChangedColumns() ) ) { $strSql .= ' targeted_service_level = ' . $this->sqlTargetedServiceLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_service_level = ' . $this->sqlCurrentServiceLevel() . ','; } elseif( true == array_key_exists( 'CurrentServiceLevel', $this->getChangedColumns() ) ) { $strSql .= ' current_service_level = ' . $this->sqlCurrentServiceLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_base_score = ' . $this->sqlAssignedBaseScore() . ','; } elseif( true == array_key_exists( 'AssignedBaseScore', $this->getChangedColumns() ) ) { $strSql .= ' assigned_base_score = ' . $this->sqlAssignedBaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_service_level = ' . $this->sqlOverrideServiceLevel() . ','; } elseif( true == array_key_exists( 'OverrideServiceLevel', $this->getChangedColumns() ) ) { $strSql .= ' override_service_level = ' . $this->sqlOverrideServiceLevel() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'targeted_service_level' => $this->getTargetedServiceLevel(),
			'current_service_level' => $this->getCurrentServiceLevel(),
			'assigned_base_score' => $this->getAssignedBaseScore(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'override_service_level' => $this->getOverrideServiceLevel()
		);
	}

}
?>