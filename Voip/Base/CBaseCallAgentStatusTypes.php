<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentStatusTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentStatusType[]
	 */
	public static function fetchCallAgentStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentStatusType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentStatusType
	 */
	public static function fetchCallAgentStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentStatusType::class, $objDatabase );
	}

	public static function fetchCallAgentStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_status_types', $objDatabase );
	}

	public static function fetchCallAgentStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentStatusType( sprintf( 'SELECT * FROM call_agent_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>