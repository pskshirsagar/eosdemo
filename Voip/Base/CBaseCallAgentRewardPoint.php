<?php

class CBaseCallAgentRewardPoint extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_reward_points';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intCallAgentRewardPointLogId;
	protected $m_fltTotalNumberOfPoints;
	protected $m_fltTotalNumberOfCoins;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTotalNumberOfPoints = '0.0';
		$this->m_fltTotalNumberOfCoins = '0.0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_agent_reward_point_log_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentRewardPointLogId', trim( $arrValues['call_agent_reward_point_log_id'] ) ); elseif( isset( $arrValues['call_agent_reward_point_log_id'] ) ) $this->setCallAgentRewardPointLogId( $arrValues['call_agent_reward_point_log_id'] );
		if( isset( $arrValues['total_number_of_points'] ) && $boolDirectSet ) $this->set( 'm_fltTotalNumberOfPoints', trim( $arrValues['total_number_of_points'] ) ); elseif( isset( $arrValues['total_number_of_points'] ) ) $this->setTotalNumberOfPoints( $arrValues['total_number_of_points'] );
		if( isset( $arrValues['total_number_of_coins'] ) && $boolDirectSet ) $this->set( 'm_fltTotalNumberOfCoins', trim( $arrValues['total_number_of_coins'] ) ); elseif( isset( $arrValues['total_number_of_coins'] ) ) $this->setTotalNumberOfCoins( $arrValues['total_number_of_coins'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallAgentRewardPointLogId( $intCallAgentRewardPointLogId ) {
		$this->set( 'm_intCallAgentRewardPointLogId', CStrings::strToIntDef( $intCallAgentRewardPointLogId, NULL, false ) );
	}

	public function getCallAgentRewardPointLogId() {
		return $this->m_intCallAgentRewardPointLogId;
	}

	public function sqlCallAgentRewardPointLogId() {
		return ( true == isset( $this->m_intCallAgentRewardPointLogId ) ) ? ( string ) $this->m_intCallAgentRewardPointLogId : 'NULL';
	}

	public function setTotalNumberOfPoints( $fltTotalNumberOfPoints ) {
		$this->set( 'm_fltTotalNumberOfPoints', CStrings::strToFloatDef( $fltTotalNumberOfPoints, NULL, false, 0 ) );
	}

	public function getTotalNumberOfPoints() {
		return $this->m_fltTotalNumberOfPoints;
	}

	public function sqlTotalNumberOfPoints() {
		return ( true == isset( $this->m_fltTotalNumberOfPoints ) ) ? ( string ) $this->m_fltTotalNumberOfPoints : '0.0';
	}

	public function setTotalNumberOfCoins( $fltTotalNumberOfCoins ) {
		$this->set( 'm_fltTotalNumberOfCoins', CStrings::strToFloatDef( $fltTotalNumberOfCoins, NULL, false, 0 ) );
	}

	public function getTotalNumberOfCoins() {
		return $this->m_fltTotalNumberOfCoins;
	}

	public function sqlTotalNumberOfCoins() {
		return ( true == isset( $this->m_fltTotalNumberOfCoins ) ) ? ( string ) $this->m_fltTotalNumberOfCoins : '0.0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, call_agent_reward_point_log_id, total_number_of_points, total_number_of_coins, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlCallAgentRewardPointLogId() . ', ' .
						$this->sqlTotalNumberOfPoints() . ', ' .
						$this->sqlTotalNumberOfCoins() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_reward_point_log_id = ' . $this->sqlCallAgentRewardPointLogId(). ',' ; } elseif( true == array_key_exists( 'CallAgentRewardPointLogId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_reward_point_log_id = ' . $this->sqlCallAgentRewardPointLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_number_of_points = ' . $this->sqlTotalNumberOfPoints(). ',' ; } elseif( true == array_key_exists( 'TotalNumberOfPoints', $this->getChangedColumns() ) ) { $strSql .= ' total_number_of_points = ' . $this->sqlTotalNumberOfPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_number_of_coins = ' . $this->sqlTotalNumberOfCoins(). ',' ; } elseif( true == array_key_exists( 'TotalNumberOfCoins', $this->getChangedColumns() ) ) { $strSql .= ' total_number_of_coins = ' . $this->sqlTotalNumberOfCoins() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_agent_reward_point_log_id' => $this->getCallAgentRewardPointLogId(),
			'total_number_of_points' => $this->getTotalNumberOfPoints(),
			'total_number_of_coins' => $this->getTotalNumberOfCoins(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>