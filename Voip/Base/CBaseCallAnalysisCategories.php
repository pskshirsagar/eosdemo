<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCategories
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisCategories extends CEosPluralBase {

	/**
	 * @return CCallAnalysisCategory[]
	 */
	public static function fetchCallAnalysisCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAnalysisCategory', $objDatabase );
	}

	/**
	 * @return CCallAnalysisCategory
	 */
	public static function fetchCallAnalysisCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAnalysisCategory', $objDatabase );
	}

	public static function fetchCallAnalysisCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_categories', $objDatabase );
	}

	public static function fetchCallAnalysisCategoryById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisCategory( sprintf( 'SELECT * FROM call_analysis_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisCategoriesByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisCategoriesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallAnalysisCategoriesByCallAnalysisScorecardId( $intCallAnalysisScorecardId, $objDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE call_analysis_scorecard_id = %d', ( int ) $intCallAnalysisScorecardId ), $objDatabase );
	}

}
?>