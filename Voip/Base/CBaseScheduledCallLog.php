<?php

class CBaseScheduledCallLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.scheduled_call_logs';

	protected $m_intId;
	protected $m_intScheduledCallId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_strExecutionStartTime;
	protected $m_strExecutionEndTime;
	protected $m_intTotalNumberOfCalls;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['scheduled_call_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledCallId', trim( $arrValues['scheduled_call_id'] ) ); elseif( isset( $arrValues['scheduled_call_id'] ) ) $this->setScheduledCallId( $arrValues['scheduled_call_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['execution_start_time'] ) && $boolDirectSet ) $this->set( 'm_strExecutionStartTime', trim( $arrValues['execution_start_time'] ) ); elseif( isset( $arrValues['execution_start_time'] ) ) $this->setExecutionStartTime( $arrValues['execution_start_time'] );
		if( isset( $arrValues['execution_end_time'] ) && $boolDirectSet ) $this->set( 'm_strExecutionEndTime', trim( $arrValues['execution_end_time'] ) ); elseif( isset( $arrValues['execution_end_time'] ) ) $this->setExecutionEndTime( $arrValues['execution_end_time'] );
		if( isset( $arrValues['total_number_of_calls'] ) && $boolDirectSet ) $this->set( 'm_intTotalNumberOfCalls', trim( $arrValues['total_number_of_calls'] ) ); elseif( isset( $arrValues['total_number_of_calls'] ) ) $this->setTotalNumberOfCalls( $arrValues['total_number_of_calls'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScheduledCallId( $intScheduledCallId ) {
		$this->set( 'm_intScheduledCallId', CStrings::strToIntDef( $intScheduledCallId, NULL, false ) );
	}

	public function getScheduledCallId() {
		return $this->m_intScheduledCallId;
	}

	public function sqlScheduledCallId() {
		return ( true == isset( $this->m_intScheduledCallId ) ) ? ( string ) $this->m_intScheduledCallId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setExecutionStartTime( $strExecutionStartTime ) {
		$this->set( 'm_strExecutionStartTime', CStrings::strTrimDef( $strExecutionStartTime, -1, NULL, true ) );
	}

	public function getExecutionStartTime() {
		return $this->m_strExecutionStartTime;
	}

	public function sqlExecutionStartTime() {
		return ( true == isset( $this->m_strExecutionStartTime ) ) ? '\'' . $this->m_strExecutionStartTime . '\'' : 'NULL';
	}

	public function setExecutionEndTime( $strExecutionEndTime ) {
		$this->set( 'm_strExecutionEndTime', CStrings::strTrimDef( $strExecutionEndTime, -1, NULL, true ) );
	}

	public function getExecutionEndTime() {
		return $this->m_strExecutionEndTime;
	}

	public function sqlExecutionEndTime() {
		return ( true == isset( $this->m_strExecutionEndTime ) ) ? '\'' . $this->m_strExecutionEndTime . '\'' : 'NULL';
	}

	public function setTotalNumberOfCalls( $intTotalNumberOfCalls ) {
		$this->set( 'm_intTotalNumberOfCalls', CStrings::strToIntDef( $intTotalNumberOfCalls, NULL, false ) );
	}

	public function getTotalNumberOfCalls() {
		return $this->m_intTotalNumberOfCalls;
	}

	public function sqlTotalNumberOfCalls() {
		return ( true == isset( $this->m_intTotalNumberOfCalls ) ) ? ( string ) $this->m_intTotalNumberOfCalls : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, scheduled_call_id, cid, property_id, execution_start_time, execution_end_time, total_number_of_calls, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScheduledCallId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlExecutionStartTime() . ', ' .
						$this->sqlExecutionEndTime() . ', ' .
						$this->sqlTotalNumberOfCalls() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_call_id = ' . $this->sqlScheduledCallId(). ',' ; } elseif( true == array_key_exists( 'ScheduledCallId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_call_id = ' . $this->sqlScheduledCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' execution_start_time = ' . $this->sqlExecutionStartTime(). ',' ; } elseif( true == array_key_exists( 'ExecutionStartTime', $this->getChangedColumns() ) ) { $strSql .= ' execution_start_time = ' . $this->sqlExecutionStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' execution_end_time = ' . $this->sqlExecutionEndTime(). ',' ; } elseif( true == array_key_exists( 'ExecutionEndTime', $this->getChangedColumns() ) ) { $strSql .= ' execution_end_time = ' . $this->sqlExecutionEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_number_of_calls = ' . $this->sqlTotalNumberOfCalls(). ',' ; } elseif( true == array_key_exists( 'TotalNumberOfCalls', $this->getChangedColumns() ) ) { $strSql .= ' total_number_of_calls = ' . $this->sqlTotalNumberOfCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'scheduled_call_id' => $this->getScheduledCallId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'execution_start_time' => $this->getExecutionStartTime(),
			'execution_end_time' => $this->getExecutionEndTime(),
			'total_number_of_calls' => $this->getTotalNumberOfCalls(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>