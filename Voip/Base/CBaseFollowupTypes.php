<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CFollowupTypes
 * Do not add any new functions to this class.
 */

class CBaseFollowupTypes extends CEosPluralBase {

	/**
	 * @return CFollowupType[]
	 */
	public static function fetchFollowupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFollowupType::class, $objDatabase );
	}

	/**
	 * @return CFollowupType
	 */
	public static function fetchFollowupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFollowupType::class, $objDatabase );
	}

	public static function fetchFollowupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'followup_types', $objDatabase );
	}

	public static function fetchFollowupTypeById( $intId, $objDatabase ) {
		return self::fetchFollowupType( sprintf( 'SELECT * FROM followup_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>