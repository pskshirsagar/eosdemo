<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseOutboundCallStatusTypes extends CEosPluralBase {

	/**
	 * @return COutboundCallStatusType[]
	 */
	public static function fetchOutboundCallStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COutboundCallStatusType::class, $objDatabase );
	}

	/**
	 * @return COutboundCallStatusType
	 */
	public static function fetchOutboundCallStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COutboundCallStatusType::class, $objDatabase );
	}

	public static function fetchOutboundCallStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'outbound_call_status_types', $objDatabase );
	}

	public static function fetchOutboundCallStatusTypeById( $intId, $objDatabase ) {
		return self::fetchOutboundCallStatusType( sprintf( 'SELECT * FROM outbound_call_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>