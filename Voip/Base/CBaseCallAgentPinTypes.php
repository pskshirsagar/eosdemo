<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentPinTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentPinTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentPinType[]
	 */
	public static function fetchCallAgentPinTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentPinType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentPinType
	 */
	public static function fetchCallAgentPinType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentPinType::class, $objDatabase );
	}

	public static function fetchCallAgentPinTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_pin_types', $objDatabase );
	}

	public static function fetchCallAgentPinTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentPinType( sprintf( 'SELECT * FROM call_agent_pin_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentPinTypesByCallAgentPinGroupId( $intCallAgentPinGroupId, $objDatabase ) {
		return self::fetchCallAgentPinTypes( sprintf( 'SELECT * FROM call_agent_pin_types WHERE call_agent_pin_group_id = %d', $intCallAgentPinGroupId ), $objDatabase );
	}

}
?>