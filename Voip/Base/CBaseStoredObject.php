<?php

class CBaseStoredObject extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.stored_objects';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intReferenceId;
	protected $m_strReferenceTable;
	protected $m_strReferenceTag;
	protected $m_strVendor;
	protected $m_strContainer;
	protected $m_strKey;
	protected $m_strTitle;
	protected $m_intContentLength;
	protected $m_strEtag;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['reference_table'] ) && $boolDirectSet ) $this->set( 'm_strReferenceTable', trim( $arrValues['reference_table'] ) ); elseif( isset( $arrValues['reference_table'] ) ) $this->setReferenceTable( $arrValues['reference_table'] );
		if( isset( $arrValues['reference_tag'] ) && $boolDirectSet ) $this->set( 'm_strReferenceTag', trim( $arrValues['reference_tag'] ) ); elseif( isset( $arrValues['reference_tag'] ) ) $this->setReferenceTag( $arrValues['reference_tag'] );
		if( isset( $arrValues['vendor'] ) && $boolDirectSet ) $this->set( 'm_strVendor', trim( $arrValues['vendor'] ) ); elseif( isset( $arrValues['vendor'] ) ) $this->setVendor( $arrValues['vendor'] );
		if( isset( $arrValues['container'] ) && $boolDirectSet ) $this->set( 'm_strContainer', trim( $arrValues['container'] ) ); elseif( isset( $arrValues['container'] ) ) $this->setContainer( $arrValues['container'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( $arrValues['key'] ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( $arrValues['key'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['content_length'] ) && $boolDirectSet ) $this->set( 'm_intContentLength', trim( $arrValues['content_length'] ) ); elseif( isset( $arrValues['content_length'] ) ) $this->setContentLength( $arrValues['content_length'] );
		if( isset( $arrValues['etag'] ) && $boolDirectSet ) $this->set( 'm_strEtag', trim( $arrValues['etag'] ) ); elseif( isset( $arrValues['etag'] ) ) $this->setEtag( $arrValues['etag'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setReferenceTable( $strReferenceTable ) {
		$this->set( 'm_strReferenceTable', CStrings::strTrimDef( $strReferenceTable, -1, NULL, true ) );
	}

	public function getReferenceTable() {
		return $this->m_strReferenceTable;
	}

	public function sqlReferenceTable() {
		return ( true == isset( $this->m_strReferenceTable ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceTable ) : '\'' . addslashes( $this->m_strReferenceTable ) . '\'' ) : 'NULL';
	}

	public function setReferenceTag( $strReferenceTag ) {
		$this->set( 'm_strReferenceTag', CStrings::strTrimDef( $strReferenceTag, -1, NULL, true ) );
	}

	public function getReferenceTag() {
		return $this->m_strReferenceTag;
	}

	public function sqlReferenceTag() {
		return ( true == isset( $this->m_strReferenceTag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceTag ) : '\'' . addslashes( $this->m_strReferenceTag ) . '\'' ) : 'NULL';
	}

	public function setVendor( $strVendor ) {
		$this->set( 'm_strVendor', CStrings::strTrimDef( $strVendor, -1, NULL, true ) );
	}

	public function getVendor() {
		return $this->m_strVendor;
	}

	public function sqlVendor() {
		return ( true == isset( $this->m_strVendor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVendor ) : '\'' . addslashes( $this->m_strVendor ) . '\'' ) : 'NULL';
	}

	public function setContainer( $strContainer ) {
		$this->set( 'm_strContainer', CStrings::strTrimDef( $strContainer, -1, NULL, true ) );
	}

	public function getContainer() {
		return $this->m_strContainer;
	}

	public function sqlContainer() {
		return ( true == isset( $this->m_strContainer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContainer ) : '\'' . addslashes( $this->m_strContainer ) . '\'' ) : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, -1, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKey ) : '\'' . addslashes( $this->m_strKey ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, -1, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setContentLength( $intContentLength ) {
		$this->set( 'm_intContentLength', CStrings::strToIntDef( $intContentLength, NULL, false ) );
	}

	public function getContentLength() {
		return $this->m_intContentLength;
	}

	public function sqlContentLength() {
		return ( true == isset( $this->m_intContentLength ) ) ? ( string ) $this->m_intContentLength : 'NULL';
	}

	public function setEtag( $strEtag ) {
		$this->set( 'm_strEtag', CStrings::strTrimDef( $strEtag, -1, NULL, true ) );
	}

	public function getEtag() {
		return $this->m_strEtag;
	}

	public function sqlEtag() {
		return ( true == isset( $this->m_strEtag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEtag ) : '\'' . addslashes( $this->m_strEtag ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, reference_id, reference_table, reference_tag, vendor, container, key, title, content_length, etag, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlReferenceTable() . ', ' .
						$this->sqlReferenceTag() . ', ' .
						$this->sqlVendor() . ', ' .
						$this->sqlContainer() . ', ' .
						$this->sqlKey() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlContentLength() . ', ' .
						$this->sqlEtag() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_table = ' . $this->sqlReferenceTable(). ',' ; } elseif( true == array_key_exists( 'ReferenceTable', $this->getChangedColumns() ) ) { $strSql .= ' reference_table = ' . $this->sqlReferenceTable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_tag = ' . $this->sqlReferenceTag(). ',' ; } elseif( true == array_key_exists( 'ReferenceTag', $this->getChangedColumns() ) ) { $strSql .= ' reference_tag = ' . $this->sqlReferenceTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor = ' . $this->sqlVendor(). ',' ; } elseif( true == array_key_exists( 'Vendor', $this->getChangedColumns() ) ) { $strSql .= ' vendor = ' . $this->sqlVendor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' container = ' . $this->sqlContainer(). ',' ; } elseif( true == array_key_exists( 'Container', $this->getChangedColumns() ) ) { $strSql .= ' container = ' . $this->sqlContainer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey(). ',' ; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_length = ' . $this->sqlContentLength(). ',' ; } elseif( true == array_key_exists( 'ContentLength', $this->getChangedColumns() ) ) { $strSql .= ' content_length = ' . $this->sqlContentLength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' etag = ' . $this->sqlEtag(). ',' ; } elseif( true == array_key_exists( 'Etag', $this->getChangedColumns() ) ) { $strSql .= ' etag = ' . $this->sqlEtag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'reference_id' => $this->getReferenceId(),
			'reference_table' => $this->getReferenceTable(),
			'reference_tag' => $this->getReferenceTag(),
			'vendor' => $this->getVendor(),
			'container' => $this->getContainer(),
			'key' => $this->getKey(),
			'title' => $this->getTitle(),
			'content_length' => $this->getContentLength(),
			'etag' => $this->getEtag(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>