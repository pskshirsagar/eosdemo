<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPhrases
 * Do not add any new functions to this class.
 */

class CBaseCallPhrases extends CEosPluralBase {

	/**
	 * @return CCallPhrase[]
	 */
	public static function fetchCallPhrases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallPhrase::class, $objDatabase );
	}

	/**
	 * @return CCallPhrase
	 */
	public static function fetchCallPhrase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallPhrase::class, $objDatabase );
	}

	public static function fetchCallPhraseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_phrases', $objDatabase );
	}

	public static function fetchCallPhraseById( $intId, $objDatabase ) {
		return self::fetchCallPhrase( sprintf( 'SELECT * FROM call_phrases WHERE id = %d', $intId ), $objDatabase );
	}

}
?>