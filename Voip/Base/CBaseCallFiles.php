<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFiles
 * Do not add any new functions to this class.
 */

class CBaseCallFiles extends CEosPluralBase {

	/**
	 * @return CCallFile[]
	 */
	public static function fetchCallFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallFile', $objDatabase );
	}

	/**
	 * @return CCallFile
	 */
	public static function fetchCallFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallFile', $objDatabase );
	}

	public static function fetchCallFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_files', $objDatabase );
	}

	public static function fetchCallFileById( $intId, $objDatabase ) {
		return self::fetchCallFile( sprintf( 'SELECT * FROM call_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallFilesByCid( $intCid, $objDatabase ) {
		return self::fetchCallFiles( sprintf( 'SELECT * FROM call_files WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallFilesByFileExtensionId( $intFileExtensionId, $objDatabase ) {
		return self::fetchCallFiles( sprintf( 'SELECT * FROM call_files WHERE file_extension_id = %d', ( int ) $intFileExtensionId ), $objDatabase );
	}

}
?>