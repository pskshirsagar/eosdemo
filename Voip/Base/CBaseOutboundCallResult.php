<?php

class CBaseOutboundCallResult extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.outbound_call_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intOutboundCallId;
	protected $m_intCallTypeId;
	protected $m_intPsProductId;
	protected $m_intCallResultId;
	protected $m_intReferenceNumber;
	protected $m_strNotes;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['outbound_call_id'] ) && $boolDirectSet ) $this->set( 'm_intOutboundCallId', trim( $arrValues['outbound_call_id'] ) ); elseif( isset( $arrValues['outbound_call_id'] ) ) $this->setOutboundCallId( $arrValues['outbound_call_id'] );
		if( isset( $arrValues['call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallTypeId', trim( $arrValues['call_type_id'] ) ); elseif( isset( $arrValues['call_type_id'] ) ) $this->setCallTypeId( $arrValues['call_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['call_result_id'] ) && $boolDirectSet ) $this->set( 'm_intCallResultId', trim( $arrValues['call_result_id'] ) ); elseif( isset( $arrValues['call_result_id'] ) ) $this->setCallResultId( $arrValues['call_result_id'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['notes'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setOutboundCallId( $intOutboundCallId ) {
		$this->set( 'm_intOutboundCallId', CStrings::strToIntDef( $intOutboundCallId, NULL, false ) );
	}

	public function getOutboundCallId() {
		return $this->m_intOutboundCallId;
	}

	public function sqlOutboundCallId() {
		return ( true == isset( $this->m_intOutboundCallId ) ) ? ( string ) $this->m_intOutboundCallId : 'NULL';
	}

	public function setCallTypeId( $intCallTypeId ) {
		$this->set( 'm_intCallTypeId', CStrings::strToIntDef( $intCallTypeId, NULL, false ) );
	}

	public function getCallTypeId() {
		return $this->m_intCallTypeId;
	}

	public function sqlCallTypeId() {
		return ( true == isset( $this->m_intCallTypeId ) ) ? ( string ) $this->m_intCallTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCallResultId( $intCallResultId ) {
		$this->set( 'm_intCallResultId', CStrings::strToIntDef( $intCallResultId, NULL, false ) );
	}

	public function getCallResultId() {
		return $this->m_intCallResultId;
	}

	public function sqlCallResultId() {
		return ( true == isset( $this->m_intCallResultId ) ) ? ( string ) $this->m_intCallResultId : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setNotes( $strNotes, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ), $strLocaleCode );
	}

	public function getNotes( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strNotes', $strLocaleCode );
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, outbound_call_id, call_type_id, ps_product_id, call_result_id, reference_number, notes, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlOutboundCallId() . ', ' .
						$this->sqlCallTypeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlCallResultId() . ', ' .
						$this->sqlReferenceNumber() . ', ' .
						$this->sqlNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outbound_call_id = ' . $this->sqlOutboundCallId(). ',' ; } elseif( true == array_key_exists( 'OutboundCallId', $this->getChangedColumns() ) ) { $strSql .= ' outbound_call_id = ' . $this->sqlOutboundCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId(). ',' ; } elseif( true == array_key_exists( 'CallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId(). ',' ; } elseif( true == array_key_exists( 'CallResultId', $this->getChangedColumns() ) ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'outbound_call_id' => $this->getOutboundCallId(),
			'call_type_id' => $this->getCallTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'call_result_id' => $this->getCallResultId(),
			'reference_number' => $this->getReferenceNumber(),
			'notes' => $this->getNotes(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>