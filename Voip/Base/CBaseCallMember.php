<?php

class CBaseCallMember extends CEosSingularBase {

	const TABLE_NAME = 'public.call_members';

	protected $m_intId;
	protected $m_intCallId;
	protected $m_intCallQueueId;
	protected $m_intCallAgentId;
	protected $m_strCallerName;
	protected $m_strCallerNumber;
	protected $m_intBaseScore;
	protected $m_strSessionUuid;
	protected $m_strServerIp;
	protected $m_boolIsAssignedCallAgent;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAssignedCallAgent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['caller_name'] ) && $boolDirectSet ) $this->set( 'm_strCallerName', trim( stripcslashes( $arrValues['caller_name'] ) ) ); elseif( isset( $arrValues['caller_name'] ) ) $this->setCallerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caller_name'] ) : $arrValues['caller_name'] );
		if( isset( $arrValues['caller_number'] ) && $boolDirectSet ) $this->set( 'm_strCallerNumber', trim( stripcslashes( $arrValues['caller_number'] ) ) ); elseif( isset( $arrValues['caller_number'] ) ) $this->setCallerNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caller_number'] ) : $arrValues['caller_number'] );
		if( isset( $arrValues['base_score'] ) && $boolDirectSet ) $this->set( 'm_intBaseScore', trim( $arrValues['base_score'] ) ); elseif( isset( $arrValues['base_score'] ) ) $this->setBaseScore( $arrValues['base_score'] );
		if( isset( $arrValues['session_uuid'] ) && $boolDirectSet ) $this->set( 'm_strSessionUuid', trim( stripcslashes( $arrValues['session_uuid'] ) ) ); elseif( isset( $arrValues['session_uuid'] ) ) $this->setSessionUuid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['session_uuid'] ) : $arrValues['session_uuid'] );
		if( isset( $arrValues['server_ip'] ) && $boolDirectSet ) $this->set( 'm_strServerIp', trim( stripcslashes( $arrValues['server_ip'] ) ) ); elseif( isset( $arrValues['server_ip'] ) ) $this->setServerIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['server_ip'] ) : $arrValues['server_ip'] );
		if( isset( $arrValues['is_assigned_call_agent'] ) && $boolDirectSet ) $this->set( 'm_boolIsAssignedCallAgent', trim( stripcslashes( $arrValues['is_assigned_call_agent'] ) ) ); elseif( isset( $arrValues['is_assigned_call_agent'] ) ) $this->setIsAssignedCallAgent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_assigned_call_agent'] ) : $arrValues['is_assigned_call_agent'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallerName( $strCallerName ) {
		$this->set( 'm_strCallerName', CStrings::strTrimDef( $strCallerName, 240, NULL, true ) );
	}

	public function getCallerName() {
		return $this->m_strCallerName;
	}

	public function sqlCallerName() {
		return ( true == isset( $this->m_strCallerName ) ) ? '\'' . addslashes( $this->m_strCallerName ) . '\'' : 'NULL';
	}

	public function setCallerNumber( $strCallerNumber ) {
		$this->set( 'm_strCallerNumber', CStrings::strTrimDef( $strCallerNumber, 30, NULL, true ) );
	}

	public function getCallerNumber() {
		return $this->m_strCallerNumber;
	}

	public function sqlCallerNumber() {
		return ( true == isset( $this->m_strCallerNumber ) ) ? '\'' . addslashes( $this->m_strCallerNumber ) . '\'' : 'NULL';
	}

	public function setBaseScore( $intBaseScore ) {
		$this->m_intBaseScore = CStrings::strToIntDef( $intBaseScore, NULL, false );
		$this->set( 'm_intBaseScore', CStrings::strToIntDef( $intBaseScore, NULL, false ) );
	}

	public function getBaseScore() {
		return $this->m_intBaseScore;
	}

	public function sqlBaseScore() {
		return ( true == isset( $this->m_intBaseScore ) ) ? ( string ) $this->m_intBaseScore : 'NULL';
	}

	public function setSessionUuid( $strSessionUuid ) {
		$this->set( 'm_strSessionUuid', CStrings::strTrimDef( $strSessionUuid, 240, NULL, true ) );
	}

	public function getSessionUuid() {
		return $this->m_strSessionUuid;
	}

	public function sqlSessionUuid() {
		return ( true == isset( $this->m_strSessionUuid ) ) ? '\'' . addslashes( $this->m_strSessionUuid ) . '\'' : 'NULL';
	}

	public function setServerIp( $strServerIp ) {
		$this->set( 'm_strServerIp', CStrings::strTrimDef( $strServerIp, 240, NULL, true ) );
	}

	public function getServerIp() {
		return $this->m_strServerIp;
	}

	public function sqlServerIp() {
		return ( true == isset( $this->m_strServerIp ) ) ? '\'' . addslashes( $this->m_strServerIp ) . '\'' : 'NULL';
	}

	public function setIsAssignedCallAgent( $boolIsAssignedCallAgent ) {
		$this->set( 'm_boolIsAssignedCallAgent', CStrings::strToBool( $boolIsAssignedCallAgent ) );
	}

	public function getIsAssignedCallAgent() {
		return $this->m_boolIsAssignedCallAgent;
	}

	public function sqlIsAssignedCallAgent() {
		return ( true == isset( $this->m_boolIsAssignedCallAgent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAssignedCallAgent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_id, call_queue_id, call_agent_id, caller_name, caller_number, base_score, session_uuid, server_ip, is_assigned_call_agent, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlCallQueueId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallerName() . ', ' .
 						$this->sqlCallerNumber() . ', ' .
 						$this->sqlBaseScore() . ', ' .
 						$this->sqlSessionUuid() . ', ' .
 						$this->sqlServerIp() . ', ' .
 						$this->sqlIsAssignedCallAgent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caller_name = ' . $this->sqlCallerName() . ','; } elseif( true == array_key_exists( 'CallerName', $this->getChangedColumns() ) ) { $strSql .= ' caller_name = ' . $this->sqlCallerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caller_number = ' . $this->sqlCallerNumber() . ','; } elseif( true == array_key_exists( 'CallerNumber', $this->getChangedColumns() ) ) { $strSql .= ' caller_number = ' . $this->sqlCallerNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_score = ' . $this->sqlBaseScore() . ','; } elseif( true == array_key_exists( 'BaseScore', $this->getChangedColumns() ) ) { $strSql .= ' base_score = ' . $this->sqlBaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid() . ','; } elseif( true == array_key_exists( 'SessionUuid', $this->getChangedColumns() ) ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server_ip = ' . $this->sqlServerIp() . ','; } elseif( true == array_key_exists( 'ServerIp', $this->getChangedColumns() ) ) { $strSql .= ' server_ip = ' . $this->sqlServerIp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_assigned_call_agent = ' . $this->sqlIsAssignedCallAgent() . ','; } elseif( true == array_key_exists( 'IsAssignedCallAgent', $this->getChangedColumns() ) ) { $strSql .= ' is_assigned_call_agent = ' . $this->sqlIsAssignedCallAgent() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_id' => $this->getCallId(),
			'call_queue_id' => $this->getCallQueueId(),
			'call_agent_id' => $this->getCallAgentId(),
			'caller_name' => $this->getCallerName(),
			'caller_number' => $this->getCallerNumber(),
			'base_score' => $this->getBaseScore(),
			'session_uuid' => $this->getSessionUuid(),
			'server_ip' => $this->getServerIp(),
			'is_assigned_call_agent' => $this->getIsAssignedCallAgent(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>