<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServiceLevels
 * Do not add any new functions to this class.
 */

class CBaseCallServiceLevels extends CEosPluralBase {

	/**
	 * @return CCallServiceLevel[]
	 */
	public static function fetchCallServiceLevels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallServiceLevel::class, $objDatabase );
	}

	/**
	 * @return CCallServiceLevel
	 */
	public static function fetchCallServiceLevel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallServiceLevel::class, $objDatabase );
	}

	public static function fetchCallServiceLevelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_service_levels', $objDatabase );
	}

	public static function fetchCallServiceLevelById( $intId, $objDatabase ) {
		return self::fetchCallServiceLevel( sprintf( 'SELECT * FROM call_service_levels WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>