<?php

class CBaseStoreProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.store_products';

	protected $m_intId;
	protected $m_intStoreProductTypeId;
	protected $m_intFrequencyId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltMonetaryValue;
	protected $m_strImagePath;
	protected $m_strRequestFormUrl;
	protected $m_boolIsInventory;
	protected $m_intInitialQuantity;
	protected $m_intCurrentQuantity;
	protected $m_intRequiredCoins;
	protected $m_intPerkCredit;
	protected $m_fltCostAmount;
	protected $m_strRemark;
	protected $m_boolIsPublished;
	protected $m_intAddedBy;
	protected $m_strAddedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intStoreProductTypeId = '1';
		$this->m_strDescription = NULL;
		$this->m_fltMonetaryValue = '0.0';
		$this->m_strImagePath = NULL;
		$this->m_strRequestFormUrl = NULL;
		$this->m_boolIsInventory = false;
		$this->m_intInitialQuantity = '0';
		$this->m_intCurrentQuantity = '0';
		$this->m_intRequiredCoins = '0';
		$this->m_fltCostAmount = '0.0';
		$this->m_strRemark = NULL;
		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['store_product_type_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreProductTypeId', trim( $arrValues['store_product_type_id'] ) ); elseif( isset( $arrValues['store_product_type_id'] ) ) $this->setStoreProductTypeId( $arrValues['store_product_type_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['monetary_value'] ) && $boolDirectSet ) $this->set( 'm_fltMonetaryValue', trim( $arrValues['monetary_value'] ) ); elseif( isset( $arrValues['monetary_value'] ) ) $this->setMonetaryValue( $arrValues['monetary_value'] );
		if( isset( $arrValues['image_path'] ) && $boolDirectSet ) $this->set( 'm_strImagePath', trim( stripcslashes( $arrValues['image_path'] ) ) ); elseif( isset( $arrValues['image_path'] ) ) $this->setImagePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_path'] ) : $arrValues['image_path'] );
		if( isset( $arrValues['request_form_url'] ) && $boolDirectSet ) $this->set( 'm_strRequestFormUrl', trim( stripcslashes( $arrValues['request_form_url'] ) ) ); elseif( isset( $arrValues['request_form_url'] ) ) $this->setRequestFormUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_form_url'] ) : $arrValues['request_form_url'] );
		if( isset( $arrValues['is_inventory'] ) && $boolDirectSet ) $this->set( 'm_boolIsInventory', trim( stripcslashes( $arrValues['is_inventory'] ) ) ); elseif( isset( $arrValues['is_inventory'] ) ) $this->setIsInventory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_inventory'] ) : $arrValues['is_inventory'] );
		if( isset( $arrValues['initial_quantity'] ) && $boolDirectSet ) $this->set( 'm_intInitialQuantity', trim( $arrValues['initial_quantity'] ) ); elseif( isset( $arrValues['initial_quantity'] ) ) $this->setInitialQuantity( $arrValues['initial_quantity'] );
		if( isset( $arrValues['current_quantity'] ) && $boolDirectSet ) $this->set( 'm_intCurrentQuantity', trim( $arrValues['current_quantity'] ) ); elseif( isset( $arrValues['current_quantity'] ) ) $this->setCurrentQuantity( $arrValues['current_quantity'] );
		if( isset( $arrValues['required_coins'] ) && $boolDirectSet ) $this->set( 'm_intRequiredCoins', trim( $arrValues['required_coins'] ) ); elseif( isset( $arrValues['required_coins'] ) ) $this->setRequiredCoins( $arrValues['required_coins'] );
		if( isset( $arrValues['perk_credit'] ) && $boolDirectSet ) $this->set( 'm_intPerkCredit', trim( $arrValues['perk_credit'] ) ); elseif( isset( $arrValues['perk_credit'] ) ) $this->setPerkCredit( $arrValues['perk_credit'] );
		if( isset( $arrValues['cost_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCostAmount', trim( $arrValues['cost_amount'] ) ); elseif( isset( $arrValues['cost_amount'] ) ) $this->setCostAmount( $arrValues['cost_amount'] );
		if( isset( $arrValues['remark'] ) && $boolDirectSet ) $this->set( 'm_strRemark', trim( stripcslashes( $arrValues['remark'] ) ) ); elseif( isset( $arrValues['remark'] ) ) $this->setRemark( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remark'] ) : $arrValues['remark'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['added_by'] ) && $boolDirectSet ) $this->set( 'm_intAddedBy', trim( $arrValues['added_by'] ) ); elseif( isset( $arrValues['added_by'] ) ) $this->setAddedBy( $arrValues['added_by'] );
		if( isset( $arrValues['added_on'] ) && $boolDirectSet ) $this->set( 'm_strAddedOn', trim( $arrValues['added_on'] ) ); elseif( isset( $arrValues['added_on'] ) ) $this->setAddedOn( $arrValues['added_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStoreProductTypeId( $intStoreProductTypeId ) {
		$this->set( 'm_intStoreProductTypeId', CStrings::strToIntDef( $intStoreProductTypeId, NULL, false ) );
	}

	public function getStoreProductTypeId() {
		return $this->m_intStoreProductTypeId;
	}

	public function sqlStoreProductTypeId() {
		return ( true == isset( $this->m_intStoreProductTypeId ) ) ? ( string ) $this->m_intStoreProductTypeId : '1';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 250, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setMonetaryValue( $fltMonetaryValue ) {
		$this->set( 'm_fltMonetaryValue', CStrings::strToFloatDef( $fltMonetaryValue, NULL, false, 0 ) );
	}

	public function getMonetaryValue() {
		return $this->m_fltMonetaryValue;
	}

	public function sqlMonetaryValue() {
		return ( true == isset( $this->m_fltMonetaryValue ) ) ? ( string ) $this->m_fltMonetaryValue : '0.0';
	}

	public function setImagePath( $strImagePath ) {
		$this->set( 'm_strImagePath', CStrings::strTrimDef( $strImagePath, 150, NULL, true ) );
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function sqlImagePath() {
		return ( true == isset( $this->m_strImagePath ) ) ? '\'' . addslashes( $this->m_strImagePath ) . '\'' : '\'NULL\'';
	}

	public function setRequestFormUrl( $strRequestFormUrl ) {
		$this->set( 'm_strRequestFormUrl', CStrings::strTrimDef( $strRequestFormUrl, 150, NULL, true ) );
	}

	public function getRequestFormUrl() {
		return $this->m_strRequestFormUrl;
	}

	public function sqlRequestFormUrl() {
		return ( true == isset( $this->m_strRequestFormUrl ) ) ? '\'' . addslashes( $this->m_strRequestFormUrl ) . '\'' : '\'NULL\'';
	}

	public function setIsInventory( $boolIsInventory ) {
		$this->set( 'm_boolIsInventory', CStrings::strToBool( $boolIsInventory ) );
	}

	public function getIsInventory() {
		return $this->m_boolIsInventory;
	}

	public function sqlIsInventory() {
		return ( true == isset( $this->m_boolIsInventory ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInventory ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInitialQuantity( $intInitialQuantity ) {
		$this->set( 'm_intInitialQuantity', CStrings::strToIntDef( $intInitialQuantity, NULL, false ) );
	}

	public function getInitialQuantity() {
		return $this->m_intInitialQuantity;
	}

	public function sqlInitialQuantity() {
		return ( true == isset( $this->m_intInitialQuantity ) ) ? ( string ) $this->m_intInitialQuantity : '0';
	}

	public function setCurrentQuantity( $intCurrentQuantity ) {
		$this->set( 'm_intCurrentQuantity', CStrings::strToIntDef( $intCurrentQuantity, NULL, false ) );
	}

	public function getCurrentQuantity() {
		return $this->m_intCurrentQuantity;
	}

	public function sqlCurrentQuantity() {
		return ( true == isset( $this->m_intCurrentQuantity ) ) ? ( string ) $this->m_intCurrentQuantity : '0';
	}

	public function setRequiredCoins( $intRequiredCoins ) {
		$this->set( 'm_intRequiredCoins', CStrings::strToIntDef( $intRequiredCoins, NULL, false ) );
	}

	public function getRequiredCoins() {
		return $this->m_intRequiredCoins;
	}

	public function sqlRequiredCoins() {
		return ( true == isset( $this->m_intRequiredCoins ) ) ? ( string ) $this->m_intRequiredCoins : '0';
	}

	public function setPerkCredit( $intPerkCredit ) {
		$this->set( 'm_intPerkCredit', CStrings::strToIntDef( $intPerkCredit, NULL, false ) );
	}

	public function getPerkCredit() {
		return $this->m_intPerkCredit;
	}

	public function sqlPerkCredit() {
		return ( true == isset( $this->m_intPerkCredit ) ) ? ( string ) $this->m_intPerkCredit : 'NULL';
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->set( 'm_fltCostAmount', CStrings::strToFloatDef( $fltCostAmount, NULL, false, 0 ) );
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function sqlCostAmount() {
		return ( true == isset( $this->m_fltCostAmount ) ) ? ( string ) $this->m_fltCostAmount : '0.0';
	}

	public function setRemark( $strRemark ) {
		$this->set( 'm_strRemark', CStrings::strTrimDef( $strRemark, 100, NULL, true ) );
	}

	public function getRemark() {
		return $this->m_strRemark;
	}

	public function sqlRemark() {
		return ( true == isset( $this->m_strRemark ) ) ? '\'' . addslashes( $this->m_strRemark ) . '\'' : '\'NULL\'';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAddedBy( $intAddedBy ) {
		$this->set( 'm_intAddedBy', CStrings::strToIntDef( $intAddedBy, NULL, false ) );
	}

	public function getAddedBy() {
		return $this->m_intAddedBy;
	}

	public function sqlAddedBy() {
		return ( true == isset( $this->m_intAddedBy ) ) ? ( string ) $this->m_intAddedBy : 'NULL';
	}

	public function setAddedOn( $strAddedOn ) {
		$this->set( 'm_strAddedOn', CStrings::strTrimDef( $strAddedOn, -1, NULL, true ) );
	}

	public function getAddedOn() {
		return $this->m_strAddedOn;
	}

	public function sqlAddedOn() {
		return ( true == isset( $this->m_strAddedOn ) ) ? '\'' . $this->m_strAddedOn . '\'' : 'NOW()';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, store_product_type_id, frequency_id, name, description, monetary_value, image_path, request_form_url, is_inventory, initial_quantity, current_quantity, required_coins, perk_credit, cost_amount, remark, is_published, added_by, added_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlStoreProductTypeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlMonetaryValue() . ', ' .
						$this->sqlImagePath() . ', ' .
						$this->sqlRequestFormUrl() . ', ' .
						$this->sqlIsInventory() . ', ' .
						$this->sqlInitialQuantity() . ', ' .
						$this->sqlCurrentQuantity() . ', ' .
						$this->sqlRequiredCoins() . ', ' .
						$this->sqlPerkCredit() . ', ' .
						$this->sqlCostAmount() . ', ' .
						$this->sqlRemark() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlAddedBy() . ', ' .
						$this->sqlAddedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_product_type_id = ' . $this->sqlStoreProductTypeId(). ',' ; } elseif( true == array_key_exists( 'StoreProductTypeId', $this->getChangedColumns() ) ) { $strSql .= ' store_product_type_id = ' . $this->sqlStoreProductTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monetary_value = ' . $this->sqlMonetaryValue(). ',' ; } elseif( true == array_key_exists( 'MonetaryValue', $this->getChangedColumns() ) ) { $strSql .= ' monetary_value = ' . $this->sqlMonetaryValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_path = ' . $this->sqlImagePath(). ',' ; } elseif( true == array_key_exists( 'ImagePath', $this->getChangedColumns() ) ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_form_url = ' . $this->sqlRequestFormUrl(). ',' ; } elseif( true == array_key_exists( 'RequestFormUrl', $this->getChangedColumns() ) ) { $strSql .= ' request_form_url = ' . $this->sqlRequestFormUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_inventory = ' . $this->sqlIsInventory(). ',' ; } elseif( true == array_key_exists( 'IsInventory', $this->getChangedColumns() ) ) { $strSql .= ' is_inventory = ' . $this->sqlIsInventory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initial_quantity = ' . $this->sqlInitialQuantity(). ',' ; } elseif( true == array_key_exists( 'InitialQuantity', $this->getChangedColumns() ) ) { $strSql .= ' initial_quantity = ' . $this->sqlInitialQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_quantity = ' . $this->sqlCurrentQuantity(). ',' ; } elseif( true == array_key_exists( 'CurrentQuantity', $this->getChangedColumns() ) ) { $strSql .= ' current_quantity = ' . $this->sqlCurrentQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_coins = ' . $this->sqlRequiredCoins(). ',' ; } elseif( true == array_key_exists( 'RequiredCoins', $this->getChangedColumns() ) ) { $strSql .= ' required_coins = ' . $this->sqlRequiredCoins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' perk_credit = ' . $this->sqlPerkCredit(). ',' ; } elseif( true == array_key_exists( 'PerkCredit', $this->getChangedColumns() ) ) { $strSql .= ' perk_credit = ' . $this->sqlPerkCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount(). ',' ; } elseif( true == array_key_exists( 'CostAmount', $this->getChangedColumns() ) ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remark = ' . $this->sqlRemark(). ',' ; } elseif( true == array_key_exists( 'Remark', $this->getChangedColumns() ) ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' added_by = ' . $this->sqlAddedBy(). ',' ; } elseif( true == array_key_exists( 'AddedBy', $this->getChangedColumns() ) ) { $strSql .= ' added_by = ' . $this->sqlAddedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' added_on = ' . $this->sqlAddedOn(). ',' ; } elseif( true == array_key_exists( 'AddedOn', $this->getChangedColumns() ) ) { $strSql .= ' added_on = ' . $this->sqlAddedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'store_product_type_id' => $this->getStoreProductTypeId(),
			'frequency_id' => $this->getFrequencyId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'monetary_value' => $this->getMonetaryValue(),
			'image_path' => $this->getImagePath(),
			'request_form_url' => $this->getRequestFormUrl(),
			'is_inventory' => $this->getIsInventory(),
			'initial_quantity' => $this->getInitialQuantity(),
			'current_quantity' => $this->getCurrentQuantity(),
			'required_coins' => $this->getRequiredCoins(),
			'perk_credit' => $this->getPerkCredit(),
			'cost_amount' => $this->getCostAmount(),
			'remark' => $this->getRemark(),
			'is_published' => $this->getIsPublished(),
			'added_by' => $this->getAddedBy(),
			'added_on' => $this->getAddedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>