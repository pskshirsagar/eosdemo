<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFollowups
 * Do not add any new functions to this class.
 */

class CBaseCallFollowups extends CEosPluralBase {

	/**
	 * @return CCallFollowup[]
	 */
	public static function fetchCallFollowups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallFollowup', $objDatabase );
	}

	/**
	 * @return CCallFollowup
	 */
	public static function fetchCallFollowup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallFollowup', $objDatabase );
	}

	public static function fetchCallFollowupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_followups', $objDatabase );
	}

	public static function fetchCallFollowupById( $intId, $objDatabase ) {
		return self::fetchCallFollowup( sprintf( 'SELECT * FROM call_followups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallFollowupsByCid( $intCid, $objDatabase ) {
		return self::fetchCallFollowups( sprintf( 'SELECT * FROM call_followups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallFollowupsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallFollowups( sprintf( 'SELECT * FROM call_followups WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallFollowupsByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchCallFollowups( sprintf( 'SELECT * FROM call_followups WHERE applicant_id = %d', ( int ) $intApplicantId ), $objDatabase );
	}

	public static function fetchCallFollowupsByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchCallFollowups( sprintf( 'SELECT * FROM call_followups WHERE application_id = %d', ( int ) $intApplicationId ), $objDatabase );
	}

	public static function fetchCallFollowupsByApplicantApplicationId( $intApplicantApplicationId, $objDatabase ) {
		return self::fetchCallFollowups( sprintf( 'SELECT * FROM call_followups WHERE applicant_application_id = %d', ( int ) $intApplicantApplicationId ), $objDatabase );
	}

}
?>