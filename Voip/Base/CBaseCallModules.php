<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallModules
 * Do not add any new functions to this class.
 */

class CBaseCallModules extends CEosPluralBase {

	/**
	 * @return CCallModule[]
	 */
	public static function fetchCallModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallModule', $objDatabase );
	}

	/**
	 * @return CCallModule
	 */
	public static function fetchCallModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallModule', $objDatabase );
	}

	public static function fetchCallModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_modules', $objDatabase );
	}

	public static function fetchCallModuleById( $intId, $objDatabase ) {
		return self::fetchCallModule( sprintf( 'SELECT * FROM call_modules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>