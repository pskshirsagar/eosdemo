<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentScheduleDetails
 * Do not add any new functions to this class.
 */

class CBaseCallAgentScheduleDetails extends CEosPluralBase {

	/**
	 * @return CCallAgentScheduleDetail[]
	 */
	public static function fetchCallAgentScheduleDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentScheduleDetail', $objDatabase );
	}

	/**
	 * @return CCallAgentScheduleDetail
	 */
	public static function fetchCallAgentScheduleDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentScheduleDetail', $objDatabase );
	}

	public static function fetchCallAgentScheduleDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_schedule_details', $objDatabase );
	}

	public static function fetchCallAgentScheduleDetailById( $intId, $objDatabase ) {
		return self::fetchCallAgentScheduleDetail( sprintf( 'SELECT * FROM call_agent_schedule_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentScheduleDetailsByCallAgentScheduleId( $intCallAgentScheduleId, $objDatabase ) {
		return self::fetchCallAgentScheduleDetails( sprintf( 'SELECT * FROM call_agent_schedule_details WHERE call_agent_schedule_id = %d', ( int ) $intCallAgentScheduleId ), $objDatabase );
	}

	public static function fetchCallAgentScheduleDetailsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentScheduleDetails( sprintf( 'SELECT * FROM call_agent_schedule_details WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentScheduleDetailsByCallAgentStatusTypeId( $intCallAgentStatusTypeId, $objDatabase ) {
		return self::fetchCallAgentScheduleDetails( sprintf( 'SELECT * FROM call_agent_schedule_details WHERE call_agent_status_type_id = %d', ( int ) $intCallAgentStatusTypeId ), $objDatabase );
	}

}
?>