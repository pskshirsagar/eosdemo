<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentSkills
 * Do not add any new functions to this class.
 */

class CBaseCallAgentSkills extends CEosPluralBase {

	/**
	 * @return CCallAgentSkill[]
	 */
	public static function fetchCallAgentSkills( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentSkill', $objDatabase );
	}

	/**
	 * @return CCallAgentSkill
	 */
	public static function fetchCallAgentSkill( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentSkill', $objDatabase );
	}

	public static function fetchCallAgentSkillCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_skills', $objDatabase );
	}

	public static function fetchCallAgentSkillById( $intId, $objDatabase ) {
		return self::fetchCallAgentSkill( sprintf( 'SELECT * FROM call_agent_skills WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentSkillsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentSkills( sprintf( 'SELECT * FROM call_agent_skills WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentSkillsByCallSkillId( $intCallSkillId, $objDatabase ) {
		return self::fetchCallAgentSkills( sprintf( 'SELECT * FROM call_agent_skills WHERE call_skill_id = %d', ( int ) $intCallSkillId ), $objDatabase );
	}

}
?>