<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CGreetings
 * Do not add any new functions to this class.
 */

class CBaseGreetings extends CEosPluralBase {

	/**
	 * @return CGreeting[]
	 */
	public static function fetchGreetings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGreeting', $objDatabase );
	}

	/**
	 * @return CGreeting
	 */
	public static function fetchGreeting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGreeting', $objDatabase );
	}

	public static function fetchGreetingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'greetings', $objDatabase );
	}

	public static function fetchGreetingById( $intId, $objDatabase ) {
		return self::fetchGreeting( sprintf( 'SELECT * FROM greetings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGreetingsByCid( $intCid, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchGreetingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchGreetingsByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

	public static function fetchGreetingsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchGreetingsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchGreetingsByPhraseId( $intPhraseId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE phrase_id = %d', ( int ) $intPhraseId ), $objDatabase );
	}

	public static function fetchGreetingsByGreetingTypeId( $intGreetingTypeId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE greeting_type_id = %d', ( int ) $intGreetingTypeId ), $objDatabase );
	}

	public static function fetchGreetingsByCallFileId( $intCallFileId, $objDatabase ) {
		return self::fetchGreetings( sprintf( 'SELECT * FROM greetings WHERE call_file_id = %d', ( int ) $intCallFileId ), $objDatabase );
	}

}
?>