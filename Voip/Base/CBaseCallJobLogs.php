<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallJobLogs
 * Do not add any new functions to this class.
 */

class CBaseCallJobLogs extends CEosPluralBase {

	public static function fetchCallJobLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallJobLog', $objDatabase );
	}

	public static function fetchCallJobLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallJobLog', $objDatabase );
	}

	public static function fetchCallJobLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_job_logs', $objDatabase );
	}

	public static function fetchCallJobLogById( $intId, $objDatabase ) {
		return self::fetchCallJobLog( sprintf( 'SELECT * FROM call_job_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallJobLogsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallJobLogs( sprintf( 'SELECT * FROM call_job_logs WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallJobLogsByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchCallJobLogs( sprintf( 'SELECT * FROM call_job_logs WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

}
?>