<?php

class CBaseCallAnalysisCallDisputeCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.call_analysis_call_dispute_categories';

	protected $m_intId;
	protected $m_intCallAnalysisCallDisputeId;
	protected $m_intCallAnalysisCategoryId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_analysis_call_dispute_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisCallDisputeId', trim( $arrValues['call_analysis_call_dispute_id'] ) ); elseif( isset( $arrValues['call_analysis_call_dispute_id'] ) ) $this->setCallAnalysisCallDisputeId( $arrValues['call_analysis_call_dispute_id'] );
		if( isset( $arrValues['call_analysis_category_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisCategoryId', trim( $arrValues['call_analysis_category_id'] ) ); elseif( isset( $arrValues['call_analysis_category_id'] ) ) $this->setCallAnalysisCategoryId( $arrValues['call_analysis_category_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAnalysisCallDisputeId( $intCallAnalysisCallDisputeId ) {
		$this->set( 'm_intCallAnalysisCallDisputeId', CStrings::strToIntDef( $intCallAnalysisCallDisputeId, NULL, false ) );
	}

	public function getCallAnalysisCallDisputeId() {
		return $this->m_intCallAnalysisCallDisputeId;
	}

	public function sqlCallAnalysisCallDisputeId() {
		return ( true == isset( $this->m_intCallAnalysisCallDisputeId ) ) ? ( string ) $this->m_intCallAnalysisCallDisputeId : 'NULL';
	}

	public function setCallAnalysisCategoryId( $intCallAnalysisCategoryId ) {
		$this->set( 'm_intCallAnalysisCategoryId', CStrings::strToIntDef( $intCallAnalysisCategoryId, NULL, false ) );
	}

	public function getCallAnalysisCategoryId() {
		return $this->m_intCallAnalysisCategoryId;
	}

	public function sqlCallAnalysisCategoryId() {
		return ( true == isset( $this->m_intCallAnalysisCategoryId ) ) ? ( string ) $this->m_intCallAnalysisCategoryId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_analysis_call_dispute_id, call_analysis_category_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAnalysisCallDisputeId() . ', ' .
						$this->sqlCallAnalysisCategoryId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_call_dispute_id = ' . $this->sqlCallAnalysisCallDisputeId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisCallDisputeId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_call_dispute_id = ' . $this->sqlCallAnalysisCallDisputeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_category_id = ' . $this->sqlCallAnalysisCategoryId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_category_id = ' . $this->sqlCallAnalysisCategoryId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_analysis_call_dispute_id' => $this->getCallAnalysisCallDisputeId(),
			'call_analysis_category_id' => $this->getCallAnalysisCategoryId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>