<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentPreferences
 * Do not add any new functions to this class.
 */

class CBaseCallAgentPreferences extends CEosPluralBase {

	/**
	 * @return CCallAgentPreference[]
	 */
	public static function fetchCallAgentPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentPreference::class, $objDatabase );
	}

	/**
	 * @return CCallAgentPreference
	 */
	public static function fetchCallAgentPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentPreference::class, $objDatabase );
	}

	public static function fetchCallAgentPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_preferences', $objDatabase );
	}

	public static function fetchCallAgentPreferenceById( $intId, $objDatabase ) {
		return self::fetchCallAgentPreference( sprintf( 'SELECT * FROM call_agent_preferences WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentPreferencesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCallAgentPreferences( sprintf( 'SELECT * FROM call_agent_preferences WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallAgentPreferencesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentPreferences( sprintf( 'SELECT * FROM call_agent_preferences WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

}
?>