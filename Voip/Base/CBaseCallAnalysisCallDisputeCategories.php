<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCallDisputeCategories
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisCallDisputeCategories extends CEosPluralBase {

	/**
	 * @return CCallAnalysisCallDisputeCategory[]
	 */
	public static function fetchCallAnalysisCallDisputeCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisCallDisputeCategory::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisCallDisputeCategory
	 */
	public static function fetchCallAnalysisCallDisputeCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisCallDisputeCategory::class, $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_call_dispute_categories', $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeCategoryById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputeCategory( sprintf( 'SELECT * FROM call_analysis_call_dispute_categories WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeCategoriesByCallAnalysisCallDisputeId( $intCallAnalysisCallDisputeId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputeCategories( sprintf( 'SELECT * FROM call_analysis_call_dispute_categories WHERE call_analysis_call_dispute_id = %d', $intCallAnalysisCallDisputeId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeCategoriesByCallAnalysisCategoryId( $intCallAnalysisCategoryId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputeCategories( sprintf( 'SELECT * FROM call_analysis_call_dispute_categories WHERE call_analysis_category_id = %d', $intCallAnalysisCategoryId ), $objDatabase );
	}

}
?>