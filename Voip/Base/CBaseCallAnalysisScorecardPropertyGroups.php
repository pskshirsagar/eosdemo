<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisScorecardPropertyGroups
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisScorecardPropertyGroups extends CEosPluralBase {

	/**
	 * @return CCallAnalysisScorecardPropertyGroup[]
	 */
	public static function fetchCallAnalysisScorecardPropertyGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAnalysisScorecardPropertyGroup', $objDatabase );
	}

	/**
	 * @return CCallAnalysisScorecardPropertyGroup
	 */
	public static function fetchCallAnalysisScorecardPropertyGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAnalysisScorecardPropertyGroup', $objDatabase );
	}

	public static function fetchCallAnalysisScorecardPropertyGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_scorecard_property_groups', $objDatabase );
	}

	public static function fetchCallAnalysisScorecardPropertyGroupById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisScorecardPropertyGroup( sprintf( 'SELECT * FROM call_analysis_scorecard_property_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisScorecardPropertyGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisScorecardPropertyGroups( sprintf( 'SELECT * FROM call_analysis_scorecard_property_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisScorecardPropertyGroupsByCallAnalysisScorecardId( $intCallAnalysisScorecardId, $objDatabase ) {
		return self::fetchCallAnalysisScorecardPropertyGroups( sprintf( 'SELECT * FROM call_analysis_scorecard_property_groups WHERE call_analysis_scorecard_id = %d', ( int ) $intCallAnalysisScorecardId ), $objDatabase );
	}

	public static function fetchCallAnalysisScorecardPropertyGroupsByPropertyGroupId( $intPropertyGroupId, $objDatabase ) {
		return self::fetchCallAnalysisScorecardPropertyGroups( sprintf( 'SELECT * FROM call_analysis_scorecard_property_groups WHERE property_group_id = %d', ( int ) $intPropertyGroupId ), $objDatabase );
	}

}
?>