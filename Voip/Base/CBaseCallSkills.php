<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSkills
 * Do not add any new functions to this class.
 */

class CBaseCallSkills extends CEosPluralBase {

	/**
	 * @return CCallSkill[]
	 */
	public static function fetchCallSkills( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallSkill::class, $objDatabase );
	}

	/**
	 * @return CCallSkill
	 */
	public static function fetchCallSkill( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallSkill::class, $objDatabase );
	}

	public static function fetchCallSkillCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_skills', $objDatabase );
	}

	public static function fetchCallSkillById( $intId, $objDatabase ) {
		return self::fetchCallSkill( sprintf( 'SELECT * FROM call_skills WHERE id = %d', $intId ), $objDatabase );
	}

}
?>