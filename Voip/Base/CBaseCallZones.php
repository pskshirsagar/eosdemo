<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallZones
 * Do not add any new functions to this class.
 */

class CBaseCallZones extends CEosPluralBase {

	/**
	 * @return CCallZone[]
	 */
	public static function fetchCallZones( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallZone::class, $objDatabase );
	}

	/**
	 * @return CCallZone
	 */
	public static function fetchCallZone( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallZone::class, $objDatabase );
	}

	public static function fetchCallZoneCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_zones', $objDatabase );
	}

	public static function fetchCallZoneById( $intId, $objDatabase ) {
		return self::fetchCallZone( sprintf( 'SELECT * FROM call_zones WHERE id = %d', $intId ), $objDatabase );
	}

}
?>