<?php

class CBaseIvrOfficeHour extends CEosSingularBase {

	const TABLE_NAME = 'public.ivr_office_hours';

	protected $m_intId;
	protected $m_intIvrId;
	protected $m_strOpenTime;
	protected $m_strCloseTime;
	protected $m_intWeekDay;
	protected $m_boolIsWeekend;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsWeekend = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ivr_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrId', trim( $arrValues['ivr_id'] ) ); elseif( isset( $arrValues['ivr_id'] ) ) $this->setIvrId( $arrValues['ivr_id'] );
		if( isset( $arrValues['open_time'] ) && $boolDirectSet ) $this->set( 'm_strOpenTime', trim( $arrValues['open_time'] ) ); elseif( isset( $arrValues['open_time'] ) ) $this->setOpenTime( $arrValues['open_time'] );
		if( isset( $arrValues['close_time'] ) && $boolDirectSet ) $this->set( 'm_strCloseTime', trim( $arrValues['close_time'] ) ); elseif( isset( $arrValues['close_time'] ) ) $this->setCloseTime( $arrValues['close_time'] );
		if( isset( $arrValues['week_day'] ) && $boolDirectSet ) $this->set( 'm_intWeekDay', trim( $arrValues['week_day'] ) ); elseif( isset( $arrValues['week_day'] ) ) $this->setWeekDay( $arrValues['week_day'] );
		if( isset( $arrValues['is_weekend'] ) && $boolDirectSet ) $this->set( 'm_boolIsWeekend', trim( stripcslashes( $arrValues['is_weekend'] ) ) ); elseif( isset( $arrValues['is_weekend'] ) ) $this->setIsWeekend( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_weekend'] ) : $arrValues['is_weekend'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIvrId( $intIvrId ) {
		$this->set( 'm_intIvrId', CStrings::strToIntDef( $intIvrId, NULL, false ) );
	}

	public function getIvrId() {
		return $this->m_intIvrId;
	}

	public function sqlIvrId() {
		return ( true == isset( $this->m_intIvrId ) ) ? ( string ) $this->m_intIvrId : 'NULL';
	}

	public function setOpenTime( $strOpenTime ) {
		$this->set( 'm_strOpenTime', CStrings::strTrimDef( $strOpenTime, -1, NULL, true ) );
	}

	public function getOpenTime() {
		return $this->m_strOpenTime;
	}

	public function sqlOpenTime() {
		return ( true == isset( $this->m_strOpenTime ) ) ? '\'' . $this->m_strOpenTime . '\'' : 'NULL';
	}

	public function setCloseTime( $strCloseTime ) {
		$this->set( 'm_strCloseTime', CStrings::strTrimDef( $strCloseTime, -1, NULL, true ) );
	}

	public function getCloseTime() {
		return $this->m_strCloseTime;
	}

	public function sqlCloseTime() {
		return ( true == isset( $this->m_strCloseTime ) ) ? '\'' . $this->m_strCloseTime . '\'' : 'NULL';
	}

	public function setWeekDay( $intWeekDay ) {
		$this->set( 'm_intWeekDay', CStrings::strToIntDef( $intWeekDay, NULL, false ) );
	}

	public function getWeekDay() {
		return $this->m_intWeekDay;
	}

	public function sqlWeekDay() {
		return ( true == isset( $this->m_intWeekDay ) ) ? ( string ) $this->m_intWeekDay : 'NULL';
	}

	public function setIsWeekend( $boolIsWeekend ) {
		$this->set( 'm_boolIsWeekend', CStrings::strToBool( $boolIsWeekend ) );
	}

	public function getIsWeekend() {
		return $this->m_boolIsWeekend;
	}

	public function sqlIsWeekend() {
		return ( true == isset( $this->m_boolIsWeekend ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsWeekend ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ivr_id, open_time, close_time, week_day, is_weekend, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlIvrId() . ', ' .
 						$this->sqlOpenTime() . ', ' .
 						$this->sqlCloseTime() . ', ' .
 						$this->sqlWeekDay() . ', ' .
 						$this->sqlIsWeekend() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId() . ','; } elseif( true == array_key_exists( 'IvrId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_time = ' . $this->sqlOpenTime() . ','; } elseif( true == array_key_exists( 'OpenTime', $this->getChangedColumns() ) ) { $strSql .= ' open_time = ' . $this->sqlOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_time = ' . $this->sqlCloseTime() . ','; } elseif( true == array_key_exists( 'CloseTime', $this->getChangedColumns() ) ) { $strSql .= ' close_time = ' . $this->sqlCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week_day = ' . $this->sqlWeekDay() . ','; } elseif( true == array_key_exists( 'WeekDay', $this->getChangedColumns() ) ) { $strSql .= ' week_day = ' . $this->sqlWeekDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_weekend = ' . $this->sqlIsWeekend() . ','; } elseif( true == array_key_exists( 'IsWeekend', $this->getChangedColumns() ) ) { $strSql .= ' is_weekend = ' . $this->sqlIsWeekend() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ivr_id' => $this->getIvrId(),
			'open_time' => $this->getOpenTime(),
			'close_time' => $this->getCloseTime(),
			'week_day' => $this->getWeekDay(),
			'is_weekend' => $this->getIsWeekend(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>