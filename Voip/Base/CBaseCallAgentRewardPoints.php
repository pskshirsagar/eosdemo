<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentRewardPoints
 * Do not add any new functions to this class.
 */

class CBaseCallAgentRewardPoints extends CEosPluralBase {

	/**
	 * @return CCallAgentRewardPoint[]
	 */
	public static function fetchCallAgentRewardPoints( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentRewardPoint::class, $objDatabase );
	}

	/**
	 * @return CCallAgentRewardPoint
	 */
	public static function fetchCallAgentRewardPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentRewardPoint::class, $objDatabase );
	}

	public static function fetchCallAgentRewardPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_reward_points', $objDatabase );
	}

	public static function fetchCallAgentRewardPointById( $intId, $objDatabase ) {
		return self::fetchCallAgentRewardPoint( sprintf( 'SELECT * FROM call_agent_reward_points WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentRewardPoints( sprintf( 'SELECT * FROM call_agent_reward_points WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointsByCallAgentRewardPointLogId( $intCallAgentRewardPointLogId, $objDatabase ) {
		return self::fetchCallAgentRewardPoints( sprintf( 'SELECT * FROM call_agent_reward_points WHERE call_agent_reward_point_log_id = %d', $intCallAgentRewardPointLogId ), $objDatabase );
	}

}
?>