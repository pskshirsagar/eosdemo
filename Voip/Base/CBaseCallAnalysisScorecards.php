<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisScorecards
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisScorecards extends CEosPluralBase {

	/**
	 * @return CCallAnalysisScorecard[]
	 */
	public static function fetchCallAnalysisScorecards( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisScorecard::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisScorecard
	 */
	public static function fetchCallAnalysisScorecard( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisScorecard::class, $objDatabase );
	}

	public static function fetchCallAnalysisScorecardCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_scorecards', $objDatabase );
	}

	public static function fetchCallAnalysisScorecardById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisScorecard( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisScorecardsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisScorecards( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisScorecardsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisScorecards( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

}
?>