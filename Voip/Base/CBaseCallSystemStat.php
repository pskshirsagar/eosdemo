<?php

class CBaseCallSystemStat extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_system_stats';

	protected $m_intId;
	protected $m_intCallServerId;
	protected $m_intSessionCount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strFreeswitchIp;
	protected $m_fltIdleCpu;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_server_id'] ) && $boolDirectSet ) $this->set( 'm_intCallServerId', trim( $arrValues['call_server_id'] ) ); elseif( isset( $arrValues['call_server_id'] ) ) $this->setCallServerId( $arrValues['call_server_id'] );
		if( isset( $arrValues['session_count'] ) && $boolDirectSet ) $this->set( 'm_intSessionCount', trim( $arrValues['session_count'] ) ); elseif( isset( $arrValues['session_count'] ) ) $this->setSessionCount( $arrValues['session_count'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['freeswitch_ip'] ) && $boolDirectSet ) $this->set( 'm_strFreeswitchIp', trim( $arrValues['freeswitch_ip'] ) ); elseif( isset( $arrValues['freeswitch_ip'] ) ) $this->setFreeswitchIp( $arrValues['freeswitch_ip'] );
		if( isset( $arrValues['idle_cpu'] ) && $boolDirectSet ) $this->set( 'm_fltIdleCpu', trim( $arrValues['idle_cpu'] ) ); elseif( isset( $arrValues['idle_cpu'] ) ) $this->setIdleCpu( $arrValues['idle_cpu'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallServerId( $intCallServerId ) {
		$this->set( 'm_intCallServerId', CStrings::strToIntDef( $intCallServerId, NULL, false ) );
	}

	public function getCallServerId() {
		return $this->m_intCallServerId;
	}

	public function sqlCallServerId() {
		return ( true == isset( $this->m_intCallServerId ) ) ? ( string ) $this->m_intCallServerId : 'NULL';
	}

	public function setSessionCount( $intSessionCount ) {
		$this->set( 'm_intSessionCount', CStrings::strToIntDef( $intSessionCount, NULL, false ) );
	}

	public function getSessionCount() {
		return $this->m_intSessionCount;
	}

	public function sqlSessionCount() {
		return ( true == isset( $this->m_intSessionCount ) ) ? ( string ) $this->m_intSessionCount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setFreeswitchIp( $strFreeswitchIp ) {
		$this->set( 'm_strFreeswitchIp', CStrings::strTrimDef( $strFreeswitchIp, 20, NULL, true ) );
	}

	public function getFreeswitchIp() {
		return $this->m_strFreeswitchIp;
	}

	public function sqlFreeswitchIp() {
		return ( true == isset( $this->m_strFreeswitchIp ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFreeswitchIp ) : '\'' . addslashes( $this->m_strFreeswitchIp ) . '\'' ) : 'NULL';
	}

	public function setIdleCpu( $fltIdleCpu ) {
		$this->set( 'm_fltIdleCpu', CStrings::strToFloatDef( $fltIdleCpu, NULL, false, 0 ) );
	}

	public function getIdleCpu() {
		return $this->m_fltIdleCpu;
	}

	public function sqlIdleCpu() {
		return ( true == isset( $this->m_fltIdleCpu ) ) ? ( string ) $this->m_fltIdleCpu : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_server_id, session_count, details, created_by, created_on, freeswitch_ip, idle_cpu )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCallServerId() . ', ' .
		          $this->sqlSessionCount() . ', ' .
		          $this->sqlDetails() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlFreeswitchIp() . ', ' .
		          $this->sqlIdleCpu() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_server_id = ' . $this->sqlCallServerId(). ',' ; } elseif( true == array_key_exists( 'CallServerId', $this->getChangedColumns() ) ) { $strSql .= ' call_server_id = ' . $this->sqlCallServerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_count = ' . $this->sqlSessionCount(). ',' ; } elseif( true == array_key_exists( 'SessionCount', $this->getChangedColumns() ) ) { $strSql .= ' session_count = ' . $this->sqlSessionCount() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' freeswitch_ip = ' . $this->sqlFreeswitchIp(). ',' ; } elseif( true == array_key_exists( 'FreeswitchIp', $this->getChangedColumns() ) ) { $strSql .= ' freeswitch_ip = ' . $this->sqlFreeswitchIp() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' idle_cpu = ' . $this->sqlIdleCpu() ; } elseif( true == array_key_exists( 'IdleCpu', $this->getChangedColumns() ) ) { $strSql .= ' idle_cpu = ' . $this->sqlIdleCpu() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_server_id' => $this->getCallServerId(),
			'session_count' => $this->getSessionCount(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'freeswitch_ip' => $this->getFreeswitchIp(),
			'idle_cpu' => $this->getIdleCpu()
		);
	}

}
?>