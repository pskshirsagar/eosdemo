<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSkillQueues
 * Do not add any new functions to this class.
 */

class CBaseCallSkillQueues extends CEosPluralBase {

	/**
	 * @return CCallSkillQueue[]
	 */
	public static function fetchCallSkillQueues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallSkillQueue', $objDatabase );
	}

	/**
	 * @return CCallSkillQueue
	 */
	public static function fetchCallSkillQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallSkillQueue', $objDatabase );
	}

	public static function fetchCallSkillQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_skill_queues', $objDatabase );
	}

	public static function fetchCallSkillQueueById( $intId, $objDatabase ) {
		return self::fetchCallSkillQueue( sprintf( 'SELECT * FROM call_skill_queues WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallSkillQueuesByCallSkillId( $intCallSkillId, $objDatabase ) {
		return self::fetchCallSkillQueues( sprintf( 'SELECT * FROM call_skill_queues WHERE call_skill_id = %d', ( int ) $intCallSkillId ), $objDatabase );
	}

	public static function fetchCallSkillQueuesByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallSkillQueues( sprintf( 'SELECT * FROM call_skill_queues WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

}
?>