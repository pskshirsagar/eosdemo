<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyIvrs
 * Do not add any new functions to this class.
 */

class CBasePropertyIvrs extends CEosPluralBase {

	/**
	 * @return CPropertyIvr[]
	 */
	public static function fetchPropertyIvrs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyIvr', $objDatabase );
	}

	/**
	 * @return CPropertyIvr
	 */
	public static function fetchPropertyIvr( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyIvr', $objDatabase );
	}

	public static function fetchPropertyIvrCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_ivrs', $objDatabase );
	}

	public static function fetchPropertyIvrById( $intId, $objDatabase ) {
		return self::fetchPropertyIvr( sprintf( 'SELECT * FROM property_ivrs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyIvrsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyIvrs( sprintf( 'SELECT * FROM property_ivrs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyIvrsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyIvrs( sprintf( 'SELECT * FROM property_ivrs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchPropertyIvrsByIvrId( $intIvrId, $objDatabase ) {
		return self::fetchPropertyIvrs( sprintf( 'SELECT * FROM property_ivrs WHERE ivr_id = %d', ( int ) $intIvrId ), $objDatabase );
	}

}
?>