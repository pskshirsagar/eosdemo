<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallEvents
 * Do not add any new functions to this class.
 */

class CBaseCallEvents extends CEosPluralBase {

	/**
	 * @return CCallEvent[]
	 */
	public static function fetchCallEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallEvent', $objDatabase );
	}

	/**
	 * @return CCallEvent
	 */
	public static function fetchCallEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallEvent', $objDatabase );
	}

	public static function fetchCallEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_events', $objDatabase );
	}

	public static function fetchCallEventById( $intId, $objDatabase ) {
		return self::fetchCallEvent( sprintf( 'SELECT * FROM call_events WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallEventsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallEvents( sprintf( 'SELECT * FROM call_events WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallEventsByCallEventTypeId( $intCallEventTypeId, $objDatabase ) {
		return self::fetchCallEvents( sprintf( 'SELECT * FROM call_events WHERE call_event_type_id = %d', ( int ) $intCallEventTypeId ), $objDatabase );
	}

}
?>