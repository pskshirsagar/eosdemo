<?php

class CBaseCallJobLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_job_logs';

	protected $m_intId;
	protected $m_intCallId;
	protected $m_intClusterId;
	protected $m_strMessage;
	protected $m_intLockSequenceNumber;
	protected $m_strLockedOn;
	protected $m_strSentOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['lock_sequence_number'] ) && $boolDirectSet ) $this->set( 'm_intLockSequenceNumber', trim( $arrValues['lock_sequence_number'] ) ); elseif( isset( $arrValues['lock_sequence_number'] ) ) $this->setLockSequenceNumber( $arrValues['lock_sequence_number'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, 1024, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setLockSequenceNumber( $intLockSequenceNumber ) {
		$this->set( 'm_intLockSequenceNumber', CStrings::strToIntDef( $intLockSequenceNumber, NULL, false ) );
	}

	public function getLockSequenceNumber() {
		return $this->m_intLockSequenceNumber;
	}

	public function sqlLockSequenceNumber() {
		return ( true == isset( $this->m_intLockSequenceNumber ) ) ? ( string ) $this->m_intLockSequenceNumber : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_id, cluster_id, message, lock_sequence_number, locked_on, sent_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlLockSequenceNumber() . ', ' .
 						$this->sqlLockedOn() . ', ' .
 						$this->sqlSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; } elseif( true == array_key_exists( 'LockSequenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence_number = ' . $this->sqlLockSequenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_id' => $this->getCallId(),
			'cluster_id' => $this->getClusterId(),
			'message' => $this->getMessage(),
			'lock_sequence_number' => $this->getLockSequenceNumber(),
			'locked_on' => $this->getLockedOn(),
			'sent_on' => $this->getSentOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>