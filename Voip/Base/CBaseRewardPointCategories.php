<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRewardPointCategories
 * Do not add any new functions to this class.
 */

class CBaseRewardPointCategories extends CEosPluralBase {

	/**
	 * @return CRewardPointCategory[]
	 */
	public static function fetchRewardPointCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRewardPointCategory::class, $objDatabase );
	}

	/**
	 * @return CRewardPointCategory
	 */
	public static function fetchRewardPointCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRewardPointCategory::class, $objDatabase );
	}

	public static function fetchRewardPointCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reward_point_categories', $objDatabase );
	}

	public static function fetchRewardPointCategoryById( $intId, $objDatabase ) {
		return self::fetchRewardPointCategory( sprintf( 'SELECT * FROM reward_point_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>