<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProducts
 * Do not add any new functions to this class.
 */

class CBaseStoreProducts extends CEosPluralBase {

	/**
	 * @return CStoreProduct[]
	 */
	public static function fetchStoreProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProduct::class, $objDatabase );
	}

	/**
	 * @return CStoreProduct
	 */
	public static function fetchStoreProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProduct::class, $objDatabase );
	}

	public static function fetchStoreProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_products', $objDatabase );
	}

	public static function fetchStoreProductById( $intId, $objDatabase ) {
		return self::fetchStoreProduct( sprintf( 'SELECT * FROM store_products WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchStoreProductsByStoreProductTypeId( $intStoreProductTypeId, $objDatabase ) {
		return self::fetchStoreProducts( sprintf( 'SELECT * FROM store_products WHERE store_product_type_id = %d', $intStoreProductTypeId ), $objDatabase );
	}

	public static function fetchStoreProductsByFrequencyId( $intFrequencyId, $objDatabase ) {
		return self::fetchStoreProducts( sprintf( 'SELECT * FROM store_products WHERE frequency_id = %d', $intFrequencyId ), $objDatabase );
	}

}
?>