<?php

class CBaseCallServerCommand extends CEosSingularBase {

	const TABLE_NAME = 'public.call_server_commands';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCommand;
	protected $m_strArguments;
	protected $m_boolIsExecutionAllowed;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['command'] ) && $boolDirectSet ) $this->set( 'm_strCommand', trim( $arrValues['command'] ) ); elseif( isset( $arrValues['command'] ) ) $this->setCommand( $arrValues['command'] );
		if( isset( $arrValues['arguments'] ) && $boolDirectSet ) $this->set( 'm_strArguments', trim( $arrValues['arguments'] ) ); elseif( isset( $arrValues['arguments'] ) ) $this->setArguments( $arrValues['arguments'] );
		if( isset( $arrValues['is_execution_allowed'] ) && $boolDirectSet ) $this->set( 'm_boolIsExecutionAllowed', trim( stripcslashes( $arrValues['is_execution_allowed'] ) ) ); elseif( isset( $arrValues['is_execution_allowed'] ) ) $this->setIsExecutionAllowed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_execution_allowed'] ) : $arrValues['is_execution_allowed'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setCommand( $strCommand ) {
		$this->set( 'm_strCommand', CStrings::strTrimDef( $strCommand, -1, NULL, true ) );
	}

	public function getCommand() {
		return $this->m_strCommand;
	}

	public function sqlCommand() {
		return ( true == isset( $this->m_strCommand ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCommand ) : '\'' . addslashes( $this->m_strCommand ) . '\'' ) : 'NULL';
	}

	public function setArguments( $strArguments ) {
		$this->set( 'm_strArguments', CStrings::strTrimDef( $strArguments, -1, NULL, true ) );
	}

	public function getArguments() {
		return $this->m_strArguments;
	}

	public function sqlArguments() {
		return ( true == isset( $this->m_strArguments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strArguments ) : '\'' . addslashes( $this->m_strArguments ) . '\'' ) : 'NULL';
	}

	public function setIsExecutionAllowed( $boolIsExecutionAllowed ) {
		$this->set( 'm_boolIsExecutionAllowed', CStrings::strToBool( $boolIsExecutionAllowed ) );
	}

	public function getIsExecutionAllowed() {
		return $this->m_boolIsExecutionAllowed;
	}

	public function sqlIsExecutionAllowed() {
		return ( true == isset( $this->m_boolIsExecutionAllowed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExecutionAllowed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'command' => $this->getCommand(),
			'arguments' => $this->getArguments(),
			'is_execution_allowed' => $this->getIsExecutionAllowed(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>