<?php

class CBaseCallWordCorrelation extends CEosSingularBase {

	const TABLE_NAME = 'public.call_word_correlations';

	protected $m_intId;
	protected $m_strSpeaker;
	protected $m_strWord;
	protected $m_fltCallIsAbandonedCc;
	protected $m_fltCallIsLeadCc;
	protected $m_fltCallIsMarketSurveyCc;
	protected $m_fltCallIsResidentCc;
	protected $m_fltCallIsSolicitorCc;
	protected $m_fltCallIsSupportCc;
	protected $m_fltCallIsVendorCc;
	protected $m_fltCallIsWorkOrderCc;
	protected $m_fltCallIsWrongNumberCc;
	protected $m_fltCallAverageCc;
	protected $m_fltLeadIsConvertedCc;
	protected $m_intCallIsAbandonedCcRank;
	protected $m_intCallIsLeadCcRank;
	protected $m_intCallIsMarketSurveyCcRank;
	protected $m_intCallIsResidentCcRank;
	protected $m_intCallIsSolicitorCcRank;
	protected $m_intCallIsSupportCcRank;
	protected $m_intCallIsVendorCcRank;
	protected $m_intCallIsWorkOrderCcRank;
	protected $m_intCallIsWrongNumberCcRank;
	protected $m_intCallAverageCcRank;
	protected $m_intLeadIsConvertedCcRank;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['speaker'] ) && $boolDirectSet ) $this->set( 'm_strSpeaker', trim( stripcslashes( $arrValues['speaker'] ) ) ); elseif( isset( $arrValues['speaker'] ) ) $this->setSpeaker( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['speaker'] ) : $arrValues['speaker'] );
		if( isset( $arrValues['word'] ) && $boolDirectSet ) $this->set( 'm_strWord', trim( stripcslashes( $arrValues['word'] ) ) ); elseif( isset( $arrValues['word'] ) ) $this->setWord( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['word'] ) : $arrValues['word'] );
		if( isset( $arrValues['call_is_abandoned_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsAbandonedCc', trim( $arrValues['call_is_abandoned_cc'] ) ); elseif( isset( $arrValues['call_is_abandoned_cc'] ) ) $this->setCallIsAbandonedCc( $arrValues['call_is_abandoned_cc'] );
		if( isset( $arrValues['call_is_lead_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsLeadCc', trim( $arrValues['call_is_lead_cc'] ) ); elseif( isset( $arrValues['call_is_lead_cc'] ) ) $this->setCallIsLeadCc( $arrValues['call_is_lead_cc'] );
		if( isset( $arrValues['call_is_market_survey_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsMarketSurveyCc', trim( $arrValues['call_is_market_survey_cc'] ) ); elseif( isset( $arrValues['call_is_market_survey_cc'] ) ) $this->setCallIsMarketSurveyCc( $arrValues['call_is_market_survey_cc'] );
		if( isset( $arrValues['call_is_resident_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsResidentCc', trim( $arrValues['call_is_resident_cc'] ) ); elseif( isset( $arrValues['call_is_resident_cc'] ) ) $this->setCallIsResidentCc( $arrValues['call_is_resident_cc'] );
		if( isset( $arrValues['call_is_solicitor_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsSolicitorCc', trim( $arrValues['call_is_solicitor_cc'] ) ); elseif( isset( $arrValues['call_is_solicitor_cc'] ) ) $this->setCallIsSolicitorCc( $arrValues['call_is_solicitor_cc'] );
		if( isset( $arrValues['call_is_support_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsSupportCc', trim( $arrValues['call_is_support_cc'] ) ); elseif( isset( $arrValues['call_is_support_cc'] ) ) $this->setCallIsSupportCc( $arrValues['call_is_support_cc'] );
		if( isset( $arrValues['call_is_vendor_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsVendorCc', trim( $arrValues['call_is_vendor_cc'] ) ); elseif( isset( $arrValues['call_is_vendor_cc'] ) ) $this->setCallIsVendorCc( $arrValues['call_is_vendor_cc'] );
		if( isset( $arrValues['call_is_work_order_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsWorkOrderCc', trim( $arrValues['call_is_work_order_cc'] ) ); elseif( isset( $arrValues['call_is_work_order_cc'] ) ) $this->setCallIsWorkOrderCc( $arrValues['call_is_work_order_cc'] );
		if( isset( $arrValues['call_is_wrong_number_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallIsWrongNumberCc', trim( $arrValues['call_is_wrong_number_cc'] ) ); elseif( isset( $arrValues['call_is_wrong_number_cc'] ) ) $this->setCallIsWrongNumberCc( $arrValues['call_is_wrong_number_cc'] );
		if( isset( $arrValues['call_average_cc'] ) && $boolDirectSet ) $this->set( 'm_fltCallAverageCc', trim( $arrValues['call_average_cc'] ) ); elseif( isset( $arrValues['call_average_cc'] ) ) $this->setCallAverageCc( $arrValues['call_average_cc'] );
		if( isset( $arrValues['lead_is_converted_cc'] ) && $boolDirectSet ) $this->set( 'm_fltLeadIsConvertedCc', trim( $arrValues['lead_is_converted_cc'] ) ); elseif( isset( $arrValues['lead_is_converted_cc'] ) ) $this->setLeadIsConvertedCc( $arrValues['lead_is_converted_cc'] );
		if( isset( $arrValues['call_is_abandoned_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsAbandonedCcRank', trim( $arrValues['call_is_abandoned_cc_rank'] ) ); elseif( isset( $arrValues['call_is_abandoned_cc_rank'] ) ) $this->setCallIsAbandonedCcRank( $arrValues['call_is_abandoned_cc_rank'] );
		if( isset( $arrValues['call_is_lead_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsLeadCcRank', trim( $arrValues['call_is_lead_cc_rank'] ) ); elseif( isset( $arrValues['call_is_lead_cc_rank'] ) ) $this->setCallIsLeadCcRank( $arrValues['call_is_lead_cc_rank'] );
		if( isset( $arrValues['call_is_market_survey_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsMarketSurveyCcRank', trim( $arrValues['call_is_market_survey_cc_rank'] ) ); elseif( isset( $arrValues['call_is_market_survey_cc_rank'] ) ) $this->setCallIsMarketSurveyCcRank( $arrValues['call_is_market_survey_cc_rank'] );
		if( isset( $arrValues['call_is_resident_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsResidentCcRank', trim( $arrValues['call_is_resident_cc_rank'] ) ); elseif( isset( $arrValues['call_is_resident_cc_rank'] ) ) $this->setCallIsResidentCcRank( $arrValues['call_is_resident_cc_rank'] );
		if( isset( $arrValues['call_is_solicitor_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsSolicitorCcRank', trim( $arrValues['call_is_solicitor_cc_rank'] ) ); elseif( isset( $arrValues['call_is_solicitor_cc_rank'] ) ) $this->setCallIsSolicitorCcRank( $arrValues['call_is_solicitor_cc_rank'] );
		if( isset( $arrValues['call_is_support_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsSupportCcRank', trim( $arrValues['call_is_support_cc_rank'] ) ); elseif( isset( $arrValues['call_is_support_cc_rank'] ) ) $this->setCallIsSupportCcRank( $arrValues['call_is_support_cc_rank'] );
		if( isset( $arrValues['call_is_vendor_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsVendorCcRank', trim( $arrValues['call_is_vendor_cc_rank'] ) ); elseif( isset( $arrValues['call_is_vendor_cc_rank'] ) ) $this->setCallIsVendorCcRank( $arrValues['call_is_vendor_cc_rank'] );
		if( isset( $arrValues['call_is_work_order_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsWorkOrderCcRank', trim( $arrValues['call_is_work_order_cc_rank'] ) ); elseif( isset( $arrValues['call_is_work_order_cc_rank'] ) ) $this->setCallIsWorkOrderCcRank( $arrValues['call_is_work_order_cc_rank'] );
		if( isset( $arrValues['call_is_wrong_number_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallIsWrongNumberCcRank', trim( $arrValues['call_is_wrong_number_cc_rank'] ) ); elseif( isset( $arrValues['call_is_wrong_number_cc_rank'] ) ) $this->setCallIsWrongNumberCcRank( $arrValues['call_is_wrong_number_cc_rank'] );
		if( isset( $arrValues['call_average_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intCallAverageCcRank', trim( $arrValues['call_average_cc_rank'] ) ); elseif( isset( $arrValues['call_average_cc_rank'] ) ) $this->setCallAverageCcRank( $arrValues['call_average_cc_rank'] );
		if( isset( $arrValues['lead_is_converted_cc_rank'] ) && $boolDirectSet ) $this->set( 'm_intLeadIsConvertedCcRank', trim( $arrValues['lead_is_converted_cc_rank'] ) ); elseif( isset( $arrValues['lead_is_converted_cc_rank'] ) ) $this->setLeadIsConvertedCcRank( $arrValues['lead_is_converted_cc_rank'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSpeaker( $strSpeaker ) {
		$this->set( 'm_strSpeaker', CStrings::strTrimDef( $strSpeaker, -1, NULL, true ) );
	}

	public function getSpeaker() {
		return $this->m_strSpeaker;
	}

	public function sqlSpeaker() {
		return ( true == isset( $this->m_strSpeaker ) ) ? '\'' . addslashes( $this->m_strSpeaker ) . '\'' : 'NULL';
	}

	public function setWord( $strWord ) {
		$this->set( 'm_strWord', CStrings::strTrimDef( $strWord, -1, NULL, true ) );
	}

	public function getWord() {
		return $this->m_strWord;
	}

	public function sqlWord() {
		return ( true == isset( $this->m_strWord ) ) ? '\'' . addslashes( $this->m_strWord ) . '\'' : 'NULL';
	}

	public function setCallIsAbandonedCc( $fltCallIsAbandonedCc ) {
		$this->set( 'm_fltCallIsAbandonedCc', CStrings::strToFloatDef( $fltCallIsAbandonedCc, NULL, false, 4 ) );
	}

	public function getCallIsAbandonedCc() {
		return $this->m_fltCallIsAbandonedCc;
	}

	public function sqlCallIsAbandonedCc() {
		return ( true == isset( $this->m_fltCallIsAbandonedCc ) ) ? ( string ) $this->m_fltCallIsAbandonedCc : 'NULL';
	}

	public function setCallIsLeadCc( $fltCallIsLeadCc ) {
		$this->set( 'm_fltCallIsLeadCc', CStrings::strToFloatDef( $fltCallIsLeadCc, NULL, false, 4 ) );
	}

	public function getCallIsLeadCc() {
		return $this->m_fltCallIsLeadCc;
	}

	public function sqlCallIsLeadCc() {
		return ( true == isset( $this->m_fltCallIsLeadCc ) ) ? ( string ) $this->m_fltCallIsLeadCc : 'NULL';
	}

	public function setCallIsMarketSurveyCc( $fltCallIsMarketSurveyCc ) {
		$this->set( 'm_fltCallIsMarketSurveyCc', CStrings::strToFloatDef( $fltCallIsMarketSurveyCc, NULL, false, 4 ) );
	}

	public function getCallIsMarketSurveyCc() {
		return $this->m_fltCallIsMarketSurveyCc;
	}

	public function sqlCallIsMarketSurveyCc() {
		return ( true == isset( $this->m_fltCallIsMarketSurveyCc ) ) ? ( string ) $this->m_fltCallIsMarketSurveyCc : 'NULL';
	}

	public function setCallIsResidentCc( $fltCallIsResidentCc ) {
		$this->set( 'm_fltCallIsResidentCc', CStrings::strToFloatDef( $fltCallIsResidentCc, NULL, false, 4 ) );
	}

	public function getCallIsResidentCc() {
		return $this->m_fltCallIsResidentCc;
	}

	public function sqlCallIsResidentCc() {
		return ( true == isset( $this->m_fltCallIsResidentCc ) ) ? ( string ) $this->m_fltCallIsResidentCc : 'NULL';
	}

	public function setCallIsSolicitorCc( $fltCallIsSolicitorCc ) {
		$this->set( 'm_fltCallIsSolicitorCc', CStrings::strToFloatDef( $fltCallIsSolicitorCc, NULL, false, 4 ) );
	}

	public function getCallIsSolicitorCc() {
		return $this->m_fltCallIsSolicitorCc;
	}

	public function sqlCallIsSolicitorCc() {
		return ( true == isset( $this->m_fltCallIsSolicitorCc ) ) ? ( string ) $this->m_fltCallIsSolicitorCc : 'NULL';
	}

	public function setCallIsSupportCc( $fltCallIsSupportCc ) {
		$this->set( 'm_fltCallIsSupportCc', CStrings::strToFloatDef( $fltCallIsSupportCc, NULL, false, 4 ) );
	}

	public function getCallIsSupportCc() {
		return $this->m_fltCallIsSupportCc;
	}

	public function sqlCallIsSupportCc() {
		return ( true == isset( $this->m_fltCallIsSupportCc ) ) ? ( string ) $this->m_fltCallIsSupportCc : 'NULL';
	}

	public function setCallIsVendorCc( $fltCallIsVendorCc ) {
		$this->set( 'm_fltCallIsVendorCc', CStrings::strToFloatDef( $fltCallIsVendorCc, NULL, false, 4 ) );
	}

	public function getCallIsVendorCc() {
		return $this->m_fltCallIsVendorCc;
	}

	public function sqlCallIsVendorCc() {
		return ( true == isset( $this->m_fltCallIsVendorCc ) ) ? ( string ) $this->m_fltCallIsVendorCc : 'NULL';
	}

	public function setCallIsWorkOrderCc( $fltCallIsWorkOrderCc ) {
		$this->set( 'm_fltCallIsWorkOrderCc', CStrings::strToFloatDef( $fltCallIsWorkOrderCc, NULL, false, 4 ) );
	}

	public function getCallIsWorkOrderCc() {
		return $this->m_fltCallIsWorkOrderCc;
	}

	public function sqlCallIsWorkOrderCc() {
		return ( true == isset( $this->m_fltCallIsWorkOrderCc ) ) ? ( string ) $this->m_fltCallIsWorkOrderCc : 'NULL';
	}

	public function setCallIsWrongNumberCc( $fltCallIsWrongNumberCc ) {
		$this->set( 'm_fltCallIsWrongNumberCc', CStrings::strToFloatDef( $fltCallIsWrongNumberCc, NULL, false, 4 ) );
	}

	public function getCallIsWrongNumberCc() {
		return $this->m_fltCallIsWrongNumberCc;
	}

	public function sqlCallIsWrongNumberCc() {
		return ( true == isset( $this->m_fltCallIsWrongNumberCc ) ) ? ( string ) $this->m_fltCallIsWrongNumberCc : 'NULL';
	}

	public function setCallAverageCc( $fltCallAverageCc ) {
		$this->set( 'm_fltCallAverageCc', CStrings::strToFloatDef( $fltCallAverageCc, NULL, false, 4 ) );
	}

	public function getCallAverageCc() {
		return $this->m_fltCallAverageCc;
	}

	public function sqlCallAverageCc() {
		return ( true == isset( $this->m_fltCallAverageCc ) ) ? ( string ) $this->m_fltCallAverageCc : 'NULL';
	}

	public function setLeadIsConvertedCc( $fltLeadIsConvertedCc ) {
		$this->set( 'm_fltLeadIsConvertedCc', CStrings::strToFloatDef( $fltLeadIsConvertedCc, NULL, false, 4 ) );
	}

	public function getLeadIsConvertedCc() {
		return $this->m_fltLeadIsConvertedCc;
	}

	public function sqlLeadIsConvertedCc() {
		return ( true == isset( $this->m_fltLeadIsConvertedCc ) ) ? ( string ) $this->m_fltLeadIsConvertedCc : 'NULL';
	}

	public function setCallIsAbandonedCcRank( $intCallIsAbandonedCcRank ) {
		$this->set( 'm_intCallIsAbandonedCcRank', CStrings::strToIntDef( $intCallIsAbandonedCcRank, NULL, false ) );
	}

	public function getCallIsAbandonedCcRank() {
		return $this->m_intCallIsAbandonedCcRank;
	}

	public function sqlCallIsAbandonedCcRank() {
		return ( true == isset( $this->m_intCallIsAbandonedCcRank ) ) ? ( string ) $this->m_intCallIsAbandonedCcRank : 'NULL';
	}

	public function setCallIsLeadCcRank( $intCallIsLeadCcRank ) {
		$this->set( 'm_intCallIsLeadCcRank', CStrings::strToIntDef( $intCallIsLeadCcRank, NULL, false ) );
	}

	public function getCallIsLeadCcRank() {
		return $this->m_intCallIsLeadCcRank;
	}

	public function sqlCallIsLeadCcRank() {
		return ( true == isset( $this->m_intCallIsLeadCcRank ) ) ? ( string ) $this->m_intCallIsLeadCcRank : 'NULL';
	}

	public function setCallIsMarketSurveyCcRank( $intCallIsMarketSurveyCcRank ) {
		$this->set( 'm_intCallIsMarketSurveyCcRank', CStrings::strToIntDef( $intCallIsMarketSurveyCcRank, NULL, false ) );
	}

	public function getCallIsMarketSurveyCcRank() {
		return $this->m_intCallIsMarketSurveyCcRank;
	}

	public function sqlCallIsMarketSurveyCcRank() {
		return ( true == isset( $this->m_intCallIsMarketSurveyCcRank ) ) ? ( string ) $this->m_intCallIsMarketSurveyCcRank : 'NULL';
	}

	public function setCallIsResidentCcRank( $intCallIsResidentCcRank ) {
		$this->set( 'm_intCallIsResidentCcRank', CStrings::strToIntDef( $intCallIsResidentCcRank, NULL, false ) );
	}

	public function getCallIsResidentCcRank() {
		return $this->m_intCallIsResidentCcRank;
	}

	public function sqlCallIsResidentCcRank() {
		return ( true == isset( $this->m_intCallIsResidentCcRank ) ) ? ( string ) $this->m_intCallIsResidentCcRank : 'NULL';
	}

	public function setCallIsSolicitorCcRank( $intCallIsSolicitorCcRank ) {
		$this->set( 'm_intCallIsSolicitorCcRank', CStrings::strToIntDef( $intCallIsSolicitorCcRank, NULL, false ) );
	}

	public function getCallIsSolicitorCcRank() {
		return $this->m_intCallIsSolicitorCcRank;
	}

	public function sqlCallIsSolicitorCcRank() {
		return ( true == isset( $this->m_intCallIsSolicitorCcRank ) ) ? ( string ) $this->m_intCallIsSolicitorCcRank : 'NULL';
	}

	public function setCallIsSupportCcRank( $intCallIsSupportCcRank ) {
		$this->set( 'm_intCallIsSupportCcRank', CStrings::strToIntDef( $intCallIsSupportCcRank, NULL, false ) );
	}

	public function getCallIsSupportCcRank() {
		return $this->m_intCallIsSupportCcRank;
	}

	public function sqlCallIsSupportCcRank() {
		return ( true == isset( $this->m_intCallIsSupportCcRank ) ) ? ( string ) $this->m_intCallIsSupportCcRank : 'NULL';
	}

	public function setCallIsVendorCcRank( $intCallIsVendorCcRank ) {
		$this->set( 'm_intCallIsVendorCcRank', CStrings::strToIntDef( $intCallIsVendorCcRank, NULL, false ) );
	}

	public function getCallIsVendorCcRank() {
		return $this->m_intCallIsVendorCcRank;
	}

	public function sqlCallIsVendorCcRank() {
		return ( true == isset( $this->m_intCallIsVendorCcRank ) ) ? ( string ) $this->m_intCallIsVendorCcRank : 'NULL';
	}

	public function setCallIsWorkOrderCcRank( $intCallIsWorkOrderCcRank ) {
		$this->set( 'm_intCallIsWorkOrderCcRank', CStrings::strToIntDef( $intCallIsWorkOrderCcRank, NULL, false ) );
	}

	public function getCallIsWorkOrderCcRank() {
		return $this->m_intCallIsWorkOrderCcRank;
	}

	public function sqlCallIsWorkOrderCcRank() {
		return ( true == isset( $this->m_intCallIsWorkOrderCcRank ) ) ? ( string ) $this->m_intCallIsWorkOrderCcRank : 'NULL';
	}

	public function setCallIsWrongNumberCcRank( $intCallIsWrongNumberCcRank ) {
		$this->set( 'm_intCallIsWrongNumberCcRank', CStrings::strToIntDef( $intCallIsWrongNumberCcRank, NULL, false ) );
	}

	public function getCallIsWrongNumberCcRank() {
		return $this->m_intCallIsWrongNumberCcRank;
	}

	public function sqlCallIsWrongNumberCcRank() {
		return ( true == isset( $this->m_intCallIsWrongNumberCcRank ) ) ? ( string ) $this->m_intCallIsWrongNumberCcRank : 'NULL';
	}

	public function setCallAverageCcRank( $intCallAverageCcRank ) {
		$this->set( 'm_intCallAverageCcRank', CStrings::strToIntDef( $intCallAverageCcRank, NULL, false ) );
	}

	public function getCallAverageCcRank() {
		return $this->m_intCallAverageCcRank;
	}

	public function sqlCallAverageCcRank() {
		return ( true == isset( $this->m_intCallAverageCcRank ) ) ? ( string ) $this->m_intCallAverageCcRank : 'NULL';
	}

	public function setLeadIsConvertedCcRank( $intLeadIsConvertedCcRank ) {
		$this->set( 'm_intLeadIsConvertedCcRank', CStrings::strToIntDef( $intLeadIsConvertedCcRank, NULL, false ) );
	}

	public function getLeadIsConvertedCcRank() {
		return $this->m_intLeadIsConvertedCcRank;
	}

	public function sqlLeadIsConvertedCcRank() {
		return ( true == isset( $this->m_intLeadIsConvertedCcRank ) ) ? ( string ) $this->m_intLeadIsConvertedCcRank : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, speaker, word, call_is_abandoned_cc, call_is_lead_cc, call_is_market_survey_cc, call_is_resident_cc, call_is_solicitor_cc, call_is_support_cc, call_is_vendor_cc, call_is_work_order_cc, call_is_wrong_number_cc, call_average_cc, lead_is_converted_cc, call_is_abandoned_cc_rank, call_is_lead_cc_rank, call_is_market_survey_cc_rank, call_is_resident_cc_rank, call_is_solicitor_cc_rank, call_is_support_cc_rank, call_is_vendor_cc_rank, call_is_work_order_cc_rank, call_is_wrong_number_cc_rank, call_average_cc_rank, lead_is_converted_cc_rank, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSpeaker() . ', ' .
 						$this->sqlWord() . ', ' .
 						$this->sqlCallIsAbandonedCc() . ', ' .
 						$this->sqlCallIsLeadCc() . ', ' .
 						$this->sqlCallIsMarketSurveyCc() . ', ' .
 						$this->sqlCallIsResidentCc() . ', ' .
 						$this->sqlCallIsSolicitorCc() . ', ' .
 						$this->sqlCallIsSupportCc() . ', ' .
 						$this->sqlCallIsVendorCc() . ', ' .
 						$this->sqlCallIsWorkOrderCc() . ', ' .
 						$this->sqlCallIsWrongNumberCc() . ', ' .
 						$this->sqlCallAverageCc() . ', ' .
 						$this->sqlLeadIsConvertedCc() . ', ' .
 						$this->sqlCallIsAbandonedCcRank() . ', ' .
 						$this->sqlCallIsLeadCcRank() . ', ' .
 						$this->sqlCallIsMarketSurveyCcRank() . ', ' .
 						$this->sqlCallIsResidentCcRank() . ', ' .
 						$this->sqlCallIsSolicitorCcRank() . ', ' .
 						$this->sqlCallIsSupportCcRank() . ', ' .
 						$this->sqlCallIsVendorCcRank() . ', ' .
 						$this->sqlCallIsWorkOrderCcRank() . ', ' .
 						$this->sqlCallIsWrongNumberCcRank() . ', ' .
 						$this->sqlCallAverageCcRank() . ', ' .
 						$this->sqlLeadIsConvertedCcRank() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' speaker = ' . $this->sqlSpeaker() . ','; } elseif( true == array_key_exists( 'Speaker', $this->getChangedColumns() ) ) { $strSql .= ' speaker = ' . $this->sqlSpeaker() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' word = ' . $this->sqlWord() . ','; } elseif( true == array_key_exists( 'Word', $this->getChangedColumns() ) ) { $strSql .= ' word = ' . $this->sqlWord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_abandoned_cc = ' . $this->sqlCallIsAbandonedCc() . ','; } elseif( true == array_key_exists( 'CallIsAbandonedCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_abandoned_cc = ' . $this->sqlCallIsAbandonedCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_lead_cc = ' . $this->sqlCallIsLeadCc() . ','; } elseif( true == array_key_exists( 'CallIsLeadCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_lead_cc = ' . $this->sqlCallIsLeadCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_market_survey_cc = ' . $this->sqlCallIsMarketSurveyCc() . ','; } elseif( true == array_key_exists( 'CallIsMarketSurveyCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_market_survey_cc = ' . $this->sqlCallIsMarketSurveyCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_resident_cc = ' . $this->sqlCallIsResidentCc() . ','; } elseif( true == array_key_exists( 'CallIsResidentCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_resident_cc = ' . $this->sqlCallIsResidentCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_solicitor_cc = ' . $this->sqlCallIsSolicitorCc() . ','; } elseif( true == array_key_exists( 'CallIsSolicitorCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_solicitor_cc = ' . $this->sqlCallIsSolicitorCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_support_cc = ' . $this->sqlCallIsSupportCc() . ','; } elseif( true == array_key_exists( 'CallIsSupportCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_support_cc = ' . $this->sqlCallIsSupportCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_vendor_cc = ' . $this->sqlCallIsVendorCc() . ','; } elseif( true == array_key_exists( 'CallIsVendorCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_vendor_cc = ' . $this->sqlCallIsVendorCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_work_order_cc = ' . $this->sqlCallIsWorkOrderCc() . ','; } elseif( true == array_key_exists( 'CallIsWorkOrderCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_work_order_cc = ' . $this->sqlCallIsWorkOrderCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_wrong_number_cc = ' . $this->sqlCallIsWrongNumberCc() . ','; } elseif( true == array_key_exists( 'CallIsWrongNumberCc', $this->getChangedColumns() ) ) { $strSql .= ' call_is_wrong_number_cc = ' . $this->sqlCallIsWrongNumberCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_average_cc = ' . $this->sqlCallAverageCc() . ','; } elseif( true == array_key_exists( 'CallAverageCc', $this->getChangedColumns() ) ) { $strSql .= ' call_average_cc = ' . $this->sqlCallAverageCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_is_converted_cc = ' . $this->sqlLeadIsConvertedCc() . ','; } elseif( true == array_key_exists( 'LeadIsConvertedCc', $this->getChangedColumns() ) ) { $strSql .= ' lead_is_converted_cc = ' . $this->sqlLeadIsConvertedCc() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_abandoned_cc_rank = ' . $this->sqlCallIsAbandonedCcRank() . ','; } elseif( true == array_key_exists( 'CallIsAbandonedCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_abandoned_cc_rank = ' . $this->sqlCallIsAbandonedCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_lead_cc_rank = ' . $this->sqlCallIsLeadCcRank() . ','; } elseif( true == array_key_exists( 'CallIsLeadCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_lead_cc_rank = ' . $this->sqlCallIsLeadCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_market_survey_cc_rank = ' . $this->sqlCallIsMarketSurveyCcRank() . ','; } elseif( true == array_key_exists( 'CallIsMarketSurveyCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_market_survey_cc_rank = ' . $this->sqlCallIsMarketSurveyCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_resident_cc_rank = ' . $this->sqlCallIsResidentCcRank() . ','; } elseif( true == array_key_exists( 'CallIsResidentCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_resident_cc_rank = ' . $this->sqlCallIsResidentCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_solicitor_cc_rank = ' . $this->sqlCallIsSolicitorCcRank() . ','; } elseif( true == array_key_exists( 'CallIsSolicitorCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_solicitor_cc_rank = ' . $this->sqlCallIsSolicitorCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_support_cc_rank = ' . $this->sqlCallIsSupportCcRank() . ','; } elseif( true == array_key_exists( 'CallIsSupportCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_support_cc_rank = ' . $this->sqlCallIsSupportCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_vendor_cc_rank = ' . $this->sqlCallIsVendorCcRank() . ','; } elseif( true == array_key_exists( 'CallIsVendorCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_vendor_cc_rank = ' . $this->sqlCallIsVendorCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_work_order_cc_rank = ' . $this->sqlCallIsWorkOrderCcRank() . ','; } elseif( true == array_key_exists( 'CallIsWorkOrderCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_work_order_cc_rank = ' . $this->sqlCallIsWorkOrderCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_wrong_number_cc_rank = ' . $this->sqlCallIsWrongNumberCcRank() . ','; } elseif( true == array_key_exists( 'CallIsWrongNumberCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_is_wrong_number_cc_rank = ' . $this->sqlCallIsWrongNumberCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_average_cc_rank = ' . $this->sqlCallAverageCcRank() . ','; } elseif( true == array_key_exists( 'CallAverageCcRank', $this->getChangedColumns() ) ) { $strSql .= ' call_average_cc_rank = ' . $this->sqlCallAverageCcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_is_converted_cc_rank = ' . $this->sqlLeadIsConvertedCcRank() . ','; } elseif( true == array_key_exists( 'LeadIsConvertedCcRank', $this->getChangedColumns() ) ) { $strSql .= ' lead_is_converted_cc_rank = ' . $this->sqlLeadIsConvertedCcRank() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'speaker' => $this->getSpeaker(),
			'word' => $this->getWord(),
			'call_is_abandoned_cc' => $this->getCallIsAbandonedCc(),
			'call_is_lead_cc' => $this->getCallIsLeadCc(),
			'call_is_market_survey_cc' => $this->getCallIsMarketSurveyCc(),
			'call_is_resident_cc' => $this->getCallIsResidentCc(),
			'call_is_solicitor_cc' => $this->getCallIsSolicitorCc(),
			'call_is_support_cc' => $this->getCallIsSupportCc(),
			'call_is_vendor_cc' => $this->getCallIsVendorCc(),
			'call_is_work_order_cc' => $this->getCallIsWorkOrderCc(),
			'call_is_wrong_number_cc' => $this->getCallIsWrongNumberCc(),
			'call_average_cc' => $this->getCallAverageCc(),
			'lead_is_converted_cc' => $this->getLeadIsConvertedCc(),
			'call_is_abandoned_cc_rank' => $this->getCallIsAbandonedCcRank(),
			'call_is_lead_cc_rank' => $this->getCallIsLeadCcRank(),
			'call_is_market_survey_cc_rank' => $this->getCallIsMarketSurveyCcRank(),
			'call_is_resident_cc_rank' => $this->getCallIsResidentCcRank(),
			'call_is_solicitor_cc_rank' => $this->getCallIsSolicitorCcRank(),
			'call_is_support_cc_rank' => $this->getCallIsSupportCcRank(),
			'call_is_vendor_cc_rank' => $this->getCallIsVendorCcRank(),
			'call_is_work_order_cc_rank' => $this->getCallIsWorkOrderCcRank(),
			'call_is_wrong_number_cc_rank' => $this->getCallIsWrongNumberCcRank(),
			'call_average_cc_rank' => $this->getCallAverageCcRank(),
			'lead_is_converted_cc_rank' => $this->getLeadIsConvertedCcRank(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>