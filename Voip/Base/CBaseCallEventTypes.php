<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallEventTypes
 * Do not add any new functions to this class.
 */

class CBaseCallEventTypes extends CEosPluralBase {

	/**
	 * @return CCallEventType[]
	 */
	public static function fetchCallEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallEventType::class, $objDatabase );
	}

	/**
	 * @return CCallEventType
	 */
	public static function fetchCallEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallEventType::class, $objDatabase );
	}

	public static function fetchCallEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_event_types', $objDatabase );
	}

	public static function fetchCallEventTypeById( $intId, $objDatabase ) {
		return self::fetchCallEventType( sprintf( 'SELECT * FROM call_event_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>