<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProductTags
 * Do not add any new functions to this class.
 */

class CBaseStoreProductTags extends CEosPluralBase {

	/**
	 * @return CStoreProductTag[]
	 */
	public static function fetchStoreProductTags( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProductTag::class, $objDatabase );
	}

	/**
	 * @return CStoreProductTag
	 */
	public static function fetchStoreProductTag( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProductTag::class, $objDatabase );
	}

	public static function fetchStoreProductTagCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_product_tags', $objDatabase );
	}

	public static function fetchStoreProductTagById( $intId, $objDatabase ) {
		return self::fetchStoreProductTag( sprintf( 'SELECT * FROM store_product_tags WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchStoreProductTagsByStoreProductId( $intStoreProductId, $objDatabase ) {
		return self::fetchStoreProductTags( sprintf( 'SELECT * FROM store_product_tags WHERE store_product_id = %d', $intStoreProductId ), $objDatabase );
	}

	public static function fetchStoreProductTagsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchStoreProductTags( sprintf( 'SELECT * FROM store_product_tags WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

}
?>