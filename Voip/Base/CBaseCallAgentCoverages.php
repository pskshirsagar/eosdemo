<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentCoverages
 * Do not add any new functions to this class.
 */

class CBaseCallAgentCoverages extends CEosPluralBase {

	/**
	 * @return CCallAgentCoverage[]
	 */
	public static function fetchCallAgentCoverages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentCoverage', $objDatabase );
	}

	/**
	 * @return CCallAgentCoverage
	 */
	public static function fetchCallAgentCoverage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentCoverage', $objDatabase );
	}

	public static function fetchCallAgentCoverageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_coverages', $objDatabase );
	}

	public static function fetchCallAgentCoverageById( $intId, $objDatabase ) {
		return self::fetchCallAgentCoverage( sprintf( 'SELECT * FROM call_agent_coverages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentCoveragesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCallAgentCoverages( sprintf( 'SELECT * FROM call_agent_coverages WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallAgentCoveragesByEmployeeVacationRequestId( $intEmployeeVacationRequestId, $objDatabase ) {
		return self::fetchCallAgentCoverages( sprintf( 'SELECT * FROM call_agent_coverages WHERE employee_vacation_request_id = %d', ( int ) $intEmployeeVacationRequestId ), $objDatabase );
	}

	public static function fetchCallAgentCoveragesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentCoverages( sprintf( 'SELECT * FROM call_agent_coverages WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>