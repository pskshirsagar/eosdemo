<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCallStatusTypes extends CEosPluralBase {

	/**
	 * @return CCallStatusType[]
	 */
	public static function fetchCallStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallStatusType::class, $objDatabase );
	}

	/**
	 * @return CCallStatusType
	 */
	public static function fetchCallStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallStatusType::class, $objDatabase );
	}

	public static function fetchCallStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_status_types', $objDatabase );
	}

	public static function fetchCallStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCallStatusType( sprintf( 'SELECT * FROM call_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>