<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProductTypes
 * Do not add any new functions to this class.
 */

class CBaseStoreProductTypes extends CEosPluralBase {

	/**
	 * @return CStoreProductType[]
	 */
	public static function fetchStoreProductTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProductType::class, $objDatabase );
	}

	/**
	 * @return CStoreProductType
	 */
	public static function fetchStoreProductType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProductType::class, $objDatabase );
	}

	public static function fetchStoreProductTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_product_types', $objDatabase );
	}

	public static function fetchStoreProductTypeById( $intId, $objDatabase ) {
		return self::fetchStoreProductType( sprintf( 'SELECT * FROM store_product_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>