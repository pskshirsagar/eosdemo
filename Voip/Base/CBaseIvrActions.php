<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrActions
 * Do not add any new functions to this class.
 */

class CBaseIvrActions extends CEosPluralBase {

	/**
	 * @return CIvrAction[]
	 */
	public static function fetchIvrActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIvrAction::class, $objDatabase );
	}

	/**
	 * @return CIvrAction
	 */
	public static function fetchIvrAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIvrAction::class, $objDatabase );
	}

	public static function fetchIvrActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_actions', $objDatabase );
	}

	public static function fetchIvrActionById( $intId, $objDatabase ) {
		return self::fetchIvrAction( sprintf( 'SELECT * FROM ivr_actions WHERE id = %d', $intId ), $objDatabase );
	}

}
?>