<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrOfficeHours
 * Do not add any new functions to this class.
 */

class CBaseIvrOfficeHours extends CEosPluralBase {

	/**
	 * @return CIvrOfficeHour[]
	 */
	public static function fetchIvrOfficeHours( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIvrOfficeHour', $objDatabase );
	}

	/**
	 * @return CIvrOfficeHour
	 */
	public static function fetchIvrOfficeHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIvrOfficeHour', $objDatabase );
	}

	public static function fetchIvrOfficeHourCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_office_hours', $objDatabase );
	}

	public static function fetchIvrOfficeHourById( $intId, $objDatabase ) {
		return self::fetchIvrOfficeHour( sprintf( 'SELECT * FROM ivr_office_hours WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIvrOfficeHoursByIvrId( $intIvrId, $objDatabase ) {
		return self::fetchIvrOfficeHours( sprintf( 'SELECT * FROM ivr_office_hours WHERE ivr_id = %d', ( int ) $intIvrId ), $objDatabase );
	}

}
?>