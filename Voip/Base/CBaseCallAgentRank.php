<?php

class CBaseCallAgentRank extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_ranks';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intRankPointMappingId;
	protected $m_intNextRankPointMappingId;
	protected $m_boolIsCurrentActiveRank;
	protected $m_intMyCredits;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCurrentActiveRank = false;
		$this->m_intMyCredits = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['rank_point_mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intRankPointMappingId', trim( $arrValues['rank_point_mapping_id'] ) ); elseif( isset( $arrValues['rank_point_mapping_id'] ) ) $this->setRankPointMappingId( $arrValues['rank_point_mapping_id'] );
		if( isset( $arrValues['next_rank_point_mapping_id'] ) && $boolDirectSet ) $this->set( 'm_intNextRankPointMappingId', trim( $arrValues['next_rank_point_mapping_id'] ) ); elseif( isset( $arrValues['next_rank_point_mapping_id'] ) ) $this->setNextRankPointMappingId( $arrValues['next_rank_point_mapping_id'] );
		if( isset( $arrValues['is_current_active_rank'] ) && $boolDirectSet ) $this->set( 'm_boolIsCurrentActiveRank', trim( stripcslashes( $arrValues['is_current_active_rank'] ) ) ); elseif( isset( $arrValues['is_current_active_rank'] ) ) $this->setIsCurrentActiveRank( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_current_active_rank'] ) : $arrValues['is_current_active_rank'] );
		if( isset( $arrValues['my_credits'] ) && $boolDirectSet ) $this->set( 'm_intMyCredits', trim( $arrValues['my_credits'] ) ); elseif( isset( $arrValues['my_credits'] ) ) $this->setMyCredits( $arrValues['my_credits'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setRankPointMappingId( $intRankPointMappingId ) {
		$this->set( 'm_intRankPointMappingId', CStrings::strToIntDef( $intRankPointMappingId, NULL, false ) );
	}

	public function getRankPointMappingId() {
		return $this->m_intRankPointMappingId;
	}

	public function sqlRankPointMappingId() {
		return ( true == isset( $this->m_intRankPointMappingId ) ) ? ( string ) $this->m_intRankPointMappingId : 'NULL';
	}

	public function setNextRankPointMappingId( $intNextRankPointMappingId ) {
		$this->set( 'm_intNextRankPointMappingId', CStrings::strToIntDef( $intNextRankPointMappingId, NULL, false ) );
	}

	public function getNextRankPointMappingId() {
		return $this->m_intNextRankPointMappingId;
	}

	public function sqlNextRankPointMappingId() {
		return ( true == isset( $this->m_intNextRankPointMappingId ) ) ? ( string ) $this->m_intNextRankPointMappingId : 'NULL';
	}

	public function setIsCurrentActiveRank( $boolIsCurrentActiveRank ) {
		$this->set( 'm_boolIsCurrentActiveRank', CStrings::strToBool( $boolIsCurrentActiveRank ) );
	}

	public function getIsCurrentActiveRank() {
		return $this->m_boolIsCurrentActiveRank;
	}

	public function sqlIsCurrentActiveRank() {
		return ( true == isset( $this->m_boolIsCurrentActiveRank ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCurrentActiveRank ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMyCredits( $intMyCredits ) {
		$this->set( 'm_intMyCredits', CStrings::strToIntDef( $intMyCredits, NULL, false ) );
	}

	public function getMyCredits() {
		return $this->m_intMyCredits;
	}

	public function sqlMyCredits() {
		return ( true == isset( $this->m_intMyCredits ) ) ? ( string ) $this->m_intMyCredits : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, rank_point_mapping_id, next_rank_point_mapping_id, is_current_active_rank, my_credits, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlRankPointMappingId() . ', ' .
						$this->sqlNextRankPointMappingId() . ', ' .
						$this->sqlIsCurrentActiveRank() . ', ' .
						$this->sqlMyCredits() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rank_point_mapping_id = ' . $this->sqlRankPointMappingId(). ',' ; } elseif( true == array_key_exists( 'RankPointMappingId', $this->getChangedColumns() ) ) { $strSql .= ' rank_point_mapping_id = ' . $this->sqlRankPointMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_rank_point_mapping_id = ' . $this->sqlNextRankPointMappingId(). ',' ; } elseif( true == array_key_exists( 'NextRankPointMappingId', $this->getChangedColumns() ) ) { $strSql .= ' next_rank_point_mapping_id = ' . $this->sqlNextRankPointMappingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current_active_rank = ' . $this->sqlIsCurrentActiveRank(). ',' ; } elseif( true == array_key_exists( 'IsCurrentActiveRank', $this->getChangedColumns() ) ) { $strSql .= ' is_current_active_rank = ' . $this->sqlIsCurrentActiveRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' my_credits = ' . $this->sqlMyCredits(). ',' ; } elseif( true == array_key_exists( 'MyCredits', $this->getChangedColumns() ) ) { $strSql .= ' my_credits = ' . $this->sqlMyCredits() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'rank_point_mapping_id' => $this->getRankPointMappingId(),
			'next_rank_point_mapping_id' => $this->getNextRankPointMappingId(),
			'is_current_active_rank' => $this->getIsCurrentActiveRank(),
			'my_credits' => $this->getMyCredits(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>