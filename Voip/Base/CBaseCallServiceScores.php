<?php

class CBaseCallServiceScores extends CEosPluralBase {

	/**
	 * @return CCallServiceScore[]
	 */
	public static function fetchCallServiceScores( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallServiceScore::class, $objDatabase );
	}

	/**
	 * @return CCallServiceScore
	 */
	public static function fetchCallServiceScore( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallServiceScore::class, $objDatabase );
	}

	public static function fetchCallServiceScoreCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_service_scores', $objDatabase );
	}

	public static function fetchCallServiceScoreById( $intId, $objDatabase ) {
		return self::fetchCallServiceScore( sprintf( 'SELECT * FROM call_service_scores WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>