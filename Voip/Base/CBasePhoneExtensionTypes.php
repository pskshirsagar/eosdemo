<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPhoneExtensionTypes
 * Do not add any new functions to this class.
 */

class CBasePhoneExtensionTypes extends CEosPluralBase {

	/**
	 * @return CPhoneExtensionType[]
	 */
	public static function fetchPhoneExtensionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPhoneExtensionType::class, $objDatabase );
	}

	/**
	 * @return CPhoneExtensionType
	 */
	public static function fetchPhoneExtensionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPhoneExtensionType::class, $objDatabase );
	}

	public static function fetchPhoneExtensionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'phone_extension_types', $objDatabase );
	}

	public static function fetchPhoneExtensionTypeById( $intId, $objDatabase ) {
		return self::fetchPhoneExtensionType( sprintf( 'SELECT * FROM phone_extension_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>