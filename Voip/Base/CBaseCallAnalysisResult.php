<?php

class CBaseCallAnalysisResult extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_analysis_results';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCallId;
	protected $m_intCallAnalysisQuestionId;
	protected $m_fltWeight;
	protected $m_strAnswer;
	protected $m_intIsQualified;
	protected $m_strReason;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCallAnalysisAnswerId;

	public function __construct() {
		parent::__construct();

		$this->m_fltWeight = '0';
		$this->m_intIsQualified = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['call_analysis_question_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisQuestionId', trim( $arrValues['call_analysis_question_id'] ) ); elseif( isset( $arrValues['call_analysis_question_id'] ) ) $this->setCallAnalysisQuestionId( $arrValues['call_analysis_question_id'] );
		if( isset( $arrValues['weight'] ) && $boolDirectSet ) $this->set( 'm_fltWeight', trim( $arrValues['weight'] ) ); elseif( isset( $arrValues['weight'] ) ) $this->setWeight( $arrValues['weight'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['is_qualified'] ) && $boolDirectSet ) $this->set( 'm_intIsQualified', trim( $arrValues['is_qualified'] ) ); elseif( isset( $arrValues['is_qualified'] ) ) $this->setIsQualified( $arrValues['is_qualified'] );
		if( isset( $arrValues['reason'] ) && $boolDirectSet ) $this->set( 'm_strReason', trim( stripcslashes( $arrValues['reason'] ) ) ); elseif( isset( $arrValues['reason'] ) ) $this->setReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason'] ) : $arrValues['reason'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['call_analysis_answer_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisAnswerId', trim( $arrValues['call_analysis_answer_id'] ) ); elseif( isset( $arrValues['call_analysis_answer_id'] ) ) $this->setCallAnalysisAnswerId( $arrValues['call_analysis_answer_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setCallAnalysisQuestionId( $intCallAnalysisQuestionId ) {
		$this->set( 'm_intCallAnalysisQuestionId', CStrings::strToIntDef( $intCallAnalysisQuestionId, NULL, false ) );
	}

	public function getCallAnalysisQuestionId() {
		return $this->m_intCallAnalysisQuestionId;
	}

	public function sqlCallAnalysisQuestionId() {
		return ( true == isset( $this->m_intCallAnalysisQuestionId ) ) ? ( string ) $this->m_intCallAnalysisQuestionId : 'NULL';
	}

	public function setWeight( $fltWeight ) {
		$this->set( 'm_fltWeight', CStrings::strToFloatDef( $fltWeight, NULL, false, 6 ) );
	}

	public function getWeight() {
		return $this->m_fltWeight;
	}

	public function sqlWeight() {
		return ( true == isset( $this->m_fltWeight ) ) ? ( string ) $this->m_fltWeight : '0';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, 2000, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setIsQualified( $intIsQualified ) {
		$this->set( 'm_intIsQualified', CStrings::strToIntDef( $intIsQualified, NULL, false ) );
	}

	public function getIsQualified() {
		return $this->m_intIsQualified;
	}

	public function sqlIsQualified() {
		return ( true == isset( $this->m_intIsQualified ) ) ? ( string ) $this->m_intIsQualified : '1';
	}

	public function setReason( $strReason ) {
		$this->set( 'm_strReason', CStrings::strTrimDef( $strReason, 2000, NULL, true ) );
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function sqlReason() {
		return ( true == isset( $this->m_strReason ) ) ? '\'' . addslashes( $this->m_strReason ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCallAnalysisAnswerId( $intCallAnalysisAnswerId ) {
		$this->set( 'm_intCallAnalysisAnswerId', CStrings::strToIntDef( $intCallAnalysisAnswerId, NULL, false ) );
	}

	public function getCallAnalysisAnswerId() {
		return $this->m_intCallAnalysisAnswerId;
	}

	public function sqlCallAnalysisAnswerId() {
		return ( true == isset( $this->m_intCallAnalysisAnswerId ) ) ? ( string ) $this->m_intCallAnalysisAnswerId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, call_id, call_analysis_question_id, weight, answer, is_qualified, reason, updated_by, updated_on, created_by, created_on, details, call_analysis_answer_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCallId() . ', ' .
						$this->sqlCallAnalysisQuestionId() . ', ' .
						$this->sqlWeight() . ', ' .
						$this->sqlAnswer() . ', ' .
						$this->sqlIsQualified() . ', ' .
						$this->sqlReason() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCallAnalysisAnswerId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_question_id = ' . $this->sqlCallAnalysisQuestionId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_question_id = ' . $this->sqlCallAnalysisQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weight = ' . $this->sqlWeight(). ',' ; } elseif( true == array_key_exists( 'Weight', $this->getChangedColumns() ) ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer(). ',' ; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_qualified = ' . $this->sqlIsQualified(). ',' ; } elseif( true == array_key_exists( 'IsQualified', $this->getChangedColumns() ) ) { $strSql .= ' is_qualified = ' . $this->sqlIsQualified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason = ' . $this->sqlReason(). ',' ; } elseif( true == array_key_exists( 'Reason', $this->getChangedColumns() ) ) { $strSql .= ' reason = ' . $this->sqlReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_answer_id = ' . $this->sqlCallAnalysisAnswerId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisAnswerId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_answer_id = ' . $this->sqlCallAnalysisAnswerId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'call_id' => $this->getCallId(),
			'call_analysis_question_id' => $this->getCallAnalysisQuestionId(),
			'weight' => $this->getWeight(),
			'answer' => $this->getAnswer(),
			'is_qualified' => $this->getIsQualified(),
			'reason' => $this->getReason(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'call_analysis_answer_id' => $this->getCallAnalysisAnswerId()
		);
	}

}
?>