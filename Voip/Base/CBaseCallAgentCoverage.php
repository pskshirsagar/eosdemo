<?php

class CBaseCallAgentCoverage extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_coverages';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeVacationRequestId;
	protected $m_intCallAgentId;
	protected $m_strBeginDatetime;
	protected $m_strEndDatetime;
	protected $m_boolIsCoverageRequired;
	protected $m_intCoveredBy;
	protected $m_strCoveredOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCoverageRequired = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_vacation_request_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeVacationRequestId', trim( $arrValues['employee_vacation_request_id'] ) ); elseif( isset( $arrValues['employee_vacation_request_id'] ) ) $this->setEmployeeVacationRequestId( $arrValues['employee_vacation_request_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBeginDatetime', trim( $arrValues['begin_datetime'] ) ); elseif( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['is_coverage_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsCoverageRequired', trim( stripcslashes( $arrValues['is_coverage_required'] ) ) ); elseif( isset( $arrValues['is_coverage_required'] ) ) $this->setIsCoverageRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_coverage_required'] ) : $arrValues['is_coverage_required'] );
		if( isset( $arrValues['covered_by'] ) && $boolDirectSet ) $this->set( 'm_intCoveredBy', trim( $arrValues['covered_by'] ) ); elseif( isset( $arrValues['covered_by'] ) ) $this->setCoveredBy( $arrValues['covered_by'] );
		if( isset( $arrValues['covered_on'] ) && $boolDirectSet ) $this->set( 'm_strCoveredOn', trim( $arrValues['covered_on'] ) ); elseif( isset( $arrValues['covered_on'] ) ) $this->setCoveredOn( $arrValues['covered_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeVacationRequestId( $intEmployeeVacationRequestId ) {
		$this->set( 'm_intEmployeeVacationRequestId', CStrings::strToIntDef( $intEmployeeVacationRequestId, NULL, false ) );
	}

	public function getEmployeeVacationRequestId() {
		return $this->m_intEmployeeVacationRequestId;
	}

	public function sqlEmployeeVacationRequestId() {
		return ( true == isset( $this->m_intEmployeeVacationRequestId ) ) ? ( string ) $this->m_intEmployeeVacationRequestId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->set( 'm_strBeginDatetime', CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true ) );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setIsCoverageRequired( $boolIsCoverageRequired ) {
		$this->set( 'm_boolIsCoverageRequired', CStrings::strToBool( $boolIsCoverageRequired ) );
	}

	public function getIsCoverageRequired() {
		return $this->m_boolIsCoverageRequired;
	}

	public function sqlIsCoverageRequired() {
		return ( true == isset( $this->m_boolIsCoverageRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCoverageRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCoveredBy( $intCoveredBy ) {
		$this->set( 'm_intCoveredBy', CStrings::strToIntDef( $intCoveredBy, NULL, false ) );
	}

	public function getCoveredBy() {
		return $this->m_intCoveredBy;
	}

	public function sqlCoveredBy() {
		return ( true == isset( $this->m_intCoveredBy ) ) ? ( string ) $this->m_intCoveredBy : 'NULL';
	}

	public function setCoveredOn( $strCoveredOn ) {
		$this->set( 'm_strCoveredOn', CStrings::strTrimDef( $strCoveredOn, -1, NULL, true ) );
	}

	public function getCoveredOn() {
		return $this->m_strCoveredOn;
	}

	public function sqlCoveredOn() {
		return ( true == isset( $this->m_strCoveredOn ) ) ? '\'' . $this->m_strCoveredOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, employee_vacation_request_id, call_agent_id, begin_datetime, end_datetime, is_coverage_required, covered_by, covered_on, approved_by, approved_on, denied_by, denied_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeVacationRequestId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlBeginDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlIsCoverageRequired() . ', ' .
 						$this->sqlCoveredBy() . ', ' .
 						$this->sqlCoveredOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeniedBy() . ', ' .
 						$this->sqlDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_vacation_request_id = ' . $this->sqlEmployeeVacationRequestId() . ','; } elseif( true == array_key_exists( 'EmployeeVacationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' employee_vacation_request_id = ' . $this->sqlEmployeeVacationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; } elseif( true == array_key_exists( 'BeginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_coverage_required = ' . $this->sqlIsCoverageRequired() . ','; } elseif( true == array_key_exists( 'IsCoverageRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_coverage_required = ' . $this->sqlIsCoverageRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' covered_by = ' . $this->sqlCoveredBy() . ','; } elseif( true == array_key_exists( 'CoveredBy', $this->getChangedColumns() ) ) { $strSql .= ' covered_by = ' . $this->sqlCoveredBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' covered_on = ' . $this->sqlCoveredOn() . ','; } elseif( true == array_key_exists( 'CoveredOn', $this->getChangedColumns() ) ) { $strSql .= ' covered_on = ' . $this->sqlCoveredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_vacation_request_id' => $this->getEmployeeVacationRequestId(),
			'call_agent_id' => $this->getCallAgentId(),
			'begin_datetime' => $this->getBeginDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'is_coverage_required' => $this->getIsCoverageRequired(),
			'covered_by' => $this->getCoveredBy(),
			'covered_on' => $this->getCoveredOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>