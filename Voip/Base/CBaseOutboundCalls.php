<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCalls
 * Do not add any new functions to this class.
 */

class CBaseOutboundCalls extends CEosPluralBase {

	/**
	 * @return COutboundCall[]
	 */
	public static function fetchOutboundCalls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COutboundCall', $objDatabase );
	}

	/**
	 * @return COutboundCall
	 */
	public static function fetchOutboundCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COutboundCall', $objDatabase );
	}

	public static function fetchOutboundCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'outbound_calls', $objDatabase );
	}

	public static function fetchOutboundCallById( $intId, $objDatabase ) {
		return self::fetchOutboundCall( sprintf( 'SELECT * FROM outbound_calls WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCid( $intCid, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchOutboundCallsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchOutboundCallsByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE company_user_id = %d', ( int ) $intCompanyUserId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE company_employee_id = %d', ( int ) $intCompanyEmployeeId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

	public static function fetchOutboundCallsByOutboundCallStatusTypeId( $intOutboundCallStatusTypeId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE outbound_call_status_type_id = %d', ( int ) $intOutboundCallStatusTypeId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE call_type_id = %d', ( int ) $intCallTypeId ), $objDatabase );
	}

	public static function fetchOutboundCallsByOutboundCallCampaignId( $intOutboundCallCampaignId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE outbound_call_campaign_id = %d', ( int ) $intOutboundCallCampaignId ), $objDatabase );
	}

	public static function fetchOutboundCallsByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE phone_number_type_id = %d', ( int ) $intPhoneNumberTypeId ), $objDatabase );
	}

	public static function fetchOutboundCallsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchOutboundCallsByTaskNoteId( $intTaskNoteId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE task_note_id = %d', ( int ) $intTaskNoteId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCompanyUserScoreId( $intCompanyUserScoreId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE company_user_score_id = %d', ( int ) $intCompanyUserScoreId ), $objDatabase );
	}

	public static function fetchOutboundCallsByCallPriorityId( $intCallPriorityId, $objDatabase ) {
		return self::fetchOutboundCalls( sprintf( 'SELECT * FROM outbound_calls WHERE call_priority_id = %d', ( int ) $intCallPriorityId ), $objDatabase );
	}

}
?>