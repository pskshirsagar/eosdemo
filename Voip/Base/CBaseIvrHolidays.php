<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrHolidays
 * Do not add any new functions to this class.
 */

class CBaseIvrHolidays extends CEosPluralBase {

	/**
	 * @return CIvrHoliday[]
	 */
	public static function fetchIvrHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIvrHoliday', $objDatabase );
	}

	/**
	 * @return CIvrHoliday
	 */
	public static function fetchIvrHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIvrHoliday', $objDatabase );
	}

	public static function fetchIvrHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_holidays', $objDatabase );
	}

	public static function fetchIvrHolidayById( $intId, $objDatabase ) {
		return self::fetchIvrHoliday( sprintf( 'SELECT * FROM ivr_holidays WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIvrHolidaysByIvrId( $intIvrId, $objDatabase ) {
		return self::fetchIvrHolidays( sprintf( 'SELECT * FROM ivr_holidays WHERE ivr_id = %d', ( int ) $intIvrId ), $objDatabase );
	}

}
?>