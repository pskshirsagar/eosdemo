<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallers
 * Do not add any new functions to this class.
 */

class CBaseCallers extends CEosPluralBase {

	/**
	 * @return CCaller[]
	 */
	public static function fetchCallers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCaller', $objDatabase );
	}

	/**
	 * @return CCaller
	 */
	public static function fetchCaller( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCaller', $objDatabase );
	}

	public static function fetchCallerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'callers', $objDatabase );
	}

	public static function fetchCallerById( $intId, $objDatabase ) {
		return self::fetchCaller( sprintf( 'SELECT * FROM callers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallersByCallerTypeId( $intCallerTypeId, $objDatabase ) {
		return self::fetchCallers( sprintf( 'SELECT * FROM callers WHERE caller_type_id = %d', ( int ) $intCallerTypeId ), $objDatabase );
	}

}
?>