<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerPhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseCallerPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CCallerPhoneNumber[]
	 */
	public static function fetchCallerPhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallerPhoneNumber', $objDatabase );
	}

	/**
	 * @return CCallerPhoneNumber
	 */
	public static function fetchCallerPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallerPhoneNumber', $objDatabase );
	}

	public static function fetchCallerPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'caller_phone_numbers', $objDatabase );
	}

	public static function fetchCallerPhoneNumberById( $intId, $objDatabase ) {
		return self::fetchCallerPhoneNumber( sprintf( 'SELECT * FROM caller_phone_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>