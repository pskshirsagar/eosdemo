<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallChunks
 * Do not add any new functions to this class.
 */

class CBaseCallChunks extends CEosPluralBase {

	/**
	 * @return CCallChunk[]
	 */
	public static function fetchCallChunks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallChunk', $objDatabase );
	}

	/**
	 * @return CCallChunk
	 */
	public static function fetchCallChunk( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallChunk', $objDatabase );
	}

	public static function fetchCallChunkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_chunks', $objDatabase );
	}

	public static function fetchCallChunkById( $intId, $objDatabase ) {
		return self::fetchCallChunk( sprintf( 'SELECT * FROM call_chunks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallChunksByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallChunks( sprintf( 'SELECT * FROM call_chunks WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallChunksByCallChunkTypeId( $intCallChunkTypeId, $objDatabase ) {
		return self::fetchCallChunks( sprintf( 'SELECT * FROM call_chunks WHERE call_chunk_type_id = %d', ( int ) $intCallChunkTypeId ), $objDatabase );
	}

}
?>