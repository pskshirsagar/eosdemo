<?php

class CBaseCallIvrMenuAction extends CEosSingularBase {

	const TABLE_NAME = 'public.call_ivr_menu_actions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCallId;
	protected $m_intIvrMenuActionId;
	protected $m_intIvrMenuId;
	protected $m_intIvrActionId;
	protected $m_strActionKey;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['ivr_menu_action_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuActionId', trim( $arrValues['ivr_menu_action_id'] ) ); elseif( isset( $arrValues['ivr_menu_action_id'] ) ) $this->setIvrMenuActionId( $arrValues['ivr_menu_action_id'] );
		if( isset( $arrValues['ivr_menu_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuId', trim( $arrValues['ivr_menu_id'] ) ); elseif( isset( $arrValues['ivr_menu_id'] ) ) $this->setIvrMenuId( $arrValues['ivr_menu_id'] );
		if( isset( $arrValues['ivr_action_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrActionId', trim( $arrValues['ivr_action_id'] ) ); elseif( isset( $arrValues['ivr_action_id'] ) ) $this->setIvrActionId( $arrValues['ivr_action_id'] );
		if( isset( $arrValues['action_key'] ) && $boolDirectSet ) $this->set( 'm_strActionKey', trim( stripcslashes( $arrValues['action_key'] ) ) ); elseif( isset( $arrValues['action_key'] ) ) $this->setActionKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_key'] ) : $arrValues['action_key'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setIvrMenuActionId( $intIvrMenuActionId ) {
		$this->set( 'm_intIvrMenuActionId', CStrings::strToIntDef( $intIvrMenuActionId, NULL, false ) );
	}

	public function getIvrMenuActionId() {
		return $this->m_intIvrMenuActionId;
	}

	public function sqlIvrMenuActionId() {
		return ( true == isset( $this->m_intIvrMenuActionId ) ) ? ( string ) $this->m_intIvrMenuActionId : 'NULL';
	}

	public function setIvrMenuId( $intIvrMenuId ) {
		$this->set( 'm_intIvrMenuId', CStrings::strToIntDef( $intIvrMenuId, NULL, false ) );
	}

	public function getIvrMenuId() {
		return $this->m_intIvrMenuId;
	}

	public function sqlIvrMenuId() {
		return ( true == isset( $this->m_intIvrMenuId ) ) ? ( string ) $this->m_intIvrMenuId : 'NULL';
	}

	public function setIvrActionId( $intIvrActionId ) {
		$this->set( 'm_intIvrActionId', CStrings::strToIntDef( $intIvrActionId, NULL, false ) );
	}

	public function getIvrActionId() {
		return $this->m_intIvrActionId;
	}

	public function sqlIvrActionId() {
		return ( true == isset( $this->m_intIvrActionId ) ) ? ( string ) $this->m_intIvrActionId : 'NULL';
	}

	public function setActionKey( $strActionKey ) {
		$this->set( 'm_strActionKey', CStrings::strTrimDef( $strActionKey, 5, NULL, true ) );
	}

	public function getActionKey() {
		return $this->m_strActionKey;
	}

	public function sqlActionKey() {
		return ( true == isset( $this->m_strActionKey ) ) ? '\'' . addslashes( $this->m_strActionKey ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, call_id, ivr_menu_action_id, ivr_menu_id, ivr_action_id, action_key, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlIvrMenuActionId() . ', ' .
 						$this->sqlIvrMenuId() . ', ' .
 						$this->sqlIvrActionId() . ', ' .
 						$this->sqlActionKey() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_action_id = ' . $this->sqlIvrMenuActionId() . ','; } elseif( true == array_key_exists( 'IvrMenuActionId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_action_id = ' . $this->sqlIvrMenuActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; } elseif( true == array_key_exists( 'IvrMenuId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_action_id = ' . $this->sqlIvrActionId() . ','; } elseif( true == array_key_exists( 'IvrActionId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_action_id = ' . $this->sqlIvrActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_key = ' . $this->sqlActionKey() . ','; } elseif( true == array_key_exists( 'ActionKey', $this->getChangedColumns() ) ) { $strSql .= ' action_key = ' . $this->sqlActionKey() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'call_id' => $this->getCallId(),
			'ivr_menu_action_id' => $this->getIvrMenuActionId(),
			'ivr_menu_id' => $this->getIvrMenuId(),
			'ivr_action_id' => $this->getIvrActionId(),
			'action_key' => $this->getActionKey(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>