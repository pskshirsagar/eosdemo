<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentLogs
 * Do not add any new functions to this class.
 */

class CBaseCallAgentLogs extends CEosPluralBase {

	/**
	 * @return CCallAgentLog[]
	 */
	public static function fetchCallAgentLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentLog', $objDatabase );
	}

	/**
	 * @return CCallAgentLog
	 */
	public static function fetchCallAgentLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentLog', $objDatabase );
	}

	public static function fetchCallAgentLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_logs', $objDatabase );
	}

	public static function fetchCallAgentLogById( $intId, $objDatabase ) {
		return self::fetchCallAgentLog( sprintf( 'SELECT * FROM call_agent_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentLogs( sprintf( 'SELECT * FROM call_agent_logs WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentLogsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallAgentLogs( sprintf( 'SELECT * FROM call_agent_logs WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentStatusTypeId( $intCallAgentStatusTypeId, $objDatabase ) {
		return self::fetchCallAgentLogs( sprintf( 'SELECT * FROM call_agent_logs WHERE call_agent_status_type_id = %d', ( int ) $intCallAgentStatusTypeId ), $objDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentStateTypeId( $intCallAgentStateTypeId, $objDatabase ) {
		return self::fetchCallAgentLogs( sprintf( 'SELECT * FROM call_agent_logs WHERE call_agent_state_type_id = %d', ( int ) $intCallAgentStateTypeId ), $objDatabase );
	}

}
?>