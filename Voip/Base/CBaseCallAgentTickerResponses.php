<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentTickerResponses
 * Do not add any new functions to this class.
 */

class CBaseCallAgentTickerResponses extends CEosPluralBase {

	/**
	 * @return CCallAgentTickerResponse[]
	 */
	public static function fetchCallAgentTickerResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentTickerResponse::class, $objDatabase );
	}

	/**
	 * @return CCallAgentTickerResponse
	 */
	public static function fetchCallAgentTickerResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentTickerResponse::class, $objDatabase );
	}

	public static function fetchCallAgentTickerResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_ticker_responses', $objDatabase );
	}

	public static function fetchCallAgentTickerResponseById( $intId, $objDatabase ) {
		return self::fetchCallAgentTickerResponse( sprintf( 'SELECT * FROM call_agent_ticker_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentTickerResponsesByCallAgentTickerId( $intCallAgentTickerId, $objDatabase ) {
		return self::fetchCallAgentTickerResponses( sprintf( 'SELECT * FROM call_agent_ticker_responses WHERE call_agent_ticker_id = %d', ( int ) $intCallAgentTickerId ), $objDatabase );
	}

	public static function fetchCallAgentTickerResponsesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentTickerResponses( sprintf( 'SELECT * FROM call_agent_ticker_responses WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>