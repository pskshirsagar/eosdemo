<?php

class CBaseCallAgentPin extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_pins';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intCallAgentPinTypeId;
	protected $m_intSourceCallAgentId;
	protected $m_strRemark;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strRemark = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_agent_pin_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentPinTypeId', trim( $arrValues['call_agent_pin_type_id'] ) ); elseif( isset( $arrValues['call_agent_pin_type_id'] ) ) $this->setCallAgentPinTypeId( $arrValues['call_agent_pin_type_id'] );
		if( isset( $arrValues['source_call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceCallAgentId', trim( $arrValues['source_call_agent_id'] ) ); elseif( isset( $arrValues['source_call_agent_id'] ) ) $this->setSourceCallAgentId( $arrValues['source_call_agent_id'] );
		if( isset( $arrValues['remark'] ) && $boolDirectSet ) $this->set( 'm_strRemark', trim( stripcslashes( $arrValues['remark'] ) ) ); elseif( isset( $arrValues['remark'] ) ) $this->setRemark( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remark'] ) : $arrValues['remark'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallAgentPinTypeId( $intCallAgentPinTypeId ) {
		$this->set( 'm_intCallAgentPinTypeId', CStrings::strToIntDef( $intCallAgentPinTypeId, NULL, false ) );
	}

	public function getCallAgentPinTypeId() {
		return $this->m_intCallAgentPinTypeId;
	}

	public function sqlCallAgentPinTypeId() {
		return ( true == isset( $this->m_intCallAgentPinTypeId ) ) ? ( string ) $this->m_intCallAgentPinTypeId : 'NULL';
	}

	public function setSourceCallAgentId( $intSourceCallAgentId ) {
		$this->set( 'm_intSourceCallAgentId', CStrings::strToIntDef( $intSourceCallAgentId, NULL, false ) );
	}

	public function getSourceCallAgentId() {
		return $this->m_intSourceCallAgentId;
	}

	public function sqlSourceCallAgentId() {
		return ( true == isset( $this->m_intSourceCallAgentId ) ) ? ( string ) $this->m_intSourceCallAgentId : 'NULL';
	}

	public function setRemark( $strRemark ) {
		$this->set( 'm_strRemark', CStrings::strTrimDef( $strRemark, 100, NULL, true ) );
	}

	public function getRemark() {
		return $this->m_strRemark;
	}

	public function sqlRemark() {
		return ( true == isset( $this->m_strRemark ) ) ? '\'' . addslashes( $this->m_strRemark ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, call_agent_pin_type_id, source_call_agent_id, remark, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallAgentPinTypeId() . ', ' .
 						$this->sqlSourceCallAgentId() . ', ' .
 						$this->sqlRemark() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_pin_type_id = ' . $this->sqlCallAgentPinTypeId() . ','; } elseif( true == array_key_exists( 'CallAgentPinTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_pin_type_id = ' . $this->sqlCallAgentPinTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_call_agent_id = ' . $this->sqlSourceCallAgentId() . ','; } elseif( true == array_key_exists( 'SourceCallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' source_call_agent_id = ' . $this->sqlSourceCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; } elseif( true == array_key_exists( 'Remark', $this->getChangedColumns() ) ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_agent_pin_type_id' => $this->getCallAgentPinTypeId(),
			'source_call_agent_id' => $this->getSourceCallAgentId(),
			'remark' => $this->getRemark(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>