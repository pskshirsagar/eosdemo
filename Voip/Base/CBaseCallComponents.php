<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallComponents
 * Do not add any new functions to this class.
 */

class CBaseCallComponents extends CEosPluralBase {

	/**
	 * @return CCallComponent[]
	 */
	public static function fetchCallComponents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallComponent::class, $objDatabase );
	}

	/**
	 * @return CCallComponent
	 */
	public static function fetchCallComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallComponent::class, $objDatabase );
	}

	public static function fetchCallComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_components', $objDatabase );
	}

	public static function fetchCallComponentById( $intId, $objDatabase ) {
		return self::fetchCallComponent( sprintf( 'SELECT * FROM call_components WHERE id = %d', $intId ), $objDatabase );
	}

}
?>