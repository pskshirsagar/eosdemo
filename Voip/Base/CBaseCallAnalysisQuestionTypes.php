<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisQuestionTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisQuestionTypes extends CEosPluralBase {

	/**
	 * @return CCallAnalysisQuestionType[]
	 */
	public static function fetchCallAnalysisQuestionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisQuestionType::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisQuestionType
	 */
	public static function fetchCallAnalysisQuestionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisQuestionType::class, $objDatabase );
	}

	public static function fetchCallAnalysisQuestionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_question_types', $objDatabase );
	}

	public static function fetchCallAnalysisQuestionTypeById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisQuestionType( sprintf( 'SELECT * FROM call_analysis_question_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>