<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisQuestions
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisQuestions extends CEosPluralBase {

	/**
	 * @return CCallAnalysisQuestion[]
	 */
	public static function fetchCallAnalysisQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisQuestion::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisQuestion
	 */
	public static function fetchCallAnalysisQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisQuestion::class, $objDatabase );
	}

	public static function fetchCallAnalysisQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_questions', $objDatabase );
	}

	public static function fetchCallAnalysisQuestionById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisQuestion( sprintf( 'SELECT * FROM call_analysis_questions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisQuestionsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisQuestions( sprintf( 'SELECT * FROM call_analysis_questions WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisCategoryId( $intCallAnalysisCategoryId, $objDatabase ) {
		return self::fetchCallAnalysisQuestions( sprintf( 'SELECT * FROM call_analysis_questions WHERE call_analysis_category_id = %d', $intCallAnalysisCategoryId ), $objDatabase );
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisQuestionId( $intCallAnalysisQuestionId, $objDatabase ) {
		return self::fetchCallAnalysisQuestions( sprintf( 'SELECT * FROM call_analysis_questions WHERE call_analysis_question_id = %d', $intCallAnalysisQuestionId ), $objDatabase );
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisQuestionTypeId( $intCallAnalysisQuestionTypeId, $objDatabase ) {
		return self::fetchCallAnalysisQuestions( sprintf( 'SELECT * FROM call_analysis_questions WHERE call_analysis_question_type_id = %d', $intCallAnalysisQuestionTypeId ), $objDatabase );
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisScorecardId( $intCallAnalysisScorecardId, $objDatabase ) {
		return self::fetchCallAnalysisQuestions( sprintf( 'SELECT * FROM call_analysis_questions WHERE call_analysis_scorecard_id = %d', $intCallAnalysisScorecardId ), $objDatabase );
	}

}
?>