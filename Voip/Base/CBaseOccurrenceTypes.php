<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COccurrenceTypes
 * Do not add any new functions to this class.
 */

class CBaseOccurrenceTypes extends CEosPluralBase {

	/**
	 * @return COccurrenceType[]
	 */
	public static function fetchOccurrenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COccurrenceType::class, $objDatabase );
	}

	/**
	 * @return COccurrenceType
	 */
	public static function fetchOccurrenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COccurrenceType::class, $objDatabase );
	}

	public static function fetchOccurrenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'occurrence_types', $objDatabase );
	}

	public static function fetchOccurrenceTypeById( $intId, $objDatabase ) {
		return self::fetchOccurrenceType( sprintf( 'SELECT * FROM occurrence_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>