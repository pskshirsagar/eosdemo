<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentPins
 * Do not add any new functions to this class.
 */

class CBaseCallAgentPins extends CEosPluralBase {

	/**
	 * @return CCallAgentPin[]
	 */
	public static function fetchCallAgentPins( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentPin::class, $objDatabase );
	}

	/**
	 * @return CCallAgentPin
	 */
	public static function fetchCallAgentPin( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentPin::class, $objDatabase );
	}

	public static function fetchCallAgentPinCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_pins', $objDatabase );
	}

	public static function fetchCallAgentPinById( $intId, $objDatabase ) {
		return self::fetchCallAgentPin( sprintf( 'SELECT * FROM call_agent_pins WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentPinsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentPins( sprintf( 'SELECT * FROM call_agent_pins WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentPinsByCallAgentPinTypeId( $intCallAgentPinTypeId, $objDatabase ) {
		return self::fetchCallAgentPins( sprintf( 'SELECT * FROM call_agent_pins WHERE call_agent_pin_type_id = %d', ( int ) $intCallAgentPinTypeId ), $objDatabase );
	}

	public static function fetchCallAgentPinsBySourceCallAgentId( $intSourceCallAgentId, $objDatabase ) {
		return self::fetchCallAgentPins( sprintf( 'SELECT * FROM call_agent_pins WHERE source_call_agent_id = %d', ( int ) $intSourceCallAgentId ), $objDatabase );
	}

}
?>