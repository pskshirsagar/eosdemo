<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRewardPointReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseRewardPointReferenceTypes extends CEosPluralBase {

	/**
	 * @return CRewardPointReferenceType[]
	 */
	public static function fetchRewardPointReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRewardPointReferenceType::class, $objDatabase );
	}

	/**
	 * @return CRewardPointReferenceType
	 */
	public static function fetchRewardPointReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRewardPointReferenceType::class, $objDatabase );
	}

	public static function fetchRewardPointReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reward_point_reference_types', $objDatabase );
	}

	public static function fetchRewardPointReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchRewardPointReferenceType( sprintf( 'SELECT * FROM reward_point_reference_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>