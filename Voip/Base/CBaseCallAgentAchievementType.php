<?php

class CBaseCallAgentAchievementType extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_achievement_types';

	protected $m_intId;
	protected $m_intRewardPointTypeId;
	protected $m_intCallAgentAchievementTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['reward_point_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRewardPointTypeId', trim( $arrValues['reward_point_type_id'] ) ); elseif( isset( $arrValues['reward_point_type_id'] ) ) $this->setRewardPointTypeId( $arrValues['reward_point_type_id'] );
		if( isset( $arrValues['call_agent_achievement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentAchievementTypeId', trim( $arrValues['call_agent_achievement_type_id'] ) ); elseif( isset( $arrValues['call_agent_achievement_type_id'] ) ) $this->setCallAgentAchievementTypeId( $arrValues['call_agent_achievement_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRewardPointTypeId( $intRewardPointTypeId ) {
		$this->set( 'm_intRewardPointTypeId', CStrings::strToIntDef( $intRewardPointTypeId, NULL, false ) );
	}

	public function getRewardPointTypeId() {
		return $this->m_intRewardPointTypeId;
	}

	public function sqlRewardPointTypeId() {
		return ( true == isset( $this->m_intRewardPointTypeId ) ) ? ( string ) $this->m_intRewardPointTypeId : 'NULL';
	}

	public function setCallAgentAchievementTypeId( $intCallAgentAchievementTypeId ) {
		$this->set( 'm_intCallAgentAchievementTypeId', CStrings::strToIntDef( $intCallAgentAchievementTypeId, NULL, false ) );
	}

	public function getCallAgentAchievementTypeId() {
		return $this->m_intCallAgentAchievementTypeId;
	}

	public function sqlCallAgentAchievementTypeId() {
		return ( true == isset( $this->m_intCallAgentAchievementTypeId ) ) ? ( string ) $this->m_intCallAgentAchievementTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 500, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'reward_point_type_id' => $this->getRewardPointTypeId(),
			'call_agent_achievement_type_id' => $this->getCallAgentAchievementTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>