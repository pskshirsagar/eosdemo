<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFormStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCallFormStatusTypes extends CEosPluralBase {

	/**
	 * @return CCallFormStatusType[]
	 */
	public static function fetchCallFormStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallFormStatusType::class, $objDatabase );
	}

	/**
	 * @return CCallFormStatusType
	 */
	public static function fetchCallFormStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallFormStatusType::class, $objDatabase );
	}

	public static function fetchCallFormStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_form_status_types', $objDatabase );
	}

	public static function fetchCallFormStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCallFormStatusType( sprintf( 'SELECT * FROM call_form_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>