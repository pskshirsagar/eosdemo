<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueueTypes
 * Do not add any new functions to this class.
 */

class CBaseCallQueueTypes extends CEosPluralBase {

	/**
	 * @return CCallQueueType[]
	 */
	public static function fetchCallQueueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallQueueType::class, $objDatabase );
	}

	/**
	 * @return CCallQueueType
	 */
	public static function fetchCallQueueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallQueueType::class, $objDatabase );
	}

	public static function fetchCallQueueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_queue_types', $objDatabase );
	}

	public static function fetchCallQueueTypeById( $intId, $objDatabase ) {
		return self::fetchCallQueueType( sprintf( 'SELECT * FROM call_queue_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>