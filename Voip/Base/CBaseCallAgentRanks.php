<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentRanks
 * Do not add any new functions to this class.
 */

class CBaseCallAgentRanks extends CEosPluralBase {

	/**
	 * @return CCallAgentRank[]
	 */
	public static function fetchCallAgentRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentRank::class, $objDatabase );
	}

	/**
	 * @return CCallAgentRank
	 */
	public static function fetchCallAgentRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentRank::class, $objDatabase );
	}

	public static function fetchCallAgentRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_ranks', $objDatabase );
	}

	public static function fetchCallAgentRankById( $intId, $objDatabase ) {
		return self::fetchCallAgentRank( sprintf( 'SELECT * FROM call_agent_ranks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentRanksByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentRanks( sprintf( 'SELECT * FROM call_agent_ranks WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentRanksByRankPointMappingId( $intRankPointMappingId, $objDatabase ) {
		return self::fetchCallAgentRanks( sprintf( 'SELECT * FROM call_agent_ranks WHERE rank_point_mapping_id = %d', $intRankPointMappingId ), $objDatabase );
	}

	public static function fetchCallAgentRanksByNextRankPointMappingId( $intNextRankPointMappingId, $objDatabase ) {
		return self::fetchCallAgentRanks( sprintf( 'SELECT * FROM call_agent_ranks WHERE next_rank_point_mapping_id = %d', $intNextRankPointMappingId ), $objDatabase );
	}

}
?>