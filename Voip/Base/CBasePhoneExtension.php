<?php

class CBasePhoneExtension extends CEosSingularBase {

	const TABLE_NAME = 'public.phone_extensions';

	protected $m_intId;
	protected $m_intPhoneExtensionTypeId;
	protected $m_strPhoneName;
	protected $m_strExtension;
	protected $m_strPassword;
	protected $m_strVoicemailPin;
	protected $m_boolIsShared;
	protected $m_strMachineName;
	protected $m_strIpAddress;
	protected $m_intPortNumber;
	protected $m_strDomainName;
	protected $m_strDisplayName;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsShared = false;
		$this->m_strMachineName = NULL;
		$this->m_strIpAddress = NULL;
		$this->m_strDomainName = NULL;
		$this->m_strDisplayName = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['phone_extension_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneExtensionTypeId', trim( $arrValues['phone_extension_type_id'] ) ); elseif( isset( $arrValues['phone_extension_type_id'] ) ) $this->setPhoneExtensionTypeId( $arrValues['phone_extension_type_id'] );
		if( isset( $arrValues['phone_name'] ) && $boolDirectSet ) $this->set( 'm_strPhoneName', trim( stripcslashes( $arrValues['phone_name'] ) ) ); elseif( isset( $arrValues['phone_name'] ) ) $this->setPhoneName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_name'] ) : $arrValues['phone_name'] );
		if( isset( $arrValues['extension'] ) && $boolDirectSet ) $this->set( 'm_strExtension', trim( stripcslashes( $arrValues['extension'] ) ) ); elseif( isset( $arrValues['extension'] ) ) $this->setExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['extension'] ) : $arrValues['extension'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['voicemail_pin'] ) && $boolDirectSet ) $this->set( 'm_strVoicemailPin', trim( stripcslashes( $arrValues['voicemail_pin'] ) ) ); elseif( isset( $arrValues['voicemail_pin'] ) ) $this->setVoicemailPin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['voicemail_pin'] ) : $arrValues['voicemail_pin'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_boolIsShared', trim( stripcslashes( $arrValues['is_shared'] ) ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_shared'] ) : $arrValues['is_shared'] );
		if( isset( $arrValues['machine_name'] ) && $boolDirectSet ) $this->set( 'm_strMachineName', trim( stripcslashes( $arrValues['machine_name'] ) ) ); elseif( isset( $arrValues['machine_name'] ) ) $this->setMachineName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['machine_name'] ) : $arrValues['machine_name'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['port_number'] ) && $boolDirectSet ) $this->set( 'm_intPortNumber', trim( $arrValues['port_number'] ) ); elseif( isset( $arrValues['port_number'] ) ) $this->setPortNumber( $arrValues['port_number'] );
		if( isset( $arrValues['domain_name'] ) && $boolDirectSet ) $this->set( 'm_strDomainName', trim( stripcslashes( $arrValues['domain_name'] ) ) ); elseif( isset( $arrValues['domain_name'] ) ) $this->setDomainName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain_name'] ) : $arrValues['domain_name'] );
		if( isset( $arrValues['display_name'] ) && $boolDirectSet ) $this->set( 'm_strDisplayName', trim( stripcslashes( $arrValues['display_name'] ) ) ); elseif( isset( $arrValues['display_name'] ) ) $this->setDisplayName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_name'] ) : $arrValues['display_name'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPhoneExtensionTypeId( $intPhoneExtensionTypeId ) {
		$this->set( 'm_intPhoneExtensionTypeId', CStrings::strToIntDef( $intPhoneExtensionTypeId, NULL, false ) );
	}

	public function getPhoneExtensionTypeId() {
		return $this->m_intPhoneExtensionTypeId;
	}

	public function sqlPhoneExtensionTypeId() {
		return ( true == isset( $this->m_intPhoneExtensionTypeId ) ) ? ( string ) $this->m_intPhoneExtensionTypeId : 'NULL';
	}

	public function setPhoneName( $strPhoneName ) {
		$this->set( 'm_strPhoneName', CStrings::strTrimDef( $strPhoneName, 50, NULL, true ) );
	}

	public function getPhoneName() {
		return $this->m_strPhoneName;
	}

	public function sqlPhoneName() {
		return ( true == isset( $this->m_strPhoneName ) ) ? '\'' . addslashes( $this->m_strPhoneName ) . '\'' : 'NULL';
	}

	public function setExtension( $strExtension ) {
		$this->set( 'm_strExtension', CStrings::strTrimDef( $strExtension, 50, NULL, true ) );
	}

	public function getExtension() {
		return $this->m_strExtension;
	}

	public function sqlExtension() {
		return ( true == isset( $this->m_strExtension ) ) ? '\'' . addslashes( $this->m_strExtension ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 1000, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setVoicemailPin( $strVoicemailPin ) {
		$this->set( 'm_strVoicemailPin', CStrings::strTrimDef( $strVoicemailPin, 1000, NULL, true ) );
	}

	public function getVoicemailPin() {
		return $this->m_strVoicemailPin;
	}

	public function sqlVoicemailPin() {
		return ( true == isset( $this->m_strVoicemailPin ) ) ? '\'' . addslashes( $this->m_strVoicemailPin ) . '\'' : 'NULL';
	}

	public function setIsShared( $boolIsShared ) {
		$this->set( 'm_boolIsShared', CStrings::strToBool( $boolIsShared ) );
	}

	public function getIsShared() {
		return $this->m_boolIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_boolIsShared ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShared ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMachineName( $strMachineName ) {
		$this->set( 'm_strMachineName', CStrings::strTrimDef( $strMachineName, 40, NULL, true ) );
	}

	public function getMachineName() {
		return $this->m_strMachineName;
	}

	public function sqlMachineName() {
		return ( true == isset( $this->m_strMachineName ) ) ? '\'' . addslashes( $this->m_strMachineName ) . '\'' : '\'NULL\'';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 40, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : '\'NULL\'';
	}

	public function setPortNumber( $intPortNumber ) {
		$this->set( 'm_intPortNumber', CStrings::strToIntDef( $intPortNumber, NULL, false ) );
	}

	public function getPortNumber() {
		return $this->m_intPortNumber;
	}

	public function sqlPortNumber() {
		return ( true == isset( $this->m_intPortNumber ) ) ? ( string ) $this->m_intPortNumber : 'NULL';
	}

	public function setDomainName( $strDomainName ) {
		$this->set( 'm_strDomainName', CStrings::strTrimDef( $strDomainName, 40, NULL, true ) );
	}

	public function getDomainName() {
		return $this->m_strDomainName;
	}

	public function sqlDomainName() {
		return ( true == isset( $this->m_strDomainName ) ) ? '\'' . addslashes( $this->m_strDomainName ) . '\'' : '\'NULL\'';
	}

	public function setDisplayName( $strDisplayName ) {
		$this->set( 'm_strDisplayName', CStrings::strTrimDef( $strDisplayName, 100, NULL, true ) );
	}

	public function getDisplayName() {
		return $this->m_strDisplayName;
	}

	public function sqlDisplayName() {
		return ( true == isset( $this->m_strDisplayName ) ) ? '\'' . addslashes( $this->m_strDisplayName ) . '\'' : '\'NULL\'';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, phone_extension_type_id, phone_name, extension, password, voicemail_pin, is_shared, machine_name, ip_address, port_number, domain_name, display_name, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPhoneExtensionTypeId() . ', ' .
 						$this->sqlPhoneName() . ', ' .
 						$this->sqlExtension() . ', ' .
 						$this->sqlPassword() . ', ' .
 						$this->sqlVoicemailPin() . ', ' .
 						$this->sqlIsShared() . ', ' .
 						$this->sqlMachineName() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlPortNumber() . ', ' .
 						$this->sqlDomainName() . ', ' .
 						$this->sqlDisplayName() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_extension_type_id = ' . $this->sqlPhoneExtensionTypeId() . ','; } elseif( true == array_key_exists( 'PhoneExtensionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_extension_type_id = ' . $this->sqlPhoneExtensionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_name = ' . $this->sqlPhoneName() . ','; } elseif( true == array_key_exists( 'PhoneName', $this->getChangedColumns() ) ) { $strSql .= ' phone_name = ' . $this->sqlPhoneName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extension = ' . $this->sqlExtension() . ','; } elseif( true == array_key_exists( 'Extension', $this->getChangedColumns() ) ) { $strSql .= ' extension = ' . $this->sqlExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voicemail_pin = ' . $this->sqlVoicemailPin() . ','; } elseif( true == array_key_exists( 'VoicemailPin', $this->getChangedColumns() ) ) { $strSql .= ' voicemail_pin = ' . $this->sqlVoicemailPin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' machine_name = ' . $this->sqlMachineName() . ','; } elseif( true == array_key_exists( 'MachineName', $this->getChangedColumns() ) ) { $strSql .= ' machine_name = ' . $this->sqlMachineName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' port_number = ' . $this->sqlPortNumber() . ','; } elseif( true == array_key_exists( 'PortNumber', $this->getChangedColumns() ) ) { $strSql .= ' port_number = ' . $this->sqlPortNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain_name = ' . $this->sqlDomainName() . ','; } elseif( true == array_key_exists( 'DomainName', $this->getChangedColumns() ) ) { $strSql .= ' domain_name = ' . $this->sqlDomainName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_name = ' . $this->sqlDisplayName() . ','; } elseif( true == array_key_exists( 'DisplayName', $this->getChangedColumns() ) ) { $strSql .= ' display_name = ' . $this->sqlDisplayName() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'phone_extension_type_id' => $this->getPhoneExtensionTypeId(),
			'phone_name' => $this->getPhoneName(),
			'extension' => $this->getExtension(),
			'password' => $this->getPassword(),
			'voicemail_pin' => $this->getVoicemailPin(),
			'is_shared' => $this->getIsShared(),
			'machine_name' => $this->getMachineName(),
			'ip_address' => $this->getIpAddress(),
			'port_number' => $this->getPortNumber(),
			'domain_name' => $this->getDomainName(),
			'display_name' => $this->getDisplayName(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>