<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrs
 * Do not add any new functions to this class.
 */

class CBaseIvrs extends CEosPluralBase {

	/**
	 * @return CIvr[]
	 */
	public static function fetchIvrs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIvr', $objDatabase );
	}

	/**
	 * @return CIvr
	 */
	public static function fetchIvr( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIvr', $objDatabase );
	}

	public static function fetchIvrCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivrs', $objDatabase );
	}

	public static function fetchIvrById( $intId, $objDatabase ) {
		return self::fetchIvr( sprintf( 'SELECT * FROM ivrs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIvrsByCid( $intCid, $objDatabase ) {
		return self::fetchIvrs( sprintf( 'SELECT * FROM ivrs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIvrsByIvrTypeId( $intIvrTypeId, $objDatabase ) {
		return self::fetchIvrs( sprintf( 'SELECT * FROM ivrs WHERE ivr_type_id = %d', ( int ) $intIvrTypeId ), $objDatabase );
	}
}
?>