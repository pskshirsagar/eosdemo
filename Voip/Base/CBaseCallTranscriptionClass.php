<?php

class CBaseCallTranscriptionClass extends CEosSingularBase {

	const TABLE_NAME = 'public.call_transcription_classes';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallId;
	protected $m_intApplicantApplicationId;
	protected $m_strCallDatetime;
	protected $m_intCallHour;
	protected $m_intCallDurationSeconds;
	protected $m_strCallResult;
	protected $m_intCallIsAbandoned;
	protected $m_intCallIsLead;
	protected $m_intCallIsMarketSurvey;
	protected $m_intCallIsResident;
	protected $m_intCallIsSolicitor;
	protected $m_intCallIsSupport;
	protected $m_intCallIsVendor;
	protected $m_intCallIsWorkOrder;
	protected $m_intCallIsWrongNumber;
	protected $m_intLeadIsConverted;
	protected $m_strAgent;
	protected $m_strCaller;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['call_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCallDatetime', trim( $arrValues['call_datetime'] ) ); elseif( isset( $arrValues['call_datetime'] ) ) $this->setCallDatetime( $arrValues['call_datetime'] );
		if( isset( $arrValues['call_hour'] ) && $boolDirectSet ) $this->set( 'm_intCallHour', trim( $arrValues['call_hour'] ) ); elseif( isset( $arrValues['call_hour'] ) ) $this->setCallHour( $arrValues['call_hour'] );
		if( isset( $arrValues['call_duration_seconds'] ) && $boolDirectSet ) $this->set( 'm_intCallDurationSeconds', trim( $arrValues['call_duration_seconds'] ) ); elseif( isset( $arrValues['call_duration_seconds'] ) ) $this->setCallDurationSeconds( $arrValues['call_duration_seconds'] );
		if( isset( $arrValues['call_result'] ) && $boolDirectSet ) $this->set( 'm_strCallResult', trim( stripcslashes( $arrValues['call_result'] ) ) ); elseif( isset( $arrValues['call_result'] ) ) $this->setCallResult( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_result'] ) : $arrValues['call_result'] );
		if( isset( $arrValues['call_is_abandoned'] ) && $boolDirectSet ) $this->set( 'm_intCallIsAbandoned', trim( $arrValues['call_is_abandoned'] ) ); elseif( isset( $arrValues['call_is_abandoned'] ) ) $this->setCallIsAbandoned( $arrValues['call_is_abandoned'] );
		if( isset( $arrValues['call_is_lead'] ) && $boolDirectSet ) $this->set( 'm_intCallIsLead', trim( $arrValues['call_is_lead'] ) ); elseif( isset( $arrValues['call_is_lead'] ) ) $this->setCallIsLead( $arrValues['call_is_lead'] );
		if( isset( $arrValues['call_is_market_survey'] ) && $boolDirectSet ) $this->set( 'm_intCallIsMarketSurvey', trim( $arrValues['call_is_market_survey'] ) ); elseif( isset( $arrValues['call_is_market_survey'] ) ) $this->setCallIsMarketSurvey( $arrValues['call_is_market_survey'] );
		if( isset( $arrValues['call_is_resident'] ) && $boolDirectSet ) $this->set( 'm_intCallIsResident', trim( $arrValues['call_is_resident'] ) ); elseif( isset( $arrValues['call_is_resident'] ) ) $this->setCallIsResident( $arrValues['call_is_resident'] );
		if( isset( $arrValues['call_is_solicitor'] ) && $boolDirectSet ) $this->set( 'm_intCallIsSolicitor', trim( $arrValues['call_is_solicitor'] ) ); elseif( isset( $arrValues['call_is_solicitor'] ) ) $this->setCallIsSolicitor( $arrValues['call_is_solicitor'] );
		if( isset( $arrValues['call_is_support'] ) && $boolDirectSet ) $this->set( 'm_intCallIsSupport', trim( $arrValues['call_is_support'] ) ); elseif( isset( $arrValues['call_is_support'] ) ) $this->setCallIsSupport( $arrValues['call_is_support'] );
		if( isset( $arrValues['call_is_vendor'] ) && $boolDirectSet ) $this->set( 'm_intCallIsVendor', trim( $arrValues['call_is_vendor'] ) ); elseif( isset( $arrValues['call_is_vendor'] ) ) $this->setCallIsVendor( $arrValues['call_is_vendor'] );
		if( isset( $arrValues['call_is_work_order'] ) && $boolDirectSet ) $this->set( 'm_intCallIsWorkOrder', trim( $arrValues['call_is_work_order'] ) ); elseif( isset( $arrValues['call_is_work_order'] ) ) $this->setCallIsWorkOrder( $arrValues['call_is_work_order'] );
		if( isset( $arrValues['call_is_wrong_number'] ) && $boolDirectSet ) $this->set( 'm_intCallIsWrongNumber', trim( $arrValues['call_is_wrong_number'] ) ); elseif( isset( $arrValues['call_is_wrong_number'] ) ) $this->setCallIsWrongNumber( $arrValues['call_is_wrong_number'] );
		if( isset( $arrValues['lead_is_converted'] ) && $boolDirectSet ) $this->set( 'm_intLeadIsConverted', trim( $arrValues['lead_is_converted'] ) ); elseif( isset( $arrValues['lead_is_converted'] ) ) $this->setLeadIsConverted( $arrValues['lead_is_converted'] );
		if( isset( $arrValues['agent'] ) && $boolDirectSet ) $this->set( 'm_strAgent', trim( stripcslashes( $arrValues['agent'] ) ) ); elseif( isset( $arrValues['agent'] ) ) $this->setAgent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agent'] ) : $arrValues['agent'] );
		if( isset( $arrValues['caller'] ) && $boolDirectSet ) $this->set( 'm_strCaller', trim( stripcslashes( $arrValues['caller'] ) ) ); elseif( isset( $arrValues['caller'] ) ) $this->setCaller( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caller'] ) : $arrValues['caller'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setCallDatetime( $strCallDatetime ) {
		$this->set( 'm_strCallDatetime', CStrings::strTrimDef( $strCallDatetime, -1, NULL, true ) );
	}

	public function getCallDatetime() {
		return $this->m_strCallDatetime;
	}

	public function sqlCallDatetime() {
		return ( true == isset( $this->m_strCallDatetime ) ) ? '\'' . $this->m_strCallDatetime . '\'' : 'NULL';
	}

	public function setCallHour( $intCallHour ) {
		$this->set( 'm_intCallHour', CStrings::strToIntDef( $intCallHour, NULL, false ) );
	}

	public function getCallHour() {
		return $this->m_intCallHour;
	}

	public function sqlCallHour() {
		return ( true == isset( $this->m_intCallHour ) ) ? ( string ) $this->m_intCallHour : 'NULL';
	}

	public function setCallDurationSeconds( $intCallDurationSeconds ) {
		$this->set( 'm_intCallDurationSeconds', CStrings::strToIntDef( $intCallDurationSeconds, NULL, false ) );
	}

	public function getCallDurationSeconds() {
		return $this->m_intCallDurationSeconds;
	}

	public function sqlCallDurationSeconds() {
		return ( true == isset( $this->m_intCallDurationSeconds ) ) ? ( string ) $this->m_intCallDurationSeconds : 'NULL';
	}

	public function setCallResult( $strCallResult ) {
		$this->set( 'm_strCallResult', CStrings::strTrimDef( $strCallResult, 50, NULL, true ) );
	}

	public function getCallResult() {
		return $this->m_strCallResult;
	}

	public function sqlCallResult() {
		return ( true == isset( $this->m_strCallResult ) ) ? '\'' . addslashes( $this->m_strCallResult ) . '\'' : 'NULL';
	}

	public function setCallIsAbandoned( $intCallIsAbandoned ) {
		$this->set( 'm_intCallIsAbandoned', CStrings::strToIntDef( $intCallIsAbandoned, NULL, false ) );
	}

	public function getCallIsAbandoned() {
		return $this->m_intCallIsAbandoned;
	}

	public function sqlCallIsAbandoned() {
		return ( true == isset( $this->m_intCallIsAbandoned ) ) ? ( string ) $this->m_intCallIsAbandoned : 'NULL';
	}

	public function setCallIsLead( $intCallIsLead ) {
		$this->set( 'm_intCallIsLead', CStrings::strToIntDef( $intCallIsLead, NULL, false ) );
	}

	public function getCallIsLead() {
		return $this->m_intCallIsLead;
	}

	public function sqlCallIsLead() {
		return ( true == isset( $this->m_intCallIsLead ) ) ? ( string ) $this->m_intCallIsLead : 'NULL';
	}

	public function setCallIsMarketSurvey( $intCallIsMarketSurvey ) {
		$this->set( 'm_intCallIsMarketSurvey', CStrings::strToIntDef( $intCallIsMarketSurvey, NULL, false ) );
	}

	public function getCallIsMarketSurvey() {
		return $this->m_intCallIsMarketSurvey;
	}

	public function sqlCallIsMarketSurvey() {
		return ( true == isset( $this->m_intCallIsMarketSurvey ) ) ? ( string ) $this->m_intCallIsMarketSurvey : 'NULL';
	}

	public function setCallIsResident( $intCallIsResident ) {
		$this->set( 'm_intCallIsResident', CStrings::strToIntDef( $intCallIsResident, NULL, false ) );
	}

	public function getCallIsResident() {
		return $this->m_intCallIsResident;
	}

	public function sqlCallIsResident() {
		return ( true == isset( $this->m_intCallIsResident ) ) ? ( string ) $this->m_intCallIsResident : 'NULL';
	}

	public function setCallIsSolicitor( $intCallIsSolicitor ) {
		$this->set( 'm_intCallIsSolicitor', CStrings::strToIntDef( $intCallIsSolicitor, NULL, false ) );
	}

	public function getCallIsSolicitor() {
		return $this->m_intCallIsSolicitor;
	}

	public function sqlCallIsSolicitor() {
		return ( true == isset( $this->m_intCallIsSolicitor ) ) ? ( string ) $this->m_intCallIsSolicitor : 'NULL';
	}

	public function setCallIsSupport( $intCallIsSupport ) {
		$this->set( 'm_intCallIsSupport', CStrings::strToIntDef( $intCallIsSupport, NULL, false ) );
	}

	public function getCallIsSupport() {
		return $this->m_intCallIsSupport;
	}

	public function sqlCallIsSupport() {
		return ( true == isset( $this->m_intCallIsSupport ) ) ? ( string ) $this->m_intCallIsSupport : 'NULL';
	}

	public function setCallIsVendor( $intCallIsVendor ) {
		$this->set( 'm_intCallIsVendor', CStrings::strToIntDef( $intCallIsVendor, NULL, false ) );
	}

	public function getCallIsVendor() {
		return $this->m_intCallIsVendor;
	}

	public function sqlCallIsVendor() {
		return ( true == isset( $this->m_intCallIsVendor ) ) ? ( string ) $this->m_intCallIsVendor : 'NULL';
	}

	public function setCallIsWorkOrder( $intCallIsWorkOrder ) {
		$this->set( 'm_intCallIsWorkOrder', CStrings::strToIntDef( $intCallIsWorkOrder, NULL, false ) );
	}

	public function getCallIsWorkOrder() {
		return $this->m_intCallIsWorkOrder;
	}

	public function sqlCallIsWorkOrder() {
		return ( true == isset( $this->m_intCallIsWorkOrder ) ) ? ( string ) $this->m_intCallIsWorkOrder : 'NULL';
	}

	public function setCallIsWrongNumber( $intCallIsWrongNumber ) {
		$this->set( 'm_intCallIsWrongNumber', CStrings::strToIntDef( $intCallIsWrongNumber, NULL, false ) );
	}

	public function getCallIsWrongNumber() {
		return $this->m_intCallIsWrongNumber;
	}

	public function sqlCallIsWrongNumber() {
		return ( true == isset( $this->m_intCallIsWrongNumber ) ) ? ( string ) $this->m_intCallIsWrongNumber : 'NULL';
	}

	public function setLeadIsConverted( $intLeadIsConverted ) {
		$this->set( 'm_intLeadIsConverted', CStrings::strToIntDef( $intLeadIsConverted, NULL, false ) );
	}

	public function getLeadIsConverted() {
		return $this->m_intLeadIsConverted;
	}

	public function sqlLeadIsConverted() {
		return ( true == isset( $this->m_intLeadIsConverted ) ) ? ( string ) $this->m_intLeadIsConverted : 'NULL';
	}

	public function setAgent( $strAgent ) {
		$this->set( 'm_strAgent', CStrings::strTrimDef( $strAgent, -1, NULL, true ) );
	}

	public function getAgent() {
		return $this->m_strAgent;
	}

	public function sqlAgent() {
		return ( true == isset( $this->m_strAgent ) ) ? '\'' . addslashes( $this->m_strAgent ) . '\'' : 'NULL';
	}

	public function setCaller( $strCaller ) {
		$this->set( 'm_strCaller', CStrings::strTrimDef( $strCaller, -1, NULL, true ) );
	}

	public function getCaller() {
		return $this->m_strCaller;
	}

	public function sqlCaller() {
		return ( true == isset( $this->m_strCaller ) ) ? '\'' . addslashes( $this->m_strCaller ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, call_id, applicant_application_id, call_datetime, call_hour, call_duration_seconds, call_result, call_is_abandoned, call_is_lead, call_is_market_survey, call_is_resident, call_is_solicitor, call_is_support, call_is_vendor, call_is_work_order, call_is_wrong_number, lead_is_converted, agent, caller, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlApplicantApplicationId() . ', ' .
 						$this->sqlCallDatetime() . ', ' .
 						$this->sqlCallHour() . ', ' .
 						$this->sqlCallDurationSeconds() . ', ' .
 						$this->sqlCallResult() . ', ' .
 						$this->sqlCallIsAbandoned() . ', ' .
 						$this->sqlCallIsLead() . ', ' .
 						$this->sqlCallIsMarketSurvey() . ', ' .
 						$this->sqlCallIsResident() . ', ' .
 						$this->sqlCallIsSolicitor() . ', ' .
 						$this->sqlCallIsSupport() . ', ' .
 						$this->sqlCallIsVendor() . ', ' .
 						$this->sqlCallIsWorkOrder() . ', ' .
 						$this->sqlCallIsWrongNumber() . ', ' .
 						$this->sqlLeadIsConverted() . ', ' .
 						$this->sqlAgent() . ', ' .
 						$this->sqlCaller() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime() . ','; } elseif( true == array_key_exists( 'CallDatetime', $this->getChangedColumns() ) ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_hour = ' . $this->sqlCallHour() . ','; } elseif( true == array_key_exists( 'CallHour', $this->getChangedColumns() ) ) { $strSql .= ' call_hour = ' . $this->sqlCallHour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds() . ','; } elseif( true == array_key_exists( 'CallDurationSeconds', $this->getChangedColumns() ) ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_result = ' . $this->sqlCallResult() . ','; } elseif( true == array_key_exists( 'CallResult', $this->getChangedColumns() ) ) { $strSql .= ' call_result = ' . $this->sqlCallResult() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_abandoned = ' . $this->sqlCallIsAbandoned() . ','; } elseif( true == array_key_exists( 'CallIsAbandoned', $this->getChangedColumns() ) ) { $strSql .= ' call_is_abandoned = ' . $this->sqlCallIsAbandoned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_lead = ' . $this->sqlCallIsLead() . ','; } elseif( true == array_key_exists( 'CallIsLead', $this->getChangedColumns() ) ) { $strSql .= ' call_is_lead = ' . $this->sqlCallIsLead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_market_survey = ' . $this->sqlCallIsMarketSurvey() . ','; } elseif( true == array_key_exists( 'CallIsMarketSurvey', $this->getChangedColumns() ) ) { $strSql .= ' call_is_market_survey = ' . $this->sqlCallIsMarketSurvey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_resident = ' . $this->sqlCallIsResident() . ','; } elseif( true == array_key_exists( 'CallIsResident', $this->getChangedColumns() ) ) { $strSql .= ' call_is_resident = ' . $this->sqlCallIsResident() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_solicitor = ' . $this->sqlCallIsSolicitor() . ','; } elseif( true == array_key_exists( 'CallIsSolicitor', $this->getChangedColumns() ) ) { $strSql .= ' call_is_solicitor = ' . $this->sqlCallIsSolicitor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_support = ' . $this->sqlCallIsSupport() . ','; } elseif( true == array_key_exists( 'CallIsSupport', $this->getChangedColumns() ) ) { $strSql .= ' call_is_support = ' . $this->sqlCallIsSupport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_vendor = ' . $this->sqlCallIsVendor() . ','; } elseif( true == array_key_exists( 'CallIsVendor', $this->getChangedColumns() ) ) { $strSql .= ' call_is_vendor = ' . $this->sqlCallIsVendor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_work_order = ' . $this->sqlCallIsWorkOrder() . ','; } elseif( true == array_key_exists( 'CallIsWorkOrder', $this->getChangedColumns() ) ) { $strSql .= ' call_is_work_order = ' . $this->sqlCallIsWorkOrder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_is_wrong_number = ' . $this->sqlCallIsWrongNumber() . ','; } elseif( true == array_key_exists( 'CallIsWrongNumber', $this->getChangedColumns() ) ) { $strSql .= ' call_is_wrong_number = ' . $this->sqlCallIsWrongNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_is_converted = ' . $this->sqlLeadIsConverted() . ','; } elseif( true == array_key_exists( 'LeadIsConverted', $this->getChangedColumns() ) ) { $strSql .= ' lead_is_converted = ' . $this->sqlLeadIsConverted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent = ' . $this->sqlAgent() . ','; } elseif( true == array_key_exists( 'Agent', $this->getChangedColumns() ) ) { $strSql .= ' agent = ' . $this->sqlAgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caller = ' . $this->sqlCaller() . ','; } elseif( true == array_key_exists( 'Caller', $this->getChangedColumns() ) ) { $strSql .= ' caller = ' . $this->sqlCaller() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_id' => $this->getCallId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'call_datetime' => $this->getCallDatetime(),
			'call_hour' => $this->getCallHour(),
			'call_duration_seconds' => $this->getCallDurationSeconds(),
			'call_result' => $this->getCallResult(),
			'call_is_abandoned' => $this->getCallIsAbandoned(),
			'call_is_lead' => $this->getCallIsLead(),
			'call_is_market_survey' => $this->getCallIsMarketSurvey(),
			'call_is_resident' => $this->getCallIsResident(),
			'call_is_solicitor' => $this->getCallIsSolicitor(),
			'call_is_support' => $this->getCallIsSupport(),
			'call_is_vendor' => $this->getCallIsVendor(),
			'call_is_work_order' => $this->getCallIsWorkOrder(),
			'call_is_wrong_number' => $this->getCallIsWrongNumber(),
			'lead_is_converted' => $this->getLeadIsConverted(),
			'agent' => $this->getAgent(),
			'caller' => $this->getCaller(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>