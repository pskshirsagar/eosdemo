<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRankPointMappings
 * Do not add any new functions to this class.
 */

class CBaseRankPointMappings extends CEosPluralBase {

	/**
	 * @return CRankPointMapping[]
	 */
	public static function fetchRankPointMappings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRankPointMapping::class, $objDatabase );
	}

	/**
	 * @return CRankPointMapping
	 */
	public static function fetchRankPointMapping( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRankPointMapping::class, $objDatabase );
	}

	public static function fetchRankPointMappingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'rank_point_mappings', $objDatabase );
	}

	public static function fetchRankPointMappingById( $intId, $objDatabase ) {
		return self::fetchRankPointMapping( sprintf( 'SELECT * FROM rank_point_mappings WHERE id = %d', $intId ), $objDatabase );
	}

}
?>