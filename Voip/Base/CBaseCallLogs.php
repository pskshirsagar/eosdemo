<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallLogs
 * Do not add any new functions to this class.
 */

class CBaseCallLogs extends CEosPluralBase {

	/**
	 * @return CCallLog[]
	 */
	public static function fetchCallLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallLog', $objDatabase );
	}

	/**
	 * @return CCallLog
	 */
	public static function fetchCallLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallLog', $objDatabase );
	}

	public static function fetchCallLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_logs', $objDatabase );
	}

	public static function fetchCallLogById( $intId, $objDatabase ) {
		return self::fetchCallLog( sprintf( 'SELECT * FROM call_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCallLogs( sprintf( 'SELECT * FROM call_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallLogsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallLogs( sprintf( 'SELECT * FROM call_logs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallLogsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchCallLogs( sprintf( 'SELECT * FROM call_logs WHERE reference_id = %d', ( int ) $intReferenceId ), $objDatabase );
	}

}
?>