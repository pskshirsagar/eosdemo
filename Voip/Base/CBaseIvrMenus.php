<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenus
 * Do not add any new functions to this class.
 */

class CBaseIvrMenus extends CEosPluralBase {

	/**
	 * @return CIvrMenu[]
	 */
	public static function fetchIvrMenus( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIvrMenu', $objDatabase );
	}

	/**
	 * @return CIvrMenu
	 */
	public static function fetchIvrMenu( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIvrMenu', $objDatabase );
	}

	public static function fetchIvrMenuCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_menus', $objDatabase );
	}

	public static function fetchIvrMenuById( $intId, $objDatabase ) {
		return self::fetchIvrMenu( sprintf( 'SELECT * FROM ivr_menus WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIvrMenusByCid( $intCid, $objDatabase ) {
		return self::fetchIvrMenus( sprintf( 'SELECT * FROM ivr_menus WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIvrMenusByIvrId( $intIvrId, $objDatabase ) {
		return self::fetchIvrMenus( sprintf( 'SELECT * FROM ivr_menus WHERE ivr_id = %d', ( int ) $intIvrId ), $objDatabase );
	}

	public static function fetchIvrMenusByIvrMenuTypeId( $intIvrMenuTypeId, $objDatabase ) {
		return self::fetchIvrMenus( sprintf( 'SELECT * FROM ivr_menus WHERE ivr_menu_type_id = %d', ( int ) $intIvrMenuTypeId ), $objDatabase );
	}

	public static function fetchIvrMenusByIvrMenuId( $intIvrMenuId, $objDatabase ) {
		return self::fetchIvrMenus( sprintf( 'SELECT * FROM ivr_menus WHERE ivr_menu_id = %d', ( int ) $intIvrMenuId ), $objDatabase );
	}

}
?>