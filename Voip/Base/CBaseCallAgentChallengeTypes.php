<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentChallengeTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentChallengeTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentChallengeType[]
	 */
	public static function fetchCallAgentChallengeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentChallengeType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentChallengeType
	 */
	public static function fetchCallAgentChallengeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentChallengeType::class, $objDatabase );
	}

	public static function fetchCallAgentChallengeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_challenge_types', $objDatabase );
	}

	public static function fetchCallAgentChallengeTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentChallengeType( sprintf( 'SELECT * FROM call_agent_challenge_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>