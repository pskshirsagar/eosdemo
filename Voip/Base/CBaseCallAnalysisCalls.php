<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCalls
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisCalls extends CEosPluralBase {

	/**
	 * @return CCallAnalysisCall[]
	 */
	public static function fetchCallAnalysisCalls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisCall::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisCall
	 */
	public static function fetchCallAnalysisCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisCall::class, $objDatabase );
	}

	public static function fetchCallAnalysisCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_calls', $objDatabase );
	}

	public static function fetchCallAnalysisCallById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisCall( sprintf( 'SELECT * FROM call_analysis_calls WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE call_id = %d', $intCallId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE company_employee_id = %d', $intCompanyEmployeeId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallsByCallAnalysisStatusTypeId( $intCallAnalysisStatusTypeId, $objDatabase ) {
		return self::fetchCallAnalysisCalls( sprintf( 'SELECT * FROM call_analysis_calls WHERE call_analysis_status_type_id = %d', $intCallAnalysisStatusTypeId ), $objDatabase );
	}

}
?>