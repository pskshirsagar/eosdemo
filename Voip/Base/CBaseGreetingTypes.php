<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CGreetingTypes
 * Do not add any new functions to this class.
 */

class CBaseGreetingTypes extends CEosPluralBase {

	/**
	 * @return CGreetingType[]
	 */
	public static function fetchGreetingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGreetingType::class, $objDatabase );
	}

	/**
	 * @return CGreetingType
	 */
	public static function fetchGreetingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGreetingType::class, $objDatabase );
	}

	public static function fetchGreetingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'greeting_types', $objDatabase );
	}

	public static function fetchGreetingTypeById( $intId, $objDatabase ) {
		return self::fetchGreetingType( sprintf( 'SELECT * FROM greeting_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>