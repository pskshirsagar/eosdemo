<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentChallengeSchedules
 * Do not add any new functions to this class.
 */

class CBaseCallAgentChallengeSchedules extends CEosPluralBase {

	/**
	 * @return CCallAgentChallengeSchedule[]
	 */
	public static function fetchCallAgentChallengeSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentChallengeSchedule::class, $objDatabase );
	}

	/**
	 * @return CCallAgentChallengeSchedule
	 */
	public static function fetchCallAgentChallengeSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentChallengeSchedule::class, $objDatabase );
	}

	public static function fetchCallAgentChallengeScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_challenge_schedules', $objDatabase );
	}

	public static function fetchCallAgentChallengeScheduleById( $intId, $objDatabase ) {
		return self::fetchCallAgentChallengeSchedule( sprintf( 'SELECT * FROM call_agent_challenge_schedules WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentChallengeSchedulesByCallAgentChallengeTypeId( $intCallAgentChallengeTypeId, $objDatabase ) {
		return self::fetchCallAgentChallengeSchedules( sprintf( 'SELECT * FROM call_agent_challenge_schedules WHERE call_agent_challenge_type_id = %d', $intCallAgentChallengeTypeId ), $objDatabase );
	}

}
?>