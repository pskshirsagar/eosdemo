<?php

class CBaseCallAnalysisQuestion extends CEosSingularBase {

	const TABLE_NAME = 'public.call_analysis_questions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCallAnalysisCategoryId;
	protected $m_intCallAnalysisQuestionId;
	protected $m_strDescription;
	protected $m_fltWeight;
	protected $m_boolIsText;
	protected $m_boolIsPublished;
	protected $m_intIsDisplay;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCallAnalysisQuestionTypeId;
	protected $m_intCallAnalysisScorecardId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsText = false;
		$this->m_boolIsPublished = true;
		$this->m_intCallAnalysisQuestionTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['call_analysis_category_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisCategoryId', trim( $arrValues['call_analysis_category_id'] ) ); elseif( isset( $arrValues['call_analysis_category_id'] ) ) $this->setCallAnalysisCategoryId( $arrValues['call_analysis_category_id'] );
		if( isset( $arrValues['call_analysis_question_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisQuestionId', trim( $arrValues['call_analysis_question_id'] ) ); elseif( isset( $arrValues['call_analysis_question_id'] ) ) $this->setCallAnalysisQuestionId( $arrValues['call_analysis_question_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['weight'] ) && $boolDirectSet ) $this->set( 'm_fltWeight', trim( $arrValues['weight'] ) ); elseif( isset( $arrValues['weight'] ) ) $this->setWeight( $arrValues['weight'] );
		if( isset( $arrValues['is_text'] ) && $boolDirectSet ) $this->set( 'm_boolIsText', trim( stripcslashes( $arrValues['is_text'] ) ) ); elseif( isset( $arrValues['is_text'] ) ) $this->setIsText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_text'] ) : $arrValues['is_text'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['is_display'] ) && $boolDirectSet ) $this->set( 'm_intIsDisplay', trim( $arrValues['is_display'] ) ); elseif( isset( $arrValues['is_display'] ) ) $this->setIsDisplay( $arrValues['is_display'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['call_analysis_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisQuestionTypeId', trim( $arrValues['call_analysis_question_type_id'] ) ); elseif( isset( $arrValues['call_analysis_question_type_id'] ) ) $this->setCallAnalysisQuestionTypeId( $arrValues['call_analysis_question_type_id'] );
		if( isset( $arrValues['call_analysis_scorecard_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisScorecardId', trim( $arrValues['call_analysis_scorecard_id'] ) ); elseif( isset( $arrValues['call_analysis_scorecard_id'] ) ) $this->setCallAnalysisScorecardId( $arrValues['call_analysis_scorecard_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCallAnalysisCategoryId( $intCallAnalysisCategoryId ) {
		$this->set( 'm_intCallAnalysisCategoryId', CStrings::strToIntDef( $intCallAnalysisCategoryId, NULL, false ) );
	}

	public function getCallAnalysisCategoryId() {
		return $this->m_intCallAnalysisCategoryId;
	}

	public function sqlCallAnalysisCategoryId() {
		return ( true == isset( $this->m_intCallAnalysisCategoryId ) ) ? ( string ) $this->m_intCallAnalysisCategoryId : 'NULL';
	}

	public function setCallAnalysisQuestionId( $intCallAnalysisQuestionId ) {
		$this->set( 'm_intCallAnalysisQuestionId', CStrings::strToIntDef( $intCallAnalysisQuestionId, NULL, false ) );
	}

	public function getCallAnalysisQuestionId() {
		return $this->m_intCallAnalysisQuestionId;
	}

	public function sqlCallAnalysisQuestionId() {
		return ( true == isset( $this->m_intCallAnalysisQuestionId ) ) ? ( string ) $this->m_intCallAnalysisQuestionId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setWeight( $fltWeight ) {
		$this->set( 'm_fltWeight', CStrings::strToFloatDef( $fltWeight, NULL, false, 6 ) );
	}

	public function getWeight() {
		return $this->m_fltWeight;
	}

	public function sqlWeight() {
		return ( true == isset( $this->m_fltWeight ) ) ? ( string ) $this->m_fltWeight : 'NULL';
	}

	public function setIsText( $boolIsText ) {
		$this->set( 'm_boolIsText', CStrings::strToBool( $boolIsText ) );
	}

	public function getIsText() {
		return $this->m_boolIsText;
	}

	public function sqlIsText() {
		return ( true == isset( $this->m_boolIsText ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsText ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisplay( $intIsDisplay ) {
		$this->set( 'm_intIsDisplay', CStrings::strToIntDef( $intIsDisplay, NULL, false ) );
	}

	public function getIsDisplay() {
		return $this->m_intIsDisplay;
	}

	public function sqlIsDisplay() {
		return ( true == isset( $this->m_intIsDisplay ) ) ? ( string ) $this->m_intIsDisplay : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCallAnalysisQuestionTypeId( $intCallAnalysisQuestionTypeId ) {
		$this->set( 'm_intCallAnalysisQuestionTypeId', CStrings::strToIntDef( $intCallAnalysisQuestionTypeId, NULL, false ) );
	}

	public function getCallAnalysisQuestionTypeId() {
		return $this->m_intCallAnalysisQuestionTypeId;
	}

	public function sqlCallAnalysisQuestionTypeId() {
		return ( true == isset( $this->m_intCallAnalysisQuestionTypeId ) ) ? ( string ) $this->m_intCallAnalysisQuestionTypeId : '1';
	}

	public function setCallAnalysisScorecardId( $intCallAnalysisScorecardId ) {
		$this->set( 'm_intCallAnalysisScorecardId', CStrings::strToIntDef( $intCallAnalysisScorecardId, NULL, false ) );
	}

	public function getCallAnalysisScorecardId() {
		return $this->m_intCallAnalysisScorecardId;
	}

	public function sqlCallAnalysisScorecardId() {
		return ( true == isset( $this->m_intCallAnalysisScorecardId ) ) ? ( string ) $this->m_intCallAnalysisScorecardId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, call_analysis_category_id, call_analysis_question_id, description, weight, is_text, is_published, is_display, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, call_analysis_question_type_id, call_analysis_scorecard_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCallAnalysisCategoryId() . ', ' .
						$this->sqlCallAnalysisQuestionId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlWeight() . ', ' .
						$this->sqlIsText() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsDisplay() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCallAnalysisQuestionTypeId() . ', ' .
						$this->sqlCallAnalysisScorecardId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_category_id = ' . $this->sqlCallAnalysisCategoryId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_category_id = ' . $this->sqlCallAnalysisCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_question_id = ' . $this->sqlCallAnalysisQuestionId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_question_id = ' . $this->sqlCallAnalysisQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' weight = ' . $this->sqlWeight(). ',' ; } elseif( true == array_key_exists( 'Weight', $this->getChangedColumns() ) ) { $strSql .= ' weight = ' . $this->sqlWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_text = ' . $this->sqlIsText(). ',' ; } elseif( true == array_key_exists( 'IsText', $this->getChangedColumns() ) ) { $strSql .= ' is_text = ' . $this->sqlIsText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_display = ' . $this->sqlIsDisplay(). ',' ; } elseif( true == array_key_exists( 'IsDisplay', $this->getChangedColumns() ) ) { $strSql .= ' is_display = ' . $this->sqlIsDisplay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_question_type_id = ' . $this->sqlCallAnalysisQuestionTypeId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_question_type_id = ' . $this->sqlCallAnalysisQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_scorecard_id = ' . $this->sqlCallAnalysisScorecardId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisScorecardId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_scorecard_id = ' . $this->sqlCallAnalysisScorecardId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'call_analysis_category_id' => $this->getCallAnalysisCategoryId(),
			'call_analysis_question_id' => $this->getCallAnalysisQuestionId(),
			'description' => $this->getDescription(),
			'weight' => $this->getWeight(),
			'is_text' => $this->getIsText(),
			'is_published' => $this->getIsPublished(),
			'is_display' => $this->getIsDisplay(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'call_analysis_question_type_id' => $this->getCallAnalysisQuestionTypeId(),
			'call_analysis_scorecard_id' => $this->getCallAnalysisScorecardId()
		);
	}

}
?>