<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyCallSettings
 * Do not add any new functions to this class.
 */

class CBasePropertyCallSettings extends CEosPluralBase {

	/**
	 * @return CPropertyCallSetting[]
	 */
	public static function fetchPropertyCallSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyCallSetting', $objDatabase );
	}

	/**
	 * @return CPropertyCallSetting
	 */
	public static function fetchPropertyCallSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyCallSetting', $objDatabase );
	}

	public static function fetchPropertyCallSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_call_settings', $objDatabase );
	}

	public static function fetchPropertyCallSettingById( $intId, $objDatabase ) {
		return self::fetchPropertyCallSetting( sprintf( 'SELECT * FROM property_call_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyCallSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCallSettings( sprintf( 'SELECT * FROM property_call_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCallSettingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyCallSettings( sprintf( 'SELECT * FROM property_call_settings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchPropertyCallSettingsBySettingsTemplateId( $intSettingsTemplateId, $objDatabase ) {
		return self::fetchPropertyCallSettings( sprintf( 'SELECT * FROM property_call_settings WHERE settings_template_id = %d', ( int ) $intSettingsTemplateId ), $objDatabase );
	}

	public static function fetchPropertyCallSettingsByTimeZoneId( $intTimeZoneId, $objDatabase ) {
		return self::fetchPropertyCallSettings( sprintf( 'SELECT * FROM property_call_settings WHERE time_zone_id = %d', ( int ) $intTimeZoneId ), $objDatabase );
	}

}
?>