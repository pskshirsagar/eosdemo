<?php

class CBaseCallAgentLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_logs';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intCallId;
	protected $m_intCallAgentStatusTypeId;
	protected $m_intCallAgentStateTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['call_agent_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentStatusTypeId', trim( $arrValues['call_agent_status_type_id'] ) ); elseif( isset( $arrValues['call_agent_status_type_id'] ) ) $this->setCallAgentStatusTypeId( $arrValues['call_agent_status_type_id'] );
		if( isset( $arrValues['call_agent_state_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentStateTypeId', trim( $arrValues['call_agent_state_type_id'] ) ); elseif( isset( $arrValues['call_agent_state_type_id'] ) ) $this->setCallAgentStateTypeId( $arrValues['call_agent_state_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setCallAgentStatusTypeId( $intCallAgentStatusTypeId ) {
		$this->set( 'm_intCallAgentStatusTypeId', CStrings::strToIntDef( $intCallAgentStatusTypeId, NULL, false ) );
	}

	public function getCallAgentStatusTypeId() {
		return $this->m_intCallAgentStatusTypeId;
	}

	public function sqlCallAgentStatusTypeId() {
		return ( true == isset( $this->m_intCallAgentStatusTypeId ) ) ? ( string ) $this->m_intCallAgentStatusTypeId : 'NULL';
	}

	public function setCallAgentStateTypeId( $intCallAgentStateTypeId ) {
		$this->set( 'm_intCallAgentStateTypeId', CStrings::strToIntDef( $intCallAgentStateTypeId, NULL, false ) );
	}

	public function getCallAgentStateTypeId() {
		return $this->m_intCallAgentStateTypeId;
	}

	public function sqlCallAgentStateTypeId() {
		return ( true == isset( $this->m_intCallAgentStateTypeId ) ) ? ( string ) $this->m_intCallAgentStateTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, call_id, call_agent_status_type_id, call_agent_state_type_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlCallAgentStatusTypeId() . ', ' .
 						$this->sqlCallAgentStateTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId() . ','; } elseif( true == array_key_exists( 'CallAgentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_state_type_id = ' . $this->sqlCallAgentStateTypeId() . ','; } elseif( true == array_key_exists( 'CallAgentStateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_state_type_id = ' . $this->sqlCallAgentStateTypeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_id' => $this->getCallId(),
			'call_agent_status_type_id' => $this->getCallAgentStatusTypeId(),
			'call_agent_state_type_id' => $this->getCallAgentStateTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>