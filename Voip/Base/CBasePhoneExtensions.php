<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPhoneExtensions
 * Do not add any new functions to this class.
 */

class CBasePhoneExtensions extends CEosPluralBase {

	/**
	 * @return CPhoneExtension[]
	 */
	public static function fetchPhoneExtensions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPhoneExtension', $objDatabase );
	}

	/**
	 * @return CPhoneExtension
	 */
	public static function fetchPhoneExtension( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPhoneExtension', $objDatabase );
	}

	public static function fetchPhoneExtensionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'phone_extensions', $objDatabase );
	}

	public static function fetchPhoneExtensionById( $intId, $objDatabase ) {
		return self::fetchPhoneExtension( sprintf( 'SELECT * FROM phone_extensions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPhoneExtensionsByPhoneExtensionTypeId( $intPhoneExtensionTypeId, $objDatabase ) {
		return self::fetchPhoneExtensions( sprintf( 'SELECT * FROM phone_extensions WHERE phone_extension_type_id = %d', ( int ) $intPhoneExtensionTypeId ), $objDatabase );
	}

}
?>