<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFilters
 * Do not add any new functions to this class.
 */

class CBaseCallFilters extends CEosPluralBase {

	/**
	 * @return CCallFilter[]
	 */
	public static function fetchCallFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallFilter', $objDatabase );
	}

	/**
	 * @return CCallFilter
	 */
	public static function fetchCallFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallFilter', $objDatabase );
	}

	public static function fetchCallFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_filters', $objDatabase );
	}

	public static function fetchCallFilterById( $intId, $objDatabase ) {
		return self::fetchCallFilter( sprintf( 'SELECT * FROM call_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallFiltersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCallFilters( sprintf( 'SELECT * FROM call_filters WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallFiltersByCallFilterTypeId( $intCallFilterTypeId, $objDatabase ) {
		return self::fetchCallFilters( sprintf( 'SELECT * FROM call_filters WHERE call_filter_type_id = %d', ( int ) $intCallFilterTypeId ), $objDatabase );
	}

	public static function fetchCallFiltersByCallZoneId( $intCallZoneId, $objDatabase ) {
		return self::fetchCallFilters( sprintf( 'SELECT * FROM call_filters WHERE call_zone_id = %d', ( int ) $intCallZoneId ), $objDatabase );
	}

}
?>