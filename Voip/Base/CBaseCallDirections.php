<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallDirections
 * Do not add any new functions to this class.
 */

class CBaseCallDirections extends CEosPluralBase {

	/**
	 * @return CCallDirection[]
	 */
	public static function fetchCallDirections( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallDirection::class, $objDatabase );
	}

	/**
	 * @return CCallDirection
	 */
	public static function fetchCallDirection( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallDirection::class, $objDatabase );
	}

	public static function fetchCallDirectionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_directions', $objDatabase );
	}

	public static function fetchCallDirectionById( $intId, $objDatabase ) {
		return self::fetchCallDirection( sprintf( 'SELECT * FROM call_directions WHERE id = %d', $intId ), $objDatabase );
	}

}
?>