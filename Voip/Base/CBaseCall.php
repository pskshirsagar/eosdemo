<?php

class CBaseCall extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.calls';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intRedirectPropertyId;
	protected $m_intCallPhoneNumberId;
	protected $m_intCallTypeId;
	protected $m_intCallFileId;
	protected $m_intCallStatusTypeId;
	protected $m_intCallResponseTypeId;
	protected $m_intCallResultId;
	protected $m_intCallPriorityId;
	protected $m_intVoiceTypeId;
	protected $m_intCompanyEmployeeId;
	protected $m_intEmployeeId;
	protected $m_intCustomerId;
	protected $m_intApplicantApplicationId;
	protected $m_intLeadSourceId;
	protected $m_intScheduledCallId;
	protected $m_intScheduledCallTypeId;
	protected $m_intOutboundCallId;
	protected $m_intCallFormStatusTypeId;
	protected $m_strSessionUuid;
	protected $m_intCallQueueId;
	protected $m_intCallAgentId;
	protected $m_intCallAnalysisStatusTypeId;
	protected $m_intIvrMenuActionId;
	protected $m_strDestinationPhoneNumber;
	protected $m_strDestinationExtension;
	protected $m_strOriginPhoneNumber;
	protected $m_strOriginExtension;
	protected $m_strScheduledSendDatetime;
	protected $m_strCallDatetime;
	protected $m_strSyncedOn;
	protected $m_intCallDurationSeconds;
	protected $m_intCallCenterQueueTime;
	protected $m_intCallCenterTalkTime;
	protected $m_intWrapUpTime;
	protected $m_strCallNotes;
	protected $m_strCallerName;
	protected $m_strBeginSendTime;
	protected $m_strEndSendTime;
	protected $m_intAllowBlock;
	protected $m_intCallToday;
	protected $m_intIsAutoDetected;
	protected $m_intIsVoiceMail;
	protected $m_intIsTestCall;
	protected $m_intAttemptCount;
	protected $m_strVoiceMailReviewedOn;
	protected $m_strSentOn;
	protected $m_strFailedOn;
	protected $m_intAnalyzedBy;
	protected $m_strAnalyzedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCallIvrQueueTime;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strOriginPhoneNumber = '52211816';
		$this->m_intAllowBlock = '0';
		$this->m_intCallToday = '0';
		$this->m_intIsAutoDetected = '0';
		$this->m_intIsVoiceMail = '0';
		$this->m_intIsTestCall = '0';
		$this->m_intAttemptCount = '0';
		$this->m_intCallIvrQueueTime = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['redirect_property_id'] ) && $boolDirectSet ) $this->set( 'm_intRedirectPropertyId', trim( $arrValues['redirect_property_id'] ) ); elseif( isset( $arrValues['redirect_property_id'] ) ) $this->setRedirectPropertyId( $arrValues['redirect_property_id'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallTypeId', trim( $arrValues['call_type_id'] ) ); elseif( isset( $arrValues['call_type_id'] ) ) $this->setCallTypeId( $arrValues['call_type_id'] );
		if( isset( $arrValues['call_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFileId', trim( $arrValues['call_file_id'] ) ); elseif( isset( $arrValues['call_file_id'] ) ) $this->setCallFileId( $arrValues['call_file_id'] );
		if( isset( $arrValues['call_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallStatusTypeId', trim( $arrValues['call_status_type_id'] ) ); elseif( isset( $arrValues['call_status_type_id'] ) ) $this->setCallStatusTypeId( $arrValues['call_status_type_id'] );
		if( isset( $arrValues['call_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallResponseTypeId', trim( $arrValues['call_response_type_id'] ) ); elseif( isset( $arrValues['call_response_type_id'] ) ) $this->setCallResponseTypeId( $arrValues['call_response_type_id'] );
		if( isset( $arrValues['call_result_id'] ) && $boolDirectSet ) $this->set( 'm_intCallResultId', trim( $arrValues['call_result_id'] ) ); elseif( isset( $arrValues['call_result_id'] ) ) $this->setCallResultId( $arrValues['call_result_id'] );
		if( isset( $arrValues['call_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPriorityId', trim( $arrValues['call_priority_id'] ) ); elseif( isset( $arrValues['call_priority_id'] ) ) $this->setCallPriorityId( $arrValues['call_priority_id'] );
		if( isset( $arrValues['voice_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVoiceTypeId', trim( $arrValues['voice_type_id'] ) ); elseif( isset( $arrValues['voice_type_id'] ) ) $this->setVoiceTypeId( $arrValues['voice_type_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['applicant_application_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantApplicationId', trim( $arrValues['applicant_application_id'] ) ); elseif( isset( $arrValues['applicant_application_id'] ) ) $this->setApplicantApplicationId( $arrValues['applicant_application_id'] );
		if( isset( $arrValues['lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadSourceId', trim( $arrValues['lead_source_id'] ) ); elseif( isset( $arrValues['lead_source_id'] ) ) $this->setLeadSourceId( $arrValues['lead_source_id'] );
		if( isset( $arrValues['scheduled_call_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledCallId', trim( $arrValues['scheduled_call_id'] ) ); elseif( isset( $arrValues['scheduled_call_id'] ) ) $this->setScheduledCallId( $arrValues['scheduled_call_id'] );
		if( isset( $arrValues['scheduled_call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledCallTypeId', trim( $arrValues['scheduled_call_type_id'] ) ); elseif( isset( $arrValues['scheduled_call_type_id'] ) ) $this->setScheduledCallTypeId( $arrValues['scheduled_call_type_id'] );
		if( isset( $arrValues['outbound_call_id'] ) && $boolDirectSet ) $this->set( 'm_intOutboundCallId', trim( $arrValues['outbound_call_id'] ) ); elseif( isset( $arrValues['outbound_call_id'] ) ) $this->setOutboundCallId( $arrValues['outbound_call_id'] );
		if( isset( $arrValues['call_form_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFormStatusTypeId', trim( $arrValues['call_form_status_type_id'] ) ); elseif( isset( $arrValues['call_form_status_type_id'] ) ) $this->setCallFormStatusTypeId( $arrValues['call_form_status_type_id'] );
		if( isset( $arrValues['session_uuid'] ) && $boolDirectSet ) $this->set( 'm_strSessionUuid', trim( $arrValues['session_uuid'] ) ); elseif( isset( $arrValues['session_uuid'] ) ) $this->setSessionUuid( $arrValues['session_uuid'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_analysis_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAnalysisStatusTypeId', trim( $arrValues['call_analysis_status_type_id'] ) ); elseif( isset( $arrValues['call_analysis_status_type_id'] ) ) $this->setCallAnalysisStatusTypeId( $arrValues['call_analysis_status_type_id'] );
		if( isset( $arrValues['ivr_menu_action_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuActionId', trim( $arrValues['ivr_menu_action_id'] ) ); elseif( isset( $arrValues['ivr_menu_action_id'] ) ) $this->setIvrMenuActionId( $arrValues['ivr_menu_action_id'] );
		if( isset( $arrValues['destination_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strDestinationPhoneNumber', trim( stripcslashes( $arrValues['destination_phone_number'] ) ) ); elseif( isset( $arrValues['destination_phone_number'] ) ) $this->setDestinationPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['destination_phone_number'] ) : $arrValues['destination_phone_number'] );
		if( isset( $arrValues['destination_extension'] ) && $boolDirectSet ) $this->set( 'm_strDestinationExtension', trim( stripcslashes( $arrValues['destination_extension'] ) ) ); elseif( isset( $arrValues['destination_extension'] ) ) $this->setDestinationExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['destination_extension'] ) : $arrValues['destination_extension'] );
		if( isset( $arrValues['origin_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strOriginPhoneNumber', trim( stripcslashes( $arrValues['origin_phone_number'] ) ) ); elseif( isset( $arrValues['origin_phone_number'] ) ) $this->setOriginPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['origin_phone_number'] ) : $arrValues['origin_phone_number'] );
		if( isset( $arrValues['origin_extension'] ) && $boolDirectSet ) $this->set( 'm_strOriginExtension', trim( stripcslashes( $arrValues['origin_extension'] ) ) ); elseif( isset( $arrValues['origin_extension'] ) ) $this->setOriginExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['origin_extension'] ) : $arrValues['origin_extension'] );
		if( isset( $arrValues['scheduled_send_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledSendDatetime', trim( $arrValues['scheduled_send_datetime'] ) ); elseif( isset( $arrValues['scheduled_send_datetime'] ) ) $this->setScheduledSendDatetime( $arrValues['scheduled_send_datetime'] );
		if( isset( $arrValues['call_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCallDatetime', trim( $arrValues['call_datetime'] ) ); elseif( isset( $arrValues['call_datetime'] ) ) $this->setCallDatetime( $arrValues['call_datetime'] );
		if( isset( $arrValues['synced_on'] ) && $boolDirectSet ) $this->set( 'm_strSyncedOn', trim( $arrValues['synced_on'] ) ); elseif( isset( $arrValues['synced_on'] ) ) $this->setSyncedOn( $arrValues['synced_on'] );
		if( isset( $arrValues['call_duration_seconds'] ) && $boolDirectSet ) $this->set( 'm_intCallDurationSeconds', trim( $arrValues['call_duration_seconds'] ) ); elseif( isset( $arrValues['call_duration_seconds'] ) ) $this->setCallDurationSeconds( $arrValues['call_duration_seconds'] );
		if( isset( $arrValues['call_center_queue_time'] ) && $boolDirectSet ) $this->set( 'm_intCallCenterQueueTime', trim( $arrValues['call_center_queue_time'] ) ); elseif( isset( $arrValues['call_center_queue_time'] ) ) $this->setCallCenterQueueTime( $arrValues['call_center_queue_time'] );
		if( isset( $arrValues['call_center_talk_time'] ) && $boolDirectSet ) $this->set( 'm_intCallCenterTalkTime', trim( $arrValues['call_center_talk_time'] ) ); elseif( isset( $arrValues['call_center_talk_time'] ) ) $this->setCallCenterTalkTime( $arrValues['call_center_talk_time'] );
		if( isset( $arrValues['wrap_up_time'] ) && $boolDirectSet ) $this->set( 'm_intWrapUpTime', trim( $arrValues['wrap_up_time'] ) ); elseif( isset( $arrValues['wrap_up_time'] ) ) $this->setWrapUpTime( $arrValues['wrap_up_time'] );
		if( isset( $arrValues['call_notes'] ) && $boolDirectSet ) $this->set( 'm_strCallNotes', trim( stripcslashes( $arrValues['call_notes'] ) ) ); elseif( isset( $arrValues['call_notes'] ) ) $this->setCallNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_notes'] ) : $arrValues['call_notes'] );
		if( isset( $arrValues['caller_name'] ) && $boolDirectSet ) $this->set( 'm_strCallerName', trim( stripcslashes( $arrValues['caller_name'] ) ) ); elseif( isset( $arrValues['caller_name'] ) ) $this->setCallerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caller_name'] ) : $arrValues['caller_name'] );
		if( isset( $arrValues['begin_send_time'] ) && $boolDirectSet ) $this->set( 'm_strBeginSendTime', trim( $arrValues['begin_send_time'] ) ); elseif( isset( $arrValues['begin_send_time'] ) ) $this->setBeginSendTime( $arrValues['begin_send_time'] );
		if( isset( $arrValues['end_send_time'] ) && $boolDirectSet ) $this->set( 'm_strEndSendTime', trim( $arrValues['end_send_time'] ) ); elseif( isset( $arrValues['end_send_time'] ) ) $this->setEndSendTime( $arrValues['end_send_time'] );
		if( isset( $arrValues['allow_block'] ) && $boolDirectSet ) $this->set( 'm_intAllowBlock', trim( $arrValues['allow_block'] ) ); elseif( isset( $arrValues['allow_block'] ) ) $this->setAllowBlock( $arrValues['allow_block'] );
		if( isset( $arrValues['call_today'] ) && $boolDirectSet ) $this->set( 'm_intCallToday', trim( $arrValues['call_today'] ) ); elseif( isset( $arrValues['call_today'] ) ) $this->setCallToday( $arrValues['call_today'] );
		if( isset( $arrValues['is_auto_detected'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoDetected', trim( $arrValues['is_auto_detected'] ) ); elseif( isset( $arrValues['is_auto_detected'] ) ) $this->setIsAutoDetected( $arrValues['is_auto_detected'] );
		if( isset( $arrValues['is_voice_mail'] ) && $boolDirectSet ) $this->set( 'm_intIsVoiceMail', trim( $arrValues['is_voice_mail'] ) ); elseif( isset( $arrValues['is_voice_mail'] ) ) $this->setIsVoiceMail( $arrValues['is_voice_mail'] );
		if( isset( $arrValues['is_test_call'] ) && $boolDirectSet ) $this->set( 'm_intIsTestCall', trim( $arrValues['is_test_call'] ) ); elseif( isset( $arrValues['is_test_call'] ) ) $this->setIsTestCall( $arrValues['is_test_call'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['voice_mail_reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strVoiceMailReviewedOn', trim( $arrValues['voice_mail_reviewed_on'] ) ); elseif( isset( $arrValues['voice_mail_reviewed_on'] ) ) $this->setVoiceMailReviewedOn( $arrValues['voice_mail_reviewed_on'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['analyzed_by'] ) && $boolDirectSet ) $this->set( 'm_intAnalyzedBy', trim( $arrValues['analyzed_by'] ) ); elseif( isset( $arrValues['analyzed_by'] ) ) $this->setAnalyzedBy( $arrValues['analyzed_by'] );
		if( isset( $arrValues['analyzed_on'] ) && $boolDirectSet ) $this->set( 'm_strAnalyzedOn', trim( $arrValues['analyzed_on'] ) ); elseif( isset( $arrValues['analyzed_on'] ) ) $this->setAnalyzedOn( $arrValues['analyzed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['call_ivr_queue_time'] ) && $boolDirectSet ) $this->set( 'm_intCallIvrQueueTime', trim( $arrValues['call_ivr_queue_time'] ) ); elseif( isset( $arrValues['call_ivr_queue_time'] ) ) $this->setCallIvrQueueTime( $arrValues['call_ivr_queue_time'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setRedirectPropertyId( $intRedirectPropertyId ) {
		$this->set( 'm_intRedirectPropertyId', CStrings::strToIntDef( $intRedirectPropertyId, NULL, false ) );
	}

	public function getRedirectPropertyId() {
		return $this->m_intRedirectPropertyId;
	}

	public function sqlRedirectPropertyId() {
		return ( true == isset( $this->m_intRedirectPropertyId ) ) ? ( string ) $this->m_intRedirectPropertyId : 'NULL';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setCallTypeId( $intCallTypeId ) {
		$this->set( 'm_intCallTypeId', CStrings::strToIntDef( $intCallTypeId, NULL, false ) );
	}

	public function getCallTypeId() {
		return $this->m_intCallTypeId;
	}

	public function sqlCallTypeId() {
		return ( true == isset( $this->m_intCallTypeId ) ) ? ( string ) $this->m_intCallTypeId : 'NULL';
	}

	public function setCallFileId( $intCallFileId ) {
		$this->set( 'm_intCallFileId', CStrings::strToIntDef( $intCallFileId, NULL, false ) );
	}

	public function getCallFileId() {
		return $this->m_intCallFileId;
	}

	public function sqlCallFileId() {
		return ( true == isset( $this->m_intCallFileId ) ) ? ( string ) $this->m_intCallFileId : 'NULL';
	}

	public function setCallStatusTypeId( $intCallStatusTypeId ) {
		$this->set( 'm_intCallStatusTypeId', CStrings::strToIntDef( $intCallStatusTypeId, NULL, false ) );
	}

	public function getCallStatusTypeId() {
		return $this->m_intCallStatusTypeId;
	}

	public function sqlCallStatusTypeId() {
		return ( true == isset( $this->m_intCallStatusTypeId ) ) ? ( string ) $this->m_intCallStatusTypeId : 'NULL';
	}

	public function setCallResponseTypeId( $intCallResponseTypeId ) {
		$this->set( 'm_intCallResponseTypeId', CStrings::strToIntDef( $intCallResponseTypeId, NULL, false ) );
	}

	public function getCallResponseTypeId() {
		return $this->m_intCallResponseTypeId;
	}

	public function sqlCallResponseTypeId() {
		return ( true == isset( $this->m_intCallResponseTypeId ) ) ? ( string ) $this->m_intCallResponseTypeId : 'NULL';
	}

	public function setCallResultId( $intCallResultId ) {
		$this->set( 'm_intCallResultId', CStrings::strToIntDef( $intCallResultId, NULL, false ) );
	}

	public function getCallResultId() {
		return $this->m_intCallResultId;
	}

	public function sqlCallResultId() {
		return ( true == isset( $this->m_intCallResultId ) ) ? ( string ) $this->m_intCallResultId : 'NULL';
	}

	public function setCallPriorityId( $intCallPriorityId ) {
		$this->set( 'm_intCallPriorityId', CStrings::strToIntDef( $intCallPriorityId, NULL, false ) );
	}

	public function getCallPriorityId() {
		return $this->m_intCallPriorityId;
	}

	public function sqlCallPriorityId() {
		return ( true == isset( $this->m_intCallPriorityId ) ) ? ( string ) $this->m_intCallPriorityId : 'NULL';
	}

	public function setVoiceTypeId( $intVoiceTypeId ) {
		$this->set( 'm_intVoiceTypeId', CStrings::strToIntDef( $intVoiceTypeId, NULL, false ) );
	}

	public function getVoiceTypeId() {
		return $this->m_intVoiceTypeId;
	}

	public function sqlVoiceTypeId() {
		return ( true == isset( $this->m_intVoiceTypeId ) ) ? ( string ) $this->m_intVoiceTypeId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setApplicantApplicationId( $intApplicantApplicationId ) {
		$this->set( 'm_intApplicantApplicationId', CStrings::strToIntDef( $intApplicantApplicationId, NULL, false ) );
	}

	public function getApplicantApplicationId() {
		return $this->m_intApplicantApplicationId;
	}

	public function sqlApplicantApplicationId() {
		return ( true == isset( $this->m_intApplicantApplicationId ) ) ? ( string ) $this->m_intApplicantApplicationId : 'NULL';
	}

	public function setLeadSourceId( $intLeadSourceId ) {
		$this->set( 'm_intLeadSourceId', CStrings::strToIntDef( $intLeadSourceId, NULL, false ) );
	}

	public function getLeadSourceId() {
		return $this->m_intLeadSourceId;
	}

	public function sqlLeadSourceId() {
		return ( true == isset( $this->m_intLeadSourceId ) ) ? ( string ) $this->m_intLeadSourceId : 'NULL';
	}

	public function setScheduledCallId( $intScheduledCallId ) {
		$this->set( 'm_intScheduledCallId', CStrings::strToIntDef( $intScheduledCallId, NULL, false ) );
	}

	public function getScheduledCallId() {
		return $this->m_intScheduledCallId;
	}

	public function sqlScheduledCallId() {
		return ( true == isset( $this->m_intScheduledCallId ) ) ? ( string ) $this->m_intScheduledCallId : 'NULL';
	}

	public function setScheduledCallTypeId( $intScheduledCallTypeId ) {
		$this->set( 'm_intScheduledCallTypeId', CStrings::strToIntDef( $intScheduledCallTypeId, NULL, false ) );
	}

	public function getScheduledCallTypeId() {
		return $this->m_intScheduledCallTypeId;
	}

	public function sqlScheduledCallTypeId() {
		return ( true == isset( $this->m_intScheduledCallTypeId ) ) ? ( string ) $this->m_intScheduledCallTypeId : 'NULL';
	}

	public function setOutboundCallId( $intOutboundCallId ) {
		$this->set( 'm_intOutboundCallId', CStrings::strToIntDef( $intOutboundCallId, NULL, false ) );
	}

	public function getOutboundCallId() {
		return $this->m_intOutboundCallId;
	}

	public function sqlOutboundCallId() {
		return ( true == isset( $this->m_intOutboundCallId ) ) ? ( string ) $this->m_intOutboundCallId : 'NULL';
	}

	public function setCallFormStatusTypeId( $intCallFormStatusTypeId ) {
		$this->set( 'm_intCallFormStatusTypeId', CStrings::strToIntDef( $intCallFormStatusTypeId, NULL, false ) );
	}

	public function getCallFormStatusTypeId() {
		return $this->m_intCallFormStatusTypeId;
	}

	public function sqlCallFormStatusTypeId() {
		return ( true == isset( $this->m_intCallFormStatusTypeId ) ) ? ( string ) $this->m_intCallFormStatusTypeId : 'NULL';
	}

	public function setSessionUuid( $strSessionUuid ) {
		$this->set( 'm_strSessionUuid', CStrings::strTrimDef( $strSessionUuid, 36, NULL, true ) );
	}

	public function getSessionUuid() {
		return $this->m_strSessionUuid;
	}

	public function sqlSessionUuid() {
		return ( true == isset( $this->m_strSessionUuid ) ) ? '\'' . addslashes( $this->m_strSessionUuid ) . '\'' : 'NULL';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallAnalysisStatusTypeId( $intCallAnalysisStatusTypeId ) {
		$this->set( 'm_intCallAnalysisStatusTypeId', CStrings::strToIntDef( $intCallAnalysisStatusTypeId, NULL, false ) );
	}

	public function getCallAnalysisStatusTypeId() {
		return $this->m_intCallAnalysisStatusTypeId;
	}

	public function sqlCallAnalysisStatusTypeId() {
		return ( true == isset( $this->m_intCallAnalysisStatusTypeId ) ) ? ( string ) $this->m_intCallAnalysisStatusTypeId : 'NULL';
	}

	public function setIvrMenuActionId( $intIvrMenuActionId ) {
		$this->set( 'm_intIvrMenuActionId', CStrings::strToIntDef( $intIvrMenuActionId, NULL, false ) );
	}

	public function getIvrMenuActionId() {
		return $this->m_intIvrMenuActionId;
	}

	public function sqlIvrMenuActionId() {
		return ( true == isset( $this->m_intIvrMenuActionId ) ) ? ( string ) $this->m_intIvrMenuActionId : 'NULL';
	}

	public function setDestinationPhoneNumber( $strDestinationPhoneNumber ) {
		$this->set( 'm_strDestinationPhoneNumber', CStrings::strTrimDef( $strDestinationPhoneNumber, 30, NULL, true ) );
	}

	public function getDestinationPhoneNumber() {
		return $this->m_strDestinationPhoneNumber;
	}

	public function sqlDestinationPhoneNumber() {
		return ( true == isset( $this->m_strDestinationPhoneNumber ) ) ? '\'' . addslashes( $this->m_strDestinationPhoneNumber ) . '\'' : 'NULL';
	}

	public function setDestinationExtension( $strDestinationExtension ) {
		$this->set( 'm_strDestinationExtension', CStrings::strTrimDef( $strDestinationExtension, 10, NULL, true ) );
	}

	public function getDestinationExtension() {
		return $this->m_strDestinationExtension;
	}

	public function sqlDestinationExtension() {
		return ( true == isset( $this->m_strDestinationExtension ) ) ? '\'' . addslashes( $this->m_strDestinationExtension ) . '\'' : 'NULL';
	}

	public function setOriginPhoneNumber( $strOriginPhoneNumber ) {
		$this->set( 'm_strOriginPhoneNumber', CStrings::strTrimDef( $strOriginPhoneNumber, 30, NULL, true ) );
	}

	public function getOriginPhoneNumber() {
		return $this->m_strOriginPhoneNumber;
	}

	public function sqlOriginPhoneNumber() {
		return ( true == isset( $this->m_strOriginPhoneNumber ) ) ? '\'' . addslashes( $this->m_strOriginPhoneNumber ) . '\'' : '52211816';
	}

	public function setOriginExtension( $strOriginExtension ) {
		$this->set( 'm_strOriginExtension', CStrings::strTrimDef( $strOriginExtension, 10, NULL, true ) );
	}

	public function getOriginExtension() {
		return $this->m_strOriginExtension;
	}

	public function sqlOriginExtension() {
		return ( true == isset( $this->m_strOriginExtension ) ) ? '\'' . addslashes( $this->m_strOriginExtension ) . '\'' : 'NULL';
	}

	public function setScheduledSendDatetime( $strScheduledSendDatetime ) {
		$this->set( 'm_strScheduledSendDatetime', CStrings::strTrimDef( $strScheduledSendDatetime, -1, NULL, true ) );
	}

	public function getScheduledSendDatetime() {
		return $this->m_strScheduledSendDatetime;
	}

	public function sqlScheduledSendDatetime() {
		return ( true == isset( $this->m_strScheduledSendDatetime ) ) ? '\'' . $this->m_strScheduledSendDatetime . '\'' : 'NULL';
	}

	public function setCallDatetime( $strCallDatetime ) {
		$this->set( 'm_strCallDatetime', CStrings::strTrimDef( $strCallDatetime, -1, NULL, true ) );
	}

	public function getCallDatetime() {
		return $this->m_strCallDatetime;
	}

	public function sqlCallDatetime() {
		return ( true == isset( $this->m_strCallDatetime ) ) ? '\'' . $this->m_strCallDatetime . '\'' : 'NOW()';
	}

	public function setSyncedOn( $strSyncedOn ) {
		$this->set( 'm_strSyncedOn', CStrings::strTrimDef( $strSyncedOn, -1, NULL, true ) );
	}

	public function getSyncedOn() {
		return $this->m_strSyncedOn;
	}

	public function sqlSyncedOn() {
		return ( true == isset( $this->m_strSyncedOn ) ) ? '\'' . $this->m_strSyncedOn . '\'' : 'NULL';
	}

	public function setCallDurationSeconds( $intCallDurationSeconds ) {
		$this->set( 'm_intCallDurationSeconds', CStrings::strToIntDef( $intCallDurationSeconds, NULL, false ) );
	}

	public function getCallDurationSeconds() {
		return $this->m_intCallDurationSeconds;
	}

	public function sqlCallDurationSeconds() {
		return ( true == isset( $this->m_intCallDurationSeconds ) ) ? ( string ) $this->m_intCallDurationSeconds : 'NULL';
	}

	public function setCallCenterQueueTime( $intCallCenterQueueTime ) {
		$this->set( 'm_intCallCenterQueueTime', CStrings::strToIntDef( $intCallCenterQueueTime, NULL, false ) );
	}

	public function getCallCenterQueueTime() {
		return $this->m_intCallCenterQueueTime;
	}

	public function sqlCallCenterQueueTime() {
		return ( true == isset( $this->m_intCallCenterQueueTime ) ) ? ( string ) $this->m_intCallCenterQueueTime : 'NULL';
	}

	public function setCallCenterTalkTime( $intCallCenterTalkTime ) {
		$this->set( 'm_intCallCenterTalkTime', CStrings::strToIntDef( $intCallCenterTalkTime, NULL, false ) );
	}

	public function getCallCenterTalkTime() {
		return $this->m_intCallCenterTalkTime;
	}

	public function sqlCallCenterTalkTime() {
		return ( true == isset( $this->m_intCallCenterTalkTime ) ) ? ( string ) $this->m_intCallCenterTalkTime : 'NULL';
	}

	public function setWrapUpTime( $intWrapUpTime ) {
		$this->set( 'm_intWrapUpTime', CStrings::strToIntDef( $intWrapUpTime, NULL, false ) );
	}

	public function getWrapUpTime() {
		return $this->m_intWrapUpTime;
	}

	public function sqlWrapUpTime() {
		return ( true == isset( $this->m_intWrapUpTime ) ) ? ( string ) $this->m_intWrapUpTime : 'NULL';
	}

	public function setCallNotes( $strCallNotes ) {
		$this->set( 'm_strCallNotes', CStrings::strTrimDef( $strCallNotes, -1, NULL, true ) );
	}

	public function getCallNotes() {
		return $this->m_strCallNotes;
	}

	public function sqlCallNotes() {
		return ( true == isset( $this->m_strCallNotes ) ) ? '\'' . addslashes( $this->m_strCallNotes ) . '\'' : 'NULL';
	}

	public function setCallerName( $strCallerName ) {
		$this->set( 'm_strCallerName', CStrings::strTrimDef( $strCallerName, 240, NULL, true ) );
	}

	public function getCallerName() {
		return $this->m_strCallerName;
	}

	public function sqlCallerName() {
		return ( true == isset( $this->m_strCallerName ) ) ? '\'' . addslashes( $this->m_strCallerName ) . '\'' : 'NULL';
	}

	public function setBeginSendTime( $strBeginSendTime ) {
		$this->set( 'm_strBeginSendTime', CStrings::strTrimDef( $strBeginSendTime, NULL, NULL, true ) );
	}

	public function getBeginSendTime() {
		return $this->m_strBeginSendTime;
	}

	public function sqlBeginSendTime() {
		return ( true == isset( $this->m_strBeginSendTime ) ) ? '\'' . $this->m_strBeginSendTime . '\'' : 'NULL';
	}

	public function setEndSendTime( $strEndSendTime ) {
		$this->set( 'm_strEndSendTime', CStrings::strTrimDef( $strEndSendTime, NULL, NULL, true ) );
	}

	public function getEndSendTime() {
		return $this->m_strEndSendTime;
	}

	public function sqlEndSendTime() {
		return ( true == isset( $this->m_strEndSendTime ) ) ? '\'' . $this->m_strEndSendTime . '\'' : 'NULL';
	}

	public function setAllowBlock( $intAllowBlock ) {
		$this->set( 'm_intAllowBlock', CStrings::strToIntDef( $intAllowBlock, NULL, false ) );
	}

	public function getAllowBlock() {
		return $this->m_intAllowBlock;
	}

	public function sqlAllowBlock() {
		return ( true == isset( $this->m_intAllowBlock ) ) ? ( string ) $this->m_intAllowBlock : '0';
	}

	public function setCallToday( $intCallToday ) {
		$this->set( 'm_intCallToday', CStrings::strToIntDef( $intCallToday, NULL, false ) );
	}

	public function getCallToday() {
		return $this->m_intCallToday;
	}

	public function sqlCallToday() {
		return ( true == isset( $this->m_intCallToday ) ) ? ( string ) $this->m_intCallToday : '0';
	}

	public function setIsAutoDetected( $intIsAutoDetected ) {
		$this->set( 'm_intIsAutoDetected', CStrings::strToIntDef( $intIsAutoDetected, NULL, false ) );
	}

	public function getIsAutoDetected() {
		return $this->m_intIsAutoDetected;
	}

	public function sqlIsAutoDetected() {
		return ( true == isset( $this->m_intIsAutoDetected ) ) ? ( string ) $this->m_intIsAutoDetected : '0';
	}

	public function setIsVoiceMail( $intIsVoiceMail ) {
		$this->set( 'm_intIsVoiceMail', CStrings::strToIntDef( $intIsVoiceMail, NULL, false ) );
	}

	public function getIsVoiceMail() {
		return $this->m_intIsVoiceMail;
	}

	public function sqlIsVoiceMail() {
		return ( true == isset( $this->m_intIsVoiceMail ) ) ? ( string ) $this->m_intIsVoiceMail : '0';
	}

	public function setIsTestCall( $intIsTestCall ) {
		$this->set( 'm_intIsTestCall', CStrings::strToIntDef( $intIsTestCall, NULL, false ) );
	}

	public function getIsTestCall() {
		return $this->m_intIsTestCall;
	}

	public function sqlIsTestCall() {
		return ( true == isset( $this->m_intIsTestCall ) ) ? ( string ) $this->m_intIsTestCall : '0';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setVoiceMailReviewedOn( $strVoiceMailReviewedOn ) {
		$this->set( 'm_strVoiceMailReviewedOn', CStrings::strTrimDef( $strVoiceMailReviewedOn, -1, NULL, true ) );
	}

	public function getVoiceMailReviewedOn() {
		return $this->m_strVoiceMailReviewedOn;
	}

	public function sqlVoiceMailReviewedOn() {
		return ( true == isset( $this->m_strVoiceMailReviewedOn ) ) ? '\'' . $this->m_strVoiceMailReviewedOn . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setAnalyzedBy( $intAnalyzedBy ) {
		$this->set( 'm_intAnalyzedBy', CStrings::strToIntDef( $intAnalyzedBy, NULL, false ) );
	}

	public function getAnalyzedBy() {
		return $this->m_intAnalyzedBy;
	}

	public function sqlAnalyzedBy() {
		return ( true == isset( $this->m_intAnalyzedBy ) ) ? ( string ) $this->m_intAnalyzedBy : 'NULL';
	}

	public function setAnalyzedOn( $strAnalyzedOn ) {
		$this->set( 'm_strAnalyzedOn', CStrings::strTrimDef( $strAnalyzedOn, -1, NULL, true ) );
	}

	public function getAnalyzedOn() {
		return $this->m_strAnalyzedOn;
	}

	public function sqlAnalyzedOn() {
		return ( true == isset( $this->m_strAnalyzedOn ) ) ? '\'' . $this->m_strAnalyzedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCallIvrQueueTime( $intCallIvrQueueTime ) {
		$this->set( 'm_intCallIvrQueueTime', CStrings::strToIntDef( $intCallIvrQueueTime, NULL, false ) );
	}

	public function getCallIvrQueueTime() {
		return $this->m_intCallIvrQueueTime;
	}

	public function sqlCallIvrQueueTime() {
		return ( true == isset( $this->m_intCallIvrQueueTime ) ) ? ( string ) $this->m_intCallIvrQueueTime : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, redirect_property_id, call_phone_number_id, call_type_id, call_file_id, call_status_type_id, call_response_type_id, call_result_id, call_priority_id, voice_type_id, company_employee_id, employee_id, customer_id, applicant_application_id, lead_source_id, scheduled_call_id, scheduled_call_type_id, outbound_call_id, call_form_status_type_id, session_uuid, call_queue_id, call_agent_id, call_analysis_status_type_id, ivr_menu_action_id, destination_phone_number, destination_extension, origin_phone_number, origin_extension, scheduled_send_datetime, call_datetime, synced_on, call_duration_seconds, call_center_queue_time, call_center_talk_time, wrap_up_time, call_notes, caller_name, begin_send_time, end_send_time, allow_block, call_today, is_auto_detected, is_voice_mail, is_test_call, attempt_count, voice_mail_reviewed_on, sent_on, failed_on, analyzed_by, analyzed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, call_ivr_queue_time, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlRedirectPropertyId() . ', ' .
						$this->sqlCallPhoneNumberId() . ', ' .
						$this->sqlCallTypeId() . ', ' .
						$this->sqlCallFileId() . ', ' .
						$this->sqlCallStatusTypeId() . ', ' .
						$this->sqlCallResponseTypeId() . ', ' .
						$this->sqlCallResultId() . ', ' .
						$this->sqlCallPriorityId() . ', ' .
						$this->sqlVoiceTypeId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlApplicantApplicationId() . ', ' .
						$this->sqlLeadSourceId() . ', ' .
						$this->sqlScheduledCallId() . ', ' .
						$this->sqlScheduledCallTypeId() . ', ' .
						$this->sqlOutboundCallId() . ', ' .
						$this->sqlCallFormStatusTypeId() . ', ' .
						$this->sqlSessionUuid() . ', ' .
						$this->sqlCallQueueId() . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlCallAnalysisStatusTypeId() . ', ' .
						$this->sqlIvrMenuActionId() . ', ' .
						$this->sqlDestinationPhoneNumber() . ', ' .
						$this->sqlDestinationExtension() . ', ' .
						$this->sqlOriginPhoneNumber() . ', ' .
						$this->sqlOriginExtension() . ', ' .
						$this->sqlScheduledSendDatetime() . ', ' .
						$this->sqlCallDatetime() . ', ' .
						$this->sqlSyncedOn() . ', ' .
						$this->sqlCallDurationSeconds() . ', ' .
						$this->sqlCallCenterQueueTime() . ', ' .
						$this->sqlCallCenterTalkTime() . ', ' .
						$this->sqlWrapUpTime() . ', ' .
						$this->sqlCallNotes() . ', ' .
						$this->sqlCallerName() . ', ' .
						$this->sqlBeginSendTime() . ', ' .
						$this->sqlEndSendTime() . ', ' .
						$this->sqlAllowBlock() . ', ' .
						$this->sqlCallToday() . ', ' .
						$this->sqlIsAutoDetected() . ', ' .
						$this->sqlIsVoiceMail() . ', ' .
						$this->sqlIsTestCall() . ', ' .
						$this->sqlAttemptCount() . ', ' .
						$this->sqlVoiceMailReviewedOn() . ', ' .
						$this->sqlSentOn() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlAnalyzedBy() . ', ' .
						$this->sqlAnalyzedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCallIvrQueueTime() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redirect_property_id = ' . $this->sqlRedirectPropertyId(). ',' ; } elseif( true == array_key_exists( 'RedirectPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' redirect_property_id = ' . $this->sqlRedirectPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId(). ',' ; } elseif( true == array_key_exists( 'CallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_file_id = ' . $this->sqlCallFileId(). ',' ; } elseif( true == array_key_exists( 'CallFileId', $this->getChangedColumns() ) ) { $strSql .= ' call_file_id = ' . $this->sqlCallFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_status_type_id = ' . $this->sqlCallStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CallStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_status_type_id = ' . $this->sqlCallStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_response_type_id = ' . $this->sqlCallResponseTypeId(). ',' ; } elseif( true == array_key_exists( 'CallResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_response_type_id = ' . $this->sqlCallResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId(). ',' ; } elseif( true == array_key_exists( 'CallResultId', $this->getChangedColumns() ) ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId(). ',' ; } elseif( true == array_key_exists( 'CallPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voice_type_id = ' . $this->sqlVoiceTypeId(). ',' ; } elseif( true == array_key_exists( 'VoiceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' voice_type_id = ' . $this->sqlVoiceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId(). ',' ; } elseif( true == array_key_exists( 'ApplicantApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_id = ' . $this->sqlApplicantApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'LeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_id = ' . $this->sqlLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_call_id = ' . $this->sqlScheduledCallId(). ',' ; } elseif( true == array_key_exists( 'ScheduledCallId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_call_id = ' . $this->sqlScheduledCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_call_type_id = ' . $this->sqlScheduledCallTypeId(). ',' ; } elseif( true == array_key_exists( 'ScheduledCallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_call_type_id = ' . $this->sqlScheduledCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outbound_call_id = ' . $this->sqlOutboundCallId(). ',' ; } elseif( true == array_key_exists( 'OutboundCallId', $this->getChangedColumns() ) ) { $strSql .= ' outbound_call_id = ' . $this->sqlOutboundCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_form_status_type_id = ' . $this->sqlCallFormStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CallFormStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_form_status_type_id = ' . $this->sqlCallFormStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid(). ',' ; } elseif( true == array_key_exists( 'SessionUuid', $this->getChangedColumns() ) ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId(). ',' ; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_status_type_id = ' . $this->sqlCallAnalysisStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_status_type_id = ' . $this->sqlCallAnalysisStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_action_id = ' . $this->sqlIvrMenuActionId(). ',' ; } elseif( true == array_key_exists( 'IvrMenuActionId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_action_id = ' . $this->sqlIvrMenuActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_phone_number = ' . $this->sqlDestinationPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'DestinationPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' destination_phone_number = ' . $this->sqlDestinationPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_extension = ' . $this->sqlDestinationExtension(). ',' ; } elseif( true == array_key_exists( 'DestinationExtension', $this->getChangedColumns() ) ) { $strSql .= ' destination_extension = ' . $this->sqlDestinationExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_phone_number = ' . $this->sqlOriginPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'OriginPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' origin_phone_number = ' . $this->sqlOriginPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_extension = ' . $this->sqlOriginExtension(). ',' ; } elseif( true == array_key_exists( 'OriginExtension', $this->getChangedColumns() ) ) { $strSql .= ' origin_extension = ' . $this->sqlOriginExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledSendDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime(). ',' ; } elseif( true == array_key_exists( 'CallDatetime', $this->getChangedColumns() ) ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn(). ',' ; } elseif( true == array_key_exists( 'SyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds(). ',' ; } elseif( true == array_key_exists( 'CallDurationSeconds', $this->getChangedColumns() ) ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_center_queue_time = ' . $this->sqlCallCenterQueueTime(). ',' ; } elseif( true == array_key_exists( 'CallCenterQueueTime', $this->getChangedColumns() ) ) { $strSql .= ' call_center_queue_time = ' . $this->sqlCallCenterQueueTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_center_talk_time = ' . $this->sqlCallCenterTalkTime(). ',' ; } elseif( true == array_key_exists( 'CallCenterTalkTime', $this->getChangedColumns() ) ) { $strSql .= ' call_center_talk_time = ' . $this->sqlCallCenterTalkTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wrap_up_time = ' . $this->sqlWrapUpTime(). ',' ; } elseif( true == array_key_exists( 'WrapUpTime', $this->getChangedColumns() ) ) { $strSql .= ' wrap_up_time = ' . $this->sqlWrapUpTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_notes = ' . $this->sqlCallNotes(). ',' ; } elseif( true == array_key_exists( 'CallNotes', $this->getChangedColumns() ) ) { $strSql .= ' call_notes = ' . $this->sqlCallNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caller_name = ' . $this->sqlCallerName(). ',' ; } elseif( true == array_key_exists( 'CallerName', $this->getChangedColumns() ) ) { $strSql .= ' caller_name = ' . $this->sqlCallerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_send_time = ' . $this->sqlBeginSendTime(). ',' ; } elseif( true == array_key_exists( 'BeginSendTime', $this->getChangedColumns() ) ) { $strSql .= ' begin_send_time = ' . $this->sqlBeginSendTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_send_time = ' . $this->sqlEndSendTime(). ',' ; } elseif( true == array_key_exists( 'EndSendTime', $this->getChangedColumns() ) ) { $strSql .= ' end_send_time = ' . $this->sqlEndSendTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_block = ' . $this->sqlAllowBlock(). ',' ; } elseif( true == array_key_exists( 'AllowBlock', $this->getChangedColumns() ) ) { $strSql .= ' allow_block = ' . $this->sqlAllowBlock() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_today = ' . $this->sqlCallToday(). ',' ; } elseif( true == array_key_exists( 'CallToday', $this->getChangedColumns() ) ) { $strSql .= ' call_today = ' . $this->sqlCallToday() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_detected = ' . $this->sqlIsAutoDetected(). ',' ; } elseif( true == array_key_exists( 'IsAutoDetected', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_detected = ' . $this->sqlIsAutoDetected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_voice_mail = ' . $this->sqlIsVoiceMail(). ',' ; } elseif( true == array_key_exists( 'IsVoiceMail', $this->getChangedColumns() ) ) { $strSql .= ' is_voice_mail = ' . $this->sqlIsVoiceMail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test_call = ' . $this->sqlIsTestCall(). ',' ; } elseif( true == array_key_exists( 'IsTestCall', $this->getChangedColumns() ) ) { $strSql .= ' is_test_call = ' . $this->sqlIsTestCall() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount(). ',' ; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voice_mail_reviewed_on = ' . $this->sqlVoiceMailReviewedOn(). ',' ; } elseif( true == array_key_exists( 'VoiceMailReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' voice_mail_reviewed_on = ' . $this->sqlVoiceMailReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn(). ',' ; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' analyzed_by = ' . $this->sqlAnalyzedBy(). ',' ; } elseif( true == array_key_exists( 'AnalyzedBy', $this->getChangedColumns() ) ) { $strSql .= ' analyzed_by = ' . $this->sqlAnalyzedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' analyzed_on = ' . $this->sqlAnalyzedOn(). ',' ; } elseif( true == array_key_exists( 'AnalyzedOn', $this->getChangedColumns() ) ) { $strSql .= ' analyzed_on = ' . $this->sqlAnalyzedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_ivr_queue_time = ' . $this->sqlCallIvrQueueTime(). ',' ; } elseif( true == array_key_exists( 'CallIvrQueueTime', $this->getChangedColumns() ) ) { $strSql .= ' call_ivr_queue_time = ' . $this->sqlCallIvrQueueTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'redirect_property_id' => $this->getRedirectPropertyId(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'call_type_id' => $this->getCallTypeId(),
			'call_file_id' => $this->getCallFileId(),
			'call_status_type_id' => $this->getCallStatusTypeId(),
			'call_response_type_id' => $this->getCallResponseTypeId(),
			'call_result_id' => $this->getCallResultId(),
			'call_priority_id' => $this->getCallPriorityId(),
			'voice_type_id' => $this->getVoiceTypeId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'customer_id' => $this->getCustomerId(),
			'applicant_application_id' => $this->getApplicantApplicationId(),
			'lead_source_id' => $this->getLeadSourceId(),
			'scheduled_call_id' => $this->getScheduledCallId(),
			'scheduled_call_type_id' => $this->getScheduledCallTypeId(),
			'outbound_call_id' => $this->getOutboundCallId(),
			'call_form_status_type_id' => $this->getCallFormStatusTypeId(),
			'session_uuid' => $this->getSessionUuid(),
			'call_queue_id' => $this->getCallQueueId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_analysis_status_type_id' => $this->getCallAnalysisStatusTypeId(),
			'ivr_menu_action_id' => $this->getIvrMenuActionId(),
			'destination_phone_number' => $this->getDestinationPhoneNumber(),
			'destination_extension' => $this->getDestinationExtension(),
			'origin_phone_number' => $this->getOriginPhoneNumber(),
			'origin_extension' => $this->getOriginExtension(),
			'scheduled_send_datetime' => $this->getScheduledSendDatetime(),
			'call_datetime' => $this->getCallDatetime(),
			'synced_on' => $this->getSyncedOn(),
			'call_duration_seconds' => $this->getCallDurationSeconds(),
			'call_center_queue_time' => $this->getCallCenterQueueTime(),
			'call_center_talk_time' => $this->getCallCenterTalkTime(),
			'wrap_up_time' => $this->getWrapUpTime(),
			'call_notes' => $this->getCallNotes(),
			'caller_name' => $this->getCallerName(),
			'begin_send_time' => $this->getBeginSendTime(),
			'end_send_time' => $this->getEndSendTime(),
			'allow_block' => $this->getAllowBlock(),
			'call_today' => $this->getCallToday(),
			'is_auto_detected' => $this->getIsAutoDetected(),
			'is_voice_mail' => $this->getIsVoiceMail(),
			'is_test_call' => $this->getIsTestCall(),
			'attempt_count' => $this->getAttemptCount(),
			'voice_mail_reviewed_on' => $this->getVoiceMailReviewedOn(),
			'sent_on' => $this->getSentOn(),
			'failed_on' => $this->getFailedOn(),
			'analyzed_by' => $this->getAnalyzedBy(),
			'analyzed_on' => $this->getAnalyzedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'call_ivr_queue_time' => $this->getCallIvrQueueTime(),
			'details' => $this->getDetails()
		);
	}

}
?>