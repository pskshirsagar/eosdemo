<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallForwardPreferences
 * Do not add any new functions to this class.
 */

class CBaseCallForwardPreferences extends CEosPluralBase {

	/**
	 * @return CCallForwardPreference[]
	 */
	public static function fetchCallForwardPreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallForwardPreference::class, $objDatabase );
	}

	/**
	 * @return CCallForwardPreference
	 */
	public static function fetchCallForwardPreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallForwardPreference::class, $objDatabase );
	}

	public static function fetchCallForwardPreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_forward_preferences', $objDatabase );
	}

	public static function fetchCallForwardPreferenceById( $intId, $objDatabase ) {
		return self::fetchCallForwardPreference( sprintf( 'SELECT * FROM call_forward_preferences WHERE id = %d', $intId ), $objDatabase );
	}

}
?>