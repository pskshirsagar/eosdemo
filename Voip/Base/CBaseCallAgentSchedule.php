<?php

class CBaseCallAgentSchedule extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_schedules';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_strIntervalStartTimestamp;
	protected $m_intIntervalStartEpoch;
	protected $m_strIntervalEndTimestamp;
	protected $m_intIntervalEndEpoch;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['interval_start_timestamp'] ) && $boolDirectSet ) $this->set( 'm_strIntervalStartTimestamp', trim( $arrValues['interval_start_timestamp'] ) ); elseif( isset( $arrValues['interval_start_timestamp'] ) ) $this->setIntervalStartTimestamp( $arrValues['interval_start_timestamp'] );
		if( isset( $arrValues['interval_start_epoch'] ) && $boolDirectSet ) $this->set( 'm_intIntervalStartEpoch', trim( $arrValues['interval_start_epoch'] ) ); elseif( isset( $arrValues['interval_start_epoch'] ) ) $this->setIntervalStartEpoch( $arrValues['interval_start_epoch'] );
		if( isset( $arrValues['interval_end_timestamp'] ) && $boolDirectSet ) $this->set( 'm_strIntervalEndTimestamp', trim( $arrValues['interval_end_timestamp'] ) ); elseif( isset( $arrValues['interval_end_timestamp'] ) ) $this->setIntervalEndTimestamp( $arrValues['interval_end_timestamp'] );
		if( isset( $arrValues['interval_end_epoch'] ) && $boolDirectSet ) $this->set( 'm_intIntervalEndEpoch', trim( $arrValues['interval_end_epoch'] ) ); elseif( isset( $arrValues['interval_end_epoch'] ) ) $this->setIntervalEndEpoch( $arrValues['interval_end_epoch'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setIntervalStartTimestamp( $strIntervalStartTimestamp ) {
		$this->set( 'm_strIntervalStartTimestamp', CStrings::strTrimDef( $strIntervalStartTimestamp, -1, NULL, true ) );
	}

	public function getIntervalStartTimestamp() {
		return $this->m_strIntervalStartTimestamp;
	}

	public function sqlIntervalStartTimestamp() {
		return ( true == isset( $this->m_strIntervalStartTimestamp ) ) ? '\'' . $this->m_strIntervalStartTimestamp . '\'' : 'NOW()';
	}

	public function setIntervalStartEpoch( $intIntervalStartEpoch ) {
		$this->set( 'm_intIntervalStartEpoch', CStrings::strToIntDef( $intIntervalStartEpoch, NULL, false ) );
	}

	public function getIntervalStartEpoch() {
		return $this->m_intIntervalStartEpoch;
	}

	public function sqlIntervalStartEpoch() {
		return ( true == isset( $this->m_intIntervalStartEpoch ) ) ? ( string ) $this->m_intIntervalStartEpoch : 'NULL';
	}

	public function setIntervalEndTimestamp( $strIntervalEndTimestamp ) {
		$this->set( 'm_strIntervalEndTimestamp', CStrings::strTrimDef( $strIntervalEndTimestamp, -1, NULL, true ) );
	}

	public function getIntervalEndTimestamp() {
		return $this->m_strIntervalEndTimestamp;
	}

	public function sqlIntervalEndTimestamp() {
		return ( true == isset( $this->m_strIntervalEndTimestamp ) ) ? '\'' . $this->m_strIntervalEndTimestamp . '\'' : 'NULL';
	}

	public function setIntervalEndEpoch( $intIntervalEndEpoch ) {
		$this->set( 'm_intIntervalEndEpoch', CStrings::strToIntDef( $intIntervalEndEpoch, NULL, false ) );
	}

	public function getIntervalEndEpoch() {
		return $this->m_intIntervalEndEpoch;
	}

	public function sqlIntervalEndEpoch() {
		return ( true == isset( $this->m_intIntervalEndEpoch ) ) ? ( string ) $this->m_intIntervalEndEpoch : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, interval_start_timestamp, interval_start_epoch, interval_end_timestamp, interval_end_epoch, is_active, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlIntervalStartTimestamp() . ', ' .
 						$this->sqlIntervalStartEpoch() . ', ' .
 						$this->sqlIntervalEndTimestamp() . ', ' .
 						$this->sqlIntervalEndEpoch() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_start_timestamp = ' . $this->sqlIntervalStartTimestamp() . ','; } elseif( true == array_key_exists( 'IntervalStartTimestamp', $this->getChangedColumns() ) ) { $strSql .= ' interval_start_timestamp = ' . $this->sqlIntervalStartTimestamp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_start_epoch = ' . $this->sqlIntervalStartEpoch() . ','; } elseif( true == array_key_exists( 'IntervalStartEpoch', $this->getChangedColumns() ) ) { $strSql .= ' interval_start_epoch = ' . $this->sqlIntervalStartEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_end_timestamp = ' . $this->sqlIntervalEndTimestamp() . ','; } elseif( true == array_key_exists( 'IntervalEndTimestamp', $this->getChangedColumns() ) ) { $strSql .= ' interval_end_timestamp = ' . $this->sqlIntervalEndTimestamp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interval_end_epoch = ' . $this->sqlIntervalEndEpoch() . ','; } elseif( true == array_key_exists( 'IntervalEndEpoch', $this->getChangedColumns() ) ) { $strSql .= ' interval_end_epoch = ' . $this->sqlIntervalEndEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'interval_start_timestamp' => $this->getIntervalStartTimestamp(),
			'interval_start_epoch' => $this->getIntervalStartEpoch(),
			'interval_end_timestamp' => $this->getIntervalEndTimestamp(),
			'interval_end_epoch' => $this->getIntervalEndEpoch(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>