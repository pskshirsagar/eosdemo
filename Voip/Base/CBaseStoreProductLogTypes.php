<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProductLogTypes
 * Do not add any new functions to this class.
 */

class CBaseStoreProductLogTypes extends CEosPluralBase {

	/**
	 * @return CStoreProductLogType[]
	 */
	public static function fetchStoreProductLogTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProductLogType::class, $objDatabase );
	}

	/**
	 * @return CStoreProductLogType
	 */
	public static function fetchStoreProductLogType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProductLogType::class, $objDatabase );
	}

	public static function fetchStoreProductLogTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_product_log_types', $objDatabase );
	}

	public static function fetchStoreProductLogTypeById( $intId, $objDatabase ) {
		return self::fetchStoreProductLogType( sprintf( 'SELECT * FROM store_product_log_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>