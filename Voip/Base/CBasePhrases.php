<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPhrases
 * Do not add any new functions to this class.
 */

class CBasePhrases extends CEosPluralBase {

	/**
	 * @return CPhrase[]
	 */
	public static function fetchPhrases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPhrase', $objDatabase );
	}

	/**
	 * @return CPhrase
	 */
	public static function fetchPhrase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPhrase', $objDatabase );
	}

	public static function fetchPhraseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'phrases', $objDatabase );
	}

	public static function fetchPhraseById( $intId, $objDatabase ) {
		return self::fetchPhrase( sprintf( 'SELECT * FROM phrases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPhrasesByCid( $intCid, $objDatabase ) {
		return self::fetchPhrases( sprintf( 'SELECT * FROM phrases WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPhrasesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPhrases( sprintf( 'SELECT * FROM phrases WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>