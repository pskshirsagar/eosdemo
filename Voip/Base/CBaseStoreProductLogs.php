<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProductLogs
 * Do not add any new functions to this class.
 */

class CBaseStoreProductLogs extends CEosPluralBase {

	/**
	 * @return CStoreProductLog[]
	 */
	public static function fetchStoreProductLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProductLog::class, $objDatabase );
	}

	/**
	 * @return CStoreProductLog
	 */
	public static function fetchStoreProductLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProductLog::class, $objDatabase );
	}

	public static function fetchStoreProductLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_product_logs', $objDatabase );
	}

	public static function fetchStoreProductLogById( $intId, $objDatabase ) {
		return self::fetchStoreProductLog( sprintf( 'SELECT * FROM store_product_logs WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchStoreProductLogsByStoreProductLogTypeId( $intStoreProductLogTypeId, $objDatabase ) {
		return self::fetchStoreProductLogs( sprintf( 'SELECT * FROM store_product_logs WHERE store_product_log_type_id = %d', $intStoreProductLogTypeId ), $objDatabase );
	}

	public static function fetchStoreProductLogsByStoreProductId( $intStoreProductId, $objDatabase ) {
		return self::fetchStoreProductLogs( sprintf( 'SELECT * FROM store_product_logs WHERE store_product_id = %d', $intStoreProductId ), $objDatabase );
	}

}
?>