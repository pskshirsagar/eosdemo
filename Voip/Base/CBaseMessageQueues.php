<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CMessageQueues
 * Do not add any new functions to this class.
 */

class CBaseMessageQueues extends CEosPluralBase {

	/**
	 * @return CMessageQueue[]
	 */
	public static function fetchMessageQueues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageQueue::class, $objDatabase );
	}

	/**
	 * @return CMessageQueue
	 */
	public static function fetchMessageQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageQueue::class, $objDatabase );
	}

	public static function fetchMessageQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_queues', $objDatabase );
	}

	public static function fetchMessageQueueById( $intId, $objDatabase ) {
		return self::fetchMessageQueue( sprintf( 'SELECT * FROM message_queues WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMessageQueuesByCid( $intCid, $objDatabase ) {
		return self::fetchMessageQueues( sprintf( 'SELECT * FROM message_queues WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMessageQueuesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchMessageQueues( sprintf( 'SELECT * FROM message_queues WHERE company_user_id = %d', $intCompanyUserId ), $objDatabase );
	}

	public static function fetchMessageQueuesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchMessageQueues( sprintf( 'SELECT * FROM message_queues WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchMessageQueuesByMessageQueueTypeId( $intMessageQueueTypeId, $objDatabase ) {
		return self::fetchMessageQueues( sprintf( 'SELECT * FROM message_queues WHERE message_queue_type_id = %d', $intMessageQueueTypeId ), $objDatabase );
	}

}
?>