<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallWordCorrelations
 * Do not add any new functions to this class.
 */

class CBaseCallWordCorrelations extends CEosPluralBase {

	/**
	 * @return CCallWordCorrelation[]
	 */
	public static function fetchCallWordCorrelations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallWordCorrelation::class, $objDatabase );
	}

	/**
	 * @return CCallWordCorrelation
	 */
	public static function fetchCallWordCorrelation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallWordCorrelation::class, $objDatabase );
	}

	public static function fetchCallWordCorrelationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_word_correlations', $objDatabase );
	}

	public static function fetchCallWordCorrelationById( $intId, $objDatabase ) {
		return self::fetchCallWordCorrelation( sprintf( 'SELECT * FROM call_word_correlations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>