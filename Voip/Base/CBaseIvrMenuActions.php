<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenuActions
 * Do not add any new functions to this class.
 */

class CBaseIvrMenuActions extends CEosPluralBase {

	/**
	 * @return CIvrMenuAction[]
	 */
	public static function fetchIvrMenuActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIvrMenuAction', $objDatabase );
	}

	/**
	 * @return CIvrMenuAction
	 */
	public static function fetchIvrMenuAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIvrMenuAction', $objDatabase );
	}

	public static function fetchIvrMenuActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_menu_actions', $objDatabase );
	}

	public static function fetchIvrMenuActionById( $intId, $objDatabase ) {
		return self::fetchIvrMenuAction( sprintf( 'SELECT * FROM ivr_menu_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIvrMenuActionsByCid( $intCid, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchIvrMenuActionsByIvrMenuId( $intIvrMenuId, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE ivr_menu_id = %d', ( int ) $intIvrMenuId ), $objDatabase );
	}

	public static function fetchIvrMenuActionsBySubIvrMenuId( $intSubIvrMenuId, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE sub_ivr_menu_id = %d', ( int ) $intSubIvrMenuId ), $objDatabase );
	}

	public static function fetchIvrMenuActionsByIvrActionId( $intIvrActionId, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE ivr_action_id = %d', ( int ) $intIvrActionId ), $objDatabase );
	}

	public static function fetchIvrMenuActionsByGreetingId( $intGreetingId, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE greeting_id = %d', ( int ) $intGreetingId ), $objDatabase );
	}

	public static function fetchIvrMenuActionsByCallResultId( $intCallResultId, $objDatabase ) {
		return self::fetchIvrMenuActions( sprintf( 'SELECT * FROM ivr_menu_actions WHERE call_result_id = %d', ( int ) $intCallResultId ), $objDatabase );
	}

}
?>