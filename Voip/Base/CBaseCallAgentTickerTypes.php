<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentTickerTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentTickerTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentTickerType[]
	 */
	public static function fetchCallAgentTickerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentTickerType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentTickerType
	 */
	public static function fetchCallAgentTickerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentTickerType::class, $objDatabase );
	}

	public static function fetchCallAgentTickerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_ticker_types', $objDatabase );
	}

	public static function fetchCallAgentTickerTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentTickerType( sprintf( 'SELECT * FROM call_agent_ticker_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>