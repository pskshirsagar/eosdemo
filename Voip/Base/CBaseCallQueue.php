<?php

class CBaseCallQueue extends CEosSingularBase {

	const TABLE_NAME = 'public.call_queues';

	protected $m_intId;
	protected $m_intOverflowCallQueueId;
	protected $m_intCallStrategyTypeId;
	protected $m_intCallQueueTypeId;
	protected $m_intDepartmentId;
	protected $m_intCallFileId;
	protected $m_intQueuePhoneExtensionId;
	protected $m_intCallTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDomain;
	protected $m_strMohSound;
	protected $m_strRecordTemplate;
	protected $m_strTimeBaseScore;
	protected $m_intTierRulesApply;
	protected $m_intTierRuleWaitSecond;
	protected $m_intTierRuleWaitMultiplyLevel;
	protected $m_intTierRuleNoAgentNoWait;
	protected $m_intAbandonedResumeAllowed;
	protected $m_intMaxWaitTime;
	protected $m_intMaxWaitTimeWithNoAgent;
	protected $m_intMaxWaitTimeWithNoAgentTimeReached;
	protected $m_intDiscardAbandonedAfterCallers;
	protected $m_intMultiplyLevelWait;
	protected $m_strNotificationEmail;
	protected $m_intIsOverflowEnabled;
	protected $m_intIsVoicemail;
	protected $m_intIsCustomSettings;
	protected $m_intIsAutoIdleState;
	protected $m_strLanguage;
	protected $m_intTimezoneOffset;
	protected $m_strAnnounceSound;
	protected $m_intAnnounceFrequency;
	protected $m_strSunOpenTime;
	protected $m_strSunCloseTime;
	protected $m_strMonOpenTime;
	protected $m_strMonCloseTime;
	protected $m_strTueOpenTime;
	protected $m_strTueCloseTime;
	protected $m_strWedOpenTime;
	protected $m_strWedCloseTime;
	protected $m_strThuOpenTime;
	protected $m_strThuCloseTime;
	protected $m_strFriOpenTime;
	protected $m_strFriCloseTime;
	protected $m_strSatOpenTime;
	protected $m_strSatCloseTime;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intStaticWrapUpTime;

	public function __construct() {
		parent::__construct();

		$this->m_intTierRulesApply = '0';
		$this->m_intTierRuleWaitSecond = '1';
		$this->m_intTierRuleWaitMultiplyLevel = '1';
		$this->m_intTierRuleNoAgentNoWait = '0';
		$this->m_intAbandonedResumeAllowed = '0';
		$this->m_intMaxWaitTime = '0';
		$this->m_intMaxWaitTimeWithNoAgentTimeReached = '1';
		$this->m_intMultiplyLevelWait = '0';
		$this->m_intIsOverflowEnabled = '0';
		$this->m_intIsVoicemail = '0';
		$this->m_intIsCustomSettings = '0';
		$this->m_intIsAutoIdleState = '1';
		$this->m_strLanguage = 'en';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';
		$this->m_intStaticWrapUpTime = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['overflow_call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intOverflowCallQueueId', trim( $arrValues['overflow_call_queue_id'] ) ); elseif( isset( $arrValues['overflow_call_queue_id'] ) ) $this->setOverflowCallQueueId( $arrValues['overflow_call_queue_id'] );
		if( isset( $arrValues['call_strategy_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallStrategyTypeId', trim( $arrValues['call_strategy_type_id'] ) ); elseif( isset( $arrValues['call_strategy_type_id'] ) ) $this->setCallStrategyTypeId( $arrValues['call_strategy_type_id'] );
		if( isset( $arrValues['call_queue_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueTypeId', trim( $arrValues['call_queue_type_id'] ) ); elseif( isset( $arrValues['call_queue_type_id'] ) ) $this->setCallQueueTypeId( $arrValues['call_queue_type_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['call_file_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFileId', trim( $arrValues['call_file_id'] ) ); elseif( isset( $arrValues['call_file_id'] ) ) $this->setCallFileId( $arrValues['call_file_id'] );
		if( isset( $arrValues['queue_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intQueuePhoneExtensionId', trim( $arrValues['queue_phone_extension_id'] ) ); elseif( isset( $arrValues['queue_phone_extension_id'] ) ) $this->setQueuePhoneExtensionId( $arrValues['queue_phone_extension_id'] );
		if( isset( $arrValues['call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallTypeId', trim( $arrValues['call_type_id'] ) ); elseif( isset( $arrValues['call_type_id'] ) ) $this->setCallTypeId( $arrValues['call_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['moh_sound'] ) && $boolDirectSet ) $this->set( 'm_strMohSound', trim( stripcslashes( $arrValues['moh_sound'] ) ) ); elseif( isset( $arrValues['moh_sound'] ) ) $this->setMohSound( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['moh_sound'] ) : $arrValues['moh_sound'] );
		if( isset( $arrValues['record_template'] ) && $boolDirectSet ) $this->set( 'm_strRecordTemplate', trim( stripcslashes( $arrValues['record_template'] ) ) ); elseif( isset( $arrValues['record_template'] ) ) $this->setRecordTemplate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['record_template'] ) : $arrValues['record_template'] );
		if( isset( $arrValues['time_base_score'] ) && $boolDirectSet ) $this->set( 'm_strTimeBaseScore', trim( stripcslashes( $arrValues['time_base_score'] ) ) ); elseif( isset( $arrValues['time_base_score'] ) ) $this->setTimeBaseScore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['time_base_score'] ) : $arrValues['time_base_score'] );
		if( isset( $arrValues['tier_rules_apply'] ) && $boolDirectSet ) $this->set( 'm_intTierRulesApply', trim( $arrValues['tier_rules_apply'] ) ); elseif( isset( $arrValues['tier_rules_apply'] ) ) $this->setTierRulesApply( $arrValues['tier_rules_apply'] );
		if( isset( $arrValues['tier_rule_wait_second'] ) && $boolDirectSet ) $this->set( 'm_intTierRuleWaitSecond', trim( $arrValues['tier_rule_wait_second'] ) ); elseif( isset( $arrValues['tier_rule_wait_second'] ) ) $this->setTierRuleWaitSecond( $arrValues['tier_rule_wait_second'] );
		if( isset( $arrValues['tier_rule_wait_multiply_level'] ) && $boolDirectSet ) $this->set( 'm_intTierRuleWaitMultiplyLevel', trim( $arrValues['tier_rule_wait_multiply_level'] ) ); elseif( isset( $arrValues['tier_rule_wait_multiply_level'] ) ) $this->setTierRuleWaitMultiplyLevel( $arrValues['tier_rule_wait_multiply_level'] );
		if( isset( $arrValues['tier_rule_no_agent_no_wait'] ) && $boolDirectSet ) $this->set( 'm_intTierRuleNoAgentNoWait', trim( $arrValues['tier_rule_no_agent_no_wait'] ) ); elseif( isset( $arrValues['tier_rule_no_agent_no_wait'] ) ) $this->setTierRuleNoAgentNoWait( $arrValues['tier_rule_no_agent_no_wait'] );
		if( isset( $arrValues['abandoned_resume_allowed'] ) && $boolDirectSet ) $this->set( 'm_intAbandonedResumeAllowed', trim( $arrValues['abandoned_resume_allowed'] ) ); elseif( isset( $arrValues['abandoned_resume_allowed'] ) ) $this->setAbandonedResumeAllowed( $arrValues['abandoned_resume_allowed'] );
		if( isset( $arrValues['max_wait_time'] ) && $boolDirectSet ) $this->set( 'm_intMaxWaitTime', trim( $arrValues['max_wait_time'] ) ); elseif( isset( $arrValues['max_wait_time'] ) ) $this->setMaxWaitTime( $arrValues['max_wait_time'] );
		if( isset( $arrValues['max_wait_time_with_no_agent'] ) && $boolDirectSet ) $this->set( 'm_intMaxWaitTimeWithNoAgent', trim( $arrValues['max_wait_time_with_no_agent'] ) ); elseif( isset( $arrValues['max_wait_time_with_no_agent'] ) ) $this->setMaxWaitTimeWithNoAgent( $arrValues['max_wait_time_with_no_agent'] );
		if( isset( $arrValues['max_wait_time_with_no_agent_time_reached'] ) && $boolDirectSet ) $this->set( 'm_intMaxWaitTimeWithNoAgentTimeReached', trim( $arrValues['max_wait_time_with_no_agent_time_reached'] ) ); elseif( isset( $arrValues['max_wait_time_with_no_agent_time_reached'] ) ) $this->setMaxWaitTimeWithNoAgentTimeReached( $arrValues['max_wait_time_with_no_agent_time_reached'] );
		if( isset( $arrValues['discard_abandoned_after_callers'] ) && $boolDirectSet ) $this->set( 'm_intDiscardAbandonedAfterCallers', trim( $arrValues['discard_abandoned_after_callers'] ) ); elseif( isset( $arrValues['discard_abandoned_after_callers'] ) ) $this->setDiscardAbandonedAfterCallers( $arrValues['discard_abandoned_after_callers'] );
		if( isset( $arrValues['multiply_level_wait'] ) && $boolDirectSet ) $this->set( 'm_intMultiplyLevelWait', trim( $arrValues['multiply_level_wait'] ) ); elseif( isset( $arrValues['multiply_level_wait'] ) ) $this->setMultiplyLevelWait( $arrValues['multiply_level_wait'] );
		if( isset( $arrValues['notification_email'] ) && $boolDirectSet ) $this->set( 'm_strNotificationEmail', trim( stripcslashes( $arrValues['notification_email'] ) ) ); elseif( isset( $arrValues['notification_email'] ) ) $this->setNotificationEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notification_email'] ) : $arrValues['notification_email'] );
		if( isset( $arrValues['is_overflow_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsOverflowEnabled', trim( $arrValues['is_overflow_enabled'] ) ); elseif( isset( $arrValues['is_overflow_enabled'] ) ) $this->setIsOverflowEnabled( $arrValues['is_overflow_enabled'] );
		if( isset( $arrValues['is_voicemail'] ) && $boolDirectSet ) $this->set( 'm_intIsVoicemail', trim( $arrValues['is_voicemail'] ) ); elseif( isset( $arrValues['is_voicemail'] ) ) $this->setIsVoicemail( $arrValues['is_voicemail'] );
		if( isset( $arrValues['is_custom_settings'] ) && $boolDirectSet ) $this->set( 'm_intIsCustomSettings', trim( $arrValues['is_custom_settings'] ) ); elseif( isset( $arrValues['is_custom_settings'] ) ) $this->setIsCustomSettings( $arrValues['is_custom_settings'] );
		if( isset( $arrValues['is_auto_idle_state'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoIdleState', trim( $arrValues['is_auto_idle_state'] ) ); elseif( isset( $arrValues['is_auto_idle_state'] ) ) $this->setIsAutoIdleState( $arrValues['is_auto_idle_state'] );
		if( isset( $arrValues['language'] ) && $boolDirectSet ) $this->set( 'm_strLanguage', trim( stripcslashes( $arrValues['language'] ) ) ); elseif( isset( $arrValues['language'] ) ) $this->setLanguage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['language'] ) : $arrValues['language'] );
		if( isset( $arrValues['timezone_offset'] ) && $boolDirectSet ) $this->set( 'm_intTimezoneOffset', trim( $arrValues['timezone_offset'] ) ); elseif( isset( $arrValues['timezone_offset'] ) ) $this->setTimezoneOffset( $arrValues['timezone_offset'] );
		if( isset( $arrValues['announce_sound'] ) && $boolDirectSet ) $this->set( 'm_strAnnounceSound', trim( stripcslashes( $arrValues['announce_sound'] ) ) ); elseif( isset( $arrValues['announce_sound'] ) ) $this->setAnnounceSound( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['announce_sound'] ) : $arrValues['announce_sound'] );
		if( isset( $arrValues['announce_frequency'] ) && $boolDirectSet ) $this->set( 'm_intAnnounceFrequency', trim( $arrValues['announce_frequency'] ) ); elseif( isset( $arrValues['announce_frequency'] ) ) $this->setAnnounceFrequency( $arrValues['announce_frequency'] );
		if( isset( $arrValues['sun_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSunOpenTime', trim( $arrValues['sun_open_time'] ) ); elseif( isset( $arrValues['sun_open_time'] ) ) $this->setSunOpenTime( $arrValues['sun_open_time'] );
		if( isset( $arrValues['sun_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSunCloseTime', trim( $arrValues['sun_close_time'] ) ); elseif( isset( $arrValues['sun_close_time'] ) ) $this->setSunCloseTime( $arrValues['sun_close_time'] );
		if( isset( $arrValues['mon_open_time'] ) && $boolDirectSet ) $this->set( 'm_strMonOpenTime', trim( $arrValues['mon_open_time'] ) ); elseif( isset( $arrValues['mon_open_time'] ) ) $this->setMonOpenTime( $arrValues['mon_open_time'] );
		if( isset( $arrValues['mon_close_time'] ) && $boolDirectSet ) $this->set( 'm_strMonCloseTime', trim( $arrValues['mon_close_time'] ) ); elseif( isset( $arrValues['mon_close_time'] ) ) $this->setMonCloseTime( $arrValues['mon_close_time'] );
		if( isset( $arrValues['tue_open_time'] ) && $boolDirectSet ) $this->set( 'm_strTueOpenTime', trim( $arrValues['tue_open_time'] ) ); elseif( isset( $arrValues['tue_open_time'] ) ) $this->setTueOpenTime( $arrValues['tue_open_time'] );
		if( isset( $arrValues['tue_close_time'] ) && $boolDirectSet ) $this->set( 'm_strTueCloseTime', trim( $arrValues['tue_close_time'] ) ); elseif( isset( $arrValues['tue_close_time'] ) ) $this->setTueCloseTime( $arrValues['tue_close_time'] );
		if( isset( $arrValues['wed_open_time'] ) && $boolDirectSet ) $this->set( 'm_strWedOpenTime', trim( $arrValues['wed_open_time'] ) ); elseif( isset( $arrValues['wed_open_time'] ) ) $this->setWedOpenTime( $arrValues['wed_open_time'] );
		if( isset( $arrValues['wed_close_time'] ) && $boolDirectSet ) $this->set( 'm_strWedCloseTime', trim( $arrValues['wed_close_time'] ) ); elseif( isset( $arrValues['wed_close_time'] ) ) $this->setWedCloseTime( $arrValues['wed_close_time'] );
		if( isset( $arrValues['thu_open_time'] ) && $boolDirectSet ) $this->set( 'm_strThuOpenTime', trim( $arrValues['thu_open_time'] ) ); elseif( isset( $arrValues['thu_open_time'] ) ) $this->setThuOpenTime( $arrValues['thu_open_time'] );
		if( isset( $arrValues['thu_close_time'] ) && $boolDirectSet ) $this->set( 'm_strThuCloseTime', trim( $arrValues['thu_close_time'] ) ); elseif( isset( $arrValues['thu_close_time'] ) ) $this->setThuCloseTime( $arrValues['thu_close_time'] );
		if( isset( $arrValues['fri_open_time'] ) && $boolDirectSet ) $this->set( 'm_strFriOpenTime', trim( $arrValues['fri_open_time'] ) ); elseif( isset( $arrValues['fri_open_time'] ) ) $this->setFriOpenTime( $arrValues['fri_open_time'] );
		if( isset( $arrValues['fri_close_time'] ) && $boolDirectSet ) $this->set( 'm_strFriCloseTime', trim( $arrValues['fri_close_time'] ) ); elseif( isset( $arrValues['fri_close_time'] ) ) $this->setFriCloseTime( $arrValues['fri_close_time'] );
		if( isset( $arrValues['sat_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSatOpenTime', trim( $arrValues['sat_open_time'] ) ); elseif( isset( $arrValues['sat_open_time'] ) ) $this->setSatOpenTime( $arrValues['sat_open_time'] );
		if( isset( $arrValues['sat_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSatCloseTime', trim( $arrValues['sat_close_time'] ) ); elseif( isset( $arrValues['sat_close_time'] ) ) $this->setSatCloseTime( $arrValues['sat_close_time'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['static_wrap_up_time'] ) && $boolDirectSet ) $this->set( 'm_intStaticWrapUpTime', trim( $arrValues['static_wrap_up_time'] ) ); elseif( isset( $arrValues['static_wrap_up_time'] ) ) $this->setStaticWrapUpTime( $arrValues['static_wrap_up_time'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOverflowCallQueueId( $intOverflowCallQueueId ) {
		$this->set( 'm_intOverflowCallQueueId', CStrings::strToIntDef( $intOverflowCallQueueId, NULL, false ) );
	}

	public function getOverflowCallQueueId() {
		return $this->m_intOverflowCallQueueId;
	}

	public function sqlOverflowCallQueueId() {
		return ( true == isset( $this->m_intOverflowCallQueueId ) ) ? ( string ) $this->m_intOverflowCallQueueId : 'NULL';
	}

	public function setCallStrategyTypeId( $intCallStrategyTypeId ) {
		$this->set( 'm_intCallStrategyTypeId', CStrings::strToIntDef( $intCallStrategyTypeId, NULL, false ) );
	}

	public function getCallStrategyTypeId() {
		return $this->m_intCallStrategyTypeId;
	}

	public function sqlCallStrategyTypeId() {
		return ( true == isset( $this->m_intCallStrategyTypeId ) ) ? ( string ) $this->m_intCallStrategyTypeId : 'NULL';
	}

	public function setCallQueueTypeId( $intCallQueueTypeId ) {
		$this->set( 'm_intCallQueueTypeId', CStrings::strToIntDef( $intCallQueueTypeId, NULL, false ) );
	}

	public function getCallQueueTypeId() {
		return $this->m_intCallQueueTypeId;
	}

	public function sqlCallQueueTypeId() {
		return ( true == isset( $this->m_intCallQueueTypeId ) ) ? ( string ) $this->m_intCallQueueTypeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setCallFileId( $intCallFileId ) {
		$this->set( 'm_intCallFileId', CStrings::strToIntDef( $intCallFileId, NULL, false ) );
	}

	public function getCallFileId() {
		return $this->m_intCallFileId;
	}

	public function sqlCallFileId() {
		return ( true == isset( $this->m_intCallFileId ) ) ? ( string ) $this->m_intCallFileId : 'NULL';
	}

	public function setQueuePhoneExtensionId( $intQueuePhoneExtensionId ) {
		$this->set( 'm_intQueuePhoneExtensionId', CStrings::strToIntDef( $intQueuePhoneExtensionId, NULL, false ) );
	}

	public function getQueuePhoneExtensionId() {
		return $this->m_intQueuePhoneExtensionId;
	}

	public function sqlQueuePhoneExtensionId() {
		return ( true == isset( $this->m_intQueuePhoneExtensionId ) ) ? ( string ) $this->m_intQueuePhoneExtensionId : 'NULL';
	}

	public function setCallTypeId( $intCallTypeId ) {
		$this->set( 'm_intCallTypeId', CStrings::strToIntDef( $intCallTypeId, NULL, false ) );
	}

	public function getCallTypeId() {
		return $this->m_intCallTypeId;
	}

	public function sqlCallTypeId() {
		return ( true == isset( $this->m_intCallTypeId ) ) ? ( string ) $this->m_intCallTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 520, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 240, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setMohSound( $strMohSound ) {
		$this->set( 'm_strMohSound', CStrings::strTrimDef( $strMohSound, 240, NULL, true ) );
	}

	public function getMohSound() {
		return $this->m_strMohSound;
	}

	public function sqlMohSound() {
		return ( true == isset( $this->m_strMohSound ) ) ? '\'' . addslashes( $this->m_strMohSound ) . '\'' : 'NULL';
	}

	public function setRecordTemplate( $strRecordTemplate ) {
		$this->set( 'm_strRecordTemplate', CStrings::strTrimDef( $strRecordTemplate, 240, NULL, true ) );
	}

	public function getRecordTemplate() {
		return $this->m_strRecordTemplate;
	}

	public function sqlRecordTemplate() {
		return ( true == isset( $this->m_strRecordTemplate ) ) ? '\'' . addslashes( $this->m_strRecordTemplate ) . '\'' : 'NULL';
	}

	public function setTimeBaseScore( $strTimeBaseScore ) {
		$this->set( 'm_strTimeBaseScore', CStrings::strTrimDef( $strTimeBaseScore, 240, NULL, true ) );
	}

	public function getTimeBaseScore() {
		return $this->m_strTimeBaseScore;
	}

	public function sqlTimeBaseScore() {
		return ( true == isset( $this->m_strTimeBaseScore ) ) ? '\'' . addslashes( $this->m_strTimeBaseScore ) . '\'' : 'NULL';
	}

	public function setTierRulesApply( $intTierRulesApply ) {
		$this->set( 'm_intTierRulesApply', CStrings::strToIntDef( $intTierRulesApply, NULL, false ) );
	}

	public function getTierRulesApply() {
		return $this->m_intTierRulesApply;
	}

	public function sqlTierRulesApply() {
		return ( true == isset( $this->m_intTierRulesApply ) ) ? ( string ) $this->m_intTierRulesApply : '0';
	}

	public function setTierRuleWaitSecond( $intTierRuleWaitSecond ) {
		$this->set( 'm_intTierRuleWaitSecond', CStrings::strToIntDef( $intTierRuleWaitSecond, NULL, false ) );
	}

	public function getTierRuleWaitSecond() {
		return $this->m_intTierRuleWaitSecond;
	}

	public function sqlTierRuleWaitSecond() {
		return ( true == isset( $this->m_intTierRuleWaitSecond ) ) ? ( string ) $this->m_intTierRuleWaitSecond : '1';
	}

	public function setTierRuleWaitMultiplyLevel( $intTierRuleWaitMultiplyLevel ) {
		$this->set( 'm_intTierRuleWaitMultiplyLevel', CStrings::strToIntDef( $intTierRuleWaitMultiplyLevel, NULL, false ) );
	}

	public function getTierRuleWaitMultiplyLevel() {
		return $this->m_intTierRuleWaitMultiplyLevel;
	}

	public function sqlTierRuleWaitMultiplyLevel() {
		return ( true == isset( $this->m_intTierRuleWaitMultiplyLevel ) ) ? ( string ) $this->m_intTierRuleWaitMultiplyLevel : '1';
	}

	public function setTierRuleNoAgentNoWait( $intTierRuleNoAgentNoWait ) {
		$this->set( 'm_intTierRuleNoAgentNoWait', CStrings::strToIntDef( $intTierRuleNoAgentNoWait, NULL, false ) );
	}

	public function getTierRuleNoAgentNoWait() {
		return $this->m_intTierRuleNoAgentNoWait;
	}

	public function sqlTierRuleNoAgentNoWait() {
		return ( true == isset( $this->m_intTierRuleNoAgentNoWait ) ) ? ( string ) $this->m_intTierRuleNoAgentNoWait : '0';
	}

	public function setAbandonedResumeAllowed( $intAbandonedResumeAllowed ) {
		$this->set( 'm_intAbandonedResumeAllowed', CStrings::strToIntDef( $intAbandonedResumeAllowed, NULL, false ) );
	}

	public function getAbandonedResumeAllowed() {
		return $this->m_intAbandonedResumeAllowed;
	}

	public function sqlAbandonedResumeAllowed() {
		return ( true == isset( $this->m_intAbandonedResumeAllowed ) ) ? ( string ) $this->m_intAbandonedResumeAllowed : '0';
	}

	public function setMaxWaitTime( $intMaxWaitTime ) {
		$this->set( 'm_intMaxWaitTime', CStrings::strToIntDef( $intMaxWaitTime, NULL, false ) );
	}

	public function getMaxWaitTime() {
		return $this->m_intMaxWaitTime;
	}

	public function sqlMaxWaitTime() {
		return ( true == isset( $this->m_intMaxWaitTime ) ) ? ( string ) $this->m_intMaxWaitTime : '0';
	}

	public function setMaxWaitTimeWithNoAgent( $intMaxWaitTimeWithNoAgent ) {
		$this->set( 'm_intMaxWaitTimeWithNoAgent', CStrings::strToIntDef( $intMaxWaitTimeWithNoAgent, NULL, false ) );
	}

	public function getMaxWaitTimeWithNoAgent() {
		return $this->m_intMaxWaitTimeWithNoAgent;
	}

	public function sqlMaxWaitTimeWithNoAgent() {
		return ( true == isset( $this->m_intMaxWaitTimeWithNoAgent ) ) ? ( string ) $this->m_intMaxWaitTimeWithNoAgent : 'NULL';
	}

	public function setMaxWaitTimeWithNoAgentTimeReached( $intMaxWaitTimeWithNoAgentTimeReached ) {
		$this->set( 'm_intMaxWaitTimeWithNoAgentTimeReached', CStrings::strToIntDef( $intMaxWaitTimeWithNoAgentTimeReached, NULL, false ) );
	}

	public function getMaxWaitTimeWithNoAgentTimeReached() {
		return $this->m_intMaxWaitTimeWithNoAgentTimeReached;
	}

	public function sqlMaxWaitTimeWithNoAgentTimeReached() {
		return ( true == isset( $this->m_intMaxWaitTimeWithNoAgentTimeReached ) ) ? ( string ) $this->m_intMaxWaitTimeWithNoAgentTimeReached : '1';
	}

	public function setDiscardAbandonedAfterCallers( $intDiscardAbandonedAfterCallers ) {
		$this->set( 'm_intDiscardAbandonedAfterCallers', CStrings::strToIntDef( $intDiscardAbandonedAfterCallers, NULL, false ) );
	}

	public function getDiscardAbandonedAfterCallers() {
		return $this->m_intDiscardAbandonedAfterCallers;
	}

	public function sqlDiscardAbandonedAfterCallers() {
		return ( true == isset( $this->m_intDiscardAbandonedAfterCallers ) ) ? ( string ) $this->m_intDiscardAbandonedAfterCallers : 'NULL';
	}

	public function setMultiplyLevelWait( $intMultiplyLevelWait ) {
		$this->set( 'm_intMultiplyLevelWait', CStrings::strToIntDef( $intMultiplyLevelWait, NULL, false ) );
	}

	public function getMultiplyLevelWait() {
		return $this->m_intMultiplyLevelWait;
	}

	public function sqlMultiplyLevelWait() {
		return ( true == isset( $this->m_intMultiplyLevelWait ) ) ? ( string ) $this->m_intMultiplyLevelWait : '0';
	}

	public function setNotificationEmail( $strNotificationEmail ) {
		$this->set( 'm_strNotificationEmail', CStrings::strTrimDef( $strNotificationEmail, 120, NULL, true ) );
	}

	public function getNotificationEmail() {
		return $this->m_strNotificationEmail;
	}

	public function sqlNotificationEmail() {
		return ( true == isset( $this->m_strNotificationEmail ) ) ? '\'' . addslashes( $this->m_strNotificationEmail ) . '\'' : 'NULL';
	}

	public function setIsOverflowEnabled( $intIsOverflowEnabled ) {
		$this->set( 'm_intIsOverflowEnabled', CStrings::strToIntDef( $intIsOverflowEnabled, NULL, false ) );
	}

	public function getIsOverflowEnabled() {
		return $this->m_intIsOverflowEnabled;
	}

	public function sqlIsOverflowEnabled() {
		return ( true == isset( $this->m_intIsOverflowEnabled ) ) ? ( string ) $this->m_intIsOverflowEnabled : '0';
	}

	public function setIsVoicemail( $intIsVoicemail ) {
		$this->set( 'm_intIsVoicemail', CStrings::strToIntDef( $intIsVoicemail, NULL, false ) );
	}

	public function getIsVoicemail() {
		return $this->m_intIsVoicemail;
	}

	public function sqlIsVoicemail() {
		return ( true == isset( $this->m_intIsVoicemail ) ) ? ( string ) $this->m_intIsVoicemail : '0';
	}

	public function setIsCustomSettings( $intIsCustomSettings ) {
		$this->set( 'm_intIsCustomSettings', CStrings::strToIntDef( $intIsCustomSettings, NULL, false ) );
	}

	public function getIsCustomSettings() {
		return $this->m_intIsCustomSettings;
	}

	public function sqlIsCustomSettings() {
		return ( true == isset( $this->m_intIsCustomSettings ) ) ? ( string ) $this->m_intIsCustomSettings : '0';
	}

	public function setIsAutoIdleState( $intIsAutoIdleState ) {
		$this->set( 'm_intIsAutoIdleState', CStrings::strToIntDef( $intIsAutoIdleState, NULL, false ) );
	}

	public function getIsAutoIdleState() {
		return $this->m_intIsAutoIdleState;
	}

	public function sqlIsAutoIdleState() {
		return ( true == isset( $this->m_intIsAutoIdleState ) ) ? ( string ) $this->m_intIsAutoIdleState : '1';
	}

	public function setLanguage( $strLanguage ) {
		$this->set( 'm_strLanguage', CStrings::strTrimDef( $strLanguage, 10, NULL, true ) );
	}

	public function getLanguage() {
		return $this->m_strLanguage;
	}

	public function sqlLanguage() {
		return ( true == isset( $this->m_strLanguage ) ) ? '\'' . addslashes( $this->m_strLanguage ) . '\'' : '\'en\'';
	}

	public function setTimezoneOffset( $intTimezoneOffset ) {
		$this->set( 'm_intTimezoneOffset', CStrings::strToIntDef( $intTimezoneOffset, NULL, false ) );
	}

	public function getTimezoneOffset() {
		return $this->m_intTimezoneOffset;
	}

	public function sqlTimezoneOffset() {
		return ( true == isset( $this->m_intTimezoneOffset ) ) ? ( string ) $this->m_intTimezoneOffset : 'NULL';
	}

	public function setAnnounceSound( $strAnnounceSound ) {
		$this->set( 'm_strAnnounceSound', CStrings::strTrimDef( $strAnnounceSound, 2048, NULL, true ) );
	}

	public function getAnnounceSound() {
		return $this->m_strAnnounceSound;
	}

	public function sqlAnnounceSound() {
		return ( true == isset( $this->m_strAnnounceSound ) ) ? '\'' . addslashes( $this->m_strAnnounceSound ) . '\'' : 'NULL';
	}

	public function setAnnounceFrequency( $intAnnounceFrequency ) {
		$this->set( 'm_intAnnounceFrequency', CStrings::strToIntDef( $intAnnounceFrequency, NULL, false ) );
	}

	public function getAnnounceFrequency() {
		return $this->m_intAnnounceFrequency;
	}

	public function sqlAnnounceFrequency() {
		return ( true == isset( $this->m_intAnnounceFrequency ) ) ? ( string ) $this->m_intAnnounceFrequency : 'NULL';
	}

	public function setSunOpenTime( $strSunOpenTime ) {
		$this->set( 'm_strSunOpenTime', CStrings::strTrimDef( $strSunOpenTime, NULL, NULL, true ) );
	}

	public function getSunOpenTime() {
		return $this->m_strSunOpenTime;
	}

	public function sqlSunOpenTime() {
		return ( true == isset( $this->m_strSunOpenTime ) ) ? '\'' . $this->m_strSunOpenTime . '\'' : 'NULL';
	}

	public function setSunCloseTime( $strSunCloseTime ) {
		$this->set( 'm_strSunCloseTime', CStrings::strTrimDef( $strSunCloseTime, NULL, NULL, true ) );
	}

	public function getSunCloseTime() {
		return $this->m_strSunCloseTime;
	}

	public function sqlSunCloseTime() {
		return ( true == isset( $this->m_strSunCloseTime ) ) ? '\'' . $this->m_strSunCloseTime . '\'' : 'NULL';
	}

	public function setMonOpenTime( $strMonOpenTime ) {
		$this->set( 'm_strMonOpenTime', CStrings::strTrimDef( $strMonOpenTime, NULL, NULL, true ) );
	}

	public function getMonOpenTime() {
		return $this->m_strMonOpenTime;
	}

	public function sqlMonOpenTime() {
		return ( true == isset( $this->m_strMonOpenTime ) ) ? '\'' . $this->m_strMonOpenTime . '\'' : 'NULL';
	}

	public function setMonCloseTime( $strMonCloseTime ) {
		$this->set( 'm_strMonCloseTime', CStrings::strTrimDef( $strMonCloseTime, NULL, NULL, true ) );
	}

	public function getMonCloseTime() {
		return $this->m_strMonCloseTime;
	}

	public function sqlMonCloseTime() {
		return ( true == isset( $this->m_strMonCloseTime ) ) ? '\'' . $this->m_strMonCloseTime . '\'' : 'NULL';
	}

	public function setTueOpenTime( $strTueOpenTime ) {
		$this->set( 'm_strTueOpenTime', CStrings::strTrimDef( $strTueOpenTime, NULL, NULL, true ) );
	}

	public function getTueOpenTime() {
		return $this->m_strTueOpenTime;
	}

	public function sqlTueOpenTime() {
		return ( true == isset( $this->m_strTueOpenTime ) ) ? '\'' . $this->m_strTueOpenTime . '\'' : 'NULL';
	}

	public function setTueCloseTime( $strTueCloseTime ) {
		$this->set( 'm_strTueCloseTime', CStrings::strTrimDef( $strTueCloseTime, NULL, NULL, true ) );
	}

	public function getTueCloseTime() {
		return $this->m_strTueCloseTime;
	}

	public function sqlTueCloseTime() {
		return ( true == isset( $this->m_strTueCloseTime ) ) ? '\'' . $this->m_strTueCloseTime . '\'' : 'NULL';
	}

	public function setWedOpenTime( $strWedOpenTime ) {
		$this->set( 'm_strWedOpenTime', CStrings::strTrimDef( $strWedOpenTime, NULL, NULL, true ) );
	}

	public function getWedOpenTime() {
		return $this->m_strWedOpenTime;
	}

	public function sqlWedOpenTime() {
		return ( true == isset( $this->m_strWedOpenTime ) ) ? '\'' . $this->m_strWedOpenTime . '\'' : 'NULL';
	}

	public function setWedCloseTime( $strWedCloseTime ) {
		$this->set( 'm_strWedCloseTime', CStrings::strTrimDef( $strWedCloseTime, NULL, NULL, true ) );
	}

	public function getWedCloseTime() {
		return $this->m_strWedCloseTime;
	}

	public function sqlWedCloseTime() {
		return ( true == isset( $this->m_strWedCloseTime ) ) ? '\'' . $this->m_strWedCloseTime . '\'' : 'NULL';
	}

	public function setThuOpenTime( $strThuOpenTime ) {
		$this->set( 'm_strThuOpenTime', CStrings::strTrimDef( $strThuOpenTime, NULL, NULL, true ) );
	}

	public function getThuOpenTime() {
		return $this->m_strThuOpenTime;
	}

	public function sqlThuOpenTime() {
		return ( true == isset( $this->m_strThuOpenTime ) ) ? '\'' . $this->m_strThuOpenTime . '\'' : 'NULL';
	}

	public function setThuCloseTime( $strThuCloseTime ) {
		$this->set( 'm_strThuCloseTime', CStrings::strTrimDef( $strThuCloseTime, NULL, NULL, true ) );
	}

	public function getThuCloseTime() {
		return $this->m_strThuCloseTime;
	}

	public function sqlThuCloseTime() {
		return ( true == isset( $this->m_strThuCloseTime ) ) ? '\'' . $this->m_strThuCloseTime . '\'' : 'NULL';
	}

	public function setFriOpenTime( $strFriOpenTime ) {
		$this->set( 'm_strFriOpenTime', CStrings::strTrimDef( $strFriOpenTime, NULL, NULL, true ) );
	}

	public function getFriOpenTime() {
		return $this->m_strFriOpenTime;
	}

	public function sqlFriOpenTime() {
		return ( true == isset( $this->m_strFriOpenTime ) ) ? '\'' . $this->m_strFriOpenTime . '\'' : 'NULL';
	}

	public function setFriCloseTime( $strFriCloseTime ) {
		$this->set( 'm_strFriCloseTime', CStrings::strTrimDef( $strFriCloseTime, NULL, NULL, true ) );
	}

	public function getFriCloseTime() {
		return $this->m_strFriCloseTime;
	}

	public function sqlFriCloseTime() {
		return ( true == isset( $this->m_strFriCloseTime ) ) ? '\'' . $this->m_strFriCloseTime . '\'' : 'NULL';
	}

	public function setSatOpenTime( $strSatOpenTime ) {
		$this->set( 'm_strSatOpenTime', CStrings::strTrimDef( $strSatOpenTime, NULL, NULL, true ) );
	}

	public function getSatOpenTime() {
		return $this->m_strSatOpenTime;
	}

	public function sqlSatOpenTime() {
		return ( true == isset( $this->m_strSatOpenTime ) ) ? '\'' . $this->m_strSatOpenTime . '\'' : 'NULL';
	}

	public function setSatCloseTime( $strSatCloseTime ) {
		$this->set( 'm_strSatCloseTime', CStrings::strTrimDef( $strSatCloseTime, NULL, NULL, true ) );
	}

	public function getSatCloseTime() {
		return $this->m_strSatCloseTime;
	}

	public function sqlSatCloseTime() {
		return ( true == isset( $this->m_strSatCloseTime ) ) ? '\'' . $this->m_strSatCloseTime . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStaticWrapUpTime( $intStaticWrapUpTime ) {
		$this->set( 'm_intStaticWrapUpTime', CStrings::strToIntDef( $intStaticWrapUpTime, NULL, false ) );
	}

	public function getStaticWrapUpTime() {
		return $this->m_intStaticWrapUpTime;
	}

	public function sqlStaticWrapUpTime() {
		return ( true == isset( $this->m_intStaticWrapUpTime ) ) ? ( string ) $this->m_intStaticWrapUpTime : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, overflow_call_queue_id, call_strategy_type_id, call_queue_type_id, department_id, call_file_id, queue_phone_extension_id, call_type_id, name, description, domain, moh_sound, record_template, time_base_score, tier_rules_apply, tier_rule_wait_second, tier_rule_wait_multiply_level, tier_rule_no_agent_no_wait, abandoned_resume_allowed, max_wait_time, max_wait_time_with_no_agent, max_wait_time_with_no_agent_time_reached, discard_abandoned_after_callers, multiply_level_wait, notification_email, is_overflow_enabled, is_voicemail, is_custom_settings, is_auto_idle_state, language, timezone_offset, announce_sound, announce_frequency, sun_open_time, sun_close_time, mon_open_time, mon_close_time, tue_open_time, tue_close_time, wed_open_time, wed_close_time, thu_open_time, thu_close_time, fri_open_time, fri_close_time, sat_open_time, sat_close_time, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, static_wrap_up_time )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOverflowCallQueueId() . ', ' .
 						$this->sqlCallStrategyTypeId() . ', ' .
 						$this->sqlCallQueueTypeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlCallFileId() . ', ' .
 						$this->sqlQueuePhoneExtensionId() . ', ' .
 						$this->sqlCallTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDomain() . ', ' .
 						$this->sqlMohSound() . ', ' .
 						$this->sqlRecordTemplate() . ', ' .
 						$this->sqlTimeBaseScore() . ', ' .
 						$this->sqlTierRulesApply() . ', ' .
 						$this->sqlTierRuleWaitSecond() . ', ' .
 						$this->sqlTierRuleWaitMultiplyLevel() . ', ' .
 						$this->sqlTierRuleNoAgentNoWait() . ', ' .
 						$this->sqlAbandonedResumeAllowed() . ', ' .
 						$this->sqlMaxWaitTime() . ', ' .
 						$this->sqlMaxWaitTimeWithNoAgent() . ', ' .
 						$this->sqlMaxWaitTimeWithNoAgentTimeReached() . ', ' .
 						$this->sqlDiscardAbandonedAfterCallers() . ', ' .
 						$this->sqlMultiplyLevelWait() . ', ' .
 						$this->sqlNotificationEmail() . ', ' .
 						$this->sqlIsOverflowEnabled() . ', ' .
 						$this->sqlIsVoicemail() . ', ' .
 						$this->sqlIsCustomSettings() . ', ' .
 						$this->sqlIsAutoIdleState() . ', ' .
 						$this->sqlLanguage() . ', ' .
 						$this->sqlTimezoneOffset() . ', ' .
 						$this->sqlAnnounceSound() . ', ' .
 						$this->sqlAnnounceFrequency() . ', ' .
 						$this->sqlSunOpenTime() . ', ' .
 						$this->sqlSunCloseTime() . ', ' .
 						$this->sqlMonOpenTime() . ', ' .
 						$this->sqlMonCloseTime() . ', ' .
 						$this->sqlTueOpenTime() . ', ' .
 						$this->sqlTueCloseTime() . ', ' .
 						$this->sqlWedOpenTime() . ', ' .
 						$this->sqlWedCloseTime() . ', ' .
 						$this->sqlThuOpenTime() . ', ' .
 						$this->sqlThuCloseTime() . ', ' .
 						$this->sqlFriOpenTime() . ', ' .
 						$this->sqlFriCloseTime() . ', ' .
 						$this->sqlSatOpenTime() . ', ' .
 						$this->sqlSatCloseTime() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlStaticWrapUpTime() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' overflow_call_queue_id = ' . $this->sqlOverflowCallQueueId() . ','; } elseif( true == array_key_exists( 'OverflowCallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' overflow_call_queue_id = ' . $this->sqlOverflowCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_strategy_type_id = ' . $this->sqlCallStrategyTypeId() . ','; } elseif( true == array_key_exists( 'CallStrategyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_strategy_type_id = ' . $this->sqlCallStrategyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_type_id = ' . $this->sqlCallQueueTypeId() . ','; } elseif( true == array_key_exists( 'CallQueueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_type_id = ' . $this->sqlCallQueueTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_file_id = ' . $this->sqlCallFileId() . ','; } elseif( true == array_key_exists( 'CallFileId', $this->getChangedColumns() ) ) { $strSql .= ' call_file_id = ' . $this->sqlCallFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue_phone_extension_id = ' . $this->sqlQueuePhoneExtensionId() . ','; } elseif( true == array_key_exists( 'QueuePhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' queue_phone_extension_id = ' . $this->sqlQueuePhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; } elseif( true == array_key_exists( 'CallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' moh_sound = ' . $this->sqlMohSound() . ','; } elseif( true == array_key_exists( 'MohSound', $this->getChangedColumns() ) ) { $strSql .= ' moh_sound = ' . $this->sqlMohSound() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' record_template = ' . $this->sqlRecordTemplate() . ','; } elseif( true == array_key_exists( 'RecordTemplate', $this->getChangedColumns() ) ) { $strSql .= ' record_template = ' . $this->sqlRecordTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_base_score = ' . $this->sqlTimeBaseScore() . ','; } elseif( true == array_key_exists( 'TimeBaseScore', $this->getChangedColumns() ) ) { $strSql .= ' time_base_score = ' . $this->sqlTimeBaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_rules_apply = ' . $this->sqlTierRulesApply() . ','; } elseif( true == array_key_exists( 'TierRulesApply', $this->getChangedColumns() ) ) { $strSql .= ' tier_rules_apply = ' . $this->sqlTierRulesApply() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_rule_wait_second = ' . $this->sqlTierRuleWaitSecond() . ','; } elseif( true == array_key_exists( 'TierRuleWaitSecond', $this->getChangedColumns() ) ) { $strSql .= ' tier_rule_wait_second = ' . $this->sqlTierRuleWaitSecond() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_rule_wait_multiply_level = ' . $this->sqlTierRuleWaitMultiplyLevel() . ','; } elseif( true == array_key_exists( 'TierRuleWaitMultiplyLevel', $this->getChangedColumns() ) ) { $strSql .= ' tier_rule_wait_multiply_level = ' . $this->sqlTierRuleWaitMultiplyLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tier_rule_no_agent_no_wait = ' . $this->sqlTierRuleNoAgentNoWait() . ','; } elseif( true == array_key_exists( 'TierRuleNoAgentNoWait', $this->getChangedColumns() ) ) { $strSql .= ' tier_rule_no_agent_no_wait = ' . $this->sqlTierRuleNoAgentNoWait() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' abandoned_resume_allowed = ' . $this->sqlAbandonedResumeAllowed() . ','; } elseif( true == array_key_exists( 'AbandonedResumeAllowed', $this->getChangedColumns() ) ) { $strSql .= ' abandoned_resume_allowed = ' . $this->sqlAbandonedResumeAllowed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_wait_time = ' . $this->sqlMaxWaitTime() . ','; } elseif( true == array_key_exists( 'MaxWaitTime', $this->getChangedColumns() ) ) { $strSql .= ' max_wait_time = ' . $this->sqlMaxWaitTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_wait_time_with_no_agent = ' . $this->sqlMaxWaitTimeWithNoAgent() . ','; } elseif( true == array_key_exists( 'MaxWaitTimeWithNoAgent', $this->getChangedColumns() ) ) { $strSql .= ' max_wait_time_with_no_agent = ' . $this->sqlMaxWaitTimeWithNoAgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_wait_time_with_no_agent_time_reached = ' . $this->sqlMaxWaitTimeWithNoAgentTimeReached() . ','; } elseif( true == array_key_exists( 'MaxWaitTimeWithNoAgentTimeReached', $this->getChangedColumns() ) ) { $strSql .= ' max_wait_time_with_no_agent_time_reached = ' . $this->sqlMaxWaitTimeWithNoAgentTimeReached() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discard_abandoned_after_callers = ' . $this->sqlDiscardAbandonedAfterCallers() . ','; } elseif( true == array_key_exists( 'DiscardAbandonedAfterCallers', $this->getChangedColumns() ) ) { $strSql .= ' discard_abandoned_after_callers = ' . $this->sqlDiscardAbandonedAfterCallers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' multiply_level_wait = ' . $this->sqlMultiplyLevelWait() . ','; } elseif( true == array_key_exists( 'MultiplyLevelWait', $this->getChangedColumns() ) ) { $strSql .= ' multiply_level_wait = ' . $this->sqlMultiplyLevelWait() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_email = ' . $this->sqlNotificationEmail() . ','; } elseif( true == array_key_exists( 'NotificationEmail', $this->getChangedColumns() ) ) { $strSql .= ' notification_email = ' . $this->sqlNotificationEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_overflow_enabled = ' . $this->sqlIsOverflowEnabled() . ','; } elseif( true == array_key_exists( 'IsOverflowEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_overflow_enabled = ' . $this->sqlIsOverflowEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_voicemail = ' . $this->sqlIsVoicemail() . ','; } elseif( true == array_key_exists( 'IsVoicemail', $this->getChangedColumns() ) ) { $strSql .= ' is_voicemail = ' . $this->sqlIsVoicemail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_custom_settings = ' . $this->sqlIsCustomSettings() . ','; } elseif( true == array_key_exists( 'IsCustomSettings', $this->getChangedColumns() ) ) { $strSql .= ' is_custom_settings = ' . $this->sqlIsCustomSettings() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_idle_state = ' . $this->sqlIsAutoIdleState() . ','; } elseif( true == array_key_exists( 'IsAutoIdleState', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_idle_state = ' . $this->sqlIsAutoIdleState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' language = ' . $this->sqlLanguage() . ','; } elseif( true == array_key_exists( 'Language', $this->getChangedColumns() ) ) { $strSql .= ' language = ' . $this->sqlLanguage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' timezone_offset = ' . $this->sqlTimezoneOffset() . ','; } elseif( true == array_key_exists( 'TimezoneOffset', $this->getChangedColumns() ) ) { $strSql .= ' timezone_offset = ' . $this->sqlTimezoneOffset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' announce_sound = ' . $this->sqlAnnounceSound() . ','; } elseif( true == array_key_exists( 'AnnounceSound', $this->getChangedColumns() ) ) { $strSql .= ' announce_sound = ' . $this->sqlAnnounceSound() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' announce_frequency = ' . $this->sqlAnnounceFrequency() . ','; } elseif( true == array_key_exists( 'AnnounceFrequency', $this->getChangedColumns() ) ) { $strSql .= ' announce_frequency = ' . $this->sqlAnnounceFrequency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; } elseif( true == array_key_exists( 'SunOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; } elseif( true == array_key_exists( 'SunCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mon_open_time = ' . $this->sqlMonOpenTime() . ','; } elseif( true == array_key_exists( 'MonOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' mon_open_time = ' . $this->sqlMonOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mon_close_time = ' . $this->sqlMonCloseTime() . ','; } elseif( true == array_key_exists( 'MonCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' mon_close_time = ' . $this->sqlMonCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tue_open_time = ' . $this->sqlTueOpenTime() . ','; } elseif( true == array_key_exists( 'TueOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' tue_open_time = ' . $this->sqlTueOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tue_close_time = ' . $this->sqlTueCloseTime() . ','; } elseif( true == array_key_exists( 'TueCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' tue_close_time = ' . $this->sqlTueCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wed_open_time = ' . $this->sqlWedOpenTime() . ','; } elseif( true == array_key_exists( 'WedOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' wed_open_time = ' . $this->sqlWedOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wed_close_time = ' . $this->sqlWedCloseTime() . ','; } elseif( true == array_key_exists( 'WedCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' wed_close_time = ' . $this->sqlWedCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thu_open_time = ' . $this->sqlThuOpenTime() . ','; } elseif( true == array_key_exists( 'ThuOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' thu_open_time = ' . $this->sqlThuOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thu_close_time = ' . $this->sqlThuCloseTime() . ','; } elseif( true == array_key_exists( 'ThuCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' thu_close_time = ' . $this->sqlThuCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fri_open_time = ' . $this->sqlFriOpenTime() . ','; } elseif( true == array_key_exists( 'FriOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' fri_open_time = ' . $this->sqlFriOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fri_close_time = ' . $this->sqlFriCloseTime() . ','; } elseif( true == array_key_exists( 'FriCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' fri_close_time = ' . $this->sqlFriCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; } elseif( true == array_key_exists( 'SatOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; } elseif( true == array_key_exists( 'SatCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' static_wrap_up_time = ' . $this->sqlStaticWrapUpTime() . ','; } elseif( true == array_key_exists( 'StaticWrapUpTime', $this->getChangedColumns() ) ) { $strSql .= ' static_wrap_up_time = ' . $this->sqlStaticWrapUpTime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'overflow_call_queue_id' => $this->getOverflowCallQueueId(),
			'call_strategy_type_id' => $this->getCallStrategyTypeId(),
			'call_queue_type_id' => $this->getCallQueueTypeId(),
			'department_id' => $this->getDepartmentId(),
			'call_file_id' => $this->getCallFileId(),
			'queue_phone_extension_id' => $this->getQueuePhoneExtensionId(),
			'call_type_id' => $this->getCallTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'domain' => $this->getDomain(),
			'moh_sound' => $this->getMohSound(),
			'record_template' => $this->getRecordTemplate(),
			'time_base_score' => $this->getTimeBaseScore(),
			'tier_rules_apply' => $this->getTierRulesApply(),
			'tier_rule_wait_second' => $this->getTierRuleWaitSecond(),
			'tier_rule_wait_multiply_level' => $this->getTierRuleWaitMultiplyLevel(),
			'tier_rule_no_agent_no_wait' => $this->getTierRuleNoAgentNoWait(),
			'abandoned_resume_allowed' => $this->getAbandonedResumeAllowed(),
			'max_wait_time' => $this->getMaxWaitTime(),
			'max_wait_time_with_no_agent' => $this->getMaxWaitTimeWithNoAgent(),
			'max_wait_time_with_no_agent_time_reached' => $this->getMaxWaitTimeWithNoAgentTimeReached(),
			'discard_abandoned_after_callers' => $this->getDiscardAbandonedAfterCallers(),
			'multiply_level_wait' => $this->getMultiplyLevelWait(),
			'notification_email' => $this->getNotificationEmail(),
			'is_overflow_enabled' => $this->getIsOverflowEnabled(),
			'is_voicemail' => $this->getIsVoicemail(),
			'is_custom_settings' => $this->getIsCustomSettings(),
			'is_auto_idle_state' => $this->getIsAutoIdleState(),
			'language' => $this->getLanguage(),
			'timezone_offset' => $this->getTimezoneOffset(),
			'announce_sound' => $this->getAnnounceSound(),
			'announce_frequency' => $this->getAnnounceFrequency(),
			'sun_open_time' => $this->getSunOpenTime(),
			'sun_close_time' => $this->getSunCloseTime(),
			'mon_open_time' => $this->getMonOpenTime(),
			'mon_close_time' => $this->getMonCloseTime(),
			'tue_open_time' => $this->getTueOpenTime(),
			'tue_close_time' => $this->getTueCloseTime(),
			'wed_open_time' => $this->getWedOpenTime(),
			'wed_close_time' => $this->getWedCloseTime(),
			'thu_open_time' => $this->getThuOpenTime(),
			'thu_close_time' => $this->getThuCloseTime(),
			'fri_open_time' => $this->getFriOpenTime(),
			'fri_close_time' => $this->getFriCloseTime(),
			'sat_open_time' => $this->getSatOpenTime(),
			'sat_close_time' => $this->getSatCloseTime(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'static_wrap_up_time' => $this->getStaticWrapUpTime()
		);
	}

}
?>