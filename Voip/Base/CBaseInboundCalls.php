<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCalls
 * Do not add any new functions to this class.
 */

class CBaseInboundCalls extends CEosPluralBase {

	/**
	 * @return CInboundCall[]
	 */
	public static function fetchInboundCalls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInboundCall', $objDatabase );
	}

	/**
	 * @return CInboundCall
	 */
	public static function fetchInboundCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInboundCall', $objDatabase );
	}

	public static function fetchInboundCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inbound_calls', $objDatabase );
	}

	public static function fetchInboundCallById( $intId, $objDatabase ) {
		return self::fetchInboundCall( sprintf( 'SELECT * FROM inbound_calls WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInboundCallsByCid( $intCid, $objDatabase ) {
		return self::fetchInboundCalls( sprintf( 'SELECT * FROM inbound_calls WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchInboundCallsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchInboundCalls( sprintf( 'SELECT * FROM inbound_calls WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>