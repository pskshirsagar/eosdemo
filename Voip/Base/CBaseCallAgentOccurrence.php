<?php

class CBaseCallAgentOccurrence extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_occurrences';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intOccurrenceTypeId;
	protected $m_strOccurrenceDate;
	protected $m_fltOccurrenceAmount;
	protected $m_boolIsReported;
	protected $m_boolIsDisputed;
	protected $m_strDisputeReason;
	protected $m_boolIsDisputeApproved;
	protected $m_intDisputeReviewedBy;
	protected $m_strDisputeReviewedOn;
	protected $m_strDisputeReviewedNotes;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltOccurrenceAmount = '0.00';
		$this->m_boolIsReported = false;
		$this->m_boolIsDisputed = false;
		$this->m_boolIsDisputeApproved = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['occurrence_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOccurrenceTypeId', trim( $arrValues['occurrence_type_id'] ) ); elseif( isset( $arrValues['occurrence_type_id'] ) ) $this->setOccurrenceTypeId( $arrValues['occurrence_type_id'] );
		if( isset( $arrValues['occurrence_date'] ) && $boolDirectSet ) $this->set( 'm_strOccurrenceDate', trim( $arrValues['occurrence_date'] ) ); elseif( isset( $arrValues['occurrence_date'] ) ) $this->setOccurrenceDate( $arrValues['occurrence_date'] );
		if( isset( $arrValues['occurrence_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOccurrenceAmount', trim( $arrValues['occurrence_amount'] ) ); elseif( isset( $arrValues['occurrence_amount'] ) ) $this->setOccurrenceAmount( $arrValues['occurrence_amount'] );
		if( isset( $arrValues['is_reported'] ) && $boolDirectSet ) $this->set( 'm_boolIsReported', trim( stripcslashes( $arrValues['is_reported'] ) ) ); elseif( isset( $arrValues['is_reported'] ) ) $this->setIsReported( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reported'] ) : $arrValues['is_reported'] );
		if( isset( $arrValues['is_disputed'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisputed', trim( stripcslashes( $arrValues['is_disputed'] ) ) ); elseif( isset( $arrValues['is_disputed'] ) ) $this->setIsDisputed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disputed'] ) : $arrValues['is_disputed'] );
		if( isset( $arrValues['dispute_reason'] ) && $boolDirectSet ) $this->set( 'm_strDisputeReason', trim( stripcslashes( $arrValues['dispute_reason'] ) ) ); elseif( isset( $arrValues['dispute_reason'] ) ) $this->setDisputeReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dispute_reason'] ) : $arrValues['dispute_reason'] );
		if( isset( $arrValues['is_dispute_approved'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisputeApproved', trim( stripcslashes( $arrValues['is_dispute_approved'] ) ) ); elseif( isset( $arrValues['is_dispute_approved'] ) ) $this->setIsDisputeApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dispute_approved'] ) : $arrValues['is_dispute_approved'] );
		if( isset( $arrValues['dispute_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intDisputeReviewedBy', trim( $arrValues['dispute_reviewed_by'] ) ); elseif( isset( $arrValues['dispute_reviewed_by'] ) ) $this->setDisputeReviewedBy( $arrValues['dispute_reviewed_by'] );
		if( isset( $arrValues['dispute_reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strDisputeReviewedOn', trim( $arrValues['dispute_reviewed_on'] ) ); elseif( isset( $arrValues['dispute_reviewed_on'] ) ) $this->setDisputeReviewedOn( $arrValues['dispute_reviewed_on'] );
		if( isset( $arrValues['dispute_reviewed_notes'] ) && $boolDirectSet ) $this->set( 'm_strDisputeReviewedNotes', trim( stripcslashes( $arrValues['dispute_reviewed_notes'] ) ) ); elseif( isset( $arrValues['dispute_reviewed_notes'] ) ) $this->setDisputeReviewedNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dispute_reviewed_notes'] ) : $arrValues['dispute_reviewed_notes'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setOccurrenceTypeId( $intOccurrenceTypeId ) {
		$this->set( 'm_intOccurrenceTypeId', CStrings::strToIntDef( $intOccurrenceTypeId, NULL, false ) );
	}

	public function getOccurrenceTypeId() {
		return $this->m_intOccurrenceTypeId;
	}

	public function sqlOccurrenceTypeId() {
		return ( true == isset( $this->m_intOccurrenceTypeId ) ) ? ( string ) $this->m_intOccurrenceTypeId : 'NULL';
	}

	public function setOccurrenceDate( $strOccurrenceDate ) {
		$this->set( 'm_strOccurrenceDate', CStrings::strTrimDef( $strOccurrenceDate, -1, NULL, true ) );
	}

	public function getOccurrenceDate() {
		return $this->m_strOccurrenceDate;
	}

	public function sqlOccurrenceDate() {
		return ( true == isset( $this->m_strOccurrenceDate ) ) ? '\'' . $this->m_strOccurrenceDate . '\'' : 'NOW()';
	}

	public function setOccurrenceAmount( $fltOccurrenceAmount ) {
		$this->set( 'm_fltOccurrenceAmount', CStrings::strToFloatDef( $fltOccurrenceAmount, NULL, false, 2 ) );
	}

	public function getOccurrenceAmount() {
		return $this->m_fltOccurrenceAmount;
	}

	public function sqlOccurrenceAmount() {
		return ( true == isset( $this->m_fltOccurrenceAmount ) ) ? ( string ) $this->m_fltOccurrenceAmount : '0.00';
	}

	public function setIsReported( $boolIsReported ) {
		$this->set( 'm_boolIsReported', CStrings::strToBool( $boolIsReported ) );
	}

	public function getIsReported() {
		return $this->m_boolIsReported;
	}

	public function sqlIsReported() {
		return ( true == isset( $this->m_boolIsReported ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReported ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisputed( $boolIsDisputed ) {
		$this->set( 'm_boolIsDisputed', CStrings::strToBool( $boolIsDisputed ) );
	}

	public function getIsDisputed() {
		return $this->m_boolIsDisputed;
	}

	public function sqlIsDisputed() {
		return ( true == isset( $this->m_boolIsDisputed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisputed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisputeReason( $strDisputeReason ) {
		$this->set( 'm_strDisputeReason', CStrings::strTrimDef( $strDisputeReason, 1500, NULL, true ) );
	}

	public function getDisputeReason() {
		return $this->m_strDisputeReason;
	}

	public function sqlDisputeReason() {
		return ( true == isset( $this->m_strDisputeReason ) ) ? '\'' . addslashes( $this->m_strDisputeReason ) . '\'' : 'NULL';
	}

	public function setIsDisputeApproved( $boolIsDisputeApproved ) {
		$this->set( 'm_boolIsDisputeApproved', CStrings::strToBool( $boolIsDisputeApproved ) );
	}

	public function getIsDisputeApproved() {
		return $this->m_boolIsDisputeApproved;
	}

	public function sqlIsDisputeApproved() {
		return ( true == isset( $this->m_boolIsDisputeApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisputeApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDisputeReviewedBy( $intDisputeReviewedBy ) {
		$this->set( 'm_intDisputeReviewedBy', CStrings::strToIntDef( $intDisputeReviewedBy, NULL, false ) );
	}

	public function getDisputeReviewedBy() {
		return $this->m_intDisputeReviewedBy;
	}

	public function sqlDisputeReviewedBy() {
		return ( true == isset( $this->m_intDisputeReviewedBy ) ) ? ( string ) $this->m_intDisputeReviewedBy : 'NULL';
	}

	public function setDisputeReviewedOn( $strDisputeReviewedOn ) {
		$this->set( 'm_strDisputeReviewedOn', CStrings::strTrimDef( $strDisputeReviewedOn, -1, NULL, true ) );
	}

	public function getDisputeReviewedOn() {
		return $this->m_strDisputeReviewedOn;
	}

	public function sqlDisputeReviewedOn() {
		return ( true == isset( $this->m_strDisputeReviewedOn ) ) ? '\'' . $this->m_strDisputeReviewedOn . '\'' : 'NULL';
	}

	public function setDisputeReviewedNotes( $strDisputeReviewedNotes ) {
		$this->set( 'm_strDisputeReviewedNotes', CStrings::strTrimDef( $strDisputeReviewedNotes, 1500, NULL, true ) );
	}

	public function getDisputeReviewedNotes() {
		return $this->m_strDisputeReviewedNotes;
	}

	public function sqlDisputeReviewedNotes() {
		return ( true == isset( $this->m_strDisputeReviewedNotes ) ) ? '\'' . addslashes( $this->m_strDisputeReviewedNotes ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, occurrence_type_id, occurrence_date, occurrence_amount, is_reported, is_disputed, dispute_reason, is_dispute_approved, dispute_reviewed_by, dispute_reviewed_on, dispute_reviewed_notes, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlOccurrenceTypeId() . ', ' .
 						$this->sqlOccurrenceDate() . ', ' .
 						$this->sqlOccurrenceAmount() . ', ' .
 						$this->sqlIsReported() . ', ' .
 						$this->sqlIsDisputed() . ', ' .
 						$this->sqlDisputeReason() . ', ' .
 						$this->sqlIsDisputeApproved() . ', ' .
 						$this->sqlDisputeReviewedBy() . ', ' .
 						$this->sqlDisputeReviewedOn() . ', ' .
 						$this->sqlDisputeReviewedNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occurrence_type_id = ' . $this->sqlOccurrenceTypeId() . ','; } elseif( true == array_key_exists( 'OccurrenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' occurrence_type_id = ' . $this->sqlOccurrenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occurrence_date = ' . $this->sqlOccurrenceDate() . ','; } elseif( true == array_key_exists( 'OccurrenceDate', $this->getChangedColumns() ) ) { $strSql .= ' occurrence_date = ' . $this->sqlOccurrenceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occurrence_amount = ' . $this->sqlOccurrenceAmount() . ','; } elseif( true == array_key_exists( 'OccurrenceAmount', $this->getChangedColumns() ) ) { $strSql .= ' occurrence_amount = ' . $this->sqlOccurrenceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reported = ' . $this->sqlIsReported() . ','; } elseif( true == array_key_exists( 'IsReported', $this->getChangedColumns() ) ) { $strSql .= ' is_reported = ' . $this->sqlIsReported() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disputed = ' . $this->sqlIsDisputed() . ','; } elseif( true == array_key_exists( 'IsDisputed', $this->getChangedColumns() ) ) { $strSql .= ' is_disputed = ' . $this->sqlIsDisputed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_reason = ' . $this->sqlDisputeReason() . ','; } elseif( true == array_key_exists( 'DisputeReason', $this->getChangedColumns() ) ) { $strSql .= ' dispute_reason = ' . $this->sqlDisputeReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dispute_approved = ' . $this->sqlIsDisputeApproved() . ','; } elseif( true == array_key_exists( 'IsDisputeApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_dispute_approved = ' . $this->sqlIsDisputeApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_reviewed_by = ' . $this->sqlDisputeReviewedBy() . ','; } elseif( true == array_key_exists( 'DisputeReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' dispute_reviewed_by = ' . $this->sqlDisputeReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_reviewed_on = ' . $this->sqlDisputeReviewedOn() . ','; } elseif( true == array_key_exists( 'DisputeReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' dispute_reviewed_on = ' . $this->sqlDisputeReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dispute_reviewed_notes = ' . $this->sqlDisputeReviewedNotes() . ','; } elseif( true == array_key_exists( 'DisputeReviewedNotes', $this->getChangedColumns() ) ) { $strSql .= ' dispute_reviewed_notes = ' . $this->sqlDisputeReviewedNotes() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'occurrence_type_id' => $this->getOccurrenceTypeId(),
			'occurrence_date' => $this->getOccurrenceDate(),
			'occurrence_amount' => $this->getOccurrenceAmount(),
			'is_reported' => $this->getIsReported(),
			'is_disputed' => $this->getIsDisputed(),
			'dispute_reason' => $this->getDisputeReason(),
			'is_dispute_approved' => $this->getIsDisputeApproved(),
			'dispute_reviewed_by' => $this->getDisputeReviewedBy(),
			'dispute_reviewed_on' => $this->getDisputeReviewedOn(),
			'dispute_reviewed_notes' => $this->getDisputeReviewedNotes(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>