<?php

class CBaseIvrMenuAction extends CEosSingularBase {

	const TABLE_NAME = 'public.ivr_menu_actions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIvrMenuId;
	protected $m_intSubIvrMenuId;
	protected $m_intIvrActionId;
	protected $m_intGreetingId;
	protected $m_intCallResultId;
	protected $m_strActionKey;
	protected $m_strName;
	protected $m_strActionParameter;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ivr_menu_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuId', trim( $arrValues['ivr_menu_id'] ) ); elseif( isset( $arrValues['ivr_menu_id'] ) ) $this->setIvrMenuId( $arrValues['ivr_menu_id'] );
		if( isset( $arrValues['sub_ivr_menu_id'] ) && $boolDirectSet ) $this->set( 'm_intSubIvrMenuId', trim( $arrValues['sub_ivr_menu_id'] ) ); elseif( isset( $arrValues['sub_ivr_menu_id'] ) ) $this->setSubIvrMenuId( $arrValues['sub_ivr_menu_id'] );
		if( isset( $arrValues['ivr_action_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrActionId', trim( $arrValues['ivr_action_id'] ) ); elseif( isset( $arrValues['ivr_action_id'] ) ) $this->setIvrActionId( $arrValues['ivr_action_id'] );
		if( isset( $arrValues['greeting_id'] ) && $boolDirectSet ) $this->set( 'm_intGreetingId', trim( $arrValues['greeting_id'] ) ); elseif( isset( $arrValues['greeting_id'] ) ) $this->setGreetingId( $arrValues['greeting_id'] );
		if( isset( $arrValues['call_result_id'] ) && $boolDirectSet ) $this->set( 'm_intCallResultId', trim( $arrValues['call_result_id'] ) ); elseif( isset( $arrValues['call_result_id'] ) ) $this->setCallResultId( $arrValues['call_result_id'] );
		if( isset( $arrValues['action_key'] ) && $boolDirectSet ) $this->set( 'm_strActionKey', trim( stripcslashes( $arrValues['action_key'] ) ) ); elseif( isset( $arrValues['action_key'] ) ) $this->setActionKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_key'] ) : $arrValues['action_key'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['action_parameter'] ) && $boolDirectSet ) $this->set( 'm_strActionParameter', trim( stripcslashes( $arrValues['action_parameter'] ) ) ); elseif( isset( $arrValues['action_parameter'] ) ) $this->setActionParameter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_parameter'] ) : $arrValues['action_parameter'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIvrMenuId( $intIvrMenuId ) {
		$this->set( 'm_intIvrMenuId', CStrings::strToIntDef( $intIvrMenuId, NULL, false ) );
	}

	public function getIvrMenuId() {
		return $this->m_intIvrMenuId;
	}

	public function sqlIvrMenuId() {
		return ( true == isset( $this->m_intIvrMenuId ) ) ? ( string ) $this->m_intIvrMenuId : 'NULL';
	}

	public function setSubIvrMenuId( $intSubIvrMenuId ) {
		$this->set( 'm_intSubIvrMenuId', CStrings::strToIntDef( $intSubIvrMenuId, NULL, false ) );
	}

	public function getSubIvrMenuId() {
		return $this->m_intSubIvrMenuId;
	}

	public function sqlSubIvrMenuId() {
		return ( true == isset( $this->m_intSubIvrMenuId ) ) ? ( string ) $this->m_intSubIvrMenuId : 'NULL';
	}

	public function setIvrActionId( $intIvrActionId ) {
		$this->set( 'm_intIvrActionId', CStrings::strToIntDef( $intIvrActionId, NULL, false ) );
	}

	public function getIvrActionId() {
		return $this->m_intIvrActionId;
	}

	public function sqlIvrActionId() {
		return ( true == isset( $this->m_intIvrActionId ) ) ? ( string ) $this->m_intIvrActionId : 'NULL';
	}

	public function setGreetingId( $intGreetingId ) {
		$this->set( 'm_intGreetingId', CStrings::strToIntDef( $intGreetingId, NULL, false ) );
	}

	public function getGreetingId() {
		return $this->m_intGreetingId;
	}

	public function sqlGreetingId() {
		return ( true == isset( $this->m_intGreetingId ) ) ? ( string ) $this->m_intGreetingId : 'NULL';
	}

	public function setCallResultId( $intCallResultId ) {
		$this->set( 'm_intCallResultId', CStrings::strToIntDef( $intCallResultId, NULL, false ) );
	}

	public function getCallResultId() {
		return $this->m_intCallResultId;
	}

	public function sqlCallResultId() {
		return ( true == isset( $this->m_intCallResultId ) ) ? ( string ) $this->m_intCallResultId : 'NULL';
	}

	public function setActionKey( $strActionKey ) {
		$this->set( 'm_strActionKey', CStrings::strTrimDef( $strActionKey, 40, NULL, true ) );
	}

	public function getActionKey() {
		return $this->m_strActionKey;
	}

	public function sqlActionKey() {
		return ( true == isset( $this->m_strActionKey ) ) ? '\'' . addslashes( $this->m_strActionKey ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setActionParameter( $strActionParameter ) {
		$this->set( 'm_strActionParameter', CStrings::strTrimDef( $strActionParameter, 512, NULL, true ) );
	}

	public function getActionParameter() {
		return $this->m_strActionParameter;
	}

	public function sqlActionParameter() {
		return ( true == isset( $this->m_strActionParameter ) ) ? '\'' . addslashes( $this->m_strActionParameter ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ivr_menu_id, sub_ivr_menu_id, ivr_action_id, greeting_id, call_result_id, action_key, name, action_parameter, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIvrMenuId() . ', ' .
 						$this->sqlSubIvrMenuId() . ', ' .
 						$this->sqlIvrActionId() . ', ' .
 						$this->sqlGreetingId() . ', ' .
 						$this->sqlCallResultId() . ', ' .
 						$this->sqlActionKey() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlActionParameter() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; } elseif( true == array_key_exists( 'IvrMenuId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_ivr_menu_id = ' . $this->sqlSubIvrMenuId() . ','; } elseif( true == array_key_exists( 'SubIvrMenuId', $this->getChangedColumns() ) ) { $strSql .= ' sub_ivr_menu_id = ' . $this->sqlSubIvrMenuId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_action_id = ' . $this->sqlIvrActionId() . ','; } elseif( true == array_key_exists( 'IvrActionId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_action_id = ' . $this->sqlIvrActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' greeting_id = ' . $this->sqlGreetingId() . ','; } elseif( true == array_key_exists( 'GreetingId', $this->getChangedColumns() ) ) { $strSql .= ' greeting_id = ' . $this->sqlGreetingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId() . ','; } elseif( true == array_key_exists( 'CallResultId', $this->getChangedColumns() ) ) { $strSql .= ' call_result_id = ' . $this->sqlCallResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_key = ' . $this->sqlActionKey() . ','; } elseif( true == array_key_exists( 'ActionKey', $this->getChangedColumns() ) ) { $strSql .= ' action_key = ' . $this->sqlActionKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_parameter = ' . $this->sqlActionParameter() . ','; } elseif( true == array_key_exists( 'ActionParameter', $this->getChangedColumns() ) ) { $strSql .= ' action_parameter = ' . $this->sqlActionParameter() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ivr_menu_id' => $this->getIvrMenuId(),
			'sub_ivr_menu_id' => $this->getSubIvrMenuId(),
			'ivr_action_id' => $this->getIvrActionId(),
			'greeting_id' => $this->getGreetingId(),
			'call_result_id' => $this->getCallResultId(),
			'action_key' => $this->getActionKey(),
			'name' => $this->getName(),
			'action_parameter' => $this->getActionParameter(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>