<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CFollowups
 * Do not add any new functions to this class.
 */

class CBaseFollowups extends CEosPluralBase {

	/**
	 * @return CFollowup[]
	 */
	public static function fetchFollowups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CFollowup::class, $objDatabase );
	}

	/**
	 * @return CFollowup
	 */
	public static function fetchFollowup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CFollowup::class, $objDatabase );
	}

	public static function fetchFollowupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'followups', $objDatabase );
	}

	public static function fetchFollowupById( $intId, $objDatabase ) {
		return self::fetchFollowup( sprintf( 'SELECT * FROM followups WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchFollowupsByFollowupTypeId( $intFollowupTypeId, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE followup_type_id = %d', $intFollowupTypeId ), $objDatabase );
	}

	public static function fetchFollowupsByCid( $intCid, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchFollowupsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchFollowupsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchFollowupsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchFollowupsByFollowupStatusTypeId( $intFollowupStatusTypeId, $objDatabase ) {
		return self::fetchFollowups( sprintf( 'SELECT * FROM followups WHERE followup_status_type_id = %d', $intFollowupStatusTypeId ), $objDatabase );
	}

}
?>