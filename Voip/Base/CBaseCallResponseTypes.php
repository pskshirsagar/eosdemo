<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallResponseTypes
 * Do not add any new functions to this class.
 */

class CBaseCallResponseTypes extends CEosPluralBase {

	/**
	 * @return CCallResponseType[]
	 */
	public static function fetchCallResponseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallResponseType::class, $objDatabase );
	}

	/**
	 * @return CCallResponseType
	 */
	public static function fetchCallResponseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallResponseType::class, $objDatabase );
	}

	public static function fetchCallResponseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_response_types', $objDatabase );
	}

	public static function fetchCallResponseTypeById( $intId, $objDatabase ) {
		return self::fetchCallResponseType( sprintf( 'SELECT * FROM call_response_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>