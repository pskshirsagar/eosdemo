<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRewardPointTypes
 * Do not add any new functions to this class.
 */

class CBaseRewardPointTypes extends CEosPluralBase {

	/**
	 * @return CRewardPointType[]
	 */
	public static function fetchRewardPointTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRewardPointType::class, $objDatabase );
	}

	/**
	 * @return CRewardPointType
	 */
	public static function fetchRewardPointType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRewardPointType::class, $objDatabase );
	}

	public static function fetchRewardPointTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reward_point_types', $objDatabase );
	}

	public static function fetchRewardPointTypeById( $intId, $objDatabase ) {
		return self::fetchRewardPointType( sprintf( 'SELECT * FROM reward_point_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>