<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentQueues
 * Do not add any new functions to this class.
 */

class CBaseCallAgentQueues extends CEosPluralBase {

	/**
	 * @return CCallAgentQueue[]
	 */
	public static function fetchCallAgentQueues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentQueue', $objDatabase );
	}

	/**
	 * @return CCallAgentQueue
	 */
	public static function fetchCallAgentQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentQueue', $objDatabase );
	}

	public static function fetchCallAgentQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_queues', $objDatabase );
	}

	public static function fetchCallAgentQueueById( $intId, $objDatabase ) {
		return self::fetchCallAgentQueue( sprintf( 'SELECT * FROM call_agent_queues WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentQueuesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentQueues( sprintf( 'SELECT * FROM call_agent_queues WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentQueuesByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallAgentQueues( sprintf( 'SELECT * FROM call_agent_queues WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

}
?>