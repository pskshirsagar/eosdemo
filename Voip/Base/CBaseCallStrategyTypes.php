<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallStrategyTypes
 * Do not add any new functions to this class.
 */

class CBaseCallStrategyTypes extends CEosPluralBase {

	/**
	 * @return CCallStrategyType[]
	 */
	public static function fetchCallStrategyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallStrategyType::class, $objDatabase );
	}

	/**
	 * @return CCallStrategyType
	 */
	public static function fetchCallStrategyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallStrategyType::class, $objDatabase );
	}

	public static function fetchCallStrategyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_strategy_types', $objDatabase );
	}

	public static function fetchCallStrategyTypeById( $intId, $objDatabase ) {
		return self::fetchCallStrategyType( sprintf( 'SELECT * FROM call_strategy_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>