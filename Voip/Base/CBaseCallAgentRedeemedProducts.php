<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentRedeemedProducts
 * Do not add any new functions to this class.
 */

class CBaseCallAgentRedeemedProducts extends CEosPluralBase {

	/**
	 * @return CCallAgentRedeemedProduct[]
	 */
	public static function fetchCallAgentRedeemedProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentRedeemedProduct::class, $objDatabase );
	}

	/**
	 * @return CCallAgentRedeemedProduct
	 */
	public static function fetchCallAgentRedeemedProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentRedeemedProduct::class, $objDatabase );
	}

	public static function fetchCallAgentRedeemedProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_redeemed_products', $objDatabase );
	}

	public static function fetchCallAgentRedeemedProductById( $intId, $objDatabase ) {
		return self::fetchCallAgentRedeemedProduct( sprintf( 'SELECT * FROM call_agent_redeemed_products WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentRedeemedProductsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentRedeemedProducts( sprintf( 'SELECT * FROM call_agent_redeemed_products WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentRedeemedProductsByStoreProductId( $intStoreProductId, $objDatabase ) {
		return self::fetchCallAgentRedeemedProducts( sprintf( 'SELECT * FROM call_agent_redeemed_products WHERE store_product_id = %d', $intStoreProductId ), $objDatabase );
	}

	public static function fetchCallAgentRedeemedProductsByCallAgentRankId( $intCallAgentRankId, $objDatabase ) {
		return self::fetchCallAgentRedeemedProducts( sprintf( 'SELECT * FROM call_agent_redeemed_products WHERE call_agent_rank_id = %d', $intCallAgentRankId ), $objDatabase );
	}

}
?>