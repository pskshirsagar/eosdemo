<?php

class CBaseCallQueueLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_queue_logs';

	protected $m_intId;
	protected $m_intCallQueueId;
	protected $m_strCurrentData;
	protected $m_jsonCurrentData;
	protected $m_strPreviousData;
	protected $m_jsonPreviousData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['current_data'] ) ) $this->set( 'm_strCurrentData', trim( $arrValues['current_data'] ) );
		if( isset( $arrValues['previous_data'] ) ) $this->set( 'm_strPreviousData', trim( $arrValues['previous_data'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setCurrentData( $jsonCurrentData ) {
		if( true == valObj( $jsonCurrentData, 'stdClass' ) ) {
			$this->set( 'm_jsonCurrentData', $jsonCurrentData );
		} elseif( true == valJsonString( $jsonCurrentData ) ) {
			$this->set( 'm_jsonCurrentData', CStrings::strToJson( $jsonCurrentData ) );
		} else {
			$this->set( 'm_jsonCurrentData', NULL ); 
		}
		unset( $this->m_strCurrentData );
	}

	public function getCurrentData() {
		if( true == isset( $this->m_strCurrentData ) ) {
			$this->m_jsonCurrentData = CStrings::strToJson( $this->m_strCurrentData );
			unset( $this->m_strCurrentData );
		}
		return $this->m_jsonCurrentData;
	}

	public function sqlCurrentData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getCurrentData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getCurrentData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getCurrentData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setPreviousData( $jsonPreviousData ) {
		if( true == valObj( $jsonPreviousData, 'stdClass' ) ) {
			$this->set( 'm_jsonPreviousData', $jsonPreviousData );
		} elseif( true == valJsonString( $jsonPreviousData ) ) {
			$this->set( 'm_jsonPreviousData', CStrings::strToJson( $jsonPreviousData ) );
		} else {
			$this->set( 'm_jsonPreviousData', NULL ); 
		}
		unset( $this->m_strPreviousData );
	}

	public function getPreviousData() {
		if( true == isset( $this->m_strPreviousData ) ) {
			$this->m_jsonPreviousData = CStrings::strToJson( $this->m_strPreviousData );
			unset( $this->m_strPreviousData );
		}
		return $this->m_jsonPreviousData;
	}

	public function sqlPreviousData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getPreviousData() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getPreviousData() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getPreviousData() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_queue_id, current_data, previous_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallQueueId() . ', ' .
						$this->sqlCurrentData() . ', ' .
						$this->sqlPreviousData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId(). ',' ; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_data = ' . $this->sqlCurrentData(). ',' ; } elseif( true == array_key_exists( 'CurrentData', $this->getChangedColumns() ) ) { $strSql .= ' current_data = ' . $this->sqlCurrentData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_data = ' . $this->sqlPreviousData() ; } elseif( true == array_key_exists( 'PreviousData', $this->getChangedColumns() ) ) { $strSql .= ' previous_data = ' . $this->sqlPreviousData() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_queue_id' => $this->getCallQueueId(),
			'current_data' => $this->getCurrentData(),
			'previous_data' => $this->getPreviousData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>