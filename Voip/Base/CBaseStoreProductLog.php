<?php

class CBaseStoreProductLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.store_product_logs';

	protected $m_intId;
	protected $m_intStoreProductLogTypeId;
	protected $m_intStoreProductId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['store_product_log_type_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreProductLogTypeId', trim( $arrValues['store_product_log_type_id'] ) ); elseif( isset( $arrValues['store_product_log_type_id'] ) ) $this->setStoreProductLogTypeId( $arrValues['store_product_log_type_id'] );
		if( isset( $arrValues['store_product_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreProductId', trim( $arrValues['store_product_id'] ) ); elseif( isset( $arrValues['store_product_id'] ) ) $this->setStoreProductId( $arrValues['store_product_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStoreProductLogTypeId( $intStoreProductLogTypeId ) {
		$this->set( 'm_intStoreProductLogTypeId', CStrings::strToIntDef( $intStoreProductLogTypeId, NULL, false ) );
	}

	public function getStoreProductLogTypeId() {
		return $this->m_intStoreProductLogTypeId;
	}

	public function sqlStoreProductLogTypeId() {
		return ( true == isset( $this->m_intStoreProductLogTypeId ) ) ? ( string ) $this->m_intStoreProductLogTypeId : 'NULL';
	}

	public function setStoreProductId( $intStoreProductId ) {
		$this->set( 'm_intStoreProductId', CStrings::strToIntDef( $intStoreProductId, NULL, false ) );
	}

	public function getStoreProductId() {
		return $this->m_intStoreProductId;
	}

	public function sqlStoreProductId() {
		return ( true == isset( $this->m_intStoreProductId ) ) ? ( string ) $this->m_intStoreProductId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, store_product_log_type_id, store_product_id, details, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlStoreProductLogTypeId() . ', ' .
						$this->sqlStoreProductId() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_product_log_type_id = ' . $this->sqlStoreProductLogTypeId(). ',' ; } elseif( true == array_key_exists( 'StoreProductLogTypeId', $this->getChangedColumns() ) ) { $strSql .= ' store_product_log_type_id = ' . $this->sqlStoreProductLogTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_product_id = ' . $this->sqlStoreProductId(). ',' ; } elseif( true == array_key_exists( 'StoreProductId', $this->getChangedColumns() ) ) { $strSql .= ' store_product_id = ' . $this->sqlStoreProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'store_product_log_type_id' => $this->getStoreProductLogTypeId(),
			'store_product_id' => $this->getStoreProductId(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>