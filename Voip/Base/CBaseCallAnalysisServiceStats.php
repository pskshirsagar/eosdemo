<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisServiceStats
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisServiceStats extends CEosPluralBase {

	/**
	 * @return CCallAnalysisServiceStat[]
	 */
	public static function fetchCallAnalysisServiceStats( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisServiceStat::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisServiceStat
	 */
	public static function fetchCallAnalysisServiceStat( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisServiceStat::class, $objDatabase );
	}

	public static function fetchCallAnalysisServiceStatCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_service_stats', $objDatabase );
	}

	public static function fetchCallAnalysisServiceStatById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisServiceStat( sprintf( 'SELECT * FROM call_analysis_service_stats WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisServiceStatsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisServiceStats( sprintf( 'SELECT * FROM call_analysis_service_stats WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisServiceStatsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisServiceStats( sprintf( 'SELECT * FROM call_analysis_service_stats WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

}
?>