<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRewardPointMultipliers
 * Do not add any new functions to this class.
 */

class CBaseRewardPointMultipliers extends CEosPluralBase {

	/**
	 * @return CRewardPointMultiplier[]
	 */
	public static function fetchRewardPointMultipliers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRewardPointMultiplier::class, $objDatabase );
	}

	/**
	 * @return CRewardPointMultiplier
	 */
	public static function fetchRewardPointMultiplier( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRewardPointMultiplier::class, $objDatabase );
	}

	public static function fetchRewardPointMultiplierCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reward_point_multipliers', $objDatabase );
	}

	public static function fetchRewardPointMultiplierById( $intId, $objDatabase ) {
		return self::fetchRewardPointMultiplier( sprintf( 'SELECT * FROM reward_point_multipliers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>