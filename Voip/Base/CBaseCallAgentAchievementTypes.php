<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentAchievementTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentAchievementTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentAchievementType[]
	 */
	public static function fetchCallAgentAchievementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentAchievementType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentAchievementType
	 */
	public static function fetchCallAgentAchievementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentAchievementType::class, $objDatabase );
	}

	public static function fetchCallAgentAchievementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_achievement_types', $objDatabase );
	}

	public static function fetchCallAgentAchievementTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentAchievementType( sprintf( 'SELECT * FROM call_agent_achievement_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentAchievementTypesByRewardPointTypeId( $intRewardPointTypeId, $objDatabase ) {
		return self::fetchCallAgentAchievementTypes( sprintf( 'SELECT * FROM call_agent_achievement_types WHERE reward_point_type_id = %d', $intRewardPointTypeId ), $objDatabase );
	}

	public static function fetchCallAgentAchievementTypesByCallAgentAchievementTypeId( $intCallAgentAchievementTypeId, $objDatabase ) {
		return self::fetchCallAgentAchievementTypes( sprintf( 'SELECT * FROM call_agent_achievement_types WHERE call_agent_achievement_type_id = %d', $intCallAgentAchievementTypeId ), $objDatabase );
	}

}
?>