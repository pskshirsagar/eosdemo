<?php

class CBaseCallFilter extends CEosSingularBase {

	const TABLE_NAME = 'public.call_filters';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCids;
	protected $m_strProperties;
	protected $m_intEmployeeId;
	protected $m_intCallFilterTypeId;
	protected $m_intCallZoneId;
	protected $m_strCallQueues;
	protected $m_strCallTypes;
	protected $m_strCallAgents;
	protected $m_strTeams;
	protected $m_strCallStatusTypes;
	protected $m_strCallResults;
	protected $m_strCallDirection;
	protected $m_strPhoneNumbers;
	protected $m_strLeadSourceIds;
	protected $m_strApplicantApplicationIds;
	protected $m_strPsProductPropertyIds;
	protected $m_strVoicemailStatusTypes;
	protected $m_strGenericData;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_boolIsArchivedFlag;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsArchivedFlag = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['cids'] ) && $boolDirectSet ) $this->set( 'm_strCids', trim( stripcslashes( $arrValues['cids'] ) ) ); elseif( isset( $arrValues['cids'] ) ) $this->setCids( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cids'] ) : $arrValues['cids'] );
		if( isset( $arrValues['properties'] ) && $boolDirectSet ) $this->set( 'm_strProperties', trim( stripcslashes( $arrValues['properties'] ) ) ); elseif( isset( $arrValues['properties'] ) ) $this->setProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['properties'] ) : $arrValues['properties'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['call_filter_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallFilterTypeId', trim( $arrValues['call_filter_type_id'] ) ); elseif( isset( $arrValues['call_filter_type_id'] ) ) $this->setCallFilterTypeId( $arrValues['call_filter_type_id'] );
		if( isset( $arrValues['call_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intCallZoneId', trim( $arrValues['call_zone_id'] ) ); elseif( isset( $arrValues['call_zone_id'] ) ) $this->setCallZoneId( $arrValues['call_zone_id'] );
		if( isset( $arrValues['call_queues'] ) && $boolDirectSet ) $this->set( 'm_strCallQueues', trim( stripcslashes( $arrValues['call_queues'] ) ) ); elseif( isset( $arrValues['call_queues'] ) ) $this->setCallQueues( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_queues'] ) : $arrValues['call_queues'] );
		if( isset( $arrValues['call_types'] ) && $boolDirectSet ) $this->set( 'm_strCallTypes', trim( stripcslashes( $arrValues['call_types'] ) ) ); elseif( isset( $arrValues['call_types'] ) ) $this->setCallTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_types'] ) : $arrValues['call_types'] );
		if( isset( $arrValues['call_agents'] ) && $boolDirectSet ) $this->set( 'm_strCallAgents', trim( stripcslashes( $arrValues['call_agents'] ) ) ); elseif( isset( $arrValues['call_agents'] ) ) $this->setCallAgents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_agents'] ) : $arrValues['call_agents'] );
		if( isset( $arrValues['teams'] ) && $boolDirectSet ) $this->set( 'm_strTeams', trim( stripcslashes( $arrValues['teams'] ) ) ); elseif( isset( $arrValues['teams'] ) ) $this->setTeams( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['teams'] ) : $arrValues['teams'] );
		if( isset( $arrValues['call_status_types'] ) && $boolDirectSet ) $this->set( 'm_strCallStatusTypes', trim( stripcslashes( $arrValues['call_status_types'] ) ) ); elseif( isset( $arrValues['call_status_types'] ) ) $this->setCallStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_status_types'] ) : $arrValues['call_status_types'] );
		if( isset( $arrValues['call_results'] ) && $boolDirectSet ) $this->set( 'm_strCallResults', trim( stripcslashes( $arrValues['call_results'] ) ) ); elseif( isset( $arrValues['call_results'] ) ) $this->setCallResults( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_results'] ) : $arrValues['call_results'] );
		if( isset( $arrValues['call_direction'] ) && $boolDirectSet ) $this->set( 'm_strCallDirection', trim( stripcslashes( $arrValues['call_direction'] ) ) ); elseif( isset( $arrValues['call_direction'] ) ) $this->setCallDirection( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_direction'] ) : $arrValues['call_direction'] );
		if( isset( $arrValues['phone_numbers'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumbers', trim( stripcslashes( $arrValues['phone_numbers'] ) ) ); elseif( isset( $arrValues['phone_numbers'] ) ) $this->setPhoneNumbers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_numbers'] ) : $arrValues['phone_numbers'] );
		if( isset( $arrValues['lead_source_ids'] ) && $boolDirectSet ) $this->set( 'm_strLeadSourceIds', trim( stripcslashes( $arrValues['lead_source_ids'] ) ) ); elseif( isset( $arrValues['lead_source_ids'] ) ) $this->setLeadSourceIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_source_ids'] ) : $arrValues['lead_source_ids'] );
		if( isset( $arrValues['applicant_application_ids'] ) && $boolDirectSet ) $this->set( 'm_strApplicantApplicationIds', trim( stripcslashes( $arrValues['applicant_application_ids'] ) ) ); elseif( isset( $arrValues['applicant_application_ids'] ) ) $this->setApplicantApplicationIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['applicant_application_ids'] ) : $arrValues['applicant_application_ids'] );
		if( isset( $arrValues['ps_product_property_ids'] ) && $boolDirectSet ) $this->set( 'm_strPsProductPropertyIds', trim( stripcslashes( $arrValues['ps_product_property_ids'] ) ) ); elseif( isset( $arrValues['ps_product_property_ids'] ) ) $this->setPsProductPropertyIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_product_property_ids'] ) : $arrValues['ps_product_property_ids'] );
		if( isset( $arrValues['voicemail_status_types'] ) && $boolDirectSet ) $this->set( 'm_strVoicemailStatusTypes', trim( stripcslashes( $arrValues['voicemail_status_types'] ) ) ); elseif( isset( $arrValues['voicemail_status_types'] ) ) $this->setVoicemailStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['voicemail_status_types'] ) : $arrValues['voicemail_status_types'] );
		if( isset( $arrValues['generic_data'] ) && $boolDirectSet ) $this->set( 'm_strGenericData', trim( stripcslashes( $arrValues['generic_data'] ) ) ); elseif( isset( $arrValues['generic_data'] ) ) $this->setGenericData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['generic_data'] ) : $arrValues['generic_data'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['is_archived_flag'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchivedFlag', trim( stripcslashes( $arrValues['is_archived_flag'] ) ) ); elseif( isset( $arrValues['is_archived_flag'] ) ) $this->setIsArchivedFlag( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived_flag'] ) : $arrValues['is_archived_flag'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCids( $strCids ) {
		$this->set( 'm_strCids', CStrings::strTrimDef( $strCids, 5000, NULL, true ) );
	}

	public function getCids() {
		return $this->m_strCids;
	}

	public function sqlCids() {
		return ( true == isset( $this->m_strCids ) ) ? '\'' . addslashes( $this->m_strCids ) . '\'' : 'NULL';
	}

	public function setProperties( $strProperties ) {
		$this->set( 'm_strProperties', CStrings::strTrimDef( $strProperties, 5000, NULL, true ) );
	}

	public function getProperties() {
		return $this->m_strProperties;
	}

	public function sqlProperties() {
		return ( true == isset( $this->m_strProperties ) ) ? '\'' . addslashes( $this->m_strProperties ) . '\'' : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCallFilterTypeId( $intCallFilterTypeId ) {
		$this->set( 'm_intCallFilterTypeId', CStrings::strToIntDef( $intCallFilterTypeId, NULL, false ) );
	}

	public function getCallFilterTypeId() {
		return $this->m_intCallFilterTypeId;
	}

	public function sqlCallFilterTypeId() {
		return ( true == isset( $this->m_intCallFilterTypeId ) ) ? ( string ) $this->m_intCallFilterTypeId : 'NULL';
	}

	public function setCallZoneId( $intCallZoneId ) {
		$this->set( 'm_intCallZoneId', CStrings::strToIntDef( $intCallZoneId, NULL, false ) );
	}

	public function getCallZoneId() {
		return $this->m_intCallZoneId;
	}

	public function sqlCallZoneId() {
		return ( true == isset( $this->m_intCallZoneId ) ) ? ( string ) $this->m_intCallZoneId : 'NULL';
	}

	public function setCallQueues( $strCallQueues ) {
		$this->set( 'm_strCallQueues', CStrings::strTrimDef( $strCallQueues, 1000, NULL, true ) );
	}

	public function getCallQueues() {
		return $this->m_strCallQueues;
	}

	public function sqlCallQueues() {
		return ( true == isset( $this->m_strCallQueues ) ) ? '\'' . addslashes( $this->m_strCallQueues ) . '\'' : 'NULL';
	}

	public function setCallTypes( $strCallTypes ) {
		$this->set( 'm_strCallTypes', CStrings::strTrimDef( $strCallTypes, 1000, NULL, true ) );
	}

	public function getCallTypes() {
		return $this->m_strCallTypes;
	}

	public function sqlCallTypes() {
		return ( true == isset( $this->m_strCallTypes ) ) ? '\'' . addslashes( $this->m_strCallTypes ) . '\'' : 'NULL';
	}

	public function setCallAgents( $strCallAgents ) {
		$this->set( 'm_strCallAgents', CStrings::strTrimDef( $strCallAgents, 1000, NULL, true ) );
	}

	public function getCallAgents() {
		return $this->m_strCallAgents;
	}

	public function sqlCallAgents() {
		return ( true == isset( $this->m_strCallAgents ) ) ? '\'' . addslashes( $this->m_strCallAgents ) . '\'' : 'NULL';
	}

	public function setTeams( $strTeams ) {
		$this->set( 'm_strTeams', CStrings::strTrimDef( $strTeams, 1000, NULL, true ) );
	}

	public function getTeams() {
		return $this->m_strTeams;
	}

	public function sqlTeams() {
		return ( true == isset( $this->m_strTeams ) ) ? '\'' . addslashes( $this->m_strTeams ) . '\'' : 'NULL';
	}

	public function setCallStatusTypes( $strCallStatusTypes ) {
		$this->set( 'm_strCallStatusTypes', CStrings::strTrimDef( $strCallStatusTypes, 1000, NULL, true ) );
	}

	public function getCallStatusTypes() {
		return $this->m_strCallStatusTypes;
	}

	public function sqlCallStatusTypes() {
		return ( true == isset( $this->m_strCallStatusTypes ) ) ? '\'' . addslashes( $this->m_strCallStatusTypes ) . '\'' : 'NULL';
	}

	public function setCallResults( $strCallResults ) {
		$this->set( 'm_strCallResults', CStrings::strTrimDef( $strCallResults, 1000, NULL, true ) );
	}

	public function getCallResults() {
		return $this->m_strCallResults;
	}

	public function sqlCallResults() {
		return ( true == isset( $this->m_strCallResults ) ) ? '\'' . addslashes( $this->m_strCallResults ) . '\'' : 'NULL';
	}

	public function setCallDirection( $strCallDirection ) {
		$this->set( 'm_strCallDirection', CStrings::strTrimDef( $strCallDirection, 50, NULL, true ) );
	}

	public function getCallDirection() {
		return $this->m_strCallDirection;
	}

	public function sqlCallDirection() {
		return ( true == isset( $this->m_strCallDirection ) ) ? '\'' . addslashes( $this->m_strCallDirection ) . '\'' : 'NULL';
	}

	public function setPhoneNumbers( $strPhoneNumbers ) {
		$this->set( 'm_strPhoneNumbers', CStrings::strTrimDef( $strPhoneNumbers, 5000, NULL, true ) );
	}

	public function getPhoneNumbers() {
		return $this->m_strPhoneNumbers;
	}

	public function sqlPhoneNumbers() {
		return ( true == isset( $this->m_strPhoneNumbers ) ) ? '\'' . addslashes( $this->m_strPhoneNumbers ) . '\'' : 'NULL';
	}

	public function setLeadSourceIds( $strLeadSourceIds ) {
		$this->set( 'm_strLeadSourceIds', CStrings::strTrimDef( $strLeadSourceIds, 5000, NULL, true ) );
	}

	public function getLeadSourceIds() {
		return $this->m_strLeadSourceIds;
	}

	public function sqlLeadSourceIds() {
		return ( true == isset( $this->m_strLeadSourceIds ) ) ? '\'' . addslashes( $this->m_strLeadSourceIds ) . '\'' : 'NULL';
	}

	public function setApplicantApplicationIds( $strApplicantApplicationIds ) {
		$this->set( 'm_strApplicantApplicationIds', CStrings::strTrimDef( $strApplicantApplicationIds, 5000, NULL, true ) );
	}

	public function getApplicantApplicationIds() {
		return $this->m_strApplicantApplicationIds;
	}

	public function sqlApplicantApplicationIds() {
		return ( true == isset( $this->m_strApplicantApplicationIds ) ) ? '\'' . addslashes( $this->m_strApplicantApplicationIds ) . '\'' : 'NULL';
	}

	public function setPsProductPropertyIds( $strPsProductPropertyIds ) {
		$this->set( 'm_strPsProductPropertyIds', CStrings::strTrimDef( $strPsProductPropertyIds, 5000, NULL, true ) );
	}

	public function getPsProductPropertyIds() {
		return $this->m_strPsProductPropertyIds;
	}

	public function sqlPsProductPropertyIds() {
		return ( true == isset( $this->m_strPsProductPropertyIds ) ) ? '\'' . addslashes( $this->m_strPsProductPropertyIds ) . '\'' : 'NULL';
	}

	public function setVoicemailStatusTypes( $strVoicemailStatusTypes ) {
		$this->set( 'm_strVoicemailStatusTypes', CStrings::strTrimDef( $strVoicemailStatusTypes, 50, NULL, true ) );
	}

	public function getVoicemailStatusTypes() {
		return $this->m_strVoicemailStatusTypes;
	}

	public function sqlVoicemailStatusTypes() {
		return ( true == isset( $this->m_strVoicemailStatusTypes ) ) ? '\'' . addslashes( $this->m_strVoicemailStatusTypes ) . '\'' : 'NULL';
	}

	public function setGenericData( $strGenericData ) {
		$this->set( 'm_strGenericData', CStrings::strTrimDef( $strGenericData, 1000, NULL, true ) );
	}

	public function getGenericData() {
		return $this->m_strGenericData;
	}

	public function sqlGenericData() {
		return ( true == isset( $this->m_strGenericData ) ) ? '\'' . addslashes( $this->m_strGenericData ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setIsArchivedFlag( $boolIsArchivedFlag ) {
		$this->set( 'm_boolIsArchivedFlag', CStrings::strToBool( $boolIsArchivedFlag ) );
	}

	public function getIsArchivedFlag() {
		return $this->m_boolIsArchivedFlag;
	}

	public function sqlIsArchivedFlag() {
		return ( true == isset( $this->m_boolIsArchivedFlag ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchivedFlag ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, description, cids, properties, employee_id, call_filter_type_id, call_zone_id, call_queues, call_types, call_agents, teams, call_status_types, call_results, call_direction, phone_numbers, lead_source_ids, applicant_application_ids, ps_product_property_ids, voicemail_status_types, generic_data, start_date, end_date, is_archived_flag, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCids() . ', ' .
 						$this->sqlProperties() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlCallFilterTypeId() . ', ' .
 						$this->sqlCallZoneId() . ', ' .
 						$this->sqlCallQueues() . ', ' .
 						$this->sqlCallTypes() . ', ' .
 						$this->sqlCallAgents() . ', ' .
 						$this->sqlTeams() . ', ' .
 						$this->sqlCallStatusTypes() . ', ' .
 						$this->sqlCallResults() . ', ' .
 						$this->sqlCallDirection() . ', ' .
 						$this->sqlPhoneNumbers() . ', ' .
 						$this->sqlLeadSourceIds() . ', ' .
 						$this->sqlApplicantApplicationIds() . ', ' .
 						$this->sqlPsProductPropertyIds() . ', ' .
 						$this->sqlVoicemailStatusTypes() . ', ' .
 						$this->sqlGenericData() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlIsArchivedFlag() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cids = ' . $this->sqlCids() . ','; } elseif( true == array_key_exists( 'Cids', $this->getChangedColumns() ) ) { $strSql .= ' cids = ' . $this->sqlCids() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; } elseif( true == array_key_exists( 'Properties', $this->getChangedColumns() ) ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_filter_type_id = ' . $this->sqlCallFilterTypeId() . ','; } elseif( true == array_key_exists( 'CallFilterTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_filter_type_id = ' . $this->sqlCallFilterTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_zone_id = ' . $this->sqlCallZoneId() . ','; } elseif( true == array_key_exists( 'CallZoneId', $this->getChangedColumns() ) ) { $strSql .= ' call_zone_id = ' . $this->sqlCallZoneId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queues = ' . $this->sqlCallQueues() . ','; } elseif( true == array_key_exists( 'CallQueues', $this->getChangedColumns() ) ) { $strSql .= ' call_queues = ' . $this->sqlCallQueues() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_types = ' . $this->sqlCallTypes() . ','; } elseif( true == array_key_exists( 'CallTypes', $this->getChangedColumns() ) ) { $strSql .= ' call_types = ' . $this->sqlCallTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agents = ' . $this->sqlCallAgents() . ','; } elseif( true == array_key_exists( 'CallAgents', $this->getChangedColumns() ) ) { $strSql .= ' call_agents = ' . $this->sqlCallAgents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' teams = ' . $this->sqlTeams() . ','; } elseif( true == array_key_exists( 'Teams', $this->getChangedColumns() ) ) { $strSql .= ' teams = ' . $this->sqlTeams() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_status_types = ' . $this->sqlCallStatusTypes() . ','; } elseif( true == array_key_exists( 'CallStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' call_status_types = ' . $this->sqlCallStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_results = ' . $this->sqlCallResults() . ','; } elseif( true == array_key_exists( 'CallResults', $this->getChangedColumns() ) ) { $strSql .= ' call_results = ' . $this->sqlCallResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_direction = ' . $this->sqlCallDirection() . ','; } elseif( true == array_key_exists( 'CallDirection', $this->getChangedColumns() ) ) { $strSql .= ' call_direction = ' . $this->sqlCallDirection() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_numbers = ' . $this->sqlPhoneNumbers() . ','; } elseif( true == array_key_exists( 'PhoneNumbers', $this->getChangedColumns() ) ) { $strSql .= ' phone_numbers = ' . $this->sqlPhoneNumbers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_source_ids = ' . $this->sqlLeadSourceIds() . ','; } elseif( true == array_key_exists( 'LeadSourceIds', $this->getChangedColumns() ) ) { $strSql .= ' lead_source_ids = ' . $this->sqlLeadSourceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_application_ids = ' . $this->sqlApplicantApplicationIds() . ','; } elseif( true == array_key_exists( 'ApplicantApplicationIds', $this->getChangedColumns() ) ) { $strSql .= ' applicant_application_ids = ' . $this->sqlApplicantApplicationIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_property_ids = ' . $this->sqlPsProductPropertyIds() . ','; } elseif( true == array_key_exists( 'PsProductPropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_property_ids = ' . $this->sqlPsProductPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voicemail_status_types = ' . $this->sqlVoicemailStatusTypes() . ','; } elseif( true == array_key_exists( 'VoicemailStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' voicemail_status_types = ' . $this->sqlVoicemailStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' generic_data = ' . $this->sqlGenericData() . ','; } elseif( true == array_key_exists( 'GenericData', $this->getChangedColumns() ) ) { $strSql .= ' generic_data = ' . $this->sqlGenericData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived_flag = ' . $this->sqlIsArchivedFlag() . ','; } elseif( true == array_key_exists( 'IsArchivedFlag', $this->getChangedColumns() ) ) { $strSql .= ' is_archived_flag = ' . $this->sqlIsArchivedFlag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'cids' => $this->getCids(),
			'properties' => $this->getProperties(),
			'employee_id' => $this->getEmployeeId(),
			'call_filter_type_id' => $this->getCallFilterTypeId(),
			'call_zone_id' => $this->getCallZoneId(),
			'call_queues' => $this->getCallQueues(),
			'call_types' => $this->getCallTypes(),
			'call_agents' => $this->getCallAgents(),
			'teams' => $this->getTeams(),
			'call_status_types' => $this->getCallStatusTypes(),
			'call_results' => $this->getCallResults(),
			'call_direction' => $this->getCallDirection(),
			'phone_numbers' => $this->getPhoneNumbers(),
			'lead_source_ids' => $this->getLeadSourceIds(),
			'applicant_application_ids' => $this->getApplicantApplicationIds(),
			'ps_product_property_ids' => $this->getPsProductPropertyIds(),
			'voicemail_status_types' => $this->getVoicemailStatusTypes(),
			'generic_data' => $this->getGenericData(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'is_archived_flag' => $this->getIsArchivedFlag(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>