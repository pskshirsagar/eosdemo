<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CDialplans
 * Do not add any new functions to this class.
 */

class CBaseDialplans extends CEosPluralBase {

	/**
	 * @return CDialplan[]
	 */
	public static function fetchDialplans( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDialplan', $objDatabase );
	}

	/**
	 * @return CDialplan
	 */
	public static function fetchDialplan( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDialplan', $objDatabase );
	}

	public static function fetchDialplanCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dialplans', $objDatabase );
	}

	public static function fetchDialplanById( $intId, $objDatabase ) {
		return self::fetchDialplan( sprintf( 'SELECT * FROM dialplans WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>