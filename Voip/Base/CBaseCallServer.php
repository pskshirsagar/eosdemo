<?php

class CBaseCallServer extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_servers';

	protected $m_intId;
	protected $m_intCallServerTypeId;
	protected $m_strServerName;
	protected $m_strIpAddress;
	protected $m_strUsername;
	protected $m_strPassword;
	protected $m_strDnsName;
	protected $m_intTotalCpu;
	protected $m_strOsName;
	protected $m_strOsVersion;
	protected $m_intMemorySize;
	protected $m_boolIsPrimary;
	protected $m_boolIsActive;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPrimary = false;
		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_server_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallServerTypeId', trim( $arrValues['call_server_type_id'] ) ); elseif( isset( $arrValues['call_server_type_id'] ) ) $this->setCallServerTypeId( $arrValues['call_server_type_id'] );
		if( isset( $arrValues['server_name'] ) && $boolDirectSet ) $this->set( 'm_strServerName', trim( $arrValues['server_name'] ) ); elseif( isset( $arrValues['server_name'] ) ) $this->setServerName( $arrValues['server_name'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( $arrValues['username'] ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( $arrValues['username'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( $arrValues['password'] ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( $arrValues['password'] );
		if( isset( $arrValues['dns_name'] ) && $boolDirectSet ) $this->set( 'm_strDnsName', trim( $arrValues['dns_name'] ) ); elseif( isset( $arrValues['dns_name'] ) ) $this->setDnsName( $arrValues['dns_name'] );
		if( isset( $arrValues['total_cpu'] ) && $boolDirectSet ) $this->set( 'm_intTotalCpu', trim( $arrValues['total_cpu'] ) ); elseif( isset( $arrValues['total_cpu'] ) ) $this->setTotalCpu( $arrValues['total_cpu'] );
		if( isset( $arrValues['os_name'] ) && $boolDirectSet ) $this->set( 'm_strOsName', trim( $arrValues['os_name'] ) ); elseif( isset( $arrValues['os_name'] ) ) $this->setOsName( $arrValues['os_name'] );
		if( isset( $arrValues['os_version'] ) && $boolDirectSet ) $this->set( 'm_strOsVersion', trim( $arrValues['os_version'] ) ); elseif( isset( $arrValues['os_version'] ) ) $this->setOsVersion( $arrValues['os_version'] );
		if( isset( $arrValues['memory_size'] ) && $boolDirectSet ) $this->set( 'm_intMemorySize', trim( $arrValues['memory_size'] ) ); elseif( isset( $arrValues['memory_size'] ) ) $this->setMemorySize( $arrValues['memory_size'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrimary', trim( stripcslashes( $arrValues['is_primary'] ) ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_primary'] ) : $arrValues['is_primary'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallServerTypeId( $intCallServerTypeId ) {
		$this->set( 'm_intCallServerTypeId', CStrings::strToIntDef( $intCallServerTypeId, NULL, false ) );
	}

	public function getCallServerTypeId() {
		return $this->m_intCallServerTypeId;
	}

	public function sqlCallServerTypeId() {
		return ( true == isset( $this->m_intCallServerTypeId ) ) ? ( string ) $this->m_intCallServerTypeId : 'NULL';
	}

	public function setServerName( $strServerName ) {
		$this->set( 'm_strServerName', CStrings::strTrimDef( $strServerName, 100, NULL, true ) );
	}

	public function getServerName() {
		return $this->m_strServerName;
	}

	public function sqlServerName() {
		return ( true == isset( $this->m_strServerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServerName ) : '\'' . addslashes( $this->m_strServerName ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 40, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 1000, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUsername ) : '\'' . addslashes( $this->m_strUsername ) . '\'' ) : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 1000, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPassword ) : '\'' . addslashes( $this->m_strPassword ) . '\'' ) : 'NULL';
	}

	public function setDnsName( $strDnsName ) {
		$this->set( 'm_strDnsName', CStrings::strTrimDef( $strDnsName, 100, NULL, true ) );
	}

	public function getDnsName() {
		return $this->m_strDnsName;
	}

	public function sqlDnsName() {
		return ( true == isset( $this->m_strDnsName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDnsName ) : '\'' . addslashes( $this->m_strDnsName ) . '\'' ) : 'NULL';
	}

	public function setTotalCpu( $intTotalCpu ) {
		$this->set( 'm_intTotalCpu', CStrings::strToIntDef( $intTotalCpu, NULL, false ) );
	}

	public function getTotalCpu() {
		return $this->m_intTotalCpu;
	}

	public function sqlTotalCpu() {
		return ( true == isset( $this->m_intTotalCpu ) ) ? ( string ) $this->m_intTotalCpu : 'NULL';
	}

	public function setOsName( $strOsName ) {
		$this->set( 'm_strOsName', CStrings::strTrimDef( $strOsName, 250, NULL, true ) );
	}

	public function getOsName() {
		return $this->m_strOsName;
	}

	public function sqlOsName() {
		return ( true == isset( $this->m_strOsName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOsName ) : '\'' . addslashes( $this->m_strOsName ) . '\'' ) : 'NULL';
	}

	public function setOsVersion( $strOsVersion ) {
		$this->set( 'm_strOsVersion', CStrings::strTrimDef( $strOsVersion, 250, NULL, true ) );
	}

	public function getOsVersion() {
		return $this->m_strOsVersion;
	}

	public function sqlOsVersion() {
		return ( true == isset( $this->m_strOsVersion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strOsVersion ) : '\'' . addslashes( $this->m_strOsVersion ) . '\'' ) : 'NULL';
	}

	public function setMemorySize( $intMemorySize ) {
		$this->set( 'm_intMemorySize', CStrings::strToIntDef( $intMemorySize, NULL, false ) );
	}

	public function getMemorySize() {
		return $this->m_intMemorySize;
	}

	public function sqlMemorySize() {
		return ( true == isset( $this->m_intMemorySize ) ) ? ( string ) $this->m_intMemorySize : 'NULL';
	}

	public function setIsPrimary( $boolIsPrimary ) {
		$this->set( 'm_boolIsPrimary', CStrings::strToBool( $boolIsPrimary ) );
	}

	public function getIsPrimary() {
		return $this->m_boolIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_boolIsPrimary ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrimary ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_server_type_id, server_name, ip_address, username, password, dns_name, total_cpu, os_name, os_version, memory_size, is_primary, is_active, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallServerTypeId() . ', ' .
						$this->sqlServerName() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPassword() . ', ' .
						$this->sqlDnsName() . ', ' .
						$this->sqlTotalCpu() . ', ' .
						$this->sqlOsName() . ', ' .
						$this->sqlOsVersion() . ', ' .
						$this->sqlMemorySize() . ', ' .
						$this->sqlIsPrimary() . ', ' .
						$this->sqlIsActive() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_server_type_id = ' . $this->sqlCallServerTypeId(). ',' ; } elseif( true == array_key_exists( 'CallServerTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_server_type_id = ' . $this->sqlCallServerTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server_name = ' . $this->sqlServerName(). ',' ; } elseif( true == array_key_exists( 'ServerName', $this->getChangedColumns() ) ) { $strSql .= ' server_name = ' . $this->sqlServerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword(). ',' ; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_name = ' . $this->sqlDnsName(). ',' ; } elseif( true == array_key_exists( 'DnsName', $this->getChangedColumns() ) ) { $strSql .= ' dns_name = ' . $this->sqlDnsName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_cpu = ' . $this->sqlTotalCpu(). ',' ; } elseif( true == array_key_exists( 'TotalCpu', $this->getChangedColumns() ) ) { $strSql .= ' total_cpu = ' . $this->sqlTotalCpu() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' os_name = ' . $this->sqlOsName(). ',' ; } elseif( true == array_key_exists( 'OsName', $this->getChangedColumns() ) ) { $strSql .= ' os_name = ' . $this->sqlOsName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' os_version = ' . $this->sqlOsVersion(). ',' ; } elseif( true == array_key_exists( 'OsVersion', $this->getChangedColumns() ) ) { $strSql .= ' os_version = ' . $this->sqlOsVersion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memory_size = ' . $this->sqlMemorySize(). ',' ; } elseif( true == array_key_exists( 'MemorySize', $this->getChangedColumns() ) ) { $strSql .= ' memory_size = ' . $this->sqlMemorySize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary(). ',' ; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_server_type_id' => $this->getCallServerTypeId(),
			'server_name' => $this->getServerName(),
			'ip_address' => $this->getIpAddress(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'dns_name' => $this->getDnsName(),
			'total_cpu' => $this->getTotalCpu(),
			'os_name' => $this->getOsName(),
			'os_version' => $this->getOsVersion(),
			'memory_size' => $this->getMemorySize(),
			'is_primary' => $this->getIsPrimary(),
			'is_active' => $this->getIsActive(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>