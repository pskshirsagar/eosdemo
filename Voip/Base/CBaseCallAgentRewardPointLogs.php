<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentRewardPointLogs
 * Do not add any new functions to this class.
 */

class CBaseCallAgentRewardPointLogs extends CEosPluralBase {

	/**
	 * @return CCallAgentRewardPointLog[]
	 */
	public static function fetchCallAgentRewardPointLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentRewardPointLog::class, $objDatabase );
	}

	/**
	 * @return CCallAgentRewardPointLog
	 */
	public static function fetchCallAgentRewardPointLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentRewardPointLog::class, $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_reward_point_logs', $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogById( $intId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLog( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLogs( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogsByRewardPointMultiplierId( $intRewardPointMultiplierId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLogs( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE reward_point_multiplier_id = %d', $intRewardPointMultiplierId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogsByCallAgentRedeemedProductId( $intCallAgentRedeemedProductId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLogs( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE call_agent_redeemed_product_id = %d', $intCallAgentRedeemedProductId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogsByRewardPointReferenceTypeId( $intRewardPointReferenceTypeId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLogs( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE reward_point_reference_type_id = %d', $intRewardPointReferenceTypeId ), $objDatabase );
	}

	public static function fetchCallAgentRewardPointLogsByRewardPointReferenceId( $intRewardPointReferenceId, $objDatabase ) {
		return self::fetchCallAgentRewardPointLogs( sprintf( 'SELECT * FROM call_agent_reward_point_logs WHERE reward_point_reference_id = %d', $intRewardPointReferenceId ), $objDatabase );
	}

}
?>