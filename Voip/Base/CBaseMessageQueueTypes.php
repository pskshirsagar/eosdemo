<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CMessageQueueTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageQueueTypes extends CEosPluralBase {

	/**
	 * @return CMessageQueueType[]
	 */
	public static function fetchMessageQueueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageQueueType::class, $objDatabase );
	}

	/**
	 * @return CMessageQueueType
	 */
	public static function fetchMessageQueueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageQueueType::class, $objDatabase );
	}

	public static function fetchMessageQueueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_queue_types', $objDatabase );
	}

	public static function fetchMessageQueueTypeById( $intId, $objDatabase ) {
		return self::fetchMessageQueueType( sprintf( 'SELECT * FROM message_queue_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>