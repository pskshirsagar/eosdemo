<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallIvrMenuActions
 * Do not add any new functions to this class.
 */

class CBaseCallIvrMenuActions extends CEosPluralBase {

	/**
	 * @return CCallIvrMenuAction[]
	 */
	public static function fetchCallIvrMenuActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallIvrMenuAction', $objDatabase );
	}

	/**
	 * @return CCallIvrMenuAction
	 */
	public static function fetchCallIvrMenuAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallIvrMenuAction', $objDatabase );
	}

	public static function fetchCallIvrMenuActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_ivr_menu_actions', $objDatabase );
	}

	public static function fetchCallIvrMenuActionById( $intId, $objDatabase ) {
		return self::fetchCallIvrMenuAction( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallIvrMenuActionsByCid( $intCid, $objDatabase ) {
		return self::fetchCallIvrMenuActions( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallIvrMenuActionsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallIvrMenuActions( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallIvrMenuActionsByIvrMenuActionId( $intIvrMenuActionId, $objDatabase ) {
		return self::fetchCallIvrMenuActions( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE ivr_menu_action_id = %d', ( int ) $intIvrMenuActionId ), $objDatabase );
	}

	public static function fetchCallIvrMenuActionsByIvrMenuId( $intIvrMenuId, $objDatabase ) {
		return self::fetchCallIvrMenuActions( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE ivr_menu_id = %d', ( int ) $intIvrMenuId ), $objDatabase );
	}

	public static function fetchCallIvrMenuActionsByIvrActionId( $intIvrActionId, $objDatabase ) {
		return self::fetchCallIvrMenuActions( sprintf( 'SELECT * FROM call_ivr_menu_actions WHERE ivr_action_id = %d', ( int ) $intIvrActionId ), $objDatabase );
	}

}
?>