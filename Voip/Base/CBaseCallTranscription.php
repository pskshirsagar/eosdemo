<?php

class CBaseCallTranscription extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_transcriptions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallId;
	protected $m_strLeftChannel;
	protected $m_strRightChannel;
	protected $m_boolIsLeadCall;
	protected $m_fltCallAnalysisScore;
	protected $m_boolIsLeadCallProbability;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSentiments;
	protected $m_jsonSentiments;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLeadCall = false;
		$this->m_boolIsLeadCallProbability = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['left_channel'] ) && $boolDirectSet ) $this->set( 'm_strLeftChannel', trim( stripcslashes( $arrValues['left_channel'] ) ) ); elseif( isset( $arrValues['left_channel'] ) ) $this->setLeftChannel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['left_channel'] ) : $arrValues['left_channel'] );
		if( isset( $arrValues['right_channel'] ) && $boolDirectSet ) $this->set( 'm_strRightChannel', trim( stripcslashes( $arrValues['right_channel'] ) ) ); elseif( isset( $arrValues['right_channel'] ) ) $this->setRightChannel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['right_channel'] ) : $arrValues['right_channel'] );
		if( isset( $arrValues['is_lead_call'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeadCall', trim( stripcslashes( $arrValues['is_lead_call'] ) ) ); elseif( isset( $arrValues['is_lead_call'] ) ) $this->setIsLeadCall( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lead_call'] ) : $arrValues['is_lead_call'] );
		if( isset( $arrValues['call_analysis_score'] ) && $boolDirectSet ) $this->set( 'm_fltCallAnalysisScore', trim( $arrValues['call_analysis_score'] ) ); elseif( isset( $arrValues['call_analysis_score'] ) ) $this->setCallAnalysisScore( $arrValues['call_analysis_score'] );
		if( isset( $arrValues['is_lead_call_probability'] ) && $boolDirectSet ) $this->set( 'm_boolIsLeadCallProbability', trim( stripcslashes( $arrValues['is_lead_call_probability'] ) ) ); elseif( isset( $arrValues['is_lead_call_probability'] ) ) $this->setIsLeadCallProbability( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_lead_call_probability'] ) : $arrValues['is_lead_call_probability'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['sentiments'] ) ) $this->set( 'm_strSentiments', trim( $arrValues['sentiments'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setLeftChannel( $strLeftChannel ) {
		$this->set( 'm_strLeftChannel', CStrings::strTrimDef( $strLeftChannel, -1, NULL, true ) );
	}

	public function getLeftChannel() {
		return $this->m_strLeftChannel;
	}

	public function sqlLeftChannel() {
		return ( true == isset( $this->m_strLeftChannel ) ) ? '\'' . addslashes( $this->m_strLeftChannel ) . '\'' : 'NULL';
	}

	public function setRightChannel( $strRightChannel ) {
		$this->set( 'm_strRightChannel', CStrings::strTrimDef( $strRightChannel, -1, NULL, true ) );
	}

	public function getRightChannel() {
		return $this->m_strRightChannel;
	}

	public function sqlRightChannel() {
		return ( true == isset( $this->m_strRightChannel ) ) ? '\'' . addslashes( $this->m_strRightChannel ) . '\'' : 'NULL';
	}

	public function setIsLeadCall( $boolIsLeadCall ) {
		$this->set( 'm_boolIsLeadCall', CStrings::strToBool( $boolIsLeadCall ) );
	}

	public function getIsLeadCall() {
		return $this->m_boolIsLeadCall;
	}

	public function sqlIsLeadCall() {
		return ( true == isset( $this->m_boolIsLeadCall ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeadCall ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCallAnalysisScore( $fltCallAnalysisScore ) {
		$this->set( 'm_fltCallAnalysisScore', CStrings::strToFloatDef( $fltCallAnalysisScore, NULL, false, 4 ) );
	}

	public function getCallAnalysisScore() {
		return $this->m_fltCallAnalysisScore;
	}

	public function sqlCallAnalysisScore() {
		return ( true == isset( $this->m_fltCallAnalysisScore ) ) ? ( string ) $this->m_fltCallAnalysisScore : 'NULL';
	}

	public function setIsLeadCallProbability( $boolIsLeadCallProbability ) {
		$this->set( 'm_boolIsLeadCallProbability', CStrings::strToBool( $boolIsLeadCallProbability ) );
	}

	public function getIsLeadCallProbability() {
		return $this->m_boolIsLeadCallProbability;
	}

	public function sqlIsLeadCallProbability() {
		return ( true == isset( $this->m_boolIsLeadCallProbability ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLeadCallProbability ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSentiments( $jsonSentiments ) {
		if( true == valObj( $jsonSentiments, 'stdClass' ) ) {
			$this->set( 'm_jsonSentiments', $jsonSentiments );
		} elseif( true == valJsonString( $jsonSentiments ) ) {
			$this->set( 'm_jsonSentiments', CStrings::strToJson( $jsonSentiments ) );
		} else {
			$this->set( 'm_jsonSentiments', NULL ); 
		}
		unset( $this->m_strSentiments );
	}

	public function getSentiments() {
		if( true == isset( $this->m_strSentiments ) ) {
			$this->m_jsonSentiments = CStrings::strToJson( $this->m_strSentiments );
			unset( $this->m_strSentiments );
		}
		return $this->m_jsonSentiments;
	}

	public function sqlSentiments() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSentiments() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getSentiments() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, call_id, left_channel, right_channel, is_lead_call, call_analysis_score, is_lead_call_probability, details, updated_by, updated_on, created_by, created_on, sentiments )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCallId() . ', ' .
						$this->sqlLeftChannel() . ', ' .
						$this->sqlRightChannel() . ', ' .
						$this->sqlIsLeadCall() . ', ' .
						$this->sqlCallAnalysisScore() . ', ' .
						$this->sqlIsLeadCallProbability() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSentiments() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' left_channel = ' . $this->sqlLeftChannel(). ',' ; } elseif( true == array_key_exists( 'LeftChannel', $this->getChangedColumns() ) ) { $strSql .= ' left_channel = ' . $this->sqlLeftChannel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' right_channel = ' . $this->sqlRightChannel(). ',' ; } elseif( true == array_key_exists( 'RightChannel', $this->getChangedColumns() ) ) { $strSql .= ' right_channel = ' . $this->sqlRightChannel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lead_call = ' . $this->sqlIsLeadCall(). ',' ; } elseif( true == array_key_exists( 'IsLeadCall', $this->getChangedColumns() ) ) { $strSql .= ' is_lead_call = ' . $this->sqlIsLeadCall() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_analysis_score = ' . $this->sqlCallAnalysisScore(). ',' ; } elseif( true == array_key_exists( 'CallAnalysisScore', $this->getChangedColumns() ) ) { $strSql .= ' call_analysis_score = ' . $this->sqlCallAnalysisScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_lead_call_probability = ' . $this->sqlIsLeadCallProbability(). ',' ; } elseif( true == array_key_exists( 'IsLeadCallProbability', $this->getChangedColumns() ) ) { $strSql .= ' is_lead_call_probability = ' . $this->sqlIsLeadCallProbability() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sentiments = ' . $this->sqlSentiments(). ',' ; } elseif( true == array_key_exists( 'Sentiments', $this->getChangedColumns() ) ) { $strSql .= ' sentiments = ' . $this->sqlSentiments() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_id' => $this->getCallId(),
			'left_channel' => $this->getLeftChannel(),
			'right_channel' => $this->getRightChannel(),
			'is_lead_call' => $this->getIsLeadCall(),
			'call_analysis_score' => $this->getCallAnalysisScore(),
			'is_lead_call_probability' => $this->getIsLeadCallProbability(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'sentiments' => $this->getSentiments()
		);
	}

}
?>