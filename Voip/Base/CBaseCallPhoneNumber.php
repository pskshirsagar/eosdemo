<?php

class CBaseCallPhoneNumber extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_phone_numbers';

	protected $m_intId;
	protected $m_intCallServiceProviderId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallTypeId;
	protected $m_intCallForwardPreferenceId;
	protected $m_intCallQueueId;
	protected $m_intEmployeeId;
	protected $m_intPhoneExtensionId;
	protected $m_intDialplanId;
	protected $m_intIvrId;
	protected $m_strPhoneNumber;
	protected $m_strCallForwardPhoneNumber;
	protected $m_intBlock;
	protected $m_fltMonthlyFee;
	protected $m_fltPerMinuteFee;
	protected $m_fltMonthlyServiceFee;
	protected $m_fltPerMinuteServiceFee;
	protected $m_boolIsTollFree;
	protected $m_boolIsVerified;
	protected $m_boolIsSipTest;
	protected $m_strRequestDatetime;
	protected $m_strVerifiedOn;
	protected $m_strReleasedOn;
	protected $m_intReleasedBy;
	protected $m_strSuspendedOn;
	protected $m_intSuspendedBy;
	protected $m_strLastPostedOn;
	protected $m_intPortedBy;
	protected $m_strPortedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsSmsEnabled;
	protected $m_boolIsCallEnabled;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCountryCode;

	public function __construct() {
		parent::__construct();

		$this->m_intCallServiceProviderId = '1';
		$this->m_intCallForwardPreferenceId = '1';
		$this->m_boolIsTollFree = false;
		$this->m_boolIsVerified = false;
		$this->m_boolIsSipTest = false;
		$this->m_boolIsSmsEnabled = false;
		$this->m_boolIsCallEnabled = false;
		$this->m_strCountryCode = 'US';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_service_provider_id'] ) && $boolDirectSet ) $this->set( 'm_intCallServiceProviderId', trim( $arrValues['call_service_provider_id'] ) ); elseif( isset( $arrValues['call_service_provider_id'] ) ) $this->setCallServiceProviderId( $arrValues['call_service_provider_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallTypeId', trim( $arrValues['call_type_id'] ) ); elseif( isset( $arrValues['call_type_id'] ) ) $this->setCallTypeId( $arrValues['call_type_id'] );
		if( isset( $arrValues['call_forward_preference_id'] ) && $boolDirectSet ) $this->set( 'm_intCallForwardPreferenceId', trim( $arrValues['call_forward_preference_id'] ) ); elseif( isset( $arrValues['call_forward_preference_id'] ) ) $this->setCallForwardPreferenceId( $arrValues['call_forward_preference_id'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneExtensionId', trim( $arrValues['phone_extension_id'] ) ); elseif( isset( $arrValues['phone_extension_id'] ) ) $this->setPhoneExtensionId( $arrValues['phone_extension_id'] );
		if( isset( $arrValues['dialplan_id'] ) && $boolDirectSet ) $this->set( 'm_intDialplanId', trim( $arrValues['dialplan_id'] ) ); elseif( isset( $arrValues['dialplan_id'] ) ) $this->setDialplanId( $arrValues['dialplan_id'] );
		if( isset( $arrValues['ivr_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrId', trim( $arrValues['ivr_id'] ) ); elseif( isset( $arrValues['ivr_id'] ) ) $this->setIvrId( $arrValues['ivr_id'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['call_forward_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strCallForwardPhoneNumber', trim( stripcslashes( $arrValues['call_forward_phone_number'] ) ) ); elseif( isset( $arrValues['call_forward_phone_number'] ) ) $this->setCallForwardPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_forward_phone_number'] ) : $arrValues['call_forward_phone_number'] );
		if( isset( $arrValues['block'] ) && $boolDirectSet ) $this->set( 'm_intBlock', trim( $arrValues['block'] ) ); elseif( isset( $arrValues['block'] ) ) $this->setBlock( $arrValues['block'] );
		if( isset( $arrValues['monthly_fee'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyFee', trim( $arrValues['monthly_fee'] ) ); elseif( isset( $arrValues['monthly_fee'] ) ) $this->setMonthlyFee( $arrValues['monthly_fee'] );
		if( isset( $arrValues['per_minute_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPerMinuteFee', trim( $arrValues['per_minute_fee'] ) ); elseif( isset( $arrValues['per_minute_fee'] ) ) $this->setPerMinuteFee( $arrValues['per_minute_fee'] );
		if( isset( $arrValues['monthly_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyServiceFee', trim( $arrValues['monthly_service_fee'] ) ); elseif( isset( $arrValues['monthly_service_fee'] ) ) $this->setMonthlyServiceFee( $arrValues['monthly_service_fee'] );
		if( isset( $arrValues['per_minute_service_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPerMinuteServiceFee', trim( $arrValues['per_minute_service_fee'] ) ); elseif( isset( $arrValues['per_minute_service_fee'] ) ) $this->setPerMinuteServiceFee( $arrValues['per_minute_service_fee'] );
		if( isset( $arrValues['is_toll_free'] ) && $boolDirectSet ) $this->set( 'm_boolIsTollFree', trim( stripcslashes( $arrValues['is_toll_free'] ) ) ); elseif( isset( $arrValues['is_toll_free'] ) ) $this->setIsTollFree( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_toll_free'] ) : $arrValues['is_toll_free'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_boolIsVerified', trim( stripcslashes( $arrValues['is_verified'] ) ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_verified'] ) : $arrValues['is_verified'] );
		if( isset( $arrValues['is_sip_test'] ) && $boolDirectSet ) $this->set( 'm_boolIsSipTest', trim( stripcslashes( $arrValues['is_sip_test'] ) ) ); elseif( isset( $arrValues['is_sip_test'] ) ) $this->setIsSipTest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_sip_test'] ) : $arrValues['is_sip_test'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['released_on'] ) && $boolDirectSet ) $this->set( 'm_strReleasedOn', trim( $arrValues['released_on'] ) ); elseif( isset( $arrValues['released_on'] ) ) $this->setReleasedOn( $arrValues['released_on'] );
		if( isset( $arrValues['released_by'] ) && $boolDirectSet ) $this->set( 'm_intReleasedBy', trim( $arrValues['released_by'] ) ); elseif( isset( $arrValues['released_by'] ) ) $this->setReleasedBy( $arrValues['released_by'] );
		if( isset( $arrValues['suspended_on'] ) && $boolDirectSet ) $this->set( 'm_strSuspendedOn', trim( $arrValues['suspended_on'] ) ); elseif( isset( $arrValues['suspended_on'] ) ) $this->setSuspendedOn( $arrValues['suspended_on'] );
		if( isset( $arrValues['suspended_by'] ) && $boolDirectSet ) $this->set( 'm_intSuspendedBy', trim( $arrValues['suspended_by'] ) ); elseif( isset( $arrValues['suspended_by'] ) ) $this->setSuspendedBy( $arrValues['suspended_by'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['ported_by'] ) && $boolDirectSet ) $this->set( 'm_intPortedBy', trim( $arrValues['ported_by'] ) ); elseif( isset( $arrValues['ported_by'] ) ) $this->setPortedBy( $arrValues['ported_by'] );
		if( isset( $arrValues['ported_on'] ) && $boolDirectSet ) $this->set( 'm_strPortedOn', trim( $arrValues['ported_on'] ) ); elseif( isset( $arrValues['ported_on'] ) ) $this->setPortedOn( $arrValues['ported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_sms_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsSmsEnabled', trim( stripcslashes( $arrValues['is_sms_enabled'] ) ) ); elseif( isset( $arrValues['is_sms_enabled'] ) ) $this->setIsSmsEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_sms_enabled'] ) : $arrValues['is_sms_enabled'] );
		if( isset( $arrValues['is_call_enabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsCallEnabled', trim( stripcslashes( $arrValues['is_call_enabled'] ) ) ); elseif( isset( $arrValues['is_call_enabled'] ) ) $this->setIsCallEnabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_call_enabled'] ) : $arrValues['is_call_enabled'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallServiceProviderId( $intCallServiceProviderId ) {
		$this->set( 'm_intCallServiceProviderId', CStrings::strToIntDef( $intCallServiceProviderId, NULL, false ) );
	}

	public function getCallServiceProviderId() {
		return $this->m_intCallServiceProviderId;
	}

	public function sqlCallServiceProviderId() {
		return ( true == isset( $this->m_intCallServiceProviderId ) ) ? ( string ) $this->m_intCallServiceProviderId : '1';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallTypeId( $intCallTypeId ) {
		$this->set( 'm_intCallTypeId', CStrings::strToIntDef( $intCallTypeId, NULL, false ) );
	}

	public function getCallTypeId() {
		return $this->m_intCallTypeId;
	}

	public function sqlCallTypeId() {
		return ( true == isset( $this->m_intCallTypeId ) ) ? ( string ) $this->m_intCallTypeId : 'NULL';
	}

	public function setCallForwardPreferenceId( $intCallForwardPreferenceId ) {
		$this->set( 'm_intCallForwardPreferenceId', CStrings::strToIntDef( $intCallForwardPreferenceId, NULL, false ) );
	}

	public function getCallForwardPreferenceId() {
		return $this->m_intCallForwardPreferenceId;
	}

	public function sqlCallForwardPreferenceId() {
		return ( true == isset( $this->m_intCallForwardPreferenceId ) ) ? ( string ) $this->m_intCallForwardPreferenceId : '1';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPhoneExtensionId( $intPhoneExtensionId ) {
		$this->set( 'm_intPhoneExtensionId', CStrings::strToIntDef( $intPhoneExtensionId, NULL, false ) );
	}

	public function getPhoneExtensionId() {
		return $this->m_intPhoneExtensionId;
	}

	public function sqlPhoneExtensionId() {
		return ( true == isset( $this->m_intPhoneExtensionId ) ) ? ( string ) $this->m_intPhoneExtensionId : 'NULL';
	}

	public function setDialplanId( $intDialplanId ) {
		$this->set( 'm_intDialplanId', CStrings::strToIntDef( $intDialplanId, NULL, false ) );
	}

	public function getDialplanId() {
		return $this->m_intDialplanId;
	}

	public function sqlDialplanId() {
		return ( true == isset( $this->m_intDialplanId ) ) ? ( string ) $this->m_intDialplanId : 'NULL';
	}

	public function setIvrId( $intIvrId ) {
		$this->set( 'm_intIvrId', CStrings::strToIntDef( $intIvrId, NULL, false ) );
	}

	public function getIvrId() {
		return $this->m_intIvrId;
	}

	public function sqlIvrId() {
		return ( true == isset( $this->m_intIvrId ) ) ? ( string ) $this->m_intIvrId : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setCallForwardPhoneNumber( $strCallForwardPhoneNumber ) {
		$this->set( 'm_strCallForwardPhoneNumber', CStrings::strTrimDef( $strCallForwardPhoneNumber, 40, NULL, true ) );
	}

	public function getCallForwardPhoneNumber() {
		return $this->m_strCallForwardPhoneNumber;
	}

	public function sqlCallForwardPhoneNumber() {
		return ( true == isset( $this->m_strCallForwardPhoneNumber ) ) ? '\'' . addslashes( $this->m_strCallForwardPhoneNumber ) . '\'' : 'NULL';
	}

	public function setBlock( $intBlock ) {
		$this->set( 'm_intBlock', CStrings::strToIntDef( $intBlock, NULL, false ) );
	}

	public function getBlock() {
		return $this->m_intBlock;
	}

	public function sqlBlock() {
		return ( true == isset( $this->m_intBlock ) ) ? ( string ) $this->m_intBlock : 'NULL';
	}

	public function setMonthlyFee( $fltMonthlyFee ) {
		$this->set( 'm_fltMonthlyFee', CStrings::strToFloatDef( $fltMonthlyFee, NULL, false, 5 ) );
	}

	public function getMonthlyFee() {
		return $this->m_fltMonthlyFee;
	}

	public function sqlMonthlyFee() {
		return ( true == isset( $this->m_fltMonthlyFee ) ) ? ( string ) $this->m_fltMonthlyFee : 'NULL';
	}

	public function setPerMinuteFee( $fltPerMinuteFee ) {
		$this->set( 'm_fltPerMinuteFee', CStrings::strToFloatDef( $fltPerMinuteFee, NULL, false, 5 ) );
	}

	public function getPerMinuteFee() {
		return $this->m_fltPerMinuteFee;
	}

	public function sqlPerMinuteFee() {
		return ( true == isset( $this->m_fltPerMinuteFee ) ) ? ( string ) $this->m_fltPerMinuteFee : 'NULL';
	}

	public function setMonthlyServiceFee( $fltMonthlyServiceFee ) {
		$this->set( 'm_fltMonthlyServiceFee', CStrings::strToFloatDef( $fltMonthlyServiceFee, NULL, false, 5 ) );
	}

	public function getMonthlyServiceFee() {
		return $this->m_fltMonthlyServiceFee;
	}

	public function sqlMonthlyServiceFee() {
		return ( true == isset( $this->m_fltMonthlyServiceFee ) ) ? ( string ) $this->m_fltMonthlyServiceFee : 'NULL';
	}

	public function setPerMinuteServiceFee( $fltPerMinuteServiceFee ) {
		$this->set( 'm_fltPerMinuteServiceFee', CStrings::strToFloatDef( $fltPerMinuteServiceFee, NULL, false, 5 ) );
	}

	public function getPerMinuteServiceFee() {
		return $this->m_fltPerMinuteServiceFee;
	}

	public function sqlPerMinuteServiceFee() {
		return ( true == isset( $this->m_fltPerMinuteServiceFee ) ) ? ( string ) $this->m_fltPerMinuteServiceFee : 'NULL';
	}

	public function setIsTollFree( $boolIsTollFree ) {
		$this->set( 'm_boolIsTollFree', CStrings::strToBool( $boolIsTollFree ) );
	}

	public function getIsTollFree() {
		return $this->m_boolIsTollFree;
	}

	public function sqlIsTollFree() {
		return ( true == isset( $this->m_boolIsTollFree ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTollFree ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsVerified( $boolIsVerified ) {
		$this->set( 'm_boolIsVerified', CStrings::strToBool( $boolIsVerified ) );
	}

	public function getIsVerified() {
		return $this->m_boolIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_boolIsVerified ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVerified ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsSipTest( $boolIsSipTest ) {
		$this->set( 'm_boolIsSipTest', CStrings::strToBool( $boolIsSipTest ) );
	}

	public function getIsSipTest() {
		return $this->m_boolIsSipTest;
	}

	public function sqlIsSipTest() {
		return ( true == isset( $this->m_boolIsSipTest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSipTest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setReleasedOn( $strReleasedOn ) {
		$this->set( 'm_strReleasedOn', CStrings::strTrimDef( $strReleasedOn, -1, NULL, true ) );
	}

	public function getReleasedOn() {
		return $this->m_strReleasedOn;
	}

	public function sqlReleasedOn() {
		return ( true == isset( $this->m_strReleasedOn ) ) ? '\'' . $this->m_strReleasedOn . '\'' : 'NULL';
	}

	public function setReleasedBy( $intReleasedBy ) {
		$this->set( 'm_intReleasedBy', CStrings::strToIntDef( $intReleasedBy, NULL, false ) );
	}

	public function getReleasedBy() {
		return $this->m_intReleasedBy;
	}

	public function sqlReleasedBy() {
		return ( true == isset( $this->m_intReleasedBy ) ) ? ( string ) $this->m_intReleasedBy : 'NULL';
	}

	public function setSuspendedOn( $strSuspendedOn ) {
		$this->set( 'm_strSuspendedOn', CStrings::strTrimDef( $strSuspendedOn, -1, NULL, true ) );
	}

	public function getSuspendedOn() {
		return $this->m_strSuspendedOn;
	}

	public function sqlSuspendedOn() {
		return ( true == isset( $this->m_strSuspendedOn ) ) ? '\'' . $this->m_strSuspendedOn . '\'' : 'NULL';
	}

	public function setSuspendedBy( $intSuspendedBy ) {
		$this->set( 'm_intSuspendedBy', CStrings::strToIntDef( $intSuspendedBy, NULL, false ) );
	}

	public function getSuspendedBy() {
		return $this->m_intSuspendedBy;
	}

	public function sqlSuspendedBy() {
		return ( true == isset( $this->m_intSuspendedBy ) ) ? ( string ) $this->m_intSuspendedBy : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setPortedBy( $intPortedBy ) {
		$this->set( 'm_intPortedBy', CStrings::strToIntDef( $intPortedBy, NULL, false ) );
	}

	public function getPortedBy() {
		return $this->m_intPortedBy;
	}

	public function sqlPortedBy() {
		return ( true == isset( $this->m_intPortedBy ) ) ? ( string ) $this->m_intPortedBy : 'NULL';
	}

	public function setPortedOn( $strPortedOn ) {
		$this->set( 'm_strPortedOn', CStrings::strTrimDef( $strPortedOn, -1, NULL, true ) );
	}

	public function getPortedOn() {
		return $this->m_strPortedOn;
	}

	public function sqlPortedOn() {
		return ( true == isset( $this->m_strPortedOn ) ) ? '\'' . $this->m_strPortedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsSmsEnabled( $boolIsSmsEnabled ) {
		$this->set( 'm_boolIsSmsEnabled', CStrings::strToBool( $boolIsSmsEnabled ) );
	}

	public function getIsSmsEnabled() {
		return $this->m_boolIsSmsEnabled;
	}

	public function sqlIsSmsEnabled() {
		return ( true == isset( $this->m_boolIsSmsEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSmsEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCallEnabled( $boolIsCallEnabled ) {
		$this->set( 'm_boolIsCallEnabled', CStrings::strToBool( $boolIsCallEnabled ) );
	}

	public function getIsCallEnabled() {
		return $this->m_boolIsCallEnabled;
	}

	public function sqlIsCallEnabled() {
		return ( true == isset( $this->m_boolIsCallEnabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCallEnabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : '\'US\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_service_provider_id, cid, property_id, call_type_id, call_forward_preference_id, call_queue_id, employee_id, phone_extension_id, dialplan_id, ivr_id, phone_number, call_forward_phone_number, block, monthly_fee, per_minute_fee, monthly_service_fee, per_minute_service_fee, is_toll_free, is_verified, is_sip_test, request_datetime, verified_on, released_on, released_by, suspended_on, suspended_by, last_posted_on, ported_by, ported_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_sms_enabled, is_call_enabled, details, country_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallServiceProviderId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCallTypeId() . ', ' .
						$this->sqlCallForwardPreferenceId() . ', ' .
						$this->sqlCallQueueId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlPhoneExtensionId() . ', ' .
						$this->sqlDialplanId() . ', ' .
						$this->sqlIvrId() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlCallForwardPhoneNumber() . ', ' .
						$this->sqlBlock() . ', ' .
						$this->sqlMonthlyFee() . ', ' .
						$this->sqlPerMinuteFee() . ', ' .
						$this->sqlMonthlyServiceFee() . ', ' .
						$this->sqlPerMinuteServiceFee() . ', ' .
						$this->sqlIsTollFree() . ', ' .
						$this->sqlIsVerified() . ', ' .
						$this->sqlIsSipTest() . ', ' .
						$this->sqlRequestDatetime() . ', ' .
						$this->sqlVerifiedOn() . ', ' .
						$this->sqlReleasedOn() . ', ' .
						$this->sqlReleasedBy() . ', ' .
						$this->sqlSuspendedOn() . ', ' .
						$this->sqlSuspendedBy() . ', ' .
						$this->sqlLastPostedOn() . ', ' .
						$this->sqlPortedBy() . ', ' .
						$this->sqlPortedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsSmsEnabled() . ', ' .
						$this->sqlIsCallEnabled() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCountryCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_service_provider_id = ' . $this->sqlCallServiceProviderId(). ',' ; } elseif( true == array_key_exists( 'CallServiceProviderId', $this->getChangedColumns() ) ) { $strSql .= ' call_service_provider_id = ' . $this->sqlCallServiceProviderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId(). ',' ; } elseif( true == array_key_exists( 'CallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_forward_preference_id = ' . $this->sqlCallForwardPreferenceId(). ',' ; } elseif( true == array_key_exists( 'CallForwardPreferenceId', $this->getChangedColumns() ) ) { $strSql .= ' call_forward_preference_id = ' . $this->sqlCallForwardPreferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId(). ',' ; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_extension_id = ' . $this->sqlPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'PhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' phone_extension_id = ' . $this->sqlPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dialplan_id = ' . $this->sqlDialplanId(). ',' ; } elseif( true == array_key_exists( 'DialplanId', $this->getChangedColumns() ) ) { $strSql .= ' dialplan_id = ' . $this->sqlDialplanId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId(). ',' ; } elseif( true == array_key_exists( 'IvrId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_forward_phone_number = ' . $this->sqlCallForwardPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'CallForwardPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' call_forward_phone_number = ' . $this->sqlCallForwardPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block = ' . $this->sqlBlock(). ',' ; } elseif( true == array_key_exists( 'Block', $this->getChangedColumns() ) ) { $strSql .= ' block = ' . $this->sqlBlock() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_fee = ' . $this->sqlMonthlyFee(). ',' ; } elseif( true == array_key_exists( 'MonthlyFee', $this->getChangedColumns() ) ) { $strSql .= ' monthly_fee = ' . $this->sqlMonthlyFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_minute_fee = ' . $this->sqlPerMinuteFee(). ',' ; } elseif( true == array_key_exists( 'PerMinuteFee', $this->getChangedColumns() ) ) { $strSql .= ' per_minute_fee = ' . $this->sqlPerMinuteFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_service_fee = ' . $this->sqlMonthlyServiceFee(). ',' ; } elseif( true == array_key_exists( 'MonthlyServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' monthly_service_fee = ' . $this->sqlMonthlyServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_minute_service_fee = ' . $this->sqlPerMinuteServiceFee(). ',' ; } elseif( true == array_key_exists( 'PerMinuteServiceFee', $this->getChangedColumns() ) ) { $strSql .= ' per_minute_service_fee = ' . $this->sqlPerMinuteServiceFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_toll_free = ' . $this->sqlIsTollFree(). ',' ; } elseif( true == array_key_exists( 'IsTollFree', $this->getChangedColumns() ) ) { $strSql .= ' is_toll_free = ' . $this->sqlIsTollFree() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified(). ',' ; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sip_test = ' . $this->sqlIsSipTest(). ',' ; } elseif( true == array_key_exists( 'IsSipTest', $this->getChangedColumns() ) ) { $strSql .= ' is_sip_test = ' . $this->sqlIsSipTest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn(). ',' ; } elseif( true == array_key_exists( 'ReleasedOn', $this->getChangedColumns() ) ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy(). ',' ; } elseif( true == array_key_exists( 'ReleasedBy', $this->getChangedColumns() ) ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn(). ',' ; } elseif( true == array_key_exists( 'SuspendedOn', $this->getChangedColumns() ) ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_by = ' . $this->sqlSuspendedBy(). ',' ; } elseif( true == array_key_exists( 'SuspendedBy', $this->getChangedColumns() ) ) { $strSql .= ' suspended_by = ' . $this->sqlSuspendedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ported_by = ' . $this->sqlPortedBy(). ',' ; } elseif( true == array_key_exists( 'PortedBy', $this->getChangedColumns() ) ) { $strSql .= ' ported_by = ' . $this->sqlPortedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ported_on = ' . $this->sqlPortedOn(). ',' ; } elseif( true == array_key_exists( 'PortedOn', $this->getChangedColumns() ) ) { $strSql .= ' ported_on = ' . $this->sqlPortedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sms_enabled = ' . $this->sqlIsSmsEnabled(). ',' ; } elseif( true == array_key_exists( 'IsSmsEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_sms_enabled = ' . $this->sqlIsSmsEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_call_enabled = ' . $this->sqlIsCallEnabled(). ',' ; } elseif( true == array_key_exists( 'IsCallEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_call_enabled = ' . $this->sqlIsCallEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_service_provider_id' => $this->getCallServiceProviderId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_type_id' => $this->getCallTypeId(),
			'call_forward_preference_id' => $this->getCallForwardPreferenceId(),
			'call_queue_id' => $this->getCallQueueId(),
			'employee_id' => $this->getEmployeeId(),
			'phone_extension_id' => $this->getPhoneExtensionId(),
			'dialplan_id' => $this->getDialplanId(),
			'ivr_id' => $this->getIvrId(),
			'phone_number' => $this->getPhoneNumber(),
			'call_forward_phone_number' => $this->getCallForwardPhoneNumber(),
			'block' => $this->getBlock(),
			'monthly_fee' => $this->getMonthlyFee(),
			'per_minute_fee' => $this->getPerMinuteFee(),
			'monthly_service_fee' => $this->getMonthlyServiceFee(),
			'per_minute_service_fee' => $this->getPerMinuteServiceFee(),
			'is_toll_free' => $this->getIsTollFree(),
			'is_verified' => $this->getIsVerified(),
			'is_sip_test' => $this->getIsSipTest(),
			'request_datetime' => $this->getRequestDatetime(),
			'verified_on' => $this->getVerifiedOn(),
			'released_on' => $this->getReleasedOn(),
			'released_by' => $this->getReleasedBy(),
			'suspended_on' => $this->getSuspendedOn(),
			'suspended_by' => $this->getSuspendedBy(),
			'last_posted_on' => $this->getLastPostedOn(),
			'ported_by' => $this->getPortedBy(),
			'ported_on' => $this->getPortedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_sms_enabled' => $this->getIsSmsEnabled(),
			'is_call_enabled' => $this->getIsCallEnabled(),
			'details' => $this->getDetails(),
			'country_code' => $this->getCountryCode()
		);
	}

}
?>