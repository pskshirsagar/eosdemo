<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCallProcesses
 * Do not add any new functions to this class.
 */

class CBaseInboundCallProcesses extends CEosPluralBase {

	/**
	 * @return CInboundCallProcess[]
	 */
	public static function fetchInboundCallProcesses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInboundCallProcess', $objDatabase );
	}

	/**
	 * @return CInboundCallProcess
	 */
	public static function fetchInboundCallProcess( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInboundCallProcess', $objDatabase );
	}

	public static function fetchInboundCallProcessCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'inbound_call_processes', $objDatabase );
	}

	public static function fetchInboundCallProcessById( $intId, $objDatabase ) {
		return self::fetchInboundCallProcess( sprintf( 'SELECT * FROM inbound_call_processes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInboundCallProcessesByInboundCallId( $intInboundCallId, $objDatabase ) {
		return self::fetchInboundCallProcesses( sprintf( 'SELECT * FROM inbound_call_processes WHERE inbound_call_id = %d', ( int ) $intInboundCallId ), $objDatabase );
	}

	public static function fetchInboundCallProcessesByPaymentActivationId( $intPaymentActivationId, $objDatabase ) {
		return self::fetchInboundCallProcesses( sprintf( 'SELECT * FROM inbound_call_processes WHERE payment_activation_id = %d', ( int ) $intPaymentActivationId ), $objDatabase );
	}

	public static function fetchInboundCallProcessesByInboundCallResponseTypeId( $intInboundCallResponseTypeId, $objDatabase ) {
		return self::fetchInboundCallProcesses( sprintf( 'SELECT * FROM inbound_call_processes WHERE inbound_call_response_type_id = %d', ( int ) $intInboundCallResponseTypeId ), $objDatabase );
	}

}
?>