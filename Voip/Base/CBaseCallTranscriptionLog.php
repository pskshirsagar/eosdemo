<?php

class CBaseCallTranscriptionLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_transcription_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallId;
	protected $m_strOperationId;
	protected $m_boolIsFileConverted;
	protected $m_boolIsGpcRequestSent;
	protected $m_boolIsCallTranscribed;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsFileConverted = false;
		$this->m_boolIsGpcRequestSent = false;
		$this->m_boolIsCallTranscribed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['operation_id'] ) && $boolDirectSet ) $this->set( 'm_strOperationId', trim( stripcslashes( $arrValues['operation_id'] ) ) ); elseif( isset( $arrValues['operation_id'] ) ) $this->setOperationId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['operation_id'] ) : $arrValues['operation_id'] );
		if( isset( $arrValues['is_file_converted'] ) && $boolDirectSet ) $this->set( 'm_boolIsFileConverted', trim( stripcslashes( $arrValues['is_file_converted'] ) ) ); elseif( isset( $arrValues['is_file_converted'] ) ) $this->setIsFileConverted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_file_converted'] ) : $arrValues['is_file_converted'] );
		if( isset( $arrValues['is_gpc_request_sent'] ) && $boolDirectSet ) $this->set( 'm_boolIsGpcRequestSent', trim( stripcslashes( $arrValues['is_gpc_request_sent'] ) ) ); elseif( isset( $arrValues['is_gpc_request_sent'] ) ) $this->setIsGpcRequestSent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_gpc_request_sent'] ) : $arrValues['is_gpc_request_sent'] );
		if( isset( $arrValues['is_call_transcribed'] ) && $boolDirectSet ) $this->set( 'm_boolIsCallTranscribed', trim( stripcslashes( $arrValues['is_call_transcribed'] ) ) ); elseif( isset( $arrValues['is_call_transcribed'] ) ) $this->setIsCallTranscribed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_call_transcribed'] ) : $arrValues['is_call_transcribed'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setOperationId( $strOperationId ) {
		$this->set( 'm_strOperationId', CStrings::strTrimDef( $strOperationId, 50, NULL, true ) );
	}

	public function getOperationId() {
		return $this->m_strOperationId;
	}

	public function sqlOperationId() {
		return ( true == isset( $this->m_strOperationId ) ) ? '\'' . addslashes( $this->m_strOperationId ) . '\'' : 'NULL';
	}

	public function setIsFileConverted( $boolIsFileConverted ) {
		$this->set( 'm_boolIsFileConverted', CStrings::strToBool( $boolIsFileConverted ) );
	}

	public function getIsFileConverted() {
		return $this->m_boolIsFileConverted;
	}

	public function sqlIsFileConverted() {
		return ( true == isset( $this->m_boolIsFileConverted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFileConverted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsGpcRequestSent( $boolIsGpcRequestSent ) {
		$this->set( 'm_boolIsGpcRequestSent', CStrings::strToBool( $boolIsGpcRequestSent ) );
	}

	public function getIsGpcRequestSent() {
		return $this->m_boolIsGpcRequestSent;
	}

	public function sqlIsGpcRequestSent() {
		return ( true == isset( $this->m_boolIsGpcRequestSent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsGpcRequestSent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCallTranscribed( $boolIsCallTranscribed ) {
		$this->set( 'm_boolIsCallTranscribed', CStrings::strToBool( $boolIsCallTranscribed ) );
	}

	public function getIsCallTranscribed() {
		return $this->m_boolIsCallTranscribed;
	}

	public function sqlIsCallTranscribed() {
		return ( true == isset( $this->m_boolIsCallTranscribed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCallTranscribed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 240, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, call_id, operation_id, is_file_converted, is_gpc_request_sent, is_call_transcribed, note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlOperationId() . ', ' .
 						$this->sqlIsFileConverted() . ', ' .
 						$this->sqlIsGpcRequestSent() . ', ' .
 						$this->sqlIsCallTranscribed() . ', ' .
 						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' operation_id = ' . $this->sqlOperationId() . ','; } elseif( true == array_key_exists( 'OperationId', $this->getChangedColumns() ) ) { $strSql .= ' operation_id = ' . $this->sqlOperationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_file_converted = ' . $this->sqlIsFileConverted() . ','; } elseif( true == array_key_exists( 'IsFileConverted', $this->getChangedColumns() ) ) { $strSql .= ' is_file_converted = ' . $this->sqlIsFileConverted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_gpc_request_sent = ' . $this->sqlIsGpcRequestSent() . ','; } elseif( true == array_key_exists( 'IsGpcRequestSent', $this->getChangedColumns() ) ) { $strSql .= ' is_gpc_request_sent = ' . $this->sqlIsGpcRequestSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_call_transcribed = ' . $this->sqlIsCallTranscribed() . ','; } elseif( true == array_key_exists( 'IsCallTranscribed', $this->getChangedColumns() ) ) { $strSql .= ' is_call_transcribed = ' . $this->sqlIsCallTranscribed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_id' => $this->getCallId(),
			'operation_id' => $this->getOperationId(),
			'is_file_converted' => $this->getIsFileConverted(),
			'is_gpc_request_sent' => $this->getIsGpcRequestSent(),
			'is_call_transcribed' => $this->getIsCallTranscribed(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>