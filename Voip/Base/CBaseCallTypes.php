<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTypes
 * Do not add any new functions to this class.
 */

class CBaseCallTypes extends CEosPluralBase {

	/**
	 * @return CCallType[]
	 */
	public static function fetchCallTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallType::class, $objDatabase );
	}

	/**
	 * @return CCallType
	 */
	public static function fetchCallType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallType::class, $objDatabase );
	}

	public static function fetchCallTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_types', $objDatabase );
	}

	public static function fetchCallTypeById( $intId, $objDatabase ) {
		return self::fetchCallType( sprintf( 'SELECT * FROM call_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallTypesByDefaultCallQueueId( $intDefaultCallQueueId, $objDatabase ) {
		return self::fetchCallTypes( sprintf( 'SELECT * FROM call_types WHERE default_call_queue_id = %d', $intDefaultCallQueueId ), $objDatabase );
	}

	public static function fetchCallTypesByCallSourceId( $intCallSourceId, $objDatabase ) {
		return self::fetchCallTypes( sprintf( 'SELECT * FROM call_types WHERE call_source_id = %d', $intCallSourceId ), $objDatabase );
	}

	public static function fetchCallTypesByCallDirectionId( $intCallDirectionId, $objDatabase ) {
		return self::fetchCallTypes( sprintf( 'SELECT * FROM call_types WHERE call_direction_id = %d', $intCallDirectionId ), $objDatabase );
	}

}
?>