<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentNotes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentNotes extends CEosPluralBase {

	/**
	 * @return CCallAgentNote[]
	 */
	public static function fetchCallAgentNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentNote::class, $objDatabase );
	}

	/**
	 * @return CCallAgentNote
	 */
	public static function fetchCallAgentNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentNote::class, $objDatabase );
	}

	public static function fetchCallAgentNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_notes', $objDatabase );
	}

	public static function fetchCallAgentNoteById( $intId, $objDatabase ) {
		return self::fetchCallAgentNote( sprintf( 'SELECT * FROM call_agent_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentNotesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentNotes( sprintf( 'SELECT * FROM call_agent_notes WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentNotesByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallAgentNotes( sprintf( 'SELECT * FROM call_agent_notes WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallAgentNotesByCallAgentLogId( $intCallAgentLogId, $objDatabase ) {
		return self::fetchCallAgentNotes( sprintf( 'SELECT * FROM call_agent_notes WHERE call_agent_log_id = %d', ( int ) $intCallAgentLogId ), $objDatabase );
	}

}
?>