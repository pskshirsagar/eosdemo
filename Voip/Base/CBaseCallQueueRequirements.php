<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueueRequirements
 * Do not add any new functions to this class.
 */

class CBaseCallQueueRequirements extends CEosPluralBase {

	/**
	 * @return CCallQueueRequirement[]
	 */
	public static function fetchCallQueueRequirements( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallQueueRequirement', $objDatabase );
	}

	/**
	 * @return CCallQueueRequirement
	 */
	public static function fetchCallQueueRequirement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallQueueRequirement', $objDatabase );
	}

	public static function fetchCallQueueRequirementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_queue_requirements', $objDatabase );
	}

	public static function fetchCallQueueRequirementById( $intId, $objDatabase ) {
		return self::fetchCallQueueRequirement( sprintf( 'SELECT * FROM call_queue_requirements WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallQueueRequirementsByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallQueueRequirements( sprintf( 'SELECT * FROM call_queue_requirements WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

}
?>