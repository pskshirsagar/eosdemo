<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCalls
 * Do not add any new functions to this class.
 */

class CBaseCalls extends CEosPluralBase {

	/**
	 * @return CCall[]
	 */
	public static function fetchCalls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCall::class, $objDatabase );
	}

	/**
	 * @return CCall
	 */
	public static function fetchCall( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCall::class, $objDatabase );
	}

	public static function fetchCallCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'calls', $objDatabase );
	}

	public static function fetchCallById( $intId, $objDatabase ) {
		return self::fetchCall( sprintf( 'SELECT * FROM calls WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallsByCid( $intCid, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchCallsByRedirectPropertyId( $intRedirectPropertyId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE redirect_property_id = %d', $intRedirectPropertyId ), $objDatabase );
	}

	public static function fetchCallsByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_phone_number_id = %d', $intCallPhoneNumberId ), $objDatabase );
	}

	public static function fetchCallsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_type_id = %d', $intCallTypeId ), $objDatabase );
	}

	public static function fetchCallsByCallFileId( $intCallFileId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_file_id = %d', $intCallFileId ), $objDatabase );
	}

	public static function fetchCallsByCallStatusTypeId( $intCallStatusTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_status_type_id = %d', $intCallStatusTypeId ), $objDatabase );
	}

	public static function fetchCallsByCallResponseTypeId( $intCallResponseTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_response_type_id = %d', $intCallResponseTypeId ), $objDatabase );
	}

	public static function fetchCallsByCallResultId( $intCallResultId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_result_id = %d', $intCallResultId ), $objDatabase );
	}

	public static function fetchCallsByCallPriorityId( $intCallPriorityId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_priority_id = %d', $intCallPriorityId ), $objDatabase );
	}

	public static function fetchCallsByVoiceTypeId( $intVoiceTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE voice_type_id = %d', $intVoiceTypeId ), $objDatabase );
	}

	public static function fetchCallsByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE company_employee_id = %d', $intCompanyEmployeeId ), $objDatabase );
	}

	public static function fetchCallsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallsByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE customer_id = %d', $intCustomerId ), $objDatabase );
	}

	public static function fetchCallsByApplicantApplicationId( $intApplicantApplicationId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE applicant_application_id = %d', $intApplicantApplicationId ), $objDatabase );
	}

	public static function fetchCallsByLeadSourceId( $intLeadSourceId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE lead_source_id = %d', $intLeadSourceId ), $objDatabase );
	}

	public static function fetchCallsByScheduledCallId( $intScheduledCallId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE scheduled_call_id = %d', $intScheduledCallId ), $objDatabase );
	}

	public static function fetchCallsByScheduledCallTypeId( $intScheduledCallTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE scheduled_call_type_id = %d', $intScheduledCallTypeId ), $objDatabase );
	}

	public static function fetchCallsByOutboundCallId( $intOutboundCallId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE outbound_call_id = %d', $intOutboundCallId ), $objDatabase );
	}

	public static function fetchCallsByCallFormStatusTypeId( $intCallFormStatusTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_form_status_type_id = %d', $intCallFormStatusTypeId ), $objDatabase );
	}

	public static function fetchCallsByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_queue_id = %d', $intCallQueueId ), $objDatabase );
	}

	public static function fetchCallsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallsByCallAnalysisStatusTypeId( $intCallAnalysisStatusTypeId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE call_analysis_status_type_id = %d', $intCallAnalysisStatusTypeId ), $objDatabase );
	}

	public static function fetchCallsByIvrMenuActionId( $intIvrMenuActionId, $objDatabase ) {
		return self::fetchCalls( sprintf( 'SELECT * FROM calls WHERE ivr_menu_action_id = %d', $intIvrMenuActionId ), $objDatabase );
	}

}
?>