<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCompanyCallSettings
 * Do not add any new functions to this class.
 */

class CBaseCompanyCallSettings extends CEosPluralBase {

	/**
	 * @return CCompanyCallSetting[]
	 */
	public static function fetchCompanyCallSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyCallSetting::class, $objDatabase );
	}

	/**
	 * @return CCompanyCallSetting
	 */
	public static function fetchCompanyCallSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyCallSetting::class, $objDatabase );
	}

	public static function fetchCompanyCallSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_call_settings', $objDatabase );
	}

	public static function fetchCompanyCallSettingById( $intId, $objDatabase ) {
		return self::fetchCompanyCallSetting( sprintf( 'SELECT * FROM company_call_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyCallSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCallSettings( sprintf( 'SELECT * FROM company_call_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyCallSettingsByDefaultCallQueueId( $intDefaultCallQueueId, $objDatabase ) {
		return self::fetchCompanyCallSettings( sprintf( 'SELECT * FROM company_call_settings WHERE default_call_queue_id = %d', ( int ) $intDefaultCallQueueId ), $objDatabase );
	}

}
?>