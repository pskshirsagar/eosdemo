<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSources
 * Do not add any new functions to this class.
 */

class CBaseCallSources extends CEosPluralBase {

	/**
	 * @return CCallSource[]
	 */
	public static function fetchCallSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallSource::class, $objDatabase );
	}

	/**
	 * @return CCallSource
	 */
	public static function fetchCallSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallSource::class, $objDatabase );
	}

	public static function fetchCallSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_sources', $objDatabase );
	}

	public static function fetchCallSourceById( $intId, $objDatabase ) {
		return self::fetchCallSource( sprintf( 'SELECT * FROM call_sources WHERE id = %d', $intId ), $objDatabase );
	}

}
?>