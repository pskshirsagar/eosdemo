<?php

class CBaseOutboundCall extends CEosSingularBase {

	const TABLE_NAME = 'public.outbound_calls';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intLeaseId;
	protected $m_intCompanyUserId;
	protected $m_intCompanyEmployeeId;
	protected $m_intCallQueueId;
	protected $m_intOutboundCallStatusTypeId;
	protected $m_intCallAgentId;
	protected $m_intCallTypeId;
	protected $m_intOutboundCallCampaignId;
	protected $m_intPhoneNumberTypeId;
	protected $m_intTaskId;
	protected $m_intTaskNoteId;
	protected $m_intCompanyUserScoreId;
	protected $m_intCallPriorityId;
	protected $m_strReceipientName;
	protected $m_strInitiatedOn;
	protected $m_strPhoneNumber;
	protected $m_strDontCallAfter;
	protected $m_intLockSequence;
	protected $m_intAttemptCount;
	protected $m_strAssignedOn;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_strLockedOn;
	protected $m_strEmailParsedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCallPriorityId = '1';
		$this->m_intAttemptCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['lease_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseId', trim( $arrValues['lease_id'] ) ); elseif( isset( $arrValues['lease_id'] ) ) $this->setLeaseId( $arrValues['lease_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['outbound_call_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOutboundCallStatusTypeId', trim( $arrValues['outbound_call_status_type_id'] ) ); elseif( isset( $arrValues['outbound_call_status_type_id'] ) ) $this->setOutboundCallStatusTypeId( $arrValues['outbound_call_status_type_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallTypeId', trim( $arrValues['call_type_id'] ) ); elseif( isset( $arrValues['call_type_id'] ) ) $this->setCallTypeId( $arrValues['call_type_id'] );
		if( isset( $arrValues['outbound_call_campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intOutboundCallCampaignId', trim( $arrValues['outbound_call_campaign_id'] ) ); elseif( isset( $arrValues['outbound_call_campaign_id'] ) ) $this->setOutboundCallCampaignId( $arrValues['outbound_call_campaign_id'] );
		if( isset( $arrValues['phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNumberTypeId', trim( $arrValues['phone_number_type_id'] ) ); elseif( isset( $arrValues['phone_number_type_id'] ) ) $this->setPhoneNumberTypeId( $arrValues['phone_number_type_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['task_note_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskNoteId', trim( $arrValues['task_note_id'] ) ); elseif( isset( $arrValues['task_note_id'] ) ) $this->setTaskNoteId( $arrValues['task_note_id'] );
		if( isset( $arrValues['company_user_score_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserScoreId', trim( $arrValues['company_user_score_id'] ) ); elseif( isset( $arrValues['company_user_score_id'] ) ) $this->setCompanyUserScoreId( $arrValues['company_user_score_id'] );
		if( isset( $arrValues['call_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPriorityId', trim( $arrValues['call_priority_id'] ) ); elseif( isset( $arrValues['call_priority_id'] ) ) $this->setCallPriorityId( $arrValues['call_priority_id'] );
		if( isset( $arrValues['receipient_name'] ) && $boolDirectSet ) $this->set( 'm_strReceipientName', trim( stripcslashes( $arrValues['receipient_name'] ) ) ); elseif( isset( $arrValues['receipient_name'] ) ) $this->setReceipientName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['receipient_name'] ) : $arrValues['receipient_name'] );
		if( isset( $arrValues['initiated_on'] ) && $boolDirectSet ) $this->set( 'm_strInitiatedOn', trim( $arrValues['initiated_on'] ) ); elseif( isset( $arrValues['initiated_on'] ) ) $this->setInitiatedOn( $arrValues['initiated_on'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['dont_call_after'] ) && $boolDirectSet ) $this->set( 'm_strDontCallAfter', trim( $arrValues['dont_call_after'] ) ); elseif( isset( $arrValues['dont_call_after'] ) ) $this->setDontCallAfter( $arrValues['dont_call_after'] );
		if( isset( $arrValues['lock_sequence'] ) && $boolDirectSet ) $this->set( 'm_intLockSequence', trim( $arrValues['lock_sequence'] ) ); elseif( isset( $arrValues['lock_sequence'] ) ) $this->setLockSequence( $arrValues['lock_sequence'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strAssignedOn', trim( $arrValues['assigned_on'] ) ); elseif( isset( $arrValues['assigned_on'] ) ) $this->setAssignedOn( $arrValues['assigned_on'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['email_parsed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailParsedOn', trim( $arrValues['email_parsed_on'] ) ); elseif( isset( $arrValues['email_parsed_on'] ) ) $this->setEmailParsedOn( $arrValues['email_parsed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setLeaseId( $intLeaseId ) {
		$this->set( 'm_intLeaseId', CStrings::strToIntDef( $intLeaseId, NULL, false ) );
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function sqlLeaseId() {
		return ( true == isset( $this->m_intLeaseId ) ) ? ( string ) $this->m_intLeaseId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setOutboundCallStatusTypeId( $intOutboundCallStatusTypeId ) {
		$this->set( 'm_intOutboundCallStatusTypeId', CStrings::strToIntDef( $intOutboundCallStatusTypeId, NULL, false ) );
	}

	public function getOutboundCallStatusTypeId() {
		return $this->m_intOutboundCallStatusTypeId;
	}

	public function sqlOutboundCallStatusTypeId() {
		return ( true == isset( $this->m_intOutboundCallStatusTypeId ) ) ? ( string ) $this->m_intOutboundCallStatusTypeId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallTypeId( $intCallTypeId ) {
		$this->set( 'm_intCallTypeId', CStrings::strToIntDef( $intCallTypeId, NULL, false ) );
	}

	public function getCallTypeId() {
		return $this->m_intCallTypeId;
	}

	public function sqlCallTypeId() {
		return ( true == isset( $this->m_intCallTypeId ) ) ? ( string ) $this->m_intCallTypeId : 'NULL';
	}

	public function setOutboundCallCampaignId( $intOutboundCallCampaignId ) {
		$this->set( 'm_intOutboundCallCampaignId', CStrings::strToIntDef( $intOutboundCallCampaignId, NULL, false ) );
	}

	public function getOutboundCallCampaignId() {
		return $this->m_intOutboundCallCampaignId;
	}

	public function sqlOutboundCallCampaignId() {
		return ( true == isset( $this->m_intOutboundCallCampaignId ) ) ? ( string ) $this->m_intOutboundCallCampaignId : 'NULL';
	}

	public function setPhoneNumberTypeId( $intPhoneNumberTypeId ) {
		$this->set( 'm_intPhoneNumberTypeId', CStrings::strToIntDef( $intPhoneNumberTypeId, NULL, false ) );
	}

	public function getPhoneNumberTypeId() {
		return $this->m_intPhoneNumberTypeId;
	}

	public function sqlPhoneNumberTypeId() {
		return ( true == isset( $this->m_intPhoneNumberTypeId ) ) ? ( string ) $this->m_intPhoneNumberTypeId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setTaskNoteId( $intTaskNoteId ) {
		$this->set( 'm_intTaskNoteId', CStrings::strToIntDef( $intTaskNoteId, NULL, false ) );
	}

	public function getTaskNoteId() {
		return $this->m_intTaskNoteId;
	}

	public function sqlTaskNoteId() {
		return ( true == isset( $this->m_intTaskNoteId ) ) ? ( string ) $this->m_intTaskNoteId : 'NULL';
	}

	public function setCompanyUserScoreId( $intCompanyUserScoreId ) {
		$this->set( 'm_intCompanyUserScoreId', CStrings::strToIntDef( $intCompanyUserScoreId, NULL, false ) );
	}

	public function getCompanyUserScoreId() {
		return $this->m_intCompanyUserScoreId;
	}

	public function sqlCompanyUserScoreId() {
		return ( true == isset( $this->m_intCompanyUserScoreId ) ) ? ( string ) $this->m_intCompanyUserScoreId : 'NULL';
	}

	public function setCallPriorityId( $intCallPriorityId ) {
		$this->set( 'm_intCallPriorityId', CStrings::strToIntDef( $intCallPriorityId, NULL, false ) );
	}

	public function getCallPriorityId() {
		return $this->m_intCallPriorityId;
	}

	public function sqlCallPriorityId() {
		return ( true == isset( $this->m_intCallPriorityId ) ) ? ( string ) $this->m_intCallPriorityId : '1';
	}

	public function setReceipientName( $strReceipientName ) {
		$this->set( 'm_strReceipientName', CStrings::strTrimDef( $strReceipientName, 240, NULL, true ) );
	}

	public function getReceipientName() {
		return $this->m_strReceipientName;
	}

	public function sqlReceipientName() {
		return ( true == isset( $this->m_strReceipientName ) ) ? '\'' . addslashes( $this->m_strReceipientName ) . '\'' : 'NULL';
	}

	public function setInitiatedOn( $strInitiatedOn ) {
		$this->set( 'm_strInitiatedOn', CStrings::strTrimDef( $strInitiatedOn, -1, NULL, true ) );
	}

	public function getInitiatedOn() {
		return $this->m_strInitiatedOn;
	}

	public function sqlInitiatedOn() {
		return ( true == isset( $this->m_strInitiatedOn ) ) ? '\'' . $this->m_strInitiatedOn . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setDontCallAfter( $strDontCallAfter ) {
		$this->set( 'm_strDontCallAfter', CStrings::strTrimDef( $strDontCallAfter, -1, NULL, true ) );
	}

	public function getDontCallAfter() {
		return $this->m_strDontCallAfter;
	}

	public function sqlDontCallAfter() {
		return ( true == isset( $this->m_strDontCallAfter ) ) ? '\'' . $this->m_strDontCallAfter . '\'' : 'NOW()';
	}

	public function setLockSequence( $intLockSequence ) {
		$this->set( 'm_intLockSequence', CStrings::strToIntDef( $intLockSequence, NULL, false ) );
	}

	public function getLockSequence() {
		return $this->m_intLockSequence;
	}

	public function sqlLockSequence() {
		return ( true == isset( $this->m_intLockSequence ) ) ? ( string ) $this->m_intLockSequence : 'NULL';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setAssignedOn( $strAssignedOn ) {
		$this->set( 'm_strAssignedOn', CStrings::strTrimDef( $strAssignedOn, -1, NULL, true ) );
	}

	public function getAssignedOn() {
		return $this->m_strAssignedOn;
	}

	public function sqlAssignedOn() {
		return ( true == isset( $this->m_strAssignedOn ) ) ? '\'' . $this->m_strAssignedOn . '\'' : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, -1, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setEmailParsedOn( $strEmailParsedOn ) {
		$this->set( 'm_strEmailParsedOn', CStrings::strTrimDef( $strEmailParsedOn, -1, NULL, true ) );
	}

	public function getEmailParsedOn() {
		return $this->m_strEmailParsedOn;
	}

	public function sqlEmailParsedOn() {
		return ( true == isset( $this->m_strEmailParsedOn ) ) ? '\'' . $this->m_strEmailParsedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, customer_id, lease_id, company_user_id, company_employee_id, call_queue_id, outbound_call_status_type_id, call_agent_id, call_type_id, outbound_call_campaign_id, phone_number_type_id, task_id, task_note_id, company_user_score_id, call_priority_id, receipient_name, initiated_on, phone_number, dont_call_after, lock_sequence, attempt_count, assigned_on, start_time, end_time, locked_on, email_parsed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCustomerId() . ', ' .
 						$this->sqlLeaseId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlCompanyEmployeeId() . ', ' .
 						$this->sqlCallQueueId() . ', ' .
 						$this->sqlOutboundCallStatusTypeId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallTypeId() . ', ' .
 						$this->sqlOutboundCallCampaignId() . ', ' .
 						$this->sqlPhoneNumberTypeId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlTaskNoteId() . ', ' .
 						$this->sqlCompanyUserScoreId() . ', ' .
 						$this->sqlCallPriorityId() . ', ' .
 						$this->sqlReceipientName() . ', ' .
 						$this->sqlInitiatedOn() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlDontCallAfter() . ', ' .
 						$this->sqlLockSequence() . ', ' .
 						$this->sqlAttemptCount() . ', ' .
 						$this->sqlAssignedOn() . ', ' .
 						$this->sqlStartTime() . ', ' .
 						$this->sqlEndTime() . ', ' .
 						$this->sqlLockedOn() . ', ' .
 						$this->sqlEmailParsedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; } elseif( true == array_key_exists( 'LeaseId', $this->getChangedColumns() ) ) { $strSql .= ' lease_id = ' . $this->sqlLeaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outbound_call_status_type_id = ' . $this->sqlOutboundCallStatusTypeId() . ','; } elseif( true == array_key_exists( 'OutboundCallStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' outbound_call_status_type_id = ' . $this->sqlOutboundCallStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; } elseif( true == array_key_exists( 'CallTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_type_id = ' . $this->sqlCallTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outbound_call_campaign_id = ' . $this->sqlOutboundCallCampaignId() . ','; } elseif( true == array_key_exists( 'OutboundCallCampaignId', $this->getChangedColumns() ) ) { $strSql .= ' outbound_call_campaign_id = ' . $this->sqlOutboundCallCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number_type_id = ' . $this->sqlPhoneNumberTypeId() . ','; } elseif( true == array_key_exists( 'PhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' phone_number_type_id = ' . $this->sqlPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId() . ','; } elseif( true == array_key_exists( 'TaskNoteId', $this->getChangedColumns() ) ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_score_id = ' . $this->sqlCompanyUserScoreId() . ','; } elseif( true == array_key_exists( 'CompanyUserScoreId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_score_id = ' . $this->sqlCompanyUserScoreId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId() . ','; } elseif( true == array_key_exists( 'CallPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' call_priority_id = ' . $this->sqlCallPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' receipient_name = ' . $this->sqlReceipientName() . ','; } elseif( true == array_key_exists( 'ReceipientName', $this->getChangedColumns() ) ) { $strSql .= ' receipient_name = ' . $this->sqlReceipientName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn() . ','; } elseif( true == array_key_exists( 'InitiatedOn', $this->getChangedColumns() ) ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_call_after = ' . $this->sqlDontCallAfter() . ','; } elseif( true == array_key_exists( 'DontCallAfter', $this->getChangedColumns() ) ) { $strSql .= ' dont_call_after = ' . $this->sqlDontCallAfter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_sequence = ' . $this->sqlLockSequence() . ','; } elseif( true == array_key_exists( 'LockSequence', $this->getChangedColumns() ) ) { $strSql .= ' lock_sequence = ' . $this->sqlLockSequence() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; } elseif( true == array_key_exists( 'AssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_parsed_on = ' . $this->sqlEmailParsedOn() . ','; } elseif( true == array_key_exists( 'EmailParsedOn', $this->getChangedColumns() ) ) { $strSql .= ' email_parsed_on = ' . $this->sqlEmailParsedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'lease_id' => $this->getLeaseId(),
			'company_user_id' => $this->getCompanyUserId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'call_queue_id' => $this->getCallQueueId(),
			'outbound_call_status_type_id' => $this->getOutboundCallStatusTypeId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_type_id' => $this->getCallTypeId(),
			'outbound_call_campaign_id' => $this->getOutboundCallCampaignId(),
			'phone_number_type_id' => $this->getPhoneNumberTypeId(),
			'task_id' => $this->getTaskId(),
			'task_note_id' => $this->getTaskNoteId(),
			'company_user_score_id' => $this->getCompanyUserScoreId(),
			'call_priority_id' => $this->getCallPriorityId(),
			'receipient_name' => $this->getReceipientName(),
			'initiated_on' => $this->getInitiatedOn(),
			'phone_number' => $this->getPhoneNumber(),
			'dont_call_after' => $this->getDontCallAfter(),
			'lock_sequence' => $this->getLockSequence(),
			'attempt_count' => $this->getAttemptCount(),
			'assigned_on' => $this->getAssignedOn(),
			'start_time' => $this->getStartTime(),
			'end_time' => $this->getEndTime(),
			'locked_on' => $this->getLockedOn(),
			'email_parsed_on' => $this->getEmailParsedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>