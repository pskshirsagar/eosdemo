<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentSuggestions
 * Do not add any new functions to this class.
 */

class CBaseCallAgentSuggestions extends CEosPluralBase {

	/**
	 * @return CCallAgentSuggestion[]
	 */
	public static function fetchCallAgentSuggestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentSuggestion::class, $objDatabase );
	}

	/**
	 * @return CCallAgentSuggestion
	 */
	public static function fetchCallAgentSuggestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentSuggestion::class, $objDatabase );
	}

	public static function fetchCallAgentSuggestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_suggestions', $objDatabase );
	}

	public static function fetchCallAgentSuggestionById( $intId, $objDatabase ) {
		return self::fetchCallAgentSuggestion( sprintf( 'SELECT * FROM call_agent_suggestions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentSuggestionsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentSuggestions( sprintf( 'SELECT * FROM call_agent_suggestions WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchCallAgentSuggestionsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchCallAgentSuggestions( sprintf( 'SELECT * FROM call_agent_suggestions WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

}
?>