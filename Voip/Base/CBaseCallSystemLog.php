<?php

class CBaseCallSystemLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_system_logs';

	protected $m_intId;
	protected $m_intCallComponentId;
	protected $m_strCallStats;
	protected $m_strActionNotes;
	protected $m_boolIsRunning;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strActionNotes = NULL;
		$this->m_boolIsRunning = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_component_id'] ) && $boolDirectSet ) $this->set( 'm_intCallComponentId', trim( $arrValues['call_component_id'] ) ); elseif( isset( $arrValues['call_component_id'] ) ) $this->setCallComponentId( $arrValues['call_component_id'] );
		if( isset( $arrValues['call_stats'] ) && $boolDirectSet ) $this->set( 'm_strCallStats', trim( stripcslashes( $arrValues['call_stats'] ) ) ); elseif( isset( $arrValues['call_stats'] ) ) $this->setCallStats( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['call_stats'] ) : $arrValues['call_stats'] );
		if( isset( $arrValues['action_notes'] ) && $boolDirectSet ) $this->set( 'm_strActionNotes', trim( stripcslashes( $arrValues['action_notes'] ) ) ); elseif( isset( $arrValues['action_notes'] ) ) $this->setActionNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_notes'] ) : $arrValues['action_notes'] );
		if( isset( $arrValues['is_running'] ) && $boolDirectSet ) $this->set( 'm_boolIsRunning', trim( stripcslashes( $arrValues['is_running'] ) ) ); elseif( isset( $arrValues['is_running'] ) ) $this->setIsRunning( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_running'] ) : $arrValues['is_running'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallComponentId( $intCallComponentId ) {
		$this->set( 'm_intCallComponentId', CStrings::strToIntDef( $intCallComponentId, NULL, false ) );
	}

	public function getCallComponentId() {
		return $this->m_intCallComponentId;
	}

	public function sqlCallComponentId() {
		return ( true == isset( $this->m_intCallComponentId ) ) ? ( string ) $this->m_intCallComponentId : 'NULL';
	}

	public function setCallStats( $strCallStats ) {
		$this->set( 'm_strCallStats', CStrings::strTrimDef( $strCallStats, -1, NULL, true ) );
	}

	public function getCallStats() {
		return $this->m_strCallStats;
	}

	public function sqlCallStats() {
		return ( true == isset( $this->m_strCallStats ) ) ? '\'' . addslashes( $this->m_strCallStats ) . '\'' : 'NULL';
	}

	public function setActionNotes( $strActionNotes ) {
		$this->set( 'm_strActionNotes', CStrings::strTrimDef( $strActionNotes, 540, NULL, true ) );
	}

	public function getActionNotes() {
		return $this->m_strActionNotes;
	}

	public function sqlActionNotes() {
		return ( true == isset( $this->m_strActionNotes ) ) ? '\'' . addslashes( $this->m_strActionNotes ) . '\'' : '\'NULL\'';
	}

	public function setIsRunning( $boolIsRunning ) {
		$this->set( 'm_boolIsRunning', CStrings::strToBool( $boolIsRunning ) );
	}

	public function getIsRunning() {
		return $this->m_boolIsRunning;
	}

	public function sqlIsRunning() {
		return ( true == isset( $this->m_boolIsRunning ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRunning ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, -1, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_component_id, call_stats, action_notes, is_running, start_time, end_time, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallComponentId() . ', ' .
 						$this->sqlCallStats() . ', ' .
 						$this->sqlActionNotes() . ', ' .
 						$this->sqlIsRunning() . ', ' .
 						$this->sqlStartTime() . ', ' .
 						$this->sqlEndTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_component_id = ' . $this->sqlCallComponentId() . ','; } elseif( true == array_key_exists( 'CallComponentId', $this->getChangedColumns() ) ) { $strSql .= ' call_component_id = ' . $this->sqlCallComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_stats = ' . $this->sqlCallStats() . ','; } elseif( true == array_key_exists( 'CallStats', $this->getChangedColumns() ) ) { $strSql .= ' call_stats = ' . $this->sqlCallStats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_notes = ' . $this->sqlActionNotes() . ','; } elseif( true == array_key_exists( 'ActionNotes', $this->getChangedColumns() ) ) { $strSql .= ' action_notes = ' . $this->sqlActionNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_running = ' . $this->sqlIsRunning() . ','; } elseif( true == array_key_exists( 'IsRunning', $this->getChangedColumns() ) ) { $strSql .= ' is_running = ' . $this->sqlIsRunning() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_component_id' => $this->getCallComponentId(),
			'call_stats' => $this->getCallStats(),
			'action_notes' => $this->getActionNotes(),
			'is_running' => $this->getIsRunning(),
			'start_time' => $this->getStartTime(),
			'end_time' => $this->getEndTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>