<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyCallHolidays
 * Do not add any new functions to this class.
 */

class CBasePropertyCallHolidays extends CEosPluralBase {

	/**
	 * @return CPropertyCallHoliday[]
	 */
	public static function fetchPropertyCallHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPropertyCallHoliday::class, $objDatabase );
	}

	/**
	 * @return CPropertyCallHoliday
	 */
	public static function fetchPropertyCallHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPropertyCallHoliday::class, $objDatabase );
	}

	public static function fetchPropertyCallHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_call_holidays', $objDatabase );
	}

	public static function fetchPropertyCallHolidayById( $intId, $objDatabase ) {
		return self::fetchPropertyCallHoliday( sprintf( 'SELECT * FROM property_call_holidays WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyCallHolidaysByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyCallHolidays( sprintf( 'SELECT * FROM property_call_holidays WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyCallHolidaysByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyCallHolidays( sprintf( 'SELECT * FROM property_call_holidays WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchPropertyCallHolidaysByPropertyHolidayId( $intPropertyHolidayId, $objDatabase ) {
		return self::fetchPropertyCallHolidays( sprintf( 'SELECT * FROM property_call_holidays WHERE property_holiday_id = %d', ( int ) $intPropertyHolidayId ), $objDatabase );
	}

}
?>