<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFilterTypes
 * Do not add any new functions to this class.
 */

class CBaseCallFilterTypes extends CEosPluralBase {

	/**
	 * @return CCallFilterType[]
	 */
	public static function fetchCallFilterTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallFilterType::class, $objDatabase );
	}

	/**
	 * @return CCallFilterType
	 */
	public static function fetchCallFilterType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallFilterType::class, $objDatabase );
	}

	public static function fetchCallFilterTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_filter_types', $objDatabase );
	}

	public static function fetchCallFilterTypeById( $intId, $objDatabase ) {
		return self::fetchCallFilterType( sprintf( 'SELECT * FROM call_filter_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>