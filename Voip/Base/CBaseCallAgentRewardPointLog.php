<?php

class CBaseCallAgentRewardPointLog extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_reward_point_logs';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_strRewardPointMultiplierIds;
	protected $m_intRewardPointMultiplierId;
	protected $m_intCallAgentRedeemedProductId;
	protected $m_intRewardPointReferenceTypeId;
	protected $m_intRewardPointReferenceId;
	protected $m_fltEarnedNumberOfPoints;
	protected $m_fltCurrentNumberOfPoints;
	protected $m_fltPreviousNumberOfPoints;
	protected $m_fltCurrentNumberOfCoins;
	protected $m_fltPreviousNumberOfCoins;
	protected $m_strRemark;
	protected $m_strAddedDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strRewardPointMultiplierIds = NULL;
		$this->m_fltEarnedNumberOfPoints = '0.0';
		$this->m_fltCurrentNumberOfPoints = '0.0';
		$this->m_fltPreviousNumberOfPoints = '0.0';
		$this->m_fltCurrentNumberOfCoins = '0.0';
		$this->m_fltPreviousNumberOfCoins = '0.0';
		$this->m_strRemark = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['reward_point_multiplier_ids'] ) && $boolDirectSet ) $this->set( 'm_strRewardPointMultiplierIds', trim( $arrValues['reward_point_multiplier_ids'] ) ); elseif( isset( $arrValues['reward_point_multiplier_ids'] ) ) $this->setRewardPointMultiplierIds( $arrValues['reward_point_multiplier_ids'] );
		if( isset( $arrValues['reward_point_multiplier_id'] ) && $boolDirectSet ) $this->set( 'm_intRewardPointMultiplierId', trim( $arrValues['reward_point_multiplier_id'] ) ); elseif( isset( $arrValues['reward_point_multiplier_id'] ) ) $this->setRewardPointMultiplierId( $arrValues['reward_point_multiplier_id'] );
		if( isset( $arrValues['call_agent_redeemed_product_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentRedeemedProductId', trim( $arrValues['call_agent_redeemed_product_id'] ) ); elseif( isset( $arrValues['call_agent_redeemed_product_id'] ) ) $this->setCallAgentRedeemedProductId( $arrValues['call_agent_redeemed_product_id'] );
		if( isset( $arrValues['reward_point_reference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRewardPointReferenceTypeId', trim( $arrValues['reward_point_reference_type_id'] ) ); elseif( isset( $arrValues['reward_point_reference_type_id'] ) ) $this->setRewardPointReferenceTypeId( $arrValues['reward_point_reference_type_id'] );
		if( isset( $arrValues['reward_point_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intRewardPointReferenceId', trim( $arrValues['reward_point_reference_id'] ) ); elseif( isset( $arrValues['reward_point_reference_id'] ) ) $this->setRewardPointReferenceId( $arrValues['reward_point_reference_id'] );
		if( isset( $arrValues['earned_number_of_points'] ) && $boolDirectSet ) $this->set( 'm_fltEarnedNumberOfPoints', trim( $arrValues['earned_number_of_points'] ) ); elseif( isset( $arrValues['earned_number_of_points'] ) ) $this->setEarnedNumberOfPoints( $arrValues['earned_number_of_points'] );
		if( isset( $arrValues['current_number_of_points'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentNumberOfPoints', trim( $arrValues['current_number_of_points'] ) ); elseif( isset( $arrValues['current_number_of_points'] ) ) $this->setCurrentNumberOfPoints( $arrValues['current_number_of_points'] );
		if( isset( $arrValues['previous_number_of_points'] ) && $boolDirectSet ) $this->set( 'm_fltPreviousNumberOfPoints', trim( $arrValues['previous_number_of_points'] ) ); elseif( isset( $arrValues['previous_number_of_points'] ) ) $this->setPreviousNumberOfPoints( $arrValues['previous_number_of_points'] );
		if( isset( $arrValues['current_number_of_coins'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentNumberOfCoins', trim( $arrValues['current_number_of_coins'] ) ); elseif( isset( $arrValues['current_number_of_coins'] ) ) $this->setCurrentNumberOfCoins( $arrValues['current_number_of_coins'] );
		if( isset( $arrValues['previous_number_of_coins'] ) && $boolDirectSet ) $this->set( 'm_fltPreviousNumberOfCoins', trim( $arrValues['previous_number_of_coins'] ) ); elseif( isset( $arrValues['previous_number_of_coins'] ) ) $this->setPreviousNumberOfCoins( $arrValues['previous_number_of_coins'] );
		if( isset( $arrValues['remark'] ) && $boolDirectSet ) $this->set( 'm_strRemark', trim( $arrValues['remark'] ) ); elseif( isset( $arrValues['remark'] ) ) $this->setRemark( $arrValues['remark'] );
		if( isset( $arrValues['added_date'] ) && $boolDirectSet ) $this->set( 'm_strAddedDate', trim( $arrValues['added_date'] ) ); elseif( isset( $arrValues['added_date'] ) ) $this->setAddedDate( $arrValues['added_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setRewardPointMultiplierIds( $strRewardPointMultiplierIds ) {
		$this->set( 'm_strRewardPointMultiplierIds', CStrings::strTrimDef( $strRewardPointMultiplierIds, 40, NULL, true ) );
	}

	public function getRewardPointMultiplierIds() {
		return $this->m_strRewardPointMultiplierIds;
	}

	public function sqlRewardPointMultiplierIds() {
		return ( true == isset( $this->m_strRewardPointMultiplierIds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRewardPointMultiplierIds ) : '\'' . addslashes( $this->m_strRewardPointMultiplierIds ) . '\'' ) : '\'NULL\'';
	}

	public function setRewardPointMultiplierId( $intRewardPointMultiplierId ) {
		$this->set( 'm_intRewardPointMultiplierId', CStrings::strToIntDef( $intRewardPointMultiplierId, NULL, false ) );
	}

	public function getRewardPointMultiplierId() {
		return $this->m_intRewardPointMultiplierId;
	}

	public function sqlRewardPointMultiplierId() {
		return ( true == isset( $this->m_intRewardPointMultiplierId ) ) ? ( string ) $this->m_intRewardPointMultiplierId : 'NULL';
	}

	public function setCallAgentRedeemedProductId( $intCallAgentRedeemedProductId ) {
		$this->set( 'm_intCallAgentRedeemedProductId', CStrings::strToIntDef( $intCallAgentRedeemedProductId, NULL, false ) );
	}

	public function getCallAgentRedeemedProductId() {
		return $this->m_intCallAgentRedeemedProductId;
	}

	public function sqlCallAgentRedeemedProductId() {
		return ( true == isset( $this->m_intCallAgentRedeemedProductId ) ) ? ( string ) $this->m_intCallAgentRedeemedProductId : 'NULL';
	}

	public function setRewardPointReferenceTypeId( $intRewardPointReferenceTypeId ) {
		$this->set( 'm_intRewardPointReferenceTypeId', CStrings::strToIntDef( $intRewardPointReferenceTypeId, NULL, false ) );
	}

	public function getRewardPointReferenceTypeId() {
		return $this->m_intRewardPointReferenceTypeId;
	}

	public function sqlRewardPointReferenceTypeId() {
		return ( true == isset( $this->m_intRewardPointReferenceTypeId ) ) ? ( string ) $this->m_intRewardPointReferenceTypeId : 'NULL';
	}

	public function setRewardPointReferenceId( $intRewardPointReferenceId ) {
		$this->set( 'm_intRewardPointReferenceId', CStrings::strToIntDef( $intRewardPointReferenceId, NULL, false ) );
	}

	public function getRewardPointReferenceId() {
		return $this->m_intRewardPointReferenceId;
	}

	public function sqlRewardPointReferenceId() {
		return ( true == isset( $this->m_intRewardPointReferenceId ) ) ? ( string ) $this->m_intRewardPointReferenceId : 'NULL';
	}

	public function setEarnedNumberOfPoints( $fltEarnedNumberOfPoints ) {
		$this->set( 'm_fltEarnedNumberOfPoints', CStrings::strToFloatDef( $fltEarnedNumberOfPoints, NULL, false, 0 ) );
	}

	public function getEarnedNumberOfPoints() {
		return $this->m_fltEarnedNumberOfPoints;
	}

	public function sqlEarnedNumberOfPoints() {
		return ( true == isset( $this->m_fltEarnedNumberOfPoints ) ) ? ( string ) $this->m_fltEarnedNumberOfPoints : '0.0';
	}

	public function setCurrentNumberOfPoints( $fltCurrentNumberOfPoints ) {
		$this->set( 'm_fltCurrentNumberOfPoints', CStrings::strToFloatDef( $fltCurrentNumberOfPoints, NULL, false, 0 ) );
	}

	public function getCurrentNumberOfPoints() {
		return $this->m_fltCurrentNumberOfPoints;
	}

	public function sqlCurrentNumberOfPoints() {
		return ( true == isset( $this->m_fltCurrentNumberOfPoints ) ) ? ( string ) $this->m_fltCurrentNumberOfPoints : '0.0';
	}

	public function setPreviousNumberOfPoints( $fltPreviousNumberOfPoints ) {
		$this->set( 'm_fltPreviousNumberOfPoints', CStrings::strToFloatDef( $fltPreviousNumberOfPoints, NULL, false, 0 ) );
	}

	public function getPreviousNumberOfPoints() {
		return $this->m_fltPreviousNumberOfPoints;
	}

	public function sqlPreviousNumberOfPoints() {
		return ( true == isset( $this->m_fltPreviousNumberOfPoints ) ) ? ( string ) $this->m_fltPreviousNumberOfPoints : '0.0';
	}

	public function setCurrentNumberOfCoins( $fltCurrentNumberOfCoins ) {
		$this->set( 'm_fltCurrentNumberOfCoins', CStrings::strToFloatDef( $fltCurrentNumberOfCoins, NULL, false, 0 ) );
	}

	public function getCurrentNumberOfCoins() {
		return $this->m_fltCurrentNumberOfCoins;
	}

	public function sqlCurrentNumberOfCoins() {
		return ( true == isset( $this->m_fltCurrentNumberOfCoins ) ) ? ( string ) $this->m_fltCurrentNumberOfCoins : '0.0';
	}

	public function setPreviousNumberOfCoins( $fltPreviousNumberOfCoins ) {
		$this->set( 'm_fltPreviousNumberOfCoins', CStrings::strToFloatDef( $fltPreviousNumberOfCoins, NULL, false, 0 ) );
	}

	public function getPreviousNumberOfCoins() {
		return $this->m_fltPreviousNumberOfCoins;
	}

	public function sqlPreviousNumberOfCoins() {
		return ( true == isset( $this->m_fltPreviousNumberOfCoins ) ) ? ( string ) $this->m_fltPreviousNumberOfCoins : '0.0';
	}

	public function setRemark( $strRemark ) {
		$this->set( 'm_strRemark', CStrings::strTrimDef( $strRemark, 50, NULL, true ) );
	}

	public function getRemark() {
		return $this->m_strRemark;
	}

	public function sqlRemark() {
		return ( true == isset( $this->m_strRemark ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemark ) : '\'' . addslashes( $this->m_strRemark ) . '\'' ) : '\'NULL\'';
	}

	public function setAddedDate( $strAddedDate ) {
		$this->set( 'm_strAddedDate', CStrings::strTrimDef( $strAddedDate, -1, NULL, true ) );
	}

	public function getAddedDate() {
		return $this->m_strAddedDate;
	}

	public function sqlAddedDate() {
		return ( true == isset( $this->m_strAddedDate ) ) ? '\'' . $this->m_strAddedDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, reward_point_multiplier_ids, reward_point_multiplier_id, call_agent_redeemed_product_id, reward_point_reference_type_id, reward_point_reference_id, earned_number_of_points, current_number_of_points, previous_number_of_points, current_number_of_coins, previous_number_of_coins, remark, added_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlRewardPointMultiplierIds() . ', ' .
						$this->sqlRewardPointMultiplierId() . ', ' .
						$this->sqlCallAgentRedeemedProductId() . ', ' .
						$this->sqlRewardPointReferenceTypeId() . ', ' .
						$this->sqlRewardPointReferenceId() . ', ' .
						$this->sqlEarnedNumberOfPoints() . ', ' .
						$this->sqlCurrentNumberOfPoints() . ', ' .
						$this->sqlPreviousNumberOfPoints() . ', ' .
						$this->sqlCurrentNumberOfCoins() . ', ' .
						$this->sqlPreviousNumberOfCoins() . ', ' .
						$this->sqlRemark() . ', ' .
						$this->sqlAddedDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_point_multiplier_ids = ' . $this->sqlRewardPointMultiplierIds(). ',' ; } elseif( true == array_key_exists( 'RewardPointMultiplierIds', $this->getChangedColumns() ) ) { $strSql .= ' reward_point_multiplier_ids = ' . $this->sqlRewardPointMultiplierIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_point_multiplier_id = ' . $this->sqlRewardPointMultiplierId(). ',' ; } elseif( true == array_key_exists( 'RewardPointMultiplierId', $this->getChangedColumns() ) ) { $strSql .= ' reward_point_multiplier_id = ' . $this->sqlRewardPointMultiplierId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_redeemed_product_id = ' . $this->sqlCallAgentRedeemedProductId(). ',' ; } elseif( true == array_key_exists( 'CallAgentRedeemedProductId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_redeemed_product_id = ' . $this->sqlCallAgentRedeemedProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_point_reference_type_id = ' . $this->sqlRewardPointReferenceTypeId(). ',' ; } elseif( true == array_key_exists( 'RewardPointReferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reward_point_reference_type_id = ' . $this->sqlRewardPointReferenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_point_reference_id = ' . $this->sqlRewardPointReferenceId(). ',' ; } elseif( true == array_key_exists( 'RewardPointReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reward_point_reference_id = ' . $this->sqlRewardPointReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' earned_number_of_points = ' . $this->sqlEarnedNumberOfPoints(). ',' ; } elseif( true == array_key_exists( 'EarnedNumberOfPoints', $this->getChangedColumns() ) ) { $strSql .= ' earned_number_of_points = ' . $this->sqlEarnedNumberOfPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_number_of_points = ' . $this->sqlCurrentNumberOfPoints(). ',' ; } elseif( true == array_key_exists( 'CurrentNumberOfPoints', $this->getChangedColumns() ) ) { $strSql .= ' current_number_of_points = ' . $this->sqlCurrentNumberOfPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_number_of_points = ' . $this->sqlPreviousNumberOfPoints(). ',' ; } elseif( true == array_key_exists( 'PreviousNumberOfPoints', $this->getChangedColumns() ) ) { $strSql .= ' previous_number_of_points = ' . $this->sqlPreviousNumberOfPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_number_of_coins = ' . $this->sqlCurrentNumberOfCoins(). ',' ; } elseif( true == array_key_exists( 'CurrentNumberOfCoins', $this->getChangedColumns() ) ) { $strSql .= ' current_number_of_coins = ' . $this->sqlCurrentNumberOfCoins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_number_of_coins = ' . $this->sqlPreviousNumberOfCoins(). ',' ; } elseif( true == array_key_exists( 'PreviousNumberOfCoins', $this->getChangedColumns() ) ) { $strSql .= ' previous_number_of_coins = ' . $this->sqlPreviousNumberOfCoins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remark = ' . $this->sqlRemark(). ',' ; } elseif( true == array_key_exists( 'Remark', $this->getChangedColumns() ) ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' added_date = ' . $this->sqlAddedDate(). ',' ; } elseif( true == array_key_exists( 'AddedDate', $this->getChangedColumns() ) ) { $strSql .= ' added_date = ' . $this->sqlAddedDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'reward_point_multiplier_ids' => $this->getRewardPointMultiplierIds(),
			'reward_point_multiplier_id' => $this->getRewardPointMultiplierId(),
			'call_agent_redeemed_product_id' => $this->getCallAgentRedeemedProductId(),
			'reward_point_reference_type_id' => $this->getRewardPointReferenceTypeId(),
			'reward_point_reference_id' => $this->getRewardPointReferenceId(),
			'earned_number_of_points' => $this->getEarnedNumberOfPoints(),
			'current_number_of_points' => $this->getCurrentNumberOfPoints(),
			'previous_number_of_points' => $this->getPreviousNumberOfPoints(),
			'current_number_of_coins' => $this->getCurrentNumberOfCoins(),
			'previous_number_of_coins' => $this->getPreviousNumberOfCoins(),
			'remark' => $this->getRemark(),
			'added_date' => $this->getAddedDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>