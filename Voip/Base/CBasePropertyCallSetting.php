<?php

class CBasePropertyCallSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.property_call_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSettingsTemplateId;
	protected $m_intTimeZoneId;
	protected $m_strPropertyName;
	protected $m_strMarketingPropertyName;
	protected $m_strOfficePhoneNumber;
	protected $m_strMaintenancePhoneNumber;
	protected $m_strMaintenanceEmergencyPhoneNumber;
	protected $m_strAfterHoursOfficePhoneNumber;
	protected $m_strAfterHoursMaintenancePhoneNumber;
	protected $m_strAfterHoursMaintenanceEmergencyPhoneNumber;
	protected $m_strLeadRoutingCondition;
	protected $m_strMaintenanceRoutingCondition;
	protected $m_strMaintenanceEmergencyRoutingCondition;
	protected $m_intIsGuestCardRoutingToPsi;
	protected $m_intSecondsToRingPhone;
	protected $m_intIsSendVoicemailToProperty;
	protected $m_intUsePsiVoicemail;
	protected $m_intTimezoneOffset;
	protected $m_intIsAutoArchiveCall;
	protected $m_intIsOfficeForwarding;
	protected $m_intIsCalendarEnabled;
	protected $m_boolIsActiveNewDashboard;
	protected $m_strSunOpenTime;
	protected $m_strSunCloseTime;
	protected $m_strMonOpenTime;
	protected $m_strMonCloseTime;
	protected $m_strTueOpenTime;
	protected $m_strTueCloseTime;
	protected $m_strWedOpenTime;
	protected $m_strWedCloseTime;
	protected $m_strThuOpenTime;
	protected $m_strThuCloseTime;
	protected $m_strFriOpenTime;
	protected $m_strFriCloseTime;
	protected $m_strSatOpenTime;
	protected $m_strSatCloseTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTimeZoneId = '6';
		$this->m_intIsGuestCardRoutingToPsi = '0';
		$this->m_intIsSendVoicemailToProperty = '0';
		$this->m_intIsAutoArchiveCall = '0';
		$this->m_intIsOfficeForwarding = '0';
		$this->m_intIsCalendarEnabled = '0';
		$this->m_boolIsActiveNewDashboard = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['settings_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSettingsTemplateId', trim( $arrValues['settings_template_id'] ) ); elseif( isset( $arrValues['settings_template_id'] ) ) $this->setSettingsTemplateId( $arrValues['settings_template_id'] );
		if( isset( $arrValues['time_zone_id'] ) && $boolDirectSet ) $this->set( 'm_intTimeZoneId', trim( $arrValues['time_zone_id'] ) ); elseif( isset( $arrValues['time_zone_id'] ) ) $this->setTimeZoneId( $arrValues['time_zone_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['marketing_property_name'] ) && $boolDirectSet ) $this->set( 'm_strMarketingPropertyName', trim( stripcslashes( $arrValues['marketing_property_name'] ) ) ); elseif( isset( $arrValues['marketing_property_name'] ) ) $this->setMarketingPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marketing_property_name'] ) : $arrValues['marketing_property_name'] );
		if( isset( $arrValues['office_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strOfficePhoneNumber', trim( stripcslashes( $arrValues['office_phone_number'] ) ) ); elseif( isset( $arrValues['office_phone_number'] ) ) $this->setOfficePhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['office_phone_number'] ) : $arrValues['office_phone_number'] );
		if( isset( $arrValues['maintenance_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strMaintenancePhoneNumber', trim( stripcslashes( $arrValues['maintenance_phone_number'] ) ) ); elseif( isset( $arrValues['maintenance_phone_number'] ) ) $this->setMaintenancePhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_phone_number'] ) : $arrValues['maintenance_phone_number'] );
		if( isset( $arrValues['maintenance_emergency_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceEmergencyPhoneNumber', trim( stripcslashes( $arrValues['maintenance_emergency_phone_number'] ) ) ); elseif( isset( $arrValues['maintenance_emergency_phone_number'] ) ) $this->setMaintenanceEmergencyPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_emergency_phone_number'] ) : $arrValues['maintenance_emergency_phone_number'] );
		if( isset( $arrValues['after_hours_office_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAfterHoursOfficePhoneNumber', trim( stripcslashes( $arrValues['after_hours_office_phone_number'] ) ) ); elseif( isset( $arrValues['after_hours_office_phone_number'] ) ) $this->setAfterHoursOfficePhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['after_hours_office_phone_number'] ) : $arrValues['after_hours_office_phone_number'] );
		if( isset( $arrValues['after_hours_maintenance_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAfterHoursMaintenancePhoneNumber', trim( stripcslashes( $arrValues['after_hours_maintenance_phone_number'] ) ) ); elseif( isset( $arrValues['after_hours_maintenance_phone_number'] ) ) $this->setAfterHoursMaintenancePhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['after_hours_maintenance_phone_number'] ) : $arrValues['after_hours_maintenance_phone_number'] );
		if( isset( $arrValues['after_hours_maintenance_emergency_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAfterHoursMaintenanceEmergencyPhoneNumber', trim( stripcslashes( $arrValues['after_hours_maintenance_emergency_phone_number'] ) ) ); elseif( isset( $arrValues['after_hours_maintenance_emergency_phone_number'] ) ) $this->setAfterHoursMaintenanceEmergencyPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['after_hours_maintenance_emergency_phone_number'] ) : $arrValues['after_hours_maintenance_emergency_phone_number'] );
		if( isset( $arrValues['lead_routing_condition'] ) && $boolDirectSet ) $this->set( 'm_strLeadRoutingCondition', trim( stripcslashes( $arrValues['lead_routing_condition'] ) ) ); elseif( isset( $arrValues['lead_routing_condition'] ) ) $this->setLeadRoutingCondition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['lead_routing_condition'] ) : $arrValues['lead_routing_condition'] );
		if( isset( $arrValues['maintenance_routing_condition'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceRoutingCondition', trim( stripcslashes( $arrValues['maintenance_routing_condition'] ) ) ); elseif( isset( $arrValues['maintenance_routing_condition'] ) ) $this->setMaintenanceRoutingCondition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_routing_condition'] ) : $arrValues['maintenance_routing_condition'] );
		if( isset( $arrValues['maintenance_emergency_routing_condition'] ) && $boolDirectSet ) $this->set( 'm_strMaintenanceEmergencyRoutingCondition', trim( stripcslashes( $arrValues['maintenance_emergency_routing_condition'] ) ) ); elseif( isset( $arrValues['maintenance_emergency_routing_condition'] ) ) $this->setMaintenanceEmergencyRoutingCondition( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['maintenance_emergency_routing_condition'] ) : $arrValues['maintenance_emergency_routing_condition'] );
		if( isset( $arrValues['is_guest_card_routing_to_psi'] ) && $boolDirectSet ) $this->set( 'm_intIsGuestCardRoutingToPsi', trim( $arrValues['is_guest_card_routing_to_psi'] ) ); elseif( isset( $arrValues['is_guest_card_routing_to_psi'] ) ) $this->setIsGuestCardRoutingToPsi( $arrValues['is_guest_card_routing_to_psi'] );
		if( isset( $arrValues['seconds_to_ring_phone'] ) && $boolDirectSet ) $this->set( 'm_intSecondsToRingPhone', trim( $arrValues['seconds_to_ring_phone'] ) ); elseif( isset( $arrValues['seconds_to_ring_phone'] ) ) $this->setSecondsToRingPhone( $arrValues['seconds_to_ring_phone'] );
		if( isset( $arrValues['is_send_voicemail_to_property'] ) && $boolDirectSet ) $this->set( 'm_intIsSendVoicemailToProperty', trim( $arrValues['is_send_voicemail_to_property'] ) ); elseif( isset( $arrValues['is_send_voicemail_to_property'] ) ) $this->setIsSendVoicemailToProperty( $arrValues['is_send_voicemail_to_property'] );
		if( isset( $arrValues['use_psi_voicemail'] ) && $boolDirectSet ) $this->set( 'm_intUsePsiVoicemail', trim( $arrValues['use_psi_voicemail'] ) ); elseif( isset( $arrValues['use_psi_voicemail'] ) ) $this->setUsePsiVoicemail( $arrValues['use_psi_voicemail'] );
		if( isset( $arrValues['timezone_offset'] ) && $boolDirectSet ) $this->set( 'm_intTimezoneOffset', trim( $arrValues['timezone_offset'] ) ); elseif( isset( $arrValues['timezone_offset'] ) ) $this->setTimezoneOffset( $arrValues['timezone_offset'] );
		if( isset( $arrValues['is_auto_archive_call'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoArchiveCall', trim( $arrValues['is_auto_archive_call'] ) ); elseif( isset( $arrValues['is_auto_archive_call'] ) ) $this->setIsAutoArchiveCall( $arrValues['is_auto_archive_call'] );
		if( isset( $arrValues['is_office_forwarding'] ) && $boolDirectSet ) $this->set( 'm_intIsOfficeForwarding', trim( $arrValues['is_office_forwarding'] ) ); elseif( isset( $arrValues['is_office_forwarding'] ) ) $this->setIsOfficeForwarding( $arrValues['is_office_forwarding'] );
		if( isset( $arrValues['is_calendar_enabled'] ) && $boolDirectSet ) $this->set( 'm_intIsCalendarEnabled', trim( $arrValues['is_calendar_enabled'] ) ); elseif( isset( $arrValues['is_calendar_enabled'] ) ) $this->setIsCalendarEnabled( $arrValues['is_calendar_enabled'] );
		if( isset( $arrValues['is_active_new_dashboard'] ) && $boolDirectSet ) $this->set( 'm_boolIsActiveNewDashboard', trim( stripcslashes( $arrValues['is_active_new_dashboard'] ) ) ); elseif( isset( $arrValues['is_active_new_dashboard'] ) ) $this->setIsActiveNewDashboard( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active_new_dashboard'] ) : $arrValues['is_active_new_dashboard'] );
		if( isset( $arrValues['sun_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSunOpenTime', trim( $arrValues['sun_open_time'] ) ); elseif( isset( $arrValues['sun_open_time'] ) ) $this->setSunOpenTime( $arrValues['sun_open_time'] );
		if( isset( $arrValues['sun_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSunCloseTime', trim( $arrValues['sun_close_time'] ) ); elseif( isset( $arrValues['sun_close_time'] ) ) $this->setSunCloseTime( $arrValues['sun_close_time'] );
		if( isset( $arrValues['mon_open_time'] ) && $boolDirectSet ) $this->set( 'm_strMonOpenTime', trim( $arrValues['mon_open_time'] ) ); elseif( isset( $arrValues['mon_open_time'] ) ) $this->setMonOpenTime( $arrValues['mon_open_time'] );
		if( isset( $arrValues['mon_close_time'] ) && $boolDirectSet ) $this->set( 'm_strMonCloseTime', trim( $arrValues['mon_close_time'] ) ); elseif( isset( $arrValues['mon_close_time'] ) ) $this->setMonCloseTime( $arrValues['mon_close_time'] );
		if( isset( $arrValues['tue_open_time'] ) && $boolDirectSet ) $this->set( 'm_strTueOpenTime', trim( $arrValues['tue_open_time'] ) ); elseif( isset( $arrValues['tue_open_time'] ) ) $this->setTueOpenTime( $arrValues['tue_open_time'] );
		if( isset( $arrValues['tue_close_time'] ) && $boolDirectSet ) $this->set( 'm_strTueCloseTime', trim( $arrValues['tue_close_time'] ) ); elseif( isset( $arrValues['tue_close_time'] ) ) $this->setTueCloseTime( $arrValues['tue_close_time'] );
		if( isset( $arrValues['wed_open_time'] ) && $boolDirectSet ) $this->set( 'm_strWedOpenTime', trim( $arrValues['wed_open_time'] ) ); elseif( isset( $arrValues['wed_open_time'] ) ) $this->setWedOpenTime( $arrValues['wed_open_time'] );
		if( isset( $arrValues['wed_close_time'] ) && $boolDirectSet ) $this->set( 'm_strWedCloseTime', trim( $arrValues['wed_close_time'] ) ); elseif( isset( $arrValues['wed_close_time'] ) ) $this->setWedCloseTime( $arrValues['wed_close_time'] );
		if( isset( $arrValues['thu_open_time'] ) && $boolDirectSet ) $this->set( 'm_strThuOpenTime', trim( $arrValues['thu_open_time'] ) ); elseif( isset( $arrValues['thu_open_time'] ) ) $this->setThuOpenTime( $arrValues['thu_open_time'] );
		if( isset( $arrValues['thu_close_time'] ) && $boolDirectSet ) $this->set( 'm_strThuCloseTime', trim( $arrValues['thu_close_time'] ) ); elseif( isset( $arrValues['thu_close_time'] ) ) $this->setThuCloseTime( $arrValues['thu_close_time'] );
		if( isset( $arrValues['fri_open_time'] ) && $boolDirectSet ) $this->set( 'm_strFriOpenTime', trim( $arrValues['fri_open_time'] ) ); elseif( isset( $arrValues['fri_open_time'] ) ) $this->setFriOpenTime( $arrValues['fri_open_time'] );
		if( isset( $arrValues['fri_close_time'] ) && $boolDirectSet ) $this->set( 'm_strFriCloseTime', trim( $arrValues['fri_close_time'] ) ); elseif( isset( $arrValues['fri_close_time'] ) ) $this->setFriCloseTime( $arrValues['fri_close_time'] );
		if( isset( $arrValues['sat_open_time'] ) && $boolDirectSet ) $this->set( 'm_strSatOpenTime', trim( $arrValues['sat_open_time'] ) ); elseif( isset( $arrValues['sat_open_time'] ) ) $this->setSatOpenTime( $arrValues['sat_open_time'] );
		if( isset( $arrValues['sat_close_time'] ) && $boolDirectSet ) $this->set( 'm_strSatCloseTime', trim( $arrValues['sat_close_time'] ) ); elseif( isset( $arrValues['sat_close_time'] ) ) $this->setSatCloseTime( $arrValues['sat_close_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSettingsTemplateId( $intSettingsTemplateId ) {
		$this->set( 'm_intSettingsTemplateId', CStrings::strToIntDef( $intSettingsTemplateId, NULL, false ) );
	}

	public function getSettingsTemplateId() {
		return $this->m_intSettingsTemplateId;
	}

	public function sqlSettingsTemplateId() {
		return ( true == isset( $this->m_intSettingsTemplateId ) ) ? ( string ) $this->m_intSettingsTemplateId : 'NULL';
	}

	public function setTimeZoneId( $intTimeZoneId ) {
		$this->set( 'm_intTimeZoneId', CStrings::strToIntDef( $intTimeZoneId, NULL, false ) );
	}

	public function getTimeZoneId() {
		return $this->m_intTimeZoneId;
	}

	public function sqlTimeZoneId() {
		return ( true == isset( $this->m_intTimeZoneId ) ) ? ( string ) $this->m_intTimeZoneId : '6';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 100, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setMarketingPropertyName( $strMarketingPropertyName ) {
		$this->set( 'm_strMarketingPropertyName', CStrings::strTrimDef( $strMarketingPropertyName, 50, NULL, true ) );
	}

	public function getMarketingPropertyName() {
		return $this->m_strMarketingPropertyName;
	}

	public function sqlMarketingPropertyName() {
		return ( true == isset( $this->m_strMarketingPropertyName ) ) ? '\'' . addslashes( $this->m_strMarketingPropertyName ) . '\'' : 'NULL';
	}

	public function setOfficePhoneNumber( $strOfficePhoneNumber ) {
		$this->set( 'm_strOfficePhoneNumber', CStrings::strTrimDef( $strOfficePhoneNumber, 30, NULL, true ) );
	}

	public function getOfficePhoneNumber() {
		return $this->m_strOfficePhoneNumber;
	}

	public function sqlOfficePhoneNumber() {
		return ( true == isset( $this->m_strOfficePhoneNumber ) ) ? '\'' . addslashes( $this->m_strOfficePhoneNumber ) . '\'' : 'NULL';
	}

	public function setMaintenancePhoneNumber( $strMaintenancePhoneNumber ) {
		$this->set( 'm_strMaintenancePhoneNumber', CStrings::strTrimDef( $strMaintenancePhoneNumber, 30, NULL, true ) );
	}

	public function getMaintenancePhoneNumber() {
		return $this->m_strMaintenancePhoneNumber;
	}

	public function sqlMaintenancePhoneNumber() {
		return ( true == isset( $this->m_strMaintenancePhoneNumber ) ) ? '\'' . addslashes( $this->m_strMaintenancePhoneNumber ) . '\'' : 'NULL';
	}

	public function setMaintenanceEmergencyPhoneNumber( $strMaintenanceEmergencyPhoneNumber ) {
		$this->set( 'm_strMaintenanceEmergencyPhoneNumber', CStrings::strTrimDef( $strMaintenanceEmergencyPhoneNumber, 30, NULL, true ) );
	}

	public function getMaintenanceEmergencyPhoneNumber() {
		return $this->m_strMaintenanceEmergencyPhoneNumber;
	}

	public function sqlMaintenanceEmergencyPhoneNumber() {
		return ( true == isset( $this->m_strMaintenanceEmergencyPhoneNumber ) ) ? '\'' . addslashes( $this->m_strMaintenanceEmergencyPhoneNumber ) . '\'' : 'NULL';
	}

	public function setAfterHoursOfficePhoneNumber( $strAfterHoursOfficePhoneNumber ) {
		$this->set( 'm_strAfterHoursOfficePhoneNumber', CStrings::strTrimDef( $strAfterHoursOfficePhoneNumber, 30, NULL, true ) );
	}

	public function getAfterHoursOfficePhoneNumber() {
		return $this->m_strAfterHoursOfficePhoneNumber;
	}

	public function sqlAfterHoursOfficePhoneNumber() {
		return ( true == isset( $this->m_strAfterHoursOfficePhoneNumber ) ) ? '\'' . addslashes( $this->m_strAfterHoursOfficePhoneNumber ) . '\'' : 'NULL';
	}

	public function setAfterHoursMaintenancePhoneNumber( $strAfterHoursMaintenancePhoneNumber ) {
		$this->set( 'm_strAfterHoursMaintenancePhoneNumber', CStrings::strTrimDef( $strAfterHoursMaintenancePhoneNumber, 30, NULL, true ) );
	}

	public function getAfterHoursMaintenancePhoneNumber() {
		return $this->m_strAfterHoursMaintenancePhoneNumber;
	}

	public function sqlAfterHoursMaintenancePhoneNumber() {
		return ( true == isset( $this->m_strAfterHoursMaintenancePhoneNumber ) ) ? '\'' . addslashes( $this->m_strAfterHoursMaintenancePhoneNumber ) . '\'' : 'NULL';
	}

	public function setAfterHoursMaintenanceEmergencyPhoneNumber( $strAfterHoursMaintenanceEmergencyPhoneNumber ) {
		$this->set( 'm_strAfterHoursMaintenanceEmergencyPhoneNumber', CStrings::strTrimDef( $strAfterHoursMaintenanceEmergencyPhoneNumber, 30, NULL, true ) );
	}

	public function getAfterHoursMaintenanceEmergencyPhoneNumber() {
		return $this->m_strAfterHoursMaintenanceEmergencyPhoneNumber;
	}

	public function sqlAfterHoursMaintenanceEmergencyPhoneNumber() {
		return ( true == isset( $this->m_strAfterHoursMaintenanceEmergencyPhoneNumber ) ) ? '\'' . addslashes( $this->m_strAfterHoursMaintenanceEmergencyPhoneNumber ) . '\'' : 'NULL';
	}

	public function setLeadRoutingCondition( $strLeadRoutingCondition ) {
		$this->set( 'm_strLeadRoutingCondition', CStrings::strTrimDef( $strLeadRoutingCondition, 20, NULL, true ) );
	}

	public function getLeadRoutingCondition() {
		return $this->m_strLeadRoutingCondition;
	}

	public function sqlLeadRoutingCondition() {
		return ( true == isset( $this->m_strLeadRoutingCondition ) ) ? '\'' . addslashes( $this->m_strLeadRoutingCondition ) . '\'' : 'NULL';
	}

	public function setMaintenanceRoutingCondition( $strMaintenanceRoutingCondition ) {
		$this->set( 'm_strMaintenanceRoutingCondition', CStrings::strTrimDef( $strMaintenanceRoutingCondition, 20, NULL, true ) );
	}

	public function getMaintenanceRoutingCondition() {
		return $this->m_strMaintenanceRoutingCondition;
	}

	public function sqlMaintenanceRoutingCondition() {
		return ( true == isset( $this->m_strMaintenanceRoutingCondition ) ) ? '\'' . addslashes( $this->m_strMaintenanceRoutingCondition ) . '\'' : 'NULL';
	}

	public function setMaintenanceEmergencyRoutingCondition( $strMaintenanceEmergencyRoutingCondition ) {
		$this->set( 'm_strMaintenanceEmergencyRoutingCondition', CStrings::strTrimDef( $strMaintenanceEmergencyRoutingCondition, 20, NULL, true ) );
	}

	public function getMaintenanceEmergencyRoutingCondition() {
		return $this->m_strMaintenanceEmergencyRoutingCondition;
	}

	public function sqlMaintenanceEmergencyRoutingCondition() {
		return ( true == isset( $this->m_strMaintenanceEmergencyRoutingCondition ) ) ? '\'' . addslashes( $this->m_strMaintenanceEmergencyRoutingCondition ) . '\'' : 'NULL';
	}

	public function setIsGuestCardRoutingToPsi( $intIsGuestCardRoutingToPsi ) {
		$this->set( 'm_intIsGuestCardRoutingToPsi', CStrings::strToIntDef( $intIsGuestCardRoutingToPsi, NULL, false ) );
	}

	public function getIsGuestCardRoutingToPsi() {
		return $this->m_intIsGuestCardRoutingToPsi;
	}

	public function sqlIsGuestCardRoutingToPsi() {
		return ( true == isset( $this->m_intIsGuestCardRoutingToPsi ) ) ? ( string ) $this->m_intIsGuestCardRoutingToPsi : '0';
	}

	public function setSecondsToRingPhone( $intSecondsToRingPhone ) {
		$this->set( 'm_intSecondsToRingPhone', CStrings::strToIntDef( $intSecondsToRingPhone, NULL, false ) );
	}

	public function getSecondsToRingPhone() {
		return $this->m_intSecondsToRingPhone;
	}

	public function sqlSecondsToRingPhone() {
		return ( true == isset( $this->m_intSecondsToRingPhone ) ) ? ( string ) $this->m_intSecondsToRingPhone : 'NULL';
	}

	public function setIsSendVoicemailToProperty( $intIsSendVoicemailToProperty ) {
		$this->set( 'm_intIsSendVoicemailToProperty', CStrings::strToIntDef( $intIsSendVoicemailToProperty, NULL, false ) );
	}

	public function getIsSendVoicemailToProperty() {
		return $this->m_intIsSendVoicemailToProperty;
	}

	public function sqlIsSendVoicemailToProperty() {
		return ( true == isset( $this->m_intIsSendVoicemailToProperty ) ) ? ( string ) $this->m_intIsSendVoicemailToProperty : '0';
	}

	public function setUsePsiVoicemail( $intUsePsiVoicemail ) {
		$this->set( 'm_intUsePsiVoicemail', CStrings::strToIntDef( $intUsePsiVoicemail, NULL, false ) );
	}

	public function getUsePsiVoicemail() {
		return $this->m_intUsePsiVoicemail;
	}

	public function sqlUsePsiVoicemail() {
		return ( true == isset( $this->m_intUsePsiVoicemail ) ) ? ( string ) $this->m_intUsePsiVoicemail : 'NULL';
	}

	public function setTimezoneOffset( $intTimezoneOffset ) {
		$this->set( 'm_intTimezoneOffset', CStrings::strToIntDef( $intTimezoneOffset, NULL, false ) );
	}

	public function getTimezoneOffset() {
		return $this->m_intTimezoneOffset;
	}

	public function sqlTimezoneOffset() {
		return ( true == isset( $this->m_intTimezoneOffset ) ) ? ( string ) $this->m_intTimezoneOffset : 'NULL';
	}

	public function setIsAutoArchiveCall( $intIsAutoArchiveCall ) {
		$this->set( 'm_intIsAutoArchiveCall', CStrings::strToIntDef( $intIsAutoArchiveCall, NULL, false ) );
	}

	public function getIsAutoArchiveCall() {
		return $this->m_intIsAutoArchiveCall;
	}

	public function sqlIsAutoArchiveCall() {
		return ( true == isset( $this->m_intIsAutoArchiveCall ) ) ? ( string ) $this->m_intIsAutoArchiveCall : '0';
	}

	public function setIsOfficeForwarding( $intIsOfficeForwarding ) {
		$this->set( 'm_intIsOfficeForwarding', CStrings::strToIntDef( $intIsOfficeForwarding, NULL, false ) );
	}

	public function getIsOfficeForwarding() {
		return $this->m_intIsOfficeForwarding;
	}

	public function sqlIsOfficeForwarding() {
		return ( true == isset( $this->m_intIsOfficeForwarding ) ) ? ( string ) $this->m_intIsOfficeForwarding : '0';
	}

	public function setIsCalendarEnabled( $intIsCalendarEnabled ) {
		$this->set( 'm_intIsCalendarEnabled', CStrings::strToIntDef( $intIsCalendarEnabled, NULL, false ) );
	}

	public function getIsCalendarEnabled() {
		return $this->m_intIsCalendarEnabled;
	}

	public function sqlIsCalendarEnabled() {
		return ( true == isset( $this->m_intIsCalendarEnabled ) ) ? ( string ) $this->m_intIsCalendarEnabled : '0';
	}

	public function setIsActiveNewDashboard( $boolIsActiveNewDashboard ) {
		$this->set( 'm_boolIsActiveNewDashboard', CStrings::strToBool( $boolIsActiveNewDashboard ) );
	}

	public function getIsActiveNewDashboard() {
		return $this->m_boolIsActiveNewDashboard;
	}

	public function sqlIsActiveNewDashboard() {
		return ( true == isset( $this->m_boolIsActiveNewDashboard ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActiveNewDashboard ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSunOpenTime( $strSunOpenTime ) {
		$this->set( 'm_strSunOpenTime', CStrings::strTrimDef( $strSunOpenTime, -1, NULL, true ) );
	}

	public function getSunOpenTime() {
		return $this->m_strSunOpenTime;
	}

	public function sqlSunOpenTime() {
		return ( true == isset( $this->m_strSunOpenTime ) ) ? '\'' . $this->m_strSunOpenTime . '\'' : 'NULL';
	}

	public function setSunCloseTime( $strSunCloseTime ) {
		$this->set( 'm_strSunCloseTime', CStrings::strTrimDef( $strSunCloseTime, -1, NULL, true ) );
	}

	public function getSunCloseTime() {
		return $this->m_strSunCloseTime;
	}

	public function sqlSunCloseTime() {
		return ( true == isset( $this->m_strSunCloseTime ) ) ? '\'' . $this->m_strSunCloseTime . '\'' : 'NULL';
	}

	public function setMonOpenTime( $strMonOpenTime ) {
		$this->set( 'm_strMonOpenTime', CStrings::strTrimDef( $strMonOpenTime, -1, NULL, true ) );
	}

	public function getMonOpenTime() {
		return $this->m_strMonOpenTime;
	}

	public function sqlMonOpenTime() {
		return ( true == isset( $this->m_strMonOpenTime ) ) ? '\'' . $this->m_strMonOpenTime . '\'' : 'NULL';
	}

	public function setMonCloseTime( $strMonCloseTime ) {
		$this->set( 'm_strMonCloseTime', CStrings::strTrimDef( $strMonCloseTime, -1, NULL, true ) );
	}

	public function getMonCloseTime() {
		return $this->m_strMonCloseTime;
	}

	public function sqlMonCloseTime() {
		return ( true == isset( $this->m_strMonCloseTime ) ) ? '\'' . $this->m_strMonCloseTime . '\'' : 'NULL';
	}

	public function setTueOpenTime( $strTueOpenTime ) {
		$this->set( 'm_strTueOpenTime', CStrings::strTrimDef( $strTueOpenTime, -1, NULL, true ) );
	}

	public function getTueOpenTime() {
		return $this->m_strTueOpenTime;
	}

	public function sqlTueOpenTime() {
		return ( true == isset( $this->m_strTueOpenTime ) ) ? '\'' . $this->m_strTueOpenTime . '\'' : 'NULL';
	}

	public function setTueCloseTime( $strTueCloseTime ) {
		$this->set( 'm_strTueCloseTime', CStrings::strTrimDef( $strTueCloseTime, -1, NULL, true ) );
	}

	public function getTueCloseTime() {
		return $this->m_strTueCloseTime;
	}

	public function sqlTueCloseTime() {
		return ( true == isset( $this->m_strTueCloseTime ) ) ? '\'' . $this->m_strTueCloseTime . '\'' : 'NULL';
	}

	public function setWedOpenTime( $strWedOpenTime ) {
		$this->set( 'm_strWedOpenTime', CStrings::strTrimDef( $strWedOpenTime, -1, NULL, true ) );
	}

	public function getWedOpenTime() {
		return $this->m_strWedOpenTime;
	}

	public function sqlWedOpenTime() {
		return ( true == isset( $this->m_strWedOpenTime ) ) ? '\'' . $this->m_strWedOpenTime . '\'' : 'NULL';
	}

	public function setWedCloseTime( $strWedCloseTime ) {
		$this->set( 'm_strWedCloseTime', CStrings::strTrimDef( $strWedCloseTime, -1, NULL, true ) );
	}

	public function getWedCloseTime() {
		return $this->m_strWedCloseTime;
	}

	public function sqlWedCloseTime() {
		return ( true == isset( $this->m_strWedCloseTime ) ) ? '\'' . $this->m_strWedCloseTime . '\'' : 'NULL';
	}

	public function setThuOpenTime( $strThuOpenTime ) {
		$this->set( 'm_strThuOpenTime', CStrings::strTrimDef( $strThuOpenTime, -1, NULL, true ) );
	}

	public function getThuOpenTime() {
		return $this->m_strThuOpenTime;
	}

	public function sqlThuOpenTime() {
		return ( true == isset( $this->m_strThuOpenTime ) ) ? '\'' . $this->m_strThuOpenTime . '\'' : 'NULL';
	}

	public function setThuCloseTime( $strThuCloseTime ) {
		$this->set( 'm_strThuCloseTime', CStrings::strTrimDef( $strThuCloseTime, -1, NULL, true ) );
	}

	public function getThuCloseTime() {
		return $this->m_strThuCloseTime;
	}

	public function sqlThuCloseTime() {
		return ( true == isset( $this->m_strThuCloseTime ) ) ? '\'' . $this->m_strThuCloseTime . '\'' : 'NULL';
	}

	public function setFriOpenTime( $strFriOpenTime ) {
		$this->set( 'm_strFriOpenTime', CStrings::strTrimDef( $strFriOpenTime, -1, NULL, true ) );
	}

	public function getFriOpenTime() {
		return $this->m_strFriOpenTime;
	}

	public function sqlFriOpenTime() {
		return ( true == isset( $this->m_strFriOpenTime ) ) ? '\'' . $this->m_strFriOpenTime . '\'' : 'NULL';
	}

	public function setFriCloseTime( $strFriCloseTime ) {
		$this->set( 'm_strFriCloseTime', CStrings::strTrimDef( $strFriCloseTime, -1, NULL, true ) );
	}

	public function getFriCloseTime() {
		return $this->m_strFriCloseTime;
	}

	public function sqlFriCloseTime() {
		return ( true == isset( $this->m_strFriCloseTime ) ) ? '\'' . $this->m_strFriCloseTime . '\'' : 'NULL';
	}

	public function setSatOpenTime( $strSatOpenTime ) {
		$this->set( 'm_strSatOpenTime', CStrings::strTrimDef( $strSatOpenTime, -1, NULL, true ) );
	}

	public function getSatOpenTime() {
		return $this->m_strSatOpenTime;
	}

	public function sqlSatOpenTime() {
		return ( true == isset( $this->m_strSatOpenTime ) ) ? '\'' . $this->m_strSatOpenTime . '\'' : 'NULL';
	}

	public function setSatCloseTime( $strSatCloseTime ) {
		$this->set( 'm_strSatCloseTime', CStrings::strTrimDef( $strSatCloseTime, -1, NULL, true ) );
	}

	public function getSatCloseTime() {
		return $this->m_strSatCloseTime;
	}

	public function sqlSatCloseTime() {
		return ( true == isset( $this->m_strSatCloseTime ) ) ? '\'' . $this->m_strSatCloseTime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, settings_template_id, time_zone_id, property_name, marketing_property_name, office_phone_number, maintenance_phone_number, maintenance_emergency_phone_number, after_hours_office_phone_number, after_hours_maintenance_phone_number, after_hours_maintenance_emergency_phone_number, lead_routing_condition, maintenance_routing_condition, maintenance_emergency_routing_condition, is_guest_card_routing_to_psi, seconds_to_ring_phone, is_send_voicemail_to_property, use_psi_voicemail, timezone_offset, is_auto_archive_call, is_office_forwarding, is_calendar_enabled, is_active_new_dashboard, sun_open_time, sun_close_time, mon_open_time, mon_close_time, tue_open_time, tue_close_time, wed_open_time, wed_close_time, thu_open_time, thu_close_time, fri_open_time, fri_close_time, sat_open_time, sat_close_time, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlSettingsTemplateId() . ', ' .
		          $this->sqlTimeZoneId() . ', ' .
		          $this->sqlPropertyName() . ', ' .
		          $this->sqlMarketingPropertyName() . ', ' .
		          $this->sqlOfficePhoneNumber() . ', ' .
		          $this->sqlMaintenancePhoneNumber() . ', ' .
		          $this->sqlMaintenanceEmergencyPhoneNumber() . ', ' .
		          $this->sqlAfterHoursOfficePhoneNumber() . ', ' .
		          $this->sqlAfterHoursMaintenancePhoneNumber() . ', ' .
		          $this->sqlAfterHoursMaintenanceEmergencyPhoneNumber() . ', ' .
		          $this->sqlLeadRoutingCondition() . ', ' .
		          $this->sqlMaintenanceRoutingCondition() . ', ' .
		          $this->sqlMaintenanceEmergencyRoutingCondition() . ', ' .
		          $this->sqlIsGuestCardRoutingToPsi() . ', ' .
		          $this->sqlSecondsToRingPhone() . ', ' .
		          $this->sqlIsSendVoicemailToProperty() . ', ' .
		          $this->sqlUsePsiVoicemail() . ', ' .
		          $this->sqlTimezoneOffset() . ', ' .
		          $this->sqlIsAutoArchiveCall() . ', ' .
		          $this->sqlIsOfficeForwarding() . ', ' .
		          $this->sqlIsCalendarEnabled() . ', ' .
		          $this->sqlIsActiveNewDashboard() . ', ' .
		          $this->sqlSunOpenTime() . ', ' .
		          $this->sqlSunCloseTime() . ', ' .
		          $this->sqlMonOpenTime() . ', ' .
		          $this->sqlMonCloseTime() . ', ' .
		          $this->sqlTueOpenTime() . ', ' .
		          $this->sqlTueCloseTime() . ', ' .
		          $this->sqlWedOpenTime() . ', ' .
		          $this->sqlWedCloseTime() . ', ' .
		          $this->sqlThuOpenTime() . ', ' .
		          $this->sqlThuCloseTime() . ', ' .
		          $this->sqlFriOpenTime() . ', ' .
		          $this->sqlFriCloseTime() . ', ' .
		          $this->sqlSatOpenTime() . ', ' .
		          $this->sqlSatCloseTime() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settings_template_id = ' . $this->sqlSettingsTemplateId() . ','; } elseif( true == array_key_exists( 'SettingsTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' settings_template_id = ' . $this->sqlSettingsTemplateId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; } elseif( true == array_key_exists( 'TimeZoneId', $this->getChangedColumns() ) ) { $strSql .= ' time_zone_id = ' . $this->sqlTimeZoneId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_property_name = ' . $this->sqlMarketingPropertyName() . ','; } elseif( true == array_key_exists( 'MarketingPropertyName', $this->getChangedColumns() ) ) { $strSql .= ' marketing_property_name = ' . $this->sqlMarketingPropertyName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_phone_number = ' . $this->sqlOfficePhoneNumber() . ','; } elseif( true == array_key_exists( 'OfficePhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' office_phone_number = ' . $this->sqlOfficePhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_phone_number = ' . $this->sqlMaintenancePhoneNumber() . ','; } elseif( true == array_key_exists( 'MaintenancePhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_phone_number = ' . $this->sqlMaintenancePhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_emergency_phone_number = ' . $this->sqlMaintenanceEmergencyPhoneNumber() . ','; } elseif( true == array_key_exists( 'MaintenanceEmergencyPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_emergency_phone_number = ' . $this->sqlMaintenanceEmergencyPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' after_hours_office_phone_number = ' . $this->sqlAfterHoursOfficePhoneNumber() . ','; } elseif( true == array_key_exists( 'AfterHoursOfficePhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' after_hours_office_phone_number = ' . $this->sqlAfterHoursOfficePhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' after_hours_maintenance_phone_number = ' . $this->sqlAfterHoursMaintenancePhoneNumber() . ','; } elseif( true == array_key_exists( 'AfterHoursMaintenancePhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' after_hours_maintenance_phone_number = ' . $this->sqlAfterHoursMaintenancePhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' after_hours_maintenance_emergency_phone_number = ' . $this->sqlAfterHoursMaintenanceEmergencyPhoneNumber() . ','; } elseif( true == array_key_exists( 'AfterHoursMaintenanceEmergencyPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' after_hours_maintenance_emergency_phone_number = ' . $this->sqlAfterHoursMaintenanceEmergencyPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_routing_condition = ' . $this->sqlLeadRoutingCondition() . ','; } elseif( true == array_key_exists( 'LeadRoutingCondition', $this->getChangedColumns() ) ) { $strSql .= ' lead_routing_condition = ' . $this->sqlLeadRoutingCondition() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_routing_condition = ' . $this->sqlMaintenanceRoutingCondition() . ','; } elseif( true == array_key_exists( 'MaintenanceRoutingCondition', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_routing_condition = ' . $this->sqlMaintenanceRoutingCondition() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_emergency_routing_condition = ' . $this->sqlMaintenanceEmergencyRoutingCondition() . ','; } elseif( true == array_key_exists( 'MaintenanceEmergencyRoutingCondition', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_emergency_routing_condition = ' . $this->sqlMaintenanceEmergencyRoutingCondition() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_guest_card_routing_to_psi = ' . $this->sqlIsGuestCardRoutingToPsi() . ','; } elseif( true == array_key_exists( 'IsGuestCardRoutingToPsi', $this->getChangedColumns() ) ) { $strSql .= ' is_guest_card_routing_to_psi = ' . $this->sqlIsGuestCardRoutingToPsi() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seconds_to_ring_phone = ' . $this->sqlSecondsToRingPhone() . ','; } elseif( true == array_key_exists( 'SecondsToRingPhone', $this->getChangedColumns() ) ) { $strSql .= ' seconds_to_ring_phone = ' . $this->sqlSecondsToRingPhone() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_voicemail_to_property = ' . $this->sqlIsSendVoicemailToProperty() . ','; } elseif( true == array_key_exists( 'IsSendVoicemailToProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_send_voicemail_to_property = ' . $this->sqlIsSendVoicemailToProperty() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_psi_voicemail = ' . $this->sqlUsePsiVoicemail() . ','; } elseif( true == array_key_exists( 'UsePsiVoicemail', $this->getChangedColumns() ) ) { $strSql .= ' use_psi_voicemail = ' . $this->sqlUsePsiVoicemail() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' timezone_offset = ' . $this->sqlTimezoneOffset() . ','; } elseif( true == array_key_exists( 'TimezoneOffset', $this->getChangedColumns() ) ) { $strSql .= ' timezone_offset = ' . $this->sqlTimezoneOffset() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_archive_call = ' . $this->sqlIsAutoArchiveCall() . ','; } elseif( true == array_key_exists( 'IsAutoArchiveCall', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_archive_call = ' . $this->sqlIsAutoArchiveCall() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_office_forwarding = ' . $this->sqlIsOfficeForwarding() . ','; } elseif( true == array_key_exists( 'IsOfficeForwarding', $this->getChangedColumns() ) ) { $strSql .= ' is_office_forwarding = ' . $this->sqlIsOfficeForwarding() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_calendar_enabled = ' . $this->sqlIsCalendarEnabled() . ','; } elseif( true == array_key_exists( 'IsCalendarEnabled', $this->getChangedColumns() ) ) { $strSql .= ' is_calendar_enabled = ' . $this->sqlIsCalendarEnabled() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active_new_dashboard = ' . $this->sqlIsActiveNewDashboard() . ','; } elseif( true == array_key_exists( 'IsActiveNewDashboard', $this->getChangedColumns() ) ) { $strSql .= ' is_active_new_dashboard = ' . $this->sqlIsActiveNewDashboard() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; } elseif( true == array_key_exists( 'SunOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_open_time = ' . $this->sqlSunOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; } elseif( true == array_key_exists( 'SunCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sun_close_time = ' . $this->sqlSunCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mon_open_time = ' . $this->sqlMonOpenTime() . ','; } elseif( true == array_key_exists( 'MonOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' mon_open_time = ' . $this->sqlMonOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mon_close_time = ' . $this->sqlMonCloseTime() . ','; } elseif( true == array_key_exists( 'MonCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' mon_close_time = ' . $this->sqlMonCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tue_open_time = ' . $this->sqlTueOpenTime() . ','; } elseif( true == array_key_exists( 'TueOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' tue_open_time = ' . $this->sqlTueOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tue_close_time = ' . $this->sqlTueCloseTime() . ','; } elseif( true == array_key_exists( 'TueCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' tue_close_time = ' . $this->sqlTueCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wed_open_time = ' . $this->sqlWedOpenTime() . ','; } elseif( true == array_key_exists( 'WedOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' wed_open_time = ' . $this->sqlWedOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wed_close_time = ' . $this->sqlWedCloseTime() . ','; } elseif( true == array_key_exists( 'WedCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' wed_close_time = ' . $this->sqlWedCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thu_open_time = ' . $this->sqlThuOpenTime() . ','; } elseif( true == array_key_exists( 'ThuOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' thu_open_time = ' . $this->sqlThuOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' thu_close_time = ' . $this->sqlThuCloseTime() . ','; } elseif( true == array_key_exists( 'ThuCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' thu_close_time = ' . $this->sqlThuCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fri_open_time = ' . $this->sqlFriOpenTime() . ','; } elseif( true == array_key_exists( 'FriOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' fri_open_time = ' . $this->sqlFriOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fri_close_time = ' . $this->sqlFriCloseTime() . ','; } elseif( true == array_key_exists( 'FriCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' fri_close_time = ' . $this->sqlFriCloseTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; } elseif( true == array_key_exists( 'SatOpenTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_open_time = ' . $this->sqlSatOpenTime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; } elseif( true == array_key_exists( 'SatCloseTime', $this->getChangedColumns() ) ) { $strSql .= ' sat_close_time = ' . $this->sqlSatCloseTime() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'settings_template_id' => $this->getSettingsTemplateId(),
			'time_zone_id' => $this->getTimeZoneId(),
			'property_name' => $this->getPropertyName(),
			'marketing_property_name' => $this->getMarketingPropertyName(),
			'office_phone_number' => $this->getOfficePhoneNumber(),
			'maintenance_phone_number' => $this->getMaintenancePhoneNumber(),
			'maintenance_emergency_phone_number' => $this->getMaintenanceEmergencyPhoneNumber(),
			'after_hours_office_phone_number' => $this->getAfterHoursOfficePhoneNumber(),
			'after_hours_maintenance_phone_number' => $this->getAfterHoursMaintenancePhoneNumber(),
			'after_hours_maintenance_emergency_phone_number' => $this->getAfterHoursMaintenanceEmergencyPhoneNumber(),
			'lead_routing_condition' => $this->getLeadRoutingCondition(),
			'maintenance_routing_condition' => $this->getMaintenanceRoutingCondition(),
			'maintenance_emergency_routing_condition' => $this->getMaintenanceEmergencyRoutingCondition(),
			'is_guest_card_routing_to_psi' => $this->getIsGuestCardRoutingToPsi(),
			'seconds_to_ring_phone' => $this->getSecondsToRingPhone(),
			'is_send_voicemail_to_property' => $this->getIsSendVoicemailToProperty(),
			'use_psi_voicemail' => $this->getUsePsiVoicemail(),
			'timezone_offset' => $this->getTimezoneOffset(),
			'is_auto_archive_call' => $this->getIsAutoArchiveCall(),
			'is_office_forwarding' => $this->getIsOfficeForwarding(),
			'is_calendar_enabled' => $this->getIsCalendarEnabled(),
			'is_active_new_dashboard' => $this->getIsActiveNewDashboard(),
			'sun_open_time' => $this->getSunOpenTime(),
			'sun_close_time' => $this->getSunCloseTime(),
			'mon_open_time' => $this->getMonOpenTime(),
			'mon_close_time' => $this->getMonCloseTime(),
			'tue_open_time' => $this->getTueOpenTime(),
			'tue_close_time' => $this->getTueCloseTime(),
			'wed_open_time' => $this->getWedOpenTime(),
			'wed_close_time' => $this->getWedCloseTime(),
			'thu_open_time' => $this->getThuOpenTime(),
			'thu_close_time' => $this->getThuCloseTime(),
			'fri_open_time' => $this->getFriOpenTime(),
			'fri_close_time' => $this->getFriCloseTime(),
			'sat_open_time' => $this->getSatOpenTime(),
			'sat_close_time' => $this->getSatCloseTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>