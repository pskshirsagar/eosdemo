<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServiceProviders
 * Do not add any new functions to this class.
 */

class CBaseCallServiceProviders extends CEosPluralBase {

	/**
	 * @return CCallServiceProvider[]
	 */
	public static function fetchCallServiceProviders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallServiceProvider', $objDatabase );
	}

	/**
	 * @return CCallServiceProvider
	 */
	public static function fetchCallServiceProvider( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallServiceProvider', $objDatabase );
	}

	public static function fetchCallServiceProviderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_service_providers', $objDatabase );
	}

	public static function fetchCallServiceProviderById( $intId, $objDatabase ) {
		return self::fetchCallServiceProvider( sprintf( 'SELECT * FROM call_service_providers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>