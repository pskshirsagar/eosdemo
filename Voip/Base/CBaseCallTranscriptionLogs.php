<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTranscriptionLogs
 * Do not add any new functions to this class.
 */

class CBaseCallTranscriptionLogs extends CEosPluralBase {

	/**
	 * @return CCallTranscriptionLog[]
	 */
	public static function fetchCallTranscriptionLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallTranscriptionLog::class, $objDatabase );
	}

	/**
	 * @return CCallTranscriptionLog
	 */
	public static function fetchCallTranscriptionLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallTranscriptionLog::class, $objDatabase );
	}

	public static function fetchCallTranscriptionLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_transcription_logs', $objDatabase );
	}

	public static function fetchCallTranscriptionLogById( $intId, $objDatabase ) {
		return self::fetchCallTranscriptionLog( sprintf( 'SELECT * FROM call_transcription_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallTranscriptionLogsByCid( $intCid, $objDatabase ) {
		return self::fetchCallTranscriptionLogs( sprintf( 'SELECT * FROM call_transcription_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTranscriptionLogsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallTranscriptionLogs( sprintf( 'SELECT * FROM call_transcription_logs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallTranscriptionLogsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallTranscriptionLogs( sprintf( 'SELECT * FROM call_transcription_logs WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallTranscriptionLogsByOperationId( $strOperationId, $objDatabase ) {
		return self::fetchCallTranscriptionLogs( sprintf( 'SELECT * FROM call_transcription_logs WHERE operation_id = \'%s\'', $strOperationId ), $objDatabase );
	}

}
?>