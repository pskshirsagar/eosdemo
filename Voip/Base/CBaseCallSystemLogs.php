<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSystemLogs
 * Do not add any new functions to this class.
 */

class CBaseCallSystemLogs extends CEosPluralBase {

	/**
	 * @return CCallSystemLog[]
	 */
	public static function fetchCallSystemLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallSystemLog', $objDatabase );
	}

	/**
	 * @return CCallSystemLog
	 */
	public static function fetchCallSystemLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallSystemLog', $objDatabase );
	}

	public static function fetchCallSystemLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_system_logs', $objDatabase );
	}

	public static function fetchCallSystemLogById( $intId, $objDatabase ) {
		return self::fetchCallSystemLog( sprintf( 'SELECT * FROM call_system_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallSystemLogsByCallComponentId( $intCallComponentId, $objDatabase ) {
		return self::fetchCallSystemLogs( sprintf( 'SELECT * FROM call_system_logs WHERE call_component_id = %d', ( int ) $intCallComponentId ), $objDatabase );
	}

}
?>