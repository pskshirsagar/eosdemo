<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentPinGroups
 * Do not add any new functions to this class.
 */

class CBaseCallAgentPinGroups extends CEosPluralBase {

	/**
	 * @return CCallAgentPinGroup[]
	 */
	public static function fetchCallAgentPinGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentPinGroup::class, $objDatabase );
	}

	/**
	 * @return CCallAgentPinGroup
	 */
	public static function fetchCallAgentPinGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentPinGroup::class, $objDatabase );
	}

	public static function fetchCallAgentPinGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_pin_groups', $objDatabase );
	}

	public static function fetchCallAgentPinGroupById( $intId, $objDatabase ) {
		return self::fetchCallAgentPinGroup( sprintf( 'SELECT * FROM call_agent_pin_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>