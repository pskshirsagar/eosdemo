<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallResults
 * Do not add any new functions to this class.
 */

class CBaseCallResults extends CEosPluralBase {

	/**
	 * @return CCallResult[]
	 */
	public static function fetchCallResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallResult::class, $objDatabase );
	}

	/**
	 * @return CCallResult
	 */
	public static function fetchCallResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallResult::class, $objDatabase );
	}

	public static function fetchCallResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_results', $objDatabase );
	}

	public static function fetchCallResultById( $intId, $objDatabase ) {
		return self::fetchCallResult( sprintf( 'SELECT * FROM call_results WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallResultsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchCallResults( sprintf( 'SELECT * FROM call_results WHERE call_type_id = %d', $intCallTypeId ), $objDatabase );
	}

	public static function fetchCallResultsByCallResultId( $intCallResultId, $objDatabase ) {
		return self::fetchCallResults( sprintf( 'SELECT * FROM call_results WHERE call_result_id = %d', $intCallResultId ), $objDatabase );
	}

	public static function fetchCallResultsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchCallResults( sprintf( 'SELECT * FROM call_results WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

}
?>