<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgents
 * Do not add any new functions to this class.
 */

class CBaseCallAgents extends CEosPluralBase {

	/**
	 * @return CCallAgent[]
	 */
	public static function fetchCallAgents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgent::class, $objDatabase );
	}

	/**
	 * @return CCallAgent
	 */
	public static function fetchCallAgent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgent::class, $objDatabase );
	}

	public static function fetchCallAgentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agents', $objDatabase );
	}

	public static function fetchCallAgentById( $intId, $objDatabase ) {
		return self::fetchCallAgent( sprintf( 'SELECT * FROM call_agents WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAgentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE department_id = %d', $intDepartmentId ), $objDatabase );
	}

	public static function fetchCallAgentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallAgentsByUserId( $intUserId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE user_id = %d', $intUserId ), $objDatabase );
	}

	public static function fetchCallAgentsByCallAgentStatusTypeId( $intCallAgentStatusTypeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE call_agent_status_type_id = %d', $intCallAgentStatusTypeId ), $objDatabase );
	}

	public static function fetchCallAgentsByCallAgentStateTypeId( $intCallAgentStateTypeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE call_agent_state_type_id = %d', $intCallAgentStateTypeId ), $objDatabase );
	}

	public static function fetchCallAgentsByAgentPhoneExtensionId( $intAgentPhoneExtensionId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE agent_phone_extension_id = %d', $intAgentPhoneExtensionId ), $objDatabase );
	}

	public static function fetchCallAgentsByDeskPhoneExtensionId( $intDeskPhoneExtensionId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE desk_phone_extension_id = %d', $intDeskPhoneExtensionId ), $objDatabase );
	}

	public static function fetchCallAgentsByPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE physical_phone_extension_id = %d', $intPhysicalPhoneExtensionId ), $objDatabase );
	}

	public static function fetchCallAgentsByLastOfferedCallId( $intLastOfferedCallId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE last_offered_call_id = %d', $intLastOfferedCallId ), $objDatabase );
	}

	public static function fetchCallAgentsByOfferedCallQueueId( $intOfferedCallQueueId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE offered_call_queue_id = %d', $intOfferedCallQueueId ), $objDatabase );
	}

	public static function fetchCallAgentsByNextCallAgentStatusTypeId( $intNextCallAgentStatusTypeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE next_call_agent_status_type_id = %d', $intNextCallAgentStatusTypeId ), $objDatabase );
	}

	public static function fetchCallAgentsByNextCallAgentStateTypeId( $intNextCallAgentStateTypeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE next_call_agent_state_type_id = %d', $intNextCallAgentStateTypeId ), $objDatabase );
	}

	public static function fetchCallAgentsByOfficeId( $intOfficeId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE office_id = %d', $intOfficeId ), $objDatabase );
	}

	public static function fetchCallAgentsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE designation_id = %d', $intDesignationId ), $objDatabase );
	}

	public static function fetchCallAgentsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchCallAgents( sprintf( 'SELECT * FROM call_agents WHERE team_id = %d', $intTeamId ), $objDatabase );
	}

}
?>