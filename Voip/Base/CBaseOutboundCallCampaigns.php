<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallCampaigns
 * Do not add any new functions to this class.
 */

class CBaseOutboundCallCampaigns extends CEosPluralBase {

	/**
	 * @return COutboundCallCampaign[]
	 */
	public static function fetchOutboundCallCampaigns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COutboundCallCampaign', $objDatabase );
	}

	/**
	 * @return COutboundCallCampaign
	 */
	public static function fetchOutboundCallCampaign( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COutboundCallCampaign', $objDatabase );
	}

	public static function fetchOutboundCallCampaignCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'outbound_call_campaigns', $objDatabase );
	}

	public static function fetchOutboundCallCampaignById( $intId, $objDatabase ) {
		return self::fetchOutboundCallCampaign( sprintf( 'SELECT * FROM outbound_call_campaigns WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOutboundCallCampaignsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchOutboundCallCampaigns( sprintf( 'SELECT * FROM outbound_call_campaigns WHERE call_type_id = %d', ( int ) $intCallTypeId ), $objDatabase );
	}

}
?>