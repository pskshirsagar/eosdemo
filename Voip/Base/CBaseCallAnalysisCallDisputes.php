<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCallDisputes
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisCallDisputes extends CEosPluralBase {

	/**
	 * @return CCallAnalysisCallDispute[]
	 */
	public static function fetchCallAnalysisCallDisputes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisCallDispute::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisCallDispute
	 */
	public static function fetchCallAnalysisCallDispute( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisCallDispute::class, $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_call_disputes', $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputeById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisCallDispute( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputesByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputes( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputes( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputesByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputes( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE call_id = %d', $intCallId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputesByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputes( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE company_user_id = %d', $intCompanyUserId ), $objDatabase );
	}

	public static function fetchCallAnalysisCallDisputesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAnalysisCallDisputes( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

}
?>