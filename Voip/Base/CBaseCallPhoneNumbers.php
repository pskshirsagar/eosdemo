<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseCallPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CCallPhoneNumber[]
	 */
	public static function fetchCallPhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallPhoneNumber::class, $objDatabase );
	}

	/**
	 * @return CCallPhoneNumber
	 */
	public static function fetchCallPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallPhoneNumber::class, $objDatabase );
	}

	public static function fetchCallPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_phone_numbers', $objDatabase );
	}

	public static function fetchCallPhoneNumberById( $intId, $objDatabase ) {
		return self::fetchCallPhoneNumber( sprintf( 'SELECT * FROM call_phone_numbers WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByCallServiceProviderId( $intCallServiceProviderId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE call_service_provider_id = %d', $intCallServiceProviderId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE call_type_id = %d', $intCallTypeId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByCallForwardPreferenceId( $intCallForwardPreferenceId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE call_forward_preference_id = %d', $intCallForwardPreferenceId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE call_queue_id = %d', $intCallQueueId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByPhoneExtensionId( $intPhoneExtensionId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE phone_extension_id = %d', $intPhoneExtensionId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByDialplanId( $intDialplanId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE dialplan_id = %d', $intDialplanId ), $objDatabase );
	}

	public static function fetchCallPhoneNumbersByIvrId( $intIvrId, $objDatabase ) {
		return self::fetchCallPhoneNumbers( sprintf( 'SELECT * FROM call_phone_numbers WHERE ivr_id = %d', $intIvrId ), $objDatabase );
	}

}
?>