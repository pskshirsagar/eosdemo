<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTranscriptionClasses
 * Do not add any new functions to this class.
 */

class CBaseCallTranscriptionClasses extends CEosPluralBase {

	/**
	 * @return CCallTranscriptionClass[]
	 */
	public static function fetchCallTranscriptionClasses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallTranscriptionClass::class, $objDatabase );
	}

	/**
	 * @return CCallTranscriptionClass
	 */
	public static function fetchCallTranscriptionClass( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallTranscriptionClass::class, $objDatabase );
	}

	public static function fetchCallTranscriptionClassCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_transcription_classes', $objDatabase );
	}

	public static function fetchCallTranscriptionClassById( $intId, $objDatabase ) {
		return self::fetchCallTranscriptionClass( sprintf( 'SELECT * FROM call_transcription_classes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallTranscriptionClassesByCid( $intCid, $objDatabase ) {
		return self::fetchCallTranscriptionClasses( sprintf( 'SELECT * FROM call_transcription_classes WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallTranscriptionClassesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallTranscriptionClasses( sprintf( 'SELECT * FROM call_transcription_classes WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallTranscriptionClassesByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallTranscriptionClasses( sprintf( 'SELECT * FROM call_transcription_classes WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallTranscriptionClassesByApplicantApplicationId( $intApplicantApplicationId, $objDatabase ) {
		return self::fetchCallTranscriptionClasses( sprintf( 'SELECT * FROM call_transcription_classes WHERE applicant_application_id = %d', ( int ) $intApplicantApplicationId ), $objDatabase );
	}

}
?>