<?php

class CBaseCallAgentNote extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_notes';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intCallId;
	protected $m_intCallAgentLogId;
	protected $m_strNote;
	protected $m_boolIsPortfolio;
	protected $m_boolIsRedZoneWinner;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPortfolio = false;
		$this->m_boolIsRedZoneWinner = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['call_agent_log_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentLogId', trim( $arrValues['call_agent_log_id'] ) ); elseif( isset( $arrValues['call_agent_log_id'] ) ) $this->setCallAgentLogId( $arrValues['call_agent_log_id'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['is_portfolio'] ) && $boolDirectSet ) $this->set( 'm_boolIsPortfolio', trim( stripcslashes( $arrValues['is_portfolio'] ) ) ); elseif( isset( $arrValues['is_portfolio'] ) ) $this->setIsPortfolio( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_portfolio'] ) : $arrValues['is_portfolio'] );
		if( isset( $arrValues['is_red_zone_winner'] ) && $boolDirectSet ) $this->set( 'm_boolIsRedZoneWinner', trim( stripcslashes( $arrValues['is_red_zone_winner'] ) ) ); elseif( isset( $arrValues['is_red_zone_winner'] ) ) $this->setIsRedZoneWinner( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_red_zone_winner'] ) : $arrValues['is_red_zone_winner'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setCallAgentLogId( $intCallAgentLogId ) {
		$this->set( 'm_intCallAgentLogId', CStrings::strToIntDef( $intCallAgentLogId, NULL, false ) );
	}

	public function getCallAgentLogId() {
		return $this->m_intCallAgentLogId;
	}

	public function sqlCallAgentLogId() {
		return ( true == isset( $this->m_intCallAgentLogId ) ) ? ( string ) $this->m_intCallAgentLogId : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setIsPortfolio( $boolIsPortfolio ) {
		$this->set( 'm_boolIsPortfolio', CStrings::strToBool( $boolIsPortfolio ) );
	}

	public function getIsPortfolio() {
		return $this->m_boolIsPortfolio;
	}

	public function sqlIsPortfolio() {
		return ( true == isset( $this->m_boolIsPortfolio ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPortfolio ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRedZoneWinner( $boolIsRedZoneWinner ) {
		$this->set( 'm_boolIsRedZoneWinner', CStrings::strToBool( $boolIsRedZoneWinner ) );
	}

	public function getIsRedZoneWinner() {
		return $this->m_boolIsRedZoneWinner;
	}

	public function sqlIsRedZoneWinner() {
		return ( true == isset( $this->m_boolIsRedZoneWinner ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRedZoneWinner ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, call_id, call_agent_log_id, note, is_portfolio, is_red_zone_winner, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlCallAgentLogId() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlIsPortfolio() . ', ' .
 						$this->sqlIsRedZoneWinner() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_log_id = ' . $this->sqlCallAgentLogId() . ','; } elseif( true == array_key_exists( 'CallAgentLogId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_log_id = ' . $this->sqlCallAgentLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_portfolio = ' . $this->sqlIsPortfolio() . ','; } elseif( true == array_key_exists( 'IsPortfolio', $this->getChangedColumns() ) ) { $strSql .= ' is_portfolio = ' . $this->sqlIsPortfolio() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_red_zone_winner = ' . $this->sqlIsRedZoneWinner() . ','; } elseif( true == array_key_exists( 'IsRedZoneWinner', $this->getChangedColumns() ) ) { $strSql .= ' is_red_zone_winner = ' . $this->sqlIsRedZoneWinner() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_id' => $this->getCallId(),
			'call_agent_log_id' => $this->getCallAgentLogId(),
			'note' => $this->getNote(),
			'is_portfolio' => $this->getIsPortfolio(),
			'is_red_zone_winner' => $this->getIsRedZoneWinner(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>