<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CStoreProductRanks
 * Do not add any new functions to this class.
 */

class CBaseStoreProductRanks extends CEosPluralBase {

	/**
	 * @return CStoreProductRank[]
	 */
	public static function fetchStoreProductRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStoreProductRank::class, $objDatabase );
	}

	/**
	 * @return CStoreProductRank
	 */
	public static function fetchStoreProductRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStoreProductRank::class, $objDatabase );
	}

	public static function fetchStoreProductRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'store_product_ranks', $objDatabase );
	}

	public static function fetchStoreProductRankById( $intId, $objDatabase ) {
		return self::fetchStoreProductRank( sprintf( 'SELECT * FROM store_product_ranks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchStoreProductRanksByStoreProductId( $intStoreProductId, $objDatabase ) {
		return self::fetchStoreProductRanks( sprintf( 'SELECT * FROM store_product_ranks WHERE store_product_id = %d', $intStoreProductId ), $objDatabase );
	}

	public static function fetchStoreProductRanksByRankPointMappingId( $intRankPointMappingId, $objDatabase ) {
		return self::fetchStoreProductRanks( sprintf( 'SELECT * FROM store_product_ranks WHERE rank_point_mapping_id = %d', $intRankPointMappingId ), $objDatabase );
	}

}
?>