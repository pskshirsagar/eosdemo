<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenuTypes
 * Do not add any new functions to this class.
 */

class CBaseIvrMenuTypes extends CEosPluralBase {

	/**
	 * @return CIvrMenuType[]
	 */
	public static function fetchIvrMenuTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIvrMenuType::class, $objDatabase );
	}

	/**
	 * @return CIvrMenuType
	 */
	public static function fetchIvrMenuType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIvrMenuType::class, $objDatabase );
	}

	public static function fetchIvrMenuTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_menu_types', $objDatabase );
	}

	public static function fetchIvrMenuTypeById( $intId, $objDatabase ) {
		return self::fetchIvrMenuType( sprintf( 'SELECT * FROM ivr_menu_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>