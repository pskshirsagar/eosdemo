<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CRewardPointSettings
 * Do not add any new functions to this class.
 */

class CBaseRewardPointSettings extends CEosPluralBase {

	/**
	 * @return CRewardPointSetting[]
	 */
	public static function fetchRewardPointSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CRewardPointSetting::class, $objDatabase );
	}

	/**
	 * @return CRewardPointSetting
	 */
	public static function fetchRewardPointSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CRewardPointSetting::class, $objDatabase );
	}

	public static function fetchRewardPointSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reward_point_settings', $objDatabase );
	}

	public static function fetchRewardPointSettingById( $intId, $objDatabase ) {
		return self::fetchRewardPointSetting( sprintf( 'SELECT * FROM reward_point_settings WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchRewardPointSettingsByRewardPointTypeId( $intRewardPointTypeId, $objDatabase ) {
		return self::fetchRewardPointSettings( sprintf( 'SELECT * FROM reward_point_settings WHERE reward_point_type_id = %d', $intRewardPointTypeId ), $objDatabase );
	}

	public static function fetchRewardPointSettingsByRewardPointCategoryId( $intRewardPointCategoryId, $objDatabase ) {
		return self::fetchRewardPointSettings( sprintf( 'SELECT * FROM reward_point_settings WHERE reward_point_category_id = %d', $intRewardPointCategoryId ), $objDatabase );
	}

}
?>