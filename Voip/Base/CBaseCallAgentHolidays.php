<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentHolidays
 * Do not add any new functions to this class.
 */

class CBaseCallAgentHolidays extends CEosPluralBase {

	/**
	 * @return CCallAgentHoliday[]
	 */
	public static function fetchCallAgentHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentHoliday::class, $objDatabase );
	}

	/**
	 * @return CCallAgentHoliday
	 */
	public static function fetchCallAgentHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentHoliday::class, $objDatabase );
	}

	public static function fetchCallAgentHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_holidays', $objDatabase );
	}

	public static function fetchCallAgentHolidayById( $intId, $objDatabase ) {
		return self::fetchCallAgentHoliday( sprintf( 'SELECT * FROM call_agent_holidays WHERE id = %d', $intId ), $objDatabase );
	}

}
?>