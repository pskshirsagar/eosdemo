<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerTypes
 * Do not add any new functions to this class.
 */

class CBaseCallerTypes extends CEosPluralBase {

	/**
	 * @return CCallerType[]
	 */
	public static function fetchCallerTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallerType::class, $objDatabase );
	}

	/**
	 * @return CCallerType
	 */
	public static function fetchCallerType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallerType::class, $objDatabase );
	}

	public static function fetchCallerTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'caller_types', $objDatabase );
	}

	public static function fetchCallerTypeById( $intId, $objDatabase ) {
		return self::fetchCallerType( sprintf( 'SELECT * FROM caller_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>