<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallResults
 * Do not add any new functions to this class.
 */

class CBaseOutboundCallResults extends CEosPluralBase {

	/**
	 * @return COutboundCallResult[]
	 */
	public static function fetchOutboundCallResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COutboundCallResult::class, $objDatabase );
	}

	/**
	 * @return COutboundCallResult
	 */
	public static function fetchOutboundCallResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COutboundCallResult::class, $objDatabase );
	}

	public static function fetchOutboundCallResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'outbound_call_results', $objDatabase );
	}

	public static function fetchOutboundCallResultById( $intId, $objDatabase ) {
		return self::fetchOutboundCallResult( sprintf( 'SELECT * FROM outbound_call_results WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchOutboundCallResultsByCid( $intCid, $objDatabase ) {
		return self::fetchOutboundCallResults( sprintf( 'SELECT * FROM outbound_call_results WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchOutboundCallResultsByOutboundCallId( $intOutboundCallId, $objDatabase ) {
		return self::fetchOutboundCallResults( sprintf( 'SELECT * FROM outbound_call_results WHERE outbound_call_id = %d', $intOutboundCallId ), $objDatabase );
	}

	public static function fetchOutboundCallResultsByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchOutboundCallResults( sprintf( 'SELECT * FROM outbound_call_results WHERE call_type_id = %d', $intCallTypeId ), $objDatabase );
	}

	public static function fetchOutboundCallResultsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchOutboundCallResults( sprintf( 'SELECT * FROM outbound_call_results WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

	public static function fetchOutboundCallResultsByCallResultId( $intCallResultId, $objDatabase ) {
		return self::fetchOutboundCallResults( sprintf( 'SELECT * FROM outbound_call_results WHERE call_result_id = %d', $intCallResultId ), $objDatabase );
	}

}
?>