<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerAssociations
 * Do not add any new functions to this class.
 */

class CBaseCallerAssociations extends CEosPluralBase {

	/**
	 * @return CCallerAssociation[]
	 */
	public static function fetchCallerAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallerAssociation', $objDatabase );
	}

	/**
	 * @return CCallerAssociation
	 */
	public static function fetchCallerAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallerAssociation', $objDatabase );
	}

	public static function fetchCallerAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'caller_associations', $objDatabase );
	}

	public static function fetchCallerAssociationById( $intId, $objDatabase ) {
		return self::fetchCallerAssociation( sprintf( 'SELECT * FROM caller_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallerAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchCallerAssociations( sprintf( 'SELECT * FROM caller_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCallerAssociationsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCallerAssociations( sprintf( 'SELECT * FROM caller_associations WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchCallerAssociationsByCallerPhoneNumberId( $intCallerPhoneNumberId, $objDatabase ) {
		return self::fetchCallerAssociations( sprintf( 'SELECT * FROM caller_associations WHERE caller_phone_number_id = %d', ( int ) $intCallerPhoneNumberId ), $objDatabase );
	}

	public static function fetchCallerAssociationsByCallerId( $intCallerId, $objDatabase ) {
		return self::fetchCallerAssociations( sprintf( 'SELECT * FROM caller_associations WHERE caller_id = %d', ( int ) $intCallerId ), $objDatabase );
	}

}
?>