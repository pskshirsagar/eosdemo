<?php

class CBaseIvrMenu extends CEosSingularBase {

	const TABLE_NAME = 'public.ivr_menus';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intIvrId;
	protected $m_intIvrMenuTypeId;
	protected $m_intIvrMenuId;
	protected $m_strName;
	protected $m_strInvalidSound;
	protected $m_strExitSound;
	protected $m_strConfirmMacro;
	protected $m_strConfirmKey;
	protected $m_strTtsEngine;
	protected $m_strTimeout;
	protected $m_strInterDigitTimeout;
	protected $m_intMaxFailure;
	protected $m_intMaxTimeout;
	protected $m_intDigitLen;
	protected $m_strPhraseLang;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIvrMenuTypeId = '1';
		$this->m_strTtsEngine = 'cepstral';
		$this->m_strTimeout = '10000';
		$this->m_strInterDigitTimeout = '1000';
		$this->m_intMaxFailure = '3';
		$this->m_intMaxTimeout = '3';
		$this->m_intDigitLen = '4';
		$this->m_strPhraseLang = 'en';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ivr_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrId', trim( $arrValues['ivr_id'] ) ); elseif( isset( $arrValues['ivr_id'] ) ) $this->setIvrId( $arrValues['ivr_id'] );
		if( isset( $arrValues['ivr_menu_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuTypeId', trim( $arrValues['ivr_menu_type_id'] ) ); elseif( isset( $arrValues['ivr_menu_type_id'] ) ) $this->setIvrMenuTypeId( $arrValues['ivr_menu_type_id'] );
		if( isset( $arrValues['ivr_menu_id'] ) && $boolDirectSet ) $this->set( 'm_intIvrMenuId', trim( $arrValues['ivr_menu_id'] ) ); elseif( isset( $arrValues['ivr_menu_id'] ) ) $this->setIvrMenuId( $arrValues['ivr_menu_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['invalid_sound'] ) && $boolDirectSet ) $this->set( 'm_strInvalidSound', trim( stripcslashes( $arrValues['invalid_sound'] ) ) ); elseif( isset( $arrValues['invalid_sound'] ) ) $this->setInvalidSound( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invalid_sound'] ) : $arrValues['invalid_sound'] );
		if( isset( $arrValues['exit_sound'] ) && $boolDirectSet ) $this->set( 'm_strExitSound', trim( stripcslashes( $arrValues['exit_sound'] ) ) ); elseif( isset( $arrValues['exit_sound'] ) ) $this->setExitSound( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['exit_sound'] ) : $arrValues['exit_sound'] );
		if( isset( $arrValues['confirm_macro'] ) && $boolDirectSet ) $this->set( 'm_strConfirmMacro', trim( stripcslashes( $arrValues['confirm_macro'] ) ) ); elseif( isset( $arrValues['confirm_macro'] ) ) $this->setConfirmMacro( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['confirm_macro'] ) : $arrValues['confirm_macro'] );
		if( isset( $arrValues['confirm_key'] ) && $boolDirectSet ) $this->set( 'm_strConfirmKey', trim( stripcslashes( $arrValues['confirm_key'] ) ) ); elseif( isset( $arrValues['confirm_key'] ) ) $this->setConfirmKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['confirm_key'] ) : $arrValues['confirm_key'] );
		if( isset( $arrValues['tts_engine'] ) && $boolDirectSet ) $this->set( 'm_strTtsEngine', trim( stripcslashes( $arrValues['tts_engine'] ) ) ); elseif( isset( $arrValues['tts_engine'] ) ) $this->setTtsEngine( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tts_engine'] ) : $arrValues['tts_engine'] );
		if( isset( $arrValues['timeout'] ) && $boolDirectSet ) $this->set( 'm_strTimeout', trim( stripcslashes( $arrValues['timeout'] ) ) ); elseif( isset( $arrValues['timeout'] ) ) $this->setTimeout( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['timeout'] ) : $arrValues['timeout'] );
		if( isset( $arrValues['inter_digit_timeout'] ) && $boolDirectSet ) $this->set( 'm_strInterDigitTimeout', trim( stripcslashes( $arrValues['inter_digit_timeout'] ) ) ); elseif( isset( $arrValues['inter_digit_timeout'] ) ) $this->setInterDigitTimeout( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['inter_digit_timeout'] ) : $arrValues['inter_digit_timeout'] );
		if( isset( $arrValues['max_failure'] ) && $boolDirectSet ) $this->set( 'm_intMaxFailure', trim( $arrValues['max_failure'] ) ); elseif( isset( $arrValues['max_failure'] ) ) $this->setMaxFailure( $arrValues['max_failure'] );
		if( isset( $arrValues['max_timeout'] ) && $boolDirectSet ) $this->set( 'm_intMaxTimeout', trim( $arrValues['max_timeout'] ) ); elseif( isset( $arrValues['max_timeout'] ) ) $this->setMaxTimeout( $arrValues['max_timeout'] );
		if( isset( $arrValues['digit_len'] ) && $boolDirectSet ) $this->set( 'm_intDigitLen', trim( $arrValues['digit_len'] ) ); elseif( isset( $arrValues['digit_len'] ) ) $this->setDigitLen( $arrValues['digit_len'] );
		if( isset( $arrValues['phrase_lang'] ) && $boolDirectSet ) $this->set( 'm_strPhraseLang', trim( stripcslashes( $arrValues['phrase_lang'] ) ) ); elseif( isset( $arrValues['phrase_lang'] ) ) $this->setPhraseLang( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phrase_lang'] ) : $arrValues['phrase_lang'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setIvrId( $intIvrId ) {
		$this->set( 'm_intIvrId', CStrings::strToIntDef( $intIvrId, NULL, false ) );
	}

	public function getIvrId() {
		return $this->m_intIvrId;
	}

	public function sqlIvrId() {
		return ( true == isset( $this->m_intIvrId ) ) ? ( string ) $this->m_intIvrId : 'NULL';
	}

	public function setIvrMenuTypeId( $intIvrMenuTypeId ) {
		$this->set( 'm_intIvrMenuTypeId', CStrings::strToIntDef( $intIvrMenuTypeId, NULL, false ) );
	}

	public function getIvrMenuTypeId() {
		return $this->m_intIvrMenuTypeId;
	}

	public function sqlIvrMenuTypeId() {
		return ( true == isset( $this->m_intIvrMenuTypeId ) ) ? ( string ) $this->m_intIvrMenuTypeId : '1';
	}

	public function setIvrMenuId( $intIvrMenuId ) {
		$this->set( 'm_intIvrMenuId', CStrings::strToIntDef( $intIvrMenuId, NULL, false ) );
	}

	public function getIvrMenuId() {
		return $this->m_intIvrMenuId;
	}

	public function sqlIvrMenuId() {
		return ( true == isset( $this->m_intIvrMenuId ) ) ? ( string ) $this->m_intIvrMenuId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setInvalidSound( $strInvalidSound ) {
		$this->set( 'm_strInvalidSound', CStrings::strTrimDef( $strInvalidSound, 1024, NULL, true ) );
	}

	public function getInvalidSound() {
		return $this->m_strInvalidSound;
	}

	public function sqlInvalidSound() {
		return ( true == isset( $this->m_strInvalidSound ) ) ? '\'' . addslashes( $this->m_strInvalidSound ) . '\'' : 'NULL';
	}

	public function setExitSound( $strExitSound ) {
		$this->set( 'm_strExitSound', CStrings::strTrimDef( $strExitSound, 1024, NULL, true ) );
	}

	public function getExitSound() {
		return $this->m_strExitSound;
	}

	public function sqlExitSound() {
		return ( true == isset( $this->m_strExitSound ) ) ? '\'' . addslashes( $this->m_strExitSound ) . '\'' : 'NULL';
	}

	public function setConfirmMacro( $strConfirmMacro ) {
		$this->set( 'm_strConfirmMacro', CStrings::strTrimDef( $strConfirmMacro, 1024, NULL, true ) );
	}

	public function getConfirmMacro() {
		return $this->m_strConfirmMacro;
	}

	public function sqlConfirmMacro() {
		return ( true == isset( $this->m_strConfirmMacro ) ) ? '\'' . addslashes( $this->m_strConfirmMacro ) . '\'' : 'NULL';
	}

	public function setConfirmKey( $strConfirmKey ) {
		$this->set( 'm_strConfirmKey', CStrings::strTrimDef( $strConfirmKey, 40, NULL, true ) );
	}

	public function getConfirmKey() {
		return $this->m_strConfirmKey;
	}

	public function sqlConfirmKey() {
		return ( true == isset( $this->m_strConfirmKey ) ) ? '\'' . addslashes( $this->m_strConfirmKey ) . '\'' : 'NULL';
	}

	public function setTtsEngine( $strTtsEngine ) {
		$this->set( 'm_strTtsEngine', CStrings::strTrimDef( $strTtsEngine, 40, NULL, true ) );
	}

	public function getTtsEngine() {
		return $this->m_strTtsEngine;
	}

	public function sqlTtsEngine() {
		return ( true == isset( $this->m_strTtsEngine ) ) ? '\'' . addslashes( $this->m_strTtsEngine ) . '\'' : '\'cepstral\'';
	}

	public function setTimeout( $strTimeout ) {
		$this->set( 'm_strTimeout', CStrings::strTrimDef( $strTimeout, 40, NULL, true ) );
	}

	public function getTimeout() {
		return $this->m_strTimeout;
	}

	public function sqlTimeout() {
		return ( true == isset( $this->m_strTimeout ) ) ? '\'' . addslashes( $this->m_strTimeout ) . '\'' : '\'10000\'';
	}

	public function setInterDigitTimeout( $strInterDigitTimeout ) {
		$this->set( 'm_strInterDigitTimeout', CStrings::strTrimDef( $strInterDigitTimeout, 40, NULL, true ) );
	}

	public function getInterDigitTimeout() {
		return $this->m_strInterDigitTimeout;
	}

	public function sqlInterDigitTimeout() {
		return ( true == isset( $this->m_strInterDigitTimeout ) ) ? '\'' . addslashes( $this->m_strInterDigitTimeout ) . '\'' : '\'1000\'';
	}

	public function setMaxFailure( $intMaxFailure ) {
		$this->set( 'm_intMaxFailure', CStrings::strToIntDef( $intMaxFailure, NULL, false ) );
	}

	public function getMaxFailure() {
		return $this->m_intMaxFailure;
	}

	public function sqlMaxFailure() {
		return ( true == isset( $this->m_intMaxFailure ) ) ? ( string ) $this->m_intMaxFailure : '3';
	}

	public function setMaxTimeout( $intMaxTimeout ) {
		$this->set( 'm_intMaxTimeout', CStrings::strToIntDef( $intMaxTimeout, NULL, false ) );
	}

	public function getMaxTimeout() {
		return $this->m_intMaxTimeout;
	}

	public function sqlMaxTimeout() {
		return ( true == isset( $this->m_intMaxTimeout ) ) ? ( string ) $this->m_intMaxTimeout : '3';
	}

	public function setDigitLen( $intDigitLen ) {
		$this->set( 'm_intDigitLen', CStrings::strToIntDef( $intDigitLen, NULL, false ) );
	}

	public function getDigitLen() {
		return $this->m_intDigitLen;
	}

	public function sqlDigitLen() {
		return ( true == isset( $this->m_intDigitLen ) ) ? ( string ) $this->m_intDigitLen : '4';
	}

	public function setPhraseLang( $strPhraseLang ) {
		$this->set( 'm_strPhraseLang', CStrings::strTrimDef( $strPhraseLang, 40, NULL, true ) );
	}

	public function getPhraseLang() {
		return $this->m_strPhraseLang;
	}

	public function sqlPhraseLang() {
		return ( true == isset( $this->m_strPhraseLang ) ) ? '\'' . addslashes( $this->m_strPhraseLang ) . '\'' : '\'en\'';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ivr_id, ivr_menu_type_id, ivr_menu_id, name, invalid_sound, exit_sound, confirm_macro, confirm_key, tts_engine, timeout, inter_digit_timeout, max_failure, max_timeout, digit_len, phrase_lang, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlIvrId() . ', ' .
 						$this->sqlIvrMenuTypeId() . ', ' .
 						$this->sqlIvrMenuId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlInvalidSound() . ', ' .
 						$this->sqlExitSound() . ', ' .
 						$this->sqlConfirmMacro() . ', ' .
 						$this->sqlConfirmKey() . ', ' .
 						$this->sqlTtsEngine() . ', ' .
 						$this->sqlTimeout() . ', ' .
 						$this->sqlInterDigitTimeout() . ', ' .
 						$this->sqlMaxFailure() . ', ' .
 						$this->sqlMaxTimeout() . ', ' .
 						$this->sqlDigitLen() . ', ' .
 						$this->sqlPhraseLang() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId() . ','; } elseif( true == array_key_exists( 'IvrId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_id = ' . $this->sqlIvrId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_type_id = ' . $this->sqlIvrMenuTypeId() . ','; } elseif( true == array_key_exists( 'IvrMenuTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_type_id = ' . $this->sqlIvrMenuTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; } elseif( true == array_key_exists( 'IvrMenuId', $this->getChangedColumns() ) ) { $strSql .= ' ivr_menu_id = ' . $this->sqlIvrMenuId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invalid_sound = ' . $this->sqlInvalidSound() . ','; } elseif( true == array_key_exists( 'InvalidSound', $this->getChangedColumns() ) ) { $strSql .= ' invalid_sound = ' . $this->sqlInvalidSound() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exit_sound = ' . $this->sqlExitSound() . ','; } elseif( true == array_key_exists( 'ExitSound', $this->getChangedColumns() ) ) { $strSql .= ' exit_sound = ' . $this->sqlExitSound() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirm_macro = ' . $this->sqlConfirmMacro() . ','; } elseif( true == array_key_exists( 'ConfirmMacro', $this->getChangedColumns() ) ) { $strSql .= ' confirm_macro = ' . $this->sqlConfirmMacro() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirm_key = ' . $this->sqlConfirmKey() . ','; } elseif( true == array_key_exists( 'ConfirmKey', $this->getChangedColumns() ) ) { $strSql .= ' confirm_key = ' . $this->sqlConfirmKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tts_engine = ' . $this->sqlTtsEngine() . ','; } elseif( true == array_key_exists( 'TtsEngine', $this->getChangedColumns() ) ) { $strSql .= ' tts_engine = ' . $this->sqlTtsEngine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' timeout = ' . $this->sqlTimeout() . ','; } elseif( true == array_key_exists( 'Timeout', $this->getChangedColumns() ) ) { $strSql .= ' timeout = ' . $this->sqlTimeout() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' inter_digit_timeout = ' . $this->sqlInterDigitTimeout() . ','; } elseif( true == array_key_exists( 'InterDigitTimeout', $this->getChangedColumns() ) ) { $strSql .= ' inter_digit_timeout = ' . $this->sqlInterDigitTimeout() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_failure = ' . $this->sqlMaxFailure() . ','; } elseif( true == array_key_exists( 'MaxFailure', $this->getChangedColumns() ) ) { $strSql .= ' max_failure = ' . $this->sqlMaxFailure() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_timeout = ' . $this->sqlMaxTimeout() . ','; } elseif( true == array_key_exists( 'MaxTimeout', $this->getChangedColumns() ) ) { $strSql .= ' max_timeout = ' . $this->sqlMaxTimeout() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digit_len = ' . $this->sqlDigitLen() . ','; } elseif( true == array_key_exists( 'DigitLen', $this->getChangedColumns() ) ) { $strSql .= ' digit_len = ' . $this->sqlDigitLen() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phrase_lang = ' . $this->sqlPhraseLang() . ','; } elseif( true == array_key_exists( 'PhraseLang', $this->getChangedColumns() ) ) { $strSql .= ' phrase_lang = ' . $this->sqlPhraseLang() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ivr_id' => $this->getIvrId(),
			'ivr_menu_type_id' => $this->getIvrMenuTypeId(),
			'ivr_menu_id' => $this->getIvrMenuId(),
			'name' => $this->getName(),
			'invalid_sound' => $this->getInvalidSound(),
			'exit_sound' => $this->getExitSound(),
			'confirm_macro' => $this->getConfirmMacro(),
			'confirm_key' => $this->getConfirmKey(),
			'tts_engine' => $this->getTtsEngine(),
			'timeout' => $this->getTimeout(),
			'inter_digit_timeout' => $this->getInterDigitTimeout(),
			'max_failure' => $this->getMaxFailure(),
			'max_timeout' => $this->getMaxTimeout(),
			'digit_len' => $this->getDigitLen(),
			'phrase_lang' => $this->getPhraseLang(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>