<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisResults
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisResults extends CEosPluralBase {

	/**
	 * @return CCallAnalysisResult[]
	 */
	public static function fetchCallAnalysisResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisResult::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisResult
	 */
	public static function fetchCallAnalysisResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisResult::class, $objDatabase );
	}

	public static function fetchCallAnalysisResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_results', $objDatabase );
	}

	public static function fetchCallAnalysisResultById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisResult( sprintf( 'SELECT * FROM call_analysis_results WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisResultsByCid( $intCid, $objDatabase ) {
		return self::fetchCallAnalysisResults( sprintf( 'SELECT * FROM call_analysis_results WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchCallAnalysisResultsByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallAnalysisResults( sprintf( 'SELECT * FROM call_analysis_results WHERE call_id = %d', $intCallId ), $objDatabase );
	}

	public static function fetchCallAnalysisResultsByCallAnalysisQuestionId( $intCallAnalysisQuestionId, $objDatabase ) {
		return self::fetchCallAnalysisResults( sprintf( 'SELECT * FROM call_analysis_results WHERE call_analysis_question_id = %d', $intCallAnalysisQuestionId ), $objDatabase );
	}

	public static function fetchCallAnalysisResultsByCallAnalysisAnswerId( $intCallAnalysisAnswerId, $objDatabase ) {
		return self::fetchCallAnalysisResults( sprintf( 'SELECT * FROM call_analysis_results WHERE call_analysis_answer_id = %d', $intCallAnalysisAnswerId ), $objDatabase );
	}

}
?>