<?php

class CBaseCallAgentChallenge extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_challenges';

	protected $m_intId;
	protected $m_intCallAgentAchievementTypeId;
	protected $m_intCallAgentChallengeScheduleId;
	protected $m_intTeamId;
	protected $m_intReferenceValue;
	protected $m_intPointValue;
	protected $m_intMonthOfChallenge;
	protected $m_intYearOfChallenge;
	protected $m_strNotes;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intReferenceValue = '0';
		$this->m_intPointValue = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_achievement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentAchievementTypeId', trim( $arrValues['call_agent_achievement_type_id'] ) ); elseif( isset( $arrValues['call_agent_achievement_type_id'] ) ) $this->setCallAgentAchievementTypeId( $arrValues['call_agent_achievement_type_id'] );
		if( isset( $arrValues['call_agent_challenge_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentChallengeScheduleId', trim( $arrValues['call_agent_challenge_schedule_id'] ) ); elseif( isset( $arrValues['call_agent_challenge_schedule_id'] ) ) $this->setCallAgentChallengeScheduleId( $arrValues['call_agent_challenge_schedule_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['reference_value'] ) && $boolDirectSet ) $this->set( 'm_intReferenceValue', trim( $arrValues['reference_value'] ) ); elseif( isset( $arrValues['reference_value'] ) ) $this->setReferenceValue( $arrValues['reference_value'] );
		if( isset( $arrValues['point_value'] ) && $boolDirectSet ) $this->set( 'm_intPointValue', trim( $arrValues['point_value'] ) ); elseif( isset( $arrValues['point_value'] ) ) $this->setPointValue( $arrValues['point_value'] );
		if( isset( $arrValues['month_of_challenge'] ) && $boolDirectSet ) $this->set( 'm_intMonthOfChallenge', trim( $arrValues['month_of_challenge'] ) ); elseif( isset( $arrValues['month_of_challenge'] ) ) $this->setMonthOfChallenge( $arrValues['month_of_challenge'] );
		if( isset( $arrValues['year_of_challenge'] ) && $boolDirectSet ) $this->set( 'm_intYearOfChallenge', trim( $arrValues['year_of_challenge'] ) ); elseif( isset( $arrValues['year_of_challenge'] ) ) $this->setYearOfChallenge( $arrValues['year_of_challenge'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentAchievementTypeId( $intCallAgentAchievementTypeId ) {
		$this->set( 'm_intCallAgentAchievementTypeId', CStrings::strToIntDef( $intCallAgentAchievementTypeId, NULL, false ) );
	}

	public function getCallAgentAchievementTypeId() {
		return $this->m_intCallAgentAchievementTypeId;
	}

	public function sqlCallAgentAchievementTypeId() {
		return ( true == isset( $this->m_intCallAgentAchievementTypeId ) ) ? ( string ) $this->m_intCallAgentAchievementTypeId : 'NULL';
	}

	public function setCallAgentChallengeScheduleId( $intCallAgentChallengeScheduleId ) {
		$this->set( 'm_intCallAgentChallengeScheduleId', CStrings::strToIntDef( $intCallAgentChallengeScheduleId, NULL, false ) );
	}

	public function getCallAgentChallengeScheduleId() {
		return $this->m_intCallAgentChallengeScheduleId;
	}

	public function sqlCallAgentChallengeScheduleId() {
		return ( true == isset( $this->m_intCallAgentChallengeScheduleId ) ) ? ( string ) $this->m_intCallAgentChallengeScheduleId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setReferenceValue( $intReferenceValue ) {
		$this->set( 'm_intReferenceValue', CStrings::strToIntDef( $intReferenceValue, NULL, false ) );
	}

	public function getReferenceValue() {
		return $this->m_intReferenceValue;
	}

	public function sqlReferenceValue() {
		return ( true == isset( $this->m_intReferenceValue ) ) ? ( string ) $this->m_intReferenceValue : '0';
	}

	public function setPointValue( $intPointValue ) {
		$this->set( 'm_intPointValue', CStrings::strToIntDef( $intPointValue, NULL, false ) );
	}

	public function getPointValue() {
		return $this->m_intPointValue;
	}

	public function sqlPointValue() {
		return ( true == isset( $this->m_intPointValue ) ) ? ( string ) $this->m_intPointValue : '0';
	}

	public function setMonthOfChallenge( $intMonthOfChallenge ) {
		$this->set( 'm_intMonthOfChallenge', CStrings::strToIntDef( $intMonthOfChallenge, NULL, false ) );
	}

	public function getMonthOfChallenge() {
		return $this->m_intMonthOfChallenge;
	}

	public function sqlMonthOfChallenge() {
		return ( true == isset( $this->m_intMonthOfChallenge ) ) ? ( string ) $this->m_intMonthOfChallenge : 'NULL';
	}

	public function setYearOfChallenge( $intYearOfChallenge ) {
		$this->set( 'm_intYearOfChallenge', CStrings::strToIntDef( $intYearOfChallenge, NULL, false ) );
	}

	public function getYearOfChallenge() {
		return $this->m_intYearOfChallenge;
	}

	public function sqlYearOfChallenge() {
		return ( true == isset( $this->m_intYearOfChallenge ) ) ? ( string ) $this->m_intYearOfChallenge : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 500, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_achievement_type_id, call_agent_challenge_schedule_id, team_id, reference_value, point_value, month_of_challenge, year_of_challenge, notes, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentAchievementTypeId() . ', ' .
						$this->sqlCallAgentChallengeScheduleId() . ', ' .
						$this->sqlTeamId() . ', ' .
						$this->sqlReferenceValue() . ', ' .
						$this->sqlPointValue() . ', ' .
						$this->sqlMonthOfChallenge() . ', ' .
						$this->sqlYearOfChallenge() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_achievement_type_id = ' . $this->sqlCallAgentAchievementTypeId(). ',' ; } elseif( true == array_key_exists( 'CallAgentAchievementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_achievement_type_id = ' . $this->sqlCallAgentAchievementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_challenge_schedule_id = ' . $this->sqlCallAgentChallengeScheduleId(). ',' ; } elseif( true == array_key_exists( 'CallAgentChallengeScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_challenge_schedule_id = ' . $this->sqlCallAgentChallengeScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId(). ',' ; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_value = ' . $this->sqlReferenceValue(). ',' ; } elseif( true == array_key_exists( 'ReferenceValue', $this->getChangedColumns() ) ) { $strSql .= ' reference_value = ' . $this->sqlReferenceValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' point_value = ' . $this->sqlPointValue(). ',' ; } elseif( true == array_key_exists( 'PointValue', $this->getChangedColumns() ) ) { $strSql .= ' point_value = ' . $this->sqlPointValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month_of_challenge = ' . $this->sqlMonthOfChallenge(). ',' ; } elseif( true == array_key_exists( 'MonthOfChallenge', $this->getChangedColumns() ) ) { $strSql .= ' month_of_challenge = ' . $this->sqlMonthOfChallenge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_of_challenge = ' . $this->sqlYearOfChallenge(). ',' ; } elseif( true == array_key_exists( 'YearOfChallenge', $this->getChangedColumns() ) ) { $strSql .= ' year_of_challenge = ' . $this->sqlYearOfChallenge() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_achievement_type_id' => $this->getCallAgentAchievementTypeId(),
			'call_agent_challenge_schedule_id' => $this->getCallAgentChallengeScheduleId(),
			'team_id' => $this->getTeamId(),
			'reference_value' => $this->getReferenceValue(),
			'point_value' => $this->getPointValue(),
			'month_of_challenge' => $this->getMonthOfChallenge(),
			'year_of_challenge' => $this->getYearOfChallenge(),
			'notes' => $this->getNotes(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>