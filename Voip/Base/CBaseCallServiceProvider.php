<?php

class CBaseCallServiceProvider extends CEosSingularBase {

	const TABLE_NAME = 'public.call_service_providers';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strApiUsername;
	protected $m_strApiPassword;
	protected $m_strApiUrl;
	protected $m_strApiRoutesIp;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['api_username'] ) && $boolDirectSet ) $this->set( 'm_strApiUsername', trim( stripcslashes( $arrValues['api_username'] ) ) ); elseif( isset( $arrValues['api_username'] ) ) $this->setApiUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['api_username'] ) : $arrValues['api_username'] );
		if( isset( $arrValues['api_password'] ) && $boolDirectSet ) $this->set( 'm_strApiPassword', trim( stripcslashes( $arrValues['api_password'] ) ) ); elseif( isset( $arrValues['api_password'] ) ) $this->setApiPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['api_password'] ) : $arrValues['api_password'] );
		if( isset( $arrValues['api_url'] ) && $boolDirectSet ) $this->set( 'm_strApiUrl', trim( stripcslashes( $arrValues['api_url'] ) ) ); elseif( isset( $arrValues['api_url'] ) ) $this->setApiUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['api_url'] ) : $arrValues['api_url'] );
		if( isset( $arrValues['api_routes_ip'] ) && $boolDirectSet ) $this->set( 'm_strApiRoutesIp', trim( stripcslashes( $arrValues['api_routes_ip'] ) ) ); elseif( isset( $arrValues['api_routes_ip'] ) ) $this->setApiRoutesIp( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['api_routes_ip'] ) : $arrValues['api_routes_ip'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setApiUsername( $strApiUsername ) {
		$this->set( 'm_strApiUsername', CStrings::strTrimDef( $strApiUsername, 100, NULL, true ) );
	}

	public function getApiUsername() {
		return $this->m_strApiUsername;
	}

	public function sqlApiUsername() {
		return ( true == isset( $this->m_strApiUsername ) ) ? '\'' . addslashes( $this->m_strApiUsername ) . '\'' : 'NULL';
	}

	public function setApiPassword( $strApiPassword ) {
		$this->set( 'm_strApiPassword', CStrings::strTrimDef( $strApiPassword, 100, NULL, true ) );
	}

	public function getApiPassword() {
		return $this->m_strApiPassword;
	}

	public function sqlApiPassword() {
		return ( true == isset( $this->m_strApiPassword ) ) ? '\'' . addslashes( $this->m_strApiPassword ) . '\'' : 'NULL';
	}

	public function setApiUrl( $strApiUrl ) {
		$this->set( 'm_strApiUrl', CStrings::strTrimDef( $strApiUrl, 100, NULL, true ) );
	}

	public function getApiUrl() {
		return $this->m_strApiUrl;
	}

	public function sqlApiUrl() {
		return ( true == isset( $this->m_strApiUrl ) ) ? '\'' . addslashes( $this->m_strApiUrl ) . '\'' : 'NULL';
	}

	public function setApiRoutesIp( $strApiRoutesIp ) {
		$this->set( 'm_strApiRoutesIp', CStrings::strTrimDef( $strApiRoutesIp, 100, NULL, true ) );
	}

	public function getApiRoutesIp() {
		return $this->m_strApiRoutesIp;
	}

	public function sqlApiRoutesIp() {
		return ( true == isset( $this->m_strApiRoutesIp ) ) ? '\'' . addslashes( $this->m_strApiRoutesIp ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'api_username' => $this->getApiUsername(),
			'api_password' => $this->getApiPassword(),
			'api_url' => $this->getApiUrl(),
			'api_routes_ip' => $this->getApiRoutesIp(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>