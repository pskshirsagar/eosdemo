<?php

class CBaseCallAgent extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.call_agents';

	protected $m_intId;
	protected $m_intDepartmentId;
	protected $m_intEmployeeId;
	protected $m_intUserId;
	protected $m_intCallAgentStatusTypeId;
	protected $m_intCallAgentStateTypeId;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intDeskPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;
	protected $m_intLastOfferedCallId;
	protected $m_intOfferedCallQueueId;
	protected $m_intNextCallAgentStatusTypeId;
	protected $m_intNextCallAgentStateTypeId;
	protected $m_strFullName;
	protected $m_strUsername;
	protected $m_strPassword;
	protected $m_strDialedExtension;
	protected $m_strDomain;
	protected $m_intFailsToAnswer;
	protected $m_boolIsDisabled;
	protected $m_intRejectDelayTime;
	protected $m_intNoAnswerDelayTime;
	protected $m_intLastBridgeStart;
	protected $m_intLastBridgeEnd;
	protected $m_intLastOfferedCall;
	protected $m_intNoAnswerCount;
	protected $m_intTalkTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOfficeId;
	protected $m_intDesignationId;
	protected $m_intTeamId;
	protected $m_strBirthDate;
	protected $m_strDateStarted;
	protected $m_strDateTerminated;
	protected $m_strPreferredName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strGender;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCallAgentStatusTypeId = '1';
		$this->m_intCallAgentStateTypeId = '1';
		$this->m_intFailsToAnswer = '0';
		$this->m_boolIsDisabled = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['call_agent_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentStatusTypeId', trim( $arrValues['call_agent_status_type_id'] ) ); elseif( isset( $arrValues['call_agent_status_type_id'] ) ) $this->setCallAgentStatusTypeId( $arrValues['call_agent_status_type_id'] );
		if( isset( $arrValues['call_agent_state_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentStateTypeId', trim( $arrValues['call_agent_state_type_id'] ) ); elseif( isset( $arrValues['call_agent_state_type_id'] ) ) $this->setCallAgentStateTypeId( $arrValues['call_agent_state_type_id'] );
		if( isset( $arrValues['agent_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intAgentPhoneExtensionId', trim( $arrValues['agent_phone_extension_id'] ) ); elseif( isset( $arrValues['agent_phone_extension_id'] ) ) $this->setAgentPhoneExtensionId( $arrValues['agent_phone_extension_id'] );
		if( isset( $arrValues['desk_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intDeskPhoneExtensionId', trim( $arrValues['desk_phone_extension_id'] ) ); elseif( isset( $arrValues['desk_phone_extension_id'] ) ) $this->setDeskPhoneExtensionId( $arrValues['desk_phone_extension_id'] );
		if( isset( $arrValues['physical_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intPhysicalPhoneExtensionId', trim( $arrValues['physical_phone_extension_id'] ) ); elseif( isset( $arrValues['physical_phone_extension_id'] ) ) $this->setPhysicalPhoneExtensionId( $arrValues['physical_phone_extension_id'] );
		if( isset( $arrValues['last_offered_call_id'] ) && $boolDirectSet ) $this->set( 'm_intLastOfferedCallId', trim( $arrValues['last_offered_call_id'] ) ); elseif( isset( $arrValues['last_offered_call_id'] ) ) $this->setLastOfferedCallId( $arrValues['last_offered_call_id'] );
		if( isset( $arrValues['offered_call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intOfferedCallQueueId', trim( $arrValues['offered_call_queue_id'] ) ); elseif( isset( $arrValues['offered_call_queue_id'] ) ) $this->setOfferedCallQueueId( $arrValues['offered_call_queue_id'] );
		if( isset( $arrValues['next_call_agent_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNextCallAgentStatusTypeId', trim( $arrValues['next_call_agent_status_type_id'] ) ); elseif( isset( $arrValues['next_call_agent_status_type_id'] ) ) $this->setNextCallAgentStatusTypeId( $arrValues['next_call_agent_status_type_id'] );
		if( isset( $arrValues['next_call_agent_state_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNextCallAgentStateTypeId', trim( $arrValues['next_call_agent_state_type_id'] ) ); elseif( isset( $arrValues['next_call_agent_state_type_id'] ) ) $this->setNextCallAgentStateTypeId( $arrValues['next_call_agent_state_type_id'] );
		if( isset( $arrValues['full_name'] ) && $boolDirectSet ) $this->set( 'm_strFullName', trim( stripcslashes( $arrValues['full_name'] ) ) ); elseif( isset( $arrValues['full_name'] ) ) $this->setFullName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['full_name'] ) : $arrValues['full_name'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['dialed_extension'] ) && $boolDirectSet ) $this->set( 'm_strDialedExtension', trim( stripcslashes( $arrValues['dialed_extension'] ) ) ); elseif( isset( $arrValues['dialed_extension'] ) ) $this->setDialedExtension( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['dialed_extension'] ) : $arrValues['dialed_extension'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['fails_to_answer'] ) && $boolDirectSet ) $this->set( 'm_intFailsToAnswer', trim( $arrValues['fails_to_answer'] ) ); elseif( isset( $arrValues['fails_to_answer'] ) ) $this->setFailsToAnswer( $arrValues['fails_to_answer'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['reject_delay_time'] ) && $boolDirectSet ) $this->set( 'm_intRejectDelayTime', trim( $arrValues['reject_delay_time'] ) ); elseif( isset( $arrValues['reject_delay_time'] ) ) $this->setRejectDelayTime( $arrValues['reject_delay_time'] );
		if( isset( $arrValues['no_answer_delay_time'] ) && $boolDirectSet ) $this->set( 'm_intNoAnswerDelayTime', trim( $arrValues['no_answer_delay_time'] ) ); elseif( isset( $arrValues['no_answer_delay_time'] ) ) $this->setNoAnswerDelayTime( $arrValues['no_answer_delay_time'] );
		if( isset( $arrValues['last_bridge_start'] ) && $boolDirectSet ) $this->set( 'm_intLastBridgeStart', trim( $arrValues['last_bridge_start'] ) ); elseif( isset( $arrValues['last_bridge_start'] ) ) $this->setLastBridgeStart( $arrValues['last_bridge_start'] );
		if( isset( $arrValues['last_bridge_end'] ) && $boolDirectSet ) $this->set( 'm_intLastBridgeEnd', trim( $arrValues['last_bridge_end'] ) ); elseif( isset( $arrValues['last_bridge_end'] ) ) $this->setLastBridgeEnd( $arrValues['last_bridge_end'] );
		if( isset( $arrValues['last_offered_call'] ) && $boolDirectSet ) $this->set( 'm_intLastOfferedCall', trim( $arrValues['last_offered_call'] ) ); elseif( isset( $arrValues['last_offered_call'] ) ) $this->setLastOfferedCall( $arrValues['last_offered_call'] );
		if( isset( $arrValues['no_answer_count'] ) && $boolDirectSet ) $this->set( 'm_intNoAnswerCount', trim( $arrValues['no_answer_count'] ) ); elseif( isset( $arrValues['no_answer_count'] ) ) $this->setNoAnswerCount( $arrValues['no_answer_count'] );
		if( isset( $arrValues['talk_time'] ) && $boolDirectSet ) $this->set( 'm_intTalkTime', trim( $arrValues['talk_time'] ) ); elseif( isset( $arrValues['talk_time'] ) ) $this->setTalkTime( $arrValues['talk_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['office_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeId', trim( $arrValues['office_id'] ) ); elseif( isset( $arrValues['office_id'] ) ) $this->setOfficeId( $arrValues['office_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( stripcslashes( $arrValues['preferred_name'] ) ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['preferred_name'] ) : $arrValues['preferred_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( stripcslashes( $arrValues['name_prefix'] ) ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_prefix'] ) : $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( stripcslashes( $arrValues['name_middle'] ) ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_middle'] ) : $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( stripcslashes( $arrValues['gender'] ) ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gender'] ) : $arrValues['gender'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setCallAgentStatusTypeId( $intCallAgentStatusTypeId ) {
		$this->set( 'm_intCallAgentStatusTypeId', CStrings::strToIntDef( $intCallAgentStatusTypeId, NULL, false ) );
	}

	public function getCallAgentStatusTypeId() {
		return $this->m_intCallAgentStatusTypeId;
	}

	public function sqlCallAgentStatusTypeId() {
		return ( true == isset( $this->m_intCallAgentStatusTypeId ) ) ? ( string ) $this->m_intCallAgentStatusTypeId : '1';
	}

	public function setCallAgentStateTypeId( $intCallAgentStateTypeId ) {
		$this->set( 'm_intCallAgentStateTypeId', CStrings::strToIntDef( $intCallAgentStateTypeId, NULL, false ) );
	}

	public function getCallAgentStateTypeId() {
		return $this->m_intCallAgentStateTypeId;
	}

	public function sqlCallAgentStateTypeId() {
		return ( true == isset( $this->m_intCallAgentStateTypeId ) ) ? ( string ) $this->m_intCallAgentStateTypeId : '1';
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->set( 'm_intAgentPhoneExtensionId', CStrings::strToIntDef( $intAgentPhoneExtensionId, NULL, false ) );
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function sqlAgentPhoneExtensionId() {
		return ( true == isset( $this->m_intAgentPhoneExtensionId ) ) ? ( string ) $this->m_intAgentPhoneExtensionId : 'NULL';
	}

	public function setDeskPhoneExtensionId( $intDeskPhoneExtensionId ) {
		$this->set( 'm_intDeskPhoneExtensionId', CStrings::strToIntDef( $intDeskPhoneExtensionId, NULL, false ) );
	}

	public function getDeskPhoneExtensionId() {
		return $this->m_intDeskPhoneExtensionId;
	}

	public function sqlDeskPhoneExtensionId() {
		return ( true == isset( $this->m_intDeskPhoneExtensionId ) ) ? ( string ) $this->m_intDeskPhoneExtensionId : 'NULL';
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->set( 'm_intPhysicalPhoneExtensionId', CStrings::strToIntDef( $intPhysicalPhoneExtensionId, NULL, false ) );
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function sqlPhysicalPhoneExtensionId() {
		return ( true == isset( $this->m_intPhysicalPhoneExtensionId ) ) ? ( string ) $this->m_intPhysicalPhoneExtensionId : 'NULL';
	}

	public function setLastOfferedCallId( $intLastOfferedCallId ) {
		$this->set( 'm_intLastOfferedCallId', CStrings::strToIntDef( $intLastOfferedCallId, NULL, false ) );
	}

	public function getLastOfferedCallId() {
		return $this->m_intLastOfferedCallId;
	}

	public function sqlLastOfferedCallId() {
		return ( true == isset( $this->m_intLastOfferedCallId ) ) ? ( string ) $this->m_intLastOfferedCallId : 'NULL';
	}

	public function setOfferedCallQueueId( $intOfferedCallQueueId ) {
		$this->set( 'm_intOfferedCallQueueId', CStrings::strToIntDef( $intOfferedCallQueueId, NULL, false ) );
	}

	public function getOfferedCallQueueId() {
		return $this->m_intOfferedCallQueueId;
	}

	public function sqlOfferedCallQueueId() {
		return ( true == isset( $this->m_intOfferedCallQueueId ) ) ? ( string ) $this->m_intOfferedCallQueueId : 'NULL';
	}

	public function setNextCallAgentStatusTypeId( $intNextCallAgentStatusTypeId ) {
		$this->set( 'm_intNextCallAgentStatusTypeId', CStrings::strToIntDef( $intNextCallAgentStatusTypeId, NULL, false ) );
	}

	public function getNextCallAgentStatusTypeId() {
		return $this->m_intNextCallAgentStatusTypeId;
	}

	public function sqlNextCallAgentStatusTypeId() {
		return ( true == isset( $this->m_intNextCallAgentStatusTypeId ) ) ? ( string ) $this->m_intNextCallAgentStatusTypeId : 'NULL';
	}

	public function setNextCallAgentStateTypeId( $intNextCallAgentStateTypeId ) {
		$this->set( 'm_intNextCallAgentStateTypeId', CStrings::strToIntDef( $intNextCallAgentStateTypeId, NULL, false ) );
	}

	public function getNextCallAgentStateTypeId() {
		return $this->m_intNextCallAgentStateTypeId;
	}

	public function sqlNextCallAgentStateTypeId() {
		return ( true == isset( $this->m_intNextCallAgentStateTypeId ) ) ? ( string ) $this->m_intNextCallAgentStateTypeId : 'NULL';
	}

	public function setFullName( $strFullName ) {
		$this->set( 'm_strFullName', CStrings::strTrimDef( $strFullName, 240, NULL, true ) );
	}

	public function getFullName() {
		return $this->m_strFullName;
	}

	public function sqlFullName() {
		return ( true == isset( $this->m_strFullName ) ) ? '\'' . addslashes( $this->m_strFullName ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 40, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 255, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setDialedExtension( $strDialedExtension ) {
		$this->set( 'm_strDialedExtension', CStrings::strTrimDef( $strDialedExtension, 10, NULL, true ) );
	}

	public function getDialedExtension() {
		return $this->m_strDialedExtension;
	}

	public function sqlDialedExtension() {
		return ( true == isset( $this->m_strDialedExtension ) ) ? '\'' . addslashes( $this->m_strDialedExtension ) . '\'' : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 240, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setFailsToAnswer( $intFailsToAnswer ) {
		$this->set( 'm_intFailsToAnswer', CStrings::strToIntDef( $intFailsToAnswer, NULL, false ) );
	}

	public function getFailsToAnswer() {
		return $this->m_intFailsToAnswer;
	}

	public function sqlFailsToAnswer() {
		return ( true == isset( $this->m_intFailsToAnswer ) ) ? ( string ) $this->m_intFailsToAnswer : '0';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRejectDelayTime( $intRejectDelayTime ) {
		$this->set( 'm_intRejectDelayTime', CStrings::strToIntDef( $intRejectDelayTime, NULL, false ) );
	}

	public function getRejectDelayTime() {
		return $this->m_intRejectDelayTime;
	}

	public function sqlRejectDelayTime() {
		return ( true == isset( $this->m_intRejectDelayTime ) ) ? ( string ) $this->m_intRejectDelayTime : 'NULL';
	}

	public function setNoAnswerDelayTime( $intNoAnswerDelayTime ) {
		$this->set( 'm_intNoAnswerDelayTime', CStrings::strToIntDef( $intNoAnswerDelayTime, NULL, false ) );
	}

	public function getNoAnswerDelayTime() {
		return $this->m_intNoAnswerDelayTime;
	}

	public function sqlNoAnswerDelayTime() {
		return ( true == isset( $this->m_intNoAnswerDelayTime ) ) ? ( string ) $this->m_intNoAnswerDelayTime : 'NULL';
	}

	public function setLastBridgeStart( $intLastBridgeStart ) {
		$this->set( 'm_intLastBridgeStart', CStrings::strToIntDef( $intLastBridgeStart, NULL, false ) );
	}

	public function getLastBridgeStart() {
		return $this->m_intLastBridgeStart;
	}

	public function sqlLastBridgeStart() {
		return ( true == isset( $this->m_intLastBridgeStart ) ) ? ( string ) $this->m_intLastBridgeStart : 'NULL';
	}

	public function setLastBridgeEnd( $intLastBridgeEnd ) {
		$this->set( 'm_intLastBridgeEnd', CStrings::strToIntDef( $intLastBridgeEnd, NULL, false ) );
	}

	public function getLastBridgeEnd() {
		return $this->m_intLastBridgeEnd;
	}

	public function sqlLastBridgeEnd() {
		return ( true == isset( $this->m_intLastBridgeEnd ) ) ? ( string ) $this->m_intLastBridgeEnd : 'NULL';
	}

	public function setLastOfferedCall( $intLastOfferedCall ) {
		$this->set( 'm_intLastOfferedCall', CStrings::strToIntDef( $intLastOfferedCall, NULL, false ) );
	}

	public function getLastOfferedCall() {
		return $this->m_intLastOfferedCall;
	}

	public function sqlLastOfferedCall() {
		return ( true == isset( $this->m_intLastOfferedCall ) ) ? ( string ) $this->m_intLastOfferedCall : 'NULL';
	}

	public function setNoAnswerCount( $intNoAnswerCount ) {
		$this->set( 'm_intNoAnswerCount', CStrings::strToIntDef( $intNoAnswerCount, NULL, false ) );
	}

	public function getNoAnswerCount() {
		return $this->m_intNoAnswerCount;
	}

	public function sqlNoAnswerCount() {
		return ( true == isset( $this->m_intNoAnswerCount ) ) ? ( string ) $this->m_intNoAnswerCount : 'NULL';
	}

	public function setTalkTime( $intTalkTime ) {
		$this->set( 'm_intTalkTime', CStrings::strToIntDef( $intTalkTime, NULL, false ) );
	}

	public function getTalkTime() {
		return $this->m_intTalkTime;
	}

	public function sqlTalkTime() {
		return ( true == isset( $this->m_intTalkTime ) ) ? ( string ) $this->m_intTalkTime : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOfficeId( $intOfficeId ) {
		$this->set( 'm_intOfficeId', CStrings::strToIntDef( $intOfficeId, NULL, false ) );
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function sqlOfficeId() {
		return ( true == isset( $this->m_intOfficeId ) ) ? ( string ) $this->m_intOfficeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 100, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? '\'' . addslashes( $this->m_strPreferredName ) . '\'' : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 10, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? '\'' . addslashes( $this->m_strNamePrefix ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? '\'' . addslashes( $this->m_strNameMiddle ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 10, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? '\'' . addslashes( $this->m_strGender ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, department_id, employee_id, user_id, call_agent_status_type_id, call_agent_state_type_id, agent_phone_extension_id, desk_phone_extension_id, physical_phone_extension_id, last_offered_call_id, offered_call_queue_id, next_call_agent_status_type_id, next_call_agent_state_type_id, full_name, username, password, dialed_extension, domain, fails_to_answer, is_disabled, reject_delay_time, no_answer_delay_time, last_bridge_start, last_bridge_end, last_offered_call, no_answer_count, talk_time, updated_by, updated_on, created_by, created_on, office_id, designation_id, team_id, birth_date, date_started, date_terminated, preferred_name, name_prefix, name_first, name_middle, name_last, gender, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlCallAgentStatusTypeId() . ', ' .
						$this->sqlCallAgentStateTypeId() . ', ' .
						$this->sqlAgentPhoneExtensionId() . ', ' .
						$this->sqlDeskPhoneExtensionId() . ', ' .
						$this->sqlPhysicalPhoneExtensionId() . ', ' .
						$this->sqlLastOfferedCallId() . ', ' .
						$this->sqlOfferedCallQueueId() . ', ' .
						$this->sqlNextCallAgentStatusTypeId() . ', ' .
						$this->sqlNextCallAgentStateTypeId() . ', ' .
						$this->sqlFullName() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPassword() . ', ' .
						$this->sqlDialedExtension() . ', ' .
						$this->sqlDomain() . ', ' .
						$this->sqlFailsToAnswer() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlRejectDelayTime() . ', ' .
						$this->sqlNoAnswerDelayTime() . ', ' .
						$this->sqlLastBridgeStart() . ', ' .
						$this->sqlLastBridgeEnd() . ', ' .
						$this->sqlLastOfferedCall() . ', ' .
						$this->sqlNoAnswerCount() . ', ' .
						$this->sqlTalkTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOfficeId() . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlTeamId() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlDateStarted() . ', ' .
						$this->sqlDateTerminated() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CallAgentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_status_type_id = ' . $this->sqlCallAgentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_state_type_id = ' . $this->sqlCallAgentStateTypeId(). ',' ; } elseif( true == array_key_exists( 'CallAgentStateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_state_type_id = ' . $this->sqlCallAgentStateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_phone_extension_id = ' . $this->sqlAgentPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'AgentPhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' agent_phone_extension_id = ' . $this->sqlAgentPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_phone_extension_id = ' . $this->sqlDeskPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'DeskPhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' desk_phone_extension_id = ' . $this->sqlDeskPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' physical_phone_extension_id = ' . $this->sqlPhysicalPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'PhysicalPhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' physical_phone_extension_id = ' . $this->sqlPhysicalPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_offered_call_id = ' . $this->sqlLastOfferedCallId(). ',' ; } elseif( true == array_key_exists( 'LastOfferedCallId', $this->getChangedColumns() ) ) { $strSql .= ' last_offered_call_id = ' . $this->sqlLastOfferedCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offered_call_queue_id = ' . $this->sqlOfferedCallQueueId(). ',' ; } elseif( true == array_key_exists( 'OfferedCallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' offered_call_queue_id = ' . $this->sqlOfferedCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_call_agent_status_type_id = ' . $this->sqlNextCallAgentStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'NextCallAgentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' next_call_agent_status_type_id = ' . $this->sqlNextCallAgentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' next_call_agent_state_type_id = ' . $this->sqlNextCallAgentStateTypeId(). ',' ; } elseif( true == array_key_exists( 'NextCallAgentStateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' next_call_agent_state_type_id = ' . $this->sqlNextCallAgentStateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' full_name = ' . $this->sqlFullName(). ',' ; } elseif( true == array_key_exists( 'FullName', $this->getChangedColumns() ) ) { $strSql .= ' full_name = ' . $this->sqlFullName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword(). ',' ; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dialed_extension = ' . $this->sqlDialedExtension(). ',' ; } elseif( true == array_key_exists( 'DialedExtension', $this->getChangedColumns() ) ) { $strSql .= ' dialed_extension = ' . $this->sqlDialedExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain(). ',' ; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fails_to_answer = ' . $this->sqlFailsToAnswer(). ',' ; } elseif( true == array_key_exists( 'FailsToAnswer', $this->getChangedColumns() ) ) { $strSql .= ' fails_to_answer = ' . $this->sqlFailsToAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reject_delay_time = ' . $this->sqlRejectDelayTime(). ',' ; } elseif( true == array_key_exists( 'RejectDelayTime', $this->getChangedColumns() ) ) { $strSql .= ' reject_delay_time = ' . $this->sqlRejectDelayTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_answer_delay_time = ' . $this->sqlNoAnswerDelayTime(). ',' ; } elseif( true == array_key_exists( 'NoAnswerDelayTime', $this->getChangedColumns() ) ) { $strSql .= ' no_answer_delay_time = ' . $this->sqlNoAnswerDelayTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_bridge_start = ' . $this->sqlLastBridgeStart(). ',' ; } elseif( true == array_key_exists( 'LastBridgeStart', $this->getChangedColumns() ) ) { $strSql .= ' last_bridge_start = ' . $this->sqlLastBridgeStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_bridge_end = ' . $this->sqlLastBridgeEnd(). ',' ; } elseif( true == array_key_exists( 'LastBridgeEnd', $this->getChangedColumns() ) ) { $strSql .= ' last_bridge_end = ' . $this->sqlLastBridgeEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_offered_call = ' . $this->sqlLastOfferedCall(). ',' ; } elseif( true == array_key_exists( 'LastOfferedCall', $this->getChangedColumns() ) ) { $strSql .= ' last_offered_call = ' . $this->sqlLastOfferedCall() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_answer_count = ' . $this->sqlNoAnswerCount(). ',' ; } elseif( true == array_key_exists( 'NoAnswerCount', $this->getChangedColumns() ) ) { $strSql .= ' no_answer_count = ' . $this->sqlNoAnswerCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' talk_time = ' . $this->sqlTalkTime(). ',' ; } elseif( true == array_key_exists( 'TalkTime', $this->getChangedColumns() ) ) { $strSql .= ' talk_time = ' . $this->sqlTalkTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_id = ' . $this->sqlOfficeId(). ',' ; } elseif( true == array_key_exists( 'OfficeId', $this->getChangedColumns() ) ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId(). ',' ; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted(). ',' ; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated(). ',' ; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'department_id' => $this->getDepartmentId(),
			'employee_id' => $this->getEmployeeId(),
			'user_id' => $this->getUserId(),
			'call_agent_status_type_id' => $this->getCallAgentStatusTypeId(),
			'call_agent_state_type_id' => $this->getCallAgentStateTypeId(),
			'agent_phone_extension_id' => $this->getAgentPhoneExtensionId(),
			'desk_phone_extension_id' => $this->getDeskPhoneExtensionId(),
			'physical_phone_extension_id' => $this->getPhysicalPhoneExtensionId(),
			'last_offered_call_id' => $this->getLastOfferedCallId(),
			'offered_call_queue_id' => $this->getOfferedCallQueueId(),
			'next_call_agent_status_type_id' => $this->getNextCallAgentStatusTypeId(),
			'next_call_agent_state_type_id' => $this->getNextCallAgentStateTypeId(),
			'full_name' => $this->getFullName(),
			'username' => $this->getUsername(),
			'password' => $this->getPassword(),
			'dialed_extension' => $this->getDialedExtension(),
			'domain' => $this->getDomain(),
			'fails_to_answer' => $this->getFailsToAnswer(),
			'is_disabled' => $this->getIsDisabled(),
			'reject_delay_time' => $this->getRejectDelayTime(),
			'no_answer_delay_time' => $this->getNoAnswerDelayTime(),
			'last_bridge_start' => $this->getLastBridgeStart(),
			'last_bridge_end' => $this->getLastBridgeEnd(),
			'last_offered_call' => $this->getLastOfferedCall(),
			'no_answer_count' => $this->getNoAnswerCount(),
			'talk_time' => $this->getTalkTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'office_id' => $this->getOfficeId(),
			'designation_id' => $this->getDesignationId(),
			'team_id' => $this->getTeamId(),
			'birth_date' => $this->getBirthDate(),
			'date_started' => $this->getDateStarted(),
			'date_terminated' => $this->getDateTerminated(),
			'preferred_name' => $this->getPreferredName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'gender' => $this->getGender(),
			'details' => $this->getDetails()
		);
	}

}
?>