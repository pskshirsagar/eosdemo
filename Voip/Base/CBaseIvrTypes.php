<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrTypes
 * Do not add any new functions to this class.
 */

class CBaseIvrTypes extends CEosPluralBase {

	/**
	 * @return CIvrType[]
	 */
	public static function fetchIvrTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIvrType::class, $objDatabase );
	}

	/**
	 * @return CIvrType
	 */
	public static function fetchIvrType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIvrType::class, $objDatabase );
	}

	public static function fetchIvrTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ivr_types', $objDatabase );
	}

	public static function fetchIvrTypeById( $intId, $objDatabase ) {
		return self::fetchIvrType( sprintf( 'SELECT * FROM ivr_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>