<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisAnswers
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisAnswers extends CEosPluralBase {

	/**
	 * @return CCallAnalysisAnswer[]
	 */
	public static function fetchCallAnalysisAnswers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisAnswer::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisAnswer
	 */
	public static function fetchCallAnalysisAnswer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisAnswer::class, $objDatabase );
	}

	public static function fetchCallAnalysisAnswerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_answers', $objDatabase );
	}

	public static function fetchCallAnalysisAnswerById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisAnswer( sprintf( 'SELECT * FROM call_analysis_answers WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisAnswersByCallAnalysisQuestionId( $intCallAnalysisQuestionId, $objDatabase ) {
		return self::fetchCallAnalysisAnswers( sprintf( 'SELECT * FROM call_analysis_answers WHERE call_analysis_question_id = %d', $intCallAnalysisQuestionId ), $objDatabase );
	}

}
?>