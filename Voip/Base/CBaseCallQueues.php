<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueues
 * Do not add any new functions to this class.
 */

class CBaseCallQueues extends CEosPluralBase {

	/**
	 * @return CCallQueue[]
	 */
	public static function fetchCallQueues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallQueue::class, $objDatabase );
	}

	/**
	 * @return CCallQueue
	 */
	public static function fetchCallQueue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallQueue::class, $objDatabase );
	}

	public static function fetchCallQueueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_queues', $objDatabase );
	}

	public static function fetchCallQueueById( $intId, $objDatabase ) {
		return self::fetchCallQueue( sprintf( 'SELECT * FROM call_queues WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallQueuesByOverflowCallQueueId( $intOverflowCallQueueId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE overflow_call_queue_id = %d', ( int ) $intOverflowCallQueueId ), $objDatabase );
	}

	public static function fetchCallQueuesByCallStrategyTypeId( $intCallStrategyTypeId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE call_strategy_type_id = %d', ( int ) $intCallStrategyTypeId ), $objDatabase );
	}

	public static function fetchCallQueuesByCallQueueTypeId( $intCallQueueTypeId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE call_queue_type_id = %d', ( int ) $intCallQueueTypeId ), $objDatabase );
	}

	public static function fetchCallQueuesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchCallQueuesByCallFileId( $intCallFileId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE call_file_id = %d', ( int ) $intCallFileId ), $objDatabase );
	}

	public static function fetchCallQueuesByQueuePhoneExtensionId( $intQueuePhoneExtensionId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE queue_phone_extension_id = %d', ( int ) $intQueuePhoneExtensionId ), $objDatabase );
	}

	public static function fetchCallQueuesByCallTypeId( $intCallTypeId, $objDatabase ) {
		return self::fetchCallQueues( sprintf( 'SELECT * FROM call_queues WHERE call_type_id = %d', ( int ) $intCallTypeId ), $objDatabase );
	}

}
?>