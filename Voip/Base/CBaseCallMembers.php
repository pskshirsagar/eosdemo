<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallMembers
 * Do not add any new functions to this class.
 */

class CBaseCallMembers extends CEosPluralBase {

	/**
	 * @return CCallMember[]
	 */
	public static function fetchCallMembers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallMember', $objDatabase );
	}

	/**
	 * @return CCallMember
	 */
	public static function fetchCallMember( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallMember', $objDatabase );
	}

	public static function fetchCallMemberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_members', $objDatabase );
	}

	public static function fetchCallMemberById( $intId, $objDatabase ) {
		return self::fetchCallMember( sprintf( 'SELECT * FROM call_members WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallMembersByCallId( $intCallId, $objDatabase ) {
		return self::fetchCallMembers( sprintf( 'SELECT * FROM call_members WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchCallMembersByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallMembers( sprintf( 'SELECT * FROM call_members WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

	public static function fetchCallMembersByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallMembers( sprintf( 'SELECT * FROM call_members WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>