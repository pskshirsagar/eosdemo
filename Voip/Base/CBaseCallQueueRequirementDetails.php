<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueueRequirementDetails
 * Do not add any new functions to this class.
 */

class CBaseCallQueueRequirementDetails extends CEosPluralBase {

	/**
	 * @return CCallQueueRequirementDetail[]
	 */
	public static function fetchCallQueueRequirementDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallQueueRequirementDetail', $objDatabase );
	}

	/**
	 * @return CCallQueueRequirementDetail
	 */
	public static function fetchCallQueueRequirementDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallQueueRequirementDetail', $objDatabase );
	}

	public static function fetchCallQueueRequirementDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_queue_requirement_details', $objDatabase );
	}

	public static function fetchCallQueueRequirementDetailById( $intId, $objDatabase ) {
		return self::fetchCallQueueRequirementDetail( sprintf( 'SELECT * FROM call_queue_requirement_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallQueueRequirementDetailsByCallQueueRequirementId( $intCallQueueRequirementId, $objDatabase ) {
		return self::fetchCallQueueRequirementDetails( sprintf( 'SELECT * FROM call_queue_requirement_details WHERE call_queue_requirement_id = %d', ( int ) $intCallQueueRequirementId ), $objDatabase );
	}

	public static function fetchCallQueueRequirementDetailsByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchCallQueueRequirementDetails( sprintf( 'SELECT * FROM call_queue_requirement_details WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

}
?>