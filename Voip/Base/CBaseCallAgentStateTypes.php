<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentStateTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAgentStateTypes extends CEosPluralBase {

	/**
	 * @return CCallAgentStateType[]
	 */
	public static function fetchCallAgentStateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAgentStateType::class, $objDatabase );
	}

	/**
	 * @return CCallAgentStateType
	 */
	public static function fetchCallAgentStateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAgentStateType::class, $objDatabase );
	}

	public static function fetchCallAgentStateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_state_types', $objDatabase );
	}

	public static function fetchCallAgentStateTypeById( $intId, $objDatabase ) {
		return self::fetchCallAgentStateType( sprintf( 'SELECT * FROM call_agent_state_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>