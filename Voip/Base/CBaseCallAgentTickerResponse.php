<?php

class CBaseCallAgentTickerResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_ticker_responses';

	protected $m_intId;
	protected $m_intCallAgentTickerId;
	protected $m_intCallAgentId;
	protected $m_strResponse;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strResponse = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_ticker_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentTickerId', trim( $arrValues['call_agent_ticker_id'] ) ); elseif( isset( $arrValues['call_agent_ticker_id'] ) ) $this->setCallAgentTickerId( $arrValues['call_agent_ticker_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['response'] ) && $boolDirectSet ) $this->set( 'm_strResponse', trim( stripcslashes( $arrValues['response'] ) ) ); elseif( isset( $arrValues['response'] ) ) $this->setResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response'] ) : $arrValues['response'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentTickerId( $intCallAgentTickerId ) {
		$this->set( 'm_intCallAgentTickerId', CStrings::strToIntDef( $intCallAgentTickerId, NULL, false ) );
	}

	public function getCallAgentTickerId() {
		return $this->m_intCallAgentTickerId;
	}

	public function sqlCallAgentTickerId() {
		return ( true == isset( $this->m_intCallAgentTickerId ) ) ? ( string ) $this->m_intCallAgentTickerId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setResponse( $strResponse ) {
		$this->set( 'm_strResponse', CStrings::strTrimDef( $strResponse, 100, NULL, true ) );
	}

	public function getResponse() {
		return $this->m_strResponse;
	}

	public function sqlResponse() {
		return ( true == isset( $this->m_strResponse ) ) ? '\'' . addslashes( $this->m_strResponse ) . '\'' : '\'NULL\'';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_ticker_id, call_agent_id, response, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallAgentTickerId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlResponse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_ticker_id = ' . $this->sqlCallAgentTickerId() . ','; } elseif( true == array_key_exists( 'CallAgentTickerId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_ticker_id = ' . $this->sqlCallAgentTickerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; } elseif( true == array_key_exists( 'Response', $this->getChangedColumns() ) ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_ticker_id' => $this->getCallAgentTickerId(),
			'call_agent_id' => $this->getCallAgentId(),
			'response' => $this->getResponse(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>