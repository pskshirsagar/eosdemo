<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCompanyCallServiceLevels
 * Do not add any new functions to this class.
 */

class CBaseCompanyCallServiceLevels extends CEosPluralBase {

	/**
	 * @return CCompanyCallServiceLevel[]
	 */
	public static function fetchCompanyCallServiceLevels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyCallServiceLevel::class, $objDatabase );
	}

	/**
	 * @return CCompanyCallServiceLevel
	 */
	public static function fetchCompanyCallServiceLevel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyCallServiceLevel::class, $objDatabase );
	}

	public static function fetchCompanyCallServiceLevelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_call_service_levels', $objDatabase );
	}

	public static function fetchCompanyCallServiceLevelById( $intId, $objDatabase ) {
		return self::fetchCompanyCallServiceLevel( sprintf( 'SELECT * FROM company_call_service_levels WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCompanyCallServiceLevelsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyCallServiceLevels( sprintf( 'SELECT * FROM company_call_service_levels WHERE cid = %d', $intCid ), $objDatabase );
	}

}
?>