<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentSchedules
 * Do not add any new functions to this class.
 */

class CBaseCallAgentSchedules extends CEosPluralBase {

	/**
	 * @return CCallAgentSchedule[]
	 */
	public static function fetchCallAgentSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallAgentSchedule', $objDatabase );
	}

	/**
	 * @return CCallAgentSchedule
	 */
	public static function fetchCallAgentSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallAgentSchedule', $objDatabase );
	}

	public static function fetchCallAgentScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_agent_schedules', $objDatabase );
	}

	public static function fetchCallAgentScheduleById( $intId, $objDatabase ) {
		return self::fetchCallAgentSchedule( sprintf( 'SELECT * FROM call_agent_schedules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallAgentSchedulesByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchCallAgentSchedules( sprintf( 'SELECT * FROM call_agent_schedules WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>