<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServerModules
 * Do not add any new functions to this class.
 */

class CBaseCallServerModules extends CEosPluralBase {

	/**
	 * @return CCallServerModule[]
	 */
	public static function fetchCallServerModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCallServerModule', $objDatabase );
	}

	/**
	 * @return CCallServerModule
	 */
	public static function fetchCallServerModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCallServerModule', $objDatabase );
	}

	public static function fetchCallServerModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_server_modules', $objDatabase );
	}

	public static function fetchCallServerModuleById( $intId, $objDatabase ) {
		return self::fetchCallServerModule( sprintf( 'SELECT * FROM call_server_modules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCallServerModulesByCallServerId( $intCallServerId, $objDatabase ) {
		return self::fetchCallServerModules( sprintf( 'SELECT * FROM call_server_modules WHERE call_server_id = %d', ( int ) $intCallServerId ), $objDatabase );
	}

}
?>