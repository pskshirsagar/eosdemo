<?php

class CBaseCallAgentRedeemedProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.call_agent_redeemed_products';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_intStoreProductId;
	protected $m_intCallAgentRankId;
	protected $m_intRedeemedCoins;
	protected $m_intCreditBalance;
	protected $m_intCreditExpense;
	protected $m_intRedeemedBy;
	protected $m_strRedeemedOn;
	protected $m_intReceivedBy;
	protected $m_strReceivedOn;
	protected $m_intReleasedBy;
	protected $m_strReleasedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPurchasedBy;
	protected $m_strPurchasedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRedeemedCoins = '0';
		$this->m_intCreditBalance = '0';
		$this->m_intCreditExpense = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['store_product_id'] ) && $boolDirectSet ) $this->set( 'm_intStoreProductId', trim( $arrValues['store_product_id'] ) ); elseif( isset( $arrValues['store_product_id'] ) ) $this->setStoreProductId( $arrValues['store_product_id'] );
		if( isset( $arrValues['call_agent_rank_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentRankId', trim( $arrValues['call_agent_rank_id'] ) ); elseif( isset( $arrValues['call_agent_rank_id'] ) ) $this->setCallAgentRankId( $arrValues['call_agent_rank_id'] );
		if( isset( $arrValues['redeemed_coins'] ) && $boolDirectSet ) $this->set( 'm_intRedeemedCoins', trim( $arrValues['redeemed_coins'] ) ); elseif( isset( $arrValues['redeemed_coins'] ) ) $this->setRedeemedCoins( $arrValues['redeemed_coins'] );
		if( isset( $arrValues['credit_balance'] ) && $boolDirectSet ) $this->set( 'm_intCreditBalance', trim( $arrValues['credit_balance'] ) ); elseif( isset( $arrValues['credit_balance'] ) ) $this->setCreditBalance( $arrValues['credit_balance'] );
		if( isset( $arrValues['credit_expense'] ) && $boolDirectSet ) $this->set( 'm_intCreditExpense', trim( $arrValues['credit_expense'] ) ); elseif( isset( $arrValues['credit_expense'] ) ) $this->setCreditExpense( $arrValues['credit_expense'] );
		if( isset( $arrValues['redeemed_by'] ) && $boolDirectSet ) $this->set( 'm_intRedeemedBy', trim( $arrValues['redeemed_by'] ) ); elseif( isset( $arrValues['redeemed_by'] ) ) $this->setRedeemedBy( $arrValues['redeemed_by'] );
		if( isset( $arrValues['redeemed_on'] ) && $boolDirectSet ) $this->set( 'm_strRedeemedOn', trim( $arrValues['redeemed_on'] ) ); elseif( isset( $arrValues['redeemed_on'] ) ) $this->setRedeemedOn( $arrValues['redeemed_on'] );
		if( isset( $arrValues['received_by'] ) && $boolDirectSet ) $this->set( 'm_intReceivedBy', trim( $arrValues['received_by'] ) ); elseif( isset( $arrValues['received_by'] ) ) $this->setReceivedBy( $arrValues['received_by'] );
		if( isset( $arrValues['received_on'] ) && $boolDirectSet ) $this->set( 'm_strReceivedOn', trim( $arrValues['received_on'] ) ); elseif( isset( $arrValues['received_on'] ) ) $this->setReceivedOn( $arrValues['received_on'] );
		if( isset( $arrValues['released_by'] ) && $boolDirectSet ) $this->set( 'm_intReleasedBy', trim( $arrValues['released_by'] ) ); elseif( isset( $arrValues['released_by'] ) ) $this->setReleasedBy( $arrValues['released_by'] );
		if( isset( $arrValues['released_on'] ) && $boolDirectSet ) $this->set( 'm_strReleasedOn', trim( $arrValues['released_on'] ) ); elseif( isset( $arrValues['released_on'] ) ) $this->setReleasedOn( $arrValues['released_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['purchased_by'] ) && $boolDirectSet ) $this->set( 'm_intPurchasedBy', trim( $arrValues['purchased_by'] ) ); elseif( isset( $arrValues['purchased_by'] ) ) $this->setPurchasedBy( $arrValues['purchased_by'] );
		if( isset( $arrValues['purchased_on'] ) && $boolDirectSet ) $this->set( 'm_strPurchasedOn', trim( $arrValues['purchased_on'] ) ); elseif( isset( $arrValues['purchased_on'] ) ) $this->setPurchasedOn( $arrValues['purchased_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setStoreProductId( $intStoreProductId ) {
		$this->set( 'm_intStoreProductId', CStrings::strToIntDef( $intStoreProductId, NULL, false ) );
	}

	public function getStoreProductId() {
		return $this->m_intStoreProductId;
	}

	public function sqlStoreProductId() {
		return ( true == isset( $this->m_intStoreProductId ) ) ? ( string ) $this->m_intStoreProductId : 'NULL';
	}

	public function setCallAgentRankId( $intCallAgentRankId ) {
		$this->set( 'm_intCallAgentRankId', CStrings::strToIntDef( $intCallAgentRankId, NULL, false ) );
	}

	public function getCallAgentRankId() {
		return $this->m_intCallAgentRankId;
	}

	public function sqlCallAgentRankId() {
		return ( true == isset( $this->m_intCallAgentRankId ) ) ? ( string ) $this->m_intCallAgentRankId : 'NULL';
	}

	public function setRedeemedCoins( $intRedeemedCoins ) {
		$this->set( 'm_intRedeemedCoins', CStrings::strToIntDef( $intRedeemedCoins, NULL, false ) );
	}

	public function getRedeemedCoins() {
		return $this->m_intRedeemedCoins;
	}

	public function sqlRedeemedCoins() {
		return ( true == isset( $this->m_intRedeemedCoins ) ) ? ( string ) $this->m_intRedeemedCoins : '0';
	}

	public function setCreditBalance( $intCreditBalance ) {
		$this->set( 'm_intCreditBalance', CStrings::strToIntDef( $intCreditBalance, NULL, false ) );
	}

	public function getCreditBalance() {
		return $this->m_intCreditBalance;
	}

	public function sqlCreditBalance() {
		return ( true == isset( $this->m_intCreditBalance ) ) ? ( string ) $this->m_intCreditBalance : '0';
	}

	public function setCreditExpense( $intCreditExpense ) {
		$this->set( 'm_intCreditExpense', CStrings::strToIntDef( $intCreditExpense, NULL, false ) );
	}

	public function getCreditExpense() {
		return $this->m_intCreditExpense;
	}

	public function sqlCreditExpense() {
		return ( true == isset( $this->m_intCreditExpense ) ) ? ( string ) $this->m_intCreditExpense : '0';
	}

	public function setRedeemedBy( $intRedeemedBy ) {
		$this->set( 'm_intRedeemedBy', CStrings::strToIntDef( $intRedeemedBy, NULL, false ) );
	}

	public function getRedeemedBy() {
		return $this->m_intRedeemedBy;
	}

	public function sqlRedeemedBy() {
		return ( true == isset( $this->m_intRedeemedBy ) ) ? ( string ) $this->m_intRedeemedBy : 'NULL';
	}

	public function setRedeemedOn( $strRedeemedOn ) {
		$this->set( 'm_strRedeemedOn', CStrings::strTrimDef( $strRedeemedOn, -1, NULL, true ) );
	}

	public function getRedeemedOn() {
		return $this->m_strRedeemedOn;
	}

	public function sqlRedeemedOn() {
		return ( true == isset( $this->m_strRedeemedOn ) ) ? '\'' . $this->m_strRedeemedOn . '\'' : 'NULL';
	}

	public function setReceivedBy( $intReceivedBy ) {
		$this->set( 'm_intReceivedBy', CStrings::strToIntDef( $intReceivedBy, NULL, false ) );
	}

	public function getReceivedBy() {
		return $this->m_intReceivedBy;
	}

	public function sqlReceivedBy() {
		return ( true == isset( $this->m_intReceivedBy ) ) ? ( string ) $this->m_intReceivedBy : 'NULL';
	}

	public function setReceivedOn( $strReceivedOn ) {
		$this->set( 'm_strReceivedOn', CStrings::strTrimDef( $strReceivedOn, -1, NULL, true ) );
	}

	public function getReceivedOn() {
		return $this->m_strReceivedOn;
	}

	public function sqlReceivedOn() {
		return ( true == isset( $this->m_strReceivedOn ) ) ? '\'' . $this->m_strReceivedOn . '\'' : 'NULL';
	}

	public function setReleasedBy( $intReleasedBy ) {
		$this->set( 'm_intReleasedBy', CStrings::strToIntDef( $intReleasedBy, NULL, false ) );
	}

	public function getReleasedBy() {
		return $this->m_intReleasedBy;
	}

	public function sqlReleasedBy() {
		return ( true == isset( $this->m_intReleasedBy ) ) ? ( string ) $this->m_intReleasedBy : 'NULL';
	}

	public function setReleasedOn( $strReleasedOn ) {
		$this->set( 'm_strReleasedOn', CStrings::strTrimDef( $strReleasedOn, -1, NULL, true ) );
	}

	public function getReleasedOn() {
		return $this->m_strReleasedOn;
	}

	public function sqlReleasedOn() {
		return ( true == isset( $this->m_strReleasedOn ) ) ? '\'' . $this->m_strReleasedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPurchasedBy( $intPurchasedBy ) {
		$this->set( 'm_intPurchasedBy', CStrings::strToIntDef( $intPurchasedBy, NULL, false ) );
	}

	public function getPurchasedBy() {
		return $this->m_intPurchasedBy;
	}

	public function sqlPurchasedBy() {
		return ( true == isset( $this->m_intPurchasedBy ) ) ? ( string ) $this->m_intPurchasedBy : 'NULL';
	}

	public function setPurchasedOn( $strPurchasedOn ) {
		$this->set( 'm_strPurchasedOn', CStrings::strTrimDef( $strPurchasedOn, -1, NULL, true ) );
	}

	public function getPurchasedOn() {
		return $this->m_strPurchasedOn;
	}

	public function sqlPurchasedOn() {
		return ( true == isset( $this->m_strPurchasedOn ) ) ? '\'' . $this->m_strPurchasedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, store_product_id, call_agent_rank_id, redeemed_coins, credit_balance, credit_expense, redeemed_by, redeemed_on, received_by, received_on, released_by, released_on, created_by, created_on, purchased_by, purchased_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlStoreProductId() . ', ' .
						$this->sqlCallAgentRankId() . ', ' .
						$this->sqlRedeemedCoins() . ', ' .
						$this->sqlCreditBalance() . ', ' .
						$this->sqlCreditExpense() . ', ' .
						$this->sqlRedeemedBy() . ', ' .
						$this->sqlRedeemedOn() . ', ' .
						$this->sqlReceivedBy() . ', ' .
						$this->sqlReceivedOn() . ', ' .
						$this->sqlReleasedBy() . ', ' .
						$this->sqlReleasedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPurchasedBy() . ', ' .
						$this->sqlPurchasedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' store_product_id = ' . $this->sqlStoreProductId(). ',' ; } elseif( true == array_key_exists( 'StoreProductId', $this->getChangedColumns() ) ) { $strSql .= ' store_product_id = ' . $this->sqlStoreProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_rank_id = ' . $this->sqlCallAgentRankId(). ',' ; } elseif( true == array_key_exists( 'CallAgentRankId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_rank_id = ' . $this->sqlCallAgentRankId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redeemed_coins = ' . $this->sqlRedeemedCoins(). ',' ; } elseif( true == array_key_exists( 'RedeemedCoins', $this->getChangedColumns() ) ) { $strSql .= ' redeemed_coins = ' . $this->sqlRedeemedCoins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_balance = ' . $this->sqlCreditBalance(). ',' ; } elseif( true == array_key_exists( 'CreditBalance', $this->getChangedColumns() ) ) { $strSql .= ' credit_balance = ' . $this->sqlCreditBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_expense = ' . $this->sqlCreditExpense(). ',' ; } elseif( true == array_key_exists( 'CreditExpense', $this->getChangedColumns() ) ) { $strSql .= ' credit_expense = ' . $this->sqlCreditExpense() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redeemed_by = ' . $this->sqlRedeemedBy(). ',' ; } elseif( true == array_key_exists( 'RedeemedBy', $this->getChangedColumns() ) ) { $strSql .= ' redeemed_by = ' . $this->sqlRedeemedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' redeemed_on = ' . $this->sqlRedeemedOn(). ',' ; } elseif( true == array_key_exists( 'RedeemedOn', $this->getChangedColumns() ) ) { $strSql .= ' redeemed_on = ' . $this->sqlRedeemedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy(). ',' ; } elseif( true == array_key_exists( 'ReceivedBy', $this->getChangedColumns() ) ) { $strSql .= ' received_by = ' . $this->sqlReceivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn(). ',' ; } elseif( true == array_key_exists( 'ReceivedOn', $this->getChangedColumns() ) ) { $strSql .= ' received_on = ' . $this->sqlReceivedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy(). ',' ; } elseif( true == array_key_exists( 'ReleasedBy', $this->getChangedColumns() ) ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn(). ',' ; } elseif( true == array_key_exists( 'ReleasedOn', $this->getChangedColumns() ) ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_by = ' . $this->sqlPurchasedBy(). ',' ; } elseif( true == array_key_exists( 'PurchasedBy', $this->getChangedColumns() ) ) { $strSql .= ' purchased_by = ' . $this->sqlPurchasedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_on = ' . $this->sqlPurchasedOn(). ',' ; } elseif( true == array_key_exists( 'PurchasedOn', $this->getChangedColumns() ) ) { $strSql .= ' purchased_on = ' . $this->sqlPurchasedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'store_product_id' => $this->getStoreProductId(),
			'call_agent_rank_id' => $this->getCallAgentRankId(),
			'redeemed_coins' => $this->getRedeemedCoins(),
			'credit_balance' => $this->getCreditBalance(),
			'credit_expense' => $this->getCreditExpense(),
			'redeemed_by' => $this->getRedeemedBy(),
			'redeemed_on' => $this->getRedeemedOn(),
			'received_by' => $this->getReceivedBy(),
			'received_on' => $this->getReceivedOn(),
			'released_by' => $this->getReleasedBy(),
			'released_on' => $this->getReleasedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'purchased_by' => $this->getPurchasedBy(),
			'purchased_on' => $this->getPurchasedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>