<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseCallAnalysisStatusTypes extends CEosPluralBase {

	/**
	 * @return CCallAnalysisStatusType[]
	 */
	public static function fetchCallAnalysisStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCallAnalysisStatusType::class, $objDatabase );
	}

	/**
	 * @return CCallAnalysisStatusType
	 */
	public static function fetchCallAnalysisStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCallAnalysisStatusType::class, $objDatabase );
	}

	public static function fetchCallAnalysisStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'call_analysis_status_types', $objDatabase );
	}

	public static function fetchCallAnalysisStatusTypeById( $intId, $objDatabase ) {
		return self::fetchCallAnalysisStatusType( sprintf( 'SELECT * FROM call_analysis_status_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchCallAnalysisStatusTypesByCallAnalysisStatusTypeId( $intCallAnalysisStatusTypeId, $objDatabase ) {
		return self::fetchCallAnalysisStatusTypes( sprintf( 'SELECT * FROM call_analysis_status_types WHERE call_analysis_status_type_id = %d', $intCallAnalysisStatusTypeId ), $objDatabase );
	}

}
?>