<?php

class CCallFollowup extends CBaseCallFollowup {

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
        	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Client Id is required.' ) );
        }
        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
        	$boolIsValid = false;
        	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Property Id is required.' ) );
        }
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        		$boolIsValid &= $this->valCid();
        		$boolIsValid &= $this->valPropertyId();
        		break;

        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>