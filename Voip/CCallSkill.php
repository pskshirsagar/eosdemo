<?php

class CCallSkill extends CBaseCallSkill {

	protected $m_strCallQueueName;

	/**
	* Set Functions
	*/

 public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_queue_name'] ) ) $this->setCallQueueName( $arrmixValues['call_queue_name'] );

		return;
 }

 public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
 }

	/**
	* Get Functions
	*/

 public function getCallQueueName() {
		return $this->m_strCallQueueName;
 }

	/**
	* Validate Functions
	*/

 public function valConflictName( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( true == valId( $this->fetchCallSkillCountByName( $objVoipDatabase ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use.' ) );
		}

		return $boolIsValid;
 }

 public function validate( $strAction, $objVoipDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictName( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
 }

	/**
	* Create Functions
	*/

 public function createCallSkillQueue() {
		$objCallSkillQueue = new CCallSkillQueue();
		$objCallSkillQueue->setCallSkillId( $this->getId() );
		return $objCallSkillQueue;
 }

 public function createCallAgentSkill() {
		$objCallAgentSkill = new CCallAgentSkill();
		$objCallAgentSkill->setCallSkillId( $this->getId() );
		return $objCallAgentSkill;
 }

	/**
	* fetch functions
	*/

 public function fetchCallSkillCountByName( $objVoipDatabase ) {
		$strWhere = 'WHERE LOWER(name) = LOWER(\'' . $this->getName() . '\')';

		if( true == valId( $this->getId() ) ) {
			$strWhere .= ' AND id <> ' . ( int ) $this->getId();
		}

		return CCallSkills::fetchCallSkillCount( $strWhere, $objVoipDatabase );
 }

 public function fetchCallSkillQueues( $objVoipDatabase ) {
		return CCallSkillQueues::fetchCallSkillQueues( 'SELECT * FROM call_skill_queues WHERE call_skill_id = ' . ( int ) $this->getId(), $objVoipDatabase );
 }

}
?>