<?php

class COccurrenceType extends CBaseOccurrenceType {

	const REPORTED_TARDY			= 1;
	const UNREPORTED_TARDY			= 2;
	const APPROVED_EARLY_LEAVE		= 3;
	const UNAPPROVED_EARLY_LEAVE	= 4;
	const REPORTED_ABSENCE			= 5;
	const UNREPORTED_ABSENCE		= 6;
	const CREDIT					= 7;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>