<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSkillQueues
 * Do not add any new functions to this class.
 */

class CCallSkillQueues extends CBaseCallSkillQueues {

	public static function fetchCallSkillQueuesByCallSkillIdByCallQueueIds( $intCallSkillId, $arrintCallQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallQueueIds ) ) return NULL;

		$strSql = 'SELECT
						csq.id,
						cq.id AS call_queue_id,
						ca.id AS call_agent_id,
						cq.queue_phone_extension_id,
						cq.domain AS call_queue_domain,
						ca.domain AS call_agent_domain,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						caq.level AS call_agent_queue_level,
						caq.position AS call_agent_queue_position,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_skill_queues AS csq
						JOIN call_skills AS cs ON ( cs.id = csq.call_skill_id )
						JOIN call_queues AS cq ON ( cq.id = csq.call_queue_id )
						JOIN call_agent_queues AS caq ON ( caq.call_queue_id = cq.id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id AND csq.call_skill_id = cas.call_skill_id )
						JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						cs.id = ' . ( int ) $intCallSkillId . '
						AND cq.id IN ( ' . implode( ', ', $arrintCallQueueIds ) . ' )
					GROUP BY
						csq.id,
						cq.id,
						ca.id,
						cq.queue_phone_extension_id,
						call_queue_domain,
						call_agent_domain,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						call_agent_queue_level,
						call_agent_queue_position,
						call_agent_state_type_name,
						call_agent_status_type_name,
						caq.override_priority,
						caq.override_expiration,
						csq.priority';

		return self::fetchCallSkillQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallSkillQueueByCallSkillId( $intCallSkillId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_skill_queues WHERE call_skill_id = ' . ( int ) $intCallSkillId . ' LIMIT 1';
		return self::fetchCallSkillQueue( $strSql, $objVoipDatabase );
	}
}
?>