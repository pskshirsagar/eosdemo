<?php

class CCallAgentPinType extends CBaseCallAgentPinType {

	protected $m_strCallAgentPinGroupName;
	protected $m_boolIsCallAgentPinAchieved;

	// Quality Assuarance
	const CALL_QUALITY_SCORE_I				= 1;
	const CALL_QUALITY_SCORE_II				= 2;
	const CALL_QUALITY_SCORE_III			= 3;

	// Joneses Club
	const JONESES_CLUB_I					= 4;
	const JONESES_CLUB_II					= 5;
	const JONESES_CLUB_III					= 6;
	const JONESES_CLUB_IV					= 30;

	// Operational
	const OPERATIONAL_I						= 7;
	const OPERATIONAL_II					= 8;
	const OPERATIONAL_III					= 9;
	const OPERATIONAL_IV					= 10;
	const VALUES_WEEK_2020					= 31;

	// Phone Call Milestones
	const PHONE_CALL_MILESTONE_I			= 11;
	const PHONE_CALL_MILESTONE_II			= 12;

	// Agent Designation.
	const LEASING_PROFESSIONAL_I			= 13;
	const LEASING_PROFESSIONAL_II			= 14;
	const LEASING_PROFESSIONAL_III			= 15;
	const LEASING_PROFESSIONAL_IV			= 16;

	// Work Anniversary
	const WORK_ANNIVERSARY_I				= 17;
	const WORK_ANNIVERSARY_II				= 18;
	const WORK_ANNIVERSARY_III				= 19;
	const WORK_ANNIVERSARY_IV				= 20;

	// Special Teams
	const WORK_AT_HOME_TEAM_MEMBER			= 21;
	const RESIDENT_SUPPORT_TEAM_MEMBER		= 22;
	const COMMUNICATION_TEAM_MEMBER			= 23;
	const COACH								= 24;
	const QUALITY_ASSURANCE_MEMBER			= 25;
	const SPANISH_SPEAKING_MEMBER			= 26;

	// Strategic Client Certification
	const MONOGRAM_TRAINING					= 27;
	const HANOVER_CERTIFICATION_PROGRAM		= 28;
	const LANDMARK_CUSTOMER_CARE_TRAINING	= 29;

	const CALL_QUALITY_SCORE_I_PERCENTAGE	= 93;
	const CALL_QUALITY_SCORE_II_PERCENTAGE	= 95;
	const CALL_QUALITY_SCORE_III_PERCENTAGE	= 100;

	public static $c_arrintCallAgentWorkAnniversaryPinTypeIds = [
		CEmployee::ONE_YEAR		=> self::WORK_ANNIVERSARY_I,
		CEmployee::TWO_YEAR		=> self::WORK_ANNIVERSARY_II,
		CEmployee::THREE_YEAR	=> self::WORK_ANNIVERSARY_III,
		CEmployee::FOUR_YEAR	=> self::WORK_ANNIVERSARY_IV
	];

	public static $c_arrintEarnedCallAgentPinTypeIds = [
		self::OPERATIONAL_IV,
		self::WORK_AT_HOME_TEAM_MEMBER,
		self::RESIDENT_SUPPORT_TEAM_MEMBER,
		self::COMMUNICATION_TEAM_MEMBER,
		self::COACH,
		self::QUALITY_ASSURANCE_MEMBER,
		self::SPANISH_SPEAKING_MEMBER,
		self::MONOGRAM_TRAINING,
		self::HANOVER_CERTIFICATION_PROGRAM,
		self::LANDMARK_CUSTOMER_CARE_TRAINING
	];

	public static $c_arrstrCallAgentPinTypeTickers = [
		self::CALL_QUALITY_SCORE_I				=> 'Received the 93% Graded Call Pin!',
		self::CALL_QUALITY_SCORE_II				=> 'Received the 95% Graded Call Pin!',
		self::CALL_QUALITY_SCORE_III			=> 'Received the 100% Graded Call Pin!',
		self::PHONE_CALL_MILESTONE_I			=> 'Made 10K Phone Calls',
		self::PHONE_CALL_MILESTONE_II			=> 'Made 20K Phone Calls',
		self::LEASING_PROFESSIONAL_I			=> 'Became a level 1 Agent',
		self::LEASING_PROFESSIONAL_II			=> 'Became a level 2 Agent',
		self::LEASING_PROFESSIONAL_III			=> 'Became a level 3 Agent',
		self::LEASING_PROFESSIONAL_IV			=> 'Became a level 4 Agent',
		self::WORK_ANNIVERSARY_I				=> 'Celebrating 1 year at Entrata',
		self::WORK_ANNIVERSARY_II				=> 'Celebrating 2 year at Entrata',
		self::WORK_ANNIVERSARY_III				=> 'Celebrating 3 year at Entrata',
		self::WORK_ANNIVERSARY_IV				=> 'Celebrating 4 year at Entrata',
		self::JONESES_CLUB_I					=> 'Made it into the Joneses Club',
		self::JONESES_CLUB_II					=> 'Joneses Club for over 3 Months',
		self::JONESES_CLUB_III					=> 'Made it into the Super Joneses Club',
		self::JONESES_CLUB_IV					=> 'Made it into the Super Joneses Club Turkey',
		self::OPERATIONAL_I						=> 'Wrap-up time below 20 seconds / quarter',
		self::OPERATIONAL_II					=> 'Perfect Attendance / quarter',
		self::OPERATIONAL_III					=> 'Adherence over 95% / quarter',
		self::OPERATIONAL_IV					=> 'Received Client Feedback for being Exceptional',
		self::WORK_AT_HOME_TEAM_MEMBER			=> 'Became a member of the Work At Home team',
		self::RESIDENT_SUPPORT_TEAM_MEMBER		=> 'Became a member of the Resident Support team',
		self::COMMUNICATION_TEAM_MEMBER			=> 'Became a member of the Communications team',
		self::COACH								=> 'Became a coach',
		self::QUALITY_ASSURANCE_MEMBER			=> 'Became a member of the QA team',
		self::SPANISH_SPEAKING_MEMBER			=> 'Became a member of the Spanish Speaking team',
		self::MONOGRAM_TRAINING					=> 'Completed Monogram Training',
		self::HANOVER_CERTIFICATION_PROGRAM		=> 'Completed Hanover Certification Program',
		self::LANDMARK_CUSTOMER_CARE_TRAINING	=> 'Completed Landmark Customer Care Training',
		self::VALUES_WEEK_2020					=> 'Awarded Values Week Pin'
	];

	public static $c_arrstrRewardPointSettingNames = [
		self::CALL_QUALITY_SCORE_I		=> '>93% Graded Call',
		self::CALL_QUALITY_SCORE_II		=> '>95% Graded Call',
		self::CALL_QUALITY_SCORE_III	=> '100% Graded Call',
		self::JONESES_CLUB_I			=> 'Joneses Club',
		self::JONESES_CLUB_II			=> 'Joneses Club Turkey',
		self::JONESES_CLUB_III			=> 'Super Joneses Club',
		self::JONESES_CLUB_IV			=> 'Super Joneses Club Turkey',
		self::OPERATIONAL_II			=> 'Perfect Attendance',
		self::OPERATIONAL_III			=> '>95% Adherence',
		self::WORK_ANNIVERSARY_I		=> '1st Annual Anniversary Since Hire-date',
		self::WORK_ANNIVERSARY_II		=> '2nd Annual Anniversary Since Hire-date',
		self::WORK_ANNIVERSARY_III		=> '3rd Annual Anniversary Since Hire-date',
		self::WORK_ANNIVERSARY_IV		=> '4th Annual Anniversary Since Hire-date'
	];

	public static $c_arrmixCallAgentPinTypes = [
		self::PHONE_CALL_MILESTONE_II			=> 'Phone Call Milestone II',
		self::PHONE_CALL_MILESTONE_I			=> 'Phone Call Milestone I',
		self::WORK_ANNIVERSARY_IV				=> '4th Annual Anniversary',
		self::WORK_ANNIVERSARY_III				=> '3rd Annual Anniversary',
		self::WORK_ANNIVERSARY_II				=> '2nd Annual Anniversary',
		self::WORK_ANNIVERSARY_I				=> '1st Annual Anniversary',
		self::LEASING_PROFESSIONAL_I			=> 'Leasing Professional I',
		self::LEASING_PROFESSIONAL_II			=> 'Leasing Professional II',
		self::LEASING_PROFESSIONAL_III			=> 'Leasing Professional III',
		self::LEASING_PROFESSIONAL_IV			=> 'Leasing Professional IV',
		self::WORK_AT_HOME_TEAM_MEMBER			=> 'Work At Home Team Member',
		self::RESIDENT_SUPPORT_TEAM_MEMBER		=> 'Resident Support Team Member',
		self::COMMUNICATION_TEAM_MEMBER			=> 'Communication Team Member',
		self::COACH								=> 'Coach',
		self::QUALITY_ASSURANCE_MEMBER			=> 'Quality Assurance Member'
	];

	public function getCallAgentPinGroupName() {
		return $this->m_strCallAgentPinGroupName;
	}

	public function getIsCallAgentPinAchieved() {
		return $this->m_boolIsCallAgentPinAchieved;
	}

	public function setCallAgentPinGroupName( $strCallAgentPinGroupName ) {
		return $this->m_strCallAgentPinGroupName = $strCallAgentPinGroupName;
	}

	public function setIsCallAgentPinAchieved( $boolIsCallAgentPinAchieved ) {
		return $this->m_boolIsCallAgentPinAchieved = $boolIsCallAgentPinAchieved;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['call_agent_pin_group_name'] ) )	$this->setCallAgentPinGroupName( $arrmixValues['call_agent_pin_group_name'] );
		if( true == isset( $arrmixValues['is_call_agent_pin_achieved'] ) )	$this->setIsCallAgentPinAchieved( $arrmixValues['is_call_agent_pin_achieved'] );
		return true;
	}

}
?>