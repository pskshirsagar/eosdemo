<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisResults
 * Do not add any new functions to this class.
 */

class CCallAnalysisResults extends CBaseCallAnalysisResults {

	public static function fetchCallAnalysisResultsByCallAnalysisQuestionIdsByCallId( $arrintCallAnalysisQuestionIds, $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_analysis_results WHERE call_id= ' . ( int ) $intCallId . ' AND call_analysis_question_id in( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
		return self::fetchCallAnalysisResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisResultsByCidByCallId( $intCid, $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_analysis_results WHERE call_id= ' . ( int ) $intCallId . ' AND cid = ' . ( int ) $intCid;
		return self::fetchCallAnalysisResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoryWeightsByCidByCallAnalysisCategoryIdsByCallId( $intCid, $arrintCallAnalysisCategoryIds, $intCallId, $objVoipDatabase ) {
		$strSql = '
			SELECT
				caq.call_analysis_category_id,
				CASE
					WHEN TRUE = ( c.details ->> \'is_new_scorecard\' )::BOOLEAN THEN SUM( ( car.details ->> \'max_possible_weight\' )::INTEGER )
					ELSE SUM( car.weight )
				END AS total_weight,
				CASE
					WHEN TRUE = ( c.details ->> \'is_new_scorecard\' )::BOOLEAN THEN SUM( car.weight )
					ELSE SUM(
							CASE
								WHEN car.is_qualified = 1 THEN car.weight
							ELSE 0
						END
					)
				END AS achieved_weight
			FROM
				call_analysis_questions caq
				JOIN call_analysis_results car ON car.cid = caq.cid AND car.call_analysis_question_id = caq.id
				JOIN calls c ON c.cid = car.cid AND c.id = car.call_id AND c.analyzed_by IS NOT NULL AND c.analyzed_on IS NOT NULL
			WHERE
				caq.cid = ' . ( int ) $intCid . '
				AND	caq.call_analysis_category_id IN( ' . implode( ',', $arrintCallAnalysisCategoryIds ) . ' )
				AND car.call_id = ' . ( int ) $intCallId . '
			GROUP BY
				caq.call_analysis_category_id,
				c.details ->> \'is_new_scorecard\'
			';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisWeightsByCallIdByCid( $intCallId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intCallId ) && false == valId( $intCid ) ) return NULL;

		$strFromSql = ' calls c
						JOIN call_analysis_results car ON ( car.cid = c.cid AND c.id = car.call_id )
						JOIN call_analysis_questions caq ON ( caq.cid = car.cid AND car.call_analysis_question_id = caq.id )
						JOIN call_analysis_categories cac ON ( cac.cid = caq.cid AND caq.call_analysis_category_id = cac.id )
						JOIN call_analysis_scorecards cas ON ( cas.cid = cac.cid AND cac.call_analysis_scorecard_id = cas.id ) ';

		$strWhereSql = ' caq.cid =' . ( int ) $intCid . '
						AND car.call_id = ' . ( int ) $intCallId;

		$strTotalWeightDetailSql = ' WITH total_weight_detail AS (
											SELECT
												SUM ( car.weight ) AS total_weight
											FROM '
												. $strFromSql . '
											WHERE
												' . $strWhereSql . '
												AND ( car.is_qualified = 1
													OR car.is_qualified = 0 ) ),';

		$strAchievedWeightDetailSql = ' achieved_weight_detail AS (
												SELECT
													SUM ( car.weight ) AS achieved_weight
												FROM '
													. $strFromSql . '
												WHERE '
													. $strWhereSql . '
													AND car.is_qualified = 1
													AND c.analyzed_on IS NOT NULL )';

		$strSql = $strTotalWeightDetailSql . $strAchievedWeightDetailSql;

		$strSql .= ' SELECT
						COALESCE ( twd.total_weight, 1 ) AS total_weight,
						COALESCE ( awd.achieved_weight, 0 ) AS achieved_weight,
						( CASE
							WHEN 0 = twd.total_weight OR 0 = awd.achieved_weight THEN 0
							ELSE round( ( ( awd.achieved_weight / twd.total_weight ) * 100 ), 2 )
						END) AS average_call_score
					FROM
						total_weight_detail twd,
						achieved_weight_detail awd';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallScoreFromCallAnalysisResultsByCallIds( $arrintCallIds, $objVoipDatabase ) {

		if( false == valArr( $arrintCallIds ) ) return false;

		$strSql = ' SELECT 
						car.call_id,
						( CASE
							WHEN true = ( c.details->>\'is_new_scorecard\' )::BOOLEAN THEN
								ROUND( SUM( car.weight ) / SUM( ( car.details->>\'max_possible_weight\' )::INTEGER ) * 100, 2 )
							ELSE
						ROUND( (
							SUM( 
								CASE
									WHEN car.is_qualified = 1 THEN car.weight ELSE 0
								END
							) /
							NULLIF(
								SUM(
									CASE
										WHEN car.is_qualified IN ( 1, 0 ) THEN car.weight ELSE 0
									END
								), 0
							)
								)* 100, 2 ) 
							END
						) AS score
					FROM 
						call_analysis_results car
						JOIN calls c ON c.id = car.call_id
					WHERE
						car.call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )
						AND c.analyzed_by IS NOT NULL
						AND c.analyzed_on IS NOT NULL
					GROUP BY
						car.call_id,
						c.details->>\'is_new_scorecard\'';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisScoreByCallId( $intCallId, $objVoipDatabase ) {
		if( false == valId( $intCallId ) ) return false;

		$strSql = 'SELECT
						ROUND( SUM( car.weight ), 2 ) AS call_analysis_score,
						car.call_id
					FROM
						call_analysis_results car
					WHERE
						car.call_id = ' . ( int ) $intCallId . '
					GROUP BY
							car.call_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>