<?php

class CCallWordCorrelation extends CBaseCallWordCorrelation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpeaker() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsAbandonedCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsLeadCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsMarketSurveyCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsResidentCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSolicitorCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSupportCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsVendorCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWorkOrderCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWrongNumberCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAverageCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadIsConvertedCc() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsAbandonedCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsLeadCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsMarketSurveyCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsResidentCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSolicitorCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSupportCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsVendorCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWorkOrderCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWrongNumberCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAverageCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadIsConvertedCcRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>