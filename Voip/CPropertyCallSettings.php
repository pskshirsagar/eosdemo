<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyCallSettings
 * Do not add any new functions to this class.
 */

class CPropertyCallSettings extends CBasePropertyCallSettings {

	public static function fetchPropertyCallSettingsByCidByPropertyId( $intCid, $intPropertyId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						property_call_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyCallSetting( $strSql, $objVoipDatabase );
	}

	public static function fetchVoicemailEnabledCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT COUNT( use_psi_voicemail ) FROM property_call_settings WHERE cid = ' . ( int ) $intCid . ' AND property_id::int IN (' . implode( ',', $arrintPropertyIds ) . ')';

		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		return ( false == is_null( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : NULL;
	}

	public static function fetchCallsCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= 'SELECT
						lead_routing_condition,
						COUNT(lead_routing_condition) AS count_leads_calls,
						maintenance_routing_condition,
						COUNT(maintenance_routing_condition) AS count_maintenance_calls,
						maintenance_emergency_routing_condition,
						COUNT(maintenance_emergency_routing_condition) AS count_maintenance_emergency_calls
					FROM
						property_call_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					GROUP BY
						lead_routing_condition,
						maintenance_routing_condition,
						maintenance_emergency_routing_condition';

		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		return ( false == valArr( $arrintResponse ) ) ? NULL : $arrintResponse;
	}

	public static function fetchPropertyCallSettingByCidByPropertyId( $intCid, $intPropertyId, $objVoipDatabase ) {
		return self::fetchPropertyCallSetting( 'SELECT * FROM property_call_settings WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId, $objVoipDatabase );
	}

	public static function fetchPropertyCallSettingsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_call_settings
					WHERE
						cid IN (' . implode( ',', $arrintCids ) . ')
						AND property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return self::fetchPropertyCallSettings( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsByLeasingCenterImplementationFilterByCids( $objLeasingCenterImplementationFilter, $arrobjCids, $objVoipDatabase ) {
		$strWhereSql = '';

		if( true == valStr( $objLeasingCenterImplementationFilter->getCids() ) ) {
			$strWhereSql .= ' AND pcs.cid IN ( ' . $objLeasingCenterImplementationFilter->getCids() . ' ) ';
		}

		if( true == valStr( $objLeasingCenterImplementationFilter->getPropertyIds() ) ) {
			$strWhereSql .= ' AND pcs.property_id IN ( ' . $objLeasingCenterImplementationFilter->getPropertyIds() . ' ) ';
		}

		$strSql = 'SELECT
						*
					FROM (
							SELECT
									pcs.cid,
									pcs.property_id,
									pcs.property_name,
									pcs.lead_routing_condition,
									pcs.maintenance_routing_condition,
									pcs.maintenance_emergency_routing_condition,
									pcs.is_guest_card_routing_to_psi,
									c.call_datetime AS live_date
								FROM
								 	property_call_settings AS pcs
									JOIN (

										SELECT
											property_id,
											MIN(call_datetime) AS call_datetime
										FROM
											calls
										WHERE
											call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterCallTypes() ) . ' )
											AND cid IN ( ' . implode( ',', array_keys( $arrobjCids ) ) . ' )
										GROUP BY
											property_id
									) AS c ON ( pcs.property_id = c.property_id AND c.call_datetime <= \'' . $objLeasingCenterImplementationFilter->getEndDate() . ' \' )
								WHERE
									( pcs.lead_routing_condition <> \'NONE\'
									OR pcs.maintenance_routing_condition <> \'NONE\'
									OR pcs.maintenance_emergency_routing_condition <> \'NONE\'
									OR pcs.is_guest_card_routing_to_psi <> \'0\' ) ' . $strWhereSql . '
								ORDER BY
									pcs.cid ) subquery
						WHERE 1 = 1';

		if( true == valId( $objLeasingCenterImplementationFilter->getIsLeadCall() ) ) {
			$strSql .= ' AND subquery.lead_routing_condition <> \'NONE\' ';
		}

		if( true == valId( $objLeasingCenterImplementationFilter->getIsResidentCall() ) ) {
			$strSql .= ' AND  ( subquery.maintenance_routing_condition <> \'NONE\'
						OR subquery.maintenance_emergency_routing_condition <> \'NONE\' ) ';
		}

		if( true == valId( $objLeasingCenterImplementationFilter->getIsProspectFollowUp() ) ) {
			$strSql .= ' AND subquery.is_guest_card_routing_to_psi <> \'0\'';
		}

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsWithIntervalsByLeasingCenterImplementationFilterByCids( $objLeasingCenterImplementationFilter, $arrobjCids, $objVoipDatabase ) {
		$strSql = ' WITH interval AS (
						SELECT
							to_char( n,\'yyyy-mm-dd\' )::date AS date
						FROM generate_series( \'' . $objLeasingCenterImplementationFilter->getStartDate() . ' \', \'' . $objLeasingCenterImplementationFilter->getEndDate() . ' \',  \'1 ' . $objLeasingCenterImplementationFilter->getInterval() . '\'::INTERVAL ) n
					)
					SELECT
						interval.date,
						COUNT( DISTINCT ( subquery.cid ) ) AS companies_on_live,
						COUNT ( subquery.property_id ) AS properties_on_live,
						ARRAY_AGG( subquery.property_id ) AS property_ids
					FROM
						interval
						LEFT JOIN (
							SELECT
								DISTINCT pcs.property_id,
								pcs.cid,
								pc.call_datetime
							FROM
								property_call_settings AS pcs
								JOIN (
									SELECT
										property_id,
										MIN( call_datetime ) AS call_datetime
									FROM
										calls
									WHERE
										call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterCallTypes() ) . ' )
										AND property_id IS NOT NULL
										AND cid IN ( ' . implode( ',', array_keys( $arrobjCids ) ) . ' )
			 						GROUP BY
										property_id
								) AS pc ON ( pc.property_id = pcs.property_id )
							WHERE
								pcs.lead_routing_condition <> \'NONE\'
								OR pcs.maintenance_routing_condition <> \'NONE\'
								OR pcs.maintenance_emergency_routing_condition <> \'NONE\'
								OR pcs.is_guest_card_routing_to_psi <> \'0\'
						) AS subquery ON ( subquery.call_datetime <= interval.date )
					GROUP BY
						interval.date';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchPropertyCallSettingByPropertyId( $intPropertyId, $objVoipDatabase ) {
		return self::fetchPropertyCallSetting( 'SELECT * FROM property_call_settings WHERE property_id = ' . ( int ) $intPropertyId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchPropertyCallSettingInfoByPropertyId( $intPropertyId, $objVoipDatabase ) {
		return self::fetchPropertyCallSetting( 'SELECT pcs.*, c.database_id AS client_database_id FROM property_call_settings AS pcs, clients AS c WHERE c.id = pcs.cid AND pcs.property_id = ' . ( int ) $intPropertyId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsByPropertyIdsByCids( $arrintPropertyIds, $arrintCids, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						cid,
						property_id,
						property_name
					FROM
						property_call_settings
					WHERE
						property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' )
						AND cid IN ( ' . implode( ', ', $arrintCids ) . ' ) ';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingByCidByPropertyId( $intCid, $intPropertyId, $objVoipDatabase, $boolIsOnlyNotNullMarketingPropertyNameProperty = false ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strSqlNotNullMarketingPropertyNameClause = ( true == $boolIsOnlyNotNullMarketingPropertyNameProperty ) ? ' AND marketing_property_name IS NOT NULL ' : NULL;

		$strSql = 'SELECT
						cid,
						property_id,
						property_name,
						marketing_property_name
					FROM
						property_call_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . $strSqlNotNullMarketingPropertyNameClause;

		return fetchData( $strSql, $objVoipDatabase )[0];
	}

	public static function fetchSimplePropertyCallSettingsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objVoipDatabase, $boolIsOnlyNotNullMarketingPropertyNameProperties = false ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlNotNullMarketingPropertyNameClause = ( true == $boolIsOnlyNotNullMarketingPropertyNameProperties ) ? ' AND marketing_property_name IS NOT NULL ' : NULL;

		$strSql = 'SELECT
						cid,
						property_id,
						property_name,
						marketing_property_name
					FROM
						property_call_settings
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' . $strSqlNotNullMarketingPropertyNameClause . '
					ORDER BY
						property_name,
						marketing_property_name';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomPropertyCallSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objDatabase ) {
		$strSql = 'SELECT id, property_id,seconds_to_ring_phone FROM property_call_settings WHERE property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) ' . ' AND cid = ' . ( int ) $intCid;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomSecondsToRingPhonePropertyCallSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pcs.seconds_to_ring_phone,
						pcs.property_id
					FROM
						property_call_settings pcs
					WHERE
						pcs.seconds_to_ring_phone <= 20
						AND pcs.cid::integer = ' . ( int ) $intCid . '
						AND pcs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomLeasingPropertyPreferencesSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pcs.property_id AS property_id
					FROM
						property_call_settings pcs
					WHERE
					    ( pcs.lead_routing_condition != \'NONE\' OR pcs.maintenance_routing_condition != \'NONE\' OR pcs.maintenance_emergency_routing_condition != \'NONE\' )
						AND pcs.cid::integer = ' . ( int ) $intCid . '
						AND pcs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomEntrataVoiceMailPropertyCallSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pcs.use_psi_voicemail as value,
						pcs.property_id
					FROM
						property_call_settings pcs
					WHERE
						pcs.use_psi_voicemail = 0
						AND pcs.cid::integer = ' . ( int ) $intCid . '
						AND pcs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomOfficeTypePropertyCallSettingsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT pcs.property_id,
						(CASE WHEN pcs.property_id IS NOT NULL THEN 1 ELSE NULL END ) AS value
					FROM
						property_call_settings pcs
						JOIN call_phone_numbers cpn ON ( pcs.cid = cpn.cid AND pcs.property_id = cpn.property_id )
					WHERE
					    cpn.call_type_id = ' . CCallType::CALL_TRACKING_OFFICE . '
						AND pcs.is_office_forwarding = 1
						AND cpn.released_on IS NULL
						AND cpn.suspended_on IS NULL
						AND pcs.cid::integer = ' . ( int ) $intCid . '
						AND pcs.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsByCid( $intCid, $objVoipDatabase ) {
		return fetchData( 'SELECT property_id, property_name FROM property_call_settings WHERE cid =' . ( int ) $intCid, $objVoipDatabase );
	}

	public static function fetchPropertyCallSettingDialPlanDetailsByCidByPropertyId( $intCid, $intPropertyId, $objVoipDatabase ) {
		$strSql = 'SELECT
						pcs.*,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'sun\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.sun_open_time
							END AS sun_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'sun\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.sun_close_time
						END AS sun_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'mon\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.mon_open_time
						END AS mon_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'mon\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.mon_close_time
						END AS mon_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'tue\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.tue_open_time
						END AS tue_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'tue\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.tue_close_time
						END AS tue_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'wed\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.wed_open_time
						END AS wed_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'wed\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.wed_close_time
						END AS wed_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'thu\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.thu_open_time
						END AS thu_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'thu\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.thu_close_time
						END AS thu_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'fri\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.fri_open_time
						END AS fri_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'fri\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.fri_close_time
						END AS fri_close_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'sat\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN open_time
							ELSE pcs.sat_open_time
						END AS sat_open_time,
						CASE
							WHEN( pch.off_date IS NOT NULL AND \'sat\' = to_char( CURRENT_DATE,\'dy\' ) ) 
								THEN close_time
							ELSE pcs.sat_close_time
						END AS sat_close_time
					FROM
						property_call_settings AS pcs
					LEFT JOIN property_call_holidays AS pch ON ( pch.cid = pcs.cid AND pch.property_id = pcs.property_id AND pch.off_date = CURRENT_DATE )
					WHERE
						pcs.cid = ' . ( int ) $intCid . '
						AND pcs.property_id = ' . ( int ) $intPropertyId;

		return self::fetchPropertyCallSetting( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsByPropertyIds( $arrintPropertyIds, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						pcs.property_name,
						pcs.property_id,
						c.company_name,
						tz.time_zone_name
					FROM
						property_call_settings AS pcs
						JOIN clients AS c ON ( pcs.cid = c.id )
						JOIN time_zones AS tz ON ( tz.id = pcs.time_zone_id )
					WHERE
						pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyCallSettingsByCidsByPropertyIds( $arrintCids, $arrintPropertyIds, $objVoipDatabase ) {
		$strSql = 'SELECT
						pcs.*
						c.company_name,
					FROM
						property_call_settings pcs
						JOIN clients AS c ON ( c.id = pcs.cid )
					WHERE
						pcs.cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND pcs.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					ORDER BY
						pcs.property_id';

		$arrstrProperties = ( array ) fetchData( $strSql, $objVoipDatabase );

		return $arrstrProperties;
	}

}
?>