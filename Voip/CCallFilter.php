<?php

class CCallFilter extends CBaseCallFilter {

	protected $m_boolIsMessage;
	protected $m_boolIsShowUnreadMessage;
	protected $m_boolIsFailed;
	protected $m_boolIsCallQueueTime;
	protected $m_boolIsShowArchiveCalls;
	protected $m_boolIsShowClientsOnMaintenance;
	protected $m_strShowVanity;
	protected $m_boolIsCustom;
	protected $m_boolReportDistinctLeads;
	protected $m_boolCallAgentIsDisabled;
	protected $m_boolIsShowAllCalls;
	protected $m_boolIsCallbackItem;
	protected $m_boolIsLeadItem;
	protected $m_boolIsReviewItem;
	protected $m_boolIsAllQueueItem;
	protected $m_strLeadIds;
	protected $m_strCallId;
	protected $m_strDestinationPhoneNumber;
	protected $m_strVoiceMailType;
	protected $m_strLeads;
	protected $m_strAllLeads;
	protected $m_strGenericData;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strPropertyIds;
	protected $m_strCompanyStatusTypeIds;
	protected $m_strCids;
	protected $m_strCompanyEmployeeIds;
	protected $m_strEmployeeIds;
	protected $m_strLeasingAgentIds;
	protected $m_strCallAgentStatusTypeIds;
	protected $m_strCallAgentPinTypeIds;
	protected $m_strCallAgentIds;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_strPhoneNumber;
	protected $m_strCallTypeIds;
	protected $m_strCallResultIds;
	protected $m_strUtilityBillingCallTypeIds;
	protected $m_intSupportCallTypeId;
	protected $m_strLeadSourceIds;
	protected $m_strVoicemailStatusTypeIds;
	protected $m_strAnswerStatusTypeIds;
	protected $m_strSortBy;
	protected $m_strSortDirection;
	protected $m_strCallIds;
	protected $m_strExcludeEmployeeId;
	protected $m_strAfterDatetime;
	protected $m_strBeforeDatetime;
	protected $m_strPsProductIds;
	protected $m_strGreetingTypeIds;
	protected $m_strCallDirectionTypeIds;
	protected $m_strDataType;
	protected $m_strTeamIds;
	protected $m_strUserIds;
	protected $m_strDepartmentIds;
	protected $m_strCallsState;
	protected $m_strCallAnalyzed;
	protected $m_strCallQueueIds;
	protected $m_strCallAnalysisStatusTypes;
	protected $m_strTableName;
	protected $m_strApplicantApplicationIds;
	protected $m_strCallAgentVacationRequestSatuses;
	protected $m_strReportColumnName;
	protected $m_strReportCallPeriod;
	protected $m_strCallAgentData;
	protected $m_strCallAgentName;
	protected $m_strReportBy;
	protected $m_strOccurrenceTypeIds;
	protected $m_strAction;
	protected $m_strDisputeStatus;
	protected $m_strFollowupTypeIds;
	protected $m_intEmployeeId;
	protected $m_intPropertyId;
	protected $m_intPortfolioId;
	protected $m_intIsCurrentLeadsPageSelected;
	protected $m_intIsAllLeadsPageSelected;
	protected $m_intIsAllCallsSelected;
	protected $m_intCurrentSelectedLeadsPageNo;
	protected $m_intIsAllLeadsDeselected;
	protected $m_intIsAllCallsDeselected;
	protected $m_intPageNo;
	protected $m_intCountPerPage;
	protected $m_intPreviousPageNo;
	protected $m_intCid;
	protected $m_intCallStatusTypeId;
	protected $m_intCallQueueId;
	protected $m_intIntervalTypeId;
	protected $m_intOutboundCallId;
	protected $m_intCallAgentId;
	protected $m_intCustomerId;
	protected $m_intEmployeeStatusTypeId;
	protected $m_intCallFormStatusTypeId;
	protected $m_intCreatedBy;
	protected $m_intCallCount;
	protected $m_intReferenceId;
	protected $m_intCallDurationSeconds;
	protected $m_intPercentageCallCount;
	protected $m_intIntervalDays;
	protected $m_intTranscribedCall;
	protected $m_intDepartmentId;
	protected $m_intArchivedFilter;
	protected $m_intCallResultId;
	protected $m_intWrapUpTime;
	protected $m_intCallSourceId;
	protected $m_intFollowupStatusTypeId;
	protected $m_intFollowupId;
	protected $m_intUserId;
	protected $m_strOffice;
	protected $m_strTabName;
	protected $m_strCountryCodes;
	protected $m_strSearchKeyword;

	protected $m_arrintCallPhoneNumberTypeIds;
	protected $m_arrintVoicemailStatusTypeIds;
	protected $m_arrintLeadSourceIds;
	protected $m_arrintCallStatusTypeIds;
	protected $m_arrintOutboundCallCampaignIds;
	protected $m_arrintCallAgentOnProjectReasonIds;
	protected $m_arrstrDateIntervals;

	const ID_DEFAULT								= 1;
	const PAGE_NO									= 1;
	const COUNT_PER_PAGE							= 10;
	const INTEGRATED								= 1;
	const NON_INTEGRATED							= 0;
	const LEADS_SELECTION_TYPE_SELECT_THIS_PAGE 	= 1;
	const LEADS_SELECTION_TYPE_SELECT_ALL_PAGES 	= 2;
	const LEADS_SELECTION_TYPE_DESELECT_ALL_PAGE 	= 3;
	const CALL_RESULT_TYPE_ANSWERED					= '1';
	const CALL_RESULT_TYPE_UNANSWERED				= '2';
	const VOICE_MAIL_TYPE_VOICE_MAIL				= '1';
	const VOICE_MAIL_TYPE_UNPLAYED					= '2';
	const VOICE_MAIL_TYPE_NO_VOICE_MAIL				= '3';
	const CALL_DIRECTION_TYPE_INBOUND				= '1';
	const CALL_DIRECTION_TYPE_OUTBOUND				= '2';
	const LEASING_CENTER_REPORT_INTERVAL_HOUR		= 1;
	const LEASING_CENTER_REPORT_INTERVAL_DAILY		= 2;
	const LEASING_CENTER_REPORT_INTERVAL_WEEKLY 	= 3;
	const LEASING_CENTER_REPORT_INTERVAL_MONTHLY	= 4;
	const CALL_TRACKER_ARCHIVED_CALLS		= 'archived_calls';
	const CALL_TRACKER_CURRENT_CALLS		= 'current_calls';
	const CALL_TRACKER_SHOW_ALL_CALLS		= 'all_calls';
	const CALL_ANALYZED_YES					= 'yes';
	const CALL_ANALYZED_NO					= 'no';
	const CALL_ANALYZED_ANY					= 'any';
	const OFFICE_DEFAULT					= 'US';

	protected $m_arrmixIntervalTypes = array(
		self::LEASING_CENTER_REPORT_INTERVAL_HOUR		=> 'Hourly',
		self::LEASING_CENTER_REPORT_INTERVAL_DAILY		=> 'Daily',
		self::LEASING_CENTER_REPORT_INTERVAL_WEEKLY		=> 'Weekly',
		self::LEASING_CENTER_REPORT_INTERVAL_MONTHLY	=> 'Monthly'
	);

	public function __construct() {
		parent::__construct();
		return;
	}

	/**
	 * Sql Functions
	 *
	 */

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function sqlIsMessage() {
		return ( true == isset( $this->m_boolIsMessage ) ) ? ( string ) $this->m_boolIsMessage : '0';
	}

	public function sqlIsFailed() {
		return ( true == isset( $this->m_boolIsFailed ) ) ? ( string ) $this->m_boolIsFailed : 'NULL';
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCallAnalyzed() {
		return $this->m_strCallAnalyzed;
	}

	public function getVoiceMailType() {
		return $this->m_strVoiceMailType;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getCallPhoneNumberTypeIds() {
		return $this->m_arrintCallPhoneNumberTypeIds;
	}

	public function getIsMessage() {
		return $this->m_boolIsMessage;
	}

	public function getIsShowUnreadMessage() {
		return $this->m_boolIsShowUnreadMessage;
	}

	public function getAllLeads() {
		return $this->m_strAllLeads;
	}

	public function getGenericData() {
		return $this->m_strGenericData;
	}

	public function getIntervalTypeId() {
		return $this->m_intIntervalTypeId;
	}

	public function getOutboundCallId() {
		return $this->m_intOutboundCallId;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function getCallFormStatusTypeId() {
		return $this->m_intCallFormStatusTypeId;
	}

	public function getIntervalTypesArray() {
		return $this->m_arrmixIntervalTypes;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getCallTypeIds() {
		return $this->m_strCallTypeIds;
	}

	public function getCallTypeIdsArray() {
		if( 0 < strlen( trim( $this->m_strCallTypeIds ) ) ) {
			return array_filter( ( array ) explode( ',', str_replace( ' ', '', $this->m_strCallTypeIds ) ) );
		} else {
			return array();
		}
	}

	public function getCallResultIds() {
		return $this->m_strCallResultIds;
	}

	public function getCallResultIdsArray() {
		if( 0 < strlen( trim( $this->m_strCallResultIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strCallResultIds ) );
		} else {
			return array();
		}
	}

	public function getUtilityBillingCallTypeIds() {
		return $this->m_strUtilityBillingCallTypeIds;
	}

	public function getSupportCallTypeId() {
		return $this->m_intSupportCallTypeId;
	}

	public function getAnswerStatusTypeIds() {
		return $this->m_strAnswerStatusTypeIds;
	}

	public function getAnswerStatusTypeIdsArray() {
		if( 0 < strlen( trim( $this->m_strAnswerStatusTypeIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strAnswerStatusTypeIds ) );
		} else {
			return array();
		}
	}

	public function getExcludeEmployeeId() {
		return $this->m_strExcludeEmployeeId;
	}

	public function getLeadSourceIds() {
		return $this->m_strLeadSourceIds;
	}

	public function getLeadSourceIdsArray() {
		if( 0 < strlen( trim( $this->m_strLeadSourceIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strLeadSourceIds ) );
		} else {
			return array();
		}
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getPortfolioId() {
		return $this->m_intPortfolioId;
	}

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function getCompanyStatusTypeIds() {
		return $this->m_strCompanyStatusTypeIds;
	}

	public function getPropertyIdsArray() {
		if( 0 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strPropertyIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strPropertyIds ) );
		} else {
			return array();
		}
	}

	public function getCids() {
		return $this->m_strCids;
	}

	public function getCidsArray() {
		if( 0 < strlen( trim( $this->m_strCids ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strCids ) );
		} else {
			return array();
		}
	}

	public function getCompanyEmployeeIds() {
		return $this->m_strCompanyEmployeeIds;
	}

	public function getEmployeeIds() {
		return $this->m_strEmployeeIds;
	}

	public function getEmployeeIdsArray() {
		if( 0 < strlen( trim( $this->m_strEmployeeIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strEmployeeIds ) );
		} else {
			return array();
		}
	}

	public function getLeasingAgentIds() {
		return $this->m_strLeasingAgentIds;
	}

	public function getLeadIds() {
		return $this->m_strLeadIds;
	}

	public function getLeadIdsArray() {
		if( 0 < strlen( trim( $this->m_strLeadIds ) ) ) {
			return explode( ',', $this->m_strLeadIds );
		} else {
			return array();
		}
	}

	public function getCallId() {
		return $this->m_strCallId;
	}

	public function getVoicemailStatusTypeIds() {
		return $this->m_strVoicemailStatusTypeIds;
	}

	public function getCallDirectionTypeIds() {
		return $this->m_strCallDirectionTypeIds;
	}

	public function getCallQueueIds() {
		return $this->m_strCallQueueIds;
	}

	public function getDataType() {
		return $this->m_strDataType;
	}

	public function getVoicemailStatusTypeIdsArray() {
		if( 0 < strlen( trim( $this->m_strVoicemailStatusTypeIds ) ) ) {
			return explode( ',', $this->m_strVoicemailStatusTypeIds );
		} else {
			return array();
		}
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function getLeads() {
		return $this->m_strLeads;
	}

	public function getPageNo() {
		return $this->m_intPageNo;
	}

	public function getPreviousPageNo() {
		return $this->m_intPreviousPageNo;
	}

	public function getCountPerPage() {
		return $this->m_intCountPerPage;
	}

	public function getIsCurrentLeadsPageSelected() {
		return $this->m_intIsCurrentLeadsPageSelected;
	}

	public function getIsAllLeadsPageSelected() {
		return $this->m_intIsAllLeadsPageSelected;
	}

	public function getIsAllCallsSelected() {
		return $this->m_intIsAllCallsSelected;
	}

	public function getIsAllLeadsDeselected() {
		return $this->m_intIsAllLeadsDeselected;
	}

	public function getIsAllCallsDeselected() {
		return $this->m_intIsAllCallsDeselected;
	}

	public function getCurrentSelectedLeadsPageNo() {
		return $this->m_intCurrentSelectedLeadsPageNo;
	}

	public function getSortBy() {
		return $this->m_strSortBy;
	}

	public function getSortDirection() {
		return $this->m_strSortDirection;
	}

	public function getDestinationPhoneNumber() {
		return $this->m_strDestinationPhoneNumber;
	}

	public function getCallIds() {
		return $this->m_strCallIds;
	}

	public function getCallIdsArray() {
		if( 0 < strlen( trim( $this->m_strCallIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strCallIds ) );
		} else {
			return array();
		}
	}

	public function getIsFailed() {
		return $this->m_boolIsFailed;
	}

	public function getIsCallQueueTime() {
		return $this->m_boolIsCallQueueTime;
	}

	public function getIsShowArchiveCalls() {
		return $this->m_boolIsShowArchiveCalls;
	}

	public function getIsShowAllCalls() {
		return $this->m_boolIsShowAllCalls;
	}

	public function getIsShowClientsOnMaintenance() {
		return $this->m_boolIsShowClientsOnMaintenance;
	}

	public function getShowVanity() {
		return $this->m_strShowVanity;
	}

	public function getTranscribedCall() {
		return $this->m_intTranscribedCall;
	}

	public function getAfterDatetime() {
		return $this->m_strAfterDatetime;
	}

	public function getBeforeDatetime() {
		return $this->m_strBeforeDatetime;
	}

	public function getPsProductIds() {
		return $this->m_strPsProductIds;
	}

	public function getCallStatusTypeIds() {
		return $this->m_arrintCallStatusTypeIds;
	}

	public function getOutboundCallCampaignIds() {
		return $this->m_arrintOutboundCallCampaignIds;
	}

	public function getCallStatusTypeId() {
		return $this->m_intCallStatusTypeId;
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getGreetingTypeIds() {
		return $this->m_strGreetingTypeIds;
	}

	public function getTeamIds() {
		return $this->m_strTeamIds;
	}

	public function getUserIds() {
		return $this->m_strUserIds;
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getDepartmentIds() {
		return $this->m_strDepartmentIds;
	}

	public function getOffice() {
		return $this->m_strOffice;
	}

	public function getTabName() {
		return $this->m_strTabName;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getCallAgentStatusTypeIds() {
		return $this->m_strCallAgentStatusTypeIds;
	}

	public function getCallAgentIds() {
		return $this->m_strCallAgentIds;
	}

	public function getCallsState() {
		return $this->m_strCallsState;
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function getCallAgentIsDisabled() {
		return $this->m_boolCallAgentIsDisabled;
	}

	public function getCallAgentOnProjectReasonIds() {
		return $this->m_arrintCallAgentOnProjectReasonIds;
	}

	public function getCallAnalysisStatusTypes() {
		return $this->m_strCallAnalysisStatusTypes;
	}

	public function getCallAnalysisStatusTypesArray() {
		if( true == valStr( $this->m_strCallAnalysisStatusTypes ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strCallAnalysisStatusTypes ) );
		} else {
			return array();
		}
	}

	public function getApplicantApplicationIds() {
		return $this->m_strApplicantApplicationIds;
	}

	public function getApplicantApplicationIdsArray() {
		if( 0 < strlen( trim( $this->m_strApplicantApplicationIds ) ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strApplicantApplicationIds ) );
		} else {
			return array();
		}
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function getCallCount() {
		return $this->m_intCallCount;
	}

	public function getTableName() {
		return $this->m_strTableName;
	}

	public function getIsCustom() {
		return $this->m_boolIsCustom;
	}

	public function getCallAgentVacationRequestStatuses() {
		return $this->m_strCallAgentVacationRequestSatuses;
	}

	public function getReportColumnName() {
		return $this->m_strReportColumnName;
	}

	public function getReportCallPeriod() {
		return $this->m_strReportCallPeriod;
	}

	public function getCallAgentData() {
		return $this->m_strCallAgentData;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getReportBy() {
		return $this->m_strReportBy;
	}

	public function getTeamIdsArray() {
		if( 0 < strlen( trim( $this->m_strTeamIds ) ) ) {
			return explode( ',', $this->m_strTeamIds );
		} else {
			return array();
		}
	}

	public function getCallAgentIdsArray() {
		if( 0 < strlen( trim( $this->m_strCallAgentIds ) ) ) {
			return explode( ',', $this->m_strCallAgentIds );
		} else {
			return array();
		}
	}

	public function getReportDistinctLeads() {
		return $this->m_boolReportDistinctLeads;
	}

	public function getOccurrenceTypeIds() {
		return $this->m_strOccurrenceTypeIds;
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function getDisputeStatus() {
		return $this->m_strDisputeStatus;
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function getCallDurationSeconds() {
		return $this->m_intCallDurationSeconds;
	}

	public function getPercentageCallCount() {
		return $this->m_intPercentageCallCount;
	}

	public function getIntervalDays() {
		return $this->m_intIntervalDays;
	}

	public function getCallResultId() {
		return $this->m_intCallResultId;
	}

	public function getWrapUpTime() {
		return $this->m_intWrapUpTime;
	}

	public function getCallAgentPinTypeIds() {
		return $this->m_strCallAgentPinTypeIds;
	}

	public function getCallSourceId() {
		return $this->m_intCallSourceId;
	}

	public function getFollowupTypeIds() {
		return $this->m_strFollowupTypeIds;
	}

	public function getCountryCodes() {
		return $this->m_strCountryCodes;
	}

	public function getFollowupStatusTypeId() {
		return $this->m_intFollowupStatusTypeId;
	}

	public function getIsCallbackItem() {
		return $this->m_boolIsCallbackItem;
	}

	public function getIsLeadItem() {
		return $this->m_boolIsLeadItem;
	}

	public function getIsReviewItem() {
		return $this->m_boolIsReviewItem;
	}

	public function getIsAllQueueItem() {
		return $this->m_boolIsAllQueueItem;
	}

	public function getDateIntervals() {
		return $this->m_arrstrDateIntervals;
	}

	public function getFollowupId() {
		return $this->m_intFollowupId;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getSearchKeyword() {
		return $this->m_strSearchKeyword;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setFollowupTypeIds( $strFollowupTypeIds ) {
		$this->m_strFollowupTypeIds = $strFollowupTypeIds;
	}

	public function setDefaults() {
		$this->setCallId( NULL );
		$this->setCallIds( NULL );
		$this->setStartDate( 'Min' );
		$this->setEndDate( 'Max' );
		$this->setStartTime( '00:00:00' );
		$this->setEndTime( '23:59:59.999999' );
		$this->setReportCallPeriod( NULL );
		$this->setPropertyId( NULL );
		$this->setPropertyIds( NULL );
		$this->setCompanyStatusTypeIds( implode( ',', CCompanyStatusType::$c_arrintDefaultCompanyStatusTypeIds ) );
		$this->setCid( NULL );
		$this->setCids( NULL );
		$this->setEmployeeIds( NULL );
		$this->setCallTypeIds( NULL );
		$this->setLeadSourceIds( NULL );
		$this->setAnswerStatusTypeIds( NULL );
		$this->setVoicemailStatusTypeIds( NULL );
		$this->setCallDirectionTypeIds( NULL );
		$this->setCallQueueIds( NULL );
		$this->setGreetingTypeIds( NULL );
		$this->setDepartmentId( NULL );
		$this->setDepartmentIds( NULL );
		$this->setCallAgentIsDisabled( false );
		$this->setCallAnalysisStatusTypes( NULL );
		$this->setApplicantApplicationIds( NULL );
		$this->setCallAgentVacationRequestStatuses( NULL );

		$this->setPreviousPageNo( self::PAGE_NO );
		$this->setCountPerPage( self::COUNT_PER_PAGE );
		$this->setPageNo( self::PAGE_NO );

		$this->setCallAnalyzed( self::CALL_ANALYZED_ANY );
		$this->setIsCurrentLeadsPageSelected( 0 );
		$this->setIsAllLeadsPageSelected( 0 );
		$this->setIsAllCallsSelected( 0 );
		$this->setIsAllLeadsDeselected( 1 );
		$this->setIsAllCallsDeselected( 1 );
		$this->setCurrentSelectedLeadsPageNo( NULL );
		$this->setShowVanity( NULL );

		$this->setSortBy( NULL );
		$this->setSortDirection( NULL );
		$this->setCallStatusTypeId( NULL );
		$this->setCallQueueId( NULL );
		$this->setTeamIds( NULL );
		$this->setUserIds( NULL );
		$this->setCallAgentStatusTypeIds( NULL );
		$this->setCallAgentIds( NULL );
		$this->setEmployeeStatusTypeId( NULL );
		$this->setCallFormStatusTypeId( NULL );
		$this->setIsCallQueueTime( false );
		$this->setCallsState( self::CALL_TRACKER_CURRENT_CALLS );
		$this->setPhoneNumber( NULL );
		$this->setOutboundCallId( NULL );
		$this->setTranscribedCall( 0 );

		$this->setCallAgentOnProjectReasonIds( NULL );
		$this->setTableName( NULL );
		$this->setCreatedBy( NULL );
		$this->setIsCustom( false );
		$this->setCallCount( NULL );
		$this->setAction( NULL );
		$this->setOffice( self::OFFICE_DEFAULT );
		$this->setCallResultId( NULL );
		$this->setCallSourceId( NULL );
	}

	public function setErrorMsgs( $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setVoiceMailType( $strVoiceMailType ) {
		$this->m_strVoiceMailType = $strVoiceMailType;
	}

	public function setIsShowUnreadMessage( $boolIsShowUnreadMessage ) {
		$this->m_boolIsShowUnreadMessage = CStrings::strToIntDef( $boolIsShowUnreadMessage, NULL, false );
	}

	public function setAllLeads( $strAllLeads ) {
		$this->m_strAllLeads = $strAllLeads;
	}

	public function setGenericData( $strGenericData ) {
		$this->m_strGenericData = $strGenericData;
	}

	public function setIntervalTypeId( $intIntervalTypeId ) {
		$this->m_intIntervalTypeId = $intIntervalTypeId;
	}

	public function setOutboundCallId( $intOutboundCallId ) {
		$this->m_intOutboundCallId = $intOutboundCallId;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCustomerId( $intCustomerId ) {
		$this->m_intCustomerId = $intCustomerId;
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->m_intEmployeeStatusTypeId = $intEmployeeStatusTypeId;
	}

	public function setCallFormStatusTypeId( $intCallFormStatusTypeId ) {
		$this->m_intCallFormStatusTypeId = $intCallFormStatusTypeId;
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = $strStartDate;
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = $strEndDate;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setPortfolioId( $intPortfolioId ) {
		$this->m_intPortfolioId = $intPortfolioId;
	}

	public function setPropertyIds( $strPropertyIds ) {
		$this->m_strPropertyIds = $strPropertyIds;
	}

	public function setCompanyStatusTypeIds( $strCompanyStatusTypeIds ) {
		$this->m_strCompanyStatusTypeIds = $strCompanyStatusTypeIds;
	}

	public function setCids( $strCids ) {
		$this->m_strCids = $strCids;
	}

	public function setCompanyEmployeeIds( $strCompanyEmployeeIds ) {
		$this->m_strCompanyEmployeeIds = $strCompanyEmployeeIds;
	}

	public function setEmployeeIds( $strEmployeeIds ) {
		$this->m_strEmployeeIds = $strEmployeeIds;
	}

	public function setLeasingAgentIds( $strLeasingAgentIds ) {
		$this->m_strLeasingAgentIds = $strLeasingAgentIds;
	}

	public function setLeadIds( $strLeadIds ) {
		$this->m_strLeadIds = $strLeadIds;
	}

	public function setCallId( $intCallId ) {
		$this->m_strCallId = $intCallId;
	}

	public function setCallTypeIds( $strCallTypeIds ) {
		$this->m_strCallTypeIds = $strCallTypeIds;
	}

	public function setCallResultIds( $strCallResultIds ) {
		$this->m_strCallResultIds = $strCallResultIds;
	}

	public function setUtilityBillingCallTypeIds( $strUtilityBillingCallTypeIds ) {
		$this->m_strUtilityBillingCallTypeIds = $strUtilityBillingCallTypeIds;
	}

	public function setSupportCallTypeId( $intSupportCallTypeId ) {
		$this->m_intSupportCallTypeId = $intSupportCallTypeId;
	}

	public function setAnswerStatusTypeIds( $strAnswerStatusTypeIds ) {
		$this->m_strAnswerStatusTypeIds = $strAnswerStatusTypeIds;
	}

	public function setExcludeEmployeeId( $intExcludeEmployeeId ) {
		$this->m_strExcludeEmployeeId = $intExcludeEmployeeId;
	}

	public function setLeadSourceIds( $strLeadSourceIds ) {
		$this->m_strLeadSourceIds = $strLeadSourceIds;
	}

	public function setVoicemailStatusTypeIds( $strVoicemailStatusTypeIds ) {
		$this->m_strVoicemailStatusTypeIds = $strVoicemailStatusTypeIds;
	}

	public function setCallDirectionTypeIds( $strCallDirectionTypeIds ) {
		$this->m_strCallDirectionTypeIds = $strCallDirectionTypeIds;
	}

	public function setCallQueueIds( $strCallQueueIds ) {
		$this->m_strCallQueueIds = $strCallQueueIds;
	}

	public function setDataType( $strDataType ) {
		$this->m_strDataType = $strDataType;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
	}

	public function setLeads( $strLeads ) {
		$this->m_strLeads = $strLeads;
	}

	public function setPageNo( $intPageNo ) {
		$this->m_intPageNo = $intPageNo;
	}

	public function setPreviousPageNo( $intPreviousPageNo ) {
		$this->m_intPreviousPageNo = $intPreviousPageNo;
	}

	public function setCountPerPage( $intCountPerPage ) {
		$this->m_intCountPerPage = $intCountPerPage;
	}

	public function setCallAnalyzed( $strCallAnalyzed ) {
		$this->m_strCallAnalyzed = $strCallAnalyzed;
	}

	public function setIsCurrentLeadsPageSelected( $intIsCurrentLeadsPageSelected ) {
		$this->m_intIsCurrentLeadsPageSelected = $intIsCurrentLeadsPageSelected;
	}

	public function setIsAllLeadsPageSelected( $intIsAllLeadsPageSelected ) {
		$this->m_intIsAllLeadsPageSelected = $intIsAllLeadsPageSelected;
	}

	public function setIsAllCallsSelected( $intIsAllCallsSelected ) {
		$this->m_intIsAllCallsSelected = $intIsAllCallsSelected;
	}

	public function setIsAllLeadsDeselected( $intIsAllLeadsDeselected ) {
		$this->m_intIsAllLeadsDeselected = $intIsAllLeadsDeselected;
	}

	public function setIsAllCallsDeselected( $intIsAllCallsDeselected ) {
		$this->m_intIsAllCallsDeselected = $intIsAllCallsDeselected;
	}

	public function setCurrentSelectedLeadsPageNo( $intCurrentSelectedLeadsPageNo ) {
		$this->m_intCurrentSelectedLeadsPageNo = $intCurrentSelectedLeadsPageNo;
	}

	public function setSortBy( $strSortBy ) {
		$this->m_strSortBy = $strSortBy;
	}

	public function setSortDirection( $strSortDirection ) {
		$this->m_strSortDirection = $strSortDirection;
	}

	public function setDestinationPhoneNumber( $strDestinationPhoneNumber ) {
		$this->m_strDestinationPhoneNumber = $strDestinationPhoneNumber;
	}

	public function setCallIds( $strCallIds ) {
		$this->m_strCallIds = $strCallIds;
	}

	public function setIsFailed( $boolIsFailed ) {
		$this->m_boolIsFailed = CStrings::strToIntDef( $boolIsFailed, NULL, false );
	}

	public function setIsCallQueueTime( $boolIsCallQueueTime ) {
		$this->m_boolIsCallQueueTime = $boolIsCallQueueTime;
	}

	public function setIsShowArchiveCalls( $boolIsShowArchiveCalls ) {
		$this->m_boolIsShowArchiveCalls = $boolIsShowArchiveCalls;
	}

	public function setIsShowAllCalls( $boolIsShowAllCalls ) {
		$this->m_boolIsShowAllCalls = $boolIsShowAllCalls;
	}

	public function setIsShowClientsOnMaintenance( $boolIsShowClientsOnMaintenance ) {
		$this->m_boolIsShowClientsOnMaintenance = $boolIsShowClientsOnMaintenance;
	}

	public function setShowVanity( $strShowVanity ) {
		$this->m_strShowVanity = $strShowVanity;
	}

	public function setTranscribedCall( $intTranscribedCall ) {
		$this->m_intTranscribedCall = $intTranscribedCall;
	}

	public function setAfterDatetime( $strAfterDatetime ) {
		$this->m_strAfterDatetime = $strAfterDatetime;
	}

	public function setBeforeDatetime( $strBeforeDatetime ) {
		$this->m_strBeforeDatetime = $strBeforeDatetime;
	}

	public function setPsProductIds( $strPsProductIds ) {
		$this->m_strPsProductIds = $strPsProductIds;
	}

	public function setCallStatusTypeIds( $arrintCallStatusTypeIds ) {
		$this->m_arrintCallStatusTypeIds = $arrintCallStatusTypeIds;
	}

	public function setOutboundCallCampaignIds( $arrintOutboundCallCampaignIds ) {
		$this->m_arrintOutboundCallCampaignIds = $arrintOutboundCallCampaignIds;
	}

	public function setCallStatusTypeId( $intCallStatusTypeId ) {
		$this->m_intCallStatusTypeId = $intCallStatusTypeId;
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setGreetingTypeIds( $strGreetingIds ) {
		$this->m_strGreetingTypeIds = $strGreetingIds;
	}

	public function setTeamIds( $strTeamIds ) {
		$this->m_strTeamIds = $strTeamIds;
	}

	public function setUserIds( $strUserIds ) {
		$this->m_strUserIds = $strUserIds;
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->m_intDepartmentId = $intDepartmentId;
	}

	public function setDepartmentIds( $strDepartmentIds ) {
		$this->m_strDepartmentIds = $strDepartmentIds;
	}

	public function setOffice( $strOffice ) {
		$this->m_strOffice = $strOffice;
	}

	public function setTabName( $strTabName ) {
		$this->m_strTabName = $strTabName;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setCallAgentStatusTypeIds( $strCallAgentStatusTypeIds ) {
		$this->m_strCallAgentStatusTypeIds = $strCallAgentStatusTypeIds;
	}

	public function setCallAgentIds( $strCallAgentIds ) {
		$this->m_strCallAgentIds = $strCallAgentIds;
	}

	public function setCallsState( $strCallsState ) {
		$this->m_strCallsState = $strCallsState;
	}

	public function setStartTime( $strStartTime ) {
		$this->m_strStartTime = $strStartTime;
	}

	public function setEndTime( $strEndTime ) {
		$this->m_strEndTime = $strEndTime;
	}

	public function setCallAgentIsDisabled( $boolCallAgentIsDisabled ) {
		$this->m_boolCallAgentIsDisabled = $boolCallAgentIsDisabled;
	}

	public function setCallAgentOnProjectReasonIds( $arrintCallAgentOnProjectReasonIds ) {
		$this->m_arrintCallAgentOnProjectReasonIds = $arrintCallAgentOnProjectReasonIds;
	}

	public function setCallAnalysisStatusTypes( $strCallAnalysisStatusTypes ) {
		if( true == valStr( $strCallAnalysisStatusTypes ) ) {
			$this->m_strCallAnalysisStatusTypes = $strCallAnalysisStatusTypes;
		} elseif( true == valArr( $strCallAnalysisStatusTypes ) ) {
			$this->m_strCallAnalysisStatusTypes = implode( ',', array_filter( $strCallAnalysisStatusTypes ) );
		}
	}

	public function setApplicantApplicationIds( $strApplicantApplicationIds ) {
		if( true == valStr( $strApplicantApplicationIds ) ) {
			$this->m_strApplicantApplicationIds = $strApplicantApplicationIds;
		} elseif( true == valArr( $strApplicantApplicationIds ) ) {
			$this->m_strApplicantApplicationIds = implode( ',', array_filter( $strApplicantApplicationIds ) );
		}
	}

	public function setTableName( $strTableName ) {
		$this->m_strTableName = $strTableName;
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = $intCreatedBy;
	}

	public function setIsCustom( $boolIsCustom ) {
		$this->m_boolIsCustom = $boolIsCustom;
	}

	public function setCallAgentVacationRequestStatuses( $strEmployeeVacationRequestStatuses ) {
		if( true == valStr( $strEmployeeVacationRequestStatuses ) ) {
			$this->m_strCallAgentVacationRequestSatuses = $strEmployeeVacationRequestStatuses;
		} elseif( true == valArr( $strEmployeeVacationRequestStatuses ) ) {
			$this->m_strCallAgentVacationRequestSatuses = implode( ',', array_filter( $strEmployeeVacationRequestStatuses ) );
		}
	}

	public function setCallCount( $intCallCount ) {
		$this->m_intCallCount = $intCallCount;
	}

	public function setReportColumnName( $strColumnName ) {
		$this->m_strReportColumnName = $strColumnName;
	}

	public function setReportCallPeriod( $strCallPeriod ) {
		$this->m_strReportCallPeriod = $strCallPeriod;
	}

	public function setCallAgentData( $strCallAgentData ) {
		$this->m_strCallAgentData = $strCallAgentData;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setReportBy( $strReportBy ) {
		$this->m_strReportBy = $strReportBy;
	}

	public function setReportDistinctLeads( $boolIsDistinct ) {
		$this->m_boolReportDistinctLeads = $boolIsDistinct;
	}

	public function setOccurrenceTypeIds( $strOccurrenceTypeIds ) {
		$this->m_strOccurrenceTypeIds = $strOccurrenceTypeIds;
	}

	public function setAction( $strAction ) {
		$this->m_strAction = $strAction;
	}

	public function setDisputeStatus( $intDisputeStatus ) {
		$this->m_strDisputeStatus = $intDisputeStatus;
	}

	public function setReferenceId( $intReferenceId ) {
		$this->m_intReferenceId = $intReferenceId;
	}

	public function setCallDurationSeconds( $intCallDurationSeconds ) {
		$this->m_intCallDurationSeconds = $intCallDurationSeconds;
	}

	public function setPercentageCallCount( $intPercentageCallCount ) {
		$this->m_intPercentageCallCount = $intPercentageCallCount;
	}

	public function setIntervalDays( $intIntervalDays ) {
		$this->m_intIntervalDays = $intIntervalDays;
	}

	public function setCallResultId( $intCallResultId ) {
		$this->m_intCallResultId = $intCallResultId;
	}

	public function setWrapUpTime( $intWrapUpTime ) {
		$this->m_intWrapUpTime = $intWrapUpTime;
	}

	public function setCallAgentPinTypeIds( $strCallAgentPinTypeIds ) {
		$this->m_strCallAgentPinTypeIds = $strCallAgentPinTypeIds;
	}

	public function setCallSourceId( $intCallSourceId ) {
		$this->m_intCallSourceId = $intCallSourceId;
	}

	public function setCountryCodes( $strCountryCodes ) {
		$this->m_strCountryCodes = $strCountryCodes;
	}

	public function setFollowupStatusTypeId( $intFollowupStatusTypeId ) {
		$this->m_intFollowupStatusTypeId = $intFollowupStatusTypeId;
	}

	public function setIsCallbackItem( $boolIsCallbackItem ) {
		return $this->m_boolIsCallbackItem = $boolIsCallbackItem;
	}

	public function setIsLeadItem( $boolIsLeadItem ) {
		return $this->m_boolIsLeadItem = $boolIsLeadItem;
	}

	public function setIsReviewItem( $boolIsReviewItem ) {
		return $this->m_boolIsReviewItem = $boolIsReviewItem;
	}

	public function setIsAllQueueItem( $boolIsAllQueueItem ) {
		return $this->m_boolIsAllQueueItem = $boolIsAllQueueItem;
	}

	public function setDateIntervals( $arrstrDateIntervals ) {
		$this->m_arrstrDateIntervals = $arrstrDateIntervals;
	}

	public function setFollowupId( $intFollowupId ) {
		$this->m_intFollowupId = $intFollowupId;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setSearchKeyword( $strSearchKeyword ) {
		$this->m_strSearchKeyword = $strSearchKeyword;
	}

	/**
	 * Other Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( true == isset( $arrmixValues['call_queues'] ) && true == valArr( $arrmixValues['call_queues'] ) ) {
			$arrmixValues['call_queues'] = implode( ',', array_filter( $arrmixValues['call_queues'] ) );
		}

		if( true == isset( $arrmixValues['cids'] ) && true == valArr( $arrmixValues['cids'] ) ) {
			$arrmixValues['cids'] = implode( ',', array_filter( $arrmixValues['cids'] ) );
		}

		if( true == isset( $arrmixValues['lead_source_ids'] ) && true == valArr( $arrmixValues['lead_source_ids'] ) ) {
			if( empty( array_filter( $arrmixValues['lead_source_ids'] ) ) ) {
				$arrmixValues['lead_source_ids'] = 0;
			} else {
				$arrmixValues['lead_source_ids'] = implode( ',', array_filter( $arrmixValues['lead_source_ids'] ) );
			}
		}

		if( true == isset( $arrmixValues['applicant_application_ids'] ) && true == valArr( $arrmixValues['applicant_application_ids'] ) ) {
			$arrmixValues['applicant_application_ids'] = implode( ',', $arrmixValues['applicant_application_ids'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_id'] ) ) 						$this->setCallId( $arrmixValues['call_id'] );
		if( true == isset( $arrmixValues['is_failed'] ) ) 						$this->setIsFailed( $arrmixValues['is_failed'] );
		if( true == isset( $arrmixValues['all_leads'] ) ) 						$this->setAllLeads( $arrmixValues['all_leads'] );
		if( true == isset( $arrmixValues['advanced_search_generic_data'] ) ) 	$this->setGenericData( $arrmixValues['advanced_search_generic_data'] );
		if( true == isset( $arrmixValues['property_id'] ) )						$this->setPropertyId( $arrmixValues['property_id'] );
		if( true == isset( $arrmixValues['cid'] ) )								$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['start_date'] ) ) 						$this->setStartDate( $arrmixValues['start_date'] );
		if( true == isset( $arrmixValues['end_date'] ) ) 						$this->setEndDate( $arrmixValues['end_date'] );
		if( true == isset( $arrmixValues['start_time'] ) ) 						$this->setStartTime( $arrmixValues['start_time'] );
		if( true == isset( $arrmixValues['end_time'] ) ) 						$this->setEndTime( $arrmixValues['end_time'] );

		if( true == isset( $arrmixValues['sort_by'] ) )							$this->setSortBy( $arrmixValues['sort_by'] );
		if( true == isset( $arrmixValues['sort_direction'] ) )					$this->setSortDirection( $arrmixValues['sort_direction'] );

		if( true == isset( $arrmixValues['call_analyzed'] ) )						$this->setCallAnalyzed( $arrmixValues['call_analyzed'] );
		if( true == isset( $arrmixValues['is_current_leads_page_selected'] ) )	$this->setIsCurrentLeadsPageSelected( ( true == $boolStripSlashes )	? stripslashes( $arrmixValues['is_current_leads_page_selected'] ): $arrmixValues['is_current_leads_page_selected'] );
		if( true == isset( $arrmixValues['is_all_leads_page_selected'] ) ) 		$this->setIsAllLeadsPageSelected( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['is_all_leads_page_selected'] ): $arrmixValues['is_all_leads_page_selected'] );
		if( true == isset( $arrmixValues['is_all_calls_selected'] ) )			$this->setIsAllCallsSelected( ( true == $boolStripSlashes )			? stripslashes( $arrmixValues['is_all_calls_selected'] ): $arrmixValues['is_all_calls_selected'] );
		if( true == isset( $arrmixValues['is_all_leads_deselected'] ) ) 		$this->setIsAllLeadsDeselected( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['is_all_leads_deselected'] ): $arrmixValues['is_all_leads_deselected'] );
		if( true == isset( $arrmixValues['is_all_calls_deselected'] ) )			$this->setIsAllCallsDeselected( ( true == $boolStripSlashes )		? stripslashes( $arrmixValues['is_all_calls_deselected'] ): $arrmixValues['is_all_calls_deselected'] );
		if( true == isset( $arrmixValues['current_selected_leads_page_no'] ) ) 	$this->setCurrentSelectedLeadsPageNo( ( true == $boolStripSlashes )	? stripslashes( $arrmixValues['current_selected_leads_page_no'] ): $arrmixValues['current_selected_leads_page_no'] );
		if( true == isset( $arrmixValues['employee_id'] ) ) 					$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['interval_type_id'] ) )				$this->setIntervalTypeId( $arrmixValues['interval_type_id'] );
		if( true == isset( $arrmixValues['outbound_call_id'] ) )				$this->setOutboundCallId( $arrmixValues['outbound_call_id'] );
		if( true == isset( $arrmixValues['report_column_name'] ) )				$this->setReportColumnName( $arrmixValues['report_column_name'] );
		if( true == isset( $arrmixValues['report_call_period'] ) )				$this->setReportCallPeriod( $arrmixValues['report_call_period'] );

		if( true == isset( $arrmixValues['after_datetime'] ) )					$this->setAfterDatetime( $arrmixValues['after_datetime'] );
		if( true == isset( $arrmixValues['before_datetime'] ) )					$this->setBeforeDatetime( $arrmixValues['before_datetime'] );
		if( true == isset( $arrmixValues['ps_product_ids'] ) )					$this->setPsProductIds( $arrmixValues['ps_product_ids'] );
		if( true == isset( $arrmixValues['company_status_type_ids'] ) )			$this->setCompanyStatusTypeIds( $arrmixValues['company_status_type_ids'] );
		if( true == isset( $arrmixValues['call_ids'] ) )						$this->setCallIds( $arrmixValues['call_ids'] );
		if( true == isset( $arrmixValues['call_queue_id'] ) )					$this->setCallQueueId( $arrmixValues['call_queue_id'] );
		if( true == isset( $arrmixValues['data_type'] ) ) 						$this->setDataType( $arrmixValues['data_type'] );
		if( true == isset( $arrmixValues['phone_number'] ) )					$this->setPhoneNumber( $arrmixValues['phone_number'] );

		if( true == isset( $arrmixValues['created_by'] ) )						$this->setCreatedBy( $arrmixValues['created_by'] );
		if( true == isset( $arrmixValues['call_count'] ) )						$this->setCallCount( $arrmixValues['call_count'] );
		if( true == isset( $arrmixValues['table_name'] ) )						$this->setTableName( $arrmixValues['table_name'] );
		if( true == isset( $arrmixValues['tab_name'] ) )						$this->setTabName( $arrmixValues['tab_name'] );
		if( true == isset( $arrmixValues['is_show_clients_on_maintenance'] ) )	$this->setIsShowClientsOnMaintenance( $arrmixValues['is_show_clients_on_maintenance'] );
		if( true == isset( $arrmixValues['show_vanity'] ) )						$this->setShowVanity( $arrmixValues['show_vanity'] );
		if( true == isset( $arrmixValues['transcribed_call'] ) )				$this->setTranscribedCall( $arrmixValues['transcribed_call'] );
		if( true == isset( $arrmixValues['is_custom'] ) )						$this->setIsCustom( $arrmixValues['is_custom'] );
		if( true == isset( $arrmixValues['action'] ) )							$this->setAction( $arrmixValues['action'] );
		if( true == isset( $arrmixValues['page_no'] ) )							$this->setPageNo( $arrmixValues['page_no'] );
		if( true == isset( $arrmixValues['is_show_archive_calls'] ) )			$this->setIsShowArchiveCalls( $arrmixValues['is_show_archive_calls'] );
		if( true == isset( $arrmixValues['is_show_all_calls'] ) )				$this->setIsShowAllCalls( $arrmixValues['is_show_all_calls'] );
		if( true == isset( $arrmixValues['calls_state'] ) )						$this->setCallsState( $arrmixValues['calls_state'] );
		if( true == isset( $arrmixValues['office'] ) )							$this->setOffice( $arrmixValues['office'] );
		if( true == isset( $arrmixValues['call_source_id'] ) )					$this->setCallSourceId( $arrmixValues['call_source_id'] );

		if( true == isset( $arrmixValues['company_employee_ids'] ) && true == valArr( $arrmixValues['company_employee_ids'] ) ) {
			$this->setCompanyEmployeeIds( implode( ',', $arrmixValues['company_employee_ids'] ) );
		} elseif( true == isset( $arrmixValues['company_employee_ids'] ) && false == is_null( $arrmixValues['company_employee_ids'] ) ) {
			$this->setCompanyEmployeeIds( $arrmixValues['company_employee_ids'] );
		} else {
			$this->setCompanyEmployeeIds( NULL );
		}

		if( true == isset( $arrmixValues['report_column_name'] ) ) {
			$this->setReportColumnName( $arrmixValues['report_column_name'] );
		}

		if( true == isset( $arrmixValues['applicant_application_ids'] ) && true == valArr( $arrmixValues['applicant_application_ids'] ) ) {
			$this->setApplicantApplicationIds( implode( ',', array_filter( $arrmixValues['applicant_application_ids'] ) ) );
		} elseif( ( true == isset( $arrmixValues['applicant_application_ids'] ) ) ) {
			$this->setApplicantApplicationIds( $arrmixValues['applicant_application_ids'] );
		} else {
			$this->setApplicantApplicationIds( NULL );
		}

		if( true == isset( $arrmixValues['property_ids'] ) && true == valArr( $arrmixValues['property_ids'] ) ) {
			$this->setPropertyIds( implode( ',', array_filter( $arrmixValues['property_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['property_ids'] ) && false == is_null( $arrmixValues['property_ids'] ) ) {
			$this->setPropertyIds( $arrmixValues['property_ids'] );
		} else {
			$this->setPropertyIds( NULL );
		}

		if( true == isset( $arrmixValues['cids'] ) && true == valArr( $arrmixValues['cids'] ) ) {
			$this->setCids( implode( ',', array_filter( $arrmixValues['cids'] ) ) );
		} elseif( true == isset( $arrmixValues['cids'] ) && false == is_null( $arrmixValues['cids'] ) ) {
			$this->setCids( $arrmixValues['cids'] );
		} else {
			$this->setCids( NULL );
		}

		if( true == isset( $arrmixValues['employee_ids'] ) && true == valArr( $arrmixValues['employee_ids'] ) ) {
			$this->setEmployeeIds( implode( ',', array_filter( $arrmixValues['employee_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['employee_ids'] ) ) {
			$this->setEmployeeIds( $arrmixValues['employee_ids'] );
		} else {
			$this->setEmployeeIds( NULL );
		}

		if( true == isset( $arrmixValues['leasing_agent_ids'] ) && true == valArr( $arrmixValues['leasing_agent_ids'] ) ) {
			$this->setLeasingAgentIds( implode( ',', array_filter( $arrmixValues['leasing_agent_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['leasing_agent_ids'] ) ) {
			$this->setLeasingAgentIds( $arrmixValues['leasing_agent_ids'] );
		} else {
			$this->setLeasingAgentIds( NULL );
		}

		if( true == isset( $arrmixValues['call_type_ids'] ) && true == valArr( $arrmixValues['call_type_ids'] ) ) {
			$this->setCallTypeIds( implode( ',', array_filter( $arrmixValues['call_type_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_type_ids'] ) && false == is_null( $arrmixValues['call_type_ids'] ) ) {
			$this->setCallTypeIds( $arrmixValues['call_type_ids'] );
		} else {
			$this->setCallTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['call_result_ids'] ) && true == valArr( $arrmixValues['call_result_ids'] ) ) {
			$this->setCallResultIds( implode( ',', array_filter( $arrmixValues['call_result_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_result_ids'] ) && false == is_null( $arrmixValues['call_result_ids'] ) ) {
			$this->setCallResultIds( $arrmixValues['call_result_ids'] );
		} else {
			$this->setCallResultIds( NULL );
		}

		$arrstrColumnNames = [ 'greeting_abandoned','ivr_abandoned','answered_onsite','unanswered_onsite','transition_abandoned','answered_leasing_center','leasing_center_abandoned','leasing_center_unanswered' ];

		if( true == isset( $arrmixValues['lead_source_ids'] ) && true == valArr( $arrmixValues['lead_source_ids'] ) ) {
			$this->setLeadSourceIds( implode( ',', array_filter( $arrmixValues['lead_source_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['lead_source_ids'] ) && false == is_null( $arrmixValues['lead_source_ids'] ) ) {
			$this->setLeadSourceIds( $arrmixValues['lead_source_ids'] );
		} elseif( false == isset( $arrmixValues['lead_source_ids'] ) && true == isset( $arrmixValues['report_column_name'] ) && true == in_array( $arrmixValues['report_column_name'], $arrstrColumnNames ) && true != empty( $arrmixValues['advanced_search_generic_data'] ) ) {
			$this->setLeadSourceIds( 0 );
		}

		if( true == isset( $arrmixValues['answer_status_type_ids'] ) && true == valArr( $arrmixValues['answer_status_type_ids'] ) ) {
			$this->setAnswerStatusTypeIds( implode( ',', array_filter( $arrmixValues['answer_status_type_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['answer_status_type_ids'] ) && false == is_null( $arrmixValues['answer_status_type_ids'] ) ) {
			$this->setAnswerStatusTypeIds( $arrmixValues['answer_status_type_ids'] );
		} else {
			$this->setAnswerStatusTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['voicemail_status_type_ids'] ) && true == valArr( $arrmixValues['voicemail_status_type_ids'] ) ) {
			$this->setVoicemailStatusTypeIds( implode( ',', $arrmixValues['voicemail_status_type_ids'] ) );
		} elseif( true == isset( $arrmixValues['voicemail_status_type_ids'] ) && false == is_null( $arrmixValues['voicemail_status_type_ids'] ) ) {
			$this->setVoicemailStatusTypeIds( $arrmixValues['voicemail_status_type_ids'] );
		} else {
			$this->setVoicemailStatusTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['call_status_type_ids'] ) && true == valArr( $arrmixValues['call_status_type_ids'] ) ) {
			$this->setCallStatusTypeIds( implode( ',', array_filter( $arrmixValues['call_status_type_ids'] ) ) );
		} elseif( ( true == isset( $arrmixValues['call_status_type_ids'] ) ) ) {
			$this->setCallStatusTypeIds( $arrmixValues['call_status_type_ids'] );
		} else {
			$this->setCallStatusTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['outbound_call_campaign_ids'] ) && true == valArr( $arrmixValues['outbound_call_campaign_ids'] ) ) {
			$this->setOutboundCallCampaignIds( implode( ',', array_filter( $arrmixValues['outbound_call_campaign_ids'] ) ) );
		} elseif( ( true == isset( $arrmixValues['outbound_call_campaign_ids'] ) ) ) {
			$this->setOutboundCallCampaignIds( $arrmixValues['outbound_call_campaign_ids'] );
		} else {
			$this->setOutboundCallCampaignIds( NULL );
		}

		if( true == isset( $arrmixValues['greeting_type_ids'] ) && true == valArr( $arrmixValues['greeting_type_ids'] ) ) {
			$this->setGreetingTypeIds( implode( ',', array_filter( $arrmixValues['greeting_type_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['greeting_type_ids'] ) ) {
			$this->setGreetingTypeIds( $arrmixValues['greeting_type_ids'] );
		} else {
			$this->setGreetingTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['call_direction_type_ids'] ) && true == valArr( $arrmixValues['call_direction_type_ids'] ) ) {
			$this->setCallDirectionTypeIds( implode( ',', array_filter( $arrmixValues['call_direction_type_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_direction_type_ids'] ) && false == is_null( $arrmixValues['call_direction_type_ids'] ) ) {
			$this->setCallDirectionTypeIds( $arrmixValues['call_direction_type_ids'] );
		} else {
			$this->setCallDirectionTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['call_queue_ids'] ) && true == valArr( $arrmixValues['call_queue_ids'] ) ) {
			$this->setCallQueueIds( implode( ',', array_filter( $arrmixValues['call_queue_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_queue_ids'] ) && false == is_null( $arrmixValues['call_queue_ids'] ) ) {
			$this->setCallQueueIds( $arrmixValues['call_queue_ids'] );
		} else {
			$this->setCallQueueIds( NULL );
		}

		if( true == isset( $arrmixValues['team_ids'] ) && true == valArr( $arrmixValues['team_ids'] ) ) {
			$this->setTeamIds( implode( ',', array_filter( $arrmixValues['team_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['team_ids'] ) ) {
			$this->setTeamIds( $arrmixValues['team_ids'] );
		} else {
			$this->setTeamIds( NULL );
		}

		if( true == isset( $arrmixValues['user_ids'] ) && true == valArr( $arrmixValues['user_ids'] ) ) {
			$this->setUserIds( implode( ',', array_filter( $arrmixValues['user_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['user_ids'] ) ) {
			$this->setUserIds( $arrmixValues['user_ids'] );
		} else {
			$this->setUserIds( NULL );
		}

		if( true == isset( $arrmixValues['call_agent_status_type_ids'] ) && true == valArr( $arrmixValues['call_agent_status_type_ids'] ) ) {
			$this->setCallAgentStatusTypeIds( implode( ',', array_filter( $arrmixValues['call_agent_status_type_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_agent_status_type_ids'] ) ) {
			$this->setCallAgentStatusTypeIds( $arrmixValues['call_agent_status_type_ids'] );
		} else {
			$this->setCallAgentStatusTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['call_agent_ids'] ) && true == valArr( $arrmixValues['call_agent_ids'] ) ) {
			$this->setCallAgentIds( implode( ',', array_filter( $arrmixValues['call_agent_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['call_agent_ids'] ) ) {
			$this->setCallAgentIds( $arrmixValues['call_agent_ids'] );
		} else {
			$this->setCallAgentIds( NULL );
		}

		if( true == isset( $arrmixValues['department_id'] ) ) {
			$this->setDepartmentId( $arrmixValues['department_id'] );
		}

		if( true == isset( $arrmixValues['department_ids'] ) && true == valArr( $arrmixValues['department_ids'] ) ) {
			$this->setDepartmentIds( implode( ',', array_filter( $arrmixValues['department_ids'] ) ) );
		} elseif( true == isset( $arrmixValues['department_ids'] ) ) {
			$this->setDepartmentIds( $arrmixValues['department_ids'] );
		} else {
			$this->setDepartmentIds( NULL );
		}

		if( true == isset( $arrmixValues['call_agent_is_disabled_statuses'] ) && '' != $arrmixValues['call_agent_is_disabled_statuses'] ) {
			$this->setCallAgentIsDisabled( $arrmixValues['call_agent_is_disabled_statuses'] );
		} else {
			$this->setCallAgentIsDisabled( NULL );
		}

		if( true == isset( $arrmixValues['call_agent_on_project_reason_ids'] ) && true == valArr( $arrmixValues['call_agent_on_project_reason_ids'] ) ) {
			$this->setCallAgentOnProjectReasonIds( implode( ',', array_filter( $arrmixValues['call_agent_on_project_reason_ids'] ) ) );
		} elseif( ( true == isset( $arrmixValues['call_agent_on_project_reason_ids'] ) ) ) {
			$this->setCallAgentOnProjectReasonIds( $arrmixValues['call_agent_on_project_reason_ids'] );
		} else {
			$this->setCallAgentOnProjectReasonIds( NULL );
		}

		if( true == isset( $arrmixValues['call_analysis_status_types'] ) && true == valArr( $arrmixValues['call_analysis_status_types'] ) ) {
			$this->setCallAnalysisStatusTypes( implode( ',', array_filter( $arrmixValues['call_analysis_status_types'] ) ) );
		} elseif( ( true == isset( $arrmixValues['call_analysis_status_types'] ) ) ) {
			$this->setCallAnalysisStatusTypes( $arrmixValues['call_analysis_status_types'] );
		} else {
			$this->setCallAnalysisStatusTypes( NULL );
		}

		if( true == isset( $arrmixValues['call_agent_vacation_request_statuses'] ) && true == valArr( $arrmixValues['call_agent_vacation_request_statuses'] ) ) {
			$this->setCallAgentVacationRequestStatuses( implode( ',', array_filter( $arrmixValues['call_agent_vacation_request_statuses'] ) ) );
		} elseif( ( true == isset( $arrmixValues['call_agent_vacation_request_statuses'] ) ) ) {
			$this->setCallAgentVacationRequestStatuses( $arrmixValues['call_agent_vacation_request_statuses'] );
		} else {
			$this->setCallAgentVacationRequestStatuses( NULL );
		}

		if( true == isset( $arrmixValues['occurrence_type_ids'] ) && true == valArr( $arrmixValues['occurrence_type_ids'] ) ) {
			$this->setOccurrenceTypeIds( implode( ',', array_filter( $arrmixValues['occurrence_type_ids'] ) ) );
		} elseif( ( true == isset( $arrmixValues['occurrence_type_ids'] ) ) ) {
			$this->setOccurrenceTypeIds( $arrmixValues['occurrence_type_ids'] );
		} else {
			$this->setOccurrenceTypeIds( NULL );
		}

		if( true == isset( $arrmixValues['dispute_status'] ) && true == valArr( $arrmixValues['dispute_status'] ) ) {
			$this->setDisputeStatus( implode( ',', $arrmixValues['dispute_status'] ) );
		} elseif( true == isset( $arrmixValues['dispute_status'] ) ) {
			$this->setDisputeStatus( $arrmixValues['dispute_status'] );
		} else {
			$this->setDisputeStatus( NULL );
		}

		if( true == isset( $arrmixValues['report_column_name'] ) && true == valStr( $arrmixValues['report_column_name'] ) ) {
			$this->setReportColumnName( $arrmixValues['report_column_name'] );
		}

		if( true == isset( $arrmixValues['call_agent_data'] ) ) 	$this->setCallAgentData( $arrmixValues['call_agent_data'] );

		if( true == isset( $arrmixValues['call_agent_name'] ) ) 	$this->setCallAgentName( $arrmixValues['call_agent_name'] );

		if( true == isset( $arrmixValues['report_by'] ) ) 			$this->setReportBy( $arrmixValues['report_by'] );

		if( true == isset( $arrmixValues['employee_status_type_id'] ) && true == valId( $arrmixValues['employee_status_type_id'] ) ) $this->setEmployeeStatusTypeId( $arrmixValues['employee_status_type_id'] );

		if( true == isset( $arrmixValues['report_distinct_leads'] ) ) {
			$this->setReportDistinctLeads( $arrmixValues['report_distinct_leads'] );
		}

		if( true == isset( $arrmixValues['percentage_call_count'] ) ) $this->setPercentageCallCount( $arrmixValues['percentage_call_count'] );
		if( true == isset( $arrmixValues['interval_days'] ) ) $this->setIntervalDays( $arrmixValues['interval_days'] );
		if( true == isset( $arrmixValues['followup_type_id'] ) ) $this->setFollowupTypeIds( $arrmixValues['followup_type_id'] );

		if( true == isset( $arrmixValues['country_codes'] ) && true == valStr( $arrmixValues['country_codes'] ) ) {
			$this->setCountryCodes( $arrmixValues['country_codes'] );
		}

		if( true == isset( $arrmixValues['country_codes'] ) && true == valArr( $arrmixValues['country_codes'] ) ) {
			$this->setCountryCodes( implode( ',', $arrmixValues['country_codes'] ) );
		}

		if( true == isset( $arrmixValues['selected_tab'] ) ) $this->setFollowupStatusTypeId( $arrmixValues['selected_tab'] );

		return;
	}

	/**
	 * Create or Built Functions
	 *
	 */

	public function builtCallDownloadLogCallFilter() {
		$this->setProperties( $this->getPropertyIds() );
		$this->setCallTypes( $this->getCallTypeIds() );
		if( false == is_null( $this->getAnswerStatusTypeIds() ) ) {
			if( CCallFilter::CALL_RESULT_TYPE_UNANSWERED === $this->getAnswerStatusTypeIds() ) {
				$this->setCallStatusTypes( CCallStatusType::UNANSWERED );
			} elseif( CCallFilter::CALL_RESULT_TYPE_ANSWERED === $this->getAnswerStatusTypeIds() ) {
				$this->setCallStatusTypes( CCallStatusType::ANSWERED );
			} elseif( true == in_array( CCallStatusType::ANSWERED, explode( ',', $this->getAnswerStatusTypeIds() ) ) && true == in_array( CCallStatusType::UNANSWERED, explode( ',', $this->getAnswerStatusTypeIds() ) ) ) {
				$this->setCallStatusTypes( CCallStatusType::ANSWERED, CCallStatusType::UNANSWERED );
			}
		}

		$this->setCallResults( $this->getCallResultIds() );

		if( false == is_null( $this->getCallIds() ) ) {
			$this->setGenericData( $this->getCallIds() );
		} elseif( false == valArr( $this->getGenericData() ) ) {
			$this->setGenericData( $this->getGenericData() );
		}

		$this->setVoicemailStatusTypes( $this->getVoicemailStatusTypeIds() );
		$this->setName( 'Call Download Logs' );
		$this->setDescription( 'Call Download Logs' );
		$this->setCallFilterTypeId( CCallFilterType::CALL_DOWNLOAD_LOGS );
		return $this;
	}

	public function createCallDownloadLog() {
		$objCallDownloadLog = new CCallDownloadLog();
		$objCallDownloadLog->setDefaults();

		$objCallDownloadLog->setCid( $this->getCid() );
		$objCallDownloadLog->setCallFilterId( $this->getId() );
		return $objCallDownloadLog;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valReportBy() {
		$boolIsValid = true;
		if( false == valStr( $this->getReportBy() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Report By is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTeamIds() {
		$boolIsValid = true;
		if( false == valStr( $this->getTeamIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Team is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCallAgentIds() {
		$boolIsValid = true;
		if( false == valStr( $this->getCallAgentIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Call Agent is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valEmployeeIds() {
		$boolIsValid = true;
		if( false == valStr( $this->getEmployeeIds() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Call Agent is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCountEmployeeIds() {
		$boolIsValid = true;

		if( 10 < substr_count( $this->getEmployeeIds(), ',' ) + 1 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Maximum 10 Call Agents can be selected.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getStartDate() ) && false == CValidation::validateDate( $this->getStartDate() ) && 'Min' != $this->getStartDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date of format mm/dd/yyyy is required.' ) );
		} elseif( false == is_null( $this->getStartDate() ) ) {
			// check the dates to make sure the starting date isn't after the ending date
			if( true == is_null( $this->getEndDate() ) ) return $boolIsValid;

			$strStartDate = strtotime( $this->getStartDate() );
			$strEndDate = strtotime( $this->getEndDate() );

			if( 'Max' != $this->getEndDate() && $strStartDate > $strEndDate ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Ending date must be greater than starting date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function getValues() {
		if( true == isset( $this->m_strCallId ) )						$arrmixValues['call_id']	= $this->m_strCallId;
		if( true == isset( $this->m_boolIsFailed ) )					$arrmixValues['is_failed']	= $this->m_boolIsFailed;
		if( true == isset( $this->m_strAllLeads ) )						$arrmixValues['all_leads'] = $this->m_strAllLeads;
		if( true == isset( $this->m_strGenericData ) )					$arrmixValues['advanced_search_generic_data'] = $this->m_strGenericData;
		if( true == isset( $this->m_intPropertyId ) )					$arrmixValues['property_id']	= $this->m_intPropertyId;
		if( true == isset( $this->m_intCid ) )							$arrmixValues['cid']	= $this->m_intCid;
		if( true == isset( $this->m_strStartDate ) )					$arrmixValues['start_date']	= $this->m_strStartDate;
		if( true == isset( $this->m_strEndDate ) )						$arrmixValues['end_date'] 	= $this->m_strEndDate;
		if( true == isset( $this->m_strSortBy ) )						$arrmixValues['sort_by']	= $this->m_strSortBy;
		if( true == isset( $this->m_strSortDirection ) )				$arrmixValues['sort_direction']	= $this->m_strSortDirection;

		if( true == isset( $this->m_strCallAnalyzed ) )					$arrmixValues['call_analyzed'] = $this->m_strCallAnalyzed;

		if( true == isset( $this->m_intCurrentSelectedLeadsPageNo ) )	$arrmixValues['is_current_leads_page_selected']	= $this->m_intCurrentSelectedLeadsPageNo;
		if( true == isset( $this->m_intIsAllLeadsPageSelected ) )		$arrmixValues['is_all_leads_page_selected']	= $this->m_intIsAllLeadsPageSelected;
		if( true == isset( $this->m_intIsAllCallsSelected ) )			$arrmixValues['is_all_calls_selected']	= $this->m_intIsAllCallsSelected;
		if( true == isset( $this->m_intIsAllLeadsDeselected ) )			$arrmixValues['is_all_leads_deselected']	= $this->m_intIsAllLeadsDeselected;
		if( true == isset( $this->m_intIsAllCallsDeselected ) )			$arrmixValues['is_all_calls_deselected']	= $this->m_intIsAllCallsDeselected;
		if( true == isset( $this->m_intCurrentSelectedLeadsPageNo ) )	$arrmixValues['current_selected_leads_page_no']	= $this->m_intCurrentSelectedLeadsPageNo;
		if( true == isset( $this->m_intEmployeeId ) )					$arrmixValues['employee_id']	= $this->m_intEmployeeId;
		if( true == isset( $this->m_intIntervalTypeId ) )				$arrmixValues['interval_type_id']	= $this->m_intIntervalTypeId;
		if( true == isset( $this->m_intOutboundCallId ) )				$arrmixValues['outbound_call_id']	= $this->m_intOutboundCallId;
		if( true == isset( $this->m_intDepartmentId ) )					$arrmixValues['department_id']	= $this->m_intDepartmentId;
		if( true == isset( $this->m_intPageNo ) )						$arrmixValues['page_no']	= $this->m_intPageNo;

		if( true == isset( $this->m_strAfterDatetime ) )				$arrmixValues['after_datetime']	= $this->m_strAfterDatetime;
		if( true == isset( $this->m_strBeforeDatetime ) )				$arrmixValues['before_datetime']	= $this->m_strBeforeDatetime;
		if( true == isset( $this->m_strPsProductIds ) )					$arrmixValues['ps_product_ids']	= $this->m_strPsProductIds;
		if( true == isset( $this->m_strCallIds ) )						$arrmixValues['call_ids']	= $this->m_strCallIds;
		if( true == isset( $this->m_intCallQueueId ) )					$arrmixValues['call_queue_id']	= $this->m_intCallQueueId;
		if( true == isset( $this->m_strDataType ) )						$arrmixValues['data_type']	= $this->m_strDataType;
		if( true == isset( $this->m_strPhoneNumber ) )					$arrmixValues['phone_number']	= $this->m_strPhoneNumber;
		if( true == isset( $this->m_strCallsState ) )					$arrmixValues['calls_state']	= $this->m_strCallsState;

		if( true == isset( $this->m_intCreatedBy ) )					$arrmixValues['created_by']	= $this->m_intCreatedBy;
		if( true == isset( $this->m_strTableName ) )					$arrmixValues['table_name']	= $this->m_strTableName;
		if( true == isset( $this->m_strAction ) )						$arrmixValues['action']	= $this->m_strAction;
		if( true == isset( $this->m_boolIsShowClientsOnMaintenance ) )	$arrmixValues['is_show_clients_on_maintenance']	= $this->m_boolIsShowClientsOnMaintenance;
		if( true == isset( $this->m_strShowVanity ) )					$arrmixValues['show_vanity']	= $this->m_strShowVanity;

		if( true == isset( $this->m_strCompanyEmployeeIds ) )			$arrmixValues['company_employee_ids']	= explode( ',', $this->m_strCompanyEmployeeIds );
		if( true == isset( $this->m_strPropertyIds ) )					$arrmixValues['property_ids']	= explode( ',', $this->m_strPropertyIds );
		if( true == isset( $this->m_strCids ) )							$arrmixValues['cids'] = explode( ',', $this->m_strCids );
		if( true == isset( $this->m_strEmployeeIds ) )					$arrmixValues['employee_ids']	= explode( ',', $this->m_strEmployeeIds );

		if( true == isset( $this->m_strLeasingAgentIds ) )				$arrmixValues['leasing_agent_ids']	= explode( ',', $this->m_strLeasingAgentIds );
		if( true == isset( $this->m_strCallTypeIds ) )					$arrmixValues['call_type_ids']	= explode( ',', $this->m_strCallTypeIds );
		if( true == isset( $this->m_strCallResultIds ) )				$arrmixValues['call_result_ids']	= explode( ',', $this->m_strCallResultIds );
		if( true == isset( $this->m_strLeadSourceIds ) )				$arrmixValues['lead_source_ids']	= explode( ',', $this->m_strLeadSourceIds );
		if( true == isset( $this->m_strAnswerStatusTypeIds ) )			$arrmixValues['answer_status_type_ids']	= explode( ',', $this->m_strAnswerStatusTypeIds );
		if( true == isset( $this->m_strVoicemailStatusTypeIds ) )		$arrmixValues['voicemail_status_type_ids']	= explode( ',', $this->m_strVoicemailStatusTypeIds );
		if( true == isset( $this->m_arrintCallStatusTypeIds ) )			$arrmixValues['call_status_type_ids']	= explode( ',', $this->m_arrintCallStatusTypeIds );
		if( true == isset( $this->m_arrintOutboundCallCampaignIds ) )	$arrmixValues['outbound_call_campaign_ids']	= explode( ',', $this->m_arrintOutboundCallCampaignIds );
		if( true == isset( $this->m_strGreetingTypeIds ) )				$arrmixValues['greeting_type_ids']	= explode( ',', $this->m_strGreetingTypeIds );
		if( true == isset( $this->m_strCallDirectionTypeIds ) )			$arrmixValues['call_direction_type_ids']	= explode( ',', $this->m_strCallDirectionTypeIds );
		if( true == isset( $this->m_strCallQueueIds ) )					$arrmixValues['call_queue_ids']	= explode( ',', $this->m_strCallQueueIds );
		if( true == isset( $this->m_strTeamIds ) )						$arrmixValues['team_ids']	= explode( ',', $this->m_strTeamIds );
		if( true == isset( $this->m_strUserIds ) )						$arrmixValues['user_ids']	= explode( ',', $this->m_strUserIds );
		if( true == isset( $this->m_strCallAgentStatusTypeIds ) )		$arrmixValues['call_agent_status_type_ids']	= explode( ',', $this->m_strCallAgentStatusTypeIds );
		if( true == isset( $this->m_strCallAgentIds ) )					$arrmixValues['call_agent_ids']	= explode( ',', $this->m_strCallAgentIds );
		if( true == isset( $this->m_strDepartmentIds ) )				$arrmixValues['department_ids']	= explode( ',', $this->m_strDepartmentIds );
		if( true == isset( $this->m_strStartTime ) )					$arrmixValues['start_time']	= $this->m_strStartTime;
		if( true == isset( $this->m_strEndTime ) )						$arrmixValues['end_time']	= $this->m_strEndTime;
		if( true == isset( $this->m_strReportColumnName ) )				$arrmixValues['report_column_name']	= $this->m_strReportColumnName;
		if( true == isset( $this->m_strReportCallPeriod ) )				$arrmixValues['report_call_period']	= $this->m_strReportCallPeriod;
		if( true == isset( $this->m_boolCallAgentIsDisabled ) )				$arrmixValues['call_agent_is_disabled_statuses']	= $this->m_boolCallAgentIsDisabled;
		if( true == isset( $this->m_arrintCallAgentOnProjectReasonIds ) )	$arrmixValues['call_agent_on_project_reason_ids']	= explode( ',', $this->m_arrintCallAgentOnProjectReasonIds );
		if( true == isset( $this->m_strCallAnalysisStatusTypes ) )			$arrmixValues['call_analysis_status_types']	= explode( ',', $this->m_strCallAnalysisStatusTypes );
		if( true == isset( $this->m_strApplicantApplicationIds ) )			$arrmixValues['applicant_application_ids']	= explode( ',', $this->m_strApplicantApplicationIds );
		if( true == isset( $this->m_boolIsShowArchiveCalls ) )				$arrmixValues['is_show_archive_calls']	= $this->m_boolIsShowArchiveCalls;
		if( true == isset( $this->m_boolIsShowAllCalls ) )					$arrmixValues['is_show_all_calls']	= $this->m_boolIsShowAllCalls;
		if( true == isset( $this->m_boolReportDistinctLeads ) )				$arrmixValues['report_distinct_leads']	= $this->m_boolReportDistinctLeads;
		if( true == isset( $this->m_intCallSourceId ) )						$arrmixValues['call_source_id'] = $this->m_intCallSourceId;
		if( true == isset( $this->m_strTabName ) )						$arrmixValues['call_source_id'] = $this->m_strTabName;
		return $arrmixValues;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getEndDate() ) && false == CValidation::validateDate( $this->getEndDate() ) && 'Max' != $this->getEndDate() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date of format mm/dd/yyyy is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_adherence_report_filters':
				$boolIsValid &= $this->valReportBy();
				if( 'team' == $this->getReportBy() ) {
					$boolIsValid &= $this->valTeamIds();
				} elseif( 'agent' == $this->getReportBy() ) {
					$boolIsValid &= $this->valCallAgentIds();
				}

				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				break;

			case 'validate_gamification_sandbox_report_filters':
				$boolIsValid &= $this->valReportBy();
				if( 'team' == $this->getReportBy() ) {
					$boolIsValid &= $this->valTeamIds();
				} elseif( 'agent' == $this->getReportBy() ) {
					$boolIsValid &= $this->valEmployeeIds();
					if( true == $boolIsValid ) {
						$boolIsValid &= $this->valCountEmployeeIds();
					}
				}

				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				break;

			case 'validate_call_details_filter':
				$boolIsValid &= $this->valCallIds();
				$boolIsValid &= $this->valPhoneNumber();
				break;

			default:
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				break;
		}

		return $boolIsValid;
	}

	public function changeDateFormat() {
		if( 'Min' != $this->getStartDate() && false == is_null( $this->getStartDate() ) && '' != $this->getStartDate() ) {
			$this->setStartDate( date( 'm/d/Y', strtotime( $this->getStartDate() ) ) );
		} else {
			$this->setStartDate( 'Min' );
		}

		if( 'Max' != $this->getEndDate() && false == is_null( $this->getEndDate() ) && '' != $this->getEndDate() ) {
			$this->setEndDate( date( 'm/d/Y', strtotime( $this->getEndDate() ) ) );
		} else {
			$this->setEndDate( 'Max' );
		}
	}

	public function valStartEndDate() {
		$boolIsValid = true;

		if( true == valStr( $this->getStartDate() ) && false == valStr( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Select end date.' ) );
		} elseif( false == valStr( $this->getStartDate() ) && true == valStr( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Select start date.' ) );
		} elseif( strtotime( $this->getStartDate() . ' ' . $this->getStartTime() ) > strtotime( $this->getEndDate() . ' ' . $this->getEndTime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'End date must be greater than start date.' ) );
		}

		return $boolIsValid;
	}

	public function valCallIds() {
		$boolIsValid = true;

		$boolIsValid &= preg_match( '/^\d+(?:,\d+)*$/', str_replace( ' ', '', $this->m_strCallIds ) );

		if( true == valStr( $this->m_strCallIds ) && false == $boolIsValid ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Provide valid call ids.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		$boolIsValid &= preg_match( '/[\d()-]$/', str_replace( ' ', '', $this->m_strPhoneNumber ) );

		if( true == valStr( $this->m_strPhoneNumber ) && false == $boolIsValid ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is invalid.', NULL ) );
		}
		return $boolIsValid;
	}

	public function toArray() {
		$arrmixCallFilters = parent::toArray();

		$arrmixCallFilters['calls_state']						= $this->getCallsState();
		$arrmixCallFilters['property_ids']						= $this->getPropertyIds();
		$arrmixCallFilters['transcribed_call']					= $this->getTranscribedCall();
		$arrmixCallFilters['call_ids']							= $this->getCallIds();
		$arrmixCallFilters['phone_number']						= $this->getPhoneNumber();
		$arrmixCallFilters['company_status_type_ids']			= $this->getCompanyStatusTypeIds();
		$arrmixCallFilters['call_type_ids']						= $this->getCallTypeIds();
		$arrmixCallFilters['call_direction_type_ids']			= $this->getCallDirectionTypeIds();
		$arrmixCallFilters['employee_ids']						= $this->getEmployeeIds();
		$arrmixCallFilters['call_queue_ids']					= $this->getCallQueueIds();
		$arrmixCallFilters['call_result_ids']					= $this->getCallResultIds();
		$arrmixCallFilters['answer_status_type_ids']			= $this->getAnswerStatusTypeIds();
		$arrmixCallFilters['voicemail_status_type_ids']			= $this->getVoicemailStatusTypeIds();
		$arrmixCallFilters['call_source_id']					= $this->getCallSourceId();
		$arrmixCallFilters['country_codes']					    = $this->getCountryCodes();
		$arrmixCallFilters['office']					        = $this->getOffice();

		return $arrmixCallFilters;
	}

}
?>