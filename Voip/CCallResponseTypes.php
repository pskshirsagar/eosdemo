<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallResponseTypes
 * Do not add any new functions to this class.
 */

class CCallResponseTypes extends CBaseCallResponseTypes {

	public static function fetchCallResponseTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallResponseType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallResponseType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallResponseType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallResponseTypeByHandle( $strHandle, $objVoipDatabase ) {
		return self::fetchCallResponseType( 'SELECT *FROM call_response_types WHERE handle = \'' . $strHandle . '\' LIMIT 1', $objVoipDatabase );
	}
}
?>