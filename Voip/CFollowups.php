<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CFollowups
 * Do not add any new functions to this class.
 */

class CFollowups extends CBaseFollowups {

	public static function fetchPaginatedFollowupsByFollowupTypeId( $objManualContactQueueFilter, $objPagination, $intFollowupTypeId, $objVoipDatabase, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlSearchCondition	= NULL;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) && false == is_null( $objPagination->getQuickSearch() ) ) {
			$strSqlSearchCondition .= ' ( lower( CAST( details->>\'applicant_name\' AS TEXT ) ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' )
										OR lower( CAST( details->>\'client_name\' AS TEXT ) ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' )
										OR CAST( id AS TEXT ) LIKE \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\'
										OR lower( CAST( details->>\'property_name\' AS TEXT ) ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) ) AND';
		}

		if( true == valObj( $objManualContactQueueFilter, 'CManualContactQueueFilter' ) && true == valStr( $objManualContactQueueFilter->getCids() ) ) {
			$strSqlSearchCondition .= ' ( cid IN ( ' . $objManualContactQueueFilter->getCids() . ' ) AND';
		}

		if( true == valObj( $objManualContactQueueFilter, 'CManualContactQueueFilter' ) && true == valStr( $objManualContactQueueFilter->getPropertyIds() ) ) {
			$strSqlSearchCondition .= ' ( property_id IN ( ' . $objManualContactQueueFilter->getPropertyIds() . ' ) AND';
		}

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT( id ) AS followup_count FROM followups WHERE ' . $strSqlSearchCondition . ' followup_type_id = ' . ( int ) $intFollowupTypeId;
		} else {
			$strSql	= 'SELECT
							*
						FROM
							followups 
						WHERE
							' . $strSqlSearchCondition . ' 
							followup_type_id = ' . ( int ) $intFollowupTypeId . ' 
						ORDER BY id ASC, created_on DESC';
		}

		if( true == valStr( $strSqlOffsetLimit ) ) {
			$strSql .= $strSqlOffsetLimit;
		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['followup_count'] ) ) ? $arrintResponse[0]['followup_count'] : 0;
		} else {
			return CFollowups::fetchFollowups( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchSimpleFollowupHavingLongestWaitingTimeInQueue( $intFollowupTypeId,  $objVoipDatabase ) {
		$strSql		= 'SELECT 
							EXTRACT(EPOCH FROM now()::timestamp(0)) - EXTRACT(EPOCH FROM created_on::timestamp(0)) as longest_waiting_time
						FROM
							followups 
						WHERE
							followup_type_id = ' . ( int ) $intFollowupTypeId . '
							AND followup_status_type_id = ' . \CFollowupStatusType::QUEUED . '
							AND details->\'is_deleted\' IS NULL 
							AND completed_on IS NULL
							AND call_agent_id IS NULL
							ORDER BY created_on DESC LIMIT 1';
		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleFollowupsCountByFollowupTypeIdByDuration( $intFollowupTypeId, $intDurationHour, $objVoipDatabase ) {

		$strStartDateTime		= date( 'Y-m-d H:i:s', strtotime( '-' . $intDurationHour . ' hours' ) );
		$strEndDateTime			= date( 'Y-m-d H:i:s' );

		$strSql		= 'SELECT
							COUNT( DISTINCT( details->> \'unique_guest_card_appointment_identifier\' ) ) AS appointment_count
						FROM 
							followups
						WHERE 
							followup_type_id = ' . ( int ) $intFollowupTypeId . '
							AND created_on >= \'' . $strStartDateTime . '\'
							AND created_on <= \'' . $strEndDateTime . '\'';
		$arrintFollowupCount = fetchData( $strSql, $objVoipDatabase );

		return $arrintFollowupCount[0]['appointment_count'];
	}

	public static function fetchFollowupByClientIdByPropertyId( $intClientId, $intPropertyId, $objVoipDatabase, $intReviewId = NULL ) {
		$strSql = 'SELECT 
						*
					FROM
						followups
					WHERE
						cid = ' . ( int ) $intClientId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND followup_type_id = ' . CFollowupType::REVIEW_RESPONSE;

		if( true == valId( $intReviewId ) ) {
			$strSql .= ' AND details ->> \'review_id\' = \'' . $intReviewId . '\'';
		}

		return self::fetchFollowup( $strSql, $objVoipDatabase );
	}

}
?>
