<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallDirections
 * Do not add any new functions to this class.
 */

class CCallDirections extends CBaseCallDirections {

	public static function fetchCallDirection( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallDirection', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCallDirections( $objVoipDatabase ) {
		return self::fetchCallDirections( 'SELECT * FROM call_directions ORDER BY name ASC', $objVoipDatabase );
	}

}
?>