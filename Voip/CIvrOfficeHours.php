<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrOfficeHours
 * Do not add any new functions to this class.
 */

class CIvrOfficeHours extends CBaseIvrOfficeHours {

	public static function fetchIvrOfficeHoursByIvrId( $intIvrId, $objVoipDatabase, $boolIsWeekend = false ) {
		$strSql = 'SELECT 
						ivr_office_hours.*
					FROM
						ivr_office_hours
					WHERE
						ivr_id = ' . ( int ) $intIvrId . '
					AND is_weekend = ' . ( true == $boolIsWeekend ? '\'true\'' : '\'false\'' ) . '
					ORDER BY 
						week_day 
					ASC';

		return self::fetchIvrOfficeHours( $strSql, $objVoipDatabase );
	}

	public static function fetchAllIvrOfficeHoursByIvrId( $intIvrId, $objVoipDatabase ) {
		$strSql = 'SELECT ivr_office_hours.* FROM ivr_office_hours WHERE ivr_id = ' . ( int ) $intIvrId . ' ORDER BY week_day ASC';
		return self::fetchIvrOfficeHours( $strSql, $objVoipDatabase );
	}
}
?>
