<?php

class CCallAgentRedeemedProduct extends CBaseCallAgentRedeemedProduct {

	protected $m_intCallAgentEmployeeId;
	protected $m_intStoreProductTypeId;
	protected $m_intRankPointMappingId;
	protected $m_strCallAgentName;
	protected $m_strStoreProductName;
	protected $m_strStoreProductTypeName;
	protected $m_strFrequencyName;
	protected $m_fltStoreProductCost;
	protected $m_fltPerkCost;

	public static $c_arrstrRedemptionStatuses = [
		'requested' => 'Requested',
		'purchased' => 'Purchased',
		'fulfilled' => 'Fulfilled'
	];

	/**
	 * Set Get Functions
	 */

	public function setCallAgentEmployeeId( $intCallAgentEmployeeId ) {
		$this->m_intCallAgentEmployeeId = $intCallAgentEmployeeId;
	}

	public function getCallAgentEmployeeId() {
		return $this->m_intCallAgentEmployeeId;
	}

	public function setStoreProductTypeId( $intStoreProductTypeId ) {
		$this->m_intStoreProductTypeId = $intStoreProductTypeId;
	}

	public function getStoreProductTypeId() {
		return $this->m_intStoreProductTypeId;
	}

	public function setRankPointMappingId( $intRankPointMappingId ) {
		$this->m_intRankPointMappingId = $intRankPointMappingId;
	}

	public function getRankPointMappingId() {
		return $this->m_intRankPointMappingId;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function setStoreProductName( $strStoreProductName ) {
		$this->m_strStoreProductName = $strStoreProductName;
	}

	public function getStoreProductName() {
		return $this->m_strStoreProductName;
	}

	public function setStoreProductCost( $fltStoreProductCost ) {
		$this->m_fltStoreProductCost = $fltStoreProductCost;
	}

	public function setPerkCost( $fltPerkCost ) {
		$this->m_fltPerkCost = $fltPerkCost;
	}

	public function getStoreProductCost() {
		return ( float ) $this->m_fltStoreProductCost;
	}

	public function getPerkCost() {
		return ( float ) $this->m_fltPerkCost;
	}

	public function setStoreProductTypeName( $strStoreProductTypeName ) {
		$this->m_strStoreProductTypeName = $strStoreProductTypeName;
	}

	public function getStoreProductTypeName() {
		return $this->m_strStoreProductTypeName;
	}

	public function setFrequencyName( $strFrequencyName ) {
		$this->m_strFrequencyName = $strFrequencyName;
	}

	public function getFrequencyName() {
		return $this->m_strFrequencyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['call_agent_employee_id'] ) ) $this->setCallAgentEmployeeId( $arrmixValues['call_agent_employee_id'] );
		if( true == isset( $arrmixValues['store_product_type_id'] ) ) $this->setStoreProductTypeId( $arrmixValues['store_product_type_id'] );
		if( true == isset( $arrmixValues['rank_point_mapping_id'] ) ) $this->setRankPointMappingId( $arrmixValues['rank_point_mapping_id'] );
		if( true == isset( $arrmixValues['call_agent_name'] ) ) $this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['store_product_name'] ) ) $this->setStoreProductName( $arrmixValues['store_product_name'] );
		if( true == isset( $arrmixValues['store_product_cost'] ) ) $this->setStoreProductCost( $arrmixValues['store_product_cost'] );
		if( true == isset( $arrmixValues['perk_cost'] ) ) $this->setPerkCost( $arrmixValues['perk_cost'] );
		if( true == isset( $arrmixValues['store_product_type_name'] ) ) $this->setStoreProductTypeName( $arrmixValues['store_product_type_name'] );
		if( true == isset( $arrmixValues['frequency_name'] ) ) $this->setFrequencyName( $arrmixValues['frequency_name'] );
		return true;
	}

	/**
	 * Validate Functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStoreProductId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_product_id', 'Store product id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentRankId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRedeemedCoins() {
		$boolIsValid = true;

		if( false == valId( $this->getRedeemedCoins() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'redeemed_coins', 'Invalid redeemed coins.' ) );
		}

		return $boolIsValid;
	}

	public function valCreditBalance() {
		$boolIsValid = true;

		if( true == ( 0 > $this->getCreditBalance() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_balance', 'Not having sufficient credit balance to redeem this perk, please try again.' ) );
		}

		return $boolIsValid;
	}

	public function valRedeemedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRedeemedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceviedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceivedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_call_agent_redeemed_product_insert':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valStoreProductId();
				$boolIsValid &= $this->valRedeemedCoins();
				break;

			case 'validate_call_agent_perk_insert':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valStoreProductId();
				$boolIsValid &= $this->valCreditBalance();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>