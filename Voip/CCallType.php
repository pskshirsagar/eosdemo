<?php

class CCallType extends CBaseCallType {

	protected $m_intQueuePhoneExtensionId;
	protected $m_strCallQueueName;
	protected $m_strCallQueueDomain;
	protected $m_strCallSourceName;
	protected $m_strCallDirectionName;

	const RESIDENT_INSURE							    = 1;
	const RESIDENT_VERIFY								= 2;
	const UTILITY_BILLING_SUPPORT						= 3;
	const UTILITY_BILLING_ACCOUNT_MANAGEMENT			= 4;
	const UTILITY_BILLING_FAX							= 5;
	const ACCOUNT_MANAGEMENT							= 6;
	const DEVELOPMENT									= 7;
	const INSIDE_SALES									= 8;
	const OUTISDE_SALES									= 9;
	const CALL_TRACKER_LEAD								= 10;
	const CALL_TRACKER_MAINTENANCE						= 11;
	const CALL_TRACKER_MAINTENANCE_EMERGENCY			= 12;
	const ACCOUNTING									= 13;
	const IT											= 14;
	const CALL_MERCHANT_SERVICES						= 15;
	const CALL_CENTER									= 16;
	const CALL_TRACKER_VACANCY_ORGANIC					= 17;
	const CALL_TRACKER_VACANCY_PAID						= 18;
	const PROPERTY_FAX									= 19;
	const OUTBOUND_PROPERTY_SCHEDULED_CALL				= 20;
	const CALL_CENTER_LEAD								= 21;
	const CALL_CENTER_MAINTENANCE						= 22;
	const CALL_CENTER_EMERGENCY_MAINTENANCE				= 23;
	const MESSAGE_CENTER								= 24;
	const CALL_TRACKING_OFFICE							= 25;
	const MOVE_IN_CALLS									= 26;
	const NOTIFICATIONS									= 27;
	const OUTBOUND_SUPPORT								= 28;
	const NET_PROMOTER_SCORE							= 29;
	const TEST											= 30;
	const VISA_PHONE_AUTH								= 31;
	const PSI_OFFICE									= 32;
	const RESIDENT_SUPPORT								= 33;
	const LEASING_CENTER_OUTBOUND						= 35;
	const ENTRATA										= 36;
	const GOOGLE_PPC									= 37;
	const VENDOR_ACCESS									= 38;
	const CALL_TRACKER_LEAD_VIA_IVR						= 39;
	const CALL_TRACKER_MAINTENANCE_VIA_IVR				= 40;
	const CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR	= 41;
	const CALL_CENTER_LEAD_VIA_IVR						= 42;
	const CALL_CENTER_MAINTENANCE_VIA_IVR				= 43;
	const CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR		= 44;
	const DIRECT_EXTENSION_LINE							= 45;
	const OUTBOUND_CALL_CAMPAIGN						= 46;
	const CORPORATE_CARE_LINE							= 47;
	const MANUAL_FOLLOWUP_CALL							= 48;
	const SMS_ONLY										= 49;

	public static $c_arrintLeasingCenterCallTypes = [
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_TRACKING_OFFICE,
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::LEASING_CENTER_OUTBOUND
	];

	public static $c_arrintLeasingCenterInboundCallTypes = [
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_TRACKING_OFFICE,
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKER_LEAD_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrintVanityCallTypes = [
		self::CALL_TRACKER_LEAD						=> self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE				=> self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY	=> self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::PROPERTY_FAX							=> self::PROPERTY_FAX,
		self::CALL_TRACKING_OFFICE					=> self::CALL_TRACKING_OFFICE,
		self::RESIDENT_SUPPORT						=> self::RESIDENT_SUPPORT,
		self::SMS_ONLY								=> self::SMS_ONLY
	];

	public static $c_arrintCallTrackerCallTypes = [
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_TRACKER_LEAD_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrintCallFileCallTypes = [
		self::RESIDENT_INSURE,
		self::UTILITY_BILLING_SUPPORT,
		self::INSIDE_SALES,
		self::OUTISDE_SALES,
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_CENTER,
		self::CALL_TRACKER_VACANCY_ORGANIC,
		self::CALL_TRACKER_VACANCY_PAID,
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKING_OFFICE,
		self::NOTIFICATIONS,
		self::RESIDENT_SUPPORT,
		self::GOOGLE_PPC,
		self::VENDOR_ACCESS,
		self::CALL_TRACKER_LEAD_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrstrCallPhoneNumberCallTypes	= [
		self::ACCOUNTING							=> 'Accounting',
		self::ACCOUNT_MANAGEMENT					=> 'Account Management',
		self::CALL_CENTER							=> 'Call Center',
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY	=> 'Call Tracker Emergency',
		self::CALL_TRACKER_LEAD						=> 'Call Tracker Lead',
		self::CALL_TRACKER_MAINTENANCE				=> 'Call Tracker Maintenance',
		self::DEVELOPMENT							=> 'Development',
		self::ENTRATA								=> 'Entrata',
		self::INSIDE_SALES							=> 'Inside Sales',
		self::IT									=> 'IT',
		self::CALL_MERCHANT_SERVICES				=> 'Merchant Service',
		self::PSI_OFFICE							=> 'Psi Office',
		self::RESIDENT_SUPPORT						=> 'Resident Support'
	];

	public static $c_arrintOutboundCallCallTypes = [
		self::MOVE_IN_CALLS,
		self::OUTBOUND_SUPPORT,
		self::NET_PROMOTER_SCORE
	];

	public static $c_arrintLeadCallTypes = [
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_LEAD_VIA_IVR
	];

	public static $c_arrintLeasingCenterBasicCallTypes = [
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKING_OFFICE,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrintCallShadowCallTypes = [
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CALL_TRACKING_OFFICE,
		self::CALL_CENTER,
		self::MOVE_IN_CALLS,
		self::RESIDENT_SUPPORT,
		self::NOTIFICATIONS,
		self::OUTBOUND_SUPPORT,
		self::NET_PROMOTER_SCORE
	];

	public static $c_arrintUtilityBillingCallTypeIds = [
		self::UTILITY_BILLING_SUPPORT,
		self::UTILITY_BILLING_ACCOUNT_MANAGEMENT,
		self::UTILITY_BILLING_FAX
	];

	public static $c_arrintLeasingCenterBasicCallTypeIds = [
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKING_OFFICE,
		self::NOTIFICATIONS
	];

	public static $c_arrintAllOutboundCallTypes = [
		self::MOVE_IN_CALLS,
		self::OUTBOUND_SUPPORT,
		self::NET_PROMOTER_SCORE,
		self::LEASING_CENTER_OUTBOUND,
		self::OUTBOUND_PROPERTY_SCHEDULED_CALL
	];

	public static $c_arrintCallCenterCallTypeIds = [
		self::CALL_CENTER,
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKING_OFFICE,
		self::RESIDENT_SUPPORT,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::LEASING_CENTER_OUTBOUND
	];

	public static $c_arrintPropertyCallTypeIds = [
		self::CALL_TRACKER_LEAD,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_TRACKING_OFFICE
	];

	public static $c_arrintIvrCallTypeIds = [
		self::CALL_TRACKER_LEAD_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrintResidentCallTypeIds = [
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_VIA_IVR,
		self::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR
	];

	public static $c_arrintLeasingCenterAndSupportCallTypes = [
		self::RESIDENT_INSURE,
		self::RESIDENT_VERIFY,
		self::UTILITY_BILLING_SUPPORT,
		self::UTILITY_BILLING_ACCOUNT_MANAGEMENT,
		self::UTILITY_BILLING_FAX,
		self::INSIDE_SALES,
		self::CALL_CENTER,
		self::CALL_CENTER_LEAD,
		self::CALL_CENTER_MAINTENANCE,
		self::CALL_CENTER_EMERGENCY_MAINTENANCE,
		self::CALL_TRACKING_OFFICE,
		self::MOVE_IN_CALLS,
		self::NOTIFICATIONS,
		self::OUTBOUND_SUPPORT,
		self::NET_PROMOTER_SCORE,
		self::RESIDENT_SUPPORT,
		self::CALL_CENTER_LEAD_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_VIA_IVR,
		self::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR,
		self::CORPORATE_CARE_LINE
	];
	/**
	* set functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_queue_name'] ) )				$this->setCallQueueName( $arrmixValues['call_queue_name'] );
		if( true == isset( $arrmixValues['queue_phone_extension_id'] ) )	$this->setQueuePhoneExtensionId( $arrmixValues['queue_phone_extension_id'] );
		if( true == isset( $arrmixValues['call_queue_domain'] ) )			$this->setCallQueueDomain( $arrmixValues['call_queue_domain'] );
		if( true == isset( $arrmixValues['call_source_name'] ) )			$this->setCallSourceName( $arrmixValues['call_source_name'] );
		if( true == isset( $arrmixValues['call_direction_name'] ) )			$this->setCallDirectionName( $arrmixValues['call_direction_name'] );

		return true;
	}

	public function setQueuePhoneExtensionId( $intQueuePhoneExtensionId ) {
		$this->m_intQueuePhoneExtensionId = $intQueuePhoneExtensionId;
	}

 	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallQueueDomain( $strCallQueueDomain ) {
		$this->m_strCallQueueDomain = $strCallQueueDomain;
	}

	public function setCallSourceName( $strCallSourceName ) {
		$this->m_strCallSourceName = $strCallSourceName;
	}

	public function setCallDirectionName( $strCallDirectionName ) {
		$this->m_strCallDirectionName = $strCallDirectionName;
	}

	/**
	* get functions
	*
	*/

	public function getQueuePhoneExtensionId() {
		return $this->m_intQueuePhoneExtensionId;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallQueueDomain() {
		return $this->m_strCallQueueDomain;
	}

	public function getCallSourceName() {
		return $this->m_strCallSourceName;
	}

	public function getCallDirectionName() {
		return $this->m_strCallDirectionName;
	}

	/**
	* Validate Functions
	*
	*/

	public function valConflictName( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_ERROR );
		}

		$strWhere = ' WHERE LOWER(name) = LOWER(\'' . $this->getName() . '\')';

		if( true == valId( $this->getId() ) ) $strWhere .= ' AND id <> ' . ( int ) $this->getId();

		$intCallTypeCount = CCallTypes::fetchCallTypeCount( $strWhere, $objVoipDatabase );

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( 0 < $intCallTypeCount ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valAlphaNumeric() {
		$boolIsValid = true;

		if( true == getNumericValue( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is containing numeric data. ' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictName( $objVoipDatabase );
				$boolIsValid &= $this->valAlphaNumeric();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>