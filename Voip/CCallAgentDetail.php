<?php

class CCallAgentDetail extends CBaseCallAgentDetail {
	protected $m_intEmployeeId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagerCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['employee_id'] ) )		$this->setEmployeeId( $arrmixValues['employee_id'] );
		return true;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

}
?>