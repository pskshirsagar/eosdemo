<?php

class CCallTranscriptionLog extends CBaseCallTranscriptionLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOperationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFileConverted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsGpcRequestSent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCallTranscribed() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>