<?php

class CCallerType extends CBaseCallerType {

	const LEAD				= 1;
	const RESIDENT			= 2;
	const OTHER				= 5;
	const PROPERTY_MANAGER	= 6;
}
?>