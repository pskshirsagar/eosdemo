<?php

class CRewardPointCategory extends CBaseRewardPointCategory {

	const ATTENDANCE		= 1;
	const BASIC_EXPECTATION	= 2;
	const CALL_QUALITY		= 3;
	const CHALLENGE			= 4;
	const CONSISTENCY		= 5;
	const MULTIPLIERS		= 6;
	const PRODUCTIVITY		= 7;

	public static $c_arrstrRewardPointCategoriesColorCodes = [
		self::ATTENDANCE		=> '#1C9524',
		self::BASIC_EXPECTATION	=> '#3669C9',
		self::CALL_QUALITY		=> '#DA3A21',
		self::CHALLENGE			=> '#DB4778',
		self::CONSISTENCY		=> '#971497',
		self::MULTIPLIERS		=> '#189AC4',
		self::PRODUCTIVITY		=> '#FD9827'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>