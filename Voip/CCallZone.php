<?php

class CCallZone extends CBaseCallZone {

	const GREEN		= 1;
	const YELLOW	= 2;
	const RED		= 3;

	public static $c_arrintCallAgentPerformanceReportZoneIds = [
		self::YELLOW,
		self::RED
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>