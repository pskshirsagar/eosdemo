<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallStatusTypes
 * Do not add any new functions to this class.
 */

class COutboundCallStatusTypes extends CBaseOutboundCallStatusTypes {

	public static function fetchOutboundCallStatusTypes( $strSql, $objVoipDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'COutboundCallStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOutboundCallStatusType( $strSql, $objVoipDatabase ) {
		return parent::fetchCachedObject( $strSql, 'COutboundCallStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

 	public static function fetchAllOutboundCallStatusTypes( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM outbound_call_status_types ORDER BY name ASC';

		return self::fetchOutboundCallStatusTypes( $strSql, $objVoipDatabase );
	}

}
?>