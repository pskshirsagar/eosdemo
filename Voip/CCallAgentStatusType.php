<?php

class CCallAgentStatusType extends CBaseCallAgentStatusType {

	// predefined call agent status types.
	const LOGGED_OUT											= 1;
	const AVAILABLE												= 2; // Available means the person is logged in and gets to control their wrap up time.
	const AVAILABLE_ON_DEMAND									= 3; // Available On Demand means a person is logged in, and doesn't get to control their wrap up time.
	const ON_BREAK												= 4;
	const UNUSED												= 5;

	// Custom defined call agent status types.
	const ON_LUNCH												= 6;
	const ON_PROJECT											= 7;
	const ON_MEETING											= 8;
	const ON_TRAINING											= 9;
	const ON_COACHING											= 10;
	const IN_EMAIL_QUEUE										= 11;
	const IN_LEASING_CHAT										= 12;
	const APPROVED_TO											= 13;
	const COVERED_TO											= 14;
	const UNCOVERED_TO											= 15;
	const UNREPORTED_TO											= 16;
	const CONDITIONAL_APPROVED_TO 								= 17;
	const HR_APPROVED_OFF										= 18;

	// CallAgentOnProjectSubStatues
	const ON_PROJECT_REASON_WORKING_ON_TASK_FORCE				= 1;
	const ON_PROJECT_REASON_FOLLOWING_UP_WITH_MAINTENANCE_CALL	= 2;
	const ON_PROJECT_REASON_SHADOWING							= 3;
	const ON_PROJECT_REASON_OTHER								= 4;

	public static $c_arrintAvailableCallAgentStatusTypes = [ self::AVAILABLE, self::AVAILABLE_ON_DEMAND ];

	public static $c_arrintCallAgentStatusLogTypes = [
		self::AVAILABLE_ON_DEMAND,
		self::AVAILABLE,
		self::ON_BREAK,
		self::ON_LUNCH,
		self::ON_PROJECT,
		self::ON_MEETING,
		self::ON_TRAINING,
		self::ON_COACHING,
		self::IN_EMAIL_QUEUE,
		self::IN_LEASING_CHAT,
		self::LOGGED_OUT,
		self::HR_APPROVED_OFF
	];

	public static $c_arrstrCallAgentOnProjectSubStatusTypes = [
		self::ON_PROJECT_REASON_WORKING_ON_TASK_FORCE				=> 'Working on Task Force',
		self::ON_PROJECT_REASON_FOLLOWING_UP_WITH_MAINTENANCE_CALL	=> 'Following up with Maintenance Call',
		self::ON_PROJECT_REASON_SHADOWING							=> 'Shadowing',
		self::ON_PROJECT_REASON_OTHER								=> 'Other (Specify)'
	];

	public static $c_arrstrCallAgentStatusTypeIdToStr = [
		self::LOGGED_OUT 				=> 'Logged Out',
		self::AVAILABLE 				=> 'Available',
		self::AVAILABLE_ON_DEMAND 		=> 'Available (On Demand)',
		self::ON_BREAK 					=> 'On Break',
		self::UNUSED 					=> 'New',
		self::ON_MEETING 				=> 'On Meeting',
		self::ON_PROJECT 				=> 'On Project',
		self::ON_LUNCH 					=> 'On Lunch',
		self::ON_TRAINING 				=> 'On Training',
		self::ON_COACHING 				=> 'On Coaching',
		self::IN_EMAIL_QUEUE 			=> 'In Email Queue',
		self::IN_LEASING_CHAT 			=> 'In Leasing Chat',
		self::UNCOVERED_TO 				=> 'Uncovered To',
		self::COVERED_TO 				=> 'Covered To',
		self::CONDITIONAL_APPROVED_TO	=> 'Conditional Approved To',
		self::APPROVED_TO 				=> 'Approved To',
		self::UNREPORTED_TO 			=> 'Unreported To',
		self::HR_APPROVED_OFF			=> 'Hr Approved Off',
	];

	public static $c_arrintUnavailableCallAgentStatusIds = [
		CCallAgentStatusType::ON_BREAK,
		CCallAgentStatusType::ON_LUNCH,
		CCallAgentStatusType::ON_PROJECT,
		CCallAgentStatusType::ON_MEETING,
		CCallAgentStatusType::ON_TRAINING,
		CCallAgentStatusType::ON_COACHING,
		CCallAgentStatusType::IN_EMAIL_QUEUE,
		CCallAgentStatusType::IN_LEASING_CHAT,
		CCallAgentStatusType::HR_APPROVED_OFF
	];

	public static $c_arrstrCallAgentStatusTypes = [
		CCallAgentStatusType::AVAILABLE_ON_DEMAND	=> 'Available On Demand',
		CCallAgentStatusType::AVAILABLE				=> 'Available',
		CCallAgentStatusType::ON_BREAK				=> 'Break',
		CCallAgentStatusType::ON_LUNCH				=> 'Lunch',
		CCallAgentStatusType::ON_PROJECT			=> 'Project',
		CCallAgentStatusType::ON_MEETING			=> 'Meeting',
		CCallAgentStatusType::ON_TRAINING			=> 'Training',
		CCallAgentStatusType::ON_COACHING			=> 'Coaching',
		CCallAgentStatusType::IN_EMAIL_QUEUE		=> 'Email Queue',
		CCallAgentStatusType::IN_LEASING_CHAT		=> 'Leasing Chat',
		CCallAgentStatusType::HR_APPROVED_OFF		=> 'Hr Approved Off'
	];

	public static $c_arrstrCallAgentStatusTypeIdToAgentStatus = [
		self::LOGGED_OUT			=> CAgent::STATUS_LOGGED_OUT,
		self::AVAILABLE 			=> CAgent::STATUS_AVAILABLE,
		self::AVAILABLE_ON_DEMAND 	=> CAgent::STATUS_AVAILABLE_ON_DEMAND,
		self::ON_BREAK 				=> CAgent::STATUS_ON_BREAK,
		self::ON_LUNCH 				=> CAgent::STATUS_ON_LUNCH,
		self::ON_PROJECT 			=> CAgent::STATUS_ON_PROJECT,
		self::ON_MEETING 			=> CAgent::STATUS_ON_MEETING,
	 	self::ON_TRAINING 			=> CAgent::STATUS_ON_TRAINING,
		self::ON_COACHING 			=> CAgent::STATUS_ON_COACHING,
		self::IN_EMAIL_QUEUE 		=> CAgent::STATUS_IN_EMAIL_QUEUE,
		self::IN_LEASING_CHAT		=> CAgent::STATUS_IN_LEASING_CHAT,
		self::HR_APPROVED_OFF		=> CAgent::STATUS_HR_APPROVED_OFF
	];

	public static $c_arrintUnavailableChatStatusIds = [
		CCallAgentStatusType::LOGGED_OUT,
		CCallAgentStatusType::ON_BREAK,
		CCallAgentStatusType::ON_LUNCH,
		CCallAgentStatusType::ON_PROJECT,
		CCallAgentStatusType::ON_MEETING,
		CCallAgentStatusType::ON_TRAINING,
		CCallAgentStatusType::ON_COACHING,
		CCallAgentStatusType::IN_EMAIL_QUEUE,
		CCallAgentStatusType::HR_APPROVED_OFF
	];

	public static $c_arrintAdherenceCallAgentStatusTypeIds = [
		CCallAgentStatusType::AVAILABLE,
		CCallAgentStatusType::AVAILABLE_ON_DEMAND,
		CCallAgentStatusType::UNREPORTED_TO,
		CCallAgentStatusType::HR_APPROVED_OFF,
		CCallAgentStatusType::IN_LEASING_CHAT,
		CCallAgentStatusType::ON_MEETING
	];

	public static $c_arrmixCallAgentStatusTypeWfmCssDetails = [
		CCallAgentStatusType::AVAILABLE					=> [ 'td_class' => 'scheduled-available',				'icon_class' => 'call',										'status' => 'Available' ],
		CCallAgentStatusType::ON_MEETING				=> [ 'td_class' => 'scheduled-event',					'icon_class' => 'admin event-meeting',						'status' => 'Meeting' ],
		CCallAgentStatusType::ON_LUNCH					=> [ 'td_class' => 'scheduled-break',					'icon_class' => 'admin lunch',								'status' => 'Lunch' ],
		CCallAgentStatusType::ON_PROJECT				=> [ 'td_class' => 'scheduled-event',					'icon_class' => 'document',									'status' => 'Project' ],
		CCallAgentStatusType::ON_BREAK					=> [ 'td_class' => 'scheduled-break',					'icon_class' => 'officeopen',								'status' => 'Break' ],
		CCallAgentStatusType::ON_COACHING				=> [ 'td_class' => 'scheduled-coaching',				'icon_class' => 'school',									'status' => 'Coaching' ],
		CCallAgentStatusType::IN_LEASING_CHAT			=> [ 'td_class' => 'scheduled-chat',					'icon_class' => 'chat',										'status' => 'Chat' ],
		CCallAgentStatusType::IN_EMAIL_QUEUE			=> [ 'td_class' => 'scheduled-email',					'icon_class' => 'email',									'status' => 'Email' ],
		CCallAgentStatusType::ON_TRAINING				=> [ 'td_class' => 'scheduled-training',				'icon_class' => 'admin trainer',							'status' => 'Training' ],
		CCallAgentStatusType::APPROVED_TO				=> [ 'td_class' => 'scheduled-approve',					'icon_class' => 'approve',									'status' => 'Approved To' ],
		CCallAgentStatusType::COVERED_TO				=> [ 'td_class' => 'scheduled-user-on',					'icon_class' => 'user-on',									'status' => 'Covered To' ],
		CCallAgentStatusType::UNCOVERED_TO				=> [ 'td_class' => 'scheduled-user-off',				'icon_class' => 'user-off',									'status' => 'Uncovered To' ],
		CCallAgentStatusType::UNREPORTED_TO				=> [ 'td_class' => 'scheduled-alert-yellow',			'icon_class' => 'alert-yellow',								'status' => 'Unreported To' ],
		CCallAgentStatusType::CONDITIONAL_APPROVED_TO	=> [ 'td_class' => 'scheduled-check-circle-yellow',		'icon_class' => 'check-circle-yellow',						'status' => 'Conditional Approved To' ],
		CCallAgentStatusType::HR_APPROVED_OFF			=> [ 'td_class' => 'scheduled-medical-green-border',	'icon_class' => 'leasing-center-icon medical-green-border',	'status' => 'HR Approved Off' ]
	];

	public static function callAgentStatusTypeStrToId( $strCallAgentStatusType ) {
		switch( $strCallAgentStatusType ) {
			case 'Logged Out':
				$intCallAgentStatusTypeId = self::LOGGED_OUT;
				break;

			case 'Available':
				$intCallAgentStatusTypeId = self::AVAILABLE;
				break;

			case 'Available (On Demand)':
			case 'Available On Demand':
				$intCallAgentStatusTypeId = self::AVAILABLE_ON_DEMAND;
				break;

			case 'On Break':
				$intCallAgentStatusTypeId = self::ON_BREAK;
				break;

			case 'New':
				$intCallAgentStatusTypeId = self::UNUSED;
				break;

			case 'On Meeting':
				$intCallAgentStatusTypeId = self::ON_MEETING;
				break;

			case 'On Project':
				$intCallAgentStatusTypeId = self::ON_PROJECT;
				break;

			case 'On Lunch':
				$intCallAgentStatusTypeId = self::ON_LUNCH;
				break;

			case 'On Training':
				$intCallAgentStatusTypeId = self::ON_TRAINING;
				break;

			case 'On Coaching':
				$intCallAgentStatusTypeId = self::ON_COACHING;
				break;

			case 'In Email Queue':
				$intCallAgentStatusTypeId = self::IN_EMAIL_QUEUE;
				break;

			case 'In Leasing Chat':
				$intCallAgentStatusTypeId = self::IN_LEASING_CHAT;
				break;

			default:
				// default case
				$intCallAgentStatusTypeId = self::LOGGED_OUT;
				break;
		}

		return $intCallAgentStatusTypeId;
	}

}
?>