<?php

class CCallAgentTicker extends CBaseCallAgentTicker {

	const COMPANY		= 1;
	const TEAM			= 2;
	const INDIVIDUAL	= 3;

	public static $c_arrstrCallAgentTickerFilterTypes = [
		self::COMPANY		=> 'Company',
		self::TEAM			=> 'Team',
		self::INDIVIDUAL	=> 'Self'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentTickerTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDataReferenceId() {
		$boolIsValid = true;

		if( false == valId( $this->getDataReferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'data_reference_id', 'Data reference id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessage() {
		$boolIsValid = true;

		if( false == valStr( $this->getMessage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valDataReferenceId();
				$boolIsValid &= $this->valMessage();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other functions
	*/

	public function createCallAgentTickerResponse( $intCallAgentId = NULL, $intCallAgentTickerResponseKey = NULL ) {
		$objCallAgentTickerResponse = new CCallAgentTickerResponse();
		$objCallAgentTickerResponse->setCallAgentTickerId( $this->getId() );
		$objCallAgentTickerResponse->setCallAgentId( $intCallAgentId );

		if( true == valId( $intCallAgentTickerResponseKey ) ) {
			$objCallAgentTickerResponse->setResponse( 'Said, "' . CCallAgentTickerResponse::$c_arrstrCustomTexts[$intCallAgentTickerResponseKey] . '" for achieving ' . $this->getMessage() );
		}

		return $objCallAgentTickerResponse;
	}

}
?>