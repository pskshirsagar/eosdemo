<?php

class CCallTranscriptionClass extends CBaseCallTranscriptionClass {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApplicantApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallHour() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallDurationSeconds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallResult() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsAbandoned() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsLead() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsMarketSurvey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsResident() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSolicitor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsSupport() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsVendor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWorkOrder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallIsWrongNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadIsConverted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAgent() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaller() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>