<?php

class CScheduledCallLog extends CBaseScheduledCallLog {

	protected $m_strComapnyName;
	protected $m_strPropertyName;

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strComapnyName', $strCompanyName );
	}

	public function getCompanyName() {
		return $this->m_strComapnyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', $strPropertyName );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )						$this->setPropertyName( $arrmixValues['property_name'] );

	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>