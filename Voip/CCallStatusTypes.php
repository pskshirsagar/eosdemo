<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallStatusTypes
 * Do not add any new functions to this class.
 */

class CCallStatusTypes extends CBaseCallStatusTypes {

	public static function fetchCallStatusTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallStatusType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOrderedCallStatusTypes( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_status_types order by order_num';
		return self::fetchCallStatusTypes( $strSql, $objVoipDatabase );
	}
}
?>