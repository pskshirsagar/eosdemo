<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallLogs
 * Do not add any new functions to this class.
 */

class CCallLogs extends CBaseCallLogs {

	public static function fetchCompanyUserIdsFromCallLogsByCid( $intCid, $objVoipDatabase ) {
		$strSql  = 'SELECT
						DISTINCT created_by
					FROM
						call_logs
					WHERE
						cid = ' . ( int ) $intCid;

		$arrmixCompanyUserids = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrmixCompanyUserids ) ) {
			$arrmixCompanyUserids = array_keys( rekeyArray( 'created_by', $arrmixCompanyUserids ) );
		}

		return $arrmixCompanyUserids;
	}

	public static function fetchCallLogsByCallFilter( $objCallLogsFilter, $objVoipDatabase ) {

		$arrstrAndSearchParameters = self::fetchCallLogsSearchCriteria( $objCallLogsFilter );

		$strSql = 'SELECT
						*
					FROM
						call_logs AS cl
					WHERE '
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : ' 1 = 1 ' );

		$strSql .= ' ORDER BY created_on';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallLogsByCallLogsFilter( $objCallLogsFilter, $objVoipDatabase, $boolIsCountOnly = NULL ) {

		$arrstrAndSearchParameters = self::fetchCallLogsSearchCriteria( $objCallLogsFilter );

		if( false == is_null( $objCallLogsFilter->getPageNo() ) && false == is_null( $objCallLogsFilter->getCountPerPage() ) ) {
			$intOffset 	= ( 0 < $objCallLogsFilter->getPageNo() ) ? $objCallLogsFilter->getCountPerPage() * ( $objCallLogsFilter->getPageNo() - 1 ) : 0;
			$intLimit 	= ( int ) $objCallLogsFilter->getCountPerPage();
		}

		if( true == $boolIsCountOnly ) {
			$strSql 	= ' SELECT count( cl.id ) ';
		} else {
			$strSql 	= ' SELECT * ';
			$strOrderBy = ' ORDER BY cl.id DESC ';
		}

		$strSql .= ' FROM
						call_logs AS cl';

		$strSql .= ' WHERE '
						. ( ( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : ' 1 = 1 ' );

		if( false == $boolIsCountOnly ) {
			$strSql .= $strOrderBy;
		}

		if( false == $boolIsCountOnly && false == is_null( $objCallLogsFilter->getPageNo() ) && false == is_null( $objCallLogsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		} else {
			return parent::fetchCallLogs( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallLogsSearchCriteria( $objCallLogsFilter ) {
		$arrstrAndSearchParameters = array();
		$arrstrTableNames = array(
			'call_queues',
			'call_agent_queues',
			'calls',
			'call_agents',
			'call_phone_numbers'
		);

		if( true == valObj( $objCallLogsFilter, 'CCallFilter' ) ) {

			if( true == valStr( $objCallLogsFilter->getTableName() ) ) {
				$arrstrAndSearchParameters[] = 'cl.table_name = \'' . str_replace( ',', '\', \'', $objCallLogsFilter->getTableName() ) . '\' ';
			} else {
				$arrstrAndSearchParameters[] = 'cl.table_name IN ( \'' . str_replace( ',', '\', \'', implode( ',', $arrstrTableNames ) ) . '\' ) ';
			}

			if( false == is_null( $objCallLogsFilter->getStartDate() ) && 0 < strlen( trim( $objCallLogsFilter->getStartDate() ) ) ) {
				$arrstrAndSearchParameters[] = 'cl.created_on >= \'' . date( 'Y-m-d', strtotime( $objCallLogsFilter->getStartDate() ) ) . ' 00:00:01\'';
			}

			if( false == is_null( $objCallLogsFilter->getEndDate() ) && 0 < strlen( trim( $objCallLogsFilter->getEndDate() ) ) ) {
				$arrstrAndSearchParameters[] = 'cl.created_on <= \'' . date( 'Y-m-d', strtotime( $objCallLogsFilter->getEndDate() ) ) . ' 23:59:59\'';
			}

			if( false == is_null( $objCallLogsFilter->getCreatedBy() ) && true == is_numeric( $objCallLogsFilter->getCreatedBy() ) ) {
				$arrstrAndSearchParameters[] = 'cl.created_by = ' . $objCallLogsFilter->getCreatedBy();
			}

			if( false == is_null( $objCallLogsFilter->getCallQueueId() ) && true == is_numeric( $objCallLogsFilter->getCallQueueId() ) ) {
				$arrstrAndSearchParameters[] = 'cl.reference_id = ' . $objCallLogsFilter->getCallQueueId();
			} elseif( false == is_null( $objCallLogsFilter->getCallAgentId() ) && true == is_numeric( $objCallLogsFilter->getCallAgentId() ) ) {
				$arrstrAndSearchParameters[] = 'cl.reference_id = ' . $objCallLogsFilter->getCallAgentId();
			} elseif( false == is_null( $objCallLogsFilter->getCallId() ) && true == is_numeric( $objCallLogsFilter->getCallId() ) ) {
				$arrstrAndSearchParameters[] = 'cl.reference_id = ' . $objCallLogsFilter->getCallId();
			} elseif( false == is_null( $objCallLogsFilter->getReferenceId() ) && true == is_numeric( $objCallLogsFilter->getReferenceId() ) ) {
				$arrstrAndSearchParameters[] = 'cl.reference_id = ' . $objCallLogsFilter->getReferenceId();
			}

			if( false == is_null( $objCallLogsFilter->getPhoneNumber() ) && true == valStr( $objCallLogsFilter->getPhoneNumber() ) ) {
				$arrstrAndSearchParameters[] = 'cl.reference_id IN ( SELECT id FROM call_phone_numbers WHERE phone_number = \'' . $objCallLogsFilter->getPhoneNumber() . '\' )';
			}

			if( false == is_null( $objCallLogsFilter->getAction() ) && true == valStr( $objCallLogsFilter->getAction() ) ) {
				$arrstrAndSearchParameters[] = 'cl.action LIKE  \'%' . $objCallLogsFilter->getAction() . '%\'';
			} else {
				$arrstrAndSearchParameters[] = 'cl.action != \'TWILIO_INSERT\'';
			}

		}

		return $arrstrAndSearchParameters;
	}

	public static function fetchUserIdsFromCallLogsByTableNames( $arrstrTableNames, $objVoipDatabase ) {
		if( false == valArr( $arrstrTableNames ) ) return;

		$strSql  = 'SELECT
						DISTINCT created_by
					FROM
						call_logs
					WHERE
						table_name IN ( \'' . str_replace( ',', '\', \'', implode( ',', $arrstrTableNames ) ) . '\' ) ';

		$arrmixUserids = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrmixUserids ) ) {
			$arrmixUserids = array_keys( rekeyArray( 'created_by', $arrmixUserids ) );
		}

		return $arrmixUserids;
	}

	public static function fetchReferenceIdsFromCallLogsByRefernceName( $strReferenceName, $objVoipDatabase ) {

		$strSql  = 'SELECT
						DISTINCT reference_id
					FROM
						call_logs
					WHERE
						reference_name= \'' . $strReferenceName . '\'
						AND reference_id IS NOT NULL';

		$arrmixReferenceIds = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrmixReferenceIds ) ) {
			$arrmixReferenceIds = array_keys( rekeyArray( 'reference_id', $arrmixReferenceIds ) );
		}

		return $arrmixReferenceIds;
	}

	public static function fetchCallLogByTableNameByReferenceIdByAction( $strTableName, $intCallId, $strAction, $objVoipDatabase ) {

		$strSql  = 'SELECT
						*
					FROM
						call_logs
					WHERE
						table_name = \'' . $strTableName . '\'
						AND action = \'' . $strAction . '\'
						AND reference_id = ' . ( int ) $intCallId . '
					ORDER BY id DESC
					LIMIT 1';

		return parent::fetchCallLog( $strSql, $objVoipDatabase );
	}

	public static function fetchCallLogsByReferenceIdsByAction( $arrintReferenceIds, $strAction, $objVoipDatabase ) {

		if( false == valArr( $arrintReferenceIds ) ) return NULL;

		$strSql  = 'SELECT
						*
					FROM
						call_logs
					WHERE
						action = \'' . $strAction . '\'
						AND reference_id IN ( ' . implode( ',', $arrintReferenceIds ) . ' ) ';

		return parent::fetchCallLogs( $strSql, $objVoipDatabase );
	}

	public static function fetchCallLogsByReferenceId( $intReferenceId, $objVoipDatabase ) {

		if( false == valId( $intReferenceId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_logs
					WHERE
						reference_id = ' . ( int ) $intReferenceId . '
					ORDER BY id DESC';

		return parent::fetchCallLogs( $strSql, $objVoipDatabase );
	}

}
?>
