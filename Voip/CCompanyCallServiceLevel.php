<?php

class CCompanyCallServiceLevel extends CBaseCompanyCallServiceLevel {

	protected $m_strCompanyName;
	protected $m_strCallAgentName;

	/**
	 * set functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['call_agent_name'] ) )						$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		return true;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	/**
	 * get functions
	 *
	 */

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTargetedServiceLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentServiceLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedBaseScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOverrideServiceLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>