<?php

class CCallPhoneNumber extends CBaseCallPhoneNumber {

	protected $m_intAreaCode;
	protected $m_strStateCode;
	protected $m_intQueuePhoneExtensionId;
	protected $m_intVerifiedStatus;

	protected $m_strCallType;
	protected $m_strCallQueueName;
	protected $m_strCallQueueDomain;
	protected $m_strVerificationNote;
	protected $m_strIvrName;
	protected $m_strCallServiceProviderName;
	protected $m_strPropertyName;
	protected $m_strCompanyName;

	protected $m_objCallLog;

	protected $m_boolIsDeletedPending;
	protected $m_boolIsOverrideAfterOfficeHours;

	const MONTHLY_FEE				= 5;
	const RESERVED_DAYS				= 90;
	const LOWER_AREA_CODE_LIMIT		= 200;
	const OUTBOUND_PREFIX			= '1';
	const MAIN_SUPPORT_LINE			= '8013755522';
	const TEST_PHONE_NUMBER			= '8018238321';
	const PROTECTED_PHONE_NUMBER	= '0000000000';
	const LEASING_CENTER_LINE       = '8889106862';
	const AFTER_OFFICE_HOURS		= 'After Office Hours';
	const DURING_OFFICE_HOURS		= 'During Office Hours';

	const CREATE_SMS_VANITY_NUMBER	= 'create';
	const RELEASE_SMS_VANITY_NUMBER	= 'release';
	const SUSPEND_SMS_VANITY_NUMBER	= 'suspend';
	const RESUME_SMS_VANITY_NUMBER	= 'resume';

	const CALL_PHONE_NUMBER			= 'Vanity Text Number';

	/**
	* Get Functions
	*
	*/

	public function getIsDeletedPending() {
		return $this->m_boolIsDeletedPending;
	}

	public function getCallType() {
		return $this->m_strCallType;
	}

	public function getAreaCode() {
		return $this->m_intAreaCode;
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function getVerifiedStatus() {
		return $this->m_intVerifiedStatus;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallQueueDomain() {
		return $this->m_strCallQueueDomain;
	}

	public function getOutboundCallPhoneNumber() {
		return self::OUTBOUND_PREFIX . $this->getPhoneNumber();
	}

	public function getVerificationNote() {
		return $this->m_strVerificationNote;
	}

	public function getQueuePhoneExtensionId() {
		return $this->m_intQueuePhoneExtensionId;
	}

	public function getIvrName() {
		return $this->m_strIvrName;
	}

	public function getCallServiceProviderName() {
		return $this->m_strCallServiceProviderName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getIsOverrideAfterOfficeHours() {
		return $this->m_boolIsOverrideAfterOfficeHours;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getOrCreateCallLog() {
		if( false == valObj( $this->m_objCallLog, 'CCallLog' ) ) {
			$this->setCallLog( new CCallLog() );
		}
		return $this->m_objCallLog;
	}

	/**
	* Set Functions
	*
	*/

	public function setIsDeletedPending( $boolIsDeletedPending ) {
		$this->m_boolIsDeletedPending = $boolIsDeletedPending;
	}

	public function setCallType( $strCallType ) {
		$this->m_strCallType = $strCallType;
	}

	public function setAreaCode( $intAreaCode ) {
		$this->m_intAreaCode = $intAreaCode;
	}

	public function setStateCode( $strStateCode ) {
		$this->m_strStateCode = $strStateCode;
	}

	public function setVerifiedStatus( $intVerifiedStatus ) {
		$this->m_intVerifiedStatus = $intVerifiedStatus;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallQueueDomain( $strCallQueueDomain ) {
		$this->m_strCallQueueDomain = $strCallQueueDomain;
	}

	public function setVerificationNote( $strVerificationNote ) {
		$this->m_strVerificationNote = $strVerificationNote;
	}

	public function setQueuePhoneExtensionId( $intQueuePhoneExtensionId ) {
		$this->m_intQueuePhoneExtensionId = $intQueuePhoneExtensionId;
	}

	public function setIvrName( $strIvrName ) {
		$this->m_strIvrName = $strIvrName;
	}

	public function setCallServiceProviderName( $strCallServiceProviderName ) {
		$this->m_strCallServiceProviderName = $strCallServiceProviderName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setIsOverrideAfterOfficeHours( $boolIsOverrideAfterOfficeHours ) {
		$this->m_boolIsOverrideAfterOfficeHours = CStrings::strToBool( $boolIsOverrideAfterOfficeHours );
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setCallLog( $objCallLog ) {
		$this->m_objCallLog = $objCallLog;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['queue_phone_extension_id'] ) )			$this->setQueuePhoneExtensionId( $arrmixValues['queue_phone_extension_id'] );
		if( true == isset( $arrmixValues['is_deleted_pending'] ) )					$this->setIsDeletedPending( $arrmixValues['is_deleted_pending'] );
		if( true == isset( $arrmixValues['call_type'] ) )							$this->setCallType( $arrmixValues['call_type'] );
		if( true == isset( $arrmixValues['area_code'] ) )							$this->setAreaCode( $arrmixValues['area_code'] );
		if( true == isset( $arrmixValues['state_code'] ) )							$this->setStateCode( $arrmixValues['state_code'] );
		if( true == isset( $arrmixValues['call_queue_name'] ) )						$this->setCallQueueName( $arrmixValues['call_queue_name'] );
		if( true == isset( $arrmixValues['call_queue_domain'] ) )					$this->setCallQueueDomain( $arrmixValues['call_queue_domain'] );
		if( true == isset( $arrmixValues['ivr_name'] ) )							$this->setIvrName( $arrmixValues['ivr_name'] );
		if( true == isset( $arrmixValues['call_service_provider_name'] ) )			$this->setCallServiceProviderName( $arrmixValues['call_service_provider_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )						$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['is_override_after_office_hours'] ) )		$this->setIsOverrideAfterOfficeHours( $arrmixValues['is_override_after_office_hours'] );
		return;
	}

	/**
	* Validate Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', __( 'Please select call phone number.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Select client.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Select property.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber( $boolIsRequired = false, $strSectionName = NULL ) {
		$boolIsValid = true;
		$strSectionName	= ( true == valStr( $strSectionName ) ) ? $strSectionName : 'Phone';

		if( true == $boolIsRequired ) {
			if( false == valStr( $this->m_strPhoneNumber ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( '{%s, section_name}  number is required.', [ 'section_name' => $strSectionName ] ) ) );
			}
		}

		if( false != valStr( $this->m_strPhoneNumber ) && ( 10 !== \Psi\CStringService::singleton()->strlen( $this->m_strPhoneNumber ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( '{%s, section_name} number must be {%d, total_digits} digits.', [ 'section_name' => $strSectionName, 'total_digits' => 10 ] ), 504 ) );
			$boolIsValid = false;
		} elseif( true == isset( $this->m_strPhoneNumber ) && false == ( CValidation::validateFullPhoneNumber( $this->m_strPhoneNumber, false ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Valid {%s, section_name} number is required.', [ 'section_name' => $strSectionName ] ), 504 ) );
			$boolIsValid = false;
		}

		if( self::LOWER_AREA_CODE_LIMIT >= \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, 0, 3 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Valid phone number is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCheckCallPhoneNumber( $objVoipDatabase ) {
		$boolIsValid = true;

		$strWhere = ' WHERE
						phone_number = \'' . addslashes( $this->m_strPhoneNumber ) . '\'
						AND released_on IS NULL';

		$intCallPhoneNumberCount = CCallPhoneNumbers::fetchCallPhoneNumberCount( $strWhere, $objVoipDatabase );

		if( 0 < $intCallPhoneNumberCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '',  __( ' Sorry {%s, phone_number} number already associated with property.', [ 'phone_number' => $this->m_strPhoneNumber ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valIsTollFree() {
		$boolIsValid = true;

		if( false == $this->m_boolIsTollFree && true == in_array( \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, 0, 3 ), CAreaCode::$c_arrintTollFreeAreaCodes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'toll_free', __( ' {%s, phone_number }  is toll free phone number, please select correct toll free option.', [ 'phone_number' => $this->m_strPhoneNumber ] ) ) );
		} elseif( true == $this->m_boolIsTollFree && false === in_array( \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, 0, 3 ), CAreaCode::$c_arrintTollFreeAreaCodes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'toll_free', __( ' {%s, phone_number }  is non toll free phone number, please select correct toll free option.', [ 'phone_number' => $this->m_strPhoneNumber ] ) ) );
		}

		return $boolIsValid;
	}

	public function valAreaCode() {
		$boolIsValid = true;

		if( 0 == $this->getIsTollFree() && ( 100 >= $this->getAreaCode() || 999 <= $this->getAreaCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'area_code', __( 'In order to request numbers, you must enter a valid three-digit area code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCallTypeId() {
		$boolIsValid = true;

		if( 0 == $this->getCallTypeId() || true == is_null( $this->getCallTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Select phone number type.' ) ) );
		}

		return $boolIsValid;
	}

	public function valStateCode() {
		$boolIsValid = true;

		if( false == valStr( $this->getStateCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Select state code.' ) ) );
		}

		return $boolIsValid;
	}

	public function valCallQueueId() {
		$boolIsValid = true;

		if( 0 == $this->getCallQueueId() || true == is_null( $this->getCallQueueId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Call Queue is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valVanityNumber( $objVoipDatabase ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCallTypeId();

		if( true == in_array( $this->getCallTypeId(), CCallTypeHelper::getCallTrackerCallTypes() ) ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
		} elseif( CCallType::RESIDENT_SUPPORT == $this->getCallTypeId() ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->checkResidentSupportNumber( $objVoipDatabase );
		} elseif( CCallType::ENTRATA != $this->getCallTypeId() && CCallType::PSI_OFFICE != $this->getCallTypeId() && true == array_key_exists( $this->getCallTypeId(), CCallType::$c_arrstrCallPhoneNumberCallTypes ) ) {
			$boolIsValid &= $this->valCallQueueId();
		} elseif( CCallType::ENTRATA == $this->getCallTypeId() ) {
			$boolIsValid &= $this->valPhoneExtensionId();
		}

		return $boolIsValid;
	}

	public function checkResidentSupportNumber( $objVoipDatabase ) {
		$boolIsValid = true;

		$strWhere = ' WHERE
						call_type_id = ' . ( int ) $this->getCallTypeId() . '
						AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND released_on IS NULL';

		$intCallPhoneNumberCount = CCallPhoneNumbers::fetchCallPhoneNumberCount( $strWhere, $objVoipDatabase );

		if( 0 < $intCallPhoneNumberCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'This property already has a Resident Support number assigned.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPortNumber( $objVoipDatabase ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valCallTypeId();
		$boolIsValid &= $this->valCheckCallPhoneNumber( $objVoipDatabase );
		$boolIsValid &= $this->valPhoneNumber( true );
		$boolIsValid &= $this->valIsTollFree();

		if( true == in_array( $this->getCallTypeId(), CCallTypeHelper::getCallTrackerCallTypes() ) ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
		} elseif( CCallType::RESIDENT_SUPPORT == $this->getCallTypeId() ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
			$boolIsValid &= $this->checkResidentSupportNumber( $objVoipDatabase );
		} elseif( true == array_key_exists( $this->getCallTypeId(), CCallType::$c_arrstrCallPhoneNumberCallTypes ) ) {
			if( CCallType::ENTRATA != $this->getCallTypeId() ) {
				$boolIsValid &= $this->valCallQueueId();
			} else {
				$boolIsValid &= $this->valPhoneExtensionId();
			}
		}

		return $boolIsValid;
	}

	public function getFormattedPhoneNumber() {
		$strTempPropertyPhoneNumber = '';

		$strPropertyPhoneNumber = preg_replace( '/\D/', '', $this->getPhoneNumber() );

		if( true == valStr( $strPropertyPhoneNumber ) ) {
			$strTempPropertyPhoneNumber = \Psi\CStringService::singleton()->substr( $strPropertyPhoneNumber, 0, 3 );
			$strTempPropertyPhoneNumber .= '-';
			$strTempPropertyPhoneNumber .= \Psi\CStringService::singleton()->substr( $strPropertyPhoneNumber, 3, 3 );
			$strTempPropertyPhoneNumber .= '-';
			$strTempPropertyPhoneNumber .= \Psi\CStringService::singleton()->substr( $strPropertyPhoneNumber, 6, ( \Psi\CStringService::singleton()->strlen( $strPropertyPhoneNumber ) - 6 ) );
		}

		return $strTempPropertyPhoneNumber;
	}

	public function valUpdateCallPhoneNumber() {
		$boolIsValid = true;

		$boolIsValid &= $this->valCallTypeId();
		$boolIsValid &= $this->valPhoneNumber( true );

		if( true == in_array( $this->getCallTypeId(), CCallTypeHelper::getCallTrackerCallTypes() ) ) {
			$boolIsValid &= $this->valCid();
			$boolIsValid &= $this->valPropertyId();
		} elseif( CCallType::ENTRATA != $this->getCallTypeId() && CCallType::PSI_OFFICE != $this->getCallTypeId() && true == array_key_exists( $this->getCallTypeId(), CCallType::$c_arrstrCallPhoneNumberCallTypes ) ) {
			$boolIsValid &= $this->valCallQueueId();
		}

		if( CCallType::PSI_OFFICE == $this->getCallTypeId() ) {
			$boolIsValid &= $this->valEmployeeId();
		}

		if( CCallType::ENTRATA == $this->getCallTypeId() ) {
			$boolIsValid &= $this->valPhoneExtensionId();
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == valId( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Employee is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPhoneExtensionId() {
		$boolIsValid = true;
		if( false == valId( $this->getPhoneExtensionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( VALIDATE_INSERT, 'phone_extension_id', __( 'Phone Extension is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valCallForwardPreference() {
		$boolIsValid = true;

		if( false == valId( $this->getCallForwardPreferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Call Forward Preference is required.' ) ) );
		}

		if( CCallForwardPreference::IVR == $this->getCallForwardPreferenceId() && false == valId( $this->getIvrId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Call Ivr is required.' ) ) );
		} elseif( CCallForwardPreference::SPECIFIC_NUMBER == $this->getCallForwardPreferenceId() && false == valStr( $this->getCallForwardPhoneNumber() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Call Forward Number is required.' ) ) );
		}

		if( $this->getCallTypeId() == CCallType::CALL_TRACKING_OFFICE && CCallForwardPreference::OFFICE_CONTACTS != $this->getCallForwardPreferenceId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Selected call forward preference is not allowed for call tracking office' ) ) );
		}

		return $boolIsValid;
	}

	public function valCallForwardCallPhoneNumber( $objVoipDatabase ) {
		$boolIsValid = true;

		if( CCallForwardPreference::SPECIFIC_NUMBER !== $this->getCallForwardPreferenceId() ) {
			return $boolIsValid;
		}

		$strWhere = ' WHERE
						phone_number = \'' . addslashes( $this->m_strCallForwardPhoneNumber ) . '\'';

		$intCallPhoneNumberCount = CCallPhoneNumbers::fetchCallPhoneNumberCount( $strWhere, $objVoipDatabase );

		if( 0 < $intCallPhoneNumberCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Sorry {%s, phone_number} number is vanity number.', [ 'phone_number' => $this->m_strCallForwardPhoneNumber ] ) ) );
			$boolIsValid = false;
		}

		if( 10 !== \Psi\CStringService::singleton()->strlen( $this->m_strCallForwardPhoneNumber ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_forward_phone_number', __( 'Call Forward Phone Number must be {%d, total_digits} digits.', [ 'total_digits' => 10 ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPhoneNumber( true );
				$boolIsValid &= $this->valCheckCallPhoneNumber( $objVoipDatabase );

				if( CCallType::PSI_OFFICE == $this->getCallTypeId() ) {
					$boolIsValid &= $this->valEmployeeId();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valId();
				if( CCallType::SMS_ONLY != $this->getCallTypeId() ) {
					$boolIsValid &= $this->valCallForwardPreference();
					$boolIsValid &= $this->valCallForwardCallPhoneNumber( $objVoipDatabase );
				}
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valId();
				break;

			case 'call_phone_number_request':
				$boolIsValid &= $this->valAreaCode();
				$boolIsValid &= $this->valCallTypeId();
				$boolIsValid &= $this->valCallForwardPreference();
				$boolIsValid &= $this->valCallForwardCallPhoneNumber( $objVoipDatabase );

				if( CCallType::RESIDENT_SUPPORT == $this->getCallTypeId() ) {
					$boolIsValid &= $this->checkResidentSupportNumber( $objVoipDatabase );
				}
				break;

			case 'validate_vanity_phone_number_request':
				$boolIsValid &= $this->valAreaCode();
				break;

			case 'port_call_phone_number_request':
				$boolIsValid &= $this->valPortNumber( $objVoipDatabase );
				break;

			case 'update_call_phone_number_request':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				break;

			case 'validate_vanity_number':
				$boolIsValid &= $this->valStateCode();
				$boolIsValid &= $this->valVanityNumber( $objVoipDatabase );

				if( CCallType::PSI_OFFICE == $this->getCallTypeId() ) {
					$boolIsValid &= $this->valEmployeeId();
				}
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Custom Fetch Function
	 */

	public function fetchCallQueue( $objVoipDatabase ) {
		return CCallQueues::fetchCallQueueById( $this->getCallQueueId(), $objVoipDatabase );
	}

	public function fetchCallAgent( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgentByEmployeeId( $this->getEmployeeId(), $objVoipDatabase );
	}

	public function fetchPhysicalPhoneExtension( $objVoipDatabase ) {
		return CPhoneExtensions::fetchPhysicalPhoneExtensionByPhoneExtensionId( $this->getPhoneExtensionId(), $objVoipDatabase );
	}

	public function fetchSimpleGreetingFilePathByGreetingTypeId( $intGreetingTypeId, $objVoipDatabase ) {
		$objCallFile = CCallFiles::fetchCallFileByPropertyIdByGreetingTypeId( $this->getPropertyId(), $intGreetingTypeId, $objVoipDatabase );
		if( true == valObj( $objCallFile, 'CCallFile' ) )  return  $objCallFile->getFullFilePath();
		return CCallHelper::getGreetingTypeIdToStandardGreetingFilePath( $intGreetingTypeId );
	}

	public function fetchIvr( $objVoipDatabase ) {
		return CIvrs::fetchIvrByIdByCidByPropertyId( $this->getIvrId(), $this->getCid(), $this->getPropertyId(), $objVoipDatabase );
	}

	public function fetchPropertyLeadSources( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CPropertyLeadSources::createService()->fetchPropertyLeadSourcesByPropertyIdByCallPhoneNumberIdByCid( $this->getPropertyId(), $this->getId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchPhoneExtension( $objVoipDatabase ) {
		return CPhoneExtensions::fetchPhoneExtension( 'SELECT * FROM phone_extensions WHERE id = ' . ( int ) $this->getPhoneExtensionId(), $objVoipDatabase );
	}

	public function fetchPropertyCallSetting( $objVoipDatabase ) {
		return CPropertyCallSettings::fetchPropertyCallSettingByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objVoipDatabase );
	}

	/**
	 * Create Functions.
	 */

	public function createCall( $strOriginPhoneNumber, $objVoipDatabase ) {
		$objCall = new CCall();

		$objCall->setDefaults();
		$objCall->setCid( $this->getCid() );
		$objCall->setCallPhoneNumberId( $this->getId() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setCallTypeId( ( true == valId( $this->getIvrId() ) ? $this->getIvrCallTypeId() : $this->getCallTypeId() ) );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setCallResultId( CCallResult::UNKNOWN );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setDestinationPhoneNumber( $this->getPhoneNumber() );
		$objCall->setOriginPhoneNumber( $strOriginPhoneNumber );
		$objCall->setSessionUuid( $this->getRequestData( [ 'variable_uuid' ] ) );

		if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call record.', E_USER_WARNING );
			return NULL;
		}

		return $objCall;
	}

	public function updateCallLog( $strAction, $intUserId, $objVoipDatabase ) {
		$this->m_objCallLog = $this->getOrCreateCallLog();

		if( $this->m_objCallLog->getCurrentData() != $this->m_objCallLog->getPreviousData() ) {
			$this->m_objCallLog->updateCallLog( $this->m_objCallLog->getCurrentData(),
			$this->m_objCallLog->getPreviousData(),
			'call_phone_number_id',
			$this->getId(),
			'call_phone_numbers',
			$strAction,
			$intUserId,
			$objVoipDatabase,
			$this->getCid(),
			$this->getPropertyId() );
		}
	}

	/**
	 * Process Functions
	 */

	public function processCallLogPreviousData() {
		$this->getOrCreateCallLog();

		$strPreviousData	= '';
		$strPreviousData	.= 'Call Queue Id::' . $this->getCallQueueId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Call Type Id::' . $this->getCallTypeId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Client Id::' . $this->getCid() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Property Id::' . $this->getPropertyId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Employee Id::' . $this->getEmployeeId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Dialplan Id::' . $this->getDialplanId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Phone Extension Id::' . $this->getPhoneExtensionId() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Phone Number::' . $this->getPhoneNumber() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Suspended On::' . $this->getSuspendedOn() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Suspended By::' . $this->getSuspendedBy() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Is SIP Test::' . $this->getIsSipTest() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Is Toll Free::' . $this->getIsTollFree() . CCallLog::DATA_SEPERATOR;
		$strPreviousData	.= 'Created On::' . ( 'NOW()' == $this->getCreatedOn() ? date( 'Y-m-d h:m:s' ) : $this->getCreatedOn() ) . CCallLog::DATA_SEPERATOR;

		$this->m_objCallLog->setPreviousData( $strPreviousData );
	}

	public function processCallLogCurrentData() {
		$this->getOrCreateCallLog();

		$strCurrentData		= 'Call Queue Id::' . $this->getCallQueueId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Call Type Id::' . $this->getCallTypeId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Client Id::' . $this->getCid() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Property Id::' . $this->getPropertyId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Employee Id::' . $this->getEmployeeId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Dialplan Id::' . $this->getDialplanId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Phone Extension Id::' . $this->getPhoneExtensionId() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Phone Number::' . $this->getPhoneNumber() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Suspended On::' . ( 'NOW()' == $this->getSuspendedOn() ? date( 'Y-m-d h:m:s' ) : $this->getSuspendedOn() ) . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Suspended By::' . $this->getSuspendedBy() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Is SIP Test::' . $this->getIsSipTest() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Is Toll Free::' . $this->getIsTollFree() . CCallLog::DATA_SEPERATOR;
		$strCurrentData		.= 'Created On::' . ( 'NOW()' == $this->getCreatedOn() ? date( 'Y-m-d h:m:s' ) : $this->getCreatedOn() ) . CCallLog::DATA_SEPERATOR;

		$this->m_objCallLog->setCurrentData( $strCurrentData );
	}

	public function processMessageOriginator( $strAction, $intCurrentUserId ) {
		$boolIsValid					= false;
		$objMessageOriginatorLibrary	= new CMessageOriginatorLibrary();
		$strPhoneNumber					= $this->getE164PhoneNumber();

		switch( $strAction ) {
			case self::CREATE_SMS_VANITY_NUMBER:
				$boolIsValid = $objMessageOriginatorLibrary->createOriginator( $this->getCid(), $this->getPropertyId(), $strPhoneNumber, $this->getId(), self::CALL_PHONE_NUMBER, $this->getIsOutboundDefault(), $intCurrentUserId );
				break;

			case self::SUSPEND_SMS_VANITY_NUMBER:
				if( false == $objMessageOriginatorLibrary->isPhoneNumberAvailable( $strPhoneNumber, $this->getId(), $this->getPropertyId(), $this->getCid() ) ) {
					return true;
				}

				$boolIsValid = $objMessageOriginatorLibrary->suspendOrEnableOriginator( $strPhoneNumber, $this->getPropertyId(), $this->getCid(), $this->getId(), true, $intCurrentUserId, $this->getIsOutboundDefault() );
				break;

			case self::RELEASE_SMS_VANITY_NUMBER:
				if( false == $objMessageOriginatorLibrary->isPhoneNumberAvailable( $strPhoneNumber, $this->getId(), $this->getPropertyId(), $this->getCid() ) ) {
					return true;
				}

				$boolIsValid = $objMessageOriginatorLibrary->releaseOriginator( $strPhoneNumber, $this->getPropertyId(), $this->getCid(), $this->getId(), $intCurrentUserId );
				break;

			case self::RESUME_SMS_VANITY_NUMBER:
				if( false == $objMessageOriginatorLibrary->isPhoneNumberAvailable( $strPhoneNumber, $this->getId(), $this->getPropertyId(), $this->getCid() ) ) {
					$this->processMessageOriginator( self::CREATE_SMS_VANITY_NUMBER, $intCurrentUserId );
					return true;
				}

				$boolIsValid = $objMessageOriginatorLibrary->suspendOrEnableOriginator( $strPhoneNumber, $this->getPropertyId(), $this->getCid(), $this->getId(), false, $intCurrentUserId, $this->getIsOutboundDefault() );
				break;

			default:
				// default case
				break;
		}

		if( false == $boolIsValid ) {
			foreach( ( array ) $objMessageOriginatorLibrary->getErrorMessages() AS $strErrorMsg ) {
				$this->addErrorMsgs( new CErrorMsg( ERROR_TYPE_GENERAL, NULL, $strErrorMsg ) );
			}
		}

		return $boolIsValid;

	}

	public function isSmsVerified() {
		$arrmixReturnData = [ 'status' => 'completed' ];
		if( true == $this->getIsSmsEnabled() ) {
			$objMessageOriginatorLibrary = new CMessageOriginatorLibrary();
			if( true == $objMessageOriginatorLibrary->isPhoneNumberPublished( $this->getE164PhoneNumber(), $this->m_intId, $this->m_intPropertyId, $this->m_intCid ) ) {
				$arrmixReturnData = [ 'status' => 'completed' ];
			} else {
				$objSmsDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::SMS, CDatabaseUserType::PS_PROPERTYMANAGER );
				$objMessageOriginator = \Psi\Eos\Sms\CMessageOriginators::createService()->fetchAvailableMessageOriginatorByNumberByCallPhoneNumberIdByPropertyIdByCid( $this->getE164PhoneNumber(), $this->m_intId, $this->m_intPropertyId, $this->m_intCid, $objSmsDatabase );
				if( true == valObj( $objMessageOriginator, 'CMessageOriginator' ) ) {
					$arrmixReturnData = [ 'status' => $objMessageOriginator->getHostedNumberStatus(), 'failure_reason' => $objMessageOriginator->getHostedNumberFailureReason() ];
				} else {
					$arrmixReturnData = [ 'status' => 'other-failed' ];
				}
				$objSmsDatabase->close();
			}
		}
		return json_encode( $arrmixReturnData );
	}

	public function calcSuspendedDays() {
		$arrintSuspendedOnDate	= explode( ' ', $this->getSuspendedOn() );
		$arrintSuspendedOnDate	= explode( '/', $arrintSuspendedOnDate[0] );
		$intSuspendedOnDate		= $arrintSuspendedOnDate[2] . '-' . $arrintSuspendedOnDate[0] . '-' . $arrintSuspendedOnDate[1];
		return floor( ( strtotime( date( 'Y-m-d' ) ) - strtotime( $intSuspendedOnDate ) ) / ( 86400 ) );
	}

	public function removeVanityPhoneNumber() {

		if( CCallServiceProvider::TWILIO_SERVICES == $this->getCallServiceProviderId() ) {
			return true;
		}

		if( CCallServiceProvider::VITELITY == $this->getCallServiceProviderId() ) {
			$objCallServiceProvider = new CVitelity();
		} else {
			$objCallServiceProvider = new CVeracity();
		}

		$objCallServiceProvider->setPhoneNumber( $this->getPhoneNumber() );
		return $objCallServiceProvider->postUnassignInboundNumber();
	}

	public function getAfterOfficeHoursIvr() {
		return ( true == $this->getIsOverrideAfterOfficeHours() ) ? $this->getIvrName() . ' - ' . self::DURING_OFFICE_HOURS : $this->getIvrName() . ' - ' . self::AFTER_OFFICE_HOURS;
	}

	public function getDuringOfficeHoursIvr() {
		return $this->getIvrName() . ' - ' . self::DURING_OFFICE_HOURS;
	}

	public function getRequestData( $arrstrRequestKeys ) {
		$arrmixValue = $_REQUEST;

		if( true == valArr( $arrstrRequestKeys ) ) {
			foreach( $arrstrRequestKeys as $strRequestKey ) {
				if( true == isset( $arrmixValue[$strRequestKey] ) ) {
					$arrmixValue = $arrmixValue[$strRequestKey];
				} else {
					return NULL;
				}
			}
		}

		return $arrmixValue;
	}

	public function getIvrCallTypeId() {

		// Rollback plan: Return original call_type_id.
		// return $this->getCallTypeId();

		if( false == valId( $this->getCallTypeId() ) ) {
			$this->setCallTypeId( CCallType::CALL_CENTER );
		}

		switch( $this->getCallTypeId() ) {
			case CCallType::CALL_TRACKER_LEAD:
				$intIvrCallTypeId = CCallType::CALL_TRACKER_LEAD_VIA_IVR;
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE:
				$intIvrCallTypeId = CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR;
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY:
				$intIvrCallTypeId = CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR;
				break;

			default:
				$intIvrCallTypeId = $this->getCallTypeId();
				break;
		}

		return $intIvrCallTypeId;
	}

	public function getGreetingTypeIdByCallType() {
		$arrintGreetingTypeIds = [
			CCallType::CALL_TRACKER_LEAD                            => CGreetingType::PROPERTY_LEAD,
			CCallType::CALL_TRACKER_LEAD_VIA_IVR                    => CGreetingType::PROPERTY_LEAD,
			CCallType::CALL_TRACKER_MAINTENANCE                     => CGreetingType::PROPERTY_MAINTENANCE,
			CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR             => CGreetingType::PROPERTY_MAINTENANCE,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY           => CGreetingType::PROPERTY_MAINTENANCE_EMERGENCY,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR   => CGreetingType::PROPERTY_MAINTENANCE_EMERGENCY
		];

		return getArrayElementByKey( $this->getCallTypeId(), $arrintGreetingTypeIds );
	}

	public function fetchGreetingCallFile( $objVoipDatabase ) {
		return CCallFiles::fetchCallFileByPropertyIdByGreetingTypeId( $this->getPropertyId(), $this->getGreetingTypeIdByCallType(), $objVoipDatabase );
	}

	public function getE164PhoneNumber() {
		return ( false !== strpos( '+1', $this->getPhoneNumber() ) ) ? $this->getPhoneNumber() : '+1' . $this->getPhoneNumber();
	}

}
?>