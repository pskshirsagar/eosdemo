<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisQuestions
 * Do not add any new functions to this class.
 */

class CCallAnalysisQuestions extends CBaseCallAnalysisQuestions {

	public static function fetchCallAnalysisQuestionsByIds( $arrintCallAnalysisQuestionIds, $objVoipDatabase, $boolDeleted = false ) {
		if( false == valArr( $arrintCallAnalysisQuestionIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_analysis_questions
					WHERE
						id IN ( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
			if( false == $boolDeleted ) {
				$strSql .= ' AND deleted_on IS NULL';
			}
		$strSql .= ' ORDER BY order_num ASC';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisQuestionDetailsByIds( $arrintCallAnalysisQuestionIds, $objVoipDatabase, $boolDeleted = false ) {
		if( false == valArr( $arrintCallAnalysisQuestionIds ) ) return NULL;

		$strSql = 'SELECT
						caq.*,
						MAX( caa.points ) AS max_possible_points
					FROM
						call_analysis_questions caq
						JOIN call_analysis_answers caa ON caa.call_analysis_question_id = caq.id
					WHERE
						caq.id IN ( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
		if( false == $boolDeleted ) {
			$strSql .= ' AND caq.deleted_on IS NULL ';
		}
		$strSql .= 'GROUP BY
						caq.id
					ORDER BY 
						caq.order_num ASC';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchTotalWeightByCallAnalysisScorecardIdByCid( $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT
						SUM ( q.weight )
					FROM
						call_analysis_categories c
					JOIN
						call_analysis_questions q
						ON c.id = q.call_analysis_category_id
						AND c.cid = ' . ( int ) $intCid . '
						AND c.call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . '
						AND c.deleted_on IS NULL
						AND q.deleted_on IS NULL';

		$arrstrCount = fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrstrCount[0]['sum'] ) ) ? $arrstrCount[0]['sum'] : 0;
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisCategoryIdsByCid( $arrintCallAnalysisCategoryIds, $intCid, $objVoipDatabase, $boolIsParent = false, $arrintCallAnalysisQuestionIds = NULL ) {
		if( false == valArr( $arrintCallAnalysisCategoryIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_analysis_questions WHERE call_analysis_category_id IN( ' . implode( ',', $arrintCallAnalysisCategoryIds ) . ' ) AND cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintCallAnalysisQuestionIds ) ) {
			$strSql .= ' AND id IN ( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
		} else {
			$strSql .= ' AND deleted_on IS NULL ';
		}
		if( false == $boolIsParent ) {
			$strSql .= ' AND call_analysis_question_id IS NOT NULL';
		} else {
			$strSql .= ' AND call_analysis_question_id IS NULL';
		}

		$strSql .= ' ORDER BY order_num ASC';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchParentChildQuestionsByCallAnalysisQuestionId( $intCallAnalysisQuestionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_analysis_questions
					WHERE id = ' . ( int ) $intCallAnalysisQuestionId . ' OR
							call_analysis_question_id = ' . ( int ) $intCallAnalysisQuestionId . ' AND
							deleted_on IS NULL
					ORDER BY
						order_num ASC';
				return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchMaxOrderNumCallAnalysisQuestionsByCategoryIdByCid( $intCallAnalysisCategoryId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT
						MAX( order_num )
					FROM
						call_analysis_questions
					WHERE
						call_analysis_category_id = ' . ( int ) $intCallAnalysisCategoryId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL';
		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrintResponse[0]['max'] ) ) ? $arrintResponse[0]['max'] : 1;
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisCategoryIdByCid( $intCallAnalysisCategoryId, $intCid, $objVoipDatabase, $boolIsParent = false, $arrintCallAnalysisQuestionIds = NULL ) {
		$strSql = 'SELECT * FROM call_analysis_questions WHERE call_analysis_category_id = ' . ( int ) $intCallAnalysisCategoryId . ' AND cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintCallAnalysisQuestionIds ) ) {
			$strSql .= ' AND id IN ( ' . implode( ',', $arrintCallAnalysisQuestionIds ) . ' ) ';
		} else {
			$strSql .= ' AND deleted_on IS NULL ';
		}
		if( false == $boolIsParent ) {
			$strSql .= ' AND call_analysis_question_id IS NOT NULL';
		} else {
			$strSql .= ' AND call_analysis_question_id IS NULL';
		}

		$strSql .= ' ORDER BY order_num ASC';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchParentChildCallAnalysisQuestionsByCallAnalysisCategoryIdByCid( $intCallAnalysisCategoryId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_analysis_questions
					WHERE
						call_analysis_category_id = ' . ( int ) $intCallAnalysisCategoryId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL ';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchParentChildCallAnalysisQuestionsByCallAnalysisCategoryIdsByCid( $intCallAnalysisCategoryIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $intCallAnalysisCategoryIds ) || false == valId( $intCid ) ) return NULL;
		$strSql = 'SELECT
						*
					FROM
						call_analysis_questions
					WHERE
						call_analysis_category_id IN ( ' . implode( ',', $intCallAnalysisCategoryIds ) . ' )
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL ';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisQuestionIdsByCidByPropertyIds( $intCid, $arrintPropertyIdsAssociatedWithPsProductCallAnalysis, $objVoipDatabase ) {

		$strSql = 'SELECT
						car.call_analysis_question_id
					FROM
						call_analysis_questions caq
						JOIN call_analysis_results car ON ( car.cid = caq.cid AND car.call_analysis_question_id = caq.id )
						JOIN calls c ON ( c.cid = car.cid AND c.id = car.call_id )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.property_id IN ( ' . implode( ',', $arrintPropertyIdsAssociatedWithPsProductCallAnalysis ) . ' )
						AND c.analyzed_on IS NOT NULL
						AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
						AND ( caq.description LIKE \'%Leasing professional sets a SPECIFIC appointment with time and day%\' OR caq.description LIKE \'%Caller agrees to a SPECIFIC appointment with time and day.%\' )
					GROUP BY
						car.call_analysis_question_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisQuestionsCountByCidByPropertyIdsByCallFilterByCallAnalysisQuestionIdsByCallResultIdByIds( $intCid, $arrintPropertyIdsAssociatedWithPsProductCallAnalysis, $objSearchFilter, $arrintAppointmentQuestionIds, $intCallResultId, $objDatbase, $boolReport = false ) {

		if( true == $boolReport ) {
			$strStartDate 		= $objSearchFilter->getFilterObject( 'period' )->getStartDate();
			$strEndDate 		= $objSearchFilter->getFilterObject( 'period' )->getEndDate();
		} else {
			$strStartDate = $objSearchFilter->getSelectedStartDate();
			$strEndDate = $objSearchFilter->getSelectedEndDate();
		}

		$strSql = ' SELECT
						count( c.id ) AS total_call,
						count( CASE WHEN car.is_qualified = 1 THEN car.id ELSE NULL END) AS appointment_set_count,
						c.property_id
					FROM
						call_analysis_questions caq
						JOIN call_analysis_results car ON ( car.cid = caq.cid AND car.call_analysis_question_id = caq.id )
						JOIN calls c on ( car.cid = c.cid AND car.call_id = c.id ) ';

		$strSql .= 'WHERE
						caq.cid = ' . ( int ) $intCid;

		if( true == valArr( $arrintPropertyIdsAssociatedWithPsProductCallAnalysis ) ) {
			$arrintPropertyIds = implode( ',', $arrintPropertyIdsAssociatedWithPsProductCallAnalysis );
			$strSql .= ' AND c.property_id IN ( ' . $arrintPropertyIds . ' )';
		}

		$strSql .= '	AND DATE_TRUNC(\'day\', c.call_datetime) >= \'' . $strStartDate . '\'
						AND DATE_TRUNC(\'day\', c.call_datetime) <= \'' . $strEndDate . '\'
						AND car.call_analysis_question_id IN ( ' . implode( ',', $arrintAppointmentQuestionIds ) . ' )
						AND c.analyzed_on IS NOT NULL
						AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
						AND c.company_employee_id IS NOT NULL
						AND c.call_result_id <> ' . ( int ) $intCallResultId . '
						AND caq.id IN ( ' . implode( ',', $arrintAppointmentQuestionIds ) . ' )
				GROUP BY c.property_id';

		return fetchData( $strSql, $objDatbase );
	}

	public static function fetchCallAnalysisQuestionsByCallAnalysisCategoryIdsByFormFilterByCid( $arrintCallAnalysisCategoryIds, $arrmixFormFilter, $boolIsParentQuestions, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAnalysisCategoryIds ) ) return NULL;

		$strSql = ' SELECT
						inner_call_analysis_questions.id,
						inner_call_analysis_questions.call_analysis_category_id,
						inner_call_analysis_questions.call_analysis_question_id,
						inner_call_analysis_questions.description,
						inner_call_analysis_questions.weight,
						inner_call_analysis_questions.is_text,
						inner_call_analysis_questions.is_display,
						COUNT( distinct( car.id ) ) AS total_qualified_questions,
						COUNT( distinct( car.id ) ) * 100 / ( CASE WHEN 0 = COUNT( (c.id) ) THEN 1 ELSE COUNT( distinct( c.id ) ) END ) AS total_qualified_percentage
					FROM
						(
							SELECT
								caq.id,
								caq.weight,
								caq.cid,
								caq.call_analysis_category_id,
								caq.call_analysis_question_id,
								caq.description,
								caq.is_text,
								caq.is_display
							FROM
								call_analysis_questions caq
								LEFT JOIN call_analysis_results car ON  ( car.cid = caq.cid AND caq.id = car.call_analysis_question_id )
								JOIN calls c ON (	c.cid = car.cid AND c.id = car.call_id
													AND c.property_id IN ( ' . $arrmixFormFilter['property_id'] . ' )
													AND  c.call_datetime BETWEEN  \'' . $arrmixFormFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrmixFormFilter['end_date'] . ' 23:59:59 \'
													AND c.analyzed_on IS NOT NULL
													AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
													AND c.call_result_id <> ' . CCallResult::TEST_CALL . '
												)
					WHERE
						caq.cid = ' . ( int ) $intCid . '
						AND caq.call_analysis_category_id IN ( ' . implode( ',', ( array ) $arrintCallAnalysisCategoryIds ) . ' )
						AND (caq.deleted_on IS NULL OR caq.deleted_on >= car.created_on )		';

		if( true == $boolIsParentQuestions ) {
			$strSql .= ' AND caq.call_analysis_question_id IS NULL';
		} else {
			$strSql .= ' AND caq.call_analysis_question_id IS NOT NULL';
		}

		$strSql .= ' GROUP BY
						caq.id,
						caq.cid,
						caq.call_analysis_category_id,
						caq.call_analysis_question_id,
						caq.description,
						caq.is_text,
						caq.is_display,
						caq.weight,
						caq.created_on ';
		$strSql .= 'UNION

						SELECT
							caq.id,
							caq.weight,
							caq.cid,
							caq.call_analysis_category_id,
							caq.call_analysis_question_id,
							caq.description,
							caq.is_text,
							caq.is_display
						FROM
							call_analysis_questions caq
							LEFT JOIN call_analysis_results car ON ( car.cid = caq.cid AND caq.id = car.call_analysis_question_id )
						WHERE
							caq.call_analysis_category_id IN ( ' . implode( ',', ( array ) $arrintCallAnalysisCategoryIds ) . ' )
							AND caq.cid = ' . ( int ) $intCid;

		if( true == $boolIsParentQuestions ) {
			$strSql .= ' AND caq.call_analysis_question_id IS NULL';
		} else {
			$strSql .= ' AND caq.call_analysis_question_id IS NOT NULL';
		}

		$strSql .= ' AND car.call_analysis_question_id IS NOT NULL AND caq.deleted_on IS NULL

						GROUP BY
							caq.id ,
							caq.weight,
							caq.cid ,
							caq.call_analysis_category_id,
							caq.call_analysis_question_id,
							caq.description,
							caq.is_text,
							caq.is_display
						) AS inner_call_analysis_questions
						LEFT JOIN calls c on ( c.cid = inner_call_analysis_questions.cid
												AND c.cid = ' . ( int ) $intCid . '
												AND c.property_id IN ( ' . $arrmixFormFilter['property_id'] . ' )
												AND c.company_employee_id IN ( ' . $arrmixFormFilter['company_employee_ids'] . ')
												AND c.call_datetime BETWEEN  \'' . $arrmixFormFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrmixFormFilter['end_date'] . ' 23:59:59 \'
												AND c.analyzed_on IS NOT NULL
												AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
												AND c.call_result_id <> ' . CCallResult::TEST_CALL . ' )
						LEFT JOIN call_analysis_results car ON ( c.id = car.call_id AND car.call_analysis_question_id = inner_call_analysis_questions.id AND car.is_qualified = 1 )
						GROUP BY
							inner_call_analysis_questions.id,
							inner_call_analysis_questions.call_analysis_category_id,
							inner_call_analysis_questions.call_analysis_question_id,
							inner_call_analysis_questions.description,
							inner_call_analysis_questions.weight,
							inner_call_analysis_questions.is_display,
							inner_call_analysis_questions.is_text';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomCallAnalysisQuestionsByCallAnalysisCategoryIdsByCidByCallAnalysisQuestionTypeId( $arrintCallAnalysisCategoryIds, $intCid, $intCallAnalysisQuestionTypeId, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAnalysisCategoryIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						caq.id,
						caa.id AS answer_id,
						caa.answer
					FROM 
						call_analysis_questions caq 
						LEFT JOIN call_analysis_answers caa ON caq.id = caa.call_analysis_question_id
					WHERE 
						caq.call_analysis_category_id IN( ' . implode( ',', $arrintCallAnalysisCategoryIds ) . ' ) 
						AND caq.cid = ' . ( int ) $intCid . '
						AND caq.call_analysis_question_type_id = ' . ( int ) $intCallAnalysisQuestionTypeId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallAnalysisQuestions( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_analysis_questions WHERE call_analysis_category_id IS NOT NULL';

		return self::fetchCallAnalysisQuestions( $strSql, $objVoipDatabase );
	}

}
?>