<?php

class CCallAgentCoverage extends CBaseCallAgentCoverage {

	public function valBeginDatetime() {
		$boolIsValid = true;
		if( true == is_null( $this->getBeginDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_datetime', 'Begin date time is required.' ) );
		}

		return $boolIsValid;

	}

	public function valEndDatetime() {
		$boolIsValid = true;

		$strBeginDate	= strtotime( $this->getBeginDatetime() );
		$strEndDate		= strtotime( $this->getEndDatetime() );

		if( true == is_null( $this->getEndDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date time is required.' ) );
		} elseif( ( $strEndDate - $strBeginDate ) < 0 || $strEndDate == $strBeginDate ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_datetime', 'End date time should be greater than or equal to begin date.' ) );
		}

		return $boolIsValid;
	}

	public function valCoveredBy() {
		$boolIsValid = true;
		if( true == is_null( $this->getCoveredBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'covered_by', 'Coverage agent is require.' ) );
		}
		return $boolIsValid;
	}

	public function valCallAgentCoverageBeginDateTime() {
		$boolIsValid	= true;
		$boolIsValid	&= $this->valBeginDatetime();

		if( false == $boolIsValid )	{
			return $boolIsValid;
		}

		$arrstrBeginDate = explode( ' ', $this->getBeginDatetime() );

		if( 1 >= \Psi\Libraries\UtilFunctions\count( $arrstrBeginDate ) ) {
			if( 1 >= \Psi\Libraries\UtilFunctions\count( explode( '/', $arrstrBeginDate[0] ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_date', 'Begin date is required.' ) );
			} else {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'begin_time', 'Begin time is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCallAgentCoverageEndDateTime() {
		$boolIsValid	= true;
		$boolIsValid	&= $this->valEndDatetime();

		if( false == $boolIsValid ) {
			return $boolIsValid;
		}

		$arrstrEndDate = explode( ' ', $this->getEndDatetime() );

		if( 1 >= \Psi\Libraries\UtilFunctions\count( $arrstrEndDate ) ) {
			if( 1 >= \Psi\Libraries\UtilFunctions\count( explode( '/', $arrstrEndDate[0] ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_Date', 'End date is required.' ) );
			} else {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_time', 'End time is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valBeginDatetime();
				$boolIsValid &= $this->valEndDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_call_agent_coverage':
				$boolIsValid &= $this->valCallAgentCoverageBeginDateTime();
				$boolIsValid &= $this->valCallAgentCoverageEndDateTime();
				$boolIsValid &= $this->valCoveredBy();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>