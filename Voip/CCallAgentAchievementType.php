<?php

class CCallAgentAchievementType extends CBaseCallAgentAchievementType {

	const PERFECT_ATTENDANCE													= 1;
	const IGONRED_CALLS															= 2;
	const QA																	= 3;
	const ADHERENCE																= 4;
	const RED_ZONE																= 5;
	const ZERO_OCCURRENCES														= 6;
	const ZERO_IGNORED_CALLS													= 7;
	const NINETY_ONE_PERCENT_MONTHLY_AVERAGE									= 8;
	const ZERO_FAILED_CALLS														= 9;
	const HUNDREAD_PERCENT_BASIC_EXPECTATION									= 10;
	const NINETY_TWO_PERCENT_ADHERENCE											= 11;
	const TOP_TWENTY_FIVE_PERCENT_OF_AGENTS_WITH_THE_MOST_RED_ZONE_HOURS_LOGGED	= 12;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentAchievementTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>