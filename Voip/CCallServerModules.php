<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServerModules
 * Do not add any new functions to this class.
 */

class CCallServerModules extends CBaseCallServerModules {

	public static function fetchCallServerModules( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallServerModule', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallServerModule( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallServerModule', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>