<?php

class CCallAgentScheduleDetail extends CBaseCallAgentScheduleDetail {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	* Update Call Agent Schedule Details Functions
	*/

	public static function updateScheduleDetailsFromStartIntervalSequenceToEndIntervalSequenceForCallAgentWithCallAgentStatusTypeId( $intCallAgentId, $intCallAgentScheduleId, $intStartIntervalSequence, $intEndIntervalSequence, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valId( $intCallAgentScheduleId ) || false == valId( $intCallAgentStatusTypeId ) || false == valId( $intStartIntervalSequence ) || false == valId( $intEndIntervalSequence ) || false == valId( $intUserId ) ) return false;

		$boolIsValid							= true;
		$arrobjUpdateCallAgentScheduleDetails	= ( array ) \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->fetchCallAgentScheduleDetailsByCallAgentIdByCallAgentScheduleIdByIntervalSequence( $intCallAgentId, $intCallAgentScheduleId, $intStartIntervalSequence, $intEndIntervalSequence, $objVoipDatabase );

		foreach( $arrobjUpdateCallAgentScheduleDetails as $objCallAgentScheduleDetail ) {
			if( true == in_array( $intCallAgentStatusTypeId, [ CCallAgentStatusType::APPROVED_TO, CCallAgentStatusType::CONDITIONAL_APPROVED_TO ] ) ) {
				if( true == valId( $objCallAgentScheduleDetail->getCallAgentStatusTypeId() ) ) {
					$objCallAgentScheduleDetail->setCallAgentStatusTypeId( $intCallAgentStatusTypeId );
				}
			} else {
				$objCallAgentScheduleDetail->setCallAgentStatusTypeId( $intCallAgentStatusTypeId );
			}

			if( false == $objCallAgentScheduleDetail->update( $intUserId, $objVoipDatabase ) ) {
				$boolIsValid = false;
				break;
			}
		}

		return $boolIsValid;
	}

}
?>