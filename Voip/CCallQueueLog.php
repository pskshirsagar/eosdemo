<?php

class CCallQueueLog extends CBaseCallQueueLog {

	protected $m_objCurrentCallQueue;
	protected $m_objPreviousCallQueue;
	protected $m_objCurrentCallAgentQueue;
	protected $m_objPreviousCallAgentQueue;

	protected $m_arrobjCurrentGreetings;
	protected $m_arrobjPreviousGreetings;
	protected $m_arrobjCurrentCallFiles;
	protected $m_arrobjPreviousCallFiles;

	public function setCurrentCallQueue( $objCallQueue ) {
		$this->m_objCurrentCallQueue = $objCallQueue;
	}

	public function setPreviousCallQueue( $objCallQueue ) {
		$this->m_objPreviousCallQueue = $objCallQueue;
	}

	public function setCurrentCallAgentQueue( $objCallAgentQueue ) {
		$this->m_objCurrentCallAgentQueue = $objCallAgentQueue;
	}

	public function setPreviousCallAgentQueue( $objCallAgentQueue ) {
		$this->m_objPreviousCallAgentQueue = $objCallAgentQueue;
	}

	public function setCurrentGreetings( $arrobjGreetings ) {
		$this->m_arrobjCurrentGreetings = $arrobjGreetings;
	}

	public function setPreviousGreetings( $arrobjGreetings ) {
		$this->m_arrobjPreviousGreetings = $arrobjGreetings;
	}

	public function setCurrentCallFiles( $arrobjCallFiles ) {
		$this->m_arrobjCurrentCallFiles = $arrobjCallFiles;
	}

	public function setPreviousCallFiles( $arrobjCallFiles ) {
		$this->m_arrobjPreviousCallFiles = $arrobjCallFiles;
	}

	public function getPreviousCallQueue() {
		return $this->m_objPreviousCallQueue;
	}

	public function getPreviousCallFiles() {
		return $this->m_arrobjPreviousCallFiles;
	}

	public function getPreviousGreetings() {
		return $this->m_arrobjPreviousGreetings;
	}

	public function preparePreviousData() {
		if( true == valObj( $this->m_objPreviousCallQueue, 'CCallQueue' ) ) {
			$arrmixPreviousData = $this->m_objPreviousCallQueue->toArray();
		}

		foreach( ( array ) $this->m_arrobjPreviousCallFiles AS $objCallFile ) {
			$arrmixPreviousData['call_files'][$objCallFile->getId()]	= $objCallFile->toArray();
		}

		foreach( ( array ) $this->m_arrobjPreviousGreetings AS $objGreeting ) {
			$arrmixPreviousData['greetings'][$objGreeting->getId()]	= $objGreeting->toArray();
		}

		if( true == valObj( $this->m_objPreviousCallAgentQueue, 'CCallAgentQueue' ) ) {
			$arrmixPreviousData['call_agent_queue']	= $this->m_objPreviousCallAgentQueue->toArray();
		}

		$this->m_strPreviousData = json_encode( $arrmixPreviousData );
	}

	protected function prepareCurrentData() {
		if( true == valObj( $this->m_objCurrentCallQueue, 'CCallQueue' ) ) {
			$arrmixCurrentData = $this->m_objCurrentCallQueue->toArray();
		}

		foreach( ( array ) $this->m_arrobjCurrentCallFiles AS $objCallFile ) {
			$arrmixCurrentData['call_files'][$objCallFile->getId()]	= $objCallFile->toArray();
		}

		foreach( ( array ) $this->m_arrobjCurrentGreetings AS $objGreeting ) {
			$arrmixCurrentData['greetings'][$objGreeting->getId()]	= $objGreeting->toArray();
		}

		if( true == valObj( $this->m_objCurrentCallAgentQueue, 'CCallAgentQueue' ) ) {
			$arrmixCurrentData['call_agent_queue']	= $this->m_objCurrentCallAgentQueue->toArray();
		}

		$this->m_strCurrentData = json_encode( $arrmixCurrentData );
	}

	public function insertCallQueueLog( $intCurrentUserId, $objVoipDatabase ) {
		$this->preparePreviousData();
		$this->prepareCurrentData();
		$this->setCallQueueId( $this->m_objCurrentCallQueue->getId() );

		return $this->insert( $intCurrentUserId, $objVoipDatabase );
	}

	public function restoreCallQueue( $intCurrentUserId, $objVoipDatabase ) {
		$arrmixPreviousData = json_decode( json_encode( $this->getPreviousData() ), true );
		$objCallQueue = \Psi\Eos\Voip\CCallQueues::createService()->fetchCallQueueById( $this->getCallQueueId(), $objVoipDatabase );

		if( false == valObj( $objCallQueue, 'CCallQueue' ) ) {
			return false;
		}

		if( true == isset( $arrmixPreviousData['call_files'] ) && true == valArr( $arrmixPreviousData['call_files'] ) ) {
			$arrobjCallFiles	= \Psi\Eos\Voip\CCallFiles::createService()->fetchCallFilesByIds( ( array ) array_keys( $arrmixPreviousData['call_files'] ) );
			foreach( $arrmixPreviousData['call_files'] AS $intCallFileId => $arrstrCallFile ) {
				if( true == isset( $arrobjCallFiles[$intCallFileId] ) && true == valObj( $arrobjCallFiles[$intCallFileId], 'CCallFile' ) ) {
					$arrobjCallFiles[$intCallFileId]->setValues( $arrstrCallFile );
				}
			}
		}

		if( true == isset( $arrmixPreviousData['greetings'] ) && true == valArr( $arrmixPreviousData['greetings'] ) ) {
			$arrobjGreetings	= \Psi\Eos\Voip\CGreetings::createService()->fetchGreetingsByIds( ( array ) array_keys( $arrmixPreviousData['greetings'] ) );
			foreach( $arrmixPreviousData['greetings'] AS $intGreetingId => $arrstrGreeting ) {
				if( true == isset( $arrobjGreetings[$intGreetingId] ) && true == valObj( $arrobjGreetings[$intGreetingId], 'CGreeting' ) ) {
					$arrobjGreetings[$intGreetingId]->setValues( $arrstrGreeting );
				}
			}
		}

		$objCallQueue->setValues( $arrmixPreviousData );
		$objVoipDatabase->begin();

		if( false == $objCallQueue->update( $intCurrentUserId, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		if( true == valArr( $arrobjCallFiles ) ) {
			foreach( $arrobjCallFiles AS $objCallFile ) {
				if( false == $objCallFile->update( $intCurrentUserId, $objVoipDatabase ) ) {
					$objVoipDatabase->rollback();
					return false;
				}
			}
		}

		if( true == valArr( $arrobjGreetings ) ) {
			foreach( $arrobjGreetings AS $objGreeting ) {
				if( false == $objGreeting->update( $intCurrentUserId, $objVoipDatabase ) ) {
					$objVoipDatabase->rollback();
					return false;
				}
			}
		}

		if( CCallQueueType::INDIVIDUAL == $objCallQueue->getCallQueueTypeId() ) {
			$objCallAgentQueue = \Psi\Eos\Voip\CCallAgentQueues::createService()->fetchCallAgentQueueByCallQueueId( $objCallQueue->getId(), $objVoipDatabase );

			if( true == valObj( $objCallAgentQueue, 'CCallAgentQueue' ) ) {
				$objCallAgentQueue->setValues( $arrmixPreviousData['call_agent_queue'] );
				if( false == $objCallAgentQueue->update( $intCurrentUserId, $objVoipDatabase ) ) {
					$objVoipDatabase->rollback();
					return false;
				}
			}
		}

		$objVoipDatabase->commit();
		return true;
	}

}
?>