<?php

class CCallAgentStateType extends CBaseCallAgentStateType {

	const IDLE					= 1; // Means you're pinned in but not ready to receive your next call yet. (wrap up time)
	const WAITING				= 2; // Means you're waiting for the next call
	const RECEIVING				= 3; // Means a call is ringing to a specific agent (split second).  There is a rejected vs ignored event.  Not sure where stored.
	const IN_A_QUEUE_CALL   	= 4; // Means you're taking a call
	const REMOVED				= 5; // Probably refers to people clocking out.
	const UNUSED				= 6; // This is not used
	const NO_ANSWER				= 7; // Ignored
	const USER_BUSY				= 8; // Rejected
	const MIDNIGHT_BRIDGE		= 9; // Manual Midnight Bridge
	const DIALING				= 10; // Dialing outbound call.
	const IN_A_OUTBOUND_CALL	= 11; // In a Outbound Call

	public static $c_arrintActiveCallAgentStateTypeIds = array(
		self::IDLE,
		self::WAITING,
		self::RECEIVING,
		self::IN_A_QUEUE_CALL,
		self::DIALING
	);

	public static $c_arrstrCallAgentStateTypeIdToStr = array(
		self::IDLE				=> 'Idle',
		self::WAITING			=> 'Waiting',
		self::RECEIVING			=> 'Receiving',
		self::IN_A_QUEUE_CALL	=> 'In a queue call',
		self::REMOVED			=> 'Removed',
		self::UNUSED			=> 'New',
		self::NO_ANSWER 		=> 'Ignored',
		self::USER_BUSY			=> 'Rejected',
		self::DIALING			=> 'Dialing',
		self::IN_A_OUTBOUND_CALL => 'In a Outbound Call'
	);

	public static function callAgentStateTypeStrToId( $strCallAgentStateType ) {
		switch( $strCallAgentStateType ) {
			case 'Idle':
				$intCallAgentStateTypeId = self::IDLE;
				break;

			case 'Waiting':
				$intCallAgentStateTypeId = self::WAITING;
				break;

			case 'Receiving':
				$intCallAgentStateTypeId = self::RECEIVING;
				break;

			case 'In a Queue Call':
			case 'In a queue call':
				$intCallAgentStateTypeId = self::IN_A_QUEUE_CALL;
				break;

			case 'Removed':
				$intCallAgentStateTypeId = self::REMOVED;
				break;

			case 'New':
				$intCallAgentStateTypeId = self::UNUSED;
				break;

			case 'No Answer':
			case 'NO_ANSWER':
			case 'ORIGINATOR_CANCEL':
				$intCallAgentStateTypeId = self::NO_ANSWER;
				break;

			case 'User Busy':
			case 'USER_BUSY':
			case 'CALL_REJECTED':
				$intCallAgentStateTypeId = self::USER_BUSY;
				break;

			case 'Dialing':
				$intCallAgentStateTypeId = self::DIALING;
				break;

			case 'In a Outbound Call':
				$intCallAgentStateTypeId = self::IN_A_OUTBOUND_CALL;
				break;

			default:
				$intCallAgentStateTypeId = self::IDLE;
				break;
		}

		return $intCallAgentStateTypeId;
	}

}
?>