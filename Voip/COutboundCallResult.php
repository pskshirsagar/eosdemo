<?php

class COutboundCallResult extends CBaseOutboundCallResult {

	protected $m_strOutboundCallResultName;

	const LEFT_VOICEMAIL			= 'LEFT_VOICEMAIL';
	const WRONG_NUMBER				= 'WRONG_NUMBER';
	const RESIDENT_NOT_AVAILABLE	= 'RESIDENT_NOT_AVAILABLE';
	const NO_ANSWER					= 'NO_ANSWER';

	public static $c_arrmixCallResultsByProductId = [
		self::LEFT_VOICEMAIL			=> [
			CPsProduct::RESIDENT_PAY		=> CCallResult::RESIDENT_PAY_LEFT_VOICEMAIL,
			CPsProduct::RESIDENT_INSURE		=> CCallResult::RESIDENT_INSURE_LEFT_VOICEMAIL,
		],
		self::WRONG_NUMBER				=> [
			CPsProduct::RESIDENT_PAY		=> CCallResult::RESIDENT_PAY_WRONG_NUMBER,
			CPsProduct::RESIDENT_INSURE		=> CCallResult::RESIDENT_INSURE_WRONG_NUMBER
		],
		self::RESIDENT_NOT_AVAILABLE	=> [
			CPsProduct::RESIDENT_PAY		=> CCallResult::RESIDENT_PAY_RESIDENT_NOT_AVAILABLE,
			CPsProduct::RESIDENT_INSURE		=> CCallResult::RESIDENT_INSURE_RESIDENT_NOT_AVAILABLE
		],
		self::NO_ANSWER					=> [
			CPsProduct::RESIDENT_PAY		=> CCallResult::RESIDENT_PAY_NO_ANSWER,
			CPsProduct::RESIDENT_INSURE		=> CCallResult::RESIDENT_INSURE_NO_ANSWER
		]
	];

	/**
	 * set functions
	 */

 	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['outbound_call_result_name'] ) ) $this->setOutboundCallResultName( $arrmixValues['outbound_call_result_name'] );

		return;
	}

 	public function setOutboundCallResultName( $strOutboundCallResultName ) {
		$this->m_strOutboundCallResultName = $strOutboundCallResultName;
	}

	/**
	 * get functions
	 */

 	public function getOutboundCallResultName() {
		return $this->m_strOutboundCallResultName;
	}

 	public function valCid() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCid ) || false == is_numeric( $this->m_intCid ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

 	public function valOutboundCallId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intOutboundCallId ) || false == is_numeric( $this->m_intOutboundCallId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'outbound_call_id', 'Outbound call is required.' ) );
		}

		return $boolIsValid;
	}

 	public function valCallTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCallTypeId ) || false == is_numeric( $this->m_intCallTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_type_id', 'Call type is required.' ) );
		}

		return $boolIsValid;
	}

 	public function valCallResultId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_result_id', 'Please select call result.' ) );
		}

		return $boolIsValid;
	}

 	public function valNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNotes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Notes is required.' ) );
		}

		return $boolIsValid;
	}

 	public function valNpsOutboundCallReason() {
		$boolIsValid = true;

		if( ( false == valStr( $this->m_strNotes ) ) && ( ( CCallResult::OUTBOUND_NPS_CALL_BACK_LATER == $this->getCallResultId() ) || ( CCallResult::OUTBOUND_NPS_REFUSED == $this->getCallResultId() ) ) ) {
			$boolIsValid = false;
			if( CCallResult::OUTBOUND_NPS_CALL_BACK_LATER == $this->getCallResultId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Best time to contact is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Reason is required.' ) );
			}
		}

		return $boolIsValid;
	}

 	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_support_dialer_result':
				$boolIsValid &= $this->valCallResultId();
				break;

			case 'validate_nps_outbound_call_result':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valOutboundCallId();
				$boolIsValid &= $this->valCallTypeId();
				$boolIsValid &= $this->valCallResultId();
				$boolIsValid &= $this->valOutboundCallId();
				$boolIsValid &= $this->valNpsOutboundCallReason();
				break;

			default:
				// Default Case.
				break;
		}

		return $boolIsValid;
	}

}
?>