<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCalls
 * Do not add any new functions to this class.
 */

class CInboundCalls extends CBaseInboundCalls {

	/**
	 * Fetch Functions
	 */

	public static function fetchPaginatedInboundCallsByCidByCallsSearchByPropertyIds( $intPageNo, $intPageSize, $intCid, $objCallSearch, $arrintPropertyIds, $objVoipDatabase ) {

		if( true == is_null( $intCid ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
    	$intLimit	= ( int ) $intPageSize;

		$strSqlCondition = '';

		if( false == is_null( $objCallSearch->getPropertyId() ) && 0 < $objCallSearch->getPropertyId() ) {
			$strSqlCondition = ' AND ic.property_id = ' . ( int ) $objCallSearch->getPropertyId() . '';
		}

		if( false == is_null( $objCallSearch->getPaymentActivationId() ) && 'payment activation id' != \Psi\CStringService::singleton()->strtolower( $objCallSearch->getPaymentActivationId() ) && 0 < strlen( trim( $objCallSearch->getPaymentActivationId() ) ) ) {
			$strSqlCondition .= ' AND icp.payment_activation_id = ' . ( int ) $objCallSearch->getPaymentActivationId() . '';
		}

		$strSql = 'SELECT ic.id, ic.cid, ic.property_id, ic.caller_phone_number, ic.call_datetime, ic.call_duration_seconds, icp.payment_activation_id
					FROM inbound_calls ic,
						 inbound_call_processes icp
					WHERE ic.cid = ' . ( int ) $intCid . $strSqlCondition . ' ';

		if( true == is_null( $objCallSearch->getPropertyId() ) || false == is_numeric( $objCallSearch->getPropertyId() ) ) {
			$strSql .= 'AND ic.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')' . ' ';
		}

		$strSql .= 'AND ic.id = icp.inbound_call_id
				    -- AND icp.inbound_call_response_type_id > 6
					GROUP BY ic.id, ic.cid, ic.property_id, ic.caller_phone_number, ic.call_datetime, ic.call_duration_seconds, icp.payment_activation_id
					ORDER BY ic.id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchInboundCalls( $strSql, $objVoipDatabase );

	}

	public static function fetchTotalInboundCallsCountByCidByCallsSearchByPropertyIds( $intCid, $objCallSearch, $arrintPropertyIds, $objVoipDatabase ) {

		$strSqlCondition = '';

		if( true == is_null( $intCid ) || false == valArr( $arrintPropertyIds ) ) return NULL;

		if( false == is_null( $objCallSearch->getPropertyId() ) && 0 < $objCallSearch->getPropertyId() ) {
			$strSqlCondition = ' AND ic.property_id = ' . ( int ) $objCallSearch->getPropertyId() . '';
		}

		if( false == is_null( $objCallSearch->getPaymentActivationId() ) && 'payment activation id' != $objCallSearch->getPaymentActivationId() ) {
			$strSqlCondition .= ' AND icp.payment_activation_id = ' . ( int ) $objCallSearch->getPaymentActivationId() . '';
		}

		$strSql = 'SELECT count( ic.id )
					FROM inbound_calls ic,
						 inbound_call_processes icp
					WHERE ic.cid = ' . ( int ) $intCid . $strSqlCondition . ' ';

		if( true == is_null( $objCallSearch->getPropertyId() ) || false == is_numeric( $objCallSearch->getPropertyId() ) ) {
			$strSql .= 'AND ic.property_id IN (' . implode( ',', $arrintPropertyIds ) . ')' . ' ';
		}

		$strSql .= 'AND ic.id = icp.inbound_call_id
					-- AND icp.inbound_call_response_type_id > 6
					GROUP BY ic.cid';

		$arrstrData = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

    	return 0;
	}

}
?>