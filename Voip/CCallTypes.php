<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTypes
 * Do not add any new functions to this class.
 */

class CCallTypes extends CBaseCallTypes {

	public static function fetchAllCallTypes( $objVoipDatabase, $arrstrOrderBy = [ 'column' => 'order_num', 'order' => 'ASC' ] ) {
		$strSql = 'SELECT * FROM call_types';

		if( true == isset( $arrstrOrderBy['column'] ) ) {
			$strSql .= ' ORDER BY ' . $arrstrOrderBy['column'];
		}

		if( true == isset( $arrstrOrderBy['order'] ) ) {
			$strSql .= ' ' . $arrstrOrderBy['order'];
		}

		return self::fetchCallTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchVanityCallTypes( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_types WHERE id IN (' . implode( ', ', CCallTypeHelper::getVanityCallTypes() ) . ') ORDER BY name';
		return self::fetchCallTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTypesByIds( $arrintCallTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_types WHERE id IN ( ' . implode( ', ', $arrintCallTypeIds ) . ' ) ORDER BY name ASC';

		return self::fetchCallTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTypeById( $intCallTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ct.*,
						cq.name AS call_queue_name,
						cq.queue_phone_extension_id,
						cq.domain AS call_queue_domain
					FROM
						call_types AS ct
						LEFT JOIN call_queues AS cq ON ( ct.default_call_queue_id = cq.id )
					WHERE
						ct.id = ' . ( int ) $intCallTypeId;

		return parent::fetchCallType( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedQueueCallTypes( $objPagination, $objVoipDatabase, $boolIsFromQuickSearch = false, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$strWhereConditions = '';

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(ct.id)';
		} else {
			$strSql = 'SELECT
							ct.*,
							cq.name AS call_queue_name,
							cq.queue_phone_extension_id,
							cq.domain AS call_queue_domain,
							cs.name AS call_source_name,
							cd.name AS call_direction_name';

			$strSqlOrderBy		= ' ORDER BY ct.name';
			$strSqlOffsetLimit	= ( false == $boolIsFromQuickSearch ) ? ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize() : '';
		}

		$strSql .= ' FROM
						call_types AS ct
						LEFT JOIN call_sources AS cs ON ( cs.id = ct.call_source_id )
						LEFT JOIN call_directions AS cd ON ( cd.id = ct.call_direction_id )
						LEFT JOIN call_queues AS cq ON ( ct.default_call_queue_id = cq.id )';

		if( true == valStr( $objPagination->getQuickSearch() ) ) {
			$strWhereConditions = ' WHERE
										( CAST( ct.id AS text ) LIKE \'' . addslashes( $objPagination->getQuickSearch() ) . '\'
										OR lower( ct.name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' )
										OR lower( cq.name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) )';
		}

		$strSql .= $strWhereConditions;
		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return parent::fetchCallTypes( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallTypeByDefaultCallQueueId( $intDefaultCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM call_types WHERE default_call_queue_id = ' . ( int ) $intDefaultCallQueueId . ' LIMIT 1';
		return parent::fetchCallType( $strSql, $objVoipDatabase );
	}

	public static function fetchAllPublishedCallTypes( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_types WHERE is_published = TRUE ORDER BY name ASC';
		return parent::fetchCallTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTypesByCallSourceIds( $arrintCallSourceIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallSourceIds ) ) return [];

		$strSql = 'SELECT * FROM call_types WHERE call_source_id IN ( ' . implode( ',', $arrintCallSourceIds ) . ' ) ORDER BY order_num ASC';

		return self::fetchCallTypes( $strSql, $objVoipDatabase );
	}

}
?>