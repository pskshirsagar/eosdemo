<?php

class CCallQueueRequirement extends CBaseCallQueueRequirement {

	/**
	 * Constructor
	 */

	/**
	 * Validate Functions
	 */

 	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>