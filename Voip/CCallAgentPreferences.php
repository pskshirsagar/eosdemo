<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentPreferences
 * Do not add any new functions to this class.
 */

class CCallAgentPreferences extends CBaseCallAgentPreferences {

	public static function fetchCallAgentPreferencesByCallAgentIdByKeys( $intCallAgentId, $arrstrCallAgentPreferences, $objVoipDatabase ) {
		if( false == valArr( $arrstrCallAgentPreferences ) ) return;

		$arrobjCallAgentPreferences = self::fetchCallAgentPreferences( 'SELECT *FROM call_agent_preferences WHERE call_agent_id = ' . ( int ) $intCallAgentId . ' AND key IN ( \'' . implode( '\',\'', $arrstrCallAgentPreferences ) . '\' )', $objVoipDatabase );

		if( false == valArr( $arrobjCallAgentPreferences ) ) return;

		return rekeyObjects( 'Key', $arrobjCallAgentPreferences );
	}

	public static function fetchCallAgentPreferencesByEmployeeIdByKeysKeyed( $intEmployeeId, $arrstrCallAgentPreferenceKeys, $objVoipDatabase ) {
		if( false == valArr( $arrstrCallAgentPreferenceKeys ) ) return;

		$arrobjCallAgentPreferences = self::fetchCallAgentPreferences( 'SELECT *FROM call_agent_preferences WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND key IN ( \'' . implode( '\',\'', $arrstrCallAgentPreferenceKeys ) . '\' )', $objVoipDatabase );

		if( false == valArr( $arrobjCallAgentPreferences ) ) return;

		return rekeyObjects( 'Key', $arrobjCallAgentPreferences );
	}

	public static function fetchReKeyedCallAgentPreferencesByCallAgentId( $intCallAgentId, $objVoipDatabase ) {

		$arrobjCallAgentPreferences = self::fetchCallAgentPreferences( 'SELECT *FROM call_agent_preferences WHERE call_agent_id = ' . ( int ) $intCallAgentId, $objVoipDatabase );

		if( false == valArr( $arrobjCallAgentPreferences ) ) return;

		return rekeyObjects( 'Key', $arrobjCallAgentPreferences );
	}

	public static function fetchCallAgentPreferenceByCallAgentIdByKey( $intCallAgentId, $strKey, $objVoipDatabase ) {
		return self::fetchCallAgentPreference( 'SELECT * FROM call_agent_preferences WHERE call_agent_id = ' . ( int ) $intCallAgentId . ' AND key = \'' . $strKey . '\' ', $objVoipDatabase );
	}

	public static function fetchCallAgentPreferencesByKey( $strKey, $objVoipDatabase ) {
		return self::fetchCallAgentPreferences( 'SELECT * FROM call_agent_preferences WHERE key = \'' . $strKey . '\' ', $objVoipDatabase );
	}

}
?>