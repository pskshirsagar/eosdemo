<?php

class CCallChunk extends CBaseCallChunk {

	protected $m_intCid;
	protected $m_intPropertyId;

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( true == isset( $arrValues['property_id'] ) )   $this->setPropertyId( $arrValues['property_id'] );
		return;
	}

	/**
	 * set methods.
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/**
	 * get methods.
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	/**
	 * Misc methods.
	 */

	public function buildStoragePath( $boolIsFullFilePath = false ) {
		return ( ( true == $boolIsFullFilePath ) ? PATH_VOIP_MOUNTS : '' ) . 'call_files/property_scheduled_calls/' . $this->getCid() . '/' . $this->getPropertyId() . '/';
	}

	public function getFullFilePath() {
		return CCallFile::FREESWITCH_VOIP_MOUNTS . $this->getFilePath() . $this->getFileName();
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>