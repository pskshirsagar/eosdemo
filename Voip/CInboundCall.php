<?php

class CInboundCall extends CBaseInboundCall {

	protected $m_intPaymentActivationId;
	protected $m_intInboundCallProcessesCount;

	protected $m_arrobjInboundCallProcesses;
	protected $m_objInboundCallProcess;
	protected $m_strPaymentActivationIds;

 	public function __construct() {
		parent::__construct();
		$this->m_arrobjInboundCallProcesses = NULL;
		return;
	}

	// *****************************************************
	// ***************** add function **********************
	// *****************************************************

 	public function addInboundCallProcess( $objInboundCallProcess ) {
		$this->m_arrobjInboundCallProcesses[$objInboundCallProcess->getId()] = $objInboundCallProcess;
	}

	// *****************************************************
	// ***************** get function **********************
	// *****************************************************

 	public function getPaymentActivationId() {
		return $this->m_intPaymentActivationId;
	}

 	public function getInboundCallProcesses() {
		return $this->m_arrobjInboundCallProcesses;
	}

 	public function getPaymentActivationIds() {
		return $this->m_strPaymentActivationIds;
	}

 	public function getInboundCallProcess() {
		return $this->m_objInboundCallProcess;
	}

 	public function getInboundCallProcessesCount() {
		return $this->m_intInboundCallProcessesCount;
	}

	// *****************************************************
	// ***************** set function **********************
	// *****************************************************

 	public function setPaymentActivationIds( $strPaymentActivationIds ) {
		$this->m_strPaymentActivationIds = $strPaymentActivationIds;
	}

 	public function setInboundCallProcess( $objInboundCallProcess ) {
		$this->m_objInboundCallProcess = $objInboundCallProcess;
	}

 	public function setInboundCallProcessesCount( $intInboundCallProcessCount ) {
		$this->m_intInboundCallProcessesCount = $intInboundCallProcessCount;
	}

 	public function setPaymentActivationId( $intPaymentActivationId ) {
		$this->m_intPaymentActivationId = $intPaymentActivationId;
	}

 	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['payment_activation_id'] ) ) 	$this->setPaymentActivationId( $arrmixValues['payment_activation_id'] );

		return;
	}

	/**
	* Create Functions
	*
	*/

 	public function createInboundCallProcess( $intPaymentActivationId = NULL, $intInboundCallResponseTypeId = NULL ) {

		$objInboundCallProcess = new CInboundCallProcess();

		$objInboundCallProcess->setInboundCallId( $this->getId() );
		$objInboundCallProcess->setPaymentActivationId( $intPaymentActivationId );
		$objInboundCallProcess->setInboundCallResponseTypeId( ( false == is_null( $intInboundCallResponseTypeId ) ? $intInboundCallResponseTypeId : CInboundCallResponseType::PAYMENT_ACTIVATION_ID_EMPTY ) );
		$objInboundCallProcess->setProcessDatetime( 'NOW()' );

		return $objInboundCallProcess;
	}
}
?>