<?php

class CCallNotification extends CBaseCallNotification {

	protected $m_strUsername;

	/**
	 * Getter Functions
	 */

	public function getUsername() {
		return $this->m_strUsername;
	}

	/**
	 * Setter Functions
	 */

	public function setUsername( $strUsername ) {
		$this->m_strUsername = $strUsername;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['username'] ) ) $this->setUsername( $arrmixValues['username'] );

	}

}
?>
