<?php
/**
 * Created by PhpStorm.
 * User: hmahajan02
 * Date: 10/9/2019
 * Time: 2:41 PM
 */

class CCallAgentRewardPointLogFilter {

	protected $m_arrintCallAgentIds;
	protected $m_arrintRewardPointReferenceIds;
	protected $m_arrstrDateIntervals;

	protected $m_intRewardPointReferenceTypeId;
	protected $m_intRewardPointReferenceId;
	protected $m_intRewardPointTypeId;
	protected $m_intCallAgentId;
	protected $m_strStartDate;
	protected $m_strEndDate;

	/**
	* Setter Getter Functions
	*/

	public function setCallAgentIds( $intCallAgentIds ) {
		$this->m_arrintCallAgentIds = $intCallAgentIds;
	}

	public function getCallAgentIds() {
		return $this->m_arrintCallAgentIds;
	}

	public function setRewardPointReferenceIds( $arrintRewardPointReferenceIds ) {
		$this->m_arrintRewardPointReferenceIds = $arrintRewardPointReferenceIds;
	}

	public function getRewardPointReferenceIds() {
		return $this->m_arrintRewardPointReferenceIds;
	}

	public function setDateIntervals( $strDateIntervals ) {
		$this->m_arrstrDateIntervals = $strDateIntervals;
	}

	public function getDateIntervals() {
		return $this->m_arrstrDateIntervals;
	}

	public function setRewardPointReferenceTypeId( $intRewardPointReferenceTypeId ) {
		$this->m_intRewardPointReferenceTypeId = $intRewardPointReferenceTypeId;
	}

	public function getRewardPointReferenceTypeId() {
		return $this->m_intRewardPointReferenceTypeId;
	}

	public function setRewardPointReferenceId( $intRewardPointReferenceId ) {
		$this->m_intRewardPointReferenceId = $intRewardPointReferenceId;
	}

	public function getRewardPointReferenceId() {
		return $this->m_intRewardPointReferenceId;
	}

	public function setRewardPointTypeId( $intRewardPointTypeId ) {
		$this->m_intRewardPointTypeId = $intRewardPointTypeId;
	}

	public function getRewardPointTypeId() {
		return $this->m_intRewardPointTypeId;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = $strStartDate;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = $strEndDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	/**
	* Other Functions
	*/

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {
		if( true == valArr( $arrmixFormFields ) ) {
			$arrmixRequestForm = mergeIntersectArray( $arrmixFormFields, $arrmixRequestForm );
		}

		$this->setValues( $arrmixRequestForm );
		return true;
	}

	public function setValues( $arrmixValues ) {
		if( true == isset( $arrmixValues['call_agent_id'] ) )					$this->setCallAgentId( $arrmixValues['call_agent_id'] );
		if( true == isset( $arrmixValues['call_agent_ids'] ) )					$this->setCallAgentIds( $arrmixValues['call_agent_ids'] );
		if( true == isset( $arrmixValues['reward_point_type_id'] ) )			$this->setRewardPointTypeId( $arrmixValues['reward_point_type_id'] );
		if( true == isset( $arrmixValues['reward_point_reference_id'] ) )		$this->setRewardPointReferenceId( $arrmixValues['reward_point_reference_id'] );
		if( true == isset( $arrmixValues['reward_point_reference_type_id'] ) )	$this->setRewardPointReferenceTypeId( $arrmixValues['reward_point_reference_type_id'] );
		if( true == isset( $arrmixValues['date_intervals'] ) )					$this->setDateIntervals( $arrmixValues['date_intervals'] );
		if( true == isset( $arrmixValues['reward_point_reference_ids'] ) )		$this->setRewardPointReferenceIds( $arrmixValues['reward_point_reference_ids'] );
		if( true == isset( $arrmixValues['start_date'] ) )						$this->setStartDate( $arrmixValues['start_date'] );
		if( true == isset( $arrmixValues['end_date'] ) )						$this->setEndDate( $arrmixValues['end_date'] );
		return true;
	}

}