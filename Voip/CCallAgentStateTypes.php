<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentStateTypes
 * Do not add any new functions to this class.
 */

class CCallAgentStateTypes extends CBaseCallAgentStateTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchCallAgentStateTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallAgentStateType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallAgentStateType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallAgentStateType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCallAgentStateTypes( $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_agent_state_types';

		return self::fetchCallAgentStateTypes( $strSql, $objVoipDatabase );
	}

}
?>