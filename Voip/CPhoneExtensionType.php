<?php

class CPhoneExtensionType extends CBasePhoneExtensionType {

	const PHYSICAL_PHONE_EXTENSION_TYPE		= 1;
	const CALL_QUEUE_PHONE_EXTENSION_TYPE	= 2;
	const AGENT_PHONE_EXTENSION_TYPE		= 3;
	const DIALPLAN_PHONE_EXTENSION_TYPE		= 4;
	const EMERGENCY_CALL_SERVICES			= 5;
	const CONFERENCE_PHONE_EXTENSION		= 6;

	public static $c_arrstrPhoneExtensionTypeIdsToStr = array(
		self::PHYSICAL_PHONE_EXTENSION_TYPE		=> 'Physical Extension',
		self::CALL_QUEUE_PHONE_EXTENSION_TYPE	=> 'Call Queue Extension',
		self::AGENT_PHONE_EXTENSION_TYPE		=> 'Agent Extension',
		self::DIALPLAN_PHONE_EXTENSION_TYPE		=> 'Dialplan Extension',
		self::CONFERENCE_PHONE_EXTENSION		=> 'Conference Phone Extension'
	);

	public static $c_arrintPhoneExtensionTypes = array(
		self::PHYSICAL_PHONE_EXTENSION_TYPE,
		self::CALL_QUEUE_PHONE_EXTENSION_TYPE,
		self::AGENT_PHONE_EXTENSION_TYPE,
		self::DIALPLAN_PHONE_EXTENSION_TYPE,
		self::CONFERENCE_PHONE_EXTENSION
	);
}
?>