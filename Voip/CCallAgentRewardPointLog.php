<?php

class CCallAgentRewardPointLog extends CBaseCallAgentRewardPointLog {

	protected $m_intTeamId;
	protected $m_intEmployeeId;
	protected $m_intMonthOfChallenge;
	protected $m_strCallAgentName;

	const MAX_ALLOWED_VALUE_LENGTH = 6;

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['team_id'] ) )			$this->setTeamId( $arrmixValues['team_id'] );
		if( isset( $arrmixValues['employee_id'] ) )		$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( isset( $arrmixValues['call_agent_name'] ) )	$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		return true;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setTeamId( $intTeamId ) {
		$this->m_intTeamId = $intTeamId;
	}

	public function setMonthOfChallenge( $intMonthOfChallenge ) {
		$this->m_intMonthOfChallenge = $intMonthOfChallenge;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function getMonthOfChallenge() {
		return $this->m_intMonthOfChallenge;
	}

	/**
	* Validate Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Please select call agent.' ) );
		}

		return $boolIsValid;
	}

	public function valRewardPointMultiplierId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentRedeemedProductId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentRedeemedProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_redeemed_product_id', 'Invalid call agent redeemed product id.' ) );
		}

		return $boolIsValid;
	}

	public function valRewardPointReferenceId() {
		$boolIsValid = true;

		if( false == valId( $this->getRewardPointReferenceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reward_point_reference_id', 'Please select reason of points.' ) );
		}

		return $boolIsValid;
	}

	public function valEarnedNumberOfPoints() {
		$boolIsValid = true;

		if( NULL != $this->getEarnedNumberOfPoints() && ( false == is_numeric( $this->getEarnedNumberOfPoints() ) || 0 > $this->getEarnedNumberOfPoints() || false == valStr( $this->getEarnedNumberOfPoints() ) || self::MAX_ALLOWED_VALUE_LENGTH < strlen( round( $this->getEarnedNumberOfPoints(), 0 ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'earned_number_of_points', 'Please enter valid points.' ) );
		}

		return $boolIsValid;
	}

	public function valCurrentNumberOfPoints() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getCurrentNumberOfPoints() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_number_of_points', 'Invalid call agent current number of points.' ) );
		}

		return $boolIsValid;
	}

	public function valPreviousNumberOfPoints() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentNumberOfCoins() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getCurrentNumberOfCoins() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_number_of_coins', 'Invalid call agent current number of coins.' ) );
		}

		return $boolIsValid;
	}

	public function valPreviousNumberOfCoins() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getPreviousNumberOfCoins() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'previous_number_of_coins', 'Invalid previous number of coins.' ) );
		}

		return $boolIsValid;
	}

	public function valRemark() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddedDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getAddedDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'added_date', 'Please select date of achievement.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valEarnedNumberOfPoints();
				$boolIsValid &= $this->valCurrentNumberOfPoints();
				$boolIsValid &= $this->valCurrentNumberOfCoins();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_call_agent_reward_point_log_insert':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valCallAgentRedeemedProductId();
				$boolIsValid &= $this->valPreviousNumberOfCoins();
				break;

			case 'validate_manual_achievements':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valRewardPointReferenceId();
				$boolIsValid &= $this->valEarnedNumberOfPoints();
				break;

			case 'validate_challenge_manual_achievements':
				$boolIsValid &= $this->valAddedDate();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function processRewardPointMultipliers( $objCallAgentRewardPoint, $arrmixActiveRewardPointMultipliers, $intUserId, $objVoipDatabase, $intPointMultipleOf = 1, $arrmixRewardPointMultipliersHours = [] ) {
		$arrobjInsertCallAgentRewardPointLogs = [];

		$this->setRewardPointReferenceTypeId( CRewardPointReferenceType::REWARD_POINT_MULTIPLIER );
		$this->setRewardPointReferenceId( $objCallAgentRewardPoint->getCallAgentRewardPointLogId() );
		$fltEarnedNumberOfPoints	= $this->getEarnedNumberOfPoints();
		$fltRewardPointValue		= $fltEarnedNumberOfPoints / $intPointMultipleOf;

		foreach( $arrmixActiveRewardPointMultipliers as $intRewardPointMultiplierId => $mixPointMultiplier ) {
			$fltEarnedMultiplierPoints	= ( ( true == valArrKeyExists( $arrmixRewardPointMultipliersHours, $intRewardPointMultiplierId ) ) ? ( $fltRewardPointValue * $arrmixRewardPointMultipliersHours[$intRewardPointMultiplierId] ) : $fltEarnedNumberOfPoints ) * ( $mixPointMultiplier - 1 );
			$fltEarnedNumberOfCoins		= $fltEarnedMultiplierPoints;

			$objInsertCallAgentRewardPointLog = clone $this;
			$objInsertCallAgentRewardPointLog->setRewardPointMultiplierId( $intRewardPointMultiplierId );
			$objInsertCallAgentRewardPointLog->setEarnedNumberOfPoints( $fltEarnedMultiplierPoints );

			$fltTotalNumberOfPoints	= $objCallAgentRewardPoint->getTotalNumberOfPoints();
			$fltTotalNumberOfCoins	= $objCallAgentRewardPoint->getTotalNumberOfCoins();

			$objInsertCallAgentRewardPointLog->setCurrentNumberOfPoints( $fltEarnedMultiplierPoints + $fltTotalNumberOfPoints );
			$objInsertCallAgentRewardPointLog->setPreviousNumberOfPoints( $fltTotalNumberOfPoints );
			$objInsertCallAgentRewardPointLog->setCurrentNumberOfCoins( $fltEarnedNumberOfCoins + $fltTotalNumberOfCoins );
			$objInsertCallAgentRewardPointLog->setPreviousNumberOfCoins( $fltTotalNumberOfCoins );

			$arrobjInsertCallAgentRewardPointLogs[] = $objInsertCallAgentRewardPointLog;
			$objCallAgentRewardPoint->setTotalNumberOfPoints( $objInsertCallAgentRewardPointLog->getCurrentNumberOfPoints() );
			$objCallAgentRewardPoint->setTotalNumberOfCoins( $objInsertCallAgentRewardPointLog->getCurrentNumberOfCoins() );
		}

		if( true == valArr( $arrobjInsertCallAgentRewardPointLogs ) && false == \Psi\Eos\Voip\CCallAgentRewardPointLogs::createService()->bulkInsert( $arrobjInsertCallAgentRewardPointLogs, $intUserId, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>