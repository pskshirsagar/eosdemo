<?php

class CCallFormStatusType extends CBaseCallFormStatusType {

	const CALL_FORM_STATUS_COMPLETE = 1;
	const CALL_FORM_STATUS_INCOMPLETE = 2;

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default case
            	$boolIsValid = true;
            	break;
        }

        return $boolIsValid;
    }
}
?>