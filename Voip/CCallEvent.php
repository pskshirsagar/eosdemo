<?php

class CCallEvent extends CBaseCallEvent {

	protected $m_arrobjCallEventTypes;

	protected $m_strCallStatusTypeName;

	/**
	 * Construct Functions
	 */

 public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['call_status_type_name'] ) ) $this->setCallStatusTypeName( $arrValues['call_status_type_name'] );

		return;
 }

	/**
	 * Validate Functions
	 */

 public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
 }

	/**
	 * Adder Functions
	 */

 public function addCallEventType( $objCallEventType ) {
		$this->m_arrobjCallEventTypes[$objCallEventType->getId()] = $objCallEventType;
 }

	/**
	 * Setter Functions
	 */

 public function setCallEventTypes( $arrobjCallEventTypes ) {
		$this->m_arrobjCallEventTypes = $arrobjCallEventTypes;
 }

 public function setCallStatusTypeName( $strCallStatusTypeName ) {
		$this->m_strCallStatusTypeName = $strCallStatusTypeName;
 }

	/**
	 * Getter Functions
	 */

 public function getCallEventTypes() {
		return $this->m_arrobjCallEventTypes;
 }

 public function getCallStatusTypeName() {
		return $this->m_strCallStatusTypeName;
 }
}
?>