<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCompanyCallSettings
 * Do not add any new functions to this class.
 */

class CCompanyCallSettings extends CBaseCompanyCallSettings {

	public static function fetchCompanyCallSettingByCid( $intClientId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM company_call_settings WHERE cid = ' . ( int ) $intClientId . ' LIMIT 1';
		return self::fetchCompanyCallSetting( $strSql, $objVoipDatabase );
	}

}
?>