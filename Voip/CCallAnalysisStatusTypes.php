<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisStatusTypes
 * Do not add any new functions to this class.
 */

class CCallAnalysisStatusTypes extends CBaseCallAnalysisStatusTypes {

	public static function fetchCallAnalysisStatusTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallAnalysisStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallAnalysisStatusType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallAnalysisStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallAnalysisStatusTypesByCallAnalysisStatusTypeIdByIsPublished( $intCallAnalysisStatusTypeId, $boolIsPublished, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisStatusTypeId ) || false == is_bool( $boolIsPublished ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_analysis_status_types
					WHERE
						call_analysis_status_type_id = ' . ( int ) $intCallAnalysisStatusTypeId;

		$strSql .= ( true == $boolIsPublished ) ? ' AND is_published IS TRUE ' : ' AND is_published IS FALSE ';
		$strSql .= ' ORDER BY order_num ASC ';

		return self::fetchCallAnalysisStatusTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAnalysisStatusTypesByCallAnalysisStatusTypeIdByIsPublished( $intCallAnalysisStatusTypeId, $boolIsPublished, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisStatusTypeId ) || false == is_bool( $boolIsPublished ) ) return NULL;

		$strSql = 'SELECT
						id,
						name
					FROM
						call_analysis_status_types
					WHERE
						call_analysis_status_type_id = ' . ( int ) $intCallAnalysisStatusTypeId;

		$strSql .= ( true == $boolIsPublished ) ? ' AND is_published IS TRUE ' : ' AND is_published IS FALSE ';
		$strSql .= ' ORDER BY order_num ASC ';

		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>