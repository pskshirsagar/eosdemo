<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCalls
 * Do not add any new functions to this class.
 */

class COutboundCalls extends CBaseOutboundCalls {

	public static function fetchSimpleOutboundCallCustomerIdsByCustomerIds( $arrintCustomerIds, $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valArr( $arrintCustomerIds ) || false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						customer_id
					FROM
						outbound_calls
					WHERE
						outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		$arrintNewCustomerIds		= fetchData( $strSql, $objVoipDatabase );
		$arrintRekeyedCustomerIds	= array();

		if( true == valArr( $arrintNewCustomerIds ) ) {
			foreach( $arrintNewCustomerIds as $arrintNewCustomerId ) {
				$arrintRekeyedCustomerIds[$arrintNewCustomerId['customer_id']] = $arrintNewCustomerId['customer_id'];
			}
		}

		return $arrintRekeyedCustomerIds;
	}

	public static function fetchPendingOutboundCalls( $intOutboundCallCampaignId, $intLimit, $intMaximumCallsInQueueTolerance, $objVoipDatabase, $arrintCallQueueIds = NULL ) {
		// Revert calls that have been stuck in the queue for a long period of time (this shouldn't happen, but it might)
		$strSql = 'UPDATE
						outbound_calls
					SET
						locked_on = NULL,
						lock_sequence = NULL,
						outbound_call_status_type_id = ' . COutboundCallStatusType::PENDING . '
					WHERE
						lock_sequence IS NOT NULL
						AND COALESCE( locked_on, NOW() ) < ( NOW() - INTERVAL \'1 minute\' )
						AND outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
						AND outbound_call_status_type_id IN ( ' . COutboundCallStatusType::QUEUING . ' )';

		if( true == valArr( $arrintCallQueueIds ) ) {
			$strSql .= ' AND call_queue_id IN ( ' . implode( ', ', $arrintCallQueueIds ) . ' ) ';
		}

		fetchData( $strSql, $objVoipDatabase );

		$strSql = 'SELECT count(id) FROM outbound_calls WHERE outbound_call_status_type_id IN ( ' . COutboundCallStatusType::QUEUING . ', ' . COutboundCallStatusType::QUEUED . ' )';

		if( true == valArr( $arrintCallQueueIds ) ) {
			$strSql .= ' AND call_queue_id IN ( ' . implode( ', ', $arrintCallQueueIds ) . ' ) ';
		}

		$arrstrResult = fetchData( $strSql, $objVoipDatabase );

		if( true == isset ( $arrstrResult[0]['count'] ) && $intMaximumCallsInQueueTolerance < $arrstrResult[0]['count'] ) {
			return array();
		}

		$strSql = "SELECT nextval( 'outbound_calls_lock_seq'::text ) AS id";

		$arrstrResult = fetchData( $strSql, $objVoipDatabase );

		$intNextQueueLockNumber = $arrstrResult[0]['id'];

		$strAdditionalCondition	= '';

		if( true == valArr( $arrintCallQueueIds ) ) {
			$strAdditionalCondition = ' AND call_queue_id IN ( ' . implode( ', ', $arrintCallQueueIds ) . ' )';
		}

		$arrobjOutboundCalls = array();

		switch( $intOutboundCallCampaignId ) {

			case COutboundCallCampaign::OUTBOUND_MOVE_IN:
				$strTimeZoneCondition = ' CASE
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -1000 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Pacific/Honolulu\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -900 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Anchorage\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -800 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Los_Angeles\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -700 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Denver\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -600 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Chicago\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -500 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/New_York\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -400 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Atlantic\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_MOVE_IN . ' AND pcs.timezone_offset = -350 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Newfoundland\' ) )
												ELSE NULL
											END AS property_time_hour';

				$strJoinCondition = ' JOIN property_call_settings pcs ON ( pcs.cid = oc.cid AND pcs.property_id = oc.property_id )';

				$intPropertyCallStartTimeAtNine		= 9;
				$intPropertyCallEndTimeAtTweleve	= 12;
				$intPropertyCallStartTimeAtTweleve	= 12;
				$intPropertyCallEndTimeAtSixteen	= 16;
				$intPropertyCallStartTimeAtSixteen	= 16;
				$intPropertyCallEndTimeAtNinteen	= 19;

				$strOutboundCallTimeCondition = ' ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtNine . '
													AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtTweleve . ' )
												OR ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtSixteen . '
													AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtNinteen . ' )
												OR ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtTweleve . '
													AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtSixteen . ' )';

				$strAdditionalCondition	.= ' AND oc.call_agent_id IS NOT NULL';

				$arrobjOutboundCalls = self::processPendingOutboundCalls( $intOutboundCallCampaignId, $intNextQueueLockNumber, $intLimit, $strTimeZoneCondition, $strAdditionalCondition, $strJoinCondition, $strOutboundCallTimeCondition, $objVoipDatabase );
				break;

			case COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN:
				$strTimeZoneCondition = ' CASE
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -1000 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Pacific/Honolulu\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -900 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Anchorage\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -800 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Los_Angeles\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -700 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Denver\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -600 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Chicago\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -500 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/New_York\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -400 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Atlantic\' ) )
												WHEN oc.outbound_call_campaign_id = ' . COutboundCallCampaign::OUTBOUND_TRAINING_MOVE_IN . ' AND pcs.timezone_offset = -350 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Newfoundland\' ) )
												ELSE NULL
											END AS property_time_hour';

				$strJoinCondition = ' JOIN property_call_settings pcs ON ( pcs.cid = oc.cid AND pcs.property_id = oc.property_id )';

				$intPropertyCallStartTime	= 0;
				$intPropertyCallEndTime		= 24;

				$strOutboundCallTimeCondition = ' ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTime . '
													AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTime . ' )';

				$strAdditionalCondition	.= ' AND oc.call_agent_id IS NOT NULL';

				$arrobjOutboundCalls = self::processPendingOutboundCalls( $intOutboundCallCampaignId, $intNextQueueLockNumber, $intLimit, $strTimeZoneCondition, $strAdditionalCondition, $strJoinCondition, $strOutboundCallTimeCondition, $objVoipDatabase );
				break;

			default:
				$arrobjOutboundCalls = self::processPendingSupportOutboundDialerCalls( $intOutboundCallCampaignId, $intNextQueueLockNumber, $intLimit, $strAdditionalCondition, $objVoipDatabase );
				break;
		}

		return $arrobjOutboundCalls;
	}

	public static function processPendingOutboundCalls( $intOutboundCallCampaignId, $intNextQueueLockNumber, $intLimit, $strTimeZoneCondition, $strAdditionalCondition, $strJoinCondition, $strOutboundCallTimeCondition, $objVoipDatabase ) {

		$strAdditionalCondition = ' oc.outbound_call_status_type_id = ' . COutboundCallStatusType::PENDING . '
									AND oc.outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
									AND oc.phone_number IS NOT NULL
									AND oc.call_queue_id IS NOT NULL
									AND oc.locked_on IS NULL
									AND oc.lock_sequence IS NULL
									AND ( oc.dont_call_after IS NULL OR oc.dont_call_after > NOW() )' . $strAdditionalCondition . '
									ORDER BY
										call_priority_id DESC,
										attempt_count ASC,
										dont_call_after DESC';

		$strSql = 'BEGIN WORK;

					SELECT
						*
					FROM
						(
							SELECT
								oc.*,
								' . $strTimeZoneCondition . '
							FROM
								outbound_calls oc
								' . $strJoinCondition . '
							WHERE
								' . $strAdditionalCondition . '
						) AS subquery
					WHERE
						 ' . $strOutboundCallTimeCondition . '
					ORDER BY
						call_priority_id DESC,
						attempt_count ASC,
						dont_call_after DESC

					FOR UPDATE NOWAIT;

					UPDATE
						outbound_calls oc
					SET
						locked_on = NOW(),
						lock_sequence = ' . ( int ) $intNextQueueLockNumber . ',
						outbound_call_status_type_id = ' . COutboundCallStatusType::QUEUING . '
					WHERE
						id IN (
								SELECT
									id
								FROM
									(
										SELECT
											oc.id as id,
											' . $strTimeZoneCondition . '
										FROM
											outbound_calls oc
											' . $strJoinCondition . '
										WHERE
											' . $strAdditionalCondition . '
									) AS subquery
								WHERE
									' . $strOutboundCallTimeCondition . '
								LIMIT ' . ( int ) $intLimit . ' );
					COMMIT WORK;

					SELECT * FROM outbound_calls WHERE lock_sequence = ' . ( int ) $intNextQueueLockNumber . ' ORDER BY call_priority_id DESC, attempt_count ASC, initiated_on DESC, dont_call_after DESC;';

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function processPendingSupportOutboundDialerCalls( $intOutboundCallCampaignId, $intNextQueueLockNumber, $intLimit, $strAdditionalCondition, $objVoipDatabase ) {

		$strAdditionalCondition = ' oc.outbound_call_status_type_id = ' . COutboundCallStatusType::PENDING . '
									AND oc.outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
									AND oc.call_queue_id IS NOT NULL
									AND oc.phone_number IS NOT NULL
									AND oc.locked_on IS NULL
									AND oc.lock_sequence IS NULL
									AND ( oc.dont_call_after IS NULL OR oc.dont_call_after > NOW() ) ' . $strAdditionalCondition;

		$strSql = 'BEGIN WORK;

					SELECT
						oc.*
					FROM
						outbound_calls oc
					WHERE
						' . $strAdditionalCondition . '

					FOR UPDATE NOWAIT;

					UPDATE
						outbound_calls oc
					SET
						locked_on = NOW(),
						lock_sequence = ' . ( int ) $intNextQueueLockNumber . ',
						outbound_call_status_type_id = ' . COutboundCallStatusType::QUEUING . '
					WHERE
						id IN (
							SELECT
								oc.id
							FROM
								outbound_calls oc
							WHERE
								' . $strAdditionalCondition . '
							LIMIT ' . ( int ) $intLimit . ' );
					COMMIT WORK;

					SELECT * FROM outbound_calls WHERE lock_sequence = ' . ( int ) $intNextQueueLockNumber . ' ORDER BY initiated_on DESC, dont_call_after DESC;';

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedOutboundCallsByOutboundCallFilter( $objPagination, $objOutboundCallFilter, $objVoipDatabase, $boolCountOnly = false ) {
		$arrstrConditions	= array();
		$strSqlOffsetLimit	= '';

		if( true == $boolCountOnly ) {
			$strSql = ' SELECT
							COUNT ( DISTINCT ( oc.id ) ) AS count
						FROM
							outbound_calls AS oc
							LEFT JOIN calls AS c ON ( c.outbound_call_id = oc.id ) ';

			if( false == is_null( $objOutboundCallFilter->getCallResultIds() ) ) {
				$strSql .= ' LEFT JOIN outbound_call_results AS ocr ON ( ocr.outbound_call_id = oc.id ) ';
			}

			if( true == $objOutboundCallFilter->getIsAllPropertiesSelected() ) {
				$strSql .= 'LEFT JOIN property_call_settings AS pcs ON (oc.property_id = pcs.property_id)';
			}

			$strSql .= ' WHERE 1 = 1 ';
		} else {
			$strSql = 'SELECT
							DISTINCT ON ( oc.id )
							oc.*,
							occ.name AS outbound_call_campaign_name,
							ocst.name AS outbound_call_status_type_name,
							MAX( c.call_datetime ) AS call_datetime
						FROM
							outbound_calls AS oc
							LEFT JOIN outbound_call_campaigns AS occ ON ( oc.outbound_call_campaign_id = occ.id )
							LEFT JOIN outbound_call_status_types AS ocst ON ( oc.outbound_call_status_type_id = ocst.id )
							LEFT JOIN calls AS c ON ( c.outbound_call_id = oc.id ) ';

			if( false == is_null( $objOutboundCallFilter->getCallResultIds() ) ) {
				$strSql .= ' LEFT JOIN outbound_call_results AS ocr ON ( ocr.outbound_call_id = oc.id ) ';
			}

			if( true == $objOutboundCallFilter->getIsAllPropertiesSelected() ) {
				$strSql .= 'LEFT JOIN property_call_settings AS pcs ON (oc.property_id = pcs.property_id)';
			}

			$strSql				.= ' WHERE 1 = 1 ';
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valObj( $objOutboundCallFilter, 'COutboundCallFilter' ) ) {
			if( true == valStr( $objOutboundCallFilter->getStartDate() ) ) {
				$arrstrConditions[] = ' oc.initiated_on >= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getStartDate() ) ) . '\'';
			}

			if( true == valStr( $objOutboundCallFilter->getEndDate() ) ) {
				$arrstrConditions[] = ' oc.initiated_on <= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getEndDate() ) ) . '\'';
			}

			if( true == valStr( $objOutboundCallFilter->getCallDatetimeStartDate() ) ) {
				$arrstrConditions[] = ' DATE_TRUNC(\'day\', c.call_datetime) >= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getCallDatetimeStartDate() ) ) . '\'';
			}

			if( true == valStr( $objOutboundCallFilter->getCallDatetimeEndDate() ) ) {
				$arrstrConditions[] = ' DATE_TRUNC(\'day\', c.call_datetime) <= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getCallDatetimeEndDate() ) ) . '\'';
			}

			if( true == valStr( $objOutboundCallFilter->getDontCallAfterStartDate() ) ) {
				$arrstrConditions[] = ' oc.dont_call_after >= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getDontCallAfterStartDate() ) ) . '\'';
			}

			if( true == valStr( $objOutboundCallFilter->getDontCallAfterEndDate() ) ) {
				$arrstrConditions[] = ' oc.dont_call_after <= \'' . date( 'm-d-Y', strtotime( $objOutboundCallFilter->getDontCallAfterEndDate() ) ) . '\'';
			}

			if( true == valId( trim( $objOutboundCallFilter->getOutboundCallId() ) ) ) {
				$arrstrConditions[] = ' oc.id = ' . ( int ) $objOutboundCallFilter->getOutboundCallId();
			}

			if( true == valId( $objOutboundCallFilter->getResidentRaisedConcern() ) && true == $objOutboundCallFilter->getResidentRaisedConcern() ) {
				$arrstrConditions[] = ' c.call_notes = \'' . $objOutboundCallFilter->getResidentRaisedConcern() . '\'';
			} elseif( '0' == $objOutboundCallFilter->getResidentRaisedConcern() ) {
				$arrstrConditions[] = ' ( c.call_notes <> \'1\' OR c.call_notes IS NULL )';
			}

			if( true == valId( $objOutboundCallFilter->getCids() ) ) {
				$arrstrConditions[] = ' oc.cid = ' . ( int ) $objOutboundCallFilter->getCids();
			} elseif( true == valStr( $objOutboundCallFilter->getCids() ) ) {
				$arrstrConditions[] = ' oc.cid IN ( ' . $objOutboundCallFilter->getCids() . ' ) ';
			}

			if( true == is_numeric( $objOutboundCallFilter->getPropertyIds() ) ) {
				$arrstrConditions[] = ' oc.property_id = ' . ( int ) $objOutboundCallFilter->getPropertyIds();
			} elseif( true == valStr( $objOutboundCallFilter->getPropertyIds() ) && false == $objOutboundCallFilter->getIsAllPropertiesSelected() ) {
				$arrstrConditions[] = ' oc.property_id IN ( ' . $objOutboundCallFilter->getPropertyIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallTypeIds() ) ) {
				$arrstrConditions[] = ' oc.call_type_id = ' . ( int ) $objOutboundCallFilter->getCallTypeIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallTypeIds() ) ) {
				$arrstrConditions[] = ' oc.call_type_id IN ( ' . $objOutboundCallFilter->getCallTypeIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallResultIds() ) ) {
				$arrstrConditions[] = ' ocr.call_result_id = ' . ( int ) $objOutboundCallFilter->getCallResultIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallResultIds() ) ) {
				$arrstrConditions[] = ' ocr.call_result_id IN ( ' . $objOutboundCallFilter->getCallResultIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getOutboundCallStatusTypeIds() ) ) {
				$arrstrConditions[] = ' oc.outbound_call_status_type_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallStatusTypeIds();
			} elseif( true == valStr( $objOutboundCallFilter->getOutboundCallStatusTypeIds() ) ) {
				$arrstrConditions[] = ' oc.outbound_call_status_type_id IN ( ' . $objOutboundCallFilter->getOutboundCallStatusTypeIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallAgentIds() ) ) {
				$arrstrConditions[] = ' oc.call_agent_id = ' . ( int ) $objOutboundCallFilter->getCallAgentIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallAgentIds() ) ) {
				$arrstrConditions[] = ' oc.call_agent_id IN ( ' . $objOutboundCallFilter->getCallAgentIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getOutboundCallCampaignId() ) ) {
				$arrstrConditions[] = ' oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId();
			}

			if( true == valArr( $arrstrConditions ) ) {
				$strSql .= ' AND ' . implode( ' AND ', $arrstrConditions );
			}
		}

		if( false == $boolCountOnly ) {
			$strSql .= ' GROUP BY
							oc.id,
							oc.cid,
							oc.property_id,
							oc.customer_id,
							oc.lease_id,
							oc.company_employee_id,
							oc.call_queue_id,
							oc.outbound_call_status_type_id,
							oc.call_agent_id,
							oc.call_type_id,
							oc.outbound_call_campaign_id,
							oc.phone_number_type_id,
							oc.task_id,
							oc.task_note_id,
							oc.company_user_score_id,
							oc.receipient_name,
							oc.initiated_on,
							oc.phone_number,
							oc.dont_call_after,
							oc.lock_sequence,
							oc.assigned_on,
							oc.start_time,
							oc.end_time,
							oc.locked_on,
							oc.email_parsed_on,
							oc.updated_by,
							oc.updated_on,
							oc.created_by,
							oc.created_on,
							occ.name,
							ocst.name,
							c.call_datetime,
							c.call_notes
						ORDER BY
							oc.id DESC ';
		}

		$strSql .= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchPaginatedPendingOutboundCallsByOutboundCallFilter( $objPagination, $objOutboundCallFilter, $objVoipDatabase, $boolCountOnly = false, $boolIsOutboundCallList = false ) {
		$arrstrConditions	= array();
		$strSqlOffsetLimit	= '';

		if( true == $boolCountOnly ) {

			$strSql = ' SELECT
							oc.id,
							CASE
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -1000 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Pacific/Honolulu\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -900 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Anchorage\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -800 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Los_Angeles\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -700 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Denver\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -600 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Chicago\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -500 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/New_York\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -400 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Atlantic\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -350 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Newfoundland\' ) )
								ELSE NULL
							END AS property_time_hour
						FROM
							outbound_calls AS oc
							LEFT JOIN calls AS c ON ( c.outbound_call_id = oc.id ) ';

			if( false == is_null( $objOutboundCallFilter->getCallResultIds() ) ) {
				$strSql .= ' LEFT JOIN outbound_call_results AS ocr ON ( ocr.outbound_call_id = oc.id ) ';
			}

			if( true == $objOutboundCallFilter->getIsAllPropertiesSelected() || true == $boolIsOutboundCallList ) {
				$strSql .= 'LEFT JOIN property_call_settings AS pcs ON (oc.property_id = pcs.property_id)';
			}

			$strSql .= ' WHERE 1 = 1 ';
		} else {
			$strSql = 'SELECT
							oc.*,
							MAX( c.call_status_type_id ) AS call_status_type_id,
							CASE
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -1000 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Pacific/Honolulu\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -900 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Anchorage\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -800 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Los_Angeles\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -700 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Denver\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -600 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/Chicago\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -500 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'America/New_York\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -400 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Atlantic\' ) )
								WHEN oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId() . ' AND MAX( pcs.timezone_offset ) = -350 THEN date_part ( \'hour\', ( NOW() AT TIME ZONE \'Canada/Newfoundland\' ) )
								ELSE NULL
							END AS property_time_hour
						FROM
							outbound_calls AS oc
							LEFT JOIN calls AS c ON ( c.outbound_call_id = oc.id )';

			if( false == is_null( $objOutboundCallFilter->getCallResultIds() ) ) {
				$strSql .= ' LEFT JOIN outbound_call_results AS ocr ON ( ocr.outbound_call_id = oc.id ) ';
			}

			if( true == $objOutboundCallFilter->getIsAllPropertiesSelected() || true == $boolIsOutboundCallList ) {
				$strSql .= 'LEFT JOIN property_call_settings AS pcs ON (oc.property_id = pcs.property_id)';
			}

			$strSql				.= ' WHERE 1 = 1 ';
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valObj( $objOutboundCallFilter, 'COutboundCallFilter' ) ) {

			if( true == valId( trim( $objOutboundCallFilter->getOutboundCallId() ) ) ) {
				$arrstrConditions[] = ' oc.id = ' . ( int ) $objOutboundCallFilter->getOutboundCallId();
			}

			if( true == valStr( $objOutboundCallFilter->getCids() ) ) {
				$arrstrConditions[] = ' oc.cid IN ( ' . $objOutboundCallFilter->getCids() . ' ) ';
			}

			if( true == is_numeric( $objOutboundCallFilter->getPropertyIds() ) ) {
				$arrstrConditions[] = ' oc.property_id = ' . ( int ) $objOutboundCallFilter->getPropertyIds();
			} elseif( true == valStr( $objOutboundCallFilter->getPropertyIds() ) && false == $objOutboundCallFilter->getIsAllPropertiesSelected() ) {
				$arrstrConditions[] = ' oc.property_id IN ( ' . $objOutboundCallFilter->getPropertyIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallTypeIds() ) ) {
				$arrstrConditions[] = ' oc.call_type_id = ' . ( int ) $objOutboundCallFilter->getCallTypeIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallTypeIds() ) ) {
				$arrstrConditions[] = ' oc.call_type_id IN ( ' . $objOutboundCallFilter->getCallTypeIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallResultIds() ) ) {
				$arrstrConditions[] = ' ocr.call_result_id = ' . ( int ) $objOutboundCallFilter->getCallResultIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallResultIds() ) ) {
				$arrstrConditions[] = ' ocr.call_result_id IN ( ' . $objOutboundCallFilter->getCallResultIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getCallAgentIds() ) ) {
				$arrstrConditions[] = ' oc.call_agent_id = ' . ( int ) $objOutboundCallFilter->getCallAgentIds();
			} elseif( true == valStr( $objOutboundCallFilter->getCallAgentIds() ) ) {
				$arrstrConditions[] = ' oc.call_agent_id IN ( ' . $objOutboundCallFilter->getCallAgentIds() . ' ) ';
			}

			if( true == valId( $objOutboundCallFilter->getOutboundCallCampaignId() ) ) {
				$arrstrConditions[] = ' oc.outbound_call_campaign_id = ' . ( int ) $objOutboundCallFilter->getOutboundCallCampaignId();
			} elseif( true == valStr( $objOutboundCallFilter->getOutboundCallCampaignIds() ) ) {
				$arrstrConditions[] = ' oc.outbound_call_campaign_id IN ( ' . $objOutboundCallFilter->getOutboundCallCampaignIds() . ' ) ';
			}

			if( true == valStr( $objOutboundCallFilter->getStartDate() ) && true == valStr( $objOutboundCallFilter->getEndDate() ) && true == $objOutboundCallFilter->valStartDate() && true == $objOutboundCallFilter->valEndDate() ) {
				$arrstrConditions[] = ' oc.created_on BETWEEN  \'' . $objOutboundCallFilter->getStartDate() . '\' AND \'' . $objOutboundCallFilter->getEndDate() . '\' :: DATE +  \' 1 DAY \' ::INTERVAL';
			}

			if( true == valId( trim( $objOutboundCallFilter->getCallPriorityId() ) ) ) {
				$arrstrConditions[] = ' oc.call_priority_id = ' . ( int ) $objOutboundCallFilter->getCallPriorityId();
			}

			if( true == valArr( $arrstrConditions ) ) {
				$strSql .= ' AND ' . implode( ' AND ', $arrstrConditions );
			}

			$strSql .= ' AND ( oc.outbound_call_status_type_id IN ( ' . $objOutboundCallFilter->getOutboundCallStatusTypeIds() . ' )
								OR ( oc.outbound_call_status_type_id = ' . COutboundCallStatusType::SUCCESS . ' AND  c.call_status_type_id IN ( ' . $objOutboundCallFilter->getCallStatusTypeIds() . ' ) ) )
						AND oc.phone_number IS NOT NULL
						AND ( oc.dont_call_after IS NULL
								OR oc.dont_call_after > NOW ( ) )';
		}

		if( false == $boolCountOnly ) {
			$strSql .= ' GROUP BY
							oc.id
						ORDER BY
							oc.call_priority_id DESC,
							oc.id ASC,
						MAX( c.id ) DESC';
		} else {
			$strSql .= ' GROUP BY
						oc.id';
		}

		if( COutboundCallCampaign::OUTBOUND_MOVE_IN == $objOutboundCallFilter->getOutboundCallCampaignId() ) {
			$intPropertyCallStartTimeAtNine		= 9;
			$intPropertyCallEndTimeAtTweleve	= 12;
			$intPropertyCallStartTimeAtTweleve	= 12;
			$intPropertyCallEndTimeAtSixteen	= 16;
			$intPropertyCallStartTimeAtSixteen	= 16;
			$intPropertyCallEndTimeAtNinteen	= 19;

			$strOutboundCallTimeCondition = ' ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtNine . '
												AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtTweleve . ' )
											OR ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtSixteen . '
												AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtNinteen . ' )
											OR ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTimeAtTweleve . '
												AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTimeAtSixteen . ' )';

		} else {
			$intPropertyCallStartTime	= 0;
			$intPropertyCallEndTime		= 24;

			$strOutboundCallTimeCondition = ' ( subquery.property_time_hour >= ' . ( int ) $intPropertyCallStartTime . '
												AND subquery.property_time_hour < ' . ( int ) $intPropertyCallEndTime . ' )';
		}

		if( true == $boolCountOnly ) {
			$strOutboundCallSelectData = ' COUNT( id ) as count ';
		} else {
			$strOutboundCallSelectData = ' * ';
		}

		$strSql = 'SELECT
						' . $strOutboundCallSelectData . '
					FROM
						( ' . $strSql . ' ) AS subquery
					WHERE ' . $strOutboundCallTimeCondition;

		$strSql .= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchOutboundCallByPhoneNumber( $strPhoneNumber, $objVoipDatabase ) {
		return self::fetchOutboundCall( 'SELECT *FROM outbound_calls WHERE phone_number = \'' . $strPhoneNumber . '\' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchOutboundCallsByIds( $arrintOutboundCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallIds ) ) return NULL;

		$strSql = 'SELECT * FROM outbound_calls WHERE id IN (' . implode( ', ', $arrintOutboundCallIds ) . ')';

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallByCallId( $intCallId, $objVoipDatabase ) {
		if( false == valId( $intCallId ) ) return NULL;

		$strSql = 'SELECT
						oc.*
					FROM
						outbound_calls AS oc
					JOIN calls As c ON ( c.outbound_call_id = oc.id )
					WHERE
						c.id = ' . ( int ) $intCallId . '
					LIMIT 1';

		return self::fetchOutboundCall( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallPropertiesByOutboundCallCampaignId( $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						property_id
					FROM
						outbound_calls
					WHERE
						outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId . '
					GROUP BY
						property_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsCountByOutboundCallCampaignId( $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						COUNT( id )
					FROM
						outbound_calls
					WHERE
						outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsCountCompletedByOutboundCallCampaignId( $intOutboundCallCampaignId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallCampaignId ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT( ocr.outbound_call_id ) )
					FROM
						outbound_calls oc
						JOIN outbound_call_results ocr ON( oc.id = ocr.outbound_call_id )
					WHERE
						oc.outbound_call_campaign_id = ' . ( int ) $intOutboundCallCampaignId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsCountForOutboundCallCampaignsByOutboundCallCampaignIds( $arrintOutboundCallCampaignIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallCampaignIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( oc.id ),
						occ.id AS outbound_call_campaign_id
					FROM
						outbound_calls oc
						JOIN outbound_call_campaigns occ ON ( oc.outbound_call_campaign_id = occ.id )
					WHERE
						occ.id IN( ' . implode( ',', $arrintOutboundCallCampaignIds ) . ' )
					GROUP BY
						occ.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsCountCompletedForOutboundCallCampaignsByOutboundCallCampaignIds( $arrintOutboundCallCampaignIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallCampaignIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( DISTINCT( ocr.outbound_call_id ) ),
						occ.id AS outbound_call_campaign_id
					FROM
						outbound_call_results ocr
						JOIN outbound_calls oc ON( ocr.outbound_call_id = oc.id )
						JOIN outbound_call_campaigns occ ON( oc.outbound_call_campaign_id = occ.id )
					WHERE
						occ.id IN( ' . implode( ',', $arrintOutboundCallCampaignIds ) . ' )
					GROUP BY
						occ.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleOutboundCallsCountForOutboundCallCampaignsByOutboundCallCampaignIdsByCallResultIds( $arrintOutboundCallCampaignIds, $arrintCallResultIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallCampaignIds ) || false == valArr( $arrintCallResultIds ) ) return NULL;

		$strSql = 'SELECT
						COUNT( ocr.outbound_call_id ),
						occ.id AS outbound_call_campaign_id
					FROM
						outbound_call_results ocr
						JOIN outbound_calls oc ON( ocr.outbound_call_id = oc.id )
						JOIN outbound_call_campaigns occ ON( oc.outbound_call_campaign_id = occ.id )
					WHERE
						occ.id IN( ' . implode( ',', $arrintOutboundCallCampaignIds ) . ' )
						AND ocr.call_result_id IN( ' . implode( ',', $arrintCallResultIds ) . ' )
					GROUP BY
						occ.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallsByCidByPropertyIdsByOutboundCallStatusTypeId( $intCid, $arrintPropertyIds, $intOutboundCallStatusTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id IN( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND outbound_call_status_type_id = ' . ( int ) $intOutboundCallStatusTypeId;

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallsByCidsByOutboundCallStatusTypeId( $arrintCid, $intOutboundCallStatusTypeId, $objVoipDatabase ) {
		if( false == valArr( $arrintCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						cid IN ( ' . implode( ',', $arrintCid ) . ' )
						AND outbound_call_status_type_id = ' . ( int ) $intOutboundCallStatusTypeId;

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallByOutboundCallId( $intOutboundCallId, $objVoipDatabase ) {
		if( false == valId( $intOutboundCallId ) ) return NULL;

		$strSql = 'SELECT
						oc.*
					FROM
						outbound_calls oc
					WHERE
						oc.id = ' . ( int ) $intOutboundCallId . '
						AND oc.call_agent_id is NULL
					LIMIT 1';

		return self::fetchOutboundCall( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallsByCidsByCallQueueIdsByOutboundCallStatusTypeId( $arrintCids, $arrintCallQueueIds, $intOutboundCallStatusTypeId, $objVoipDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						cid IN ( ' . implode( ',', $arrintCids ) . ' )
						AND call_queue_id IN ( ' . implode( ',', $arrintCallQueueIds ) . ' )
						AND outbound_call_status_type_id = ' . ( int ) $intOutboundCallStatusTypeId;

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallByTaskNoteId( $intTaskNoteId, $objVoipDatabase ) {
		if( false == valId( $intTaskNoteId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						task_note_id = ' . ( int ) $intTaskNoteId;

		return self::fetchOutboundCall( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallByCidByPropertyIdByOutboundCallStatusTypeIdByCallTypeIdByCallAgentId( $intCid, $intPropertyId, $intOutboundCallStatusTypeId, $intCallTypeId, $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id  = ' . ( int ) $intPropertyId . '
						AND outbound_call_status_type_id = ' . ( int ) $intOutboundCallStatusTypeId . '
						AND call_type_id = ' . ( int ) $intCallTypeId . '
						AND call_agent_id = ' . ( int ) $intCallAgentId;

		return self::fetchOutboundCall( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundCallsdByOutboundCallStatusTypeIdByCallTypeId( $intOutboundCallStatusTypeId, $intCallTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						outbound_calls
					WHERE
						outbound_call_status_type_id = ' . ( int ) $intOutboundCallStatusTypeId . '
						AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchOutboundCalls( $strSql, $objVoipDatabase );
	}

}
?>