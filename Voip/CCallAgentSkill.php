<?php

class CCallAgentSkill extends CBaseCallAgentSkill {

	protected $m_intCallQueueId;
	protected $m_intCallAgentId;
	protected $m_strCallAgentName;
	protected $m_strCallSkillName;
	protected $m_strCallQueueName;
	protected $m_strCallAgentExtension;
	protected $m_strCallQueueExtension;
	protected $m_strCallAgentQueuePosition;
	protected $m_strCallAgentDomain;
	protected $m_strCallQueueDomain;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_intPriority;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;

	const DEFAULT_PRIORITY = 5;
	const DEFAULT_POSITION = 1;

	/**
	 * set functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['call_queue_id'] ) )					$this->setCallQueueId( $arrValues['call_queue_id'] );
		if( true == isset( $arrValues['call_agent_id'] ) )					$this->setCallAgentId( $arrValues['call_agent_id'] );
		if( true == isset( $arrValues['call_agent_name'] ) )				$this->setCallAgentName( $arrValues['call_agent_name'] );
		if( true == isset( $arrValues['call_skill_name'] ) )				$this->setCallSkillName( $arrValues['call_skill_name'] );
		if( true == isset( $arrValues['call_queue_name'] ) )				$this->setCallQueueName( $arrValues['call_queue_name'] );
		if( true == isset( $arrValues['agent_phone_extension_id'] ) )		$this->setAgentPhoneExtensionId( $arrValues['agent_phone_extension_id'] );
		if( true == isset( $arrValues['physical_phone_extension_id'] ) )	$this->setPhysicalPhoneExtensionId( $arrValues['physical_phone_extension_id'] );
		if( true == isset( $arrValues['call_agent_domain'] ) )				$this->setCallAgentDomain( $arrValues['call_agent_domain'] );
		if( true == isset( $arrValues['call_queue_domain'] ) )				$this->setCallQueueDomain( $arrValues['call_queue_domain'] );
		if( true == isset( $arrValues['call_agent_queue_position'] ) )		$this->setCallAgentQueuePosition( $arrValues['call_agent_queue_position'] );
		if( true == isset( $arrValues['call_agent_state_type_name'] ) )		$this->setCallAgentStateTypeName( $arrValues['call_agent_state_type_name'] );
		if( true == isset( $arrValues['call_agent_status_type_name'] ) )	$this->setCallAgentStatusTypeName( $arrValues['call_agent_status_type_name'] );
		if( true == isset( $arrValues['priority'] ) )						$this->setPriority( $arrValues['priority'] );

		return;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setCallSkillName( $strCallSkillName ) {
		$this->m_strCallSkillName = $strCallSkillName;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallAgentExtension( $strCallAgentExtension ) {
		$this->m_strCallAgentExtension = $strCallAgentExtension;
	}

	public function setCallQueueExtension( $strCallQueueExtension ) {
		$this->m_strCallQueueExtension = $strCallQueueExtension;
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->m_intAgentPhoneExtensionId = $intAgentPhoneExtensionId;
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->m_intPhysicalPhoneExtensionId = $intPhysicalPhoneExtensionId;
	}

	public function setCallAgentQueuePosition( $strCallAgentQueuePosition ) {
		$this->m_strCallAgentQueuePosition = $strCallAgentQueuePosition;
	}

	public function setCallAgentDomain( $strCallAgentDomain ) {
		$this->m_strCallAgentDomain = $strCallAgentDomain;
	}

	public function setCallQueueDomain( $strCallQueueDomain ) {
		$this->m_strCallQueueDomain = $strCallQueueDomain;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	public function setPriority( $intPriority ) {
		$this->m_intPriority = $intPriority;
	}

	/**
	 * get functions
	 */

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getCallSkillName() {
		return $this->m_strCallSkillName;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallAgentExtension() {
		return $this->m_strCallAgentExtension;
	}

	public function getCallQueueExtension() {
		return $this->m_strCallQueueExtension;
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function getCallAgentQueuePosition() {
		return $this->m_strCallAgentQueuePosition;
	}

	public function getCallAgentDomain() {
		return $this->m_strCallAgentDomain;
	}

	public function getCallQueueDomain() {
		return $this->m_strCallQueueDomain;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function getBuildCallQueueName() {
		return $strCallQueueName = $this->getCallQueueExtension() . CCallHelper::SIP_DOMAIN_NAME;
	}

	public function getBuildCallAgentName() {
		return $strCallAgentName = $this->getCallAgentExtension() . CCallHelper::SIP_DOMAIN_NAME;
	}

	/**
	 * validate functions
	 */

	public function valCallAgentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valCallSkillId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCallSkillId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_skill_id', 'Call skill not provided.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case 'validate_call_agent_skill':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valCallSkillId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function createCallAgentSkill() {
		$objCallAgentSkill = new CCallAgentSkill();
		$objCallAgentSkill->setDefaults();
		return $objCallAgentSkill;
	}

}
?>