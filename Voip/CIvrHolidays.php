<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrHolidays
 * Do not add any new functions to this class.
 */

class CIvrHolidays extends CBaseIvrHolidays {

	public static function fetchActiveIvrHolidaysByIvrId( $intIvrId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						i.*,
						ih.*
					FROM
						ivrs AS i
					JOIN ivr_holidays AS ih ON ( ih.ivr_id = i.id )
					WHERE
						i.id = ' . ( int ) $intIvrId . '
					AND is_published = true';

		return self::fetchIvrHolidays( $strSql, $objVoipDatabase );
	}

	public static function fetchAllIvrHolidaysByIvrId( $intIvrId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM ivr_holidays  WHERE ivr_id = ' . ( int ) $intIvrId . ' ORDER BY holiday_date ASC';
		return self::fetchIvrHolidays( $strSql, $objVoipDatabase );
	}
}
?>