<?php

class CCallServer extends CBaseCallServer {

	protected $m_strServerTypeName;
	protected $m_strOldPassword;
	protected $m_strNewPassword;
	protected $m_strConfirmPassword;
	protected $m_boolIsPasswordChanged;

	const OUTBOUND_CALL			= 'is_outbound_call';
	const INBOUND_CALL			= 'is_inbound_call';
	const SCHEDULED_CALL		= 'is_scheduled_call';
	const CALL_SHADOW			= 'is_call_shadow';
	const VOICEMAIL				= 'is_voicemail';
	const PRIMARY_SERVER		= 'primary_server';

	public static $c_arrstrModuleFlags = [
		self::OUTBOUND_CALL => 'Outbound Call',
		self::INBOUND_CALL => 'Inbound Call',
		self::SCHEDULED_CALL => 'Scheduled Call',
		self::CALL_SHADOW => 'Call Shadow',
		self::VOICEMAIL => 'Voicemail'
	];

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['server_type_name'] ) )	$this->setServerTypeName( $arrmixValues['server_type_name'] );
		if( true == isset( $arrmixValues['old_password'] ) )		$this->setOldPassword( $arrmixValues['old_password'] );
		if( true == isset( $arrmixValues['new_password'] ) )		$this->setNewPassword( $arrmixValues['new_password'] );
		if( true == isset( $arrmixValues['confirm_password'] ) )	$this->setConfirmPassword( $arrmixValues['confirm_password'] );
		if( true == isset( $arrmixValues['is_password_changed'] ) )	$this->setIsPasswordChanged( $arrmixValues['is_password_changed'] );
		return true;
	}

	public function setServerTypeName( $strServerTypeName ) {
		$this->m_strServerTypeName = $strServerTypeName;
	}

	public function setOldPassword( $strOldPassword ) {
		$this->m_strOldPassword = $strOldPassword;
	}

	public function setNewPassword( $strNewPassword ) {
		$this->m_strNewPassword = $strNewPassword;
	}

	public function setConfirmPassword( $strConfirmPassword ) {
		$this->m_strConfirmPassword = $strConfirmPassword;
	}

	public function setIsPasswordChanged( $boolIsPasswordChanged ) {
		$this->m_boolIsPasswordChanged = $boolIsPasswordChanged;
	}

	public function getServerTypeName() {
		return $this->m_strServerTypeName;
	}

	public function getOldPassword() {
		return $this->m_strOldPassword;
	}

	public function getNewPassword() {
		return $this->m_strNewPassword;
	}

	public function getConfirmPassword() {
		return $this->m_strConfirmPassword;
	}

	public function getIsPasswordChanged() {
		return $this->m_boolIsPasswordChanged;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valServerName() {
		$boolIsValid = true;

		if( false == valStr( $this->getServerName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'server_name', 'Server Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress() {
		$boolIsValid = true;

		if( false == CValidation::validateIpAddress( $this->getIpAddress() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'Invalid IP Address.' ) );
		}

		return $boolIsValid;
	}

	public function valDnsName() {
		$boolIsValid = true;

		if( false == valStr( $this->getDnsName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dns_name', 'DNS name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valServerTypeId() {
		$boolIsValid = true;

		if( false == ValId( $this->getCallServerTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'server_type_id', 'Server Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPrimary() {
		$boolIsValid = true;

		if( true == $this->getIsPrimary() && false == $this->getIsActive() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_primary', 'Server should be active to make as a Primary.' ) );
		}

		return $boolIsValid;
	}

	public function valUserName() {
		$boolIsValid = true;

		if( false == valStr( $this->getUsername() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;
		if( true == filter_var( $this->getIsPasswordChanged(), FILTER_VALIDATE_BOOLEAN ) ) {
			if( false == valStr( $this->getPassword() ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password is required.' ) );
			}

			if( $this->getPassword() != $this->getConfirmPassword() ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password and confirm password not match.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valOldPassword() {
		$boolIsValid = true;

		if( false == valStr( $this->getOldPassword() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_password', 'Old password is required.' ) );
		}

		if( $this->getPassword() != $this->getOldPassword() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Invalid old password.' ) );
		}

		return $boolIsValid;
	}

	public function valNewPassword() {
		$boolIsValid = true;

		if( false == valStr( $this->getNewPassword() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_password', 'New password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valConfirmPassword() {
		$boolIsValid = true;

		if( false == valStr( $this->getConfirmPassword() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Confirm password is required.' ) );
		}

		if( $this->getNewPassword() != $this->getConfirmPassword() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'confirm_password', 'Password and confirm password not match.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valServerName();
				$boolIsValid &= $this->valIpAddress();
				$boolIsValid &= $this->valDnsName();
				$boolIsValid &= $this->valServerTypeId();
				$boolIsValid &= $this->valIsPrimary();
				$boolIsValid &= $this->valUserName();
				$boolIsValid &= $this->valPassword();
				break;

			case 'validate_reset_password':
				$boolIsValid &= $this->valOldPassword();
				$boolIsValid &= $this->valNewPassword();
				$boolIsValid &= $this->valConfirmPassword();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function getDecryptedUsername() {
		return ( true == valStr( $this->m_strUsername ) ) ? CEncryption::decrypt( $this->m_strUsername, CONFIG_KEY_FREESWITCH_USERNAME ) : NULL;
	}

	public function getDecryptedPassword() {
		return ( true == valStr( $this->m_strPassword ) ) ? CEncryption::decrypt( $this->m_strPassword, CONFIG_KEY_FREESWITCH_PASSWORD ) : NULL;
	}

}
?>