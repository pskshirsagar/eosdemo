<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallDownloadLogs
 * Do not add any new functions to this class.
 */

class CCallDownloadLogs extends CBaseCallDownloadLogs {

	public static function fetchPeginatedCallDownloadLogsByCidByCreatedBy( $intCid, $intCompanyUserId, $objPagination, $objVoipDatabase, $intMonthCount, $boolIsCountOnly = false ) {
		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT
							COUNT( cdl.id )
						FROM call_download_logs cdl ';
		} else {
			$strSql = 'SELECT
							cdl.*,
							cf.properties,
							cf.call_types,
							cf.call_results,
							cf.lead_source_ids,
							cf.applicant_application_ids,
							cf.start_date,
							cf.end_date
						FROM call_download_logs cdl ';
		}

		$strSql .= 'JOIN call_filters cf ON ( cf.id = cdl.call_filter_id )';

		$strSql .= ' WHERE
						cdl.cid = ' . ( int ) $intCid . '
						AND cdl.created_by = ' . ( int ) $intCompanyUserId . '
						AND cdl.file_name IS NOT NULL
						AND cdl.file_path IS NOT NULL
						AND cdl.created_on > CURRENT_DATE - INTERVAL \'' . $intMonthCount . ' months\'';

		if( false == $boolIsCountOnly && true == valId( $objPagination->getPageNo() ) && true == valId( $objPagination->getPageSize() ) ) {
			$intLimit 	= ( int ) $objPagination->getPageSize();
			$strSql .= ' ORDER BY cdl.id DESC OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $intLimit;
		}
		$arrstrData = fetchData( $strSql, $objVoipDatabase );
		if( true == $boolIsCountOnly ) {
			return $arrstrData[0]['count'];
		} else {
			return $arrstrData;
		}
	}

}
?>