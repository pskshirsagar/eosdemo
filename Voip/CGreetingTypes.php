<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CGreetingTypes
 * Do not add any new functions to this class.
 */

class CGreetingTypes extends CBaseGreetingTypes {

	public static function fetchGreetingTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CGreetingType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchGreetingType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CGreetingType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOrderedGreetingTypes( $objVoipDatbase ) {
		return self::fetchGreetingTypes( 'SELECT * FROM greeting_types ORDER BY name', $objVoipDatbase );
	}

	public static function fetchGreetingTypesByGreetingTypeIds( $arrintGreetingTypeIds, $objVoipDatabase ) {
		return self::fetchGreetingTypes( 'SELECT *FROM greeting_types WHERE id IN ( ' . implode( ',', $arrintGreetingTypeIds ) . ' ) ORDER BY name', $objVoipDatabase );
	}

}
?>