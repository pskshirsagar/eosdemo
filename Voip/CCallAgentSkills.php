<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentSkills
 * Do not add any new functions to this class.
 */

class CCallAgentSkills extends CBaseCallAgentSkills {

	public static function fetchActiveCallAgentSkillsByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cas.*,
						ca.full_name AS call_agent_name,
						cs.name AS call_skill_name,
						csq.priority AS priority
					FROM
						call_agent_skills AS cas
						JOIN call_agents ca ON ( ca.id = cas.call_agent_id )
						JOIN call_skills cs ON ( cs.id = cas.call_skill_id )
						LEFT JOIN call_skill_queues csq ON ( cas.call_skill_id = csq.call_skill_id )
					WHERE
						cas.call_agent_id = ' . ( int ) $intCallAgentId . '
						AND cs.is_published = true
					ORDER BY cs.name';

		return self::fetchCallAgentSkills( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentSkillsByCallAgentIds( $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						cas.*,
						ca.full_name AS call_agent_name,
						cs.name AS call_skill_name
					FROM
						call_agent_skills AS cas
						JOIN call_agents AS ca ON ( ca.id = cas.call_agent_id )
						JOIN call_skills AS cs ON ( cs.id = cas.call_skill_id )
					WHERE
						cas.call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return self::fetchCallAgentSkills( $strSql, $objVoipDatabase );
	}

}
?>