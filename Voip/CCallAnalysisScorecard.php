<?php

class CCallAnalysisScorecard extends CBaseCallAnalysisScorecard {

	protected $m_arrobjCallAnalysisCategories;

	const CLIENT_ANALYSIS_GRADES = [
		1 => 'Both',
		2 => 'Warm',
		3 => 'Cold'
	];

	/**
	 * Getter functions.
	 */

	public function getCallAnalysisCategories() {
		return $this->m_arrobjCallAnalysisCategories;
	}

	public function setCallAnalysisCategories( $arrobjCallAnalysisCategories ) {
		$this->m_arrobjCallAnalysisCategories = $arrobjCallAnalysisCategories;
	}

 	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Scorecard title is required.' ) ) );
		}

		return $boolIsValid;
	}

 	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', __( 'Description is required.' ) ) );
		}

		return $boolIsValid;
	}

 	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createCallAnalysisScorecardPropertyGroup() {
		$objCallAnalysisScorecardPropertyGroup = new CCallAnalysisScorecardPropertyGroup();
		$objCallAnalysisScorecardPropertyGroup->setCallAnalysisScorecardId( $this->getId() );
		$objCallAnalysisScorecardPropertyGroup->setCid( $this->getCid() );

		return $objCallAnalysisScorecardPropertyGroup;
	}

	public function createCallAnalysisCategory() {
		$objCallAnalysisCategory = new CCallAnalysisCategory();
		$objCallAnalysisCategory->setCallAnalysisScorecardId( $this->getId() );
		$objCallAnalysisCategory->setCid( $this->getCid() );

		return $objCallAnalysisCategory;
	}

	public function createOrFetchCallAnalysisCategory( $arrstrCategory, $objDatabase ) {

		if( true == valId( $arrstrCategory['category_id'] ) ) {
			$objCallAnalysisCategory = CCallAnalysisCategories::fetchCallAnalysisCategoryByCidById( $this->getCid(), $arrstrCategory['category_id'], $objDatabase );
		} else {
			$objCallAnalysisCategory = new CCallAnalysisCategory();
			$objCallAnalysisCategory->setCallAnalysisScorecardId( $this->getId() );
			$objCallAnalysisCategory->setCid( $this->getCid() );
		}
		return $objCallAnalysisCategory;
	}

	public function validatePossiblePoints( $arrstrScorecardData ) {

		$boolIsValid = true;
		$intTotalPossiblePoints = 0;
		if( true == valArr( $arrstrScorecardData ) ) {
			foreach( $arrstrScorecardData as $arrstrCategory ) {
				if( true == valArr( $arrstrCategory['questions'] ) ) {
					foreach( $arrstrCategory['questions'] as $arrstrQuestion ) {
						if( true == valArr( $arrstrQuestion['answers'] ) && CCallAnalysisQuestionType::MULTIPLE_CHOICE == $arrstrQuestion['question_type'] ) {
							$arrintPoints = [];
							foreach( $arrstrQuestion['answers'] as $arrstrAnswer ) {
								if( NULL == $arrstrAnswer['points'] ) {
									$boolIsValid = false;
									$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'points', 'Points are required.' ) );
									return $boolIsValid;
								}
								$arrintPoints[] = $arrstrAnswer['points'];
							}
							if( \Psi\Libraries\UtilFunctions\count( $arrintPoints ) != \Psi\Libraries\UtilFunctions\count( array_unique( $arrintPoints ) ) ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'points', __( 'Duplicate points for same question is not allowed.' ) ) );
							}
							$intTotalPossiblePoints += max( $arrintPoints );
						}
					}
				}
			}
		}
		if( 100 < $intTotalPossiblePoints ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'points', __( 'Total scorecard points should be less than or equal to 100.' ) ) );
		}

		return $boolIsValid;
	}

	public function fetchCallAnalysisCategories( $objVoipDatabase ) {
		return $this->m_arrobjCallAnalysisCategories = CCallAnalysisCategories::fetchCallAnalysisCategoriesDetailsByCallAnalysisScorecardIdByCid( $this->getId(), $this->getCid(), $objVoipDatabase );
	}

}
?>