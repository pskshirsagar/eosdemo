<?php

class CPropertyCallHoliday extends CBasePropertyCallHoliday {

	protected $m_boolIsPublished;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Getter Functions.
	 */

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	/*
	 * Setter Functions.
	 */

	public function setIsPublished( $boolIsPublished ) {
		$this->m_boolIsPublished = $boolIsPublished;
	}

	/*
	 * Overridden Functions.
	 */

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->m_boolIsPublished && true == $this->delete( $intCurrentUserId, $objDatabase ) ) {
			return true;
		}

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>