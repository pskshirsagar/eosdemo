<?php

class CCallAnalysisCallDisputeCategory extends CBaseCallAnalysisCallDisputeCategory {

	protected $m_strCallAnalysisCategoryName;

	/**
	 * set functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_analysis_category_name'] ) )            $this->setCallAnalysisCategoryName( $arrmixValues['call_analysis_category_name'] );
		return true;
	}

	public function setCallAnalysisCategoryName( $strCallAnalysisCategoryName ) {
		$this->m_strCallAnalysisCategoryName = $strCallAnalysisCategoryName;
	}

	/**
	 * get functions
	 *
	 */

	public function getCallAnalysisCategoryName() {
		return $this->m_strCallAnalysisCategoryName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAnalysisCallDisputeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAnalysisCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>