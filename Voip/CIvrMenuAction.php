<?php

class CIvrMenuAction extends CBaseIvrMenuAction {

	protected $m_strIvrActionName;
	protected $m_strIvrActionDescription;
	protected $m_strIvrMenuActionHandler;
	protected $m_strIvrMenuActionParams;
	protected $m_strIvrSubMenuName;
	protected $m_objIvrMenu;
	protected $m_objSubIvrMenu;
	protected $m_strGreetingFileName;
	protected $m_strGreetingFilePath;

	/**
	 * Get Functions
	 */

	public function getIvrActionName() {
		return $this->m_strIvrActionName;
	}

	public function getIvrActionDescription() {
		return $this->m_strIvrActionDescription;
	}

	public function getIvrMenu() {
		return $this->m_objIvrMenu;
	}

	public function getSubIvrMenu() {
		return $this->m_objSubIvrMenu;
	}

	public function getIvrMenuActionHandler() {
		return $this->m_strIvrMenuActionHandler;
	}

	public function getIvrMenuActionParams() {
		return $this->m_strIvrMenuActionParams;
	}

	public function getIvrSubMenuName() {
		return $this->m_strIvrSubMenuName;
	}

	public function getGreetingFileName() {
		return $this->m_strGreetingFileName;
	}

	public function getGreetingFilePath() {
		return $this->m_strGreetingFilePath;
	}

	public function getGreetingFullFilePath() {
		return CCallFile::FREESWITCH_VOIP_MOUNTS . $this->getGreetingFilePath() . $this->getGreetingFileName();
	}

	/*
	 * Display Logical functions.
	 */

	public function getIsShowDefaultCallResult() {
		$boolIsShowDefaultCallResult = true;

		switch( $this->getIvrActionId() ) {
			case CIvrAction::SEND_TO_VOICEMAIL:
				$boolIsShowDefaultCallResult = false;
				break;

			case CIvrAction::RETURN_TO_PRIMARY_MENU:
				$boolIsShowDefaultCallResult = false;
				break;

			case CIvrAction::REPEAT_OPTIONS:
				$boolIsShowDefaultCallResult = false;
				break;

			case CIvrAction::SEND_TO_SECONDARY_MENU:
				$boolIsShowDefaultCallResult = false;
				break;

			default:
				$boolIsShowDefaultCallResult = true;
				break;
		}

		return $boolIsShowDefaultCallResult;
	}

	public function getIsDisabledCallResult() {
		$boolIsDisabledCallResult = true;

		switch( $this->getIvrActionId() ) {
			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER:
				$boolIsDisabledCallResult = false;
				break;

			case CIvrAction::FORWARD_TO_INTERNAL_NUMBER:
				$boolIsDisabledCallResult = false;
				break;

			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING:
				$boolIsDisabledCallResult = false;
				break;

			case CIvrAction::FORWARD_AS_PROPERTY_OFFICE_NUMBER:
				$boolIsDisabledCallResult = false;
				break;

			case CIvrAction::SEND_TO_RECORDING:
				$boolIsDisabledCallResult = false;
				break;

			default:
				// default case.
				break;
		}

		return $boolIsDisabledCallResult;
	}

	public function getIsLeadIvrAction() {
		return in_array( $this->getIvrActionId(), CIvrAction::$c_arrintLeadIvrActions );
	}

	public function getIsResidentIvrAction() {
		return in_array( $this->getIvrActionId(), CIvrAction::$c_arrintResidentIvrActions );
	}

	public function getDefaultCallResultId() {
		if( false == valId( $this->getCallResultId() ) ) {
			if( true == $this->getIsLeadIvrAction() ) {
				return CCallResult::LEAD;
			} elseif( true == $this->getIsResidentIvrAction() ) {
				return CCallResult::RESIDENT;
			}
		}
		return $this->m_intCallResultId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['ivr_action_name'] ) )				$this->setIvrActionName( $arrmixValues['ivr_action_name'] );
		if( true == isset( $arrmixValues['ivr_action_description'] ) )		$this->setIvrActionDescription( $arrmixValues['ivr_action_description'] );
		if( true == isset( $arrmixValues['ivr_menu_action_handler'] ) )		$this->setIvrMenuActionHandler( $arrmixValues['ivr_menu_action_handler'] );
		if( true == isset( $arrmixValues['ivr_menu_action_params'] ) )		$this->setIvrMenuActionParams( $arrmixValues['ivr_menu_action_params'] );
		if( true == isset( $arrmixValues['ivr_sub_menu_name'] ) )			$this->setIvrSubMenuName( $arrmixValues['ivr_sub_menu_name'] );
		if( true == isset( $arrmixValues['greeting_file_name'] ) )			$this->setGreetingFileName( $arrmixValues['greeting_file_name'] );
		if( true == isset( $arrmixValues['greeting_file_path'] ) )			$this->setGreetingFilePath( $arrmixValues['greeting_file_path'] );
		return;
	}

	public function setIvrActionName( $strIvrActionName ) {
		$this->m_strIvrActionName = $strIvrActionName;
	}

	public function setIvrActionDescription( $strIvrActionDescription ) {
		$this->m_strIvrActionDescription = $strIvrActionDescription;
	}

	public function setIvrMenu( $objIvrMenu ) {
		$this->m_objIvrMenu = $objIvrMenu;
	}

	public function setSubIvrMenu( $objSubIvrMenu ) {
		$this->m_objSubIvrMenu = $objSubIvrMenu;
	}

	public function setIvrMenuActionHandler( $strIvrMenuActionHandler ) {
		$this->m_strIvrMenuActionHandler = $strIvrMenuActionHandler;
	}

	public function setIvrMenuActionParams( $strIvrMenuActionParams ) {
		$this->m_strIvrMenuActionParams = $strIvrMenuActionParams;
	}

	public function setIvrSubMenuName( $strIvrSubMenuName ) {
		$this->m_strIvrSubMenuName = $strIvrSubMenuName;
	}

	public function setGreetingFileName( $strGreetingFileName ) {
		$this->m_strGreetingFileName = $strGreetingFileName;
	}

	public function setGreetingFilePath( $strGreetingFilePath ) {
		$this->m_strGreetingFilePath = $strGreetingFilePath;
	}

	public function setActionParameter( $strActionParameter ) {
		switch( $this->getIvrActionId() ) {
			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING:
			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER:
			case CIvrAction::FORWARD_TO_INTERNAL_NUMBER:
				$this->m_strActionParameter = $strActionParameter;
				break;

			case CIvrAction::SEND_TO_RECORDING:
				$this->m_strActionParameter = $strActionParameter;
				break;

			default:
				// default case
				break;
		}
	}

	/**
	 * Validate Functions.
	 */

	public function valIvrMenuId() {
		$boolIsValid = true;

		if( false == valId( $this->getIvrMenuId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ivr_menu_id', __( 'Ivr menu id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIvrActionId() {
		$boolIsValid = true;

		if( false == valId( $this->getIvrActionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ivr_action_id', __( 'Action 1 is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valGreetingId() {
		$boolIsValid = true;

		if( false == valId( $this->getGreetingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'greeting_id', __( 'Record message is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valActionKey() {
		$boolIsValid = true;

		if( false == valStr( $this->getActionKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'action_key', __( 'Failed to associate appropriate action key, please try after some time.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objVoipDatabase = NULL, $boolIsValidateForNameDuplicate = true ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Title is required.' ) ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9\s]+$/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Title should not contain any special characters.' ) ) );
		} elseif( true == $boolIsValidateForNameDuplicate && true == $this->validateIsNameDuplicate( $objVoipDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Ivr options title should be unique.' ) ) );
		}

		return $boolIsValid;
	}

	public function valActionParameter( $objVoipDatabase ) {
		$boolIsValid = true;

		if( true == valId( $this->getIvrActionId() ) ) {
			if( ( CIvrAction::FORWARD_TO_EXTERNAL_NUMBER == $this->getIvrActionId() || CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING == $this->getIvrActionId() ) ) {
				if( false == CValidation::validateFullPhoneNumber( $this->getActionParameter(), false ) || 10 != \Psi\CStringService::singleton()->strlen( $this->getActionParameter() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please enter valid 10 digit external number.' ) ) );
				} else {
					$objCallPhoneNumber = \Psi\Eos\Voip\CCallPhoneNumbers::createService()->fetchCallPhoneNumberByPhoneNumber( $this->getActionParameter(), $objVoipDatabase );
					if( true == valObj( $objCallPhoneNumber, 'CCallPhoneNumber' ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Vanity numbers cannot be used in this field. Please enter another phone number.' ) ) );
						$boolIsValid = false;
					}
				}
			} elseif( CIvrAction::FORWARD_TO_INTERNAL_NUMBER == $this->getIvrActionId() ) {
				if( false == CValidation::validateFullPhoneNumber( $this->getActionParameter(), false ) || 10 != \Psi\CStringService::singleton()->strlen( $this->getActionParameter() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Please select the internal number.' ) ) );
				}
			} elseif( CIvrAction::SEND_TO_RECORDING == $this->getIvrActionId() && false == valStr( $this->getActionParameter() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Action 2 record message is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valCallResultId() {
		$boolIsValid = true;

		if( ( CIvrAction::FORWARD_TO_EXTERNAL_NUMBER == $this->getIvrActionId() || CIvrAction::FORWARD_TO_INTERNAL_NUMBER == $this->getIvrActionId() || CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING == $this->getIvrActionId() || CIvrAction::FORWARD_AS_PROPERTY_OFFICE_NUMBER == $this->getIvrActionId() || CIvrAction::SEND_TO_RECORDING == $this->getIvrActionId() ) && false == valId( $this->getCallResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call result id', __( 'Please select default result.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL, $boolIsValidateForNameDuplicate = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objVoipDatabase, $boolIsValidateForNameDuplicate );
				$boolIsValid &= $this->valGreetingId();
				$boolIsValid &= $this->valIvrActionId();
				$boolIsValid &= $this->valActionKey();
				$boolIsValid &= $this->valActionParameter( $objVoipDatabase );
				$boolIsValid &= $this->valCallResultId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function buildActionParameter() {
		$strActionParameter = '';

		switch( $this->getIvrActionId() ) {
			case CIvrAction::FORWARD_AS_LEAD:
			case CIvrAction::FORWARD_AS_LEAD_SPANISH:
			case CIvrAction::FORWARD_AS_RESIDENT:
			case CIvrAction::FORWARD_AS_RESIDENT_SPANISH:
			case CIvrAction::FORWARD_AS_PROPERTY_OFFICE_NUMBER:
			case CIvrAction::FORWARD_AS_PROPERTY_LEAD_NUMBER:
			case CIvrAction::FORWARD_AS_PROPERTY_MAINTENANCE_NUMBER:
			case CIvrAction::FORWARD_AS_PROPERTY_MAINTENANCE_EMERGENCY_NUMBER:
			case CIvrAction::SEND_TO_VOICEMAIL:
				$strActionParameter = $this->getIvrMenuActionParams();
				break;

			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER:
			case CIvrAction::FORWARD_TO_INTERNAL_NUMBER:
			case CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING:
				$strActionParameter = $this->getIvrMenuActionParams() . ' ' . $this->getActionParameter();
				break;

			case CIvrAction::SEND_TO_RECORDING:
				$strActionParameter = $this->getIvrMenuActionParams() . ' ' . CCallFile::FREESWITCH_VOIP_MOUNTS . $this->getActionParameter();
				break;

			case CIvrAction::REPEAT_OPTIONS:
			case CIvrAction::RETURN_TO_PRIMARY_MENU:
				break;

			case CIvrAction::SEND_TO_SECONDARY_MENU:
				$strActionParameter = $this->getIvrSubMenuName();
				break;

			default:
				// default case.
				break;
		}

		return $strActionParameter;
	}

	/**
	 *  Custom functions.
	 */

	public function validateIsNameDuplicate( $objVoipDatabase ) {
		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) return false;

		$strWhere = ' WHERE cid = ' . ( int ) $this->getCid() . ' AND ivr_menu_id = ' . ( int ) $this->getIvrMenuId() . ' AND LOWER( name ) = LOWER( ' . $this->sqlName() . ' )';

		if( true == valId( $this->getId() ) ) $strWhere .= ' AND id <> ' . ( int ) $this->getId();

		if( true == valId( CIvrMenuActions::fetchIvrMenuActionCount( $strWhere, $objVoipDatabase ) ) ) {
			return true;
		}

		return false;
	}

}
?>