<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSystemLogs
 * Do not add any new functions to this class.
 */

class CCallSystemLogs extends CBaseCallSystemLogs {

	public static function fetchSimpleCallLogsByCreatedOn( $strStartDate, $strEndDate, $objVoipDatabase, $boolIsCachedData = NULL ) {
		if( true == is_null( $strStartDate ) || true == is_null( $strEndDate ) ) return NULL;

		$strSql = 'SELECT 
						call_stats,
						TO_CHAR(date_trunc(\'hour\', range), \'HH:MI AM\') AS start_time,
						created_on
					FROM 
						(
							SELECT call_stats,
								created_on 
							FROM 
								call_system_logs
							WHERE 
								call_component_id IS NULL 
								AND created_on BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
							ORDER BY 
								created_on ASC
						) data
					RIGHT JOIN generate_series( \'' . $strStartDate . '\', \'' . $strEndDate . '\', \'1 hour\'::INTERVAL ) range
					ON (data.created_on BETWEEN range AND range + INTERVAL \'1 hour\') ';

		if( true == $boolIsCachedData ) {
			$arrstrData = self::fetchCachedObjects( $strSql, 'CCallSystemLog', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData;
			}
		} else {
			 return self::fetchCallSystemLogs( $strSql, $objVoipDatabase );
		}
	}
}
?>