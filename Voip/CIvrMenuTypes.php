<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenuTypes
 * Do not add any new functions to this class.
 */

class CIvrMenuTypes extends CBaseIvrMenuTypes {

	public static function fetchIvrMenuTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIvrMenuType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIvrMenuType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIvrMenuType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedIvrMenuTypes( $objVoipDatabase ) {
		return self::fetchIvrMenuTypes( 'SELECT * FROM ivr_menu_types WHERE is_published = true', $objVoipDatabase );
	}
}
?>
