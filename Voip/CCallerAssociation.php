<?php

class CCallerAssociation extends CBaseCallerAssociation {

    public function valCallerPhoneNumberId() {
        if( false == valId( $this->getCallerPhoneNumberId() ) ) {
            return false;
        }

        return true;
    }

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			    $boolIsValid &= $this->valCallerPhoneNumberId();
			    break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>