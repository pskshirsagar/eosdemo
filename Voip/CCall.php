<?php
date_default_timezone_set( 'America/Denver' );
require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';

class CCall extends CBaseCall {

	protected $m_objCallFile;
	protected $m_objCallQueue;
	protected $m_objCallAgent;
	protected $m_objCallChunk;
	protected $m_objCompanyEmployee;

	protected $m_strCompanyName;
	protected $m_strPropertyName;
	protected $m_strMarketingPropertyName;
	protected $m_strCallPhoneNumber;
	protected $m_strPhoneNumberReleasedOn;
	protected $m_strVoiceMailFileName;
	protected $m_strClientName;
	protected $m_strCallTypeName;
	protected $m_strCallStatusTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallQueueName;
	protected $m_strPropertyOfficePhoneNumber;
	protected $m_strCallResultName;
	protected $m_strCallUuid;
	protected $m_strCallAgentFullName;
	protected $m_strCallAgentPreferredName;
	protected $m_strCallAgentPhysicalPhoneExtension;
	protected $m_strCallAgentAgentPhoneExtension;
	protected $m_strOriginCallerName;
	protected $m_strTimeZoneName;
	protected $m_strCallDirection;
	protected $m_strLeasingAgentName;
	protected $m_strCountryCode;

	protected $m_intIsAssociateRecording;
	protected $m_intIvrId;
	protected $m_intIsMultipleApplicantsOnPhoneNumber;
	protected $m_intTimezoneOffset;
	protected $m_intTotalCallCenterQueueTime;
	protected $m_intCallEventId;
	protected $m_intLeadProbability;
	protected $m_strAnalyzer;
	protected $m_strCallScore;
	protected $m_strCallAnalysisScore;
	protected $m_strCallDatetime;

	protected $m_boolDoNotRecordCall;

	protected $m_arrobjCallChunks;

	const CREATE_CALL_JOB					= 'create_call_job';
	const UPDATE_CALL_JOB					= 'update_call_job';
	const CALL_RINGING						= 'call_ringing';
	const CALL_WRAPPING						= 'call_wrapping';
	const CALL_RECEIVED_BY_LEASING_CENTER	= 'Leasing Center';
	const CALL_RECEIVED_BY_ON_SITE			= 'On Site';
	const CALL_RECEIVED_BY_SYSTEM			= 'System';
	const CALL_QUEUE_TIME					= 30;
	const MAX_ATTEMPTS						= 3;
	const CALL_ANALYSIS_CALL_DURATION		= 45;
	const CALL_CENTER_QUEUE_TIME			= 30;

	const LEASING_AGENT_NAME				= 'leasing_agent_name';

	public function __construct() {
		parent::__construct();
		$this->m_intCallIvrQueueTime = '0';
		return;
	}

	/**
	 * Get Functions
	 */

	public function getIsAssociateRecording() {
		return $this->m_intIsAssociateRecording;
	}

	public function getVoiceMailFileName() {
		return $this->m_strVoiceMailFileName;
	}

	public function getCallFile() {
		return $this->m_objCallFile;
	}

	public function getCallTypeName() {
		return $this->m_strCallTypeName;
	}

	public function getCallStatusTypeName() {
		return $this->m_strCallStatusTypeName;
	}

	public function getIsMultipleApplicantsOnPhoneNumber() {
		return $this->m_intIsMultipleApplicantsOnPhoneNumber;
	}

	public function getFormattedCallDuration() {
		return ( false == is_null( $this->getCallDurationSeconds() ) ) ? CCallHelper::changeCallDurationFormat( $this->getCallDurationSeconds() ) : '00:00:00';
	}

	public function getIvrId() {
		return $this->m_intIvrId;
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function getClientName() {
		if( !isset( $this->m_strClientName ) ) {
			$this->m_strClientName = '';
		}
		return $this->m_strClientName;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallAgent() {
		return $this->m_objCallAgent;
	}

	public function getCallQueue() {
		return $this->m_objCallQueue;
	}

	public function getCallChunk() {
		return $this->m_objCallChunk;
	}

	public function getCallChunks() {
		return $this->m_arrobjCallChunks;
	}

	public function getPropertyOfficePhoneNumber() {
		return $this->m_strPropertyOfficePhoneNumber;
	}

	public function getCallResultName() {
		return $this->m_strCallResultName;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getMarketingPropertyName() {
		return ( ( true == valStr( $this->m_strMarketingPropertyName ) ) ? $this->m_strMarketingPropertyName : $this->getPropertyName() );
	}

	public function getCallPhoneNumber() {
		return $this->m_strCallPhoneNumber;
	}

	public function getCallUuid() {
		return $this->m_strCallUuid;
	}

	public function getCallAgentFullName() {
		return $this->m_strCallAgentFullName;
	}

	public function getCallAgentPreferredName() {
		return $this->m_strCallAgentPreferredName;
	}

	public function getCallAgentPhysicalPhoneExtension() {
		return $this->m_strCallAgentPhysicalPhoneExtension;
	}

	public function getCallAgentAgentPhoneExtension() {
		return $this->m_strCallAgentAgentPhoneExtension;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getTimezoneOffset() {
		return $this->m_intTimezoneOffset;
	}

	public function getCallIvrTime() {
		return $this->m_intCallIvrTime;
	}

	public function getTotalCallCenterQueueTime() {
		return $this->m_intTotalCallCenterQueueTime;
	}

	public function getDoNotRecordCall() {
		return $this->m_boolDoNotRecordCall;
	}

	public function getCallReceivedBy() {
		if( true == valId( $this->m_intCallQueueId ) ) {
			if( true == valId( $this->m_intEmployeeId ) ) {
				return $this->m_strCallAgentFullName;
			} else {
				return self::CALL_RECEIVED_BY_LEASING_CENTER;
			}
		} elseif( CCallType::VISA_PHONE_AUTH == $this->m_intCallTypeId ) {
			return self::CALL_RECEIVED_BY_SYSTEM;
		} elseif( false == valId( $this->m_intOutboundCallId ) && true == in_array( $this->getCallTypeId(), CCallTypeHelper::getCallTrackerCallTypes() ) ) {
			return self::CALL_RECEIVED_BY_ON_SITE;
		}
		return false;
	}

	public function getCallDirection() {
		$this->m_strCallDirection = 'Inbound';
		return $this->m_strCallDirection;
	}

	public function getCallEventId() {
		return $this->m_intCallEventId;
	}

	public function getOriginCallerName() {
		return ( true == valStr( $this->m_strOriginCallerName ) ? $this->m_strOriginCallerName : '' );
	}

	public function getTimeZoneName() {
		return $this->m_strTimeZoneName;
	}

	public function getLeadProbability() {
		return $this->m_intLeadProbability;
	}

	public function getLeasingAgentName() {
		return $this->getDetailsArray()['leasing_agent_name'];
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getDetailsArray() {
		return $this->getDetailsField( [] );
	}

	public function getAnalyzer() {
		return $this->m_strAnalyzer;
	}

	public function getCallScore() {
		return $this->m_strCallScore;
	}

	public function getCallDatetime() {
		return $this->m_strCallDatetime;
	}

	public function getCompanyEmployee() {
		return $this->m_objCompanyEmployee;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_type_name'] ) )						$this->setCallTypeName( $arrmixValues['call_type_name'] );
		if( true == isset( $arrmixValues['call_status_type_name'] ) )				$this->setCallStatusTypeName( $arrmixValues['call_status_type_name'] );
		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )						$this->setPropertyName( $arrmixValues['property_name'] );
		if( true == isset( $arrmixValues['marketing_property_name'] ) )				$this->setMarketingPropertyName( $arrmixValues['marketing_property_name'] );
		if( true == isset( $arrmixValues['call_phone_number'] ) )					$this->setCallPhoneNumber( $arrmixValues['call_phone_number'] );
		if( true == isset( $arrmixValues['property_office_phone_number'] ) )		$this->setPropertyOfficePhoneNumber( $arrmixValues['property_office_phone_number'] );
		if( true == isset( $arrmixValues['call_result_name'] ) )					$this->setCallResultName( $arrmixValues['call_result_name'] );
		if( true == isset( $arrmixValues['call_uuid'] ) )							$this->setCallUuid( $arrmixValues['call_uuid'] );
		if( true == isset( $arrmixValues['call_agent_full_name'] ) )				$this->setCallAgentFullName( $arrmixValues['call_agent_full_name'] );
		if( true == isset( $arrmixValues['call_agent_preferred_name'] ) )				$this->setCallAgentPreferredName( $arrmixValues['call_agent_preferred_name'] );
		if( true == isset( $arrmixValues['call_agent_status_type_name'] ) )			$this->setCallAgentStatusTypeName( $arrmixValues['call_agent_status_type_name'] );
		if( true == isset( $arrmixValues['call_agent_state_type_name'] ) )			$this->setCallAgentStateTypeName( $arrmixValues['call_agent_state_type_name'] );
		if( true == isset( $arrmixValues['call_agent_physical_phone_extension'] ) )	$this->setCallAgentPhysicalPhoneExtension( $arrmixValues['call_agent_physical_phone_extension'] );
		if( true == isset( $arrmixValues['call_agent_agent_phone_extension'] ) )	$this->setCallAgentAgentPhoneExtension( $arrmixValues['call_agent_agent_phone_extension'] );
		if( true == isset( $arrmixValues['call_queue_name'] ) )						$this->setCallQueueName( $arrmixValues['call_queue_name'] );
		if( true == isset( $arrmixValues['timezone_offset'] ) )						$this->setTimezoneOffset( $arrmixValues['timezone_offset'] );
		if( true == isset( $arrmixValues['time_zone_name'] ) )						$this->setTimeZoneName( $arrmixValues['time_zone_name'] );
		if( true == isset( $arrmixValues['do_not_record_call'] ) )					$this->setDoNotRecordCall( $arrmixValues['do_not_record_call'] );
		if( true == isset( $arrmixValues['ivr_id'] ) )								$this->setIvrId( $arrmixValues['ivr_id'] );
		if( true == isset( $arrmixValues['call_ivr_time'] ) )						$this->setCallIvrTime( $arrmixValues['call_ivr_time'] );
		if( true == isset( $arrmixValues['lead_probability'] ) )					$this->setLeadProbability( $arrmixValues['lead_probability'] );
		if( true == isset( $arrmixValues['leasing_agent_name'] ) )					$this->setLeasingAgentName( $arrmixValues['leasing_agent_name'] );
		if( true == isset( $arrmixValues['country_code'] ) )						$this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['analyzer'] ) )							$this->setAnalyzer( $arrmixValues['analyzer'] );
		if( true == isset( $arrmixValues['call_score'] ) )							$this->setCallScore( $arrmixValues['call_score'] );
		if( true == isset( $arrmixValues['call_datetime'] ) )						$this->setCallDatetime( $arrmixValues['call_datetime'] );
		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function setIsAssociateRecording( $intIsAssociateRecording ) {
		$this->m_intIsAssociateRecording = $intIsAssociateRecording;
	}

	public function setVoiceMailFileName( $strVoiceMailFileName ) {
		$this->m_strVoiceMailFileName = $strVoiceMailFileName;
	}

	public function setCallFile( $objCallFile ) {
		$this->m_objCallFile = $objCallFile;
	}

	public function setCallTypeName( $strCallTypeName ) {
		$this->m_strCallTypeName = $strCallTypeName;
	}

	public function setCallStatusTypeName( $strCallStatusTypeName ) {
		$this->m_strCallStatusTypeName = $strCallStatusTypeName;
	}

	public function setIsMultipleApplicantsOnPhoneNumber( $intIsMultipleApplicantsOnPhoneNumber ) {
		$this->m_intIsMultipleApplicantsOnPhoneNumber = $intIsMultipleApplicantsOnPhoneNumber;
	}

	public function setIvrId( $intIvrId ) {
		$this->m_intIvrId = $intIvrId;
	}

	public function setEventId( $intEventId ) {
		$this->m_intEventId = $intEventId;
	}

	public function setClientName( $strName ) {
		$this->m_strClientName = $strName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setMarketingPropertyName( $strMarketingPropertyName ) {
		$this->m_strMarketingPropertyName = $strMarketingPropertyName;
	}

	public function setCallPhoneNumber( $strCallPhoneNumber ) {
		$this->m_strCallPhoneNumber = $strCallPhoneNumber;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallAgent( $objCallAgent ) {
		$this->m_objCallAgent = $objCallAgent;
	}

	public function setCallQueue( $objCallQueue ) {
		$this->m_objCallQueue = $objCallQueue;
	}

	public function setCallChunk( $objCallChunk ) {
		$this->m_objCallChunk = $objCallChunk;
	}

	public function setCallChunks( $arrobjCallChunks ) {
		$this->m_arrobjCallChunks = $arrobjCallChunks;
	}

	public function setPropertyOfficePhoneNumber( $strPropertyOfficePhoneNumber ) {
		$this->m_strPropertyOfficePhoneNumber = $strPropertyOfficePhoneNumber;
	}

	public function setCallResultName( $strCallResultName ) {
		$this->m_strCallResultName = $strCallResultName;
	}

	public function setCallUuid( $strCallUuid ) {
		$this->m_strCallUuid = $strCallUuid;
	}

	public function setCallAgentFullName( $strCallAgentFullName ) {
		$this->m_strCallAgentFullName = $strCallAgentFullName;
	}

	public function setCallAgentPreferredName( $strCallAgentPreferredName ) {
		$this->m_strCallAgentPreferredName = $strCallAgentPreferredName;
	}

	public function setCallAgentPhysicalPhoneExtension( $strCallAgentPhysicalPhoneExtension ) {
		$this->m_strCallAgentPhysicalPhoneExtension = $strCallAgentPhysicalPhoneExtension;
	}

	public function setCallAgentAgentPhoneExtension( $strCallAgentAgentPhoneExtension ) {
		$this->m_strCallAgentAgentPhoneExtension = $strCallAgentAgentPhoneExtension;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setTimezoneOffset( $intTimezoneOffset ) {
		$this->m_intTimezoneOffset = $intTimezoneOffset;
	}

	public function setTotalCallCenterQueueTime( $intTotalCallCenterQueueTime ) {
		$this->m_intTotalCallCenterQueueTime = CStrings::strToIntDef( $intTotalCallCenterQueueTime, NULL, false );
	}

	public function setRedirectPropertyId( $intRedirectPropertyId ) {
		$this->m_intRedirectPropertyId = NULL;

		if( true == valId( $intRedirectPropertyId ) && $this->getPropertyId() != $intRedirectPropertyId ) {
			$this->set( 'm_intRedirectPropertyId', CStrings::strToIntDef( $intRedirectPropertyId, NULL, false ) );
		}
	}

	public function setDoNotRecordCall( $boolDoNotRecordCall ) {
		$this->m_boolDoNotRecordCall = $boolDoNotRecordCall;
	}

	public function setOriginCallerName( $strOriginCallerName ) {
		$this->m_strOriginCallerName = $strOriginCallerName;
	}

	public function setTimeZoneName( $strTimeZoneName ) {
		$this->m_strTimeZoneName = CStrings::strTrimDef( $strTimeZoneName, 100, NULL, true );
	}

	public function setCallIvrTime( $intCallIvrTime ) {
		$this->m_intCallIvrTime = $intCallIvrTime;
	}

	public function setLeadProbability( $intLeadProbability ) {
		$this->m_intLeadProbability = $intLeadProbability;
	}

	public function setLeasingAgentName( $strLeasingAgentName ) {
		$this->m_strLeasingAgentName = $strLeasingAgentName;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setAnalyzer( $strAnalyzer ) {
		$this->m_strAnalyzer = $strAnalyzer;
	}

	public function setCallScore( $strCallScore ) {
		$this->m_strCallScore = $strCallScore;
	}

	public function setCallDatetime( $strCallDatetime ) {
		$this->m_strCallDatetime = $strCallDatetime;
	}

	public function setCompanyEmployee( $objCompanyEmployee ) {
		$this->m_objCompanyEmployee = $objCompanyEmployee;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( 1 != CValidation::checkPhoneNumberFullValidation( $this->getDestinationPhoneNumber(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is invalid.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valOriginPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getOriginPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is blank.', NULL ) );
		} elseif( false == CValidation::checkPhoneNumberFullValidation( $this->getOriginPhoneNumber(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is invalid.', NULL ) );
		}
		return $boolIsValid;
	}

	public function valCallNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallNotes() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_notes', 'Call notes is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPhoneNumber();
				break;

			case VALIDATE_DELETE:
				break;

			case 'update_call_status':
				$boolIsValid &= $this->valOriginPhoneNumber();
				break;

			case 'update_support_dialer_call':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCallNotes();
				break;

			case 'insert_resident_support':
				$boolIsValid &= $this->valCallNotes();
				$boolIsValid &= $this->valCallResult();
				break;

			case 'update_call_summary':
				$boolIsValid &= $this->valCallResultId();
				$boolIsValid &= $this->valCallNotes();
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	public function valId() {
		$boolIsValid = true;

		if( false == valStr( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Please answer call.' ) );
		}
		return $boolIsValid;
	}

	public function valCallResult() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Please select secondary call result.' ) );
		}
		return $boolIsValid;
	}

	public function valCallResultId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Please select call result.' ) );
		}
		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createCallChunk() {
		$objCallChunk = new CCallChunk();
		$objCallChunk->setCallId( $this->getId() );
		return $objCallChunk;
	}

	public function createMaintenanceRequestCall() {
		$objMaintenanceRequestCall = new CMaintenanceRequestCall();
		$objMaintenanceRequestCall->setCallId( $this->getId() );
		$objMaintenanceRequestCall->setCid( $this->getCid() );
		return $objMaintenanceRequestCall;
	}

	public function createMaintenanceRequest() {
		$objMaintenanceRequest = new CMaintenanceRequest();
		$objMaintenanceRequest->setPropertyId( $this->getPropertyId() );
		$objMaintenanceRequest->setCid( $this->getCid() );
		$objMaintenanceRequest->setMainPhoneNumber( $this->getOriginPhoneNumber() );
		$objMaintenanceRequest->setIsFloatingMaintenanceRequest( true );
		return $objMaintenanceRequest;
	}

	public function createCallMember() {
		$objCallMember = new CCallMember();
		$objCallMember->setDefaults();
		$objCallMember->setCallId( $this->getId() );
		$objCallMember->setCallerNumber( $this->getOriginPhoneNumber() );
		$objCallMember->setCallQueueId( $this->getCallQueueId() );
		$objCallMember->setSessionUuid( $this->getSessionUuid() );
		return $objCallMember;
	}

	public function createCall() {
		$objCall = new CCall();

		$objCall->setCid( $this->getCid() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setCallTypeId( CCallType::LEASING_CENTER_OUTBOUND );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallResultId( CCallResult::WORK_ORDER );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setDestinationPhoneNumber( $this->getDestinationPhoneNumber() );
		$objCall->setOriginPhoneNumber( CCallPhoneNumber::LEASING_CENTER_LINE );
		$objCall->setEmployeeId( $this->getEmployeeId() );
		$objCall->setCallAgentId( $this->getCallAgentId() );
		$objCall->setOriginExtension( $this->getOriginExtension() );

		return $objCall;
	}

	public function createCallFile() {
		$objCallFile = new CCallFile();
		$objCallFile->setFileExtensionId( CFileExtension::AUDIO_MP3 );
		$objCallFile->setFileName( $this->getId() . '.mp3' );
		$objCallFile->setFilePath( $this->buildStoragePath( $this->getCallTypeId() ) );
		$objCallFile->setFileDatetime( 'NOW()' );
		return $objCallFile;
	}

	public function createOrFetchCallFile( $objVoipDatabase ) {
		if( true == valId( $this->getCallFileId() ) ) {
			$objCallFile = CCallFiles::fetchCallFileById( $this->getCallFileId(), $objVoipDatabase );
			if( true == valObj( $objCallFile, 'CCallFile' ) ) return $objCallFile;
		} else {
			$objCallFile = new CCallFile();
			$objCallFile->setFileExtensionId( CFileExtension::AUDIO_MP3 );
			$objCallFile->setFileName( $this->getId() . '.mp3' );
			$objCallFile->setFilePath( $this->buildStoragePath( $this->getCallTypeId() ) );
			$objCallFile->setFileDatetime( 'NOW()' );
		}

		return $objCallFile;
	}

	public function createCallBlock() {
		$objCallBlock = new CCallBlock();

		$objCallBlock->setScheduledCallId( $this->getScheduledCallId() );
		$objCallBlock->setScheduledCallTypeId( $this->getScheduledCallTypeId() );
		$objCallBlock->setPhoneNumber( $this->getDestinationPhoneNumber() );
		$objCallBlock->setExtension( $this->getDestinationExtension() );
		$objCallBlock->setCid( $this->getCid() );
		return $objCallBlock;
	}

	public function createCallEvent() {
		$objCallEvent = new CCallEvent();

		$objCallEvent->setDefaults();
		$objCallEvent->setCallId( $this->getId() );

		$intCallEventTypeId	= ( false == is_null( CCallStatusType::$c_arrintCallStatusTypeToCallEventTypeId[$this->getCallStatusTypeId()] ) ) ? CCallStatusType::$c_arrintCallStatusTypeToCallEventTypeId[$this->getCallStatusTypeId()] : CCallEventType::CALL_STOP;
		$objCallEvent->setCallEventTypeId( $intCallEventTypeId );

		return $objCallEvent;
	}

	public function createOrFetchCallEvent( $objVoipDatabase, $intCallEventTypeId = NULL ) {
		$objCallEvent = NULL;
		if( false == is_null( $intCallEventTypeId ) ) {
			$objCallEvent = CCallEvents::fetchCallEventByCallIdByCallEventTypeId( $this->getId(), $intCallEventTypeId, $objVoipDatabase );
		}

		if( false == valObj( $objCallEvent, 'CCallEvent' ) ) {
			$objCallEvent = new CCallEvent();
			$objCallEvent->setDefaults();
			$objCallEvent->setCallId( $this->getId() );

			if( false == is_null( $intCallEventTypeId ) ) {
				$intCallEventTypeId = $intCallEventTypeId;
			} else {
				$intCallEventTypeId	= ( false == is_null( CCallStatusType::$c_arrintCallStatusTypeToCallEventTypeId[$this->getCallStatusTypeId()] ) ) ? CCallStatusType::$c_arrintCallStatusTypeToCallEventTypeId[$this->getCallStatusTypeId()] : CCallEventType::CALL_STOP;
			}

			$objCallEvent->setCallEventTypeId( $intCallEventTypeId );
		}
		return $objCallEvent;
	}

	public function createCallIvrMenuAction() {
		$objCallIvrMenuAction = new CCallIvrMenuAction();

		$objCallIvrMenuAction->setCid( $this->getCid() );
		$objCallIvrMenuAction->setCallId( $this->getId() );

		return $objCallIvrMenuAction;
	}

	public function createCallTranscription() {
		$objCallTranscription = new CCallTranscription();
		$objCallTranscription->setCallId( $this->getId() );
		return $objCallTranscription;
	}

	public function pullOrPushAlertMessage( $objVoipDatabase ) {
		$objAlertMessage = CAlertMessages::fetchAlertMessageByCallId( $this->getId(), $objVoipDatabase );

		$objCloneAlertMessage = NULL;

		if( true == valObj( $objAlertMessage, 'CAlertMessage' ) ) {
			if( SYSTEM_USER_ID !== $objAlertMessage->getCompanyUserId() && $objAlertMessage->getCompanyUserId() !== $this->getEmployeeId() ) {
				$objCloneAlertMessage = clone $objAlertMessage;
				$objCloneAlertMessage->setId( NULL );
				$objCloneAlertMessage->setMessage( NULL );
				$objCloneAlertMessage->setCompanyUserId( ( true == valId( $objAlertMessage->getCompanyUserId() ) ) ? $objAlertMessage->getCompanyUserId() : SYSTEM_USER_ID );
				$objCloneAlertMessage->setLockSequenceNumber( NULL );
				$objCloneAlertMessage->setLockedOn( NULL );
				$objCloneAlertMessage->setSentOn( NULL );
				$objCloneAlertMessage->setCreatedOn( NULL );
			}

			$objAlertMessage->setCompanyUserId( ( true == valId( $this->getEmployeeId() ) ) ? $this->getEmployeeId() : SYSTEM_USER_ID );
			$objAlertMessage->setCallQueueName( ( true == valObj( $this->getCallQueue(), 'CCallQueue' ) ? $this->getCallQueue()->getName() : $objAlertMessage->getCallQueueName() ) );
			$objAlertMessage->setPropertyName( ( true == valStr( $this->getMarketingPropertyName() ) ? $this->getMarketingPropertyName() : $this->getPropertyName() ) );
			$objAlertMessage->setRedirectUrl( $this->buildRedirectUrl() );
			$objAlertMessage->setLockSequenceNumber( NULL );
			$objAlertMessage->setLockedOn( NULL );
			$objAlertMessage->setSentOn( NULL );
		} else {
			$objAlertMessage = new CAlertMessage();

			$objAlertMessage->setDefaults();
			$objAlertMessage->setCid( ( false == is_null( $this->getCid() ) ? $this->getCid() : CClient::ID_DEFAULT ) );
			$objAlertMessage->setCallId( $this->getId() );
			$objAlertMessage->setCompanyUserId( ( true == valId( $this->getEmployeeId() ) ) ? $this->getEmployeeId() : SYSTEM_USER_ID );
			$objAlertMessage->setMessageCallId( $this->getId() );
			$objAlertMessage->setCallerName( CCallerHelper::getCallerNameByCall( $this, $objVoipDatabase ) );
			$objAlertMessage->setCallQueueName( ( true == valObj( $this->getCallQueue(), 'CCallQueue' ) ) ? $this->getCallQueue()->getName() : '' );
			$objAlertMessage->setCompanyName( ( true == valStr( $this->getCompanyName() ) ) ? $this->getCompanyName() : '' );
			$objAlertMessage->setPropertyName( ( true == valStr( $this->getMarketingPropertyName() ) ? $this->getMarketingPropertyName() : $this->getPropertyName() ) );
			$objAlertMessage->setLeadSourceName( '' );
			$objAlertMessage->setOriginPhoneNumber( $this->getOriginPhoneNumber() );
			$objAlertMessage->setDestinationPhoneNumber( $this->getDestinationPhoneNumber() );
			$objAlertMessage->setCallTypeName( ( CCallHelper::getCallTypeIdToCallTypeName( $this->getCallTypeId() ) ) );
			$objAlertMessage->setCallPhoneNumberId( $this->getCallPhoneNumberId() );
			$objAlertMessage->setCallTypeId( $this->getCallTypeId() );
			$objAlertMessage->setRedirectUrl( $this->buildRedirectUrl() );
		}

		if( true == valObj( $objCloneAlertMessage, 'CAlertMessage' ) ) {
			$objCloneAlertMessage->insert( SYSTEM_USER_ID, $objVoipDatabase );
		}

		return $objAlertMessage;
	}

	public function createBlankAlertMessage() {
		$objAlertMessage = new CAlertMessage();

		$objAlertMessage->setDefaults();
		$objAlertMessage->setCid( ( true == valId( $this->getCid() ) ? $this->getCid() : CClient::ID_DEFAULT ) );
		$objAlertMessage->setCallId( $this->getId() );
		$objAlertMessage->setCompanyUserId( ( true == valId( $this->getEmployeeId() ) ? $this->getEmployeeId() : SYSTEM_USER_ID ) );
		$objAlertMessage->setMessage( NULL );
		$objAlertMessage->setLockSequenceNumber( NULL );
		$objAlertMessage->setLockedOn( NULL );
		$objAlertMessage->setSentOn( NULL );

		return $objAlertMessage;
	}

	public function createApplicant() {
		require_once PATH_PHP_LIBRARIES . 'Psi/Internationalization/PhoneNumber/CPhoneNumber.class.php';
		$objApplicant = new CApplicant();

		$arrmixCustomerPhoneNumbers[] = [
			'phone_number_type' => CPhoneNumberType::HOME,
			'phone_number' => \i18n\CPhoneNumber::create( $this->getOriginPhoneNumber() )->getValue(),
			'is_primary' => true
		];
		$objApplicant->setCid( $this->getCid() );
		$objApplicant->setNameFirst( 'Unknown Caller' );
		$objApplicant->setNameLast( $this->getOriginPhoneNumber() );
		$objApplicant->setCustomerTypeId( CCustomerType::PRIMARY );
		$objApplicant->setCustomerPhoneNumbers( $arrmixCustomerPhoneNumbers );

		return $objApplicant;
	}

	public function createInboundCall() {

		$objInboundCall = new CInboundCall();

		$objInboundCall->setCid( $this->getCid() );
		$objInboundCall->setPropertyId( $this->getPropertyId() );
		$objInboundCall->setCallerPhoneNumber( $this->getOriginPhoneNumber() );
		$objInboundCall->setCallDatetime( $this->getCallDatetime() );
		$objInboundCall->setCallDurationSeconds( '0' );

		return $objInboundCall;
	}

	public function createCallAgentNote() {
		$objCallAgentNote = new CCallAgentNote();
		$objCallAgentNote->setDefaults();
		$objCallAgentNote->setCallId( $this->getId() );
		return $objCallAgentNote;
	}

	public function createAlertMessage() {
		$objAlertMessage = new CAlertMessage();

		$objAlertMessage->setCid( ( false == is_null( $this->getCid() ) ? $this->getCid() : CClient::ID_DEFAULT ) );
		$objAlertMessage->setCallId( $this->getId() );
		$objAlertMessage->setCompanyUserId( ( true == valId( $this->getEmployeeId() ) ) ? $this->getEmployeeId() : SYSTEM_USER_ID );
		$objAlertMessage->setMessageCallId( $this->getId() );
		$objAlertMessage->setCallQueueName( ( true == valObj( $this->getCallQueue(), 'CCallQueue' ) ) ? $this->getCallQueue()->getName() : '' );
		$objAlertMessage->setCompanyName( ( true == valStr( $this->getCompanyName() ) ) ? $this->getCompanyName() : '' );
		$objAlertMessage->setPropertyName( ( true == valStr( $this->getMarketingPropertyName() ) ? $this->getMarketingPropertyName() : $this->getPropertyName() ) );
		$objAlertMessage->setOriginPhoneNumber( $this->getOriginPhoneNumber() );
		$objAlertMessage->setDestinationPhoneNumber( $this->getDestinationPhoneNumber() );
		$objAlertMessage->setCallTypeName( ( CCallHelper::getCallTypeIdToCallTypeName( $this->getCallTypeId() ) ) );
		$objAlertMessage->setCallPhoneNumberId( $this->getCallPhoneNumberId() );
		$objAlertMessage->setCallTypeId( $this->getCallTypeId() );
		$objAlertMessage->setRedirectUrl( $this->buildRedirectUrl() );
		$objAlertMessage->setCallerName( '' );
		$objAlertMessage->setLeadSourceName( '' );
		return $objAlertMessage;
	}

	public function createEventCall() {

		$objEventCall = new CEventCall();
		$objEventCall->setCallId( $this->m_intId );
		$objEventCall->setPropertyId( $this->m_intPropertyId );
		$objEventCall->setCallPhoneNumberId( $this->m_intCallPhoneNumberId );
		$objEventCall->setCallTypeId( $this->m_intCallTypeId );
		$objEventCall->setCallFileId( $this->m_intCallFileId );
		$objEventCall->setCallStatusTypeId( $this->m_intCallStatusTypeId );
		$objEventCall->setCallResultId( $this->m_intCallResultId );
		$objEventCall->setCompanyEmployeeId( $this->m_intCompanyEmployeeId );
		$objEventCall->setEmployeeId( $this->m_intEmployeeId );
		$objEventCall->setCustomerId( $this->m_intCustomerId );
		$objEventCall->setApplicantApplicationId( $this->m_intApplicantApplicationId );
		$objEventCall->setCallDurationSeconds( $this->m_intCallDurationSeconds );
		$objEventCall->setCallCenterTalkTime( $this->m_intCallCenterTalkTime );
		$objEventCall->setCallCenterQueueTime( $this->m_intCallCenterQueueTime );
		$objEventCall->setCallIvrQueueTime( $this->m_intCallIvrQueueTime );
		$objEventCall->setDestinationPhoneNumber( $this->m_strDestinationPhoneNumber );
		$objEventCall->setOriginPhoneNumber( $this->m_strOriginPhoneNumber );
		$objEventCall->setCallDatetime( $this->m_strCallDatetime );

		return $objEventCall;
	}

	public function loadRedirectUrls() {

		$strRedirectUrl	= CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN;

		$this->m_arrstrRedirectUrls['view_call_tracker_call']			= $strRedirectUrl . '/?module=call_details-new&action=view_calls';
		$this->m_arrstrRedirectUrls['edit_respond_ticket']				= $strRedirectUrl . '/?module=default_dashboard_tickets-new&action=edit_respond_ticket';
		$this->m_arrstrRedirectUrls['property_utility_dashboard']		= $strRedirectUrl . '/?module=property_utility_dashboard-new';
		$this->m_arrstrRedirectUrls['outbound_calls']					= $strRedirectUrl . '/?module=outbound_call-new&action=edit_outbound_call';
		$this->m_arrstrRedirectUrls['login_user']						= $strRedirectUrl . '/?module=clients-new&action=login_user';
		$this->m_arrstrRedirectUrls['ps_lead-new']						= $strRedirectUrl . '/?module=ps_lead-new';
		$this->m_arrstrRedirectUrls['create_call_center_support_task']	= $strRedirectUrl . '/?module=task-new&action=create_call_center_support_task';
		$this->m_arrstrRedirectUrls['resident_insure']					= $strRedirectUrl . '/?module=insurance_carrier_properties-new';
		$this->m_arrstrRedirectUrls['resident_support']					= $strRedirectUrl . '/?module=resident_support-new';
		$this->m_arrstrRedirectUrls['resident_insure_unlicensed']		= $strRedirectUrl . '/?module=insurance_policies-new&action=view_insurance_policies';

		return;
	}

	public function buildRedirectUrl() {
		$this->loadRedirectUrls();

		$strRedirectUrl	= NULL;
		if( true == valStr( $this->getCid() ) )								$arrstrParams[]	= 'client[id]=' . $this->getCid();
		if( true == valStr( $this->getPropertyId() ) )						$arrstrParams[]	= 'property[id]=' . $this->getPropertyId();
		if( true == valObj( $this->m_objCallAgent, 'CCallAgent' ) )	$arrstrParams[]	= 'company_user[id]=' . $this->m_objCallAgent->getUserId();

		switch( $this->getCalltypeId() ) {
			case CCallType::UTILITY_BILLING_SUPPORT:
				$objCallLibrary			= new CCallLibrary();
				$objCallerAssociation	= CCallerAssociations::fetchCallerAssociationByCallerPhoneNumber( $this->getOriginPhoneNumber(), $objCallLibrary->loadVoipDatabase() );
				if( true == valObj( $objCallerAssociation, 'CCallerAssociation' ) && true == valId( $objCallerAssociation->getCid() ) ) {
					$objPsLead		= CPsLeads::fetchPsLeadByCid( $objCallerAssociation->getCid(), $objCallLibrary->loadAdminDatabase() );
				}
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				if( true == valObj( $objPsLead, 'CPsLead' ) && true == valId( $objCallerAssociation->getPropertyId() ) ) {
					$strRedirectUrl	= $this->m_arrstrRedirectUrls['ps_lead-new'];
					$arrstrParams[]	= 'load_tab=2';
					$arrstrParams[]	= 'client[id]=' . $objPsLead->getCid();
					$arrstrParams[]	= 'ps_lead[id]=' . $objPsLead->getId();
					$arrstrParams[]	= 'property[id]=' . $objCallerAssociation->getPropertyId();
				} else {
					$strRedirectUrl	= $this->m_arrstrRedirectUrls['property_utility_dashboard'];
				}
				break;

			case CCallType::MOVE_IN_CALLS:
			case CCallType::NET_PROMOTER_SCORE:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['outbound_calls'];
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				break;

			case CCallType::CALL_CENTER:
			case CCallType::NOTIFICATIONS:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['create_call_center_support_task'];
				$arrstrParams[]	= 'call_id=' . $this->getId();
				break;

			case CCallType::VENDOR_ACCESS:
				$objCallLibrary = new CCallLibrary();
				$objWorker = \Psi\Eos\Utilities\CWorkers::createService()->fetchActiveWorkersDetailsByPhoneNumber( $this->getOriginPhoneNumber(), $objCallLibrary->loadUtilitiesDatabase() );
				if( true == valObj( $objWorker, 'CWorker' ) ) {
					$strEncryptedUserKey = NULL;
					if( true == valObj( $this->m_objCallAgent, 'CCallAgent' ) ) {
						$strEncryptedUserKey = base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $this->m_objCallAgent->getUserId(), CONFIG_SODIUM_KEY_ID ) );
					}
					$objVendor = \Psi\Eos\Utilities\CVendors::createService()->fetchVendorById( $objWorker->getVendorId(), $objCallLibrary->loadUtilitiesDatabase() );
					$strRedirectUrl = 'https://' . $objVendor->getSubDomain() . CONFIG_VENDOR_ACCESS_SUFFIX . '/authenticate-user/' . $strEncryptedUserKey;
					$arrstrParams[]	= NULL;
				} else {
					$strRedirectUrl = $this->m_arrstrRedirectUrls['create_call_center_support_task'];
					$arrstrParams[]	= 'call_id=' . $this->getId();
				}
				break;

			case CCallType::CALL_TRACKER_VACANCY_ORGANIC:
			case CCallType::CALL_TRACKER_VACANCY_PAID:
			case CCallType::CALL_CENTER_LEAD:
			case CCallType::CALL_CENTER_MAINTENANCE:
			case CCallType::CALL_CENTER_EMERGENCY_MAINTENANCE:
			case CCallType::CALL_TRACKING_OFFICE:
			case CCallType::LEASING_CENTER_OUTBOUND:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['login_user'];
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				break;

			case CCallType::CORPORATE_CARE_LINE:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['login_user'];
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				$arrstrParams[] = 'return_url=module=leasing_center_dashboard_systemxxx';
				break;

			case CCallType::OUTBOUND_SUPPORT:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['edit_respond_ticket'];
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				break;

			case CCallType::RESIDENT_SUPPORT:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['resident_support'];
				$arrstrParams[] = 'call[id]=' . $this->getId();
				break;

			case CCallType::RESIDENT_INSURE:
			case CCallType::RESIDENT_VERIFY:
				if( $this->getCallQueueId() == CCallQueue::RESIDENT_INSURE_UNLICENSED || $this->getCallQueueId() == CCallQueue::RESIDENT_INSURE_UNLICENSED_SPANISH ) {
					$strRedirectUrl = $this->m_arrstrRedirectUrls['resident_insure_unlicensed'];
					$arrstrParams[]	= 'call[id]=' . $this->getId();
					break;
				}

			default:
				$strRedirectUrl = $this->m_arrstrRedirectUrls['login_user'];
				$arrstrParams[]	= 'call[id]=' . $this->getId();
				$arrstrParams[]	= 'call_tracker[call_id]=' . $this->getId();
				break;
		}

		return ( $strRedirectUrl . '&' . implode( '&', $arrstrParams ) );
	}

	public function insertOrUpdateLeasingCenterOutboundCallEvent( $objCustomer, $objDatabase ) {

		$objEvent = \Psi\Eos\Entrata\CEvents::createService()->fetchEventByCidByDataReferenceIdByEventTypeId( $this->getCid(), $this->getId(), CEventType::LEASING_CENTER_OUTBOUND_CALL, $objDatabase );
		$strEventAction = 'updateEvent';

		switch( NULL ) {
			default:
				$objEventLibrary			= new CEventLibrary();
				if( false == valObj( $objEvent, 'CEvent' ) ) {
					$strEventAction = 'insertEvent';
					$objEvent = $objEventLibrary->createEvent( [ $objCustomer ], CEventType::LEASING_CENTER_OUTBOUND_CALL );
					$objEvent->setCid( $this->getCid() );
					$objEvent->setPropertyId( $this->getPropertyId() );
					$objEvent->setOldStatusId( $this->getCallStatusTypeId() );
					$objEvent->setNewStatusId( $this->getCallStatusTypeId() );
					$objEvent->setEventDatetime( 'NOW()' );
					$objEvent->setDataReferenceId( $this->getId() );
					$objEvent->setIsResident( true );
					$objEvent->setDoNotTriggerEvents( true );
					$objEvent->setPsProductId( CPsProduct::LEASING_CENTER );
				}
				$objEvent->setReference( $this );
				$objEvent->setEventCall( $this->createEventCall() );

				$objEventLibraryDataObject	= $objEventLibrary->getEventLibraryDataObject();

				$objEventLibraryDataObject->setDatabase( $objDatabase );
				$objEventLibraryDataObject->setVoipDatabase( $this->loadVoipDatabase() );
				$objEventLibraryDataObject->setCustomer( $objCustomer );
				$objEventLibraryDataObject->setEvent( $objEvent );

				$objEventLibrary->setIsExportEvent( true );
				$objEventLibrary->buildEventDescription( $objCustomer );

				$objEventResult = \Psi\Eos\Entrata\CEventResults::createService()->fetchEventResultByDefaultEventResultIdByCid( CDefaultEventResult::COMPLETED, $this->getCid(), $objDatabase );

				if( true == valObj( $objEventResult, 'CEventResult' ) ) {
					$objEvent->setEventResultId( $objEventResult->getId() );
				}

				if( false == $objEventLibrary->$strEventAction( SYSTEM_USER_ID, $objDatabase ) ) {
					return false;
				}
				return true;
		}
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCallChunksByCallIdOrderByOrderNum( $objVoipDatabase ) {
		return CCallChunks::fetchCallChunksByCallIdOrderByOrderNum( $this->m_intId, $objVoipDatabase );
	}

	public function fetchCallFile( $objVoipDatabase ) {
		$this->m_objCallFile = \Psi\Eos\Voip\CCallFiles::createService()->fetchCallFileById( $this->getCallFileId(), $objVoipDatabase );
		return $this->m_objCallFile;
	}

	public function fetchClient( $objVoipDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objVoipDatabase );
	}

	public function fetchProperty( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );
	}

	public function fetchCallPhoneNumber( $objVoipDatabase ) {
		return CCallPhoneNumbers::fetchCallPhoneNumberById( $this->getCallPhoneNumberId(), $objVoipDatabase );
	}

	public function getOrFetchCallFile( $objVoipDatabase ) {
		if( true == valObj( $this->m_objCallFile, 'CCallFile' ) ) {
			return $this->m_objCallFile;
		} else {
			return $this->fetchCallFile( $objVoipDatabase );
		}
	}

	public function getDestinationPhoneNumber() {
		return preg_replace( '/\D/', '', parent::getDestinationPhoneNumber() );
	}

	public function getOriginPhoneNumber( $boolIsOrignalOriginPhoneNumber = false ) {
		if( true == $boolIsOrignalOriginPhoneNumber ) {
			return parent::getOriginPhoneNumber();
		}
		return preg_replace( '/\D/', '', parent::getOriginPhoneNumber() );
	}

	public function fetchPropertyCallSetting( $objVoipDatabase ) {
		return CPropertyCallSettings::fetchPropertyCallSettingByCidByPropertyId( $this->getCid(), $this->getPropertyId(), $objVoipDatabase );
	}

	public function fetchOutboundCall( $objVoipDatabase ) {
		return COutboundCalls::fetchOutboundCallById( $this->getOutboundCallId(), $objVoipDatabase );
	}

	public function fetchCallAgent( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgentByEmployeeId( $this->getEmployeeId(), $objVoipDatabase );
	}

	public function fetchCallQueue( $objVoipDatabase ) {
		return CCallQueues::fetchCallQueueByCallId( $this->getId(), $objVoipDatabase );
	}

	public function fetchAlertMessage( $objVoipDatabase ) {
		return CAlertMessages::fetchAlertMessageByCallId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCallIvrQueueTime( $objVoipDatabase ) {
		$strSql = 'SELECT 
						EXTRACT( EPOCH FROM Age( cimv.created_on::TIMESTAMP, \'' . $this->getCallDatetime() . '\'::TIMESTAMP ) ) AS call_ivr_queue_time 
					FROM
						call_ivr_menu_actions AS cimv
					WHERE
						cimv.call_id = ' . ( int ) $this->getId() . '
					ORDER BY created_on DESC
						LIMIT 1';

		$arrintCallIvrQueueTimes = fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrintCallIvrQueueTimes[0]['call_ivr_queue_time'] ) ? ( int ) $arrintCallIvrQueueTimes[0]['call_ivr_queue_time'] : 0 );
	}

	public function fetchPendingOutboundCallByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		return COutboundCalls::fetchOutboundCallByCidByPropertyIdByOutboundCallStatusTypeIdByCallTypeIdByCallAgentId( $this->getCid(), $this->getPropertyId(), COutboundCallStatusType::PENDING, CCallType::LEASING_CENTER_OUTBOUND, $intCallAgentId, $objVoipDatabase );
	}

	public function getOrFetchCallAnalysisScore( $objVoipDatabase, $boolIsFetch = false ) {
		if( NULL !== $this->m_strCallAnalysisScore && false == $boolIsFetch ) {
			return $this->m_strCallAnalysisScore;
		}

		$arrmixCallAnalysisResults = ( array ) CCallAnalysisResults::fetchCallAnalysisScoreByCallId( $this->getId(), $objVoipDatabase );
		$arrmixCallAnalysisResults = rekeyArray( 'call_id', $arrmixCallAnalysisResults );
		$this->m_strCallAnalysisScore = ( true == isset( $arrmixCallAnalysisResults[$this->getId()] ) ) ? $arrmixCallAnalysisResults[$this->getId()]['call_analysis_score'] : false;
		return $this->m_strCallAnalysisScore;
	}

	/**
	 * Other Functions
	 *
	 */

	public function addToDetails( $strKey, $strValue ) {
		$this->setDetailsField( $strKey, $strValue );
	}

	public function buildStoragePath( $intCallTypeId ) {
		date_default_timezone_set( 'America/Denver' );

		$strTempFilePath	= ( true == valId( $this->getCid() && CCallType::CALL_CENTER !== $this->getCallTypeId() ) ? $this->getCid() . '/' : '' ) . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';
		$strPath			= NULL;

		switch( $intCallTypeId ) {
			case CCallType::RESIDENT_INSURE:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'insurance_sales/' . $strTempFilePath;
				break;

			case CCallType::RESIDENT_VERIFY:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'resident_verify/' . $strTempFilePath;
				break;

			case CCallType::UTILITY_BILLING_SUPPORT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'utility_billing_support/' . $strTempFilePath;
				break;

			case CCallType::UTILITY_BILLING_ACCOUNT_MANAGEMENT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'utility_billing_account_management/' . $strTempFilePath;
				break;

			case CCallType::UTILITY_BILLING_FAX:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'utility_billing_fax/' . $strTempFilePath;
				break;

			case CCallType::ACCOUNT_MANAGEMENT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'account_management/' . $strTempFilePath;
				break;

			case CCallType::DEVELOPMENT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'development/' . $strTempFilePath;
				break;

			case CCallType::INSIDE_SALES:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'inside_sales/' . $strTempFilePath;
				break;

			case CCallType::OUTISDE_SALES:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'outside_sales/' . $strTempFilePath;
				break;

			case CCallType::CALL_TRACKER_LEAD:
			case CCallType::CALL_CENTER_LEAD:
			case CCallType::CALL_TRACKING_OFFICE:
			case CCallType::CALL_TRACKER_LEAD_VIA_IVR:
			case CCallType::CALL_CENTER_LEAD_VIA_IVR:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_tracker_lead/' . $strTempFilePath;
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE:
			case CCallType::CALL_CENTER_MAINTENANCE:
			case CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR:
			case CCallType::CALL_CENTER_MAINTENANCE_VIA_IVR:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_tracker_maintenance/' . $strTempFilePath;
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR:
			case CCallType::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR:
			case CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY:
			case CCallType::CALL_CENTER_EMERGENCY_MAINTENANCE:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_tracker_maintenance_emergency/' . $strTempFilePath;
				break;

			case CCallType::MOVE_IN_CALLS:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'move_in_calls/' . $strTempFilePath;
				break;

			case CCallType::NOTIFICATIONS:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'notifications/' . $strTempFilePath;
				break;

			case CCallType::ACCOUNTING:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'accounting/' . $strTempFilePath;
				break;

			case CCallType::IT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'it/' . $strTempFilePath;
				break;

			case CCallType::CALL_MERCHANT_SERVICES:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'merchant_services/' . $strTempFilePath;
				break;

			case CCallType::CALL_CENTER:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_center/' . $strTempFilePath;
				break;

			case CCallType::CALL_TRACKER_VACANCY_ORGANIC:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_tracker_vacancy_organic/' . $strTempFilePath;
				break;

			case CCallType::CALL_TRACKER_VACANCY_PAID:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'call_tracker_vacancy_paid/' . $strTempFilePath;
				break;

			case CCallType::PROPERTY_FAX:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'property_fax/' . $strTempFilePath;
				break;

			case CCallType::OUTBOUND_PROPERTY_SCHEDULED_CALL:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'property_scheduled_call/' . $strTempFilePath;
				break;

			case CCallType::OUTBOUND_SUPPORT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'outbound_support/' . $strTempFilePath;
				break;

			case CCallType::NET_PROMOTER_SCORE:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'net_promoter_score/' . $strTempFilePath;
				break;

			case CCallType::PSI_OFFICE:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'psi_office/' . $strTempFilePath;
				break;

			case CCallType::RESIDENT_SUPPORT:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'resident_support/' . $strTempFilePath;
				break;

			case CCallType::LEASING_CENTER_OUTBOUND:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'leasing_center_outbound/' . $strTempFilePath;
				break;

			case CCallType::VENDOR_ACCESS:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'vendor_access/' . $strTempFilePath;
				break;

			case CCallType::CORPORATE_CARE_LINE:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'corporate_care_line/' . $strTempFilePath;
				break;

			case CCallType::MANUAL_FOLLOWUP_CALL:
				$strPath = PATH_VOIP_MOUNTS_CALL_FILES . 'manual_followup_call/' . $strTempFilePath;
				break;

			default:
				// default case
				break;
		}
		return $strPath;
	}

	public function sendVoicemailNotificationEmail( $objClientDatabase, $objVoipDatabase, $objEmailDatabase ) {
		if( false == $this->getIsVoiceMail() ) return false;

		$objCallFile			= $this->getOrFetchCallFile( $objVoipDatabase );
		$objProperty			= $this->fetchProperty( $objClientDatabase );
		$objClient				= $objProperty->fetchClient( $objClientDatabase );
		$objPropertyEmailRule	= NULL;

		if( false == valObj( $objCallFile, 'CCallFile' ) || false == valObj( $objProperty, 'CProperty' ) ) {
			return false;
		}

		$objPropertyEmailRule = \Psi\Eos\Entrata\CPropertyEmailRules::createService()->fetchCustomPropertyEmailRuleBySystemEmailTypeIdByPropertyId( CSystemEmailType::VOICE_EMAIL, $objProperty->getId(), $objClientDatabase );

		$strToEmailAddress = '';

		switch( $this->getCallTypeId() ) {
			case CCallType::CALL_CENTER_MAINTENANCE:
			case CCallType::CALL_CENTER_EMERGENCY_MAINTENANCE:
			case CCallType::CALL_TRACKER_MAINTENANCE:
			case CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR:
			case CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR:
			case CCallType::CALL_CENTER_MAINTENANCE_VIA_IVR:
			case CCallType::CALL_CENTER_MAINTENANCE_EMERGENCY_VIA_IVR:
				$arrobjPropertyPreferences = $objProperty->fetchPropertyPreferencesByKeysKeyedByKey( array( 'MAINTENANCE_VOICEMAIL_NOTIFICATION_EMAIL', 'MAINTENANCE_NOTIFICATION_EMAIL' ), $objClientDatabase );

				$strToEmailAddress = '';

				if( true == valArr( $arrobjPropertyPreferences ) ) {
					if( true == array_key_exists( 'MAINTENANCE_VOICEMAIL_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['MAINTENANCE_VOICEMAIL_NOTIFICATION_EMAIL']->getValue() ) ) {
						$strToEmailAddress = $arrobjPropertyPreferences['MAINTENANCE_VOICEMAIL_NOTIFICATION_EMAIL']->getValue();
					} elseif( true == array_key_exists( 'MAINTENANCE_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['MAINTENANCE_NOTIFICATION_EMAIL']->getValue() ) ) {
						$strToEmailAddress = $arrobjPropertyPreferences['MAINTENANCE_NOTIFICATION_EMAIL']->getValue();
					}
				}
				break;

			default:
				$arrobjPropertyPreferences = $objProperty->fetchPropertyPreferencesByKeysKeyedByKey( array( 'LEAD_VOICEMAIL_NOTIFICATION_EMAIL' ), $objClientDatabase );

				if( true == valArr( $arrobjPropertyPreferences ) ) {
					if( true == array_key_exists( 'LEAD_VOICEMAIL_NOTIFICATION_EMAIL', $arrobjPropertyPreferences ) && true == valStr( $arrobjPropertyPreferences['LEAD_VOICEMAIL_NOTIFICATION_EMAIL']->getValue() ) ) {
						$strToEmailAddress = $arrobjPropertyPreferences['LEAD_VOICEMAIL_NOTIFICATION_EMAIL']->getValue();
					}
				}
				break;
		}

		if( true == empty( $strToEmailAddress ) ) {
			$objOfficeEmailAddress = $objProperty->fetchOfficeEmailAddress( $objClientDatabase );

			if( false == is_null( $objOfficeEmailAddress ) ) {
				$strToEmailAddress = $objOfficeEmailAddress->getEmailAddress();
			}
		}

		if( true == empty( $strToEmailAddress ) ) {
			return false;
		}

		// Do Not Remove this line. When we press "ENTER" key while inserting email address in RW, the email become gibberish.
		$strToEmailAddress = preg_replace( '/\s\s+/', '', $strToEmailAddress );

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_COMMON, false );

		$objSmarty->assign_by_ref( 'property', 				$objProperty );
		$objSmarty->assign_by_ref( 'client', 	$objClient );
		$objSmarty->assign_by_ref( 'call',					$this );
		$objSmarty->assign( 'media_library_uri',			CONFIG_MEDIA_LIBRARY_PATH );
		$objSmarty->assign( 'base_uri',						CONFIG_HOST_PREFIX . CONFIG_ENTRATA_DOMAIN . '/' );
		$objSmarty->assign( 'CONFIG_ENTRATA_LOGO_PREFIX', 	CONFIG_ENTRATA_LOGO_PREFIX );
		$objSmarty->assign( 'property_email_rule',			$objPropertyEmailRule );
		$objSmarty->assign( 'image_url',					CONFIG_COMMON_PATH );

		CCallType::assignSmartyConstants( $objSmarty );

		$strHtmlEmailContent = $objSmarty->fetch( PATH_INTERFACES_COMMON . 'system_emails/call_logs/voicemail.tpl' );

		$objSystemEmail = new CSystemEmail();

		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setPropertyId( $this->getPropertyId() );
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::VOICE_EMAIL );
		$objSystemEmail->setHtmlContent( $strHtmlEmailContent );
		$objSystemEmail->setFromEmailAddress( CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );
		$objSystemEmail->setCompanyEmployeeId( SYSTEM_USER_ID );

		$strSubject = $this->getCallDurationSeconds() . ' Second ';

		if( CCallType::CALL_TRACKER_LEAD == $this->getCallTypeId() ) {
			$strSubject .= 'Lead';
		} elseif( CCallType::CALL_TRACKER_MAINTENANCE == $this->getCallTypeId() ) {
			$strSubject .= 'Maintenance';
		} elseif( CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY == $this->getCallTypeId() ) {
			$strSubject .= 'Emergency Maintenance';
		}

		$strSubject .= ' Voicemail for ' . $objProperty->getPropertyName();

		$objSystemEmail->setSubject( $strSubject );

		$arrstrPropertyEmailAddresses	= array( $strToEmailAddress );
		$arrobjSystemEmails				= array();

		if( true == array( $arrstrPropertyEmailAddresses ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrPropertyEmailAddresses ) ) {
			foreach( $arrstrPropertyEmailAddresses as $strPropertyEmailAddress ) {
				$objSystemEmailTmp = clone $objSystemEmail;

				$objSystemEmailTmp->setToEmailAddress( $strPropertyEmailAddress );
				$objSystemEmailTmp->setReplyToEmailAddress( $strPropertyEmailAddress );

				if( true == valObj( CSystemEmailLibrary::processSystemEmailByPropertyEmailRules( $objSystemEmailTmp, $objPropertyEmailRule ), 'CSystemEmail' ) ) {
					$arrobjSystemEmails[] = $objSystemEmailTmp;
				}
			}
		}

		if( false == valArr( $arrobjSystemEmails ) ) {
			return false;
		}

		foreach( $arrobjSystemEmails as $objSystemEmail ) {
			$objEmailAttachment = new CEmailAttachment();
			$objEmailAttachment->setFilePath( dirname( $objCallFile->getFullFilePath() ) );
			$objEmailAttachment->setFileName( basename( $objCallFile->getFullFilePath() ) );
			$objSystemEmail->addEmailAttachment( $objEmailAttachment );
			if( false == $objSystemEmail->insert( SYSTEM_USER_ID, $objEmailDatabase ) ) {
				return false;
			}
		}
	}

	public function getPropertyPhoneNumbers( $objVoipDatabase ) {
		$arrstrPropertyPhoneNumbers = [];
		$objPropertyCallSetting = $this->fetchPropertyCallSetting( $objVoipDatabase );
		if( true == valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$arrstrPropertyPhoneNumbers = array_unique( array_filter( [
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getOfficePhoneNumber() )->getValue(),
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getAfterHoursOfficePhoneNumber() )->getValue(),
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getMaintenancePhoneNumber() )->getValue(),
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getMaintenanceEmergencyPhoneNumber() )->getValue(),
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getAfterHoursMaintenancePhoneNumber() )->getValue(),
				\i18n\CPhoneNumber::create( $objPropertyCallSetting->getAfterHoursMaintenanceEmergencyPhoneNumber() )->getValue()
			] ) );
		}

		return $arrstrPropertyPhoneNumbers;
	}

	/**
	 * Update FS Call handling
	 *
	 */

	public function buildMountStoragePath() {
		return PATH_VOIP_MOUNTS . $this->buildStoragePath( $this->getCallTypeId() );
	}

	public function createOutboundCall() {
		$objOutboundCall = new COutboundCall();

		$objOutboundCall->setCid( $this->getCid() );
		$objOutboundCall->setPropertyId( $this->getPropertyId() );
		$objOutboundCall->setCallTypeId( CCallType::LEASING_CENTER_OUTBOUND );
		$objOutboundCall->setOutboundCallStatusTypeId( COutboundCallStatusType::PENDING );
		$objOutboundCall->setPhoneNumber( $this->getDestinationPhoneNumber() );

		return $objOutboundCall;
	}

	public function createFreeswitchCall( $objVoipDatabase, $objConnectDatabase = NULL ) {
		$objCallFile = $this->createOrFetchCallFile( $objVoipDatabase );
		$intClusterId = CCluster::RAPID;

		$objVoipDatabase->begin();

		if( false == $objCallFile->insertOrUpdate( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		$this->setCallFileId( $objCallFile->getId() );

		if( false == $this->update( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		$objVoipDatabase->commit();

		if( true == valId( $this->getCid() ) ) {
			$objDirective = CDirectives::fetchDirectiveByCidByDirectiveTypeId( $this->getCid(), CDirectiveType::CLIENT, $objConnectDatabase );
			$intClusterId = ( true == valObj( $objDirective, 'CDirective' ) ? $objDirective->getClusterId() : CCluster::STANDARD );
		}

		$this->addJobQueue( CCall::CREATE_CALL_JOB, $intClusterId );
		return true;
	}

	public function updateFreeswitchCall( $objVoipDatabase, $objConnectDatabase = NULL ) {
		$intClusterId = CCluster::RAPID;
		$objCallFile = NULL;

		// Update Call File
		if( CCallStatusType::UNANSWERED == $this->getCallStatusTypeId() || true == $this->getDoNotRecordCall() ) {
			$objCallFile = CCallFiles::fetchCallFileById( $this->getCallFileId(), $objVoipDatabase );
			$this->setCallFileId( NULL );
		}

		// Update Call object.
		$objVoipDatabase->begin();

		if( false == $this->update( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		if( CCallStatusType::UNANSWERED == $this->getCallStatusTypeId() && true == valObj( $objCallFile, 'CCallFile' ) ) {
			$objCallFile->delete( SYSTEM_USER_ID, $objVoipDatabase );
		}

		$objVoipDatabase->commit();

		if( true == valId( $this->getCid() ) ) {
			$objDirective = CDirectives::fetchDirectiveByCidByDirectiveTypeId( $this->getCid(), CDirectiveType::CLIENT, $objConnectDatabase );
			$intClusterId = ( true == valObj( $objDirective, 'CDirective' ) ? $objDirective->getClusterId() : CCluster::STANDARD );
		}

		$this->addJobQueue( CCall::UPDATE_CALL_JOB, $intClusterId );
		return true;
	}

	public function ringingFreeswitchCall( $objVoipDatabase, $objConnectDatabase ) {
		$intClusterId = CCluster::RAPID;

		if( false == $this->update( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		if( true == $this->isLeadCall() ) {
			if( true == valId( $this->getCid() ) ) {
				$objDirective = CDirectives::fetchDirectiveByCidByDirectiveTypeId( $this->getCid(), CDirectiveType::CLIENT, $objConnectDatabase );
				$intClusterId = ( true == valObj( $objDirective, 'CDirective' ) ? $objDirective->getClusterId() : CCluster::STANDARD );
			}
		}

		if( false == $this->getIsTestCall() ) {
			$this->addJobQueue( CCall::CALL_RINGING, $intClusterId );
		}

		return true;
	}

	public function addJobQueue( $strCallAction = CCall::UPDATE_CALL_JOB, $intClusterId = CCluster::RAPID, $arrstrParams = [] ) {
		return ( new CCallsMessageSenderLibrary() )->sendCallsInboundCallMessage( $this, $strCallAction, $intClusterId, $arrstrParams );
	}

	public function addCallChunk( $objCallChunk ) {
		$this->m_arrobjCallChunks[] = $objCallChunk;
	}

	/**
	 *  Override Base Functions
	 *
	 */

	public function delete( $intUserId, $objVoipDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );
		return parent::update( $intUserId, $objVoipDatabase, $boolReturnSqlOnly );
	}

	public function deleteAlertMessages( $objVoipDatabase ) {
		$strSql = 'DELETE
					FROM
						alert_messages
					WHERE
						( call_id = ' . ( int ) $this->getId() . ' 
					AND sent_on IS NOT NULL )
					OR ( company_user_id = ' . SYSTEM_USER_ID . ' 
					AND call_id = ' . ( int ) $this->getId() . ')';

		return $this->executeSql( $strSql, $this, $objVoipDatabase );
	}

	public function syncCallAgentState( $objCompanyUser, CDatabase $objVoipDatabase, CDatabase $objFreeswitchDatabase ) {
		$boolIsSuccess			= true;
		$intIdleUpdatedOnTime	= NULL;

		if( false == valStr( $this->getEmployeeId() ) ) return $boolIsSuccess;

		$objCallAgent		= $this->fetchCallAgent( $objVoipDatabase );
		$objAgent			= $objCallAgent->fetchAgent( $objFreeswitchDatabase );
		$objAlertMessage	= $this->pullOrPushAlertMessage( $objVoipDatabase );

		if( false == valObj( $objAgent, 'CAgent' ) ) return $boolIsSuccess;

		$arrintCallAgentStatusTypeIds = array(
			CCallAgentStatusType::AVAILABLE,
			CCallAgentStatusType::AVAILABLE_ON_DEMAND
		);

		$objAlertMessage->setMessage( NULL );
		$objAlertMessage->setLockSequenceNumber( NULL );
		$objAlertMessage->setLockedOn( NULL );
		$objAlertMessage->setSentOn( NULL );
		$objAlertMessage->setCompanyUserId( $this->getEmployeeId() );

		if( false == $objAlertMessage->insertOrUpdate( $objCompanyUser->getId(), $objVoipDatabase ) ) {
			$boolIsSuccess &= false;
		}

		if( true == valObj( $objAlertMessage, 'CAlertMessage' ) ) {
			$objAlertMessage->sendCallNotification( $this, $objCallAgent );
		}

		/**
		* The code blocks meaning that the call agent still on call and not hangup yet.
		* So its determine the next expected call agent state type.
		*/
		if( false == in_array( $objCallAgent->getCallAgentStatusTypeId(), $arrintCallAgentStatusTypeIds ) || CCallAgentStateType::IDLE !== $objCallAgent->getCallAgentStateTypeId() ) {
			$intIdleUpdatedOnTime = $objCallAgent->getUpdatedOn();

			$this->setWrapUpTime( strtotime( date( 'Y-m-d H:i:s' ) ) - strtotime( $intIdleUpdatedOnTime ) );
			$objCallAgent->setNextCallAgentStateTypeId( CCallAgentStateType::WAITING );

			if( CCallAgentStateType::DIALING == $objCallAgent->getCallAgentStateTypeId() || CCallAgentStateType::IN_A_OUTBOUND_CALL == $objCallAgent->getCallAgentStateTypeId() ) {
				$objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::WAITING );
			}

			if( false == $objCallAgent->update( $objCompanyUser->getId(), $objVoipDatabase ) ) {
				$boolIsSuccess &= false;
			}

			return $boolIsSuccess;
		}

		$intIdleUpdatedOnTime = $objCallAgent->getUpdatedOn();

		// Calculate the Wrap-up time for an agent and set with call.
		$this->setWrapUpTime( strtotime( date( 'Y-m-d H:i:s' ) ) - strtotime( $intIdleUpdatedOnTime ) );

		$objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::WAITING );
		$objCallAgent->setLastOfferedCallId( NULL );
		$objAgent->setState( CAgent::STATE_WAITING );

		if( false == $objCallAgent->update( $objCompanyUser->getId(), $objVoipDatabase ) ) {
			$boolIsSuccess &= false;
		}

		$objFreeswitchDatabase->begin();

		if( false == $objAgent->update( $objCompanyUser->getId(), $objFreeswitchDatabase ) ) {
			$boolIsSuccess &= false;
			$objFreeswitchDatabase->rollback();
		}

		$objFreeswitchDatabase->commit();

		return $boolIsSuccess;
	}

	public function calcCallDetailsRecord( $objVoipDatabase ) {
		$strSql = 'WITH call_event_query AS
					(
						SELECT
							*,
							ROW_NUMBER() OVER( ORDER BY created_on ) AS call_event_order
						FROM
							call_events
							WHERE call_id = ' . ( int ) $this->getId() . ' )
				SELECT
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce5.updated_on - ce1.updated_on ) ) ), 0) AS call_duration_seconds,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce3.updated_on - ce2.updated_on ) ) ), 0) AS call_center_queue_time,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce5.updated_on - ce6.updated_on ) ) ), 0) AS call_center_voicemail_time,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce6.updated_on - ce2.updated_on ) ) ), 0) AS call_center_voicemail_queue_time,
					COALESCE( ceil(EXTRACT(EPOCH FROM (now() - ce2.updated_on ) ) ), 0) AS total_call_center_queue_time,
					COALESCE
					(
						(
							CASE WHEN ( ce4.id IS NOT NULL AND ce3.id IS NOT NULL )
									THEN ( ceil( EXTRACT(EPOCH FROM ( ce4.created_on - ce3.created_on ) ) ) )
									WHEN ( ce4.id IS NULL AND ce3.id IS NOT NULL )
									THEN ( ceil( EXTRACT(EPOCH FROM ( now() - ce3.created_on ) ) ) )
									ELSE 0
							END
						), 0
					) AS call_center_talk_time
				FROM calls c
					LEFT JOIN call_event_query AS ce1 ON (ce1.call_id = c.id AND ce1.call_event_type_id = ' . CCallEventType::CALL_START . ' )
					LEFT JOIN call_event_query AS ce2 ON (ce2.call_id = c.id AND ce2.call_event_type_id = ' . CCallEventType::IN_QUEUE . ' )
					LEFT JOIN call_event_query AS ce3 ON (ce3.call_id = c.id AND ce3.call_event_type_id = ' . CCallEventType::BRIDGE_START . ' OR ce3.call_event_type_id = ' . CCallEventType::BRIDGE_FAILED . ' )
					LEFT JOIN call_event_query AS ce4 ON (ce4.call_id = c.id AND ce4.call_event_type_id = ' . CCallEventType::BRIDGE_END . ' )
					LEFT JOIN call_event_query AS ce5 ON (ce5.call_id = c.id AND ce5.call_event_type_id = ' . CCallEventType::CALL_STOP . ' )
					LEFT JOIN call_event_query AS ce6 ON (ce6.call_id = c.id AND ce6.call_event_type_id = ' . CCallEventType::ROUTE_TO_VOICEMAIL . ' )
				WHERE
					c.id = ' . ( int ) $this->getId();

		$arrstrResponse = fetchData( $strSql, $objVoipDatabase );

		if( false == valArr( $arrstrResponse ) ) return;

		$arrstrResponse = $arrstrResponse[0];

		if( false == is_null( $arrstrResponse['call_duration_seconds'] ) )			$this->setCallDurationSeconds( $arrstrResponse['call_duration_seconds'] );
		if( false == is_null( $arrstrResponse['call_center_queue_time'] ) )			$this->setCallCenterQueueTime( $arrstrResponse['call_center_queue_time'] );
		if( false == is_null( $arrstrResponse['call_center_talk_time'] ) )			$this->setCallCenterTalkTime( $arrstrResponse['call_center_talk_time'] );
		if( false == is_null( $arrstrResponse['total_call_center_queue_time'] ) )	$this->setTotalCallCenterQueueTime( $arrstrResponse['total_call_center_queue_time'] );

		if( false == is_null( $arrstrResponse['call_center_voicemail_time'] ) && 0 < $arrstrResponse['call_center_voicemail_time'] ) {
			$this->setCallDurationSeconds( $arrstrResponse['call_center_voicemail_time'] );
			$this->setCallCenterTalkTime( $arrstrResponse['call_center_voicemail_time'] );
			$this->setCallCenterQueueTime( $arrstrResponse['call_center_voicemail_queue_time'] );
		}
		return;
	}

	/**
	 * Misc functions.
	 *
	 */

	public function getPsProductIdByCallType() {
		$intPsProductId = CPsProduct::CALL_TRACKER;
		if( true == in_array( $this->getCallTypeId(), CCallType::$c_arrintLeasingCenterBasicCallTypes ) ) {
			$intPsProductId = CPsProduct::LEASING_CENTER;
		} elseif( true == in_array( $this->getCallTypeId(), CCallType::$c_arrintCallTrackerCallTypes ) ) {
			$intPsProductId = CPsProduct::CALL_TRACKER;
		}

		return $intPsProductId;
	}

	public function getEventStatusTypeIdByCallStatusType() {
		if( CCallStatusType::CALLING == $this->getCallStatusTypeId() ) {
			return CCallStatusType::UNANSWERED;
		}

		return $this->getCallStatusTypeId();
	}

	public function sendMaintenanceEmergencyCall( $intPhysicalPhoneExtensionId ) {
		$objClickToCall = new CClickToCallLibrary();
		$boolIsCallSent = true;
		$strCallResponse = NULL;

		$objClickToCall->setDialplan( 'route_call_to_maintenance' );
		$objClickToCall->setContext( 'internal' );
		$objClickToCall->setResponseFormat( 'XML' );
		$objClickToCall->setOriginationCallerIdNumber( $this->getOriginPhoneNumber() );
		$objClickToCall->setTimeout( 45 );
		$objClickToCall->addParam( 'call_id', $this->getId() );
		$objClickToCall->addParam( 'call_type_name', CCallHelper::getCallTypeIdToName( $this->getCallTypeId() ) );
		$objClickToCall->addParam( 'client_id', $this->getCid() );
		$objClickToCall->addParam( 'property_id', $this->getPropertyId() );
		$objClickToCall->addParam( 'ignore_early_media', true );
		$objClickToCall->addParam( 'call_timeout', 45 );
		$objClickToCall->addParam( 'originate_timeout', 45 );
		$objClickToCall->addParam( 'originate_retries', 3 );
		$objClickToCall->addParam( 'loopback_bowout', false );
		$objClickToCall->addParam( 'instant_ringback', true );
		$objClickToCall->addParam( 'bridge_early_media', true );
		$objClickToCall->addParam( 'maintenance_phone_number', $this->getDestinationPhoneNumber() );

		$objXmlResponse = $objClickToCall->send( $intPhysicalPhoneExtensionId, $this->getDestinationPhoneNumber() );

		if( true == valObj( $objXmlResponse, 'PhpXmlRpc\Response' ) ) {
			if( true == valObj( $objXmlResponse->value(), 'PhpXmlRpc\Value' ) ) {
				if( true == in_array( trim( $objXmlResponse->value()->me['string'] ), CCallResponseType::$c_arrstrFailedCallResponseTypeNames ) ) {
					$boolIsCallSent &= false;
					$strCallResponse = $objXmlResponse->value()->me['string'];
				} else {
					$boolIsCallSent &= true;
					$strCallResponse = $objXmlResponse->value()->me['string'];
				}
			} else {
				$boolIsCallSent &= false;
			}
		} else {
			$boolIsCallSent &= false;
		}

		if( false == $boolIsCallSent ) {
			$this->setCallStatusTypeId( CCallStatusType::CALL_FAILED );
			$this->setCallNotes( $strCallResponse );
		}

		return $boolIsCallSent;
	}

	public function channelHangupComplete( $objEslEventMessage, $objVoipDatabase, $objFreeswitchDatabase, $objConnectDatabase ) {
		if( false == valObj( $objEslEventMessage, 'CEslEventMessage' ) ) {
			return false;
		}

		$objCallEvent = $this->createOrFetchCallEvent( $objVoipDatabase, CCallEventType::CALL_STOP );
		$objCallAgent = $this->fetchCallAgent( $objVoipDatabase );

		$objVoipDatabase->begin();

		if( false == $objCallEvent->insertOrUpdate( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		if( true == in_array( $this->getCallTypeId(), CCallType::$c_arrintIvrCallTypeIds ) ) {
			$intCallIvrQueueTime = $this->fetchCallIvrQueueTime( $objVoipDatabase );
			$this->setCallIvrQueueTime( $intCallIvrQueueTime );
		}

		$this->setCallStatusTypeId( CCallHelper::getAnswerStateToCallStatusTypeId( $this->getCallStatusTypeId() ) );
		$this->setCallQueueName( $objEslEventMessage->getCcQueue() );
		$this->setCallDurationSeconds( $objEslEventMessage->calcCallDurationSeconds() );
		$this->setCallCenterQueueTime( $objEslEventMessage->calcCallCenterQueueTime() );
		$this->setCallCenterTalkTime( $objEslEventMessage->calcCallCenterTalkTime() );
		$this->setDoNotRecordCall( $objEslEventMessage->getDoNotRecordCall() );
		$this->setIvrMenuActionId( $objEslEventMessage->getIvrMenuActionId() );

		if( false == $this->updateFreeswitchCall( $objVoipDatabase, $objConnectDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		if( true == valObj( $objCallAgent, 'CCallAgent' ) && ( true == $objCallAgent->isInAOutboundCall() || true == $objCallAgent->isDialing() ) ) {
			$objCallAgent->setCallAgentStateTypeId( CCallAgentStateType::IDLE );
			$objAgent = $objCallAgent->fetchAgent( $objFreeswitchDatabase );
			$objAgent->setState( CAgent::STATE_IDLE );

			$objFreeswitchDatabase->begin();

			if( false == $objCallAgent->update( CUser::ID_SYSTEM, $objVoipDatabase ) || false == $objAgent->update( CUser::ID_SYSTEM, $objFreeswitchDatabase ) ) {
				$objFreeswitchDatabase->rollback();
				return false;
			}

			$objFreeswitchDatabase->commit();
		}

		$objVoipDatabase->commit();
		return true;
	}

	public function getEventTypeIdByCallStatusType() {
		if( CCallStatusType::VOICEMAIL_LEFT == $this->getCallStatusTypeId() ) {
			return CEventType::VOICE_MAIL;
		} elseif( CCallStatusType::ANSWERED == $this->getCallStatusTypeId() ) {
			return CEventType::CALL_INCOMING;
		} else {
			return CEventType::CALL_INCOMING;
		}
	}

	public function getEventSubTypeIdByCallStatusType() {
		if( false == in_array( $this->getCallStatusTypeId(), [ CCallStatusType::ANSWERED, CCallStatusType::VOICEMAIL_LEFT ] ) ) {
			return CEventSubType::UNANSWERED_CALL;
		}
	}

	/**
	 * Boolean methods.
	 * @return bool
	 */
	public function isLeadCall() {
		return ( true == in_array( $this->getCallTypeId(), CCallType::$c_arrintLeadCallTypes ) ) ? true : false;
	}

	public function isAllowEntrataCallNotification() {
		$arrintCallStatusType = [
			CCallStatusType::CALLING
		];

		$arrintCallTypes = [
			CCallType::CALL_TRACKER_LEAD,
			CCallType::CALL_TRACKER_LEAD_VIA_IVR,
			CCallType::CALL_TRACKER_MAINTENANCE,
			CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR,
		];

		if( false == in_array( $this->getCallStatusTypeId(), $arrintCallStatusType ) ) {
			return false;
		}

		if( false == in_array( $this->getCallTypeId(), $arrintCallTypes ) ) {
			return false;
		}

		return true;
	}

	public function isLeasingCenterCall() {
		if( true == in_array( $this->getCallTypeId(), CCallType::$c_arrintLeasingCenterBasicCallTypes ) || true == valId( $this->getEmployeeId() ) ) {
			return true;
		}

		return false;
	}

	public function isCreateLeadFromUnknownCall() {
		if( CCallType::CALL_TRACKER_LEAD == $this->getCallTypeId()
		&& CCallStatusType::UNANSWERED == $this->getCallStatusTypeId()
		&& false == valId( $this->getApplicantApplicationId() )
		&& false == valId( $this->getCustomerId() )
		&& 0 == ( int ) $this->getIsTestCall()
		&& 0 == ( int ) $this->getIsAutoDetected() ) {
			return true;
		}
	}

	public function getIsCallAnalysisQueueCall() {
		if( CCallStatusType::ANSWERED == $this->getCallStatusTypeId()
		    && true == in_array( $this->getCallTypeId(), CCallTypeHelper::getCallTrackerCallTypes() )
		    && $this->getCallDurationSeconds() >= CCall::CALL_ANALYSIS_CALL_DURATION
		    && true == valId( $this->getCallFileId() ) ) {
			return true;
		}

		return false;
	}

	public function reloadCallServiceScores( $objVoipDatabase ) {
		if( false == valId( $this->getCid() ) ) {
			return false;
		}

		$objCompanyCallSetting = CCompanyCallSettings::fetchCompanyCallSettingByCid( $this->getCid(), $objVoipDatabase );

		if( false == valObj( $objCompanyCallSetting, 'CCompanyCallSetting' ) ) {
			return false;
		}

		$arrintCallStatusTypeIds = [ CCallStatusType::ANSWERED, CCallStatusType::UNANSWERED, CCallStatusType::CALL_FAILED, CCallStatusType::VOICEMAIL_LEFT ];

		$strSql = 'SELECT 
						(CASE WHEN total_calls > 0 THEN ( total_answered_calls * 100 / total_calls ) ELSE 0 END ) AS current_service_level
					FROM ( 
						SELECT
							( COUNT( CASE WHEN call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND call_center_queue_time <= 30 THEN id END ) ) AS total_answered_calls,
							COUNT( id ) AS total_calls
						FROM
							calls
						WHERE
							cid = ' . ( int ) $this->getCid() . '
						AND call_status_type_id IN ( ' . sqlIntImplode( $arrintCallStatusTypeIds ) . ' )
						AND call_type_id IN ( ' . sqlIntImplode( CCallType::$c_arrintLeasingCenterBasicCallTypes ) . ' )
						AND call_datetime >= CURRENT_DATE AND call_datetime < CURRENT_DATE + INTERVAL \'1 day\' 
					) AS sub_query';

		$arrintCurrentCallServiceLevels = fetchData( $strSql, $objVoipDatabase );

		$intCurrentServiceLevel = ( true == valArr( $arrintCurrentCallServiceLevels ) ) ? $arrintCurrentCallServiceLevels[0]['current_service_level'] : 0;

		$objCompanyCallServiceLevel = CCompanyCallServiceLevels::fetchCompanyCallServiceLevelByCid( $this->getCid(), $objVoipDatabase );

		if( false == valObj( $objCompanyCallServiceLevel, 'CCompanyCallServiceLevel' ) ) {
			$objCompanyCallServiceLevel = $objCompanyCallSetting->createCompanyCallServiceLevel();
		}

		$objCurrentDateTime	= ( new DateTime() )->getTimestamp();
		$objStartDateTime	= ( new DateTime( $objCompanyCallServiceLevel->getStartDate() ) )->getTimestamp();
		$objEndDateTime		= ( new DateTime( $objCompanyCallServiceLevel->getEndDate() ) )->getTimestamp();

		if( NULL != $objCompanyCallServiceLevel->getOverrideServiceLevel() && $objCurrentDateTime >= $objStartDateTime && $objCurrentDateTime <= $objEndDateTime ) {
			$objCompanyCallServiceLevel->setTargetedServiceLevel( $objCompanyCallServiceLevel->getOverrideServiceLevel() );
		} else {
			$objCompanyCallServiceLevel->setTargetedServiceLevel( $objCompanyCallSetting->getAgreementServiceLevel() );
		}

		if( false == ( $intEstimatedServiceLevel = CCallHelper::estimateBaseScore( $objCompanyCallServiceLevel->getTargetedServiceLevel(), $intCurrentServiceLevel, $objVoipDatabase ) ) ) {
			$intEstimatedServiceLevel = $objCompanyCallServiceLevel->getAssignedBaseScore();
		}

		$objCompanyCallServiceLevel->setAssignedBaseScore( $intEstimatedServiceLevel );
		$objCompanyCallServiceLevel->setCurrentServiceLevel( $intCurrentServiceLevel );

		$objVoipDatabase->begin();

		if( false == $objCompanyCallServiceLevel->insertOrUpdate( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			$objVoipDatabase->rollback();
			return false;
		}

		$objVoipDatabase->commit();

		return $objCompanyCallServiceLevel;
	}

	public function addCallAnalysisQueueCall( CDatabase $objVoipDatabase, $strCompanyEmployeeName = NULL ) {
		$objCallAnalysisCall = ( new CCallAnalysisCallLibrary() )->createCallAnalysisCall( $this, $strCompanyEmployeeName );

		if( false == valObj( $objCallAnalysisCall, 'CCallAnalysisCall' ) ) {
			return false;
		}

		if( false == $objCallAnalysisCall->insert( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $objClientDatabase = NULL ) {
		$boolIsSuccess = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == valObj( $objClientDatabase, 'CDatabase' ) ) {
			$boolIsSuccess &= $this->updateEventCall( $intCurrentUserId, $objClientDatabase, $objDatabase );
		}

		return $boolIsSuccess;
	}

	public function updateEventCall( $intCurrentUserId, CDatabase $objClientDatabase ) {
		$objEvent	= \Psi\Eos\Entrata\CEvents::createService()->fetchEventByDataReferenceIdByEventTypeIdByCid( $this->getId(), CEventType::CALL_INCOMING, $this->getCid(), $objClientDatabase );

		if( false == valObj( $objEvent, 'CEvent' ) ) return true;

		$objEvent->setDetailsField( 'event_call', $this->createEventCall()->toArray() );
		$objEvent->setSourcePsProductId( CPsProduct::CALL_TRACKER );
		$objEvent->setDestinationPsProductId( $this->getPsProductIdByCallType() );

		return $objEvent->update( $intCurrentUserId, $objClientDatabase );
	}

}

?>