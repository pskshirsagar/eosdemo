<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueues
 * Do not add any new functions to this class.
 */

class CCallQueues extends CBaseCallQueues {

	public static function fetchCachedCallQueue( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallQueue', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveCallQueues( $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.is_published = 1
						AND cq.deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchPublishedCallQueues( $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues cq, call_strategy_types AS cst
					WHERE
						cst.id = cq.call_strategy_type_id
						AND deleted_on IS NULL
						AND cq.is_published = 1
					ORDER BY
						name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchAllPublishedCallQueues( $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.is_published = 1
						AND cq.deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallQueues( $objVoipDatabase ) {
		return parent::fetchCallQueues( 'SELECT * FROM call_queues', $objVoipDatabase );
	}

	public static function fetchCallQueuesByCallAgentIds( $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						caq.id AS id,
						caq.call_agent_id AS call_agent_id,
						cq.name,
						cq.description
					FROM
						call_queues AS cq
						LEFT JOIN call_agent_queues AS caq ON ( caq.call_queue_id = cq.id )
						LEFT JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
					WHERE
						caq.call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueuesByIds( $arrintCallQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallQueueIds ) ) return NULL;
		$strSql = 'SELECT * FROM call_queues WHERE id IN ( ' . implode( ',', $arrintCallQueueIds ) . ' )';
		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchAssociateCallQueuesByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.id,
						cq.name,
						cq.queue_phone_extension_id,
						cq.domain,
						caq.level,
						caq.position,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						ca.domain AS call_agent_domain,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_queues AS cq
						JOIN call_agent_queues AS caq ON ( caq.call_queue_id = cq.id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
						LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						ca.id = ' . ( int ) $intCallAgentId . '
					GROUP BY
						cq.id,
						cq.name,
						cq.queue_phone_extension_id,
						cq.domain,
						caq.level,
						caq.position,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						call_agent_domain,
						call_agent_state_type_name,
						call_agent_status_type_name,
						caq.override_priority,
						caq.override_expiration,
						csq.priority';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueueByQueuePhoneExtensionId( $intQueuePhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM call_queues WHERE deleted_on IS NULL AND queue_phone_extension_id = ' . ( int ) $intQueuePhoneExtensionId;
		return self::fetchCallQueue( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleAssociatedCallQueueExtensions( $objVoipDatabase, $intCallQueueId = NULL ) {
		$strSql = 'SELECT DISTINCT queue_phone_extension_id FROM call_queues';

		$strSql .= ( true == valId( $intCallQueueId ) ) ? ' WHERE id <> ' . ( int ) $intCallQueueId : '';

		$arrstrCallQueueExtensions = fetchData( $strSql, $objVoipDatabase );

 		if( true == valArr( $arrstrCallQueueExtensions ) ) {
			foreach( $arrstrCallQueueExtensions as $arrstrCallQueueExtension ) {
				$arrstrRekeyedCallQueueExtensions[] = $arrstrCallQueueExtension['queue_phone_extension_id'];
			}
		}

		return $arrstrRekeyedCallQueueExtensions;
	}

	public static function fetchCallQueueByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*
					FROM
						call_queues cq
						JOIN call_agent_queues caq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents ca ON ( caq.call_agent_id = ca.id )
					WHERE
						ca.employee_id = ' . ( int ) $intEmployeeId . '
						AND cq.call_queue_type_id = ' . CCallQueueType::INDIVIDUAL . '
						AND cq.is_published  = 1
					LIMIT 1';

		return self::fetchCallQueue( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallQueuesByCallQueueTypeId( $intCallQueueTypeId, $objVoipDatabase ) {
		$arrintCallQueueIds = array();

		$arrmixResponses = ( array ) fetchData( 'SELECT id FROM call_queues WHERE call_queue_type_id = ' . ( int ) $intCallQueueTypeId, $objVoipDatabase );

		foreach( $arrmixResponses as $arrintResponse ) {
			$arrintCallQueueIds[$arrintResponse['id']] = $arrintResponse['id'];
		}

		return $arrintCallQueueIds;
	}

	public static function fetchDisabledAutoIdleStateCallQueueById( $intCallQueueId, $objVoipDatabase ) {
		return self::fetchCallQueue( 'SELECT * FROM call_queues WHERE is_auto_idle_state = 0 AND id = ' . ( int ) $intCallQueueId, $objVoipDatabase );
	}

	public static function fetchPublishedCallQueuesByCallQueueTypeId( $intCallQueueTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.call_queue_type_id = ' . ( int ) $intCallQueueTypeId . '
						AND cq.is_published = 1
						AND cq.deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueuesByDepartmentIds( $arrintDepartmentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;
		$strSql = 'SELECT * FROM call_queues WHERE is_published = 1 AND department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )';
		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallQueuesByCallQueueFilter( $objPagination, $objCallQueueFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		$strSql				= '';
		$strSqlOrderBy		= '';
		$strSqlOffsetLimit	= '';
		$arrstrWhere[]		= 'cq.deleted_by IS NULL';

		if( false == is_null( $objCallQueueFilter->getCallQueueIds() ) && true == valStr( $objCallQueueFilter->getCallQueueIds() ) ) {
			$arrstrWhere[] = 'cq.id IN ( ' . $objCallQueueFilter->getCallQueueIds() . ' )';
		}

		if( false == is_null( $objCallQueueFilter->getCallQueuePhoneExtensionIds() ) && true == valStr( $objCallQueueFilter->getCallQueuePhoneExtensionIds() ) ) {
			$arrstrWhere[] = 'cq.queue_phone_extension_id IN ( ' . $objCallQueueFilter->getCallQueuePhoneExtensionIds() . ' )';
		}

		if( false == is_null( $objCallQueueFilter->getDepartmentIds() ) && true == valStr( $objCallQueueFilter->getDepartmentIds() ) ) {
			$arrstrWhere[] = 'cq.department_id IN ( ' . $objCallQueueFilter->getDepartmentIds() . ' )';
		}

		if( false == is_null( $objCallQueueFilter->getCallStrategyTypeIds() ) && true == valStr( $objCallQueueFilter->getCallStrategyTypeIds() ) ) {
			$arrstrWhere[] = 'cq.call_strategy_type_id IN ( ' . $objCallQueueFilter->getCallStrategyTypeIds() . ' )';
		}

		if( false == is_null( $objCallQueueFilter->getCallQueueTypeIds() ) && true == valStr( $objCallQueueFilter->getCallQueueTypeIds() ) ) {
			$arrstrWhere[] = 'cq.call_queue_type_id IN ( ' . $objCallQueueFilter->getCallQueueTypeIds() . ' )';
		}

		if( false == is_null( $objCallQueueFilter->getCallQueueStatusTypeIds() ) && true == valStr( $objCallQueueFilter->getCallQueueStatusTypeIds() ) ) {
			$arrstrWhere[] = 'cq.is_published IN ( ' . $objCallQueueFilter->getCallQueueStatusTypeIds() . ' )';
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT COUNT( cq.id ) FROM call_queues cq';
		} else {
			$strSql				= 'SELECT
										cq.*,
										cst.name AS call_strategy_type_name
									FROM
										call_queues AS cq
										JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )';

			$strSqlOrderBy		= ' ORDER BY cq.name ASC';
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		if( false == is_null( $objCallQueueFilter->getGenericData() ) ) {
			$strGenericData = $objCallQueueFilter->getGenericData();
			$strSql .= ' AND ( CAST( cq.queue_phone_extension_id AS text ) LIKE \'' . addslashes( $strGenericData ) . '%\' OR  cq.name ILIKE \'%' . addslashes( $strGenericData ) . '%\' )';
		}

		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrstrData[0]['count'] ) ) ? $arrstrData[0]['count'] : 0;
		}

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueueByCallId( $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*
					FROM
						call_queues AS cq
						JOIN call_members AS cm ON ( cm.call_queue_id = cq.id )
					WHERE
						cm.call_id = ' . ( int ) $intCallId . '
					LIMIT 1';

		return self::fetchCallQueue( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueueByCallIdByCallAgentId( $intCallId, $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*
					FROM
						call_queues AS cq
						JOIN call_members AS cm ON ( cm.call_queue_id = cq.id )
					WHERE
						cm.call_id = ' . ( int ) $intCallId . '
						AND cm.call_agent_id = ' . ( int ) $intCallAgentId . '
					LIMIT 1';

		return self::fetchCallQueue( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueuesByCallQueueTypeIdByDepartmentIds( $intCallQueueTypeId, $arrintDepartmentIds, $objVoipDatabase ) {
		if( false == valId( $intCallQueueTypeId ) || false == valArr( $arrintDepartmentIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_queues WHERE is_published = 1 AND call_queue_type_id = ' . ( int ) $intCallQueueTypeId . ' AND  department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ORDER BY name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueuesByCallQueueTypeIdByDepartmentId( $intCallQueueTypeId, $intDepartmentId, $objVoipDatabase ) {
		if( false == valId( $intCallQueueTypeId ) || false == valId( $intDepartmentId ) ) return NULL;

		$strSql = 'SELECT * FROM call_queues WHERE is_published = 1 AND call_queue_type_id = ' . ( int ) $intCallQueueTypeId . ' AND department_id = ' . ( int ) $intDepartmentId . ' ORDER BY name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchCallQueues( $strSearchKeyword, $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.deleted_by IS NULL AND
						( CAST( cq.queue_phone_extension_id AS text ) LIKE \'' . addslashes( $strSearchKeyword ) . '%\' OR  lower( cq.name ) LIKE lower( \'%' . addslashes( $strSearchKeyword ) . '%\' ) )
					ORDER BY cq.name ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchDistinctCallQueuesAssoiatedWithCallQueueRequirements( $objVoipDatabase ) {
		$strSql = 'SELECT
						cq.*
					FROM
						call_queues AS cq
						JOIN call_queue_requirements AS cqr ON ( cq.id = cqr.call_queue_id )
					WHERE
						cq.is_published = 1
						AND cq.deleted_by IS NULL
					GROUP BY
						cq.id
					ORDER BY
						cq.name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchPublishedCallQueuesByCallQueueTypeIdByDepartmentId( $intCallQueueTypeId, $intDepartmentId, $objVoipDatabase ) {
		if( false == valId( $intCallQueueTypeId ) || false == valId( $intDepartmentId ) ) return NULL;

		$strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.call_queue_type_id = ' . ( int ) $intCallQueueTypeId . '
						AND	cq.department_id = ' . ( int ) $intDepartmentId . '
						AND cq.is_published = 1
						AND cq.deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueueByCallTypeIdByLanguage( $intCallTypeId, $strLanguage, $objVoipDatabase ) {
		return self::fetchCallQueue( 'SELECT *FROM call_queues WHERE call_type_id = ' . ( int ) $intCallTypeId . ' AND language = \'' . $strLanguage . '\' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchActiveCallQueuesByCallQueuePhoneExtensionIds( $arrintCallQueuePhoneExtensionIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallQueuePhoneExtensionIds ) || false == valStr( implode( ', ', $arrintCallQueuePhoneExtensionIds ) ) ) return NULL;

		$strSql = 'SELECT 
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.is_published = 1 
						AND cq.queue_phone_extension_id IN ( ' . implode( ', ', $arrintCallQueuePhoneExtensionIds ) . ' )';

		return self::fetchCallQueues( $strSql, $objVoipDatabase );
	}

    public function fetchActiveCallQueuesWithValidQueueExtensions( $objVoipDatabase ) {
        $strSql = 'SELECT
						cq.*,
						cst.name AS call_strategy_type_name
					FROM
						call_queues AS cq
						JOIN call_strategy_types AS cst ON ( cst.id = cq.call_strategy_type_id )
					WHERE
						cq.is_published = 1
						AND cq.deleted_by IS NULL
                        AND cq.queue_phone_extension_id IS NOT NULL
					ORDER BY
						name';

        return self::fetchCallQueues( $strSql, $objVoipDatabase );
    }

}
?>