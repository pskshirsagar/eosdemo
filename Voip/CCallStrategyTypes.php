<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallStrategyTypes
 * Do not add any new functions to this class.
 */

class CCallStrategyTypes extends CBaseCallStrategyTypes {

	public static function fetchCallStrategyTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallStrategyType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallStrategyType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallStrategyType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedCallStrategyTypes( $objVoipDatabase ) {
		return self::fetchCallStrategyTypes( 'SELECT * FROM call_strategy_types WHERE is_published = TRUE', $objVoipDatabase );
	}

}
?>