<?php

class CCallResult extends CBaseCallResult {

	const VOICE_MAIL											= 1;
	const UNKNOWN												= 2;
	const NO_RESULT												= 3;
	const TASK_CREATED											= 4;
	const SALE													= 5;
	const LEAD													= 6;
	const WORK_ORDER											= 7;
	const SOLICITOR												= 8;
	const VENDOR												= 9;
	const WRONG_NUMBER											= 10;
	const RESIDENT												= 11;
	const ABANDONED												= 12;
	const TEST_CALL												= 13;
	const RESIDENT_PAY_ENROLLED									= 14;
	const RESIDENT_PAY_REQUEST_MORE_INFO						= 15;
	const RESIDENT_PAY_DECLINED									= 16;
	const RESIDENT_PAY_WRONG_NUMBER								= 17;
	const RESIDENT_PAY_NO_ANSWER								= 18;
	const RESIDENT_PAY_LEFT_VOICEMAIL							= 19;
	const RESIDENT_INSURANCE_HAS_CURRENT_INSURANCE				= 20;
	const RESIDENT_INSURANCE_TRANSFERRED_TO_AN_AGENT			= 21;
	const RESIDENT_INSURANCE_REQUEST_CALL_BACK					= 22;
	const RESIDENT_INSURANCE_DECLINED							= 23;
	const RESIDENT_INSURE_WRONG_NUMBER							= 24;
	const RESIDENT_INSURE_NO_ANSWER								= 25;
	const RESIDENT_INSURE_LEFT_VOICEMAIL						= 26;
	const RESIDENT_PAY_RESIDENT_NOT_AVAILABLE					= 27;
	const RESIDENT_INSURE_RESIDENT_NOT_AVAILABLE				= 28;
	const SUPPORT												= 29;
	const OUTBOUND_SUPPORT_DIALER_VOICEMAIL						= 30;
	const OUTBOUND_SUPPORT_DIALER_WORNG_NUMBER					= 31;
	const OUTBOUND_SUPPORT_DIALER_NO_ANSWER						= 32;
	const OUTBOUND_SUPPORT_DIALER_NOT_AVAILABLE					= 33;
	const OUTBOUND_SUPPORT_DIALER_CONNECTED_ISSUE_RESOLVED		= 34;
	const OUTBOUND_SUPPORT_DIALER_CONNECTED_ISSUE_UNRESOLVED	= 35;
	const OUTBOUND_NPS_REFUSED									= 36;
	const OUTBOUND_NPS_ALREADY_RESPONDED						= 37;
	const OUTBOUND_NPS_NO_LONGER_WITH_COMPANY					= 38;
	const OUTBOUND_NPS_NOT_INTERESTED							= 39;
	const OUTBOUND_NPS_CALL_BACK_LATER							= 40;
	const OUTBOUND_NPS_NOT_AVAILABLE							= 41;
	const OUTBOUND_NPS_LEFT_VOICEMAIL							= 42;
	const OUTBOUND_NPS_WRONG_NUMBER								= 43;
	const OUTBOUND_NPS_NO_ANSWER								= 44;
	const OUTBOUND_NPS_RESPONDED_IN_CALL						= 46;
	const ENROLLMENT_AND_LOGIN									= 98;
	const NAVIGATING_PORTAL										= 99;
	const PAYMENT_BALANCE_ASSISTANCE							= 100;
	const PAYMENT_STATUS_AND_VOIDING							= 101;
	const RESIDENT_COMPLAINT									= 102;
	const RETURN_REASONS_AND_PAYMENT_FEES						= 103;
	const TROUBLESHOOTING_ERRORS_ISSUES							= 104;
	const RESIDENT_SUPPORT_UNKNOWN								= 105;
	const SIP_TEST_CALL											= 136;
	const LC_OUTBOUND_LEFT_VOICEMAIL							= 137;
	const LC_OUTBOUND_PROPERTY_CONFIRM_ISSUE					= 138;
	const LC_OUTBOUND_PROPERTY_REFUSE_SERVICE					= 139;
	const LC_OUTBOUND_INCORRECT_CONTACT							= 140;
	const MARKET_SURVEY											= 141;
	const COLD_LEAD												= 142;
	const WARM_LEAD												= 143;
	const SERVICE												= 144;

	public static $c_arrintNewCallResults	= [
		self::UNKNOWN		=> self::UNKNOWN,
		self::NO_RESULT		=> self::NO_RESULT,
		self::LEAD			=> self::LEAD,
		self::WORK_ORDER	=> self::WORK_ORDER,
		self::SOLICITOR		=> self::SOLICITOR,
		self::VENDOR		=> self::VENDOR,
		self::WRONG_NUMBER	=> self::WRONG_NUMBER,
		self::RESIDENT		=> self::RESIDENT,
		self::ABANDONED		=> self::ABANDONED,
		self::TEST_CALL		=> self::TEST_CALL,
		self::SUPPORT		=> self::SUPPORT,
		self::MARKET_SURVEY	=> self::MARKET_SURVEY
	];

	public static $c_arrintLeasingCenterOtherContactsCallResults = [
		self::NO_RESULT		=> self::NO_RESULT,
		self::SOLICITOR		=> self::SOLICITOR,
		self::VENDOR		=> self::VENDOR,
		self::WRONG_NUMBER	=> self::WRONG_NUMBER,
		self::ABANDONED		=> self::ABANDONED,
		self::TEST_CALL		=> self::TEST_CALL,
		self::MARKET_SURVEY	=> self::MARKET_SURVEY
	];

	public static $c_arrmixCallResults = [
		self::UNKNOWN		=> 'Unknown',
		self::NO_RESULT		=> 'No Result',
		self::LEAD			=> 'Lead',
		self::WORK_ORDER	=> 'Work Order',
		self::SOLICITOR		=> 'Solicitor',
		self::VENDOR		=> 'Vendor',
		self::WRONG_NUMBER	=> 'Wrong Number',
		self::RESIDENT		=> 'Resident',
		self::ABANDONED		=> 'Abandoned',
		self::TEST_CALL		=> 'Test Call'
	];

	public static $c_arrstrIvrCallResults = [
		self::LEAD			=> 'Lead',
		self::RESIDENT		=> 'Resident',
		self::WORK_ORDER	=> 'Work Order',
		self::SOLICITOR		=> 'Solicitor',
		self::VENDOR		=> 'Vendor',
		self::SUPPORT		=> 'Support',
	];

	public static $c_arrintCommonCallResultIds = [
		self::RESIDENT_PAY_LEFT_VOICEMAIL,
		self::RESIDENT_PAY_WRONG_NUMBER,
		self::RESIDENT_PAY_RESIDENT_NOT_AVAILABLE,
		self::RESIDENT_PAY_NO_ANSWER,
		self::RESIDENT_INSURE_LEFT_VOICEMAIL,
		self::RESIDENT_INSURE_WRONG_NUMBER,
		self::RESIDENT_INSURE_RESIDENT_NOT_AVAILABLE,
		self::RESIDENT_INSURE_NO_ANSWER
	];

	public static $c_arrintAllOtherCallResultIds = [
		self::UNKNOWN,
		self::NO_RESULT,
		self::SUPPORT,
		self::SOLICITOR,
		self::VENDOR,
		self::ABANDONED,
		self::WRONG_NUMBER,
		self::MARKET_SURVEY
	];

	public static $c_arrintResidentPortalEnrollmentCallResultIds = [
		self::RESIDENT_PAY_REQUEST_MORE_INFO,
		self::RESIDENT_PAY_DECLINED
	];

	public static $c_arrintResidentInsureEnrollmentCallResultIds = [
		self::RESIDENT_INSURANCE_TRANSFERRED_TO_AN_AGENT,
		self::RESIDENT_INSURANCE_REQUEST_CALL_BACK,
		self::RESIDENT_INSURANCE_DECLINED
	];

	public static $c_arrstrOutboundSupportDialerCallResults = [
		self::OUTBOUND_SUPPORT_DIALER_VOICEMAIL						=> 'Voicemail',
		self::OUTBOUND_SUPPORT_DIALER_WORNG_NUMBER					=> 'Wrong Number',
		self::OUTBOUND_SUPPORT_DIALER_NO_ANSWER						=> 'No Answer',
		self::OUTBOUND_SUPPORT_DIALER_NOT_AVAILABLE					=> 'Not Available',
		self::OUTBOUND_SUPPORT_DIALER_CONNECTED_ISSUE_RESOLVED		=> 'Connected - Issue Resolved',
		self::OUTBOUND_SUPPORT_DIALER_CONNECTED_ISSUE_UNRESOLVED	=> 'Connected - Issue Unresolved'
	];

	public static $c_arrintOutboundNpsCommonCallResultIds = [
		self::OUTBOUND_NPS_NOT_AVAILABLE,
		self::OUTBOUND_NPS_LEFT_VOICEMAIL,
		self::OUTBOUND_NPS_WRONG_NUMBER,
		self::OUTBOUND_NPS_NO_ANSWER
	];

	public static $c_arrintOtherCallResultIds = [
		self::VOICE_MAIL,
		self::TASK_CREATED,
		self::SALE,
		self::SOLICITOR,
		self::VENDOR,
		self::ABANDONED
	];

	public static $c_arrintLeasingCenterOutboundCallResults = [
		self::LC_OUTBOUND_LEFT_VOICEMAIL,
		self::LC_OUTBOUND_PROPERTY_CONFIRM_ISSUE,
		self::LC_OUTBOUND_PROPERTY_REFUSE_SERVICE,
		self::LC_OUTBOUND_INCORRECT_CONTACT
	];

	public static $c_arrintCallResultsNotToSendContactSubmissionEmail = [
		self::ABANDONED,
		self::NO_RESULT,
		self::TEST_CALL,
		self::WRONG_NUMBER
	];

	public static $c_arrintCallAnalysisCallResults	= [
		self::UNKNOWN		=> self::UNKNOWN,
		self::NO_RESULT		=> self::NO_RESULT,
		self::LEAD			=> self::LEAD,
		self::COLD_LEAD		=> self::COLD_LEAD,
		self::WARM_LEAD		=> self::WARM_LEAD,
		self::WORK_ORDER	=> self::WORK_ORDER,
		self::SOLICITOR		=> self::SOLICITOR,
		self::VENDOR		=> self::VENDOR,
		self::WRONG_NUMBER	=> self::WRONG_NUMBER,
		self::RESIDENT		=> self::RESIDENT,
		self::ABANDONED		=> self::ABANDONED,
		self::TEST_CALL		=> self::TEST_CALL,
		self::SUPPORT		=> self::SUPPORT,
		self::MARKET_SURVEY	=> self::MARKET_SURVEY
	];

	public static $c_arrintCallLogCallResults	= [
		self::UNKNOWN		=> self::UNKNOWN,
		self::NO_RESULT		=> self::NO_RESULT,
		self::LEAD			=> self::LEAD,
		self::WORK_ORDER	=> self::WORK_ORDER,
		self::SOLICITOR		=> self::SOLICITOR,
		self::VENDOR		=> self::VENDOR,
		self::WRONG_NUMBER	=> self::WRONG_NUMBER,
		self::RESIDENT		=> self::RESIDENT,
		self::ABANDONED		=> self::ABANDONED,
		self::TEST_CALL		=> self::TEST_CALL,
		self::SUPPORT		=> self::SUPPORT,
		self::MARKET_SURVEY	=> self::MARKET_SURVEY,
		self::SALE			=> self::SALE,
		self::SERVICE		=> self::SERVICE
	];

	protected $m_strCallResultNameWithPsProductName;

	public function setCallResultNameWithPsProductName( $strPsProductName ) {
		$this->m_strCallResultNameWithPsProductName = $this->getName() . ' (' . $strPsProductName . ') ';
	}

	public function getCallResultNameWithPsProductName() {
		return $this->m_strCallResultNameWithPsProductName;
	}

}
?>
