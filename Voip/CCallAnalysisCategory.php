<?php

class CCallAnalysisCategory extends CBaseCallAnalysisCategory {

	protected $m_intCategoryTotalWeight;
	protected $m_intCategoryAchievedWeight;
	protected $m_arrobjCallAnalysisQuestions;

	 /**
	  * Get Functions
	  */

	public function getCallAnalysisQuestions() {
		return $this->m_arrobjCallAnalysisQuestions;
	}

	public function getCategoryTotalWeight() {
		return $this->m_intCategoryTotalWeight;
	}

	public function getCategoryAchievedWeight() {
		return $this->m_intCategoryAchievedWeight;
	}

	 /**
	  * Set Functions
	  */

	public function setCallAnalysisQuestions( $arrobjCallAnalysisQuestions ) {
		$this->m_arrobjCallAnalysisQuestions = $arrobjCallAnalysisQuestions;
	}

	public function setCategoryTotalWeight( $intCategoryTotalWeight ) {
		$this->m_intCategoryTotalWeight = ( int ) $intCategoryTotalWeight;
	}

	public function setCategoryAchievedWeight( $intCategoryAchievedWeight ) {
		$this->m_intCategoryAchievedWeight = ( int ) $intCategoryAchievedWeight;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		if( true == isset( $arrmixValues['category_achieved_weight'] ) ) $this->setCategoryAchievedWeight( $arrmixValues['category_achieved_weight'] );
		if( true == isset( $arrmixValues['category_total_weight'] ) ) $this->setCategoryTotalWeight( $arrmixValues['category_total_weight'] );

		if( true == isset( $arrmixValues['call_analysis_questions'] ) ) {

			$arrobjCallAnalysisQuestions = [];

			if( true == valStr( $arrmixValues['call_analysis_questions'] ) ) {
				$arrmixCallAnalysisQuestions = json_decode( $arrmixValues['call_analysis_questions'], true );

				foreach( $arrmixCallAnalysisQuestions AS $arrstrCallAnalysisQuestion ) {
					$objCallAnalysisQuestion = new CCallAnalysisQuestion();
					$objCallAnalysisQuestion->setValues( $arrstrCallAnalysisQuestion );
					$arrobjCallAnalysisQuestions[$objCallAnalysisQuestion->getId()] = $objCallAnalysisQuestion;
				}

			}
			$this->setCallAnalysisQuestions( $arrobjCallAnalysisQuestions );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
	}

	/**
	 * Add Functions
	 */

	public function addCallAnalysisQuestion( $objCallAnalysisQuestion ) {
		$this->m_arrobjCallAnalysisQuestions[$objCallAnalysisQuestion->getId()] = $objCallAnalysisQuestion;
	}

	 /**
	  * Validation Functions
	  */

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_by', 'Provide details on user.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;

		if( true == is_null( $this->getDeletedOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deleted_on', 'Provide details on delete date.' ) );
		}

		return $boolIsValid;
	}

	public function valConflictCategoryName( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Improper database object provided.', E_USER_ERROR );
		}

		if( 0 < CCallAnalysisCategories::fetchCountCallAnalysisCategoryByNameByCallAnalysisScorecardIdByCid( $this->getName(), $this->getCallAnalysisScorecardId(), $this->getCid(), $objVoipDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Category name is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valConflictCategoryName( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeletedBy();
				$boolIsValid &= $this->valDeletedOn();
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function createCallAnalysisQuestion() {
		$objCallAnalysisQuestion = new CCallAnalysisQuestion();
		$objCallAnalysisQuestion->setCid( $this->getCid() );
		$objCallAnalysisQuestion->setCallAnalysisCategoryId( $this->getId() );

		return $objCallAnalysisQuestion;
	}

	public function createOrFetchCallAnalysisQuestion( $intCallAnalysisScorecardId, $arrstrQuestion, $objDatabase ) {
		if( true == valId( $arrstrQuestion['question_id'] ) ) {
			$objCallAnalysisQuestion = CCallAnalysisQuestions::fetchCallAnalysisQuestionById( $arrstrQuestion['question_id'], $objDatabase );
			$objCallAnalysisQuestion->setDescription( $arrstrQuestion['question_title'] );
		} else {
			$objCallAnalysisQuestion = new CCallAnalysisQuestion();
			$objCallAnalysisQuestion->setCid( $this->getCid() );
			$objCallAnalysisQuestion->setCallAnalysisCategoryId( $this->getId() );
			$objCallAnalysisQuestion->setDescription( $arrstrQuestion['question_title'] );
			$objCallAnalysisQuestion->setCallAnalysisQuestionTypeId( $arrstrQuestion['question_type'] );
			$objCallAnalysisQuestion->setWeight( 0 );
			$objCallAnalysisQuestion->setCallAnalysisScorecardId( $intCallAnalysisScorecardId );
		}

		return $objCallAnalysisQuestion;
	}

	public function fetchCallAnalysisQuestions( $objVoipDatabase ) {
		return $this->m_arrobjCallAnalysisQuestions = CCallAnalysisQuestions::fetchCallAnalysisQuestionsByCallAnalysisCategoryIdByCid( $this->getId(), $this->getCid(), $objVoipDatabase );
	}

}
?>