<?php

class CCallServiceLevel extends CBaseCallServiceLevel {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssignedServiceLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferencedBaseScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEvaluatedServiceLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>