<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallers
 * Do not add any new functions to this class.
 */

class CCallers extends CBaseCallers {

	public static function fetchCallerByCallerPhoneNumberByName( $strCallerPhoneNumber, $strName, $objVoipDatabase ) {
		$strSql = 'SELECT 
						c.* 
					FROM 
						callers c
					JOIN caller_associations AS ca ON ( ca.caller_id = c.id )
					JOIN caller_phone_numbers cpn ON ( ca.caller_phone_number_id = cpn.id )
					WHERE
						cpn.caller_phone_number = \'' . $strCallerPhoneNumber . '\'
						AND first_name || last_name = \'' . addslashes( $strName ) . '\'
					LIMIT 1';

		return self::fetchCaller( $strSql, $objVoipDatabase );
	}

	public static function fetchCallerByCallerPhoneNumber( $strCallerPhoneNumber, $objVoipDatabase ) {
		$strSql = 'SELECT 
						c.* 
					FROM 
						callers c
					JOIN caller_associations AS ca ON ( ca.caller_id = c.id )
					JOIN caller_phone_numbers cpn ON ( ca.caller_phone_number_id = cpn.id )
					WHERE
						cpn.caller_phone_number LIKE \'%' . $strCallerPhoneNumber . '\'
					ORDER BY created_on DESC
					LIMIT 1';

		return self::fetchCaller( $strSql, $objVoipDatabase );
	}

}
?>