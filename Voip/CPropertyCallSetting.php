<?php

class CPropertyCallSetting extends CBasePropertyCallSetting {

	protected $m_intClientDatabaseId;
	protected $m_arrstrCallRoutingConditions;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getCallRoutingConditions() {
		if( true == isset( $this->m_arrstrCallRoutingConditions ) ) {
			return $this->m_arrstrCallRoutingConditions;
		}

		$this->m_arrstrCallRoutingConditions = [
			'NONE'				=> __( 'None' ),
			'ALL'				 => __( 'All Calls (24/7) ' ),
			'AFTER_HOURS'		 => __( 'All Calls After Hours Only' ),
			'DURING_HOURS'		=> __( 'All Calls During Hours Only' ),
			'UNANSWERED'		  => __( 'Unanswered During Hours Only' ),
			'UNANSWERED_EXTENDED' => __( 'Unanswered During Hours plus All Calls After hours' )
		];

		return $this->m_arrstrCallRoutingConditions;
	}

	const CALL_CENTER_FORM_TYPE_LEAD				= 21;
	const CALL_CENTER_FORM_TYPE_MAINTENANCE_REQUEST = 22;
	const CALL_CENTER_FORM_TYPE_RESIDENT			= 24;
	const CALL_CENTER_FORM_TYPE_OTHER			   = 25;
	const SECONDS_TO_RING_PHONE					 = 30;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['client_database_id'] ) ) $this->setClientDatabaseId( $arrmixValues['client_database_id'] );

		return;
	}

	/*
	 * Set Functions.
	 */

	public function setClientDatabaseId( $intClientDatabaseId ) {
		$this->m_intClientDatabaseId = $intClientDatabaseId;

		return;
	}

	public function setDefaults() {
		$this->setUsePsiVoicemail( 0 );
		$this->setSecondsToRingPhone( self::SECONDS_TO_RING_PHONE );
		$this->setLeadRoutingCondition( 'NONE' );
		$this->setMaintenanceRoutingCondition( 'NONE' );
		$this->setMaintenanceEmergencyRoutingCondition( 'NONE' );

		return;
	}

	public function setDefaultPropertyOfficeHours() {
		$this->setMonOpenTime( NULL );
		$this->setMonCloseTime( NULL );

		$this->setTueOpenTime( NULL );
		$this->setTueCloseTime( NULL );

		$this->setWedOpenTime( NULL );
		$this->setWedCloseTime( NULL );

		$this->setThuOpenTime( NULL );
		$this->setThuCloseTime( NULL );

		$this->setFriOpenTime( NULL );
		$this->setFriCloseTime( NULL );

		$this->setSatOpenTime( NULL );
		$this->setSatCloseTime( NULL );

		$this->setSunOpenTime( NULL );
		$this->setSunCloseTime( NULL );

		return;
	}

	public function setPropertyOfficeHours( $objClientDatabase, $arrobjProperttyOfficeHours = NULL ) {
		if( true == is_null( $arrobjProperttyOfficeHours ) ) {
			$arrobjProperttyOfficeHours = CPropertyHours::fetchPropertyHoursByPropertyHourTypeByPropertyIdByCid( CPropertyHourType::OFFICE_HOURS, $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		}

		if( false == valArr( $arrobjProperttyOfficeHours ) ) {
			return;
		}

		foreach( $arrobjProperttyOfficeHours as $objPropertyOfficeHour ) {
			switch( $objPropertyOfficeHour->getDay() ) {
				case 1:
					$this->setMonOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setMonCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 2:
					$this->setTueOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setTueCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 3:
					$this->setWedOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setWedCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 4:
					$this->setThuOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setThuCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 5:
					$this->setFriOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setFriCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 6:
					$this->setSatOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setSatCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				case 7:
					$this->setSunOpenTime( $objPropertyOfficeHour->getOpenTime() );
					$this->setSunCloseTime( $objPropertyOfficeHour->getCloseTime() );
					break;

				default:
					// default case
					break;
			}
		}
	}

	public function setPropertyCallTrackerPreferences( $objClientDatabase, $arrobjCallTrackerPropertyPreferences = NULL ) {
		if( true == is_null( $arrobjCallTrackerPropertyPreferences ) ) {
			$arrstrPropertyPreferencesKeys = [
				'ACTIVATE_PROPERTY_SOLUTIONS_VOICEMAIL',
				'VOICEMAIL_ANSWER_DELAY_SECONDS',
				'REDIRECT_AFTER_HOURS_LEAD_CALL',
				'REDIRECT_AFTER_HOURS_MAINTENANCE_CALL',
				'REDIRECT_AFTER_HOURS_EMERGENCY_MAINTENANCE_CALL',
				'REDIRECT_DURING_HOURS_LEAD_CALL',
				'REDIRECT_DURING_HOURS_MAINTENANCE_CALL',
				'REDIRECT_DURING_HOURS_EMERGENCY_MAINTENANCE_CALL'
			];

			$arrobjCallTrackerPropertyPreferences = CPropertyPreferences::fetchPropertyPreferencesByKeysByPropertyIdByCid( $arrstrPropertyPreferencesKeys, $this->getPropertyId(), $this->getCid(), $objClientDatabase );
		}

		$strPropertySolutionsCallCenterLeadRoutingCondition				 = NULL;
		$strPropertySolutionsCallCenterMaintenanceRoutingCondition		  = NULL;
		$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = NULL;

		if( true == valArr( $arrobjCallTrackerPropertyPreferences ) ) {
			$arrobjCallTrackerPropertyPreferences = rekeyObjects( 'Key', $arrobjCallTrackerPropertyPreferences );

			if( true == array_key_exists( 'ACTIVATE_PROPERTY_SOLUTIONS_VOICEMAIL', $arrobjCallTrackerPropertyPreferences ) ) {
				$this->setUsePsiVoicemail( 1 );
			} else {
				$this->setUsePsiVoicemail( 0 );
			}

			if( true == array_key_exists( 'VOICEMAIL_ANSWER_DELAY_SECONDS', $arrobjCallTrackerPropertyPreferences ) ) {
				if( false == is_null( $arrobjCallTrackerPropertyPreferences['VOICEMAIL_ANSWER_DELAY_SECONDS']->getValue() ) && 0 < $arrobjCallTrackerPropertyPreferences['VOICEMAIL_ANSWER_DELAY_SECONDS']->getValue() ) {
					$this->setSecondsToRingPhone( $arrobjCallTrackerPropertyPreferences['VOICEMAIL_ANSWER_DELAY_SECONDS']->getValue() );
				}
			}

			// Lead Settings
			if( true == array_key_exists( 'REDIRECT_DURING_HOURS_LEAD_CALL', $arrobjCallTrackerPropertyPreferences ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_LEAD_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_LEAD_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterLeadRoutingCondition = 'ALL';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterLeadRoutingCondition = 'UNANSWERED_EXTENDED';
				}
			}

			if( true == is_null( $strPropertySolutionsCallCenterLeadRoutingCondition ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_LEAD_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$strPropertySolutionsCallCenterLeadRoutingCondition = 'AFTER_HOURS';
			} elseif( true == is_null( $strPropertySolutionsCallCenterLeadRoutingCondition ) && true == array_key_exists( 'REDIRECT_DURING_HOURS_LEAD_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_LEAD_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterLeadRoutingCondition = 'DURING_HOURS';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterLeadRoutingCondition = 'UNANSWERED';
				}
			}

			// Maintenanace Settings
			if( true == array_key_exists( 'REDIRECT_DURING_HOURS_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_MAINTENANCE_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'ALL';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'UNANSWERED_EXTENDED';
				}
			}

			if( true == is_null( $strPropertySolutionsCallCenterMaintenanceRoutingCondition ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'AFTER_HOURS';
			} elseif( true == is_null( $strPropertySolutionsCallCenterMaintenanceRoutingCondition ) && true == array_key_exists( 'REDIRECT_DURING_HOURS_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_MAINTENANCE_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'DURING_HOURS';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'UNANSWERED';
				}
			}

			// Emergency Maintenanace Settings
			if( true == array_key_exists( 'REDIRECT_DURING_HOURS_EMERGENCY_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_EMERGENCY_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_EMERGENCY_MAINTENANCE_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'ALL';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'UNANSWERED_EXTENDED';
				}
			}

			if( true == is_null( $strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition ) && true == array_key_exists( 'REDIRECT_AFTER_HOURS_EMERGENCY_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'AFTER_HOURS';

			} elseif( true == is_null( $strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition ) && true == array_key_exists( 'REDIRECT_DURING_HOURS_EMERGENCY_MAINTENANCE_CALL', $arrobjCallTrackerPropertyPreferences ) ) {
				$objPropertyPreference = $arrobjCallTrackerPropertyPreferences['REDIRECT_DURING_HOURS_EMERGENCY_MAINTENANCE_CALL'];

				if( 1 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'DURING_HOURS';
				} elseif( 2 == $objPropertyPreference->getValue() ) {
					$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'UNANSWERED';
				}
			}
		}

		if( true == is_null( $strPropertySolutionsCallCenterLeadRoutingCondition ) ) {
			$strPropertySolutionsCallCenterLeadRoutingCondition = 'NONE';
		}

		if( true == is_null( $strPropertySolutionsCallCenterMaintenanceRoutingCondition ) ) {
			$strPropertySolutionsCallCenterMaintenanceRoutingCondition = 'NONE';
		}

		if( true == is_null( $strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition ) ) {
			$strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition = 'NONE';
		}

		$this->setLeadRoutingCondition( $strPropertySolutionsCallCenterLeadRoutingCondition );
		$this->setMaintenanceRoutingCondition( $strPropertySolutionsCallCenterMaintenanceRoutingCondition );
		$this->setMaintenanceEmergencyRoutingCondition( $strPropertySolutionsCallCenterMaintenanceEmergencyRoutingCondition );

		return;
	}

	public function setPropertyPhoneNumbers( $objClientDatabase, $arrobjPropertyPhoneNumbers = NULL ) {
		if( true == is_null( $arrobjPropertyPhoneNumbers ) ) {
			$arrintPhoNumberTypeIds	 = [ CPhoneNumberType::OFFICE, CPhoneNumberType::MAINTENANCE, CPhoneNumberType::MAINTENANCE_EMERGENCY ];
			$arrobjPropertyPhoneNumbers = CPropertyPhoneNumbers::fetchPublishedPropertyPhoneNumbersByPropertyIdPhoneNumberTypeIdsByCid( $this->getPropertyId(), $arrintPhoNumberTypeIds, $this->getCid(), $objClientDatabase );
		}

		if( false == valArr( $arrobjPropertyPhoneNumbers ) ) {
			return;
		}

		$arrobjPropertyPhoneNumbers = reKeyObjects( 'PhoneNumberTypeId', $arrobjPropertyPhoneNumbers );

		if( true == array_key_exists( CPhoneNumberType::OFFICE, $arrobjPropertyPhoneNumbers ) ) {
			$this->setOfficePhoneNumber( $this->formatPhoneNumber( $arrobjPropertyPhoneNumbers[CPhoneNumberType::OFFICE]->getPhoneNumber() ) );
		}

		if( true == array_key_exists( CPhoneNumberType::MAINTENANCE, $arrobjPropertyPhoneNumbers ) ) {
			$this->setMaintenancePhoneNumber( $this->formatPhoneNumber( $arrobjPropertyPhoneNumbers[CPhoneNumberType::MAINTENANCE]->getPhoneNumber() ) );
		}

		if( true == array_key_exists( CPhoneNumberType::MAINTENANCE_EMERGENCY, $arrobjPropertyPhoneNumbers ) ) {
			$this->setMaintenanceEmergencyPhoneNumber( $this->formatPhoneNumber( $arrobjPropertyPhoneNumbers[CPhoneNumberType::MAINTENANCE_EMERGENCY]->getPhoneNumber() ) );
		}

		return;
	}

	/**
	 * @deprecated Doesn't handle international phone number formatting.
	 * @see		\i18n\CPhoneNumber::format() should be used.
	 */
	public function formatPhoneNumber( $strPhoneNumber ) {
		return preg_replace( '/\D/', '', $strPhoneNumber );
	}

	public function formatOfficePhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getOfficePhoneNumber() );
	}

	public function formatMaintenancePhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getMaintenancePhoneNumber() );
	}

	public function formatMaintenanceEmergencyPhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getMaintenanceEmergencyPhoneNumber() );
	}

	public function formatAfterHoursOfficePhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getAfterHoursOfficePhoneNumber() );
	}

	public function formatAfterHoursMaintenancePhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getAfterHoursMaintenancePhoneNumber() );
	}

	public function formatAfterHoursMaintenanceEmergencyPhoneNumber() {
		return preg_replace( '/\s+/', '', $this->getAfterHoursMaintenanceEmergencyPhoneNumber() );
	}

	/**
	 * Get Functions.
	 */

	public function getClientDatabaseId() {
		return $this->m_intClientDatabaseId;
	}

	public function getCallRoutingDescription( $intCallCenterFormType = self::CALL_CENTER_FORM_TYPE_LEAD ) {
		$strRoutingDescription = NULL;

		if( self::CALL_CENTER_FORM_TYPE_LEAD == $intCallCenterFormType ) {
			$strRoutingCondition = $this->getLeadRoutingCondition();
		} else {
			$strRoutingCondition = $this->getMaintenanceRoutingCondition();
		}

		switch( $strRoutingCondition ) {
			case 'ALL':
			case 'DURING_HOURS':
				$strRoutingDescription = __( 'Leasing Center handles all calls' );
				break;

			case 'AFTER_HOURS':
				$strRoutingDescription = __( 'Leasing Center handles after hours calls for this property' );
				break;

			case 'UNANSWERED':
			case 'UNANSWERED_EXTENDED':
				$strRoutingDescription = __( 'Leasing center handles unanswered calls for this property' );
				break;

			default:
				// default case
				break;
		}

		return $strRoutingDescription;
	}

	public function getFreeswitchFormatBuisnessHours( $strWeekDayName ) {
		$strFormatBuisnessHours = NULL;

		$strOpenTimeGetFunction  = 'get' . $strWeekDayName . 'OpenTime';
		$strCloseTimeGetFunction = 'get' . $strWeekDayName . 'CloseTime';

		if( true == is_null( $this->$strOpenTimeGetFunction() ) && true == is_null( $this->$strCloseTimeGetFunction() ) ) {
			return;
		}

		$strOpenTime  = strtotime( \Psi\CStringService::singleton()->substr( $this->$strOpenTimeGetFunction(), 0, 5 ) );
		$strCloseTime = strtotime( \Psi\CStringService::singleton()->substr( $this->$strCloseTimeGetFunction(), 0, 5 ) );

		$strOpenTimeHour  = date( 'H', $strOpenTime );
		$strCloseTimeHour = date( 'H', $strCloseTime );

		$strOpenTimeMins  = date( 'i', $strOpenTime );
		$strCloseTimeMins = date( 'i', $strCloseTime );

		$strFormatBuisnessHours .= ( $strOpenTimeHour !== $strCloseTimeHour ) ? ( $strOpenTimeHour . '[' . ( int ) ( $strOpenTimeMins / 10 ) . '-5][' . ( int ) ( $strOpenTimeMins % 10 ) . '-9]|' ) : '';

		$strTempOpenTimeHours = $strOpenTime;

		for( $intCnt = $strOpenTimeHour; $intCnt < ( $strCloseTimeHour - 1 ); $intCnt++ ) {
			$strTempOpenTimeHours   = strtotime( '+ 1 hour', $strTempOpenTimeHours );
			$strFormatBuisnessHours .= date( 'H', $strTempOpenTimeHours ) . '[0-5][0-9]|';
		}

		if( 0 !== ( int ) ( $strCloseTimeMins % 11 ) && 0 !== ( int ) ( $strCloseTimeMins % 10 ) ) {
			$strFormatBuisnessHours .= $strCloseTimeHour . '[0-' . ( int ) ( $strCloseTimeMins / 10 ) . '][0-' . ( int ) ( $strCloseTimeMins % 10 ) . ']';

			return $strFormatBuisnessHours;
		}

		for( $intCnt = 0; $intCnt < $strCloseTimeMins; $intCnt++ ) {
			$strFormatBuisnessHours .= $strCloseTimeHour . sprintf( '%\'.02d|', $intCnt );
		}

		$strFormatBuisnessHours .= $strCloseTimeHour . $strCloseTimeMins;

		return $strFormatBuisnessHours;
	}

	public function getPhoneNumberByCallTypeId( $intCallTypeId, $strIsoCode = 'US' ) {
		switch( $intCallTypeId ) {
			case CCallType::CALL_TRACKER_LEAD:
				$strPhoneNumber = $this->getOfficePhoneNumber();
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY:
				$strPhoneNumber = $this->getMaintenanceEmergencyPhoneNumber();
				break;

			case CCallType::CALL_TRACKER_MAINTENANCE:
				$strPhoneNumber = $this->getMaintenancePhoneNumber();
				break;

			default:
				$strPhoneNumber = $this->getOfficePhoneNumber();
				break;
		}

		return $strPhoneNumber;
	}

	/**
	 * @param	  $intCurrentUserId
	 * @param	  $objVoipDatabase
	 * @param null $objClientDatabase
	 * @param null $objClonedPropertyCallSetting
	 * @param bool $boolReturnSqlOnly
	 * @return bool
	 */
	public function insert( $intCurrentUserId, $objVoipDatabase, $objClientDatabase = NULL, $objClonedPropertyCallSetting = NULL, $boolReturnSqlOnly = false ) {
		if( false == parent::insert( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == valObj( $objClonedPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$this->log( $intCurrentUserId, $objClonedPropertyCallSetting, $objClientDatabase, $strAction = 'INSERT' );
		}

		return true;
	}

	public function update( $intCurrentUserId, $objVoipDatabase, $objClientDatabase = NULL, $objClonedPropertyCallSetting = NULL, $boolReturnSqlOnly = false ) {
		if( false == parent::update( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == valObj( $objClonedPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$this->log( $intCurrentUserId, $objClonedPropertyCallSetting, $objClientDatabase, $strAction = 'UPDATE' );
		}

		return true;
	}

	public function insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $objClientDatabase = NULL, $objClonedPropertyCallSetting = NULL, $boolReturnSqlOnly = false ) {
		if( false == parent::insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		if( true == valObj( $objClientDatabase, 'CDatabase' ) && true == valObj( $objClonedPropertyCallSetting, 'CPropertyCallSetting' ) ) {
			$strAction = 'INSERT';

			if( false == is_null( $this->getId() ) ) {
				$strAction = 'UPDATE';
			}

			$this->log( $intCurrentUserId, $objClonedPropertyCallSetting, $objClientDatabase, $strAction );
		}

		return true;
	}

	public function log( $intCurrentUserId, $objClonedPropertyCallSetting, $objClientDatabase, $strAction = 'INSERT' ) {
		if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return;
		}

		$arrstrPropertyCallSettingLogfields = [ 'Client Id', 'Property Id', 'Property Name', 'Office Phone Number', 'Maintenance Phone Number', 'Maintenance Emergency Phone Number', 'After Hours Office Phone Number', 'After Hours Maintenance Phone Number', 'After Hours Maintenance Emergency Phone Number', 'Lead Routing Condition', 'Maintenance Routing Condition', 'Maintenance Emergency Routing Condition', 'Is Guest Card Routing To Psi', 'Seconds To Ring Phone', 'Is Send Voicemail To Property', 'Use Psi Voicemail', 'Timezone Offset', 'Is Auto Archive Call', 'Is Office Forwarding', 'Sun Open Time', 'Sun Close Time', 'Mon Open Time', 'Mon Close Time', 'Tue Open Time', 'Tue Close Time', 'Wed Open Time', 'Wed Close Time', 'Thu Open Time', 'Thu Close Time', 'Fri Open Time', 'Fri Close Time', 'Sat Open Time', 'Sat Close Time' ];

		if( false == valArr( $arrstrPropertyCallSettingLogfields ) ) {
			return;
		}

		$strOldPropertyCallSettingValue = '';
		$strNewPropertyCallSettingValue = '';

		foreach( $arrstrPropertyCallSettingLogfields as $strField => $strFieldValue ) {
			$strGetFunction = preg_replace( '/\s+/', '', $strFieldValue );
			$strGetFunction = 'get' . $strGetFunction;
			$strNewKeyValue = '';
			$strOldKeyValue = '';

			if( true == method_exists( 'CPropertyCallSetting', $strGetFunction ) ) {
				$strNewKeyValue = $this->$strGetFunction();

				if( true == valObj( $objClonedPropertyCallSetting, 'CPropertyCallSetting' ) ) {
					$strOldKeyValue = $objClonedPropertyCallSetting->$strGetFunction();

					if( \Psi\CStringService::singleton()->strlen( trim( $strOldKeyValue ) ) > 0 ) {
						if( preg_match( '/OpenTime/', $strGetFunction ) || preg_match( '/CloseTime/', $strGetFunction ) ) {
							if( false != \Psi\CStringService::singleton()->strrpos( $strOldKeyValue, '-' ) ) {
								$strOldKeyValue = \Psi\CStringService::singleton()->substr( $strOldKeyValue, 0, \Psi\CStringService::singleton()->strrpos( $strOldKeyValue, ':' ) );
							}

							if( false != \Psi\CStringService::singleton()->strrpos( $strNewKeyValue, '-' ) ) {
								$strNewKeyValue = \Psi\CStringService::singleton()->substr( $strNewKeyValue, 0, \Psi\CStringService::singleton()->strrpos( $strNewKeyValue, ':' ) );
							}
						}
					}
				}

				if( 0 != \Psi\CStringService::singleton()->strcasecmp( $strNewKeyValue, $strOldKeyValue ) ) {
					$strNewPropertyCallSettingValue .= $strFieldValue . '::' . $strNewKeyValue . '~^~';
					$strOldPropertyCallSettingValue .= $strFieldValue . '::' . $strOldKeyValue . '~^~';
				}
			}
		}

		if( 0 != \Psi\CStringService::singleton()->strcasecmp( $strOldPropertyCallSettingValue, $strNewPropertyCallSettingValue ) ) {

			$objTableLog					   = new CTableLog();
			$strPropertyCallSettingDiscription = ( 'UPDATE' == $strAction ) ? __( 'Property Call Setting has been modified.' ) : __( 'Property Call Setting has been inserted.' );

			if( false == $objTableLog->insert( $intCurrentUserId, $objClientDatabase, 'property_call_settings', $this->getPropertyId(), $strAction, $this->getCid(), $strOldPropertyCallSettingValue, $strNewPropertyCallSettingValue, $strPropertyCallSettingDiscription ) ) {
				trigger_error( __( 'Failed to insert in Table log.' ), E_USER_ERROR );
			}
		}

		return;
	}

	public function createGreeting() {
		$objGreeting = new CGreeting();

		$objGreeting->setPropertyId( $this->getPropertyId() );
		$objGreeting->setCid( $this->getCid() );

		return $objGreeting;
	}

	/**
	 * Fetch Functions.
	 */

	public function fetchClient( $objVoipDatabase ) {
		return CClients::fetchClientById( $this->m_intCid, $objVoipDatabase );
	}

	public function fetchCompanyCallSetting( $objVoipDatabase ) {
		return CCompanyCallSettings::fetchCompanyCallSettingByCid( $this->getCid(), $objVoipDatabase );
	}

}

?>