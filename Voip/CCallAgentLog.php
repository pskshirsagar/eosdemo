<?php

class CCallAgentLog extends CBaseCallAgentLog {

	protected $m_strCallAgentName;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_strCallAgentStatusStateFormattedDuration;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;
	protected $m_strCallAgentSubStatusName;

	/**
	 * Setter functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_agent_name'] ) )				$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['call_agent_status_type_name'] ) )	$this->setCallAgentStatusTypeName( $arrmixValues['call_agent_status_type_name'] );
		if( true == isset( $arrmixValues['call_agent_state_type_name'] ) )		$this->setCallAgentStateTypeName( $arrmixValues['call_agent_state_type_name'] );
		if( true == isset( $arrmixValues['agent_phone_extension_id'] ) )		$this->setAgentPhoneExtensionId( $arrmixValues['agent_phone_extension_id'] );
		if( true == isset( $arrmixValues['physical_phone_extension_id'] ) )	$this->setPhysicalPhoneExtensionId( $arrmixValues['physical_phone_extension_id'] );
		if( true == isset( $arrmixValues['call_agent_sub_status_name'] ) )		$this->setCallAgentSubStatusName( $arrmixValues['call_agent_sub_status_name'] );
		if( true == isset( $arrmixValues['call_agent_status_state_formatted_duration'] ) )	$this->setCallAgentStatusStateFormattedDuration( $arrmixValues['call_agent_status_state_formatted_duration'] );

		return;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = CStrings::strTrimDef( $strCallAgentName, NULL, NULL, true );
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = CStrings::strTrimDef( $strCallAgentStatusTypeName, NULL, NULL, true );
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = CStrings::strTrimDef( $strCallAgentStateTypeName, NULL, NULL, true );
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->m_intAgentPhoneExtensionId = $intAgentPhoneExtensionId;
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->m_intPhysicalPhoneExtensionId = $intPhysicalPhoneExtensionId;
	}

	public function setCallAgentSubStatusName( $strCallAgentSubStatusName ) {
		$this->m_strCallAgentSubStatusName = $strCallAgentSubStatusName;
	}

	public function setCallAgentStatusStateFormattedDuration( $strCallAgentStatusStateFormattedDuration ) {
		$this->m_strCallAgentStatusStateFormattedDuration = $strCallAgentStatusStateFormattedDuration;
	}

	/**
	 * Getter functions
	 */


	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function getCallAgentSubStatusName() {
		return $this->m_strCallAgentSubStatusName;
	}

	public function getCallAgentStatusStateFormattedDuration() {
		return $this->m_strCallAgentStatusStateFormattedDuration;
	}

	/**
	 * Validation function
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>