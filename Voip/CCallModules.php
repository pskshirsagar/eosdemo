<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallModules
 * Do not add any new functions to this class.
 */

class CCallModules extends CBaseCallModules {

	public static function fetchCallModule( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallModule', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>