<?php

class CCallAgentChallengeSchedule extends CBaseCallAgentChallengeSchedule {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentChallengeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>