<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCompanyCallServiceLevels
 * Do not add any new functions to this class.
 */

class CCompanyCallServiceLevels extends CBaseCompanyCallServiceLevels {

	public static function fetchCompanyCallServiceLevelByCid( $intCid, $objVoipDatbase ) {
		return self::fetchCompanyCallServiceLevel( 'SELECT *FROM company_call_service_levels WHERE cid = ' . ( int ) $intCid . ' LIMIT 1', $objVoipDatbase );
	}

	public static function fetchCompanyCallServiceLevelByTarget( $objVoipDatabase ) {
		return self::fetchCompanyCallServiceLevels( 'SELECT * FROM company_call_service_levels ORDER BY cid', $objVoipDatabase );
	}

	public static function fetchCompanyCallServiceLevelsByCids( $arrintCId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						*
					FROM 
						company_call_service_levels 
					WHERE
						cid IN ( ' . sqlIntImplode( $arrintCId ) . ')';

		return self::fetchCompanyCallServiceLevels( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchCompanyCallServiceLevels( $strSearchKeyword, $objVoipDatabase ) {
		$strSql = 'SELECT
						ccsl.*,
						c.company_name
					FROM
						company_call_service_levels AS ccsl
						JOIN clients AS c ON ( c.id = ccsl.cid )
					WHERE
						( lower( c.company_name ) LIKE lower( \'%' . addslashes( $strSearchKeyword ) . '%\' ) )
					ORDER BY c.company_name ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCompanyCallServiceLevels( $objPagination, $objVoipDatabase, $boolIsPriority, $boolIsFromQuickSearch = false, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$strWhereConditions = '';

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(ccsl.id)';
		} else {
			$strSql = 'SELECT
							ccsl.*,
							c.company_name AS company_name,
							ca.full_name AS call_agent_name';

			$strSqlOrderBy		= ' ORDER BY c.company_name';
			$strSqlOffsetLimit	= ( false == $boolIsFromQuickSearch ) ? ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize() : '';
		}

		$strSql .= ' FROM
						company_call_service_levels AS ccsl
						LEFT JOIN clients AS c ON ( c.id = ccsl.cid )
						LEFT JOIN call_agents ca ON ccsl.updated_by = ca.user_id';

		if( true == valStr( $objPagination->getQuickSearch() ) ) {

			$strWhereConditions = ' WHERE
										( lower( c.company_name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) )';
		}
		if( true == $boolIsPriority ) {

			$strWhereConditions = ' WHERE
										ccsl.override_service_level IS NULL
										OR ccsl.override_service_level = 0';
		}

		$strSql .= $strWhereConditions;
		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;
		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return parent::fetchCompanyCallServiceLevels( $strSql, $objVoipDatabase );
		}
	}

}
?>