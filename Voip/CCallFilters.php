<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFilters
 * Do not add any new functions to this class.
 */

class CCallFilters extends CBaseCallFilters {

	public static function fetchSimplePublishedCallZoneIdForLeasingCenterTvDisplay( $objVoipDatabase ) {
		$strSql = 'SELECT
						call_zone_id
					FROM
						call_filters
					WHERE
						call_filter_type_id = ' . CCallFilterType::LEASING_CENTER_TV_DISPLAY . '
						AND is_published = TRUE
					ORDER BY
						id
					LIMIT 1';

		$arrintCallZone = fetchData( $strSql, $objVoipDatabase );
		return ( true == valArr( $arrintCallZone ) ) ? $arrintCallZone[0]['call_zone_id'] : NULL;
	}

}
?>