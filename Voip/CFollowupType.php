<?php

class CFollowupType extends CBaseFollowupType {
	const GUEST_CARD			= 1;
	const REVIEW_RESPONSE		= 2;
	const SCHEDULED_APPOINTMENT = 3;

	public static $c_arrintFollowupTypeIds = [
		self::GUEST_CARD,
		self::REVIEW_RESPONSE
	];
}
?>