<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CMessageQueueTypes
 * Do not add any new functions to this class.
 */

class CMessageQueueTypes extends CBaseMessageQueueTypes {

	public static function fetchMessageQueueTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CMessageQueueType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchMessageQueueType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CMessageQueueType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>