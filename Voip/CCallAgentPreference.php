<?php

class CCallAgentPreference extends CBaseCallAgentPreference {

	const FORWARD_ALWAYS			= 'FORWARD_ALWAYS';
	const FORWRAD_AT_NO_ANSWER		= 'FORWARD_AT_NO_ANSWER';
	const FORWARD_AT_BUSY			= 'FORWARD_AT_BUSY';
	const IS_RECORD_OUTBOUND_CALL	= 'IS_RECORD_OUTBOUND_CALL';
	const IS_HR_APPROVED_OFF		= 'IS_HR_APPROVED_OFF';

	public static $c_arrstrCallAgentPreferenceKeys = array(
		'forward_always'			=> self::FORWARD_ALWAYS,
		'forward_at_no_answer'		=> self::FORWRAD_AT_NO_ANSWER,
		'forward_at_busy'			=> self::FORWARD_AT_BUSY,
		'is_record_outbound_call'	=> self::IS_RECORD_OUTBOUND_CALL
	);

	public function valValue() {
		$boolIsValid = true;

		if( true == valStr( $this->getValue() ) && self::IS_RECORD_OUTBOUND_CALL !== $this->getKey() ) {
			if( false == preg_match( '/^(\d{3}|\d{4}|\d{10})$/', $this->getValue() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Call Forwarding Option', \Psi\CStringService::singleton()->ucfirst( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->getKey() ) ) ) . ': Please provide a valid phone number or ' . CONFIG_COMPANY_NAME . ' phone extension.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valValue();
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>