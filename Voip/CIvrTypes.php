<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrTypes
 * Do not add any new functions to this class.
 */

class CIvrTypes extends CBaseIvrTypes {

	public static function fetchIvrTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CIvrType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchIvrType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CIvrType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllPublishedIvrTypes( $objVoipDatabase ) {
		return self::fetchIvrTypes( 'SELECT * FROM ivr_types WHERE is_published = true', $objVoipDatabase );
	}

}
?>