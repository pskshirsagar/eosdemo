<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CFollowupTypes
 * Do not add any new functions to this class.
 */

class CFollowupTypes extends CBaseFollowupTypes {

	public static function fetchFollowupTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CFollowupType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchFollowupType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CFollowupType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>