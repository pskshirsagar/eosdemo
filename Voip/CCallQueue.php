<?php

class CCallQueue extends CBaseCallQueue {

	protected $m_arrobjCallAgents;
	protected $m_arrobjCallAgentQueues;
	protected $m_arrobjActiveCallQueues;

	protected $m_strCallStrategyTypeName;
	protected $m_strCallAgentName;
	protected $m_strCallAgentExtension;
	protected $m_strCallAgentPhysicalExtension;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_strCallAgentDomain;

	protected $m_intCallAgentId;
	protected $m_intCallSkillQueuePriority;
	protected $m_intCallSkillQueueId;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;
	protected $m_intOverflowCallQueueId;
	protected $m_intPriority;
	protected $m_intLevel;
	protected $m_intPosition;

	const READY_STATE										= 'Ready';
	const IDLE_STATE										= 'Idle';
	const WAITING_STATE										= 'Waiting';
	const LEASING_ENGLISH									= 3;
	const LEASING_SPANISH									= 4;
	const MAINTENANCE_ENGLISH								= 5;
	const MAINTENANCE_SPANISH								= 6;
	const RESIDENT_SUPPORT_ENGLISH							= 7;
	const RESIDENT_SUPPORT_SPANISH							= 8;
	const GENERAL_RESIDENT_SUPPORT							= 9;
	const TRAINING											= 10;
	const RESIDENT_UTILITY									= 11;
	const TESTING_QA										= 12;
	const CONSUMER_REQUESTS									= 13;
	const TESTING_SUPPORT									= 14;
	const TESTING_LEASING_CENTER							= 18;
	const RESIDENT_INSURE_ENGLISH							= 16;
	const RESIDENT_INSURE_SPANISH							= 17;
	const PROPERTY_SUPPORT									= 19;
	const COLONIAL_SUPPORT									= 20;
	const EMAIL_QUEUE										= 21;
	const DEVELOPMENT										= 22;
	const RESIDENT_INSURE									= 16;
	const LEASING_RESIDENT_ENGLISH							= 33;
	const LEASING_RESIDENT_SPANISH							= 34;
	const UTILITY_BILLING									= 38;
	const MOVE_IN_CALLS										= 39;
	const TRAINING_MOVE_IN_CALLS							= 40;
	const NPS_CALLS											= 41;
	const TESTING											= 42;
	const SUPPORT_GENERAL_SUPPORT							= 92;
	const SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS				= 94;
	const SUPPORT_MARKETING_WEBSITE_PRODUCTS				= 95;
	const SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT	= 96;
	const SUPPORT_ACCOUNTING_ENTRATA_CORE					= 97;
	const SUPPORT_GID_WINDSOR								= 104;
	const RESIDENT_INSURE_ENGLISH_VOICEMAIL					= 105;
	const RESIDENT_INSURE_ENGLISH_GENERAL					= 106;
	const RESIDENT_INSURE_ENGLISH_NEW_POLICY				= 107;
	const RESIDENT_INSURE_ENGLISH_CURRENT_POLICY			= 108;
	const RESIDENT_INSURE_SPANISH_VOICMAIL					= 109;
	const RESIDENT_INSURE_SPANISH_GENERAL					= 110;
	const RESIDENT_INSURE_SPANISH_CURRENT_POLICY			= 111;
	const RESIDENT_INSURE_SPANISH_NEW_POLICY				= 112;
	const SUPPORT_UDR										= 113;
	const LEASING_CENTER_RESIDENT_SUPPORT_FROM_IVR			= 114;
	const LEASING_CENTER_RESIDENT_SUPPORT_ENGLISH			= 117;
	const SUPPORT_TIER_TWO									= 115;
	const RESIDENT_INSURE_UNLICENSED						= 228;
	const RESIDENT_INSURE_UNLICENSED_SPANISH				= 227;
	const LEASING_CENTER_CORPORATE_CARE_LINE				= 269;
	const LEASING_CENTER_STRATEGIC							= 149;
	const LEASING_CENTER_STUDENT_HOUSING					= 324;
	const PROPERTY_SUPPORT_EXTENSION						= 6201;
	const GENERAL_SUPPORT									= 6070;
	const CURRENT_RESIDENT_QUEUESTION						= 6071;
	const MARKETING_AND_WEBSITE_PRODUCT						= 6072;
	const ONLINE_LEASE_AND_APPLICATIONS						= 6073;
	const ENTRATA_CORE										= 6074;
	const SUPPORT_VOICEMAIL									= 6075;
	const INSIDE_SALES_NON_CURRENT_CLIENTS					= 6101;
	const LEASING_CENTER_EMAIL_QUEUE						= 6666;

	const SUPPORT_CORE_ACCOUNTS_PAYABLE						= 273;
	const SUPPORT_MARKETING_OTHER							= 278;
	const SUPPORT_RV_SCREENING								= 281;
	const SUPPORT_LEASING_OTHER								= 283;
	const SUPPORT_OTHER_SCREENING_VENDORS					= 284;
	const SUPPORT_RESIDENTS_PORTAL							= 288;
	const SUPPORT_RESIDENT_VERIFY							= 291;
	const SUPPORT_RESIENTS_MAINTENANCE						= 293;
	const SUPPORT_CORE_AFFORDABLE							= 296;
	const SUPPORT_CORE_AP									= 298;
	const SUPPORT_GREYSTAR_GENERAL							= 301;
	const SUPPORT_GREYSTAR_CORE								= 302;
	const SUPPORT_GREYSTAR_LEASING							= 303;
	const SUPPORT_GREYSTAR_RESIDENTS						= 304;
	const SUPPORT_GREYSTAR_MARKETING						= 305;
	const SUPPORT_INTERNATIONAL_GENERAL						= 350;
	const SUPPORT_INTERNATIONAL_LEASING						= 351;
	const SUPPORT_INTERNATIONAL_RESIDENTS					= 352;
	const SUPPORT_INTERNATIONAL_CORE						= 353;
	const SUPPORT_INTERNATIONAL_MARKETING					= 354;
	const SUPPORT_ACCOUNTS_PAYABLE							= 371;

	// Default call queue to route calls.
	const DEFAULT_CALL_QUEUE = '6201@sip2.entrata.com';

	public static $c_arrstrCallQueueStates							= array(
		self::READY_STATE	=> 'Ready',
		self::IDLE_STATE	=> 'Idle',
		self::WAITING_STATE	=> 'Waiting'
	);

	public static $c_arrstrSupportCallQueues						= array(
		self::COLONIAL_SUPPORT,
		self::SUPPORT_GENERAL_SUPPORT,
		self::SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS,
		self::SUPPORT_MARKETING_WEBSITE_PRODUCTS,
		self::SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT,
		self::SUPPORT_ACCOUNTING_ENTRATA_CORE,
		self::SUPPORT_GID_WINDSOR,
		self::SUPPORT_UDR,
		self::LEASING_CENTER_RESIDENT_SUPPORT_FROM_IVR,
		self::SUPPORT_TIER_TWO
	);

	public static $c_arrintDefaultCallQueues = [
		self::LEASING_ENGLISH,
		self::LEASING_SPANISH,
		self::LEASING_RESIDENT_ENGLISH,
		self::LEASING_RESIDENT_SPANISH,
		self::LEASING_CENTER_RESIDENT_SUPPORT_ENGLISH,
		self::RESIDENT_SUPPORT_SPANISH
	];

	public static $c_arrintARCallQueuesIds = [
		self::SUPPORT_CORE_ACCOUNTS_PAYABLE,
		self::SUPPORT_ACCOUNTS_PAYABLE,
	];

	public static $c_arrintAPCallQueuesIds = [
		self::SUPPORT_CORE_AFFORDABLE,
		self::SUPPORT_CORE_AP,
	];

	public static $c_arrintEHDCallQueuesIds = [
		self::SUPPORT_GREYSTAR_CORE,
		self::SUPPORT_GREYSTAR_GENERAL,
		self::SUPPORT_GREYSTAR_LEASING,
		self::SUPPORT_GREYSTAR_MARKETING,
		self::SUPPORT_GREYSTAR_RESIDENTS
	];

	public static $c_arrintMarketingCallQueuesIds = [
		self::SUPPORT_MARKETING_OTHER
	];

	public static $c_arrintLeasingCallQueuesIds = [
		self::SUPPORT_LEASING_OTHER
	];

	public static $c_arrintResidentsCallQueuesIds = [
		self::SUPPORT_RESIDENTS_PORTAL,
		self::SUPPORT_RESIENTS_MAINTENANCE
	];

	public static $c_arrintRVCallQueuesIds = [
		self::SUPPORT_RV_SCREENING,
		self::SUPPORT_OTHER_SCREENING_VENDORS,
		self::SUPPORT_RESIDENT_VERIFY
	];

	public static $c_arrintInternationalCallQueuesIds = [
		self::SUPPORT_INTERNATIONAL_CORE,
		self::SUPPORT_INTERNATIONAL_GENERAL,
		self::SUPPORT_INTERNATIONAL_LEASING,
		self::SUPPORT_INTERNATIONAL_MARKETING,
		self::SUPPORT_INTERNATIONAL_RESIDENTS
	];

	public function setDefaults() {
		$this->m_strDomain							= 'sip.entrata.com';
		$this->m_strTimeBaseScore					= 'system';
		$this->m_intTierRulesApply					= 0;
		$this->m_intTierRuleWaitSecond				= 1;
		$this->m_intTierRuleWaitMultiplyLevel		= 1;
		$this->m_intTierRuleNoAgentNoWait			= 1;
		$this->m_intAbandonedResumeAllowed			= 0;
		$this->m_intMaxWaitTime						= 0;
		$this->m_intMultiplyLevelWait				= 0;
		$this->m_intMaxWaitTimeWithNoAgent			= 300;
		$this->m_intDiscardAbandonedAfterCallers	= 0;
		$this->m_intIsPublished						= 1;

		return;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_agent_name'] ) )				$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['call_strategy_type_name'] ) )		$this->setCallStrategyTypeName( $arrmixValues['call_strategy_type_name'] );
		if( true == isset( $arrmixValues['call_agent_id'] ) )				$this->setCallAgentId( $arrmixValues['call_agent_id'] );
		if( true == isset( $arrmixValues['call_skill_queue_priority'] ) )	$this->setCallSkillQueuePriority( $arrmixValues['call_skill_queue_priority'] );
		if( true == isset( $arrmixValues['call_skill_queue_id'] ) ) 		$this->setCallSkillQueueId( $arrmixValues['call_skill_queue_id'] );
		if( true == isset( $arrmixValues['agent_phone_extension_id'] ) )	$this->setAgentPhoneExtensionId( $arrmixValues['agent_phone_extension_id'] );
		if( true == isset( $arrmixValues['physical_phone_extension_id'] ) )	$this->setPhysicalPhoneExtensionId( $arrmixValues['physical_phone_extension_id'] );
		if( true == isset( $arrmixValues['call_agent_state_type_name'] ) )	$this->setCallAgentStateTypeName( $arrmixValues['call_agent_state_type_name'] );
		if( true == isset( $arrmixValues['call_agent_status_type_name'] ) )	$this->setCallAgentStatusTypeName( $arrmixValues['call_agent_status_type_name'] );
		if( true == isset( $arrmixValues['call_agent_domain'] ) )			$this->setCallAgentDomain( $arrmixValues['call_agent_domain'] );
		if( true == isset( $arrmixValues['priority'] ) )					$this->setPriority( $arrmixValues['priority'] );
		if( true == isset( $arrmixValues['position'] ) ) 					$this->setPosition( $arrmixValues['position'] );
		if( true == isset( $arrmixValues['level'] ) ) 						$this->setLevel( $arrmixValues['level'] );

		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function setActiveCallQueues( $arrobjActiveCallQueues ) {
		$this->m_arrobjActiveCallQueues = $arrobjActiveCallQueues;
	}

	public function setCallStrategyTypeName( $strCallStrategyTypeName ) {
		$this->m_strCallStrategyTypeName = $strCallStrategyTypeName;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setCallAgentExtension( $strCallAgentExtension ) {
		$this->m_strCallAgentExtension = $strCallAgentExtension;
	}

	public function setCallAgentPhysicalExtension( $strCallAgentPhysicalExtension ) {
		$this->m_strCallAgentPhysicalExtension = $strCallAgentPhysicalExtension;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	public function setCallAgentDomain( $strCallAgentDomain ) {
		$this->m_strCallAgentDomain = $strCallAgentDomain;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCallSkillQueuePriority( $intCallSkillQueuePriority ) {
		$this->m_intCallSkillQueuePriority = $intCallSkillQueuePriority;
	}

	public function setCallSkillQueueId( $intCallSkillQueueId ) {
		$this->m_intCallSkillQueueId = $intCallSkillQueueId;
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->m_intAgentPhoneExtensionId = $intAgentPhoneExtensionId;
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->m_intPhysicalPhoneExtensionId = $intPhysicalPhoneExtensionId;
	}

	public function setOverflowCallQueueId( $intOverflowCallQueueId ) {
		parent::setOverflowCallQueueId( $intOverflowCallQueueId );
		$this->m_intOverflowCallQueueId = ( 0 < $this->m_intOverflowCallQueueId ) ? $this->m_intOverflowCallQueueId : NULL;
	}

	public function setPriority( $intPriority ) {
		$this->m_intPriority = $intPriority;
	}

	public function setLevel( $intLevel ) {
		$this->m_intLevel = $intLevel;
	}

	public function setPosition( $intPosition ) {
		$this->m_intPosition = $intPosition;
	}

	/**
	 * Get Functions
	 */

	public function getCallAgents() {
		return $this->m_arrobjCallAgents;
	}

	public function getCallAgentQueues() {
		return $this->m_arrobjCallAgentQueues;
	}

	public function getActiveCallQueues() {
		return $this->m_arrobjActiveCallQueues;
	}

	public function getCallStrategyTypeName() {
		return $this->m_strCallStrategyTypeName;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getCallAgentExtension() {
		return $this->m_strCallAgentExtension;
	}

	public function getCallAgentPhysicalExtension() {
		return $this->m_strCallAgentPhysicalExtension;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallAgentDomain() {
		return $this->m_strCallAgentDomain;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCallSkillQueuePriority() {
		return $this->m_intCallSkillQueuePriority;
	}

	public function getCallSkillQueueId() {
		return $this->m_intCallSkillQueueId;
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function getLevel() {
		return $this->m_intLevel;
	}

	public function getPosition() {
		return $this->m_intPosition;
	}

	/**
	 * Build Functions
	 */

	public function getBuildCallQueueName() {
		return $this->getQueuePhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME;
	}

	public function getFreeswitchFormatBuisnessHours( $strWeekDayName ) {
		$strFormatBuisnessHours = NULL;

		$strOpenTimeGetFunction = 'get' . $strWeekDayName . 'OpenTime';
		$strCloseTimeGetFunction = 'get' . $strWeekDayName . 'CloseTime';

		if( true == is_null( $this->$strOpenTimeGetFunction() ) && true == is_null( $this->$strCloseTimeGetFunction() ) ) {
			return;
		}

		$strOpenTime = strtotime( $this->$strOpenTimeGetFunction() );
		$strCloseTime = strtotime( $this->$strCloseTimeGetFunction() );

		$strOpenTimeHour = date( 'H', $strOpenTime );
		$strCloseTimeHour = date( 'H', $strCloseTime );

		$strOpenTimeMins = date( 'i', $strOpenTime );
		$strCloseTimeMins = date( 'i', $strCloseTime );

		$strFormatBuisnessHours .= $strOpenTimeHour . '[' . ( int ) ( $strOpenTimeMins / 10 ) . '-5][' . ( int ) ( $strOpenTimeMins % 10 ) . '-9]|';

		$strTempOpenTimeHours = $strOpenTime;

		for( $intCnt = $strOpenTimeHour; $intCnt < ( $strCloseTimeHour - 1 ); $intCnt++ ) {
			$strTempOpenTimeHours = strtotime( '+ 1 hour', $strTempOpenTimeHours );
			$strFormatBuisnessHours .= date( 'H', $strTempOpenTimeHours ) . '[0-5][0-9]|';
		}

		$strFormatBuisnessHours .= $strCloseTimeHour . '[0-' . ( int ) ( $strCloseTimeMins / 10 ) . '][0-' . ( int ) ( $strCloseTimeMins % 10 ) . ']';

		return $strFormatBuisnessHours;
	}

	/**
	 * Add Functions
	 */

	public function addCallAgent( $objCallAgent ) {
		$this->m_arrobjCallAgents[$objCallAgent->getId()] = $objCallAgent;
		return;
	}

	public function addCallAgentQueue( $objCallAgentQueue ) {
		$this->m_arrobjCallAgentQueues[$objCallAgentQueue->getId()] = $objCallAgentQueue;
		return;
	}

	/**
	 * Create Functions
	 */

	public function createCallAgentQueue() {
		$objCallAgentQueue = new CCallAgentQueue();

		$objCallAgentQueue->setDefaults();
		$objCallAgentQueue->setCallQueueId( $this->getId() );
		$objCallAgentQueue->setState( CTier::READY );
		$objCallAgentQueue->setLevel( CCallAgentSkill::DEFAULT_PRIORITY );
		$objCallAgentQueue->setPosition( CCallAgentSkill::DEFAULT_POSITION );
		$objCallAgentQueue->setQueuePhoneExtensionId( $this->getQueuePhoneExtensionId() );

		return $objCallAgentQueue;
	}

	public function createCallQueueRequirement( $intIntervalStartEpoch, $intIntervalEndEpoch = NULL ) {
		if( false == is_numeric( $intIntervalStartEpoch ) ) return NULL;

		$strIntervalStartTimestamp	= date( 'Y-m-d', $intIntervalStartEpoch );
		$strIntervalEndTimestamp	= is_numeric( $intIntervalEndEpoch ) ? date( 'Y-m-d', $intIntervalEndEpoch ) : NULL;
		$objCallQueueRequirement	= new CCallQueueRequirement();

		$objCallQueueRequirement->setCallQueueId( $this->getId() );
		$objCallQueueRequirement->setIntervalStartTimestamp( $strIntervalStartTimestamp );
		$objCallQueueRequirement->setIntervalStartEpoch( $intIntervalStartEpoch );
		$objCallQueueRequirement->setIntervalEndTimestamp( $strIntervalEndTimestamp );
		$objCallQueueRequirement->setIntervalEndEpoch( $intIntervalEndEpoch );
		$objCallQueueRequirement->setIsActive( true );

		return $objCallQueueRequirement;
	}

	public function getCallQueueIdToCallTypeId() {
		switch( $this->getId() ) {
			case self::RESIDENT_INSURE_ENGLISH_CURRENT_POLICY:
			case self::RESIDENT_INSURE_ENGLISH_GENERAL:
			case self::RESIDENT_INSURE_ENGLISH_NEW_POLICY:
			case self::RESIDENT_INSURE_ENGLISH_VOICEMAIL:
			case self::RESIDENT_INSURE_SPANISH_NEW_POLICY:
			case self::RESIDENT_INSURE_SPANISH_CURRENT_POLICY:
			case self::RESIDENT_INSURE_SPANISH_GENERAL:
			case self::RESIDENT_INSURE_SPANISH_VOICMAIL:
				$intCallTypeId = CCallType::RESIDENT_INSURE;
				break;

			default:
				$intCallTypeId = CCallType::CALL_CENTER;
				break;
		}

		return $intCallTypeId;
	}

	public function getOrFetchCallTypeId( $objVoipDatabase ) {
		$strSql = 'SELECT id FROM call_types WHERE default_call_queue_id = ' . ( int ) $this->getId();

		$arrmixResponses = fetchData( $strSql, $objVoipDatabase );

		return ( true == valArr( $arrmixResponses ) && true == valId( $arrmixResponses[0]['id'] ) ) ? $arrmixResponses[0]['id'] : CCallType::CALL_CENTER;
	}

	/**
	 * Validate Functions
	 */

	public function valConflictName( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( true == valId( $this->fetchCallQueueCountByName( $objVoipDatabase ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valExtension( $objVoipDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getQueuePhoneExtensionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Queue extension is required.' ) );
		} elseif( false == is_numeric( $this->getQueuePhoneExtensionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Queue extension should be in numeric.' ) );
		} elseif( true == valId( $this->fetchCallQueueCountByQueuePhoneExtensionId( $objVoipDatabase ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Queue extension is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valRecordTemplate() {
		$boolIsValid = true;

		if( false == valStr( $this->getRecordTemplate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'record_template', 'Call Queue Settings : Call recording path is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDomain() {
		$boolIsValid = true;

		if( false == valStr( $this->getDomain() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'domain', 'Domain is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallQueueTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallQueueTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_queue_type_id', 'Queue type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallStrategyTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallStrategyTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_strategy_type', 'Call routing strategy is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMohSound() {
		$boolIsValid = true;

		if( false == valStr( $this->getMohSound() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'moh_sound', 'Musical file path is missing.' ) );
		}

		return $boolIsValid;
	}

	public function valCallFile() {
		$boolIsValid = true;

		if( true == is_null( $this->getRecordTemplate() ) ) return $boolIsValid;
		if( true == is_null( $_FILES['call_queue']['name']['call_file'] ) || false == valStr( $_FILES['call_queue']['name']['call_file'] ) ) return $boolIsValid;

		$arrstrFileInfo	= pathinfo( $_FILES['call_queue']['name']['call_file'] );
		$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

		$arrstrFileTypes = array( 'wmv', 'wav', 'mp3' );

		if( true == valStr( $this->getRecordTemplate() ) && false == in_array( $strExtension, $arrstrFileTypes ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_file', 'Incorrect file format.' ) );
		}

		return $boolIsValid;
	}

	public function valCallQueueAssociations( $objVoipDatabase ) {
		$boolIsValid = true;

		$arrobjCallAgentQueues = $this->fetchCallAgentQueues( $objVoipDatabase );

		if( true == valArr( $arrobjCallAgentQueues ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_queue', 'Please deassociate call agents from this queue.' ) );
		}

		return $boolIsValid;
	}

	public function valActiveCallQueueAssociations( $objVoipDatabase ) {
		$boolIsValid = true;

		if( true == $this->getIsPublished() ) return $boolIsValid;

		$arrobjCallAgentQueues = $this->fetchActiveCallAgentQueues( $objVoipDatabase );

		if( true == valArr( $arrobjCallAgentQueues ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_queue', 'Please deassociate call agents from this queue.' ) );
		}

		return $boolIsValid;
	}

	public function valMaxWaitTimeInQueue() {
		$boolIsValid = true;

		if( false == $this->getIsOverflowEnabled() ) return $boolIsValid;

		if( true == is_null( $this->getMaxWaitTime() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_wait_time', 'Ringing time is required.' ) );
		} elseif( false == is_numeric( $this->getMaxWaitTime() ) || 0 >= $this->getMaxWaitTime() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_wait_time', 'Ringing time should be numeric or should be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valIsOverflowEnabled() {
		$boolIsValid = true;

		if( false == $this->getIsOverflowEnabled() ) return $boolIsValid;

		if( false == valId( $this->getOverflowCallQueueId() ) && false == $this->getIsVoicemail() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_overflow_enabled', 'Redirect unanswered calls to another queue is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished( $objVoipDatabase ) {
		$boolIsValid = true;
		$objCallQueues = $this->fetchCallQueuesByOverflowCallQueueId( $objVoipDatabase );

		if( false == valArr( $objCallQueues ) ) {
			return $boolIsValid;
		}

		$objCallQueues = array_keys( rekeyObjects( 'name', $objCallQueues ) );
		if( false == $this->getIsPublished() && true == valArr( $objCallQueues ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_published', 'Call queue already assigned as redirected queue for ' . implode(', ', $objCallQueues) . '.' ) );
		}

		return $boolIsValid;
	}

	public function valNotificationEmail() {
		$boolIsValid = true;

		if( true == is_null( $this->getNotificationEmail() ) ) return $boolIsValid;
		if( false == $this->getIsVoicemail() ) return $boolIsValid;

		if( false == CValidation::validateEmailAddresses( $this->getNotificationEmail() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'New Voicemail message notification email does not appear to be valid.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valTime() {
		$boolIsValid	= true;
		$arrstrWeekDays = array( 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' );

		if( false == $this->getIsCustomSettings() ) return $boolIsValid;

		foreach( $arrstrWeekDays as $strDay ) {
			$strOpenTime	= 'get' . $strDay . 'OpenTime';
			$strCloseTime	= 'get' . $strDay . 'CloseTime';

			if( ( 0 == strlen( trim( $this->$strOpenTime() ) ) && 0 != strlen( trim( $this->$strCloseTime() ) ) ) || ( 0 == strlen( trim( $this->$strCloseTime() ) ) && 0 != strlen( trim( $this->$strOpenTime() ) ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'If either ' . $strDay . ' open time or close time is set, the corresponding open or close time must also be set.' ) );
				$boolIsValid &= false;
			}

			if( true == valStr( $this->$strOpenTime() ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->$strOpenTime() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'open_time', $strDay . ' Open time improperly formatted.' ) );
				$boolIsValid &= false;
			}

			if( true == valStr( $this->$strCloseTime() ) && false == CValidation::validate24HourTimeWithoutSeconds( $this->$strCloseTime() ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'close_time', $strDay . ' Close time improperly formatted.' ) );
				$boolIsValid &= false;
			}

			$arrstrCloseTime	= explode( ':', $this->$strCloseTime() );
			$arrstrOpenTime		= explode( ':', $this->$strOpenTime() );

			if( true == valStr( $this->$strCloseTime() ) && true == valStr( $this->$strOpenTime() ) && true == valArr( $arrstrCloseTime ) && true == valArr( $arrstrOpenTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrCloseTime ) && 2 == \Psi\Libraries\UtilFunctions\count( $arrstrOpenTime ) ) {
				$intOpenTime 	= ( ( int ) ( $arrstrOpenTime[0] * 60 ) + ( int ) $arrstrOpenTime[1] );
				$intCloseTime 	= ( ( int ) ( $arrstrCloseTime[0] * 60 ) + ( int ) $arrstrCloseTime[1] );

				if( $intOpenTime > $intCloseTime ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', $strDay . ' Open time is greater than the close time.' ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valStaticWrapUpTime() {
		$boolIsValid = true;

		if( 0 > $this->getStaticWrapUpTime() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'static_wrap_up_time', 'Wrap up time should be equal to or greater that zero.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictName( $objVoipDatabase );
				$boolIsValid &= $this->valCallQueueTypeId();
				$boolIsValid &= $this->valExtension( $objVoipDatabase );
				$boolIsValid &= $this->valCallStrategyTypeId();
				$boolIsValid &= $this->valRecordTemplate();
				$boolIsValid &= $this->valMaxWaitTimeInQueue();
				$boolIsValid &= $this->valIsOverflowEnabled();
				$boolIsValid &= $this->valIsPublished( $objVoipDatabase );
				$boolIsValid &= $this->valNotificationEmail();
				$boolIsValid &= $this->valActiveCallQueueAssociations( $objVoipDatabase );
				$boolIsValid &= $this->valTime();
				$boolIsValid &= $this->valStaticWrapUpTime();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valCallQueueAssociations( $objVoipDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * fetch functions
	 */

	public function fetchCallAgentQueues( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_agent_queues WHERE call_queue_id = ' . ( int ) $this->getId();

		return CCallAgentQueues::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public function fetchActiveCallAgentQueues( $objVoipDatabase ) {
		$strSql = 'SELECT
						caq.*
					FROM
						call_agent_queues caq
						JOIN call_queues cq ON (cq.id = caq.call_queue_id)
					WHERE
						caq.call_queue_id = ' . ( int ) $this->getId() . '
						AND cq.is_published = 1';

		return CCallAgentQueues::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public function fetchCallQueueCountByName( $objVoipDatabase ) {
		$strWhere = ' WHERE name = \'' . $this->getName() . '\'';

		if( true == valId( $this->getId() ) ) {
			$strWhere .= ' AND id <> ' . ( int ) $this->getId();
		}

		return CCallQueues::fetchCallQueueCount( $strWhere, $objVoipDatabase );
	}

	public function fetchCallQueueCountByQueuePhoneExtensionId( $objVoipDatabase ) {
		$strWhere = ' WHERE deleted_on IS NULL AND queue_phone_extension_id = ' . $this->getQueuePhoneExtensionId();

		if( true == valId( $this->getId() ) ) {
			$strWhere .= ' AND id <> ' . ( int ) $this->getId();
		}

		return CCallQueues::fetchCallQueueCount( $strWhere, $objVoipDatabase );
	}

	public function fetchOverflowCallQueues( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_queues WHERE id <> ' . ( int ) $this->getId() . ' AND is_published = 1 AND deleted_on IS NULL';

		return CCallQueues::fetchCallQueues( $strSql, $objVoipDatabase );
	}

	public function fetchCallQueueCountByOverflowCallQueueId( $objVoipDatabase ) {
		$strWhere = ' WHERE overflow_call_queue_id = ' . ( int ) $this->getId() . ' AND ( deleted_on IS NULL OR deleted_by IS NULL )';

		return CCallQueues::fetchCallQueueCount( $strWhere, $objVoipDatabase );
	}

	public function fetchCallFile( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_files WHERE id = ' . ( int ) $this->getCallFileId();

		return CCallFiles::fetchCallFile( $strSql, $objVoipDatabase );
	}

	public function fetchAssociateCallAgents( $objVoipDatabase ) {
		return CCallAgents::fetchAssociateCallAgentsByCallQueueId( $this->getId(), $objVoipDatabase );
	}

	public function getOrFetchCustomCallQueueGreetingFilePath( $objVoipDatabase ) {
		$objCallFile = CCallFiles::fetchCallFileById( $this->getCallFileId(), $objVoipDatabase );
		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : CCallHelper::getGreetingTypeIdToStandardGreetingFilePath( CGreetingType::PROPERTY_LEAD );
	}

	public function fetchCallAgentQueue( $objVoipDatabase ) {
		return CCallAgentQueues::fetchCallAgentQueueByCallQueueId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCallQueuesByOverflowCallQueueId( $objVoipDatabase ) {
		$strWhere = 'SELECT * from call_queues WHERE overflow_call_queue_id = ' . ( int ) $this->getId() . ' AND ( deleted_on IS NULL OR deleted_by IS NULL )';

		return CCallQueues::fetchCallQueues( $strWhere, $objVoipDatabase );
	}
	/**
	 * Inherited functions
	 */

	public function update( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( false == parent::update( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {
			$arrobjTiers = ( array ) CTiers::fetchTiersByCallQueueId( $this->getId(), $objFreeswitchDatabase );

			foreach( $arrobjTiers as $objTier ) {
				$objTier->setQueue( $this->getBuildCallQueueName() );

				if( false == $objTier->update( $intCurrentUserId, $objFreeswitchDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Call Queue Freeswitch functions
	 */
	public function callStrategy() {
		return ( \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '-', $this->getCallStrategyTypeName() ) ) );
	}

	public function musicOnHold() {
		return ( true == valStr( $this->getMohSound() ) ? $this->getMohSound() : '$${hold_music}' );
	}

	public function announceSound() {
		return ( true == valStr( $this->getAnnounceSound() ) ? $this->getAnnounceSound() : '$${hold_music}' );
	}

	public function announceFrequency() {
		return ( true == valId( $this->getAnnounceFrequency() ) ? $this->getAnnounceFrequency() : '120' );
	}

	public function recordTemplate() {
		return ( true == valStr( $this->getRecordTemplate() ) ? $this->getRecordTemplate() : CCallFile::FREESWITCH_VOIP_MOUNTS . PATH_VOIP_MOUNTS_CALL_FILES . '${call_type_name}/${client_id}/${strftime(%Y)}/${strftime(%m)}/${strftime(%d)}/${call_id}.mp3' );
	}

	public function timeBaseScore() {
		return $this->getTimeBaseScore();
	}

	public function maxWaitTime() {
		return $this->getMaxWaitTime();
	}

	public function maxWaitTimeWithNoAgent() {
		return $this->getMaxWaitTimeWithNoAgent();
	}

	public function maxWaitTimeWithNoAgentTimeReached() {
		return $this->getMaxWaitTimeWithNoAgentTimeReached();
	}

	public function tierRulesApply() {
		return ( 1 == $this->getTierRulesApply() ? 'true' : 'false' );
	}

	public function tierRuleWaitSecond() {
		return $this->getTierRuleWaitSecond();
	}

	public function tierRuleWaitMultiplyLevel() {
		return ( 1 == $this->getTierRuleWaitMultiplyLevel() ? 'true' : 'false' );
	}

	public function tierRuleNoAgentNoWait() {
		return ( 1 == $this->getTierRuleNoAgentNoWait() ? 'true' : 'false' );
	}

	public function discardAbandonedAfterCallers() {
		return $this->getDiscardAbandonedAfterCallers();
	}

	public function abandonedResumeAllowed() {
		return ( 1 == $this->getAbandonedResumeAllowed() ? 'true' : 'false' );
	}

}
?>