<?php

class CCallStatusType extends CBaseCallStatusType {

	const WAITING_TO_SEND				= 1;
	const CALL_FAILED					= 2;
	const UNANSWERED					= 3;
	const ANSWERED						= 4;
	const CALLING						= 5;
	const VOICEMAIL_LEFT				= 6;
	const OFFERING_TO_DESTINATION		= 7; // Receiving.
	const CALL_IN_PROGRESS				= 8; // In Progress
	const IN_CALL_CENTER_QUEUE			= 9; // In a Queue Call.
	const ATTENDED_CALL_TRANSFER		= 10;
	const BLIND_CALL_TRANSFER			= 11;
	const CONFIRM_ANSWERED				= 12;

	const TWILIO_INITIATED      = 'initiated';
	const TWILIO_RINGING        = 'ringing';
	const TWILIO_IN_PROGRESS    = 'in-progress';
	const TWILIO_QUEUED         = 'queued';
	const TWILIO_COMPLETED      = 'completed';
	const TWILIO_BUSY           = 'busy';
	const TWILIO_FAILED         = 'failed';
	const TWILIO_NO_ANSWER      = 'no-answer';
	const TWILIO_CANCELED       = 'canceled';
	const CALL_RINGING          = 'Ringing';

	public static $c_arrintActiveCallStatusTypeIds = [
		self::CALLING,
		self::ATTENDED_CALL_TRANSFER,
		self::OFFERING_TO_DESTINATION,
		self::CALL_IN_PROGRESS,
		self::IN_CALL_CENTER_QUEUE
	];

	public static $c_arrintPendingCallStatusTypeIds = [
		self::CALLING,
		self::ANSWERED,
		self::UNANSWERED,
		self::CALL_FAILED,
		self::CALL_IN_PROGRESS,
	];

	public static $c_arrintInQueueCallStatusTypeIds = [
		self::IN_CALL_CENTER_QUEUE,
		self::OFFERING_TO_DESTINATION
	];

	public static $c_arrintNewCallStatusTypes = [
		self::ANSWERED			=> 'Answered',
		self::UNANSWERED		=> 'Unanswered',
		self::VOICEMAIL_LEFT	=> 'Voicemail Left'
	];

	public static $c_arrintCurrentCallStatusTypes = [
		self::CALLING					=> 'Calling',
		self::OFFERING_TO_DESTINATION	=> 'Offering To Destination',
		self::CALL_IN_PROGRESS			=> 'Call In Progress',
		self::IN_CALL_CENTER_QUEUE		=> 'In Call Center Queue'
	];

	public static $c_arrintCallInQueueCallStatusTypeIds = [
		self::ATTENDED_CALL_TRANSFER,
		self::IN_CALL_CENTER_QUEUE,
		self::OFFERING_TO_DESTINATION
	];

	public static $c_arrintCallStatusTypeToCallEventTypeId = [
		self::CALLING 					=> CCallEventType::STARTING,
		self::ATTENDED_CALL_TRANSFER 	=> CCallEventType::ROUTE_TO_LEASING_CENTER,
		self::IN_CALL_CENTER_QUEUE 		=> CCallEventType::ROUTE_TO_LEASING_CENTER,
		self::OFFERING_TO_DESTINATION	=> CCallEventType::AGENT_OFFERING,
		self::CALL_IN_PROGRESS 			=> CCallEventType::CALL_START,
		self::ANSWERED 					=> CCallEventType::CALL_STOP,
		self::UNANSWERED 				=> CCallEventType::CALL_STOP,
		self::VOICEMAIL_LEFT 			=> CCallEventType::ROUTE_TO_VOICEMAIL
	];

	public static $c_arrintHangupCallStatusTypeIds = [
		CCallStatusType::ANSWERED,
		CCallStatusType::UNANSWERED,
		CCallStatusType::VOICEMAIL_LEFT,
		CCallStatusType::CALL_FAILED
	];

	public static $c_arrmixTwilioCallStatusTypes = [
		'initiated'     => self::CALLING, // The outbound call initiated from twilio system.
		'ringing'       => self::CALLING, // The call is currently ringing.
		'in-progress'   => self::CALL_IN_PROGRESS, // The call was answered and is actively in progress.
		'queued'        => self::WAITING_TO_SEND, // The call is ready and waiting in line before going out.
		'completed'     => self::ANSWERED, // The call was answered and has ended NORMALLY.
		'busy'          => self::CALL_FAILED, // The caller received a busy signal. [SD: We don't have call status to map for Twilio Busy status.]
		'failed'        => self::CALL_FAILED, // The call could not be completed as dialed, most likey because the phone number was non-existent.
		'no-answer'     => self::UNANSWERED, // The call ended without being answered.
		'canceled'      => self::UNANSWERED // The call was canceled via the REST API while queued or ringing.
	];
}

?>