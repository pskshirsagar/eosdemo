<?php

class CRankPointMapping extends CBaseRankPointMapping {

	protected $m_strRankPointMappingColor;
	protected $m_intNextRankPointMappingId;
	protected $m_intRankingPointerPosition;
	protected $m_intNextRankPointValue;
	protected $m_intCallAgentRankCredits;
	protected $m_fltCallAgentTotalNumberOfPoints;
	protected $m_boolIsCurrentActiveRank;

	const RANK_COUNT			= 10;
	const RANK_DEFAULT_ID		= 1;
	const DEFAULT_RANK_NUMBER	= 1;
	const RANK_LAST_ID			= 11;

	const RANK_1_COLOR			= 1;
	const RANK_2_COLOR			= 2;
	const RANK_3_COLOR			= 3;
	const RANK_4_COLOR			= 4;
	const RANK_5_COLOR			= 5;
	const RANK_6_COLOR			= 6;
	const RANK_7_COLOR			= 7;
	const RANK_8_COLOR			= 8;
	const RANK_9_COLOR			= 9;
	const RANK_10_COLOR			= 10;
	const RANK_11_COLOR			= 11;

	public static $c_arrstrRankPointMappingColorNames = [
		self::RANK_1_COLOR	=> '#EDEDED',
		self::RANK_2_COLOR	=> '#D3D3D3',
		self::RANK_3_COLOR	=> '#C2C2C2',
		self::RANK_4_COLOR	=> '#ADADAD',
		self::RANK_5_COLOR	=> '#919191',
		self::RANK_6_COLOR	=> '#7F7F7F',
		self::RANK_7_COLOR	=> '#6A6A6A',
		self::RANK_8_COLOR	=> '#5D5D5D',
		self::RANK_9_COLOR	=> '#474747',
		self::RANK_10_COLOR	=> '#303030',
		self::RANK_11_COLOR	=> '#010101'
	];

	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['rank_number'] ) )							$this->setRankPointMappingColor( CRankPointMapping::$c_arrstrRankPointMappingColorNames[$this->getRankNumber()] );
		if( true == isset( $arrmixValues['next_rank_point_mapping_id'] ) )			$this->setNextRankPointMappingId( $arrmixValues['next_rank_point_mapping_id'] );
		if( true == isset( $arrmixValues['call_agent_total_number_of_points'] ) )	$this->setCallAgentTotalNumberOfPoints( $arrmixValues['call_agent_total_number_of_points'] );
		if( true == isset( $arrmixValues['next_rank_point_value'] ) )				$this->setNextRankPointValue( $arrmixValues['next_rank_point_value'] );
		if( true == isset( $arrmixValues['call_agent_rank_credits'] ) )				$this->setCallAgentRankCredits( $arrmixValues['call_agent_rank_credits'] );
		if( true == isset( $arrmixValues['is_current_active_rank'] ) )				$this->setIsCurrentActiveRank( $arrmixValues['is_current_active_rank'] );
		return true;
	}

	public function setRankPointMappingColor( $strRankPointMappingColor ) {
		$this->m_strRankPointMappingColor = $strRankPointMappingColor;
	}

	public function getRankPointMappingColor() {
		return $this->m_strRankPointMappingColor;
	}

	public function setNextRankPointMappingId( $intNextRankPointMappingId ) {
		$this->m_intNextRankPointMappingId = $intNextRankPointMappingId;
	}

	public function getNextRankPointMappingId() {
		return $this->m_intNextRankPointMappingId;
	}

	public function setRankingPointerPosition( $intRankingPointerPosition ) {
		return $this->m_intRankingPointerPosition = $intRankingPointerPosition;
	}

	public function getRankingPointerPosition() {
		return $this->m_intRankingPointerPosition;
	}

	public function setNextRankPointValue( $intNextRankPointValue ) {
		return $this->m_intNextRankPointValue = $intNextRankPointValue;
	}

	public function getNextRankPointValue() {
		return $this->m_intNextRankPointValue;
	}

	public function setCallAgentRankCredits( $intCallAgentRankCredits ) {
		$this->m_intCallAgentRankCredits = $intCallAgentRankCredits;
	}

	public function getCallAgentRankCredits() {
		return $this->m_intCallAgentRankCredits;
	}

	public function setCallAgentTotalNumberOfPoints( $fltCallAgentTotalNumberOfPoints ) {
		$this->m_fltCallAgentTotalNumberOfPoints = $fltCallAgentTotalNumberOfPoints;
	}

	public function getCallAgentTotalNumberOfPoints() {
		return $this->m_fltCallAgentTotalNumberOfPoints;
	}

	public function setIsCurrentActiveRank( $boolIsCurrentActiveRank ) {
		return $this->m_boolIsCurrentActiveRank = $boolIsCurrentActiveRank;
	}

	public function getIsCurrentActiveRank() {
		return $this->m_boolIsCurrentActiveRank;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRankNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPointValue( $intPreviousPointValue, $intNextPointValue ) {
		$boolIsValid = true;

		if( false == valId( $this->getPointValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'point_value', 'Please enter valid point value.' ) );
		}

		if( true == valId( $intPreviousPointValue ) && true == valId( $this->getPointValue() ) && $intPreviousPointValue >= $this->getPointValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'point_value', 'Point value of rank ' . $this->getRankNumber() . ' should be greater then previous rank point value.' ) );
		}

		if( true == valId( $intNextPointValue ) && true == valId( $this->getPointValue() ) && $intNextPointValue <= $this->getPointValue() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'point_value', 'Point value of rank ' . $this->getRankNumber() . ' should be less then next rank point value.' ) );
		}

		return $boolIsValid;
	}

	public function valCredits() {
		$boolIsValid = true;

		if( false == valId( $this->getCredits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit amount', 'Please enter valid credit amount.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intPreviousPointValue = NULL, $intNextPointValue = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert':
				$boolIsValid &= $this->valPointValue( $intPreviousPointValue, $intNextPointValue );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>