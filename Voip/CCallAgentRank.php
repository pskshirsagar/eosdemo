<?php

class CCallAgentRank extends CBaseCallAgentRank {

	protected $m_intRankNumber;
	protected $m_intMissingCallAgentId;
	protected $m_intPointValue;
	protected $m_intNextRankPointValue;
	protected $m_intTotalNumberOfCoins;
	protected $m_fltTotalNumberOfPoints;


	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['rank_number'] ) )				$this->setRankNumber( $arrmixValues['rank_number'] );
		if( true == isset( $arrmixValues['missing_call_agent_id'] ) )	$this->setMissingCallAgentId( $arrmixValues['missing_call_agent_id'] );
		if( true == isset( $arrmixValues['point_value'] ) )				$this->setPointValue( $arrmixValues['point_value'] );
		if( true == isset( $arrmixValues['next_rank_point_value'] ) )	$this->setNextRankPointValue( $arrmixValues['next_rank_point_value'] );
		if( true == isset( $arrmixValues['total_number_of_coins'] ) )	$this->setTotalNumberOfCoins( $arrmixValues['total_number_of_coins'] );
		if( true == isset( $arrmixValues['total_number_of_points'] ) )	$this->setTotalNumberOfPoints( $arrmixValues['total_number_of_points'] );

		return true;
	}

	public function setRankNumber( $intRankNumber ) {
		$this->m_intRankNumber = $intRankNumber;
	}

	public function getRankNumber() {
		return $this->m_intRankNumber;
	}

	public function setMissingCallAgentId( $intMissingCallAgentId ) {
		$this->m_intMissingCallAgentId = $intMissingCallAgentId;
	}

	public function getMissingCallAgentId() {
		return $this->m_intMissingCallAgentId;
	}

	public function setPointValue( $intPointValue ) {
		$this->m_intPointValue = $intPointValue;
	}

	public function getPointValue() {
		return $this->m_intPointValue;
	}

	public function setNextRankPointValue( $intNextRankPointValue ) {
		return $this->m_intNextRankPointValue = $intNextRankPointValue;
	}

	public function getNextRankPointValue() {
		return $this->m_intNextRankPointValue;
	}

	public function setTotalNumberOfCoins( $intTotalNumberOfCoins ) {
		$this->m_intTotalNumberOfCoins = $intTotalNumberOfCoins;
	}

	public function getTotalNumberOfCoins() {
		return $this->m_intTotalNumberOfCoins;
	}

	public function setTotalNumberOfPoints( $fltTotalNumberOfPoints ) {
		$this->m_fltTotalNumberOfPoints = $fltTotalNumberOfPoints;
	}

	public function getTotalNumberOfPoints() {
		return $this->m_fltTotalNumberOfPoints;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRankPointMappingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNextRankPointMappingId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCurrentActiveRank() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMyCredits() {
		$boolIsValid = true;

		if( true == ( 0 > $this->getMyCredits() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'my_credits', 'Credit should not less than 0.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valMyCredits();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>