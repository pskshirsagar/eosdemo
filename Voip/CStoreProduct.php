<?php

class CStoreProduct extends CBaseStoreProduct {

	protected $m_intStoreProductRankId;
	protected $m_intRankPointMappingId;
	protected $m_intAdjustQuantity;
	protected $m_strTempFileName;
	protected $m_strImageSource;
	protected $m_boolIsAllowCallAgentPurchase;
	protected $m_boolIsCallAgentTagged;

	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['store_product_rank_id'] ) ) $this->setStoreProductRankId( $arrmixValues['store_product_rank_id'] );
		if( isset( $arrmixValues['rank_point_mapping_id'] ) ) $this->setRankPointMappingId( $arrmixValues['rank_point_mapping_id'] );
		return true;
	}

	public function setStoreProductRankId( $intStoreProductRankId ) {
		$this->m_intStoreProductRankId = $intStoreProductRankId;
	}

	public function getStoreProductRankId() {
		return $this->m_intStoreProductRankId;
	}

	public function setRankPointMappingId( $intRankPointMappingId ) {
		$this->m_intRankPointMappingId = $intRankPointMappingId;
	}

	public function getRankPointMappingId() {
		return $this->m_intRankPointMappingId;
	}

	public function setAdjustQuantity( $intAdjustQuantity ) {
		$this->m_intAdjustQuantity = $intAdjustQuantity;
	}

	public function getAdjustQuantity() {
		return $this->m_intAdjustQuantity;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function setImageSource( $strImageSource ) {
		$this->m_strImageSource = $strImageSource;
	}

	public function getImageSource() {
		return $this->m_strImageSource;
	}

	public function setIsAllowCallAgentPurchase( $boolIsAllowCallAgentPurchase ) {
		$this->m_boolIsAllowCallAgentPurchase = $boolIsAllowCallAgentPurchase;
	}

	public function getIsAllowCallAgentPurchase() {
		return $this->m_boolIsAllowCallAgentPurchase;
	}

	public function getIsCallAgentTagged() {
		return $this->m_boolIsCallAgentTagged;
	}

	public function setIsCallAgentTagged( $boolIsCallAgentTagged ) {
		$this->m_boolIsCallAgentTagged = $boolIsCallAgentTagged;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoreProductTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRankPointMappingId() {
		$boolIsValid = true;

		if( false == valId( $this->getRankPointMappingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rank_point_mapping_id', 'Rank is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonetaryValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImagePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestFormUrl() {
		$boolIsValid = true;

		if( false == filter_var( $this->getRequestFormUrl(), FILTER_VALIDATE_URL ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'request_form_url', 'Valid Google Form URL is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsInventory() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInitialQuantity( $strEditAction ) {
		$boolIsValid = true;

		if( $strEditAction == 'update' ) {
			return $boolIsValid;
		}

		if( true == $this->getIsInventory() && false == valId( $this->getInitialQuantity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'initial_quantity', 'On Hand is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCurrentQuantity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequiredCoins() {
		$boolIsValid = true;

		if( false == valId( $this->getRequiredCoins() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'required_coins', 'LC coin cost is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRemark() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoreProductImage( $arrmixStoreProductImageInfo ) {
		$boolIsValid		= true;
		$intMaxUploadSize	= 2 * 1024 * 1024;

		switch( NULL ) {
			default:
				if( false == valArr( $arrmixStoreProductImageInfo ) ) {
					break;
				}

				if( 0 == $arrmixStoreProductImageInfo['size'] ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload file. Files should be larger than 0 KB.', NULL ) );
					$boolIsValid = false;
					break;
				}

				if( $arrmixStoreProductImageInfo['size'] > $intMaxUploadSize ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload file. Files should not be larger than 2MB.', NULL ) );
					$boolIsValid = false;
					break;
				}

				if( true == valId( $arrmixStoreProductImageInfo['error'] ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Failed to upload file.', NULL ) );
					$boolIsValid = false;
					break;
				}

				if( false == in_array( $arrmixStoreProductImageInfo['mime_type'], [ 'image/png', 'image/gif', 'image/jpeg', 'image/bmp' ] ) || false == in_array( \Psi\CStringService::singleton()->strtolower( $arrmixStoreProductImageInfo['extension'] ), [ 'png', 'gif', 'jpg', 'jpeg', 'bmp' ] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'This file type is not allowed.' ) );
					$boolIsValid = false;
					break;
				}
		}

		return $boolIsValid;
	}

	public function valAdjustQuantity() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getAdjustQuantity() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'adjust_quantity', 'Please enter number of items being adjusted.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrmixStoreProductImageInfo, $strEditAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_marketplace_item_insert':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valInitialQuantity( $strEditAction );
				$boolIsValid &= $this->valRequiredCoins();
				break;

			case 'validate_perk_insert':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valRankPointMappingId();
				$boolIsValid &= $this->valRequestFormUrl();
				break;

			case 'validate_inventory_update':
				$boolIsValid &= $this->valAdjustQuantity();
				break;

			case 'validate_store_product_image':
				$boolIsValid &= $this->valStoreProductImage( $arrmixStoreProductImageInfo );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Other Functions
	*/

	public function uploadImage() {
		if( false == CFileIo::isFile( $this->getTempFileName() ) || false == valStr( $this->getImagePath() ) ) {
			return NULL;
		}

		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( false == is_dir( PATH_MOUNTS_ADMIN_MEDIAS . PATH_GAMIFICATION_STORE_PRODUCTS ) ) {
					if( false == CFileIo::recursiveMakeDir( PATH_MOUNTS_ADMIN_MEDIAS . PATH_GAMIFICATION_STORE_PRODUCTS ) ) {
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to create directory path.' ) );
						$boolIsValid = false;
						break;
					}
				}

				if( false == CFileIO::moveFile( $this->getTempFileName(), PATH_MOUNTS_ADMIN_MEDIAS . $this->getImagePath() ) ) {
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
					$boolIsValid = false;
					break;
				}
		}

		return $boolIsValid;
	}

	public function insertStoreProductLog( $intStoreProductLogTypeId, $intCurrentUserId, $objVoipDatabase ) {
		$objStoreProductLog = new CStoreProductLog();
		$objStoreProductLog->setStoreProductId( $this->getId() );
		$objStoreProductLog->setStoreProductLogTypeId( $intStoreProductLogTypeId );

		switch( $objStoreProductLog->getStoreProductLogTypeId() ) {
			case CStoreProductLogType::STORE_PRODUCT_LOG_TYPE_NEW:
				$objStoreProductLog->setCurrentQuantity( ( int ) $this->getInitialQuantity() );
				break;

			case CStoreProductLogType::STORE_PRODUCT_LOG_TYPE_REFILL:
				$objStoreProductLog->setAdjustQuantity( ( int ) $this->getAdjustQuantity() );
				break;

			case CStoreProductLogType::STORE_PRODUCT_LOG_TYPE_REDEMPTION:
				$objStoreProductLog->setRedeemedQuantity( -1 );
				break;

			default:
			// Default case
		}

		if( false == $objStoreProductLog->validate( 'validate_store_product_log_insert' ) || false == $objStoreProductLog->insert( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>