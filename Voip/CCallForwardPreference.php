<?php

class CCallForwardPreference extends CBaseCallForwardPreference {

	const OFFICE_CONTACTS	= 1;
	const IVR				= 2;
	const SPECIFIC_NUMBER	= 3;
}
?>