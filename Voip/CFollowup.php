<?php

class CFollowup extends CBaseFollowup {
	protected $m_strCallAgentName;
	protected $m_strTimezoneName;
	protected $m_strCompletedBy;
	protected $m_strContactedVia;
	protected $m_strFollowupEmailBy;
	protected $m_strFollowupCallBy;
	protected $m_strCompletedByCallAgentName;

	const SEARCH_TYPE_LEAD					= 1;
	const SEARCH_TYPE_REVIEW_RESPONSE		= 2;
	const SEARCH_TYPE_CALL_BACK_FOLLOWUP	= 3;

	public static $c_arrstrFollowupFilters = [
		self::SEARCH_TYPE_LEAD					=> 'Lead',
		self::SEARCH_TYPE_REVIEW_RESPONSE		=> 'Review Response',
		self::SEARCH_TYPE_CALL_BACK_FOLLOWUP	=> 'Call Back Followup'
	];

	public static $c_arrintFollowupTypeIds = [
		self::SEARCH_TYPE_LEAD,
		self::SEARCH_TYPE_REVIEW_RESPONSE,
		self::SEARCH_TYPE_CALL_BACK_FOLLOWUP
	];

	public static $c_arrstrCompletedFollowupFilters = [
		self::SEARCH_TYPE_LEAD					=> 'Lead',
		self::SEARCH_TYPE_REVIEW_RESPONSE		=> 'Review Response',
	];

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['timezone_name'] ) )			$this->setTimezoneName( $arrmixValues['timezone_name'] );
		if( isset( $arrmixValues['call_agent_name'] ) )			$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( isset( $arrmixValues['followup_email_by'] ) )			$this->setFollowupEmailBy( $arrmixValues['followup_email_by'] );
		if( isset( $arrmixValues['followup_call_by'] ) )			$this->setFollowupCallBy( $arrmixValues['followup_call_by'] );
		if( isset( $arrmixValues['completed_by_call_agent_name'] ) )			$this->setCompletedByCallAgentName( $arrmixValues['completed_by_call_agent_name'] );
		return true;
	}

	public function setTimezoneName( $strTimezoneName ) {
		$this->m_strTimezoneName = $strTimezoneName;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setContactedVia( $strContactedVia ) {
		$this->m_strContactedVia = $strContactedVia;
	}

	public function setFollowupEmailBy( $strFollowupEmailBy ) {
		$this->m_strFollowupEmailBy = $strFollowupEmailBy;
	}

	public function setFollowupCallBy( $strFollowupCallBy ) {
		$this->m_strFollowupCallBy = $strFollowupCallBy;
	}

	public function setCompletedByCallAgentName( $strCompletedByCallAgentName ) {
		$this->m_strCompletedByCallAgentName = $strCompletedByCallAgentName;
	}

	public function getTimezoneName() {
		return $this->m_strTimezoneName;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getContactedVia() {
		return $this->m_strContactedVia;
	}

	public function getFollowupEmailBy() {
		return $this->m_strFollowupEmailBy;
	}

	public function getFollowupCallBy() {
		return $this->m_strFollowupCallBy;
	}

	public function getCompletedByCallAgentName() {
		return $this->m_strCompletedByCallAgentName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFollowupTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>