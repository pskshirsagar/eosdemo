<?php

class CCallAgentQueue extends CBaseCallAgentQueue {

	protected $m_arrobjCallAgents;
	protected $m_strCallAgentName;
	protected $m_strCallQueueName;
	protected $m_strCallAgentExtension;
	protected $m_boolIsCallAgentQueue;
	protected $m_intEmployeeId;
	protected $m_intCallSkillQueuePriority;
	protected $m_intPriority;
	protected $m_intCallQueueTypeId;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;
	protected $m_intQueuePhoneExtensionId;
	protected $m_strCallQueueExtension;
	protected $m_objCallAgent;
	protected $m_objCallQueue;

	public function __construct() {
		parent::__construct();

		$this->m_strState		= CTier::READY;
		$this->m_intLevel		= CCallAgentSkill::DEFAULT_PRIORITY;
		$this->m_intPosition	= CCallAgentSkill::DEFAULT_POSITION;

		return;
	}

	/**
	 * set functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['call_queue_id'] ) )					$this->setCallQueueId( $arrstrValues['call_queue_id'] );
		if( true == isset( $arrstrValues['call_agent_id'] ) )					$this->setCallAgentId( $arrstrValues['call_agent_id'] );
		if( true == isset( $arrstrValues['agent_phone_extension_id'] ) )		$this->setAgentPhoneExtensionId( $arrstrValues['agent_phone_extension_id'] );
		if( true == isset( $arrstrValues['physical_phone_extension_id'] ) )		$this->setPhysicalPhoneExtensionId( $arrstrValues['physical_phone_extension_id'] );
		if( true == isset( $arrstrValues['queue_phone_extension_id'] ) )		$this->setQueuePhoneExtensionId( $arrstrValues['queue_phone_extension_id'] );
		if( true == isset( $arrstrValues['employee_id'] ) )						$this->setEmployeeId( $arrstrValues['employee_id'] );
		if( true == isset( $arrstrValues['call_queue_type_id'] ) )				$this->setCallQueueTypeId( $arrstrValues['call_queue_type_id'] );
		if( true == isset( $arrstrValues['call_agent_name'] ) )					$this->setCallAgentName( $arrstrValues['call_agent_name'] );
		if( true == isset( $arrstrValues['call_queue_name'] ) )					$this->setCallQueueName( $arrstrValues['call_queue_name'] );
		if( true == isset( $arrstrValues['call_agent_domain'] ) )				$this->setCallAgentDomain( $arrstrValues['call_agent_domain'] );
		if( true == isset( $arrstrValues['call_queue_domain'] ) )				$this->setCallQueueDomain( $arrstrValues['call_queue_domain'] );
		if( true == isset( $arrstrValues['call_agent_state_type_name'] ) )		$this->setCallAgentStateTypeName( $arrstrValues['call_agent_state_type_name'] );
		if( true == isset( $arrstrValues['call_agent_status_type_name'] ) )		$this->setCallAgentStatusTypeName( $arrstrValues['call_agent_status_type_name'] );
		if( true == isset( $arrstrValues['is_call_agent_queue'] ) )				$this->setIsCallAgentQueue( $arrstrValues['is_call_agent_queue'] );
		if( true == isset( $arrstrValues['call_skill_queue_priority'] ) )		$this->setCallSkillQueuePriority( $arrstrValues['call_skill_queue_priority'] );
		if( true == isset( $arrstrValues['call_agent_queue_position'] ) )		$this->setCallAgentQueuePosition( $arrstrValues['call_agent_queue_position'] );
		if( true == isset( $arrstrValues['priority'] ) )						$this->setPriority( $arrstrValues['priority'] );

		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setIsCallAgentQueue( $boolIsCallAgentQueue ) {
		$this->m_boolIsCallAgentQueue = $boolIsCallAgentQueue;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setCallSkillQueuePriority( $intCallSkillQueuePriority ) {
		$this->m_intCallSkillQueuePriority = $intCallSkillQueuePriority;
	}

	public function setPriority( $intPriority ) {
		$this->m_intPriority = $intPriority;
	}

	public function setCallAgent( $objCallAgent ) {
		$this->m_objCallAgent = $objCallAgent;
	}

	public function setCallQueue( $objCallQueue ) {
		$this->m_objCallQueue = $objCallQueue;
	}

	public function setQueuePhoneExtensionId( $intQueuePhoneExtensionId ) {
		$this->m_intQueuePhoneExtensionId = $intQueuePhoneExtensionId;
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->m_intAgentPhoneExtensionId = $intAgentPhoneExtensionId;
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->m_intPhysicalPhoneExtensionId = $intPhysicalPhoneExtensionId;
	}

	public function setCallAgentDomain( $strCallAgentDomain ) {
		$this->m_strCallAgentDomain = $strCallAgentDomain;
	}

	public function setCallQueueDomain( $strCallQueueDomain ) {
		$this->m_strCallQueueDomain = $strCallQueueDomain;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	public function setCallAgentQueuePosition( $strCallAgentQueuePosition ) {
		$this->m_strCallAgentQueuePosition = $strCallAgentQueuePosition;
	}

	public function setCallQueueTypeId( $intCallQueueTypeId ) {
		$this->m_intCallQueueTypeId = $intCallQueueTypeId;
	}

	/**
	 * get functions
	 */

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallAgents() {
		return $this->m_arrobjCallAgents;
	}

	public function getIsCallAgentQueue() {
		return $this->m_boolIsCallAgentQueue;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getCallSkillQueuePriority() {
		return $this->m_intCallSkillQueuePriority;
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function getCallAgent() {
		return $this->m_objCallAgent;
	}

	public function getCallQueue() {
		return $this->m_objCallQueue;
	}

	public function getCallAgentExtension() {
		return $this->m_strCallAgentExtension;
	}

	public function getQueuePhoneExtensionId() {
		return $this->m_intQueuePhoneExtensionId;
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function getCallAgentDomain() {
		return $this->m_strCallAgentDomain;
	}

	public function getCallQueueDomain() {
		return $this->m_strCallQueueDomain;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallAgentQueuePosition() {
		return $this->m_strCallAgentQueuePosition;
	}

	public function getCallQueueTypeId() {
		return $this->m_intCallQueueTypeId;
	}

	/**
	 * Misc functions
	 */

	public function getBuildCallQueueName() {
		$strCallQueueName = ( true == valObj( $this->m_objCallQueue, 'CCallQueue' ) ) ? $this->m_objCallQueue->getQueuePhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME : NULL;

		if( true == is_null( $strCallQueueName ) ) {
			$strCallQueueName = $this->getQueuePhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME;
		}

		return $strCallQueueName;
	}

	public function getBuildCallAgentName() {
		$strCallAgentName = ( true == valObj( $this->m_objCallAgent, 'CCallAgent' ) ) ? $this->m_objCallAgent->getAgentPhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME : NULL;

		if( true == is_null( $strCallAgentName ) ) {
			$strCallAgentName = $this->getAgentPhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME;
		}

		return $strCallAgentName;
	}

	/**
	 * Adder functions
	 */

	public function addCallAgent( $objCallAgent ) {
		$this->m_arrobjCallAgents[$objCallAgent->getCallAgentQueueId()] = $objCallAgent;
	}

	public function createTier() {
		$objTier = new CTier();
		$objTier->setDefaults();

		$objTier->setCallQueueId( $this->getCallQueueId() );
		$objTier->setCallAgentId( $this->getCallAgentId() );
		$objTier->setQueue( $this->getBuildCallQueueName() );
		$objTier->setAgent( $this->getBuildCallAgentName() );
		$objTier->setState( CTier::READY );
		$objTier->setPosition( CCallAgentSkill::DEFAULT_POSITION );
		$objTier->setLevel( CCallAgentSkill::DEFAULT_PRIORITY );

		if( true == valId( $this->getOverridePriority() ) && strtotime( $this->getOverrideExpiration() ) > strtotime( date( 'd-m-Y' ) ) ) {
			$objTier->setLevel( $this->getOverridePriority() );
		} elseif( false == is_null( $this->getPriority() ) ) {
			$objTier->setLevel( $this->getPriority() );
		}

		if( true == is_null( $this->getLevel() ) ) {
			$objTier->setLevel( $this->getLevel() );
		}

		return $objTier;
	}

	/**
	 * validate functions
	 */

	public function valCallAgentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valCallQueueId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCallQueueId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_queue_id', 'Call queue not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valLevel() {
		$boolIsValid = true;

		if( true == is_null( $this->getLevel() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'level', 'Level details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valPosition() {
		$boolIsValid = true;

		if( true == is_null( $this->getPosition() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'position', 'Position details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valState() {
		$boolIsValid = true;

		if( true == is_null( $this->getState() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'state', 'State details not provided.' ) );
		}

		return $boolIsValid;
	}

	public function valOverridePriority() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getOverridePriority() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'priority', 'Priority is required.' ) );
		} elseif( false == is_numeric( $this->getOverridePriority() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'priority', 'Priority should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valOverrideExpiration() {
		$boolIsValid = true;

		if( true == is_null( $this->getOverrideExpiration() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration', 'Expiration date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getOverrideExpiration() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration', 'Expiration date is not a valid date.' ) );
		} elseif( strtotime( $this->getOverrideExpiration() ) <= strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expiration', 'Expiration date should be greater than current date.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * Inherited functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase );
		}
	}

	public function insert( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {

		if( CDatabaseType::VOIP !== $objVoipDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Application Error: Invalid voip database object provided.', E_USER_ERROR );
		}

		if( false == parent::insert( $intCurrentUserId, $objVoipDatabase ) ) return false;

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {

			if( false == valObj( $this->m_objCallAgent, 'CCallAgent' ) ) {
				$this->m_objCallAgent = CCallAgents::fetchCallAgentById( $this->getCallAgentId(), $objVoipDatabase );
			}

			if( false == valObj( $this->m_objCallQueue, 'CCallQueue' ) ) {
				$this->m_objCallQueue = \Psi\Eos\Voip\CCallQueues::createService()->fetchCallQueueById( $this->getCallQueueId(), $objVoipDatabase );
			}

			$objTier = $this->createTier();

			if( false == $objTier->insert( $intCurrentUserId, $objFreeswitchDatabase ) ) return false;
		}

		return true;
	}

	public function update( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( CDatabaseType::VOIP !== $objVoipDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Application Error: Invalid database objects provided.', E_USER_ERROR );
		}

		if( false == parent::update( $intCurrentUserId, $objVoipDatabase ) ) return false;

		$objCallAgentQueue = CCallAgentQueues::fetchActiveCallAgentQueueById( $this->getId(), $objVoipDatabase );

		if( false == valObj( $objCallAgentQueue, 'CCallAgentQueue' ) ) return false;

		$this->setPriority( $objCallAgentQueue->getPriority() );

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {

			$objTier = CTiers::fetchTierByCallAgentIdByCallQueueId( $this->getCallAgentId(), $this->getCallQueueId(), $objFreeswitchDatabase );

			if( true == valObj( $objTier, 'CTier' ) ) {
				if( true == valStr( $this->getOverridePriority() ) && strtotime( $this->getOverrideExpiration() ) > strtotime( date( 'd-m-Y' ) ) ) {
					$objTier->setLevel( $this->getOverridePriority() );
				} else {
					$objTier->setLevel( $this->getPriority() );
				}

				if( false == $objTier->update( $intCurrentUserId, $objFreeswitchDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( CDatabaseType::VOIP !== $objVoipDatabase->getDatabaseTypeId() ) {
			trigger_error( 'Application Error: Invalid database objects provided.', E_USER_ERROR );
		}

		if( false == parent::delete( $intCurrentUserId, $objVoipDatabase ) ) return false;

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {
			$arrobjTiers = CTiers::fetchTiersByCallQueueIdByCallAgentId( $this->getCallQueueId(), $this->getCallAgentId(), $objFreeswitchDatabase );

			if( false == valArr( $arrobjTiers ) ) return true;

			if( false == CTiers::bulkDelete( $arrobjTiers, $objFreeswitchDatabase ) ) return false;
		}

		return true;
	}

	/**
	 * Validate function
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valCallQueueId();
				$boolIsValid &= $this->valState();
				$boolIsValid &= $this->valLevel();
				$boolIsValid &= $this->valPosition();
				break;

			case 'validate_call_agent_queue':
				$boolIsValid &= $this->valCallQueueId();
				$boolIsValid &= $this->valState();
				$boolIsValid &= $this->valLevel();
				$boolIsValid &= $this->valPosition();
				break;

			case 'validate_call_agent_queue_priority':
				$boolIsValid &= $this->valOverridePriority();
				$boolIsValid &= $this->valOverrideExpiration();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>