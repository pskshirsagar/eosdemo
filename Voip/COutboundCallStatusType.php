<?php

class COutboundCallStatusType extends CBaseOutboundCallStatusType {

	const PENDING 		= 1;
	const QUEUING 		= 2;
	const QUEUED		= 3;
	const SUCCESS		= 4;
	const FAILED		= 5;
	const CANCELLED		= 6;
	const EXPIRED		= 7;

	public static $c_arrintActiveOutboundCallStatusTypeIds = array(
		self::PENDING,
		self::QUEUING,
		self::QUEUED,
	);

	public static $c_arrstrCustomOutboundCallStatusTypes = array(
		self::PENDING 	=> 'Preparing Call',
		self::QUEUING 	=> 'Call In Progress',
		self::QUEUED	=> 'Offering Call',
		self::SUCCESS	=> 'Call Connected',
		self::FAILED	=> 'Call Failed',
	);

	public static $c_arrstrAllOutboundCallStatusTypes = array(
		self::PENDING	=> 'Pending',
		self::QUEUING	=> 'Queuing',
		self::QUEUED 	=> 'Queued',
		self::SUCCESS	=> 'Success',
		self::FAILED	=> 'Failed',
		self::CANCELLED => 'Cancelled',
		self::EXPIRED	=> 'Expired'
	);
}
?>