<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallBlocks
 * Do not add any new functions to this class.
 */

class CCallBlocks extends CBaseCallBlocks {

	public static function determineIsBlocked( $strPhoneNumber, $intScheduledCallTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT id FROM call_blocks WHERE phone_number LIKE E\'' . addslashes( $strPhoneNumber ) . '\' AND scheduled_call_type_id = ' . ( int ) $intScheduledCallTypeId . ' LIMIT 1';
		return self::fetchCallBlock( $strSql, $objVoipDatabase );
	}

	public static function fetchExistingCallBlockByCallTypeIdByPhoneNumber( $intScheduledCallTypeId, $strPhoneNumber, $objVoipDatabase ) {
		$strSql = 'SELECT id FROM call_blocks WHERE phone_number LIKE E\'' . addslashes( $strPhoneNumber ) . '\' AND scheduled_call_type_id = ' . ( int ) $intScheduledCallTypeId . ' LIMIT 1';
		return self::fetchCallBlock( $strSql, $objVoipDatabase );

	}

	public static function fetchPaginatedCallBlocksByCid( $intPageNo, $intPageSize, $intCid, $objVoipDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql = 'SELECT * FROM call_blocks WHERE cid = ' . ( int ) $intCid . 'ORDER BY block_datetime DESC OFFSET ' . ( int ) $intOffset . '	LIMIT ' . $intLimit;
		return self::fetchCallBlocks( $strSql, $objVoipDatabase );

	}

	public static function fetchPaginatedCallBlocksCountByCid( $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT count(id) FROM call_blocks WHERE cid = ' . ( int ) $intCid;
		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		if ( true == isset ( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchCustomCallBlockCount( $strPhoneNumber, $objVoipDatabase ) {
		$strSql = 'WHERE phone_number = \'' . preg_replace( '/[^0-9]*/', '', $strPhoneNumber ) . '\' AND scheduled_call_type_id = ' . ( int ) CScheduledCallType::CUSTOM;
		return self::fetchCallBlockCount( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomPaginatedCallBlocksByScheduledCallTypeId( $intScheduledCallTypeId, $objPagination, $objCallsFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		$strWhereSql = '';

		if( true == valStr( $objPagination->getQuickSearch() ) ) {
			$strWhereSql .= ' AND ( CAST( phone_number AS text ) LIKE \'' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) ';
		}

		if( false == is_null( $objCallsFilter->getStartDate() ) && false == is_null( $objCallsFilter->getEndDate() ) ) {
			if( 'Min' != $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
				$strWhereSql .= ' AND block_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\'' . ' AND \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59\'';
			} elseif( 'Min' == $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
				$strWhereSql .= ' AND block_datetime <= \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59\'';
			} elseif( 'Min' != $objCallsFilter->getStartDate() && 'Max' == $objCallsFilter->getEndDate() ) {
				$strWhereSql .= ' AND block_datetime >= \'' . date( 'Y-m-d',  strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\'';
			}
		}

		$strSql = ' SELECT
						*
					FROM
						call_blocks
					WHERE
						cid IS NULL
						AND scheduled_call_id IS NULL
						AND scheduled_call_type_id = ' . ( int ) $intScheduledCallTypeId
						. $strWhereSql . '
					ORDER BY
						block_datetime DESC';

		if( false == $boolIsCountOnly ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			return self::fetchCallBlocks( $strSql, $objVoipDatabase );
		} else {
			$arrobjCallBlocks = self::fetchCallBlocks( $strSql, $objVoipDatabase );
			return \Psi\Libraries\UtilFunctions\count( $arrobjCallBlocks );
		}
	}

	public static function fetchCustomSimpleCallBlocksByScheduledCallTypeId( $intScheduledCallTypeId, $objVoipDatabase ) {
		$strSql = ' SELECT
						id,
						phone_number,
						block_datetime
					FROM
						call_blocks
					WHERE
						cid IS NULL
						AND scheduled_call_id IS NULL
						AND scheduled_call_type_id = ' . ( int ) $intScheduledCallTypeId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallBlocksByIds( $arrintBlockCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintBlockCallIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_blocks WHERE id IN ( ' . implode( ',', $arrintBlockCallIds ) . ' )';

		return self::fetchCallBlocks( $strSql, $objVoipDatabase );
	}

}
?>
