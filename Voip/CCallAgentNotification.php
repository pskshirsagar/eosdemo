<?php

class CCallAgentNotification extends CBaseCallAgentNotification {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallNotificationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLockedSeq() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>