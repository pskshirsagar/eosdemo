<?php

class CIvrMenuType extends CBaseIvrMenuType {

	const DURING_OFFICE_HOURS	= 1;
	const AFTER_OFFICE_HOURS	= 2;
	const WEEKEND				= 3;
	const HOLIDAY				= 4;

	public static $c_arrstrIvrMenuTypeNames = array(
		self::DURING_OFFICE_HOURS	=> 'During Office Hours',
		self::AFTER_OFFICE_HOURS	=> 'After Office Hours',
	);
}
 ?>
