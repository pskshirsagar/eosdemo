<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServiceProviders
 * Do not add any new functions to this class.
 */

class CCallServiceProviders extends CBaseCallServiceProviders {

	public static function fetchCallServiceProviders( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallServiceProvider', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallServiceProvider( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallServiceProvider', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCallServiceProviders( $objVoipDatabase ) {
		return self::fetchCallServiceProviders( 'SELECT * FROM call_service_providers', $objVoipDatabase );
	}
}
?>