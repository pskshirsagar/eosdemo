<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallResults
 * Do not add any new functions to this class.
 */

class CCallResults extends CBaseCallResults {

	public static function fetchCallResults( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallResult', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCallResult( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallResult', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCallResultsByIds( $arrintCallResultIds, $objVoipDatabase, $boolOrderByOrderNum = true ) {
		if( false == valArr( $arrintCallResultIds ) ) return;
		$strSql = 'SELECT * FROM call_results WHERE id IN ( ' . implode( ',', $arrintCallResultIds ) . ' ) ORDER BY ';
		$strSql .= ( true == $boolOrderByOrderNum ) ? 'order_num' : 'name';
		return self::fetchCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallResultsByCallTypeIdByPsProductIds( $intCallTypeId, $arrintPsProductIds, $objVoipDatabase ) {
		if( false == is_numeric( $intCallTypeId ) || false == valArr( $arrintPsProductIds ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						call_results
					WHERE
						call_type_id = ' . ( int ) $intCallTypeId . '
						AND ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )
						AND is_published = true
					ORDER BY
						name';

		return parent::fetchCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallResultsByCallTypeId( $intCallTypeId, $objVoipDatabase ) {
		if( false == is_numeric( $intCallTypeId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						call_results
					WHERE
						call_type_id = ' . ( int ) $intCallTypeId . '
						AND call_result_id IS NULL
						AND is_published = true
					ORDER BY
						name';

		return parent::fetchCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallResultsByCallTypeIdByCallResultId( $intCallTypeId, $intCallResultId, $objVoipDatabase ) {
		if( false == valId( $intCallTypeId ) || false == valId( $intCallResultId ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						call_results
					WHERE
						call_type_id = ' . ( int ) $intCallTypeId . '
						AND call_result_id = ' . ( int ) $intCallResultId . '
						AND is_published = true
					ORDER BY
						name';

		return parent::fetchCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchCallResultsByCallTypeIdByCallResultIds( $intCallTypeId, $arrintCallResultIds, $objVoipDatabase ) {
		if( false == valId( $intCallTypeId ) || false == valArr( $arrintCallResultIds ) ) return NULL;

		$strSql = 'SELECT
					*
					FROM
						call_results
					WHERE
						call_type_id = ' . ( int ) $intCallTypeId . '
						AND call_result_id IN ( ' . implode( ',', $arrintCallResultIds ) . ' )
						AND is_published = true
					ORDER BY
						name';

		return parent::fetchCallResults( $strSql, $objVoipDatabase );
	}

	public static function fetchAllPublishedCallResults( $objVoipDatabase ) {
		$strSql = 'SELECT
					*
					FROM
						call_results
					WHERE
 						is_published = true
					ORDER BY
						name';

		return parent::fetchCallResults( $strSql, $objVoipDatabase );
	}

}
?>