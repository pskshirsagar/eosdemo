<?php

class CCallAnalysisQuestion extends CBaseCallAnalysisQuestion {

	protected $m_intMaxPossiblePoints;

	protected $m_arrobjCallAnalysisQuestions;
	protected $m_arrobjCallAnalysisAnswers;

	protected $m_intTotalQualifiedQuestions;
	protected $m_fltTotalQualifiedPercentage;

	 /**
	  * Get Functions
	  */

	public function getCallAnalysisQuestions() {
		return $this->m_arrobjCallAnalysisQuestions;
	}

	public function getTotalQualifiedQuestions() {
		return $this->m_intTotalQualifiedQuestions;
	}

	public function getTotalQualifiedPercentage() {
		return $this->m_fltTotalQualifiedPercentage;
	}

	public function getCallAnalysisAnswers() {
		return $this->m_arrobjCallAnalysisAnswers;
	}

	public function getMaxPossiblePoints() {
		return $this->m_intMaxPossiblePoints;
	}

	 /**
	  * Set Functions
	  */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['total_qualified_questions'] ) ) 	$this->setTotalQualifiedQuestions( $arrmixValues['total_qualified_questions'] );
		if( true == isset( $arrmixValues['total_qualified_percentage'] ) )	$this->setTotalQualifiedPercentage( $arrmixValues['total_qualified_percentage'] );
		if( true == isset( $arrmixValues['max_possible_points'] ) )			$this->setMaxPossiblePoints( $arrmixValues['max_possible_points'] );

		if( true == isset( $arrmixValues['call_analysis_answers'] ) ) {

			$arrobjCallAnalysisAnswers = [];

			if( true == valStr( $arrmixValues['call_analysis_answers'] ) ) {
				$arrmixValues['call_analysis_answers'] = json_decode( $arrmixValues['call_analysis_answers'], true );
			}

			if( true == valArr( $arrmixValues['call_analysis_answers'] ) && false == valObj( current( $arrmixValues['call_analysis_answers'] ), 'CCallAnalysisAnswer' ) ) {
				foreach( $arrmixValues['call_analysis_answers'] AS $arrstrCallAnalysisAnswer ) {
					$objCallAnalysisAnswer = new CCallAnalysisAnswer();
					$arrstrCallAnalysisAnswer['details'] = json_encode( $arrstrCallAnalysisAnswer['details'] );
					$objCallAnalysisAnswer->setValues( $arrstrCallAnalysisAnswer );
					$arrobjCallAnalysisAnswers[$objCallAnalysisAnswer->getId()] = $objCallAnalysisAnswer;
				}
			}

			$this->setCallAnalysisAnswers( $arrobjCallAnalysisAnswers );

		}

		return;
	}

	public function setCallAnalysisQuestions( $arrobjCallAnalysisQuestions ) {
		$this->m_arrobjCallAnalysisQuestions = $arrobjCallAnalysisQuestions;
	}

	public function setTotalQualifiedQuestions( $intTotalQualifiedQuestions ) {
		$this->m_intTotalQualifiedQuestions = $intTotalQualifiedQuestions;
	}

	public function setTotalQualifiedPercentage( $fltTotalQualifiedPercentage ) {
		$this->m_fltTotalQualifiedPercentage = $fltTotalQualifiedPercentage;
	}

	public function setCallAnalysisAnswers( $arrobjCallAnalysisAnswers ) {
		$this->m_arrobjCallAnalysisAnswers = $arrobjCallAnalysisAnswers;
	}

	public function setMaxPossiblePoints( $intMaxPossiblePoints ) {
		$this->m_intMaxPossiblePoints = $intMaxPossiblePoints;
	}

	/**
	 * Create functions
	 */

	public function createCallAnalysisResult() {
		$objCallAnalysisResult = new CCallAnalysisResult();
		$objCallAnalysisResult->setCid( $this->getCid() );
		$objCallAnalysisResult->setCallAnalysisQuestionId( $this->getId() );
		$objCallAnalysisResult->setMaxPossibleWeight( $this->m_intMaxPossiblePoints );
		return $objCallAnalysisResult;
	}

	/**
	 * Add Functions
	 */

	public function addCallAnalysisQuestion( $objCallAnalysisQuestion ) {
		$this->m_arrobjCallAnalysisQuestions[$objCallAnalysisQuestion->getId()] = $objCallAnalysisQuestion;
	}

	 /**
	  * Validation Functions
	  */

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			 $boolIsValid = false;
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Question is required.' ) );
		}

		return $boolIsValid;
	}

	public function valWeight() {
		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == is_null( $this->getWeight() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weight', 'Points are required.' ) );
					break;
				}

				if( false == is_numeric( $this->getWeight() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weight', 'Only numbers are allowed in points.' ) );
					break;
				}

				if( 0 > $this->getWeight() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'weight', 'Negative points are not allowed' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valWeight();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objVoipDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intUserId, $objVoipDatabase, $boolReturnSqlOnly );
	}

	public function createOrFetchCallAnalysisAnswer( $arrstrAnswer, $objDatabase ) {

		if( true == valId( $arrstrAnswer['answer_id'] ) ) {
			$objCallAnalysisAnswer = CCallAnalysisAnswers::fetchCallAnalysisAnswerById( $arrstrAnswer['answer_id'], $objDatabase );
			$objCallAnalysisAnswer->setAnswer( $arrstrAnswer['description'] );
			$objCallAnalysisAnswer->setPoints( $arrstrAnswer['points'] );
			$objCallAnalysisAnswer->setRank( $arrstrAnswer['rank'] );

		} else {
			$objCallAnalysisAnswer = new CCallAnalysisAnswer();
			$objCallAnalysisAnswer->setCallAnalysisQuestionId( $this->getId() );
			$objCallAnalysisAnswer->setAnswer( $arrstrAnswer['description'] );
			$objCallAnalysisAnswer->setPoints( $arrstrAnswer['points'] );
			$objCallAnalysisAnswer->setRank( $arrstrAnswer['rank'] );
		}

		return $objCallAnalysisAnswer;
	}

}
?>