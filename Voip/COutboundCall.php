<?php

class COutboundCall extends CBaseOutboundCall {
	protected $m_strOutboundCallStatusTypeName;
	protected $m_strOutboundCallCampaignName;
	protected $m_intCallStatusTypeId;
	protected $m_strCallDatetime;
	protected $m_objCallAgent;
	protected $m_intEmployeeId;

	const MAX_MOVE_IN_OUTBOUND_CALL_ATTEMPTS		= 4;
	/**
	 * Validate functions
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == valStr( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Outbound Call is invalid.' ) );
		}

		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valStr( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is missing.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( false == valStr( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is missing.' ) );
		}

		return $boolIsValid;
	}

	public function valCallQueueId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallQueueId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_queue_id', 'Please assign call queue to employee.' ) );
		}

		return $boolIsValid;
	}

	public function valCallTypeId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_type_id', 'Call type is missing.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOutboundCallCampaignId() {
		$boolIsValid = true;

		if( false == valStr( $this->getOutboundCallCampaignId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'outbound_call_campaign_id', 'Call campaign is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskNoteId() {
		$boolIsValid = true;

		if( false == valStr( $this->getTaskNoteId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note_id', 'Task note id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentStateAndStatusType( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid voip database object provided.', E_USER_ERROR );
		}

		$objCallAgent = CCallAgents::fetchCallAgentById( $this->getCallAgentId(), $objVoipDatabase );

		if( false == valObj( $objCallAgent, 'CCallAgent' ) ) {
			trigger_error( 'Application Error: Invalid call agent object.', E_USER_ERROR );
		}

		if( CCallAgentStatusType::AVAILABLE_ON_DEMAND !== $objCallAgent->getCallAgentStatusTypeId() && CCallAgentStateType::WAITING !== $objCallAgent->getCallAgentStateTypeId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_status_type_id', 'Please check support agent state and status type.' ) );
		}

		return $boolIsValid;
	}

	public function valCall( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid voip database object provided.', E_USER_ERROR );
		}

		$objCall = CCalls::fetchCallByOutboundCallId( $this->getId(), $objVoipDatabase );

		if( false == valObj( $objCall, 'CCall' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_status_type_id', 'Please wait call connection in progress.' ) );
		} elseif( false == in_array( $objCall->getCallStatusTypeId(), array( CCallStatusType::ANSWERED, CCallStatusType::UNANSWERED, CCallStatusType::VOICEMAIL_LEFT ) ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_status_type_id', 'Please wait call in progress.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumberFormat() {

		if( false == valStr( $this->getPhoneNumber() ) ) {
			return false;
		}

		$strPhoneNumberCheck = '/^([0-9]{3})[^0-9,-]*([0-9,-]{3})[^0-9,-]*([0-9,-]{4,9})$/';

		if( 1 !== preg_match( $strPhoneNumberCheck, $this->getPhoneNumber() ) ) {
			return false;
		}

		return true;

	}

	/**
	 * set functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['outbound_call_status_type_name'] ) )	$this->setOutboundCallStatusTypeName( $arrmixValues['outbound_call_status_type_name'] );
		if( true == isset( $arrmixValues['outbound_call_campaign_name'] ) )	    $this->setOutboundCallCampaignName( $arrmixValues['outbound_call_campaign_name'] );
		if( true == isset( $arrmixValues['call_status_type_id'] ) )			    $this->setCallStatusTypeId( $arrmixValues['call_status_type_id'] );
		if( true == isset( $arrmixValues['call_datetime'] ) )				    $this->setCallDatetime( $arrmixValues['call_datetime'] );
		if( true == isset( $arrmixValues['employee_id'] ) )                     $this->setEmployeeId( $arrmixValues['employee_id'] );
		return;
	}

	public function setCallStatusTypeId( $intCallStatusTypeId ) {
		$this->m_intCallStatusTypeId = $intCallStatusTypeId;
	}

	public function setOutboundCallStatusTypeName( $strOutboundCallStatusTypeName ) {
		$this->m_strOutboundCallStatusTypeName = $strOutboundCallStatusTypeName;
	}

	public function setOutboundCallCampaignName( $strOutboundCallCampaignName ) {
		$this->m_strOutboundCallCampaignName = $strOutboundCallCampaignName;
	}

	public function setCallDatetime( $strCallDatetime ) {
		$this->m_strCallDatetime = $strCallDatetime;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setCallAgent( $objCallAgent ) {
		$this->m_objCallAgent = $objCallAgent;
	}

	/**
	 * get functions
	 */

	public function getCallStatusTypeId() {
		return $this->m_intCallStatusTypeId;
	}

	public function getOutboundCallStatusTypeName() {
		return $this->m_strOutboundCallStatusTypeName;
	}

	public function getOutboundCallCampaignName() {
		return $this->m_strOutboundCallCampaignName;
	}

	public function getCallDatetime() {
		return $this->m_strCallDatetime;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getCallAgent() {
		return $this->m_objCallAgent;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_move_in_outbound_call':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valOutboundCallCampaignId();
				$boolIsValid &= $this->valCallQueueId();
				$boolIsValid &= $this->valCallTypeId();
				$boolIsValid &= $this->valPhoneNumberFormat();
				break;

			case 'update_outbound_support_dialer':
				$boolIsValid &= $this->valId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valOutboundCallCampaignId();
				$boolIsValid &= $this->valCallAgentStateAndStatusType( $objVoipDatabase );
				$boolIsValid &= $this->valCallQueueId();
				$boolIsValid &= $this->valCallTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valTaskNoteId();
				$boolIsValid &= $this->valCall( $objVoipDatabase );
				break;

			case 'insert_outbound_support_dialer':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valOutboundCallCampaignId();
				$boolIsValid &= $this->valCallQueueId();
				$boolIsValid &= $this->valCallTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valTaskNoteId();
				$boolIsValid &= $this->valCallAgentStateAndStatusType( $objVoipDatabase );
				break;

			default:
				// Comment
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create function
	 */

	public function createCall( $objVoipDatabase ) {
		$objCall = new CCall();

		$objCall->setCid( $this->getCid() );
		$objCall->setPropertyId( $this->getPropertyId() );
		$objCall->setCallTypeId( $this->getCallTypeId() );
		$objCall->setOriginPhoneNumber( $this->getPhoneNumber() );
		$objCall->setOutboundCallId( $this->getId() );
		$objCall->setCallStatusTypeId( CCallStatusType::CALLING );
		$objCall->setCallPriorityId( CCallPriority::NORMAL );
		$objCall->setCallResultId( CCallResult::UNKNOWN );
		$objCall->setVoiceTypeId( CVoiceType::FEMALE );
		$objCall->setDestinationPhoneNumber( $this->getPhoneNumber() );
		$objCall->setOriginPhoneNumber( '8883369435' );
		$objCall->setCallAgentId( $this->getCallAgentId() );
		$objCall->setEmployeeId( $this->getEmployeeId() );

		if( true == valObj( $this->getCallAgent(), 'CCallAgent' ) ) {
			$objCall->setCallAgentId( $this->getCallAgent()->getId() );
			$objCall->setEmployeeId( $this->getCallAgent()->getEmployeeId() );
		}

		if( false == $objCall->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call record.', E_USER_WARNING );
			return;
		}

		$objCallEvent = $objCall->createCallEvent();
		$objCallEvent->setCallEventTypeId( CCallEventType::STARTING );

		if( false == $objCallEvent->insert( SYSTEM_USER_ID, $objVoipDatabase ) ) {
			trigger_error( 'Application Error: Unable to save call event record.', E_USER_WARNING );
			return;
		}

		return $objCall;
	}

	public function createOutboundCallResult() {
		$objOutboundCallResult = new COutboundCallResult();

		$objOutboundCallResult->setDefaults();

		$objOutboundCallResult->setOutboundCallId( $this->getId() );
		$objOutboundCallResult->setCid( $this->getCid() );
		$objOutboundCallResult->setCallTypeId( $this->getCallTypeId() );

		return $objOutboundCallResult;
	}

	/**
	 * get functions
	 */

	public function loadCallQueue( $arrobjCallTypes ) {
		if( false == is_numeric( $this->getCallTypeId() ) ) {
			trigger_error( 'Call type id must be set before calling this function.', E_USER_ERROR );
			exit;
		}

		if( false == isset( $arrobjCallTypes[$this->getCallTypeId()] ) ) {
			trigger_error( 'Property call type was not sent to the function.', E_USER_ERROR );
			exit;
		}

		$this->setCallQueueId( $arrobjCallTypes[$this->getCallTypeId()]->getDefaultCallQueueId() );

		return;
	}

	public function fetchOutboundCallCampaign( $objVoipDatabase ) {
		$objOutboundCallCampaign = COutboundCallCampaigns::fetchOutboundCallCampaignById( $this->getOutboundCallCampaignId(), $objVoipDatabase );

		if( false == valObj( $objOutboundCallCampaign, 'COutboundCallCampaign' ) ) {
			trigger_error( 'Failed to load outbound call campaign associated with outbound call.', E_USER_ERROR );
			exit;
		}

		return $objOutboundCallCampaign;
	}

	public function fetchClient( $objAdminDatabase ) {
		$objClient = CClients::fetchClientById( $this->getCid(), $objAdminDatabase );

		if( false == valObj( $objClient, 'CClient' ) ) {
			trigger_error( 'Failed to load client associated with outbound call.', E_USER_ERROR );
			exit;
		}

		return $objClient;
	}

	public function fetchProperty( $objClientDatabase ) {
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objClientDatabase );

		if( false == valObj( $objProperty, 'CProperty' ) ) {
			trigger_error( 'Failed to load property associated with outbound call.', E_USER_ERROR );
			exit;
		}

		return $objProperty;
	}

	public function fetchCallTypeProducts( $objVoipDatabase ) {
		return CCallTypeProducts::fetchPublishedCallTypeProductsByCallTypeId( $this->getCallTypeId(), $objVoipDatabase );
	}

	public function fetchOutboundCallResults( $objVoipDatabase ) {
		return COutboundCallResults::fetchOutboundCallResultsByOutboundCallId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCallResults( $objVoipDatabase ) {
		return CCallResults::fetchCallResultsByCallTypeId( $this->getCallTypeId(), $objVoipDatabase );
	}

	public function fetchOutboundCallResult( $objVoipDatabase ) {
		return COutboundCallResults::fetchOutboundCallResultByOutboundCallId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCalls( $objVoipDatabase ) {
		return CCalls::fetchCallsByOutboundCallId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCustomer( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
	}

}
?>