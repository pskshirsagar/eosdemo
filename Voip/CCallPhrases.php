<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPhrases
 * Do not add any new functions to this class.
 */

class CCallPhrases extends CBaseCallPhrases {

	public static function fetchAllActiveCallPhrases( $objDatabase ) {
		$strSql = 'SELECT name FROM call_phrases where is_published = TRUE ';
		return self::fetchCachedObjects( $strSql, 'CCallPhrase', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>