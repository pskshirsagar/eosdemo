<?php

class CCallSource extends CBaseCallSource {

	const ON_SITE						= 1;
	const LEASING_CENTER				= 2;
	const SUPPORT						= 3;
	const RESIDENT_INSURE_AND_VERIFY	= 4;
	const UTILITY_BILLING				= 5;
	const OTHER							= 6;
	const MISCELLANEOUS					= 7;
}
?>