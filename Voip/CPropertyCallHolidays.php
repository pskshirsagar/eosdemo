<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyCallHolidays
 * Do not add any new functions to this class.
 */

class CPropertyCallHolidays extends CBasePropertyCallHolidays {

	public static function fetchPropertyCallHolidayByCidByPropertyIdByPropertyHolidayId( $intCid, $intPropertyId, $intPropertyHolidayId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						property_call_holidays 
					WHERE 
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND property_holiday_id = ' . ( int ) $intPropertyHolidayId;

		return self::fetchPropertyCallHoliday( $strSql, $objVoipDatabase );
	}

	public static function fetchPropertyCallHolidaysByPropertyHolidayIds( $arrintPropertyHolidayIds, $objVoipDatabase ) {

		if( false == valArr( $arrintPropertyHolidayIds ) ) return false;

		$strSql = 'SELECT 
						* 
					FROM 
						property_call_holidays
					WHERE property_holiday_id 
						IN ( ' . implode( ',', $arrintPropertyHolidayIds ) . ' )';

		return parent::fetchPropertyCallHolidays( $strSql, $objVoipDatabase );
	}

	/**
	 * Overridden Functions.
	 */

	public static function bulkUpdate( $arrobjPropertyCallHolidays, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly = false ) {

		foreach( $arrobjPropertyCallHolidays as $objPropertyCallHoliday ) {
			if( false == $objPropertyCallHoliday->getIsPublished() ) {
				$arrobjDeletablePropertyCallHolidays[] = $objPropertyCallHoliday;
			}
		}

		if( true == valArr( $arrobjDeletablePropertyCallHolidays ) && false == self::bulkDelete( $arrobjDeletablePropertyCallHolidays, $objDatabase ) ) {
			return false;
		}

		return parent::bulkUpdate( $arrobjPropertyCallHolidays, $arrmixFieldsToUpdate, $intCurrentUserId, $objDatabase, $boolIsReturnSqlOnly );
	}

}
?>