<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallMembers
 * Do not add any new functions to this class.
 */

class CCallMembers extends CBaseCallMembers {

	public static function fetchCallMemberBySessionUuidByCallQueueId( $strSessionUuid, $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM call_members WHERE session_uuid = \'' . $strSessionUuid . '\' AND call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1';
		return self::fetchCallMember( $strSql, $objVoipDatabase );
	}

	public static function fetchCallMemberByCallIdByCallQueueId( $intCallId, $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM call_members WHERE call_id = ' . ( int ) $intCallId . ' AND call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1';
		return self::fetchCallMember( $strSql, $objVoipDatabase );
	}

	public static function fetchCallMemberByCallId( $intCallId, $objVoipDatabase ) {
		return self::fetchCallMember( 'SELECT *FROM call_members WHERE call_id = ' . ( int ) $intCallId . ' LIMIT 1', $objVoipDatabase );
	}

}
?>