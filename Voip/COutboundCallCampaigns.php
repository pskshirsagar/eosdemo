<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\COutboundCallCampaigns
 * Do not add any new functions to this class.
 */

class COutboundCallCampaigns extends CBaseOutboundCallCampaigns {

	public static function fetchAllOutboundCallCampaigns( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM outbound_call_campaigns ORDER BY name';

		return self::fetchOutboundCallCampaigns( $strSql, $objVoipDatabase );
	}
}
?>