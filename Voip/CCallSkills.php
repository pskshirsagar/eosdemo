<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallSkills
 * Do not add any new functions to this class.
 */

class CCallSkills extends CBaseCallSkills {

	public static function fetchAllCallSkills( $objVoipDatabase ) {
		return self::fetchCallSkills( 'SELECT * FROM call_skills ORDER BY name', $objVoipDatabase );
	}

	public static function fetchPublishedCallSkills( $objVoipDatabase ) {
		return self::fetchCallSkills( 'SELECT * FROM call_skills WHERE is_published = true ORDER BY name', $objVoipDatabase );
	}

	public static function fetchCallSkillsByIds( $arrintCallSkillIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallSkillIds ) ) return NULL;
		$strSql = 'SELECT * FROM call_skills WHERE id IN ( ' . implode( ', ', $arrintCallSkillIds ) . ' )';
		return self::fetchCallSkills( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallSkills( $objPagination, $objVoipDatabase, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$strWhereConditions = '';

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(cs.id)';
		} else {
			$strSql = 'SELECT
							cs.*,
							cq.name AS call_queue_name';

			$strSqlOrderBy		= ' ORDER BY cs.name';
			$strSqlOffsetLimit	= ( false == valStr( $objPagination->getQuickSearch() ) ) ? ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize() : '';
		}

		$strSql .= ' FROM
						call_skills AS cs
						JOIN call_skill_queues csq ON ( csq.call_skill_id = cs.id )
						JOIN call_queues AS cq ON ( cq.id = csq.call_queue_id )';

		if( true == valStr( $objPagination->getQuickSearch() ) ) {
			$strWhereConditions .= ' WHERE ( CAST( cs.id AS text ) LIKE \'' . addslashes( $objPagination->getQuickSearch() ) . '\'
									OR lower( cs.name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' )
									OR lower( cq.name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) )';
		}

		$strSql .= $strWhereConditions;
		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchCallSkills( $strSql, $objVoipDatabase );
		}
	}

}
?>