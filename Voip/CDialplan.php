<?php

class CDialplan extends CBaseDialplan {

	// Phone utlities dialplans
	const DIAL_PLAN_CURRENT_RESIDENT_QUESTION					= 6070;
	const DIAL_PLAN_MARKETING_AND_WEBSITE_PRODUCT				= 6071;
	const DIAL_PLAN_ONLINE_LEASE_AND_APPLICATIONS				= 6072;
	const DIAL_PLAN_ENTRATA_CORE								= 6073;
	const DIAL_PLAN_SUPPORT_VOICEMAIL							= 6074;
	const DIAL_PLAN_INSIDE_SALES_NON_CURRENT_CLIENTS			= 6075;
	const DIAL_PLAN_GENERAL_SUPPORT								= 6101;
	const DIAL_PLAN_GID_WINDSOR									= 6076;
	const DIAL_PLAN_COLONIAL_QUEUE								= 6071;

	// Phone Utilities support dialplans.
	const AGENT_REMOVE											= 'AgentRemove';
	const AGENT_IN_LEASING_CHAT									= 'AgentInLeasingChat';
	const AGENT_IN_EMAIL_QUEUE									= 'AgentInEmailQueue';
	const AGENT_ON_COACHING										= 'AgentOnCoaching';
	const AGENT_ON_TRAINING										= 'AgentOnTraining';
	const AGENT_ON_LUNCH										= 'AgentOnLunch';
	const AGENT_ON_PROJECT										= 'AgentOnProject';
	const AGENT_ON_MEETING										= 'AgentOnMeeting';
	const AGENT_ON_BREAK										= 'AgentOnBreak';
	const AGENT_LOGOUT											= 'AgentLogout';
	const AGENT_ON_DEMAND										= 'AgentOnDemand';
	const AGENT_RELOGIN											= 'AgentReLogin';
	const AGENT_LOGIN											= 'AgentLogin';

	// Direct Extension Dialplans.
	const ROUTE_CALL_TO_PSI_EMPLOYEE							= 55;

	// Support System Dialplans.
	const DIAL_PLAN_MAIN_SUPPORT								= 'main_support';
	const ENTRATA_SUPPORT										= 'entrata_support';
	const DIAL_PLAN_RESIDENT_INSURE_SUPPORT						= 'resident_insure_support_center';
	const NEW_DIAL_PLAN_RESIDENT_INSURE_SUPPORT					= 'new_resident_insure_support_center';
	const DIAL_PLAN_RESIDENT_INSURE_AFTER_HOURS_SUPPORT_CENTER	= 'resident_insure_after_hours_support_center';
	const DIAL_PLAN_RESIDENT_INSURE_DURING_HOURS_SUPPORT_CENTER	= 'resident_insure_during_hours_support_center';
	const VACANCY_ORGANIC_LEAD_CALLS							= 'VacancyOrganicLeadCalls';
	const VACANCY_PAID_LEAD_CALLS								= 'VacancyPaidLeadCalls';

	// Applications dialplans
	const FORWARD_CALL_TO_PROPERTY_OFFICE						= 'forward_call_to_property_office_number';
	const FORWARD_CALL_TO_PROPERTY_LEAD_NUMBER					= 'forward_call_to_property_lead_number';
	const FORWARD_CALL_TO_PROPERTY_MAINTENANCE_NUMBER			= 'forward_call_to_property_maintenance_number';
	const FORWARD_CALL_TO_PROPERTY_MAINTENANCE_EMERGENCY_NUMBER = 'forward_call_to_property_maintenance_emergency_number';
	const FORWARD_CALL_TO_EXTERNAL_NUMBER						= 'forward_call_to_external_number';
	const FORWARD_CALL_TO_INTERNAL_NUMBER						= 'forward_call_to_internal_number';
	const FORWARD_CALL_TO_EXTERNAL_NUMBER_WITH_RECORDING		= 'forward_call_to_external_number_with_recording';
	const FORWARD_CALL_TO_RECORDING_MESSAGE						= 'forward_call_to_recording_message';
	const FORWARD_CALL_TO_VOICEMAIL								= 'forward_call_to_voicemail';

	// Call Transfer Dialplans.
	const TRANSFER_CALL_TO_CALLCENTER							= 'transfer_call_to_callcenter';
	const TRANSFER_CALL_TO_EXTERNAL								= 'transfer_call_to_external';

	// Call Routing diaplans.
	const ROUTE_CALL_TO_CALLCENTER								= 'route_call_to_callcenter';
	const SUPPORT_CALLS											= 'support_calls';
	const INSIDE_SALES											= 'inside_sales';
	const ROUTE_CALL_TO_QUEUE									= 'route_call_to_queue';
	const ROUTE_CALL_TO_EMPLOYEE								= 'route_call_to_employee';
	const ROUTE_CALL_TO_VOICEMAIL								= 'route_call_to_voicemail';
	const ROUTE_CALL_TO_INTERNAL								= 'route_call_to_internal';
	const ROUTE_CALL_TO_UTILITY_BILLING							= 'route_call_to_utility_billing';
	const ROUTE_CALL_TO_MAINTENANCE								= 'route_call_to_maintenance';
	const PUSH_CALL_TO_EXTERNAL_NUMBER							= 'push_call_to_external_number';

	// Supporting Dialplans
	const SET_PREFERENCES										= 'set_preferences';
	const INTERNAL												= 'internal';
	const BROADCAST_SCHEDULED_OUTBOUND_CALLS					= 'broadcast_scheduled_outbound_calls';
	const ROUTE_MOVE_IN_OUTBOUND_CALLS							= 'route_move_in_outbound_calls';

	/**
	 * Construct function.
	 */

	public function setDefaults() {
		$this->m_strFilePath = '/Macros/';
		return;
	}

	/**
	 * Set Functions Get Functions
	 */

	public function getFullFilePath() {
		return PATH_CALL_CENTER_APPLICATION . $this->getFilePath();
	}

	public function getAbsoluteFilePath() {
		return PATH_CALL_CENTER_APPLICATION . $this->getFilePath() . $this->getFileName();
	}

	public function getFileExtension() {
		return pathinfo( $this->getAbsoluteFilePath(), PATHINFO_EXTENSION );
	}

	/**
	 * Validate Functions
	 */

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Dialplan Name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valConflictName( $objVoipDatabase ) {

		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_ERROR );
		}

		if( false == $this->valName() ) {
			$boolIsValid &= false;
		} elseif( true == valObj( $this->fetchDialplanByName( $objVoipDatabase ), 'CDialplan' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Dialplan Name is already in use.' ) );
		}
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Dialplan File Name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valConflictFileName( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_ERROR );
		}

		if( false == $this->valFileName() ) {
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valExtension() {
		$boolIsValid = true;

		if( true == is_null( $this->getExtension() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Dialplan extension is required.' ) );
		}
		return $boolIsValid;
	}

	public function valConflictExtension( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_ERROR );
		}

		if( false == $this->valExtension() ) {
			$boolIsValid &= false;
		} elseif( true == valObj( $this->fetchDialplanByExtension( $objVoipDatabase ), 'CDialplan' ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Dialplan extension is already in use.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objVoipDatabase );
				$boolIsValid &= $this->valConflictFileName( $objVoipDatabase );
				$boolIsValid &= $this->valConflictExtension( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				// validate delete.
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * fetch functions
	 */

	public function fetchDialplanByName( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM dialplans WHERE name = \'' . $this->getName() . '\'';

		if( false == is_null( $this->getId() ) ) $strSql .= ' AND id <> ' . ( int ) $this->getId();

		return CDialplans::fetchDialplan( $strSql, $objVoipDatabase );
	}

	public function fetchDialplanByExtension( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM dialplans WHERE extension = \'' . $this->getExtension() . '\'';

		if( false == is_null( $this->getId() ) ) $strSql .= ' AND id <> ' . ( int ) $this->getId();

		return CDialplans::fetchDialplan( $strSql, $objVoipDatabase );
	}

	public function getDialplanContent() {
		return CFileIo::fileGetContents( $this->getAbsoluteFilePath() );
	}

}

?>