<?php

class CGreeting extends CBaseGreeting {

	protected $m_strGreetingTypeName;
	protected $m_strClientName;
	protected $m_strPropertyName;
	protected $m_strFileName;

	protected $m_boolIsReplaceGreeting;

	const FILE_EXTENSION_TYPE_MP3						= 'mp3';
	const FILE_EXTENSION_TYPE_WAV						= 'wav';
	const PROPERTY_DEFAULT_GREETING						= 'CALLTRACKER_DEFAULT_GREETING';
	const PROPERTY_DEFAULT_RECORDED_MESSAGE_GREETING	= 'CALL_TRACKER_RECORDED_MESSAGE_GREETING';
	const PROPERTY_DEFAULT_VOICEMAIL					= 'CALLTRACKER_DEFAULT_VM_GREETING';
	const PORPERTY_PHONE_NUMBER							= '877-399-8096';

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['greeting_type_name'] ) )		$this->setGreetingTypeName( $arrmixValues['greeting_type_name'] );
		if( isset( $arrmixValues['is_replace_greeting'] ) )		$this->setIsReplaceGreeting( $arrmixValues['is_replace_greeting'] );
		if( isset( $arrmixValues['client_name'] ) )				$this->setClientName( $arrmixValues['client_name'] );
		if( isset( $arrmixValues['property_name'] ) )			$this->setPropertyName( $arrmixValues['property_name'] );
		if( isset( $arrmixValues['file_name'] ) )				$this->setFileName( $arrmixValues['file_name'] );
		return;
	}

	public function setGreetingTypeName( $strGreetingTypeName ) {
		$this->m_strGreetingTypeName = $strGreetingTypeName;
	}

	public function setIsReplaceGreeting( $boolIsReplaceGreeting ) {
		$this->m_boolIsReplaceGreeting = $boolIsReplaceGreeting;
	}

	public function setClientName( $boolClientName ) {
		$this->m_strClientName = $boolClientName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	/**
	* Get Functions
	*
	*/

	public function getGreetingTypeName() {
		return $this->m_strGreetingTypeName;
	}

	public function getIsReplaceGreeting() {
		return $this->m_boolIsReplaceGreeting;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	/**
	*  Validate Functions
	*
	*/

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', __( 'Property is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valGreetingTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getGreetingTypeId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Greeting type is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valCallFileId() {
		$boolIsValid = true;
		if( true == is_null( $this->getCallFileId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Greeting file is required.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valGreetingCallFile() {
		$boolIsValid = true;

		if( false == $this->getIsReplaceGreeting() && false == is_null( $this->getId() ) ) return $boolIsValid;

		$intMaxUpload = 7 * 1024 * 1024;

		if( true == array_key_exists( 'upload_greeting', $_FILES ) && true == valArr( $_FILES['upload_greeting'] ) ) {
			if( true == is_null( $_FILES['upload_greeting']['name'] ) || 0 == $_FILES['upload_greeting']['size'] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload file. Files should be larger than {%d, file_size} KB.', [ 'file_size' => 0 ] ), NULL ) );
				return false;
			}

			if( $_FILES['upload_greeting']['size'] > $intMaxUpload ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to upload file. Files should not be larger than {%d, max_upload_size} KB.', [ 'max_upload_size' => ( int ) $intMaxUpload ] ), NULL ) );
				return false;
			}

			$arrstrFileInfo = pathinfo( $_FILES['upload_greeting']['name'] );
			$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;

			if( CGreetingType::IVR == $this->getGreetingTypeId() || CGreetingType::IVR_DURING_HOURS == $this->getGreetingTypeId() || CGreetingType::IVR_AFTER_HOURS == $this->getGreetingTypeId() ) {
				if( $strExtension != self::FILE_EXTENSION_TYPE_WAV && $strExtension != self::FILE_EXTENSION_TYPE_MP3 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'This file type is not allowed.' ) ) );
					return false;
				}
			} else {
				if( $strExtension != self::FILE_EXTENSION_TYPE_WAV && $strExtension != self::FILE_EXTENSION_TYPE_MP3 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'This file type is not allowed.' ) ) );
					return false;
				}
			}
		} else {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_size', __( 'Failed to upload file. Files should be larger than {%d, file_size} KB.', [ 'file_size' => 0 ] ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valRecordedGreetingCallFile() {
		$boolIsValid = true;

		if( false == $this->getIsReplaceGreeting() && false == is_null( $this->getId() ) ) return $boolIsValid;

		if( false == array_key_exists( 'upload_recorded_greeting', $_FILES ) || false == valArr( $_FILES['upload_recorded_greeting'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Call recording is required.' ), NULL ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valConflictGreeting( $objVoipDatabase ) {
		$boolIsValid	= true;
		$intGreetingId	= $this->fetchGreeting( $objVoipDatabase );

		if( true == valId( $intGreetingId ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, "__( 'This property already has custom voicemail greeting. Please' ) . ' <a href=\"javascript:void(0)\" onclick=\"javascript:editGreeting( '" . $intGreetingId . "')\">' . __( 'click here' ) . '</a>' . __( 'to edit uploaded file .' )", NULL ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Greeting name is required.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL, $boolIsRecordedFileUpload = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'insert_greeting':
				$boolIsValid &= $this->valConflictGreeting( $objVoipDatabase );
				$boolIsValid &= $this->valGreetingTypeId();
				$boolIsValid &= $this->valCid();

				if( CGreetingType::IVR != $this->getGreetingTypeId() ) {
					$boolIsValid &= $this->valPropertyId();
				} else {
					$boolIsValid &= $this->valName();
				}

				if( false == $boolIsRecordedFileUpload ) {
					$boolIsValid &= $this->valGreetingCallFile();
				} else {
					$boolIsValid &= $this->valRecordedGreetingCallFile();
				}

				$boolIsValid &= $this->valCallFileId();
				break;

			case 'recorded_message_greeting':
				$boolIsValid &= $this->valConflictGreeting( $objVoipDatabase );
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valRecordedGreetingCallFile();
				$boolIsValid &= $this->valCallFileId();
				break;

			case 'update_greeting':
				$boolIsValid &= $this->valGreetingTypeId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();

				if( false == $boolIsRecordedFileUpload ) {
					$boolIsValid &= $this->valGreetingCallFile();
				} else {
					$boolIsValid &= $this->valRecordedGreetingCallFile();
				}

				$boolIsValid &= $this->valCallFileId();
				break;

			case 'insert_ivr_menu_greeting':
				if( false == $boolIsRecordedFileUpload ) {
					$boolIsValid &= $this->valGreetingCallFile();
				} else {
					$boolIsValid &= $this->valRecordedGreetingCallFile();
					$boolIsValid &= $this->valName();
				}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictGreeting( $objVoipDatabase );
				$boolIsValid &= $this->valGreetingTypeId();
				$boolIsValid &= $this->valCid();

				if( CGreetingType::IVR != $this->getGreetingTypeId() ) {
					$boolIsValid &= $this->valPropertyId();
				} else {
					$boolIsValid &= $this->valName();
				}

				if( false == $boolIsRecordedFileUpload ) {
					$boolIsValid &= $this->valGreetingCallFile();
				} else {
					$boolIsValid &= $this->valRecordedGreetingCallFile();
				}
				break;

			case VALIDATE_INSERT:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Create Greeting
	*
	*/

	public function createCallFile( $objVoipDatabase ) {
		$objCallFile = new CCallFile();

		$objCallFile->setId( $objCallFile->fetchNextId( $objVoipDatabase ) );
		$objCallFile->setFileExtensionId( CFileExtension::AUDIO_MP3 );
		$objCallFile->setFileDatetime( 'NOW()' );
		$objCallFile->setFileName( $objCallFile->buildFileName( 'mp3' ) );
		$objCallFile->setFilePath( $objCallFile->buildGreetingCallFilePath( $this->getCid() ) );

		$this->setCallFileId( $objCallFile->getId() );

		return $objCallFile;
	}

	/**
	* Fetch Functions
	*
	*/

	public function fetchGreeting( $objVoipDatabase ) {

		$strSql = 'SELECT
						id
					FROM
						greetings
					WHERE
						cid = ' . ( int ) $this->getCid() . '
						AND property_id = ' . ( int ) $this->getPropertyId() . '
						AND greeting_type_id = ' . ( int ) $this->getGreetingTypeId();

		if( true == valId( $this->getId() ) ) {
			$strSql .= ' AND id <> ' . ( int ) $this->getId();
		}

		$arrintGreetingIds = fetchData( $strSql, $objVoipDatabase );
		return ( true == isset( $arrintGreetingIds[0]['id'] ) ) ? $arrintGreetingIds[0]['id'] : 0;
	}

}
?>