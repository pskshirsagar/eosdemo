<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPhoneExtensions
 * Do not add any new functions to this class.
 */

class CPhoneExtensions extends CBasePhoneExtensions {

	public static function fetchPhoneExtensionByPhoneExtensionTypeIdsByExtension( $arrintPhoneExtensionTypeIds, $strExtension, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM phone_extensions WHERE phone_extension_type_id IN ( ' . implode( ', ', $arrintPhoneExtensionTypeIds ) . ') AND extension = \'' . $strExtension . '\' LIMIT 1';
		return self::fetchPhoneExtension( $strSql, $objVoipDatabase );
	}

	public static function fetchPhoneExtensionsByPhoneExtensionTypeId( $intPhoneExtensionTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						phone_extensions
					WHERE
						phone_extension_type_id = ' . ( int ) $intPhoneExtensionTypeId . '
					ORDER BY id ASC';

		return self::fetchPhoneExtensions( $strSql, $objVoipDatabase );
	}

	public static function fetchPhysicalPhoneExtensionCountById( $intPhysicalPhoneExtensionId, $objVoipDatabase ) {
		$strWhere = ' WHERE
						phone_extension_type_id = ' . CPhoneExtensionType::PHYSICAL_PHONE_EXTENSION_TYPE . '
						AND id = ' . ( int ) $intPhysicalPhoneExtensionId;

		return self::fetchPhoneExtensionCount( $strWhere, $objVoipDatabase );
	}

	public static function fetchPhysicalPhoneExtensionByPhoneExtensionId( $intPhoneExtensionId, $objVoipDatabase ) {
		return self::fetchPhoneExtension( 'SELECT *FROM phone_extensions WHERE phone_extension_type_id = ' . CPhoneExtensionType::PHYSICAL_PHONE_EXTENSION_TYPE . ' AND id = ' . ( int ) $intPhoneExtensionId, $objVoipDatabase );
	}

	public static function fetchUnusedPhysicalPhoneExtensions( $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						phone_extensions
					WHERE
						id NOT IN (
							SELECT physical_phone_extension_id FROM (
								SELECT desk_phone_extension_id AS physical_phone_extension_id FROM call_agents
								UNION
								SELECT physical_phone_extension_id AS physical_phone_extension_id FROM call_agents
								UNION
								SELECT phone_extension_id AS physical_phone_extension_id FROM call_phone_numbers
							) phone_extension_ids
							WHERE
								physical_phone_extension_id IS NOT NULL
						) AND phone_extension_type_id = ' . CPhoneExtensionType::PHYSICAL_PHONE_EXTENSION_TYPE . '
					ORDER BY id ASC';

		return self::fetchPhoneExtensions( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedPhoneExtensionsByPhoneExtensionFilter( $objPagination, $objPhoneExtensionFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		$strSql            = '';
		$strSqlOrderBy     = '';
		$strSqlOffsetLimit = '';
		$arrstrWhereSql		= array();

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT COUNT( pe.id ) FROM
							phone_extensions AS pe
						LEFT JOIN call_agents AS ca ON ( ca.physical_phone_extension_id = pe.id  OR ca.agent_phone_extension_id = pe.id )';
		} else {
			$strSql = 'SELECT
							pe.*,
							ca.full_name as call_agent_name,
							( CASE WHEN pe.id IN (
										SELECT physical_phone_extension_id FROM (
											SELECT desk_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT physical_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT agent_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT phone_extension_id AS physical_phone_extension_id FROM call_phone_numbers
										) phone_extension_ids
										WHERE
										physical_phone_extension_id IS NOT NULL
									) THEN 1 ELSE 2 END ) AS extension_status_id
						FROM
							phone_extensions AS pe
						LEFT JOIN call_agents AS ca ON ( ca.physical_phone_extension_id = pe.id  OR ca.agent_phone_extension_id = pe.id )';

			$strSqlOrderBy     = ' ORDER BY pe.id ASC';
			$strSqlOffsetLimit = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valStr( $objPhoneExtensionFilter->getGenericData() ) ) {
			$strGenericData   = $objPhoneExtensionFilter->getGenericData();
			$arrstrWhereSql[] = ' ( ( pe.phone_name ) LIKE  \'%' . addslashes( $strGenericData ) . '%\'  ) ';
		}

		if( true == valStr( $objPhoneExtensionFilter->getEmployeeIds() ) ) {
			$arrstrWhereSql[] = ' ca.employee_id  IN  ( ' . $objPhoneExtensionFilter->getEmployeeIds() . ' ) ';
		}

		if( true == valStr( $objPhoneExtensionFilter->getPhoneExtensionTypeIds() ) ) {
			$arrstrWhereSql[] = ' pe.phone_extension_type_id  IN  ( ' . $objPhoneExtensionFilter->getPhoneExtensionTypeIds() . ' ) ';
		}

		if( false == is_null( $objPhoneExtensionFilter->getExtensionStatusId() ) ) {
			if( CPhoneExtensionFilter::USED_EXTENSION == $objPhoneExtensionFilter->getExtensionStatusId() ) {
				$arrstrWhereSql[] = ' pe.id IN (
										SELECT physical_phone_extension_id FROM (
											SELECT desk_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT physical_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT agent_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT phone_extension_id AS physical_phone_extension_id FROM call_phone_numbers
										) phone_extension_ids
										WHERE
										physical_phone_extension_id IS NOT NULL
									)';
			} elseif( CPhoneExtensionFilter::FREE_EXTENSION == $objPhoneExtensionFilter->getExtensionStatusId() ) {
				$arrstrWhereSql[] = ' pe.id NOT IN (
										SELECT physical_phone_extension_id FROM (
											SELECT desk_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT physical_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT agent_phone_extension_id AS physical_phone_extension_id FROM call_agents
											UNION
											SELECT phone_extension_id AS physical_phone_extension_id FROM call_phone_numbers
										) phone_extension_ids
										WHERE
										physical_phone_extension_id IS NOT NULL
									)';
			}
		}

		if( true == valArr( $arrstrWhereSql ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhereSql );
		}

		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;
		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrstrData[0]['count'] ) ) ? $arrstrData[0]['count'] : 0;
		}

		return self::fetchPhoneExtensions( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchPhoneExtensions( $strSearchKeyword, $objVoipDatabase ) {
		$strSql = 'SELECT
						pe.*
					FROM
						phone_extensions AS pe
					WHERE
						( lower( pe.phone_name ) LIKE lower( \'%' . addslashes( $strSearchKeyword ) . '%\' ) )
					ORDER BY pe.phone_name ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePhoneExtensionById( $intPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						pe.*,
						ca.employee_id AS employee_id,
						ca.full_name AS call_agent_name
					FROM
						phone_extensions AS pe
					LEFT JOIN call_agents AS ca ON ( ca.physical_phone_extension_id = pe.id  OR ca.agent_phone_extension_id = pe.id )
					WHERE 
						pe.id = ' . ( int ) $intPhoneExtensionId;

		return self::fetchPhoneExtension( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleAgentPhoneExtensionsById( $intPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						phone_extensions.id AS phone_extension_id
					FROM 
						phone_extensions
					LEFT OUTER JOIN call_agents ON ( phone_extensions.id = call_agents.agent_phone_extension_id )
					WHERE 
						phone_extensions.phone_extension_type_id = ' . CPhoneExtensionType::AGENT_PHONE_EXTENSION_TYPE . '
						AND ( call_agents.agent_phone_extension_id IS NULL 
							OR call_agents.agent_phone_extension_id = ' . ( int ) $intPhoneExtensionId . '
						)
					ORDER BY phone_extension_id ASC';

		$arrintPhoneExtensionIds = fetchData( $strSql, $objVoipDatabase );

		if( false == valArr( $arrintPhoneExtensionIds ) ) {
			return NULL;
		}

		$arrintPhoneExtensionIds = array_keys( rekeyArray( 'phone_extension_id', $arrintPhoneExtensionIds ) );

		return $arrintPhoneExtensionIds;
	}

}
?>