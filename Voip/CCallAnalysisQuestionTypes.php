<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisQuestionTypes
 * Do not add any new functions to this class.
 */

class CCallAnalysisQuestionTypes extends CBaseCallAnalysisQuestionTypes {

	public static function fetchCallAnalysisQuestionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CCallAnalysisQuestionType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallAnalysisQuestionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CCallAnalysisQuestionType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>