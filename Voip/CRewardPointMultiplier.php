<?php

class CRewardPointMultiplier extends CBaseRewardPointMultiplier {

	const WORKING_WEEKENDS		= 1;
	const EXTRA_HOURS_WORKED	= 2;
	const HOLIDAY				= 3;
	const YELLOW_ZONE			= 4;
	const RED_ZONE				= 5;
	const COVERING_A_SHIFT		= 6;

	public static $c_arrstrRewardPointMultiplierColorNames = [
		self::WORKING_WEEKENDS		=> 'dark-blue',
		self::EXTRA_HOURS_WORKED	=> 'light-orange',
		self::HOLIDAY				=> 'light-purple',
		self::YELLOW_ZONE			=> 'light-yellow',
		self::RED_ZONE				=> 'red',
		self::COVERING_A_SHIFT		=> 'light-blue'
	];

	public static $c_arrstrRewardPointMultiplierTextClassNames = [
		self::WORKING_WEEKENDS		=> 'text-white',
		self::EXTRA_HOURS_WORKED	=> 'text-white',
		self::HOLIDAY				=> 'text-white',
		self::YELLOW_ZONE			=> 'text-black',
		self::RED_ZONE				=> 'text-white',
		self::COVERING_A_SHIFT		=> 'text-white'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPointMultiplier() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getPointMultiplier() ) || 0 >= $this->getPointMultiplier() || false == valStr( $this->getPointMultiplier() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Please enter valid value.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_reward_point_multiplier':
				$boolIsValid &= $this->valPointMultiplier();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>