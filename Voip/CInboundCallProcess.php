<?php

class CInboundCallProcess extends CBaseInboundCallProcess {

	protected $m_strResponseTypeName;
	protected $m_strResponseTypeDescription;
	protected $m_strProcessDateTime;

    // *****************************************************
    // ***************** get function **********************
    // *****************************************************

    public function getResponseTypeName() {
        return $this->m_strResponseTypeName;
    }

    public function getResponseTypeDescription() {
        return $this->m_strResponseTypeDescription;
    }

    public function getProcessDateTime() {
        return $this->m_strProcessDateTime;
    }

    // *****************************************************
    // ***************** set function **********************
    // *****************************************************

    public function setResponseTypeName( $strResponseTypeName ) {
        $this->m_strResponseTypeName = $strResponseTypeName;
    }

    public function setResponseTypeDescription( $strResponseTypeDescription ) {
        $this->m_strResponseTypeDescription = $strResponseTypeDescription;
    }

    public function setProcessDateTime( $strProcessDateTime ) {
        $this->m_strProcessDateTime = $strProcessDateTime;
    }

 public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
    	parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

    	if( true == isset( $arrValues['name'] ) ) 					$this->setResponseTypeName( $arrValues['name'] );
        if( true == isset( $arrValues['description'] ) ) 			$this->setResponseTypeDescription( $arrValues['description'] );
        if( true == isset( $arrValues['process_datetime'] ) ) 		$this->setProcessDateTime( $arrValues['process_datetime'] );

        return;
 }

}
?>