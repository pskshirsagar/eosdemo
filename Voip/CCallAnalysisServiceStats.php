<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisServiceStats
 * Do not add any new functions to this class.
 */

class CCallAnalysisServiceStats extends CBaseCallAnalysisServiceStats {

	public static function fetchPaginatedCallAnalysisServiceStats( $objPagination, $objVoipDatabase, $boolIsFromQuickSearch = false, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$strWhereConditions = '';

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(cass.id)';
		} else {
			$strSql = 'SELECT
							cass.*,
							c.company_name AS company_name,
							pcs.property_name AS property_name';

			$strSqlOrderBy		= ' ORDER BY c.company_name, pcs.property_name';
			$strSqlOffsetLimit	= ( false == $boolIsFromQuickSearch ) ? ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize() : '';
		}

		$strSql .= ' FROM
						call_analysis_service_stats AS cass
						LEFT JOIN clients AS c ON ( c.id = cass.cid )
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = cass.property_id AND pcs.cid = cass.cid )';

		if( true == valStr( $objPagination->getQuickSearch() ) ) {

			$strWhereConditions = ' WHERE
										( lower( c.company_name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' )
										OR lower( pcs.property_name ) LIKE lower( \'%' . addslashes( $objPagination->getQuickSearch() ) . '%\' ) )';
		}

		$strSql .= $strWhereConditions;
		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;
		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return parent::fetchCallAnalysisServiceStats( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallAnalysisServiceStatById( $intCallAnalysisServiceStatId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cass.*,
						c.company_name AS company_name,
						pcs.property_name AS property_name
					FROM
						call_analysis_service_stats AS cass
						LEFT JOIN clients AS c ON ( c.id = cass.cid )
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = cass.property_id AND pcs.cid = cass.cid )
					WHERE
						cass.id = ' . ( int ) $intCallAnalysisServiceStatId;

		return parent::fetchCallAnalysisServiceStat( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallAnalysisServiceStats( $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_analysis_service_stats';

		return parent::fetchCallAnalysisServiceStats( $strSql, $objVoipDatabase );
	}

}

?>