<?php

class CCallAgentSchedule extends CBaseCallAgentSchedule {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Update Call Agent Schedule Functions
	 *
	 */

	public static function updateScheduleFromStartDateToEndDateForCallAgentWithCallAgentStatusTypeId( $strStartDatetime, $strEndDatetime, $intCallAgentId, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) {
		if( false == valStr( $strStartDatetime ) || false == valStr( $strEndDatetime ) || false == valId( $intCallAgentId ) || false == valId( $intCallAgentStatusTypeId ) || false == valId( $intUserId ) ) return false;
		$intStartDateEpoch			= strtotime( $strStartDatetime );
		$intEndDateEpoch			= strtotime( $strEndDatetime );
		$arrobjCallAgentSchedules	= \Psi\Eos\Voip\CCallAgentSchedules::createService()->fetchCallAgentSchedulesByCallAgentIdByIntervalStartEpochByIntervalEndEpoch( $intCallAgentId, $intStartDateEpoch, $intEndDateEpoch, $objVoipDatabase );

		if( false == valArr( $arrobjCallAgentSchedules ) ) {
			return CCallAgentHelper::insertNewScheduleFromStartDateToEndDateForCallAgentWithCallAgentStatusTypeId( $strStartDatetime, $strEndDatetime, $intCallAgentId, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase );
		}

		if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjCallAgentSchedules ) ) {
			$arrobjUpdateCallAgentScheduleDetails = [];

			reset( $arrobjCallAgentSchedules );

			$intStartScheduleId	= key( $arrobjCallAgentSchedules );
			$intEndScheduleId	= $intStartScheduleId;

			if( 7 == ( ( $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalEndEpoch() - $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch() ) / 86400 ) ) {
				if( 1 == date( 'N', $intStartDateEpoch ) ) {
					$intStartIntervalSequence = round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 );
				} else {
					$intStartIntervalSequence = ( ( 48 * ( date( 'N', $intStartDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 ) );
				}

				if( 1 == date( 'N', $intEndDateEpoch ) ) {
					$intEndIntervalSequence = ( 0 == date( 'H.i', $intEndDateEpoch ) ) ? 336 : round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) );
				} else {
					$intEndIntervalSequence = ( ( 48 * ( date( 'N', $intEndDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) ) );
				}

				if( false == CCallAgentHelper::updateScheduleDetailsFromStartIntervalSequenceToEndIntervalSequenceForCallAgentWithCallAgentStatusTypeId( $intCallAgentId, $intEndScheduleId, $intStartIntervalSequence, $intEndIntervalSequence, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) ) {
					return false;
				}

				return true;
			} else {
				$arrobjCallAgentScheduleDetails	= CCallAgentScheduleDetails::fetchCallAgentScheduleDetailsByCallAgentScheduleId( $intStartScheduleId, $objVoipDatabase );
				$arrobjCallAgentScheduleDetails	= rekeyObjects( 'IntervalSequence', $arrobjCallAgentScheduleDetails );
				$arrobjNewCallAgentSchedule		= [];

				if( true == is_null( $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalEndEpoch() ) ) {
					$intWeeks = round( ( ( $intEndDateEpoch - $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch() ) / 86400 ) / 7, 0, PHP_ROUND_HALF_DOWN );
					$intLastWeek = $intWeeks + 1;
					$intWeeks += 2;
				} else {
					$intWeeks = round( ( ( ( $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalEndEpoch() - $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch() ) / 86400 ) / 7 ) );
				}

				for( $intWeekSequence = 0; $intWeeks > $intWeekSequence; $intWeekSequence++ ) {
					$arrobjNewCallAgentSchedule[$intWeekSequence] = clone $arrobjCallAgentSchedules[$intStartScheduleId];
					$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalStartTimestamp( date( 'Y-m-d H:i:s', strtotime( '+' . ( 7 * $intWeekSequence ) . ' day', strtotime( $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartTimestamp() ) ) ) );
					$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalStartEpoch( strtotime( $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartTimestamp() ) );
					$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndTimestamp( date( 'Y-m-d H:i:s', strtotime( '+7 day', $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartEpoch() ) ) );
					$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndEpoch( strtotime( $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalEndTimestamp() ) );

					if( 0 == $intWeekSequence ) {
						if( false == $arrobjNewCallAgentSchedule[0]->update( $intUserId, $objVoipDatabase ) ) {
							return false;
						}
					}

					if( 0 < $intWeekSequence ) {
						$arrobjNewCallAgentSchedule[$intWeekSequence]->setId( NULL );

						if( true == is_null( $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalEndEpoch() ) && $intLastWeek == $intWeekSequence ) {
							$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndTimestamp( NULL );
							$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndEpoch( NULL );
						}

						if( false == $arrobjNewCallAgentSchedule[$intWeekSequence]->insert( $intUserId, $objVoipDatabase ) ) {
							return false;
						}

						$strIntervalStartTimestamp	= date( 'Y-m-d', $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartEpoch() );
						$strSqlBulkInsertAppend		= NULL;

						for( $intIntervalSequence = 1; 336 >= $intIntervalSequence; $intIntervalSequence++ ) {
							$intIntervalDayOfWeek = intval( ( $intIntervalSequence + 47 ) / 48 );

							if( true == valId( $arrobjCallAgentScheduleDetails[$intIntervalSequence]->getCallAgentStatusTypeId() ) ) {
								$strSqlBulkInsertAppend		.= ' ( ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getId() . ', ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getCallAgentId() . ', ' . ( int ) $arrobjCallAgentScheduleDetails[$intIntervalSequence]->getCallAgentStatusTypeId() . ', \'' . date( 'Y-m-d H:i', strtotime( $strIntervalStartTimestamp ) ) . '\', ' . ( int ) $intIntervalDayOfWeek . ', ' . strtotime( $strIntervalStartTimestamp ) . ', ' . ( int ) $intIntervalSequence . ',1, ' . ( int ) $intUserId . ', NOW(), ' . ( int ) $intUserId . ', NOW() ) ';
							} else {
								$strSqlBulkInsertAppend		.= ' ( ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getId() . ', ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getCallAgentId() . ', NULL, \'' . date( 'Y-m-d H:i', strtotime( $strIntervalStartTimestamp ) ) . '\', ' . ( int ) $intIntervalDayOfWeek . ', ' . strtotime( $strIntervalStartTimestamp ) . ', ' . ( int ) $intIntervalSequence . ',1, ' . ( int ) $intUserId . ', NOW(), ' . ( int ) $intUserId . ', NOW() ) ';
							}

							$strSqlBulkInsertAppend		.= ',';
							$strIntervalStartTimestamp	 = date( 'Y-m-d H:i', strtotime( '+30 minute', strtotime( $strIntervalStartTimestamp ) ) );
						}

						if( true == valStr( $strSqlBulkInsertAppend ) ) {
							if( ',' == \Psi\CStringService::singleton()->substr( $strSqlBulkInsertAppend, -1 ) ) $strSqlBulkInsertAppend = rtrim( $strSqlBulkInsertAppend, ',' );
							if( false == \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->buildCallAgentScheduleDetails( $strSqlBulkInsertAppend, $objVoipDatabase ) ) {
								return false;
							}
						}
					}
				}

				$arrobjCallAgentSchedules = \Psi\Eos\Voip\CCallAgentSchedules::createService()->fetchCallAgentSchedulesByCallAgentIdByIntervalStartEpochByIntervalEndEpoch( $intCallAgentId, $intStartDateEpoch, $intEndDateEpoch, $objVoipDatabase );

				if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjCallAgentSchedules ) ) {
					reset( $arrobjCallAgentSchedules );
					$intStartScheduleId						= key( $arrobjCallAgentSchedules );
					$intEndScheduleId						= $intStartScheduleId;

					if( 1 == date( 'N', $intStartDateEpoch ) ) {
						$intStartIntervalSequence	= round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 );
					} else {
						$intStartIntervalSequence	= ( ( 48 * ( date( 'N', $intStartDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 ) );
					}

					if( 1 == date( 'N', $intEndDateEpoch ) ) {
						$intEndIntervalSequence = ( 0 == date( 'H.i', $intEndDateEpoch ) ) ? 336 : round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) );
					} else {
						$intEndIntervalSequence = ( ( 48 * ( date( 'N', $intEndDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) ) );
					}

					if( false == CCallAgentHelper::updateScheduleDetailsFromStartIntervalSequenceToEndIntervalSequenceForCallAgentWithCallAgentStatusTypeId( $intCallAgentId, $intEndScheduleId, $intStartIntervalSequence, $intEndIntervalSequence, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) ) {
						return false;
					}

					return true;
				} else {

					foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
						if( false == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intStartDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() && $intStartDateEpoch <= $objCallAgentSchedule->getIntervalEndEpoch() ) {
							$intStartScheduleId = $objCallAgentSchedule->getId();
							break;
						} elseif( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intStartDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() ) {
							$intStartScheduleId = $objCallAgentSchedule->getId();
							break;
						}
					}

					foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
						if( false == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intEndDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() && $intEndDateEpoch <= $objCallAgentSchedule->getIntervalEndEpoch() ) {
							$intEndScheduleId = $objCallAgentSchedule->getId();
							break;
						} elseif( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intEndDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() ) {
							$intEndScheduleId = $objCallAgentSchedule->getId();
							break;
						}
					}

					$arrobjCallAgentSchedules		= \Psi\Eos\Voip\CCallAgentSchedules::createService()->fetchCallAgentSchedulesByCallAgentIdByIntervalStartEpochByIntervalEndEpoch( $intCallAgentId, $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch(), $arrobjCallAgentSchedules[$intEndScheduleId]->getIntervalEndEpoch(), $objVoipDatabase );
					$intFirstScheduleId				= key( $arrobjCallAgentSchedules );
					end( $arrobjCallAgentSchedules );
					$intLastScheduleId = key( $arrobjCallAgentSchedules );

					foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
						if( $intFirstScheduleId == $objCallAgentSchedule->getId() ) {
							if( 1 == date( 'N', $intStartDateEpoch ) ) {
								$intStartIntervalSequence = round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 );
							} else {
								$intStartIntervalSequence = ( ( 48 * ( date( 'N', $intStartDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 ) );
							}

							$intEndIntervalSequence = 336;
						} elseif( $intLastScheduleId == $objCallAgentSchedule->getId() ) {
							$intStartIntervalSequence = 1;

							if( 1 == date( 'N', $intEndDateEpoch ) ) {
								$intEndIntervalSequence = ( 0 == date( 'H.i', $intEndDateEpoch ) ) ? 336 : round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) );
							} else {
								$intEndIntervalSequence = ( ( 48 * ( date( 'N', $intEndDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) ) );
							}
						} else {
							$intStartIntervalSequence	= 1;
							$intEndIntervalSequence		= 336;
						}

						$arrobjUpdateCallAgentScheduleDetails = \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->fetchCallAgentScheduleDetailsByCallAgentIdByCallAgentScheduleIdByIntervalSequence( $intCallAgentId, $objCallAgentSchedule->getId(), $intStartIntervalSequence, $intEndIntervalSequence, $objVoipDatabase );

						foreach( $arrobjUpdateCallAgentScheduleDetails as $objCallAgentScheduleDetail ) {
							if( true == in_array( $intCallAgentStatusTypeId, [ CCallAgentStatusType::APPROVED_TO, CCallAgentStatusType::CONDITIONAL_APPROVED_TO ] ) ) {
								if( true == valId( $objCallAgentScheduleDetail->getCallAgentStatusTypeId() ) ) {
									$objCallAgentScheduleDetail->setCallAgentStatusTypeId( $intCallAgentStatusTypeId );
								}
							} else {
								$objCallAgentScheduleDetail->setCallAgentStatusTypeId( $intCallAgentStatusTypeId );
							}

							if( false == $objCallAgentScheduleDetail->update( $intUserId, $objVoipDatabase ) ) {
								return false;
							}
						}
					}

					return true;
				}
			}
		} else {
			// here finding out call agent schedule id
			foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
				if( false == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intStartDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() && $intStartDateEpoch <= $objCallAgentSchedule->getIntervalEndEpoch() ) {
					$intStartScheduleId = $objCallAgentSchedule->getId();
					break;
				} elseif( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intStartDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() ) {
					$intStartScheduleId = $objCallAgentSchedule->getId();
					break;
				}
			}

			foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
				if( false == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intEndDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() && $intEndDateEpoch <= $objCallAgentSchedule->getIntervalEndEpoch() ) {
					$intEndScheduleId = $objCallAgentSchedule->getId();
					break;
				} elseif( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && $intEndDateEpoch >= $objCallAgentSchedule->getIntervalStartEpoch() ) {
					$intEndScheduleId = $objCallAgentSchedule->getId();
					break;
				}
			}

			if( false == valId( $intStartScheduleId ) || false == valId( $intEndScheduleId ) ) {
				return false;
			}

			if( true == is_null( $arrobjCallAgentSchedules[$intEndScheduleId]->getIntervalEndEpoch() ) ) {
				$arrobjCallAgentSchedules = \Psi\Eos\Voip\CCallAgentSchedules::createService()->fetchFutureCallAgentSchedulesByCallAgentIdByIsActiveByIntervalStartEpoch( $intCallAgentId, true, $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch(), $objVoipDatabase );
			} else {
				$arrobjCallAgentSchedules = \Psi\Eos\Voip\CCallAgentSchedules::createService()->fetchCallAgentSchedulesByCallAgentIdByIntervalStartEpochByIntervalEndEpoch( $intCallAgentId, $arrobjCallAgentSchedules[$intStartScheduleId]->getIntervalStartEpoch(), $arrobjCallAgentSchedules[$intEndScheduleId]->getIntervalEndEpoch(), $objVoipDatabase );
			}

			$intFirstScheduleId = key( $arrobjCallAgentSchedules );
			end( $arrobjCallAgentSchedules );
			$intLastScheduleId = key( $arrobjCallAgentSchedules );

			foreach( $arrobjCallAgentSchedules as $objCallAgentSchedule ) {
				if( $intFirstScheduleId == $objCallAgentSchedule->getId() ) {
					if( 1 == date( 'N', $intStartDateEpoch ) ) {
						$intStartIntervalSequence = round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 );
					} else {
						$intStartIntervalSequence = ( ( 48 * ( date( 'N', $intStartDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intStartDateEpoch ) ) + 1 ) );
					}

					$intEndIntervalSequence = 336;
				} elseif( $intLastScheduleId == $objCallAgentSchedule->getId() ) {
					$intStartIntervalSequence = 1;

					if( 1 == date( 'N', $intEndDateEpoch ) ) {
						$intEndIntervalSequence = ( 0 == date( 'H.i', $intEndDateEpoch ) ) ? 336 : round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) );
					} else {
						$intEndIntervalSequence = ( ( 48 * ( date( 'N', $intEndDateEpoch ) - 1 ) ) + round( ( 2 * date( 'H.i', $intEndDateEpoch ) ) ) );
					}

					if( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) ) {
						$objUpdateCallAgentSchedule = clone $objCallAgentSchedule;
						$objUpdateCallAgentSchedule->setIntervalEndTimestamp( date( 'Y-m-d H:i:s', strtotime( '+7 day', $objUpdateCallAgentSchedule->getIntervalStartEpoch() ) ) );
						$objUpdateCallAgentSchedule->setIntervalEndEpoch( strtotime( $objUpdateCallAgentSchedule->getIntervalEndTimestamp() ) );

						$objNewCallAgentSchedule = clone $objCallAgentSchedule;
						$objNewCallAgentSchedule->setId( NULL );
						$objNewCallAgentSchedule->setIntervalStartTimestamp( date( 'Y-m-d H:i:s', $objUpdateCallAgentSchedule->getIntervalEndEpoch() ) );
						$objNewCallAgentSchedule->setIntervalStartEpoch( strtotime( $objNewCallAgentSchedule->getIntervalStartTimestamp() ) );

						if( false == $objUpdateCallAgentSchedule->update( $intUserId, $objVoipDatabase ) || false == $objNewCallAgentSchedule->insert( $intUserId, $objVoipDatabase ) ) {
							return false;
						}
					}
				} else {
					$intStartIntervalSequence = 1;
					$intEndIntervalSequence = 336;
				}

				if( true == is_null( $objCallAgentSchedule->getIntervalEndEpoch() ) && true == valObj( $objNewCallAgentSchedule, 'CCallAgentSchedule' ) ) {
					$arrmixCallAgentScheduleDetails	= \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->fetchSimpleCallAgentScheduleDetailsByCallAgentScheduleId( $objCallAgentSchedule->getId(), $objVoipDatabase );
					$arrmixCallAgentScheduleDetails	= rekeyArray( 'interval_sequence', $arrmixCallAgentScheduleDetails );
					$strIntervalStartTimestamp		= date( 'Y-m-d', $objNewCallAgentSchedule->getIntervalStartEpoch() );
					$strSqlBulkInsertAppend			= NULL;

					for( $intIntervalSequence = 1; 336 >= $intIntervalSequence; $intIntervalSequence++ ) {
						$intIntervalDayOfWeek		 = intval( ( $intIntervalSequence + 47 ) / 48 );
						$intCallAgentStatusTypeId	 = ( true == valId( $arrmixCallAgentScheduleDetails[$intIntervalSequence]['call_agent_status_type_id'] ) ) ? $arrmixCallAgentScheduleDetails[$intIntervalSequence]['call_agent_status_type_id'] : 'NULL';
						$strSqlBulkInsertAppend		.= ' ( ' . ( int ) $objNewCallAgentSchedule->getId() . ', ' . ( int ) $objNewCallAgentSchedule->getCallAgentId() . ', ' . $intCallAgentStatusTypeId . ', \'' . date( 'Y-m-d H:i', strtotime( $strIntervalStartTimestamp ) ) . '\', ' . ( int ) $intIntervalDayOfWeek . ', ' . strtotime( $strIntervalStartTimestamp ) . ', ' . ( int ) $intIntervalSequence . ',1, ' . ( int ) $intUserId . ', NOW(), ' . ( int ) $intUserId . ', NOW() ) ';
						$strSqlBulkInsertAppend		.= ',';
						$strIntervalStartTimestamp	 = date( 'Y-m-d H:i', strtotime( '+30 minute', strtotime( $strIntervalStartTimestamp ) ) );
					}

					if( true == valStr( $strSqlBulkInsertAppend ) ) {
						if( ',' == \Psi\CStringService::singleton()->substr( $strSqlBulkInsertAppend, -1 ) ) $strSqlBulkInsertAppend = rtrim( $strSqlBulkInsertAppend, ',' );
						if( false == \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->buildCallAgentScheduleDetails( $strSqlBulkInsertAppend, $objVoipDatabase ) ) {
							return false;
						}
					}

				}

				if( false == CCallAgentHelper::updateScheduleDetailsFromStartIntervalSequenceToEndIntervalSequenceForCallAgentWithCallAgentStatusTypeId( $intCallAgentId, $objCallAgentSchedule->getId(), $intStartIntervalSequence, $intEndIntervalSequence, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) ) {
					return false;
				}
			}

			return true;
		}

		return true;
	}

	public static function insertNewScheduleFromStartDateToEndDateForCallAgentWithCallAgentStatusTypeId( $strStartDatetime, $strEndDatetime, $intCallAgentId, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase ) {
		if( 1 == date( 'N', strtotime( strtotime( $strStartDatetime ) ) ) ) {
			$strIntervalStartTimestamp = $strStartDatetime;
			$intIntervalStartEpoch = strtotime( $strStartDatetime );
		} else {
			$strIntervalStartTimestamp = date( 'Y-m-d', strtotime( 'last monday', strtotime( $strStartDatetime ) ) );
			$intIntervalStartEpoch = strtotime( $strIntervalStartTimestamp );
		}

		$intCurrentWeekStartEpoch = strtotime( 'Monday this week' );

		if( $intCurrentWeekStartEpoch < $intIntervalStartEpoch ) {
			$strIntervalStartTimestamp = date( 'Y-m-d', $intCurrentWeekStartEpoch );
			$intIntervalStartEpoch = strtotime( $strIntervalStartTimestamp );
		}

		$intWeeks		= round( ( ( ( strtotime( $strEndDatetime ) - $intIntervalStartEpoch ) / 86400 ) / 7 ), 0, PHP_ROUND_HALF_DOWN );
		$intLastWeek	= $intWeeks + 1;
		$intWeeks		+= 2;

		$arrobjNewCallAgentSchedule		= [];
		$arrobjNewCallAgentSchedule[0]	= new CCallAgentSchedule();
		$arrobjNewCallAgentSchedule[0]->setCallAgentId( $intCallAgentId );
		$arrobjNewCallAgentSchedule[0]->setIntervalStartTimestamp( date( 'Y-m-d H:i:s', $intIntervalStartEpoch ) );
		$arrobjNewCallAgentSchedule[0]->setIntervalStartEpoch( strtotime( $arrobjNewCallAgentSchedule[0]->getIntervalStartTimestamp() ) );
		$arrobjNewCallAgentSchedule[0]->setIntervalEndTimestamp( date( 'Y-m-d H:i:s', strtotime( '+7 day', $arrobjNewCallAgentSchedule[0]->getIntervalStartEpoch() ) ) );
		$arrobjNewCallAgentSchedule[0]->setIntervalEndEpoch( strtotime( $arrobjNewCallAgentSchedule[0]->getIntervalEndTimestamp() ) );
		$arrobjNewCallAgentSchedule[0]->setIsActive( true );

		for( $intWeekSequence = 1; $intWeeks > $intWeekSequence; $intWeekSequence++ ) {
			$arrobjNewCallAgentSchedule[$intWeekSequence] = clone $arrobjNewCallAgentSchedule[0];
			$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalStartTimestamp( date( 'Y-m-d H:i:s', strtotime( '+' . ( 7 * $intWeekSequence ) . ' day', strtotime( $arrobjNewCallAgentSchedule[0]->getIntervalStartTimestamp() ) ) ) );
			$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalStartEpoch( strtotime( $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartTimestamp() ) );

			if( $intLastWeek == $intWeekSequence ) {
				$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndTimestamp( NULL );
				$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndEpoch( NULL );
			} else {
				$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndTimestamp( date( 'Y-m-d H:i:s', strtotime( '+7 day', $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartEpoch() ) ) );
				$arrobjNewCallAgentSchedule[$intWeekSequence]->setIntervalEndEpoch( strtotime( $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalEndTimestamp() ) );
			}
		}

		for( $intWeekSequence = 0; $intWeeks > $intWeekSequence; $intWeekSequence++ ) {
			if( false == $arrobjNewCallAgentSchedule[$intWeekSequence]->insert( $intUserId, $objVoipDatabase ) ) {
				return false;
			}

			$strIntervalStartTimestamp	= date( 'Y-m-d', $arrobjNewCallAgentSchedule[$intWeekSequence]->getIntervalStartEpoch() );
			$strSqlBulkInsertAppend		= NULL;

			for( $intIntervalSequence = 1; 336 >= $intIntervalSequence; $intIntervalSequence++ ) {
				$intIntervalDayOfWeek		 = intval( ( $intIntervalSequence + 47 ) / 48 );
				$strSqlBulkInsertAppend		.= ' ( ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getId() . ', ' . ( int ) $arrobjNewCallAgentSchedule[$intWeekSequence]->getCallAgentId() . ', NULL, \'' . date( 'Y-m-d H:i', strtotime( $strIntervalStartTimestamp ) ) . '\', ' . ( int ) $intIntervalDayOfWeek . ', ' . strtotime( $strIntervalStartTimestamp ) . ', ' . ( int ) $intIntervalSequence . ',1, ' . ( int ) $intUserId . ', NOW(), ' . ( int ) $intUserId . ', NOW() ) ';
				$strSqlBulkInsertAppend		.= ',';
				$strIntervalStartTimestamp	 = date( 'Y-m-d H:i', strtotime( '+30 minute', strtotime( $strIntervalStartTimestamp ) ) );
			}

			if( true == valStr( $strSqlBulkInsertAppend ) ) {
				if( ',' == \Psi\CStringService::singleton()->substr( $strSqlBulkInsertAppend, -1 ) ) $strSqlBulkInsertAppend = rtrim( $strSqlBulkInsertAppend, ',' );
				if( false == \Psi\Eos\Voip\CCallAgentScheduleDetails::createService()->buildCallAgentScheduleDetails( $strSqlBulkInsertAppend, $objVoipDatabase ) ) {
					return false;
				}
			}
		}

		return CCallAgentHelper::updateScheduleFromStartDateToEndDateForCallAgentWithCallAgentStatusTypeId( $strStartDatetime, $strEndDatetime, $intCallAgentId, $intCallAgentStatusTypeId, $intUserId, $objVoipDatabase );
	}

}
?>