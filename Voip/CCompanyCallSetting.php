<?php

class CCompanyCallSetting extends CBaseCompanyCallSetting {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createCompanyCallServiceLevel() {
		$objCompanyCallServiceLevel = new CCompanyCallServiceLevel();
		$objCompanyCallServiceLevel->setCid( $this->getCid() );
		$objCompanyCallServiceLevel->setTargetedServiceLevel( $this->getAgreementServiceLevel() );
		return $objCompanyCallServiceLevel;
	}

}

?>