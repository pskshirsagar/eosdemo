<?php


class CCallFile extends CBaseCallFile {

	protected $m_strTempFileName;
	protected $m_strFileError;
	protected $m_strCopyFilePath;
	protected $m_strGreetingTypeName;
	protected $m_strTempGreetingsDirectory;
	protected $m_strTempMp3Name;
	protected $m_strCallFileGreetingFolderName;

	protected $m_intGreetingTypeId;
	protected $m_intClientId;
	protected $m_boolIsCallFileAvailable;

	const FREESWITCH_VOIP_MOUNTS					= '/srv/www/vhosts/VoipMounts/';
	const FREESWITCH_MOUNTS							= '/srv/www/vhosts/Mounts/';
	const FREESWITCH_CALL_FILES_NON_BACKUP_MOUNTS	= '/srv/www/vhosts/NonBackupMounts/';
	const FREESWITCH_CALL_TRANSCRIPTIONS			= '/srv/www/vhosts/NonBackupMounts/call_transcriptions/';
	const PLAY_DAYS_LIMIT = 180;

	/**
	 * Val Methods
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['greeting_type_name'] ) )		$this->setGreetingTypeName( $arrmixValues['greeting_type_name'] );
		if( true == isset( $arrmixValues['greeting_type_id'] ) )		$this->setGreetingTypeId( $arrmixValues['greeting_type_id'] );
		if( true == isset( $arrmixValues['client_id'] ) )				$this->setClientId( $arrmixValues['client_id'] );
		if( true == isset( $arrmixValues['is_call_file_available'] ) )	$this->setIsCallFileAvailable( $arrmixValues['is_call_file_available'] );

		return;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( false == isset( $this->m_strFileName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Invalid File Name.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Setter Methods
	 */

	public function setCopyFilePath( $strCopyFilePath ) {
		$this->m_strCopyFilePath = $strCopyFilePath;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setGreetingTypeName( $strGreetingTypeName ) {
		$this->m_strGreetingTypeName = $strGreetingTypeName;
	}

	public function setGreetingTypeId( $intGreetingTypeId ) {
		$this->m_intGreetingTypeId = $intGreetingTypeId;
	}

	public function setClientId( $intClientId ) {
		$this->m_intClientId = $intClientId;
	}

	public function setTempGreetingsDirectory( $strTempGreetingsDirectory ) {
		$this->m_strTempGreetingsDirectory = $strTempGreetingsDirectory;
	}

	public function setTempMp3Name( $strTempMp3Name ) {
		$this->m_strTempMp3Name = $strTempMp3Name;
	}

	public function setIsCallFileAvailable( $boolIsCallFileAvailable ) {
		$this->m_boolIsCallFileAvailable = $boolIsCallFileAvailable;
	}

	/**
	 * Getter Methods
	 */

	public function getFullFilePath( $boolFilePathOnly = false ) {
		$boolIsFreeswitchUserAgent = ( true == isset( $_SERVER['HTTP_USER_AGENT'] ) && false !== \Psi\CStringService::singleton()->strstr( $_SERVER['HTTP_USER_AGENT'], 'freeswitch' ) ) ? true : false;
		$strFullFilePath = self::FREESWITCH_VOIP_MOUNTS . $this->getFilePath();
		return $strFullFilePath . ( false == $boolFilePathOnly ? $this->getFileName() : '' );
	}

	public function getFullFilePathOrUrl() {

		if( false != filter_var( $this->getFilePath(), FILTER_VALIDATE_URL ) ) {
			return $this->getFilePath();
		}

		return $this->getFullFilePath();
	}

	public function buildCallFileUrl( $strCallId, $strBaseUrl ) {

		if( false != filter_var( $this->getFilePath(), FILTER_VALIDATE_URL ) ) {
			return $this->getFilePath();
		}

		return $strBaseUrl . '&call[id]=' . $strCallId . '&is_call_file_play=1';
	}

	public function getMediaFilePath() {
		return '/media_library/tmp/' . $this->getMp3FileName();
	}

	public function getMediaFullFilePath() {
		return PATH_MOUNTS . 'media_library/tmp/' . $this->getMp3FileName();
	}

	public function getMp3FileName() {
		return str_replace( 'wav', 'mp3', $this->getFileName() );
	}

	public function getCopyFilePath() {
		return $this->m_strCopyFilePath;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getGreetingTypeName() {
		return $this->m_strGreetingTypeName;
	}

	public function getGreetingTypeId() {
		return $this->m_intGreetingTypeId;
	}

	public function getClientId() {
		return $this->m_intClientId;
	}

	public function getTempGreetingsDirectory() {
		return $this->m_strTempGreetingsDirectory;
	}

	public function getTempMp3Name() {
		return $this->m_strTempMp3Name;
	}

	public function getIsCallFileAvailable() {
		return $this->m_boolIsCallFileAvailable;
	}

	/**
	 * Overrides Methods
	 */

	public function delete( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly = false ) {

		$intId = $this->getId();

		if( !isset( $intId ) ) {
			$intId = 0;
		}

		if( false == valId( $intId ) ) {
			trigger_error( __( 'Invalid File Remove Request: Id not present: {%d, id}', [ 'id' => ( int ) $intId ] ), E_USER_WARNING );
		}

		// Delete the object and the file
		if( parent::delete( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly ) ) {
			if( 0 < \Psi\CStringService::singleton()->strlen( $this->getFullFilePath() ) && true == file_exists( $strFullSizeFilePath = $this->getFullFilePath() ) ) {
				if( false == unlink( $strFullSizeFilePath ) ) {
					trigger_error( __( 'Invalid File Remove Request: Fullsize File could not be removed from the Call Files Library.' ), E_USER_WARNING );
				}
			}

			return true;
		}

		return false;
	}

	public function insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->validate( NULL ) ) {
			return false;
		}

		return parent::insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Build Methods
	 */

	public function buildGreetingCallFilePath( $intCid ) {

		if( true == valStr( $intCid ) ) {
			$this->m_strCallFileGreetingFolderName = 'greetings/' . $intCid . '/';
		} else {
			$this->m_strCallFileGreetingFolderName = 'call_queue_greetings/';
		}

		$strGreetingFilePath = PATH_VOIP_MOUNTS_CALL_FILES . $this->m_strCallFileGreetingFolderName . date( 'Y', time() ) . '/' . date( 'm', time() ) . '/' . date( 'd', time() ) . '/';

		return $strGreetingFilePath;
	}

	public function buildFileName( $strExtension = 'wav' ) {
		return $this->getId() . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
	}

	public function buildCallFileObject( $strTempFileName, $objAdminDatabase, $intCid = NULL ) {
		$strExtension = 'mp3';
		$this->setFileName( $this->buildFileName( $strExtension ) );
		$strMimeType	= '';
		$objFileExtension	= \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objAdminDatabase );
		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'This file type is not allowed.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$this->setTempFileName( $strTempFileName );
		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildGreetingCallFilePath( $intCid ) );

		if( false == is_dir( PATH_VOIP_MOUNTS . $this->getFilePath() ) ) {
			if( false == CFileIo::recursiveMakeDir( $this->getFullFilePath( $boolFilePathOnly = true ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Failed to create directory path.' ) );
				return false;
			}
		}

		return true;
	}

	public function buildFileData( $objAdminDatabase, $strCallFileType = 'call_queue', $intCid = NULL ) {

		switch( $strCallFileType ) {

			case 'greeting':
				$arrstrFileInfo = pathinfo( $_FILES['upload_greeting']['name'] );
				$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
				$strFileName	= $this->buildFileName( $strExtension );

				if( true == isset( $_FILES['upload_greeting']['tmp_name'] ) )	$this->setTempFileName( $_FILES['upload_greeting']['tmp_name'] );
				if( true == isset( $_FILES['upload_greeting']['error'] ) )		$this->setFileError( $_FILES['upload_greeting']['error'] );
				if( true == isset( $strFileName ) )								$this->setFileName( $strFileName );
				break;

			case 'recorded_greeting':
				$strExtension = 'mp3';
				$this->m_setTempGreetingsDirectory = PATH_MOUNTS . 'media_library/tmp/';
				$this->m_setTempMp3Name			 = $this->buildFileName( 'mp3' );
				$strTempOggName				 = time() . '.ogg';

				if( true == move_uploaded_file( $_FILES['upload_recorded_greeting']['tmp_name'], $this->m_setTempGreetingsDirectory . $strTempOggName ) ) {
					$strOsVersion = exec( 'lsb_release -r' );

					if( false !== strpos( $strOsVersion, '18.04' ) ) {
						$strCmdToConvertIntoMp3 = 'ffmpeg -i ' . $this->m_setTempGreetingsDirectory . $strTempOggName . ' ' . $this->m_setTempGreetingsDirectory . $this->m_setTempMp3Name;
					} else {
						$strCmdToConvertIntoMp3 = 'avconv -i ' . $this->m_setTempGreetingsDirectory . $strTempOggName . ' ' . $this->m_setTempGreetingsDirectory . $this->m_setTempMp3Name;
					}

					exec( $strCmdToConvertIntoMp3 );
					unlink( $this->m_setTempGreetingsDirectory . $strTempOggName );
				}

				$this->setFileName( $this->m_setTempMp3Name );
				break;

			case 'GREETING_TYPE_CALL_QUEUE_VOICEMAIL':
			case 'GREETING_TYPE_CALL_QUEUE_DURING_HOURS_GREETING':
			case 'GREETING_TYPE_CALL_QUEUE_AFTER_HOURS_GREETING':
			case 'GREETING_TYPE_CALL_QUEUE_ANNOUNCE_SOUND':
				$arrstrFileInfo = pathinfo( $_FILES['call_files']['name'][$strCallFileType] );
				$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
				$strFileName	= $this->buildFileName( $strExtension );

				if( true == isset( $_FILES['call_files']['tmp_name'][$strCallFileType] ) )	$this->setTempFileName( $_FILES['call_files']['tmp_name'][$strCallFileType] );
				if( true == isset( $_FILES['call_files']['error'][$strCallFileType] ) )		$this->setFileError( $_FILES['call_files']['error'][$strCallFileType] );
				if( true == isset( $strFileName ) )											$this->setFileName( $strFileName );
				break;

			case 'call_queue':
			default:
				$arrstrFileInfo = pathinfo( $_FILES['call_queue']['name']['call_file'] );
				$strExtension	= ( isset( $arrstrFileInfo['extension'] ) ) ? $arrstrFileInfo['extension'] : NULL;
				$strFileName	= $this->buildFileName( $strExtension );

				if( true == isset( $_FILES['call_queue']['tmp_name']['call_file'] ) )	$this->setTempFileName( $_FILES['call_queue']['tmp_name']['call_file'] );
				if( true == isset( $_FILES['call_queue']['error']['call_file'] ) )		$this->setFileError( $_FILES['call_queue']['error']['call_file'] );
				if( true == isset( $strFileName ) )										$this->setFileName( $strFileName );
				break;
		}

		$strMimeType	= '';
		$objFileExtension	= \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objAdminDatabase );

		if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_extention', __( 'This file type is not allowed.' ) ) );
			return false;
		}

		if( true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strMimeType = $objFileExtension->getMimeType();
		}

		$this->setFileExtensionId( $objFileExtension->getId() );
		$this->setFilePath( $this->buildGreetingCallFilePath( $intCid ) );
	}

	/**
	 * Misc Methods
	 */

	public function convertWavToMp3File() {

		if( false == file_exists( PATH_MOUNTS . 'media_library/tmp/' . $this->getMp3FileName() ) && CFileExtension::WAV == $this->getFileExtensionId() ) {
			$strOsVersion = exec( 'lsb_release -r' );

			if( false !== strpos( $strOsVersion, '18.04' ) ) {
				exec( 'ffmpeg -i ' . $this->getFullFilePath() . ' -id3v2_version 3 ' . PATH_MOUNTS . 'media_library/tmp/' . $this->getMp3FileName() );
			} else {
				exec( 'avconv -i ' . $this->getFullFilePath() . ' -id3v2_version 3 ' . PATH_MOUNTS . 'media_library/tmp/' . $this->getMp3FileName() );
			}

			return true;
		} elseif( CFileExtension::AUDIO_MP3 == $this->getFileExtensionId() ) {
			$this->copyFile( PATH_MOUNTS . 'media_library/tmp/' . $this->getFileName() );
		}

		return false;
	}

	public function uploadFile() {

		$boolIsValid = true;

		if( false == is_dir( $this->getFullFilePath( $boolFilePathOnly = true ) ) ) {
			if( false == CFileIo::recursiveMakeDir( $this->getFullFilePath( $boolFilePathOnly = true ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				$boolIsValid = false;
			}
		}

		if( false == move_uploaded_file( $this->getTempFileName(), $this->getFullFilePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No file was uploaded.' ) ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function uploadRecordFile() {
		$boolIsValid = true;

		if( false == is_dir( PATH_VOIP_MOUNTS . $this->getFilePath() ) ) {
			if( false == CFileIo::recursiveMakeDir( $this->getFullFilePath( $boolFilePathOnly = true ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
				$boolIsValid = false;
			}
		}

		if( false == CFileIo::copyFile( $this->m_setTempGreetingsDirectory . $this->m_setTempMp3Name, PATH_VOIP_MOUNTS . $this->getFilePath() . $this->m_setTempMp3Name ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function copyFile( $strCopyFilePath = NULL ) {

		if( true == valStr( $strCopyFilePath ) ) {
			$this->setCopyFilePath( $strCopyFilePath );
		}

		$boolIsValid = true;

		if( false == file_exists( $this->getFullFilePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No source file exists.' ) ) );
			return false;
		}

		if( false == CFileIo::copyFile( $this->getFullFilePath(), $this->getCopyFilePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function copyTempFile( $strTempFilePath = NULL ) {

		if( false == valStr( $strTempFilePath ) ) return false;

		$boolIsValid = true;

		if( false == file_exists( $strTempFilePath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'No source file exists.' ) ) );
			return false;
		}

		if( false == CFileIo::copyFile( $strTempFilePath, $this->getFullFilePath() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File could not copied.' ) ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function getRemoteCallFileAccessUrl() {
		return CCallHelper::getRemoteCallFileAccessUrl() . '&call_file_id=' . $this->getId();
	}

}
?>