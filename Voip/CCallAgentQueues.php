<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentQueues
 * Do not add any new functions to this class.
 */

class CCallAgentQueues extends CBaseCallAgentQueues {

	public static function fetchCallAgentQueuesByCallQueueIdByCallAgentIds( $intCallQueueId, $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						caq.position AS call_agent_queue_position,
						cq.name AS call_queue_name,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agent_queues AS caq
						JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						caq.call_queue_id = ' . ( int ) $intCallQueueId . '
						AND caq.call_agent_id IN ( ' . implode( ', ', $arrintCallAgentIds ) . ' )
					GROUP BY
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						call_agent_state_type_name,
						call_agent_status_type_name,
						call_agent_queue_position,
						ca.domain,
						cq.domain,
						cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentQueuesByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT
						caq.*,
						ca.full_name AS call_agent_name,
						cq.name AS call_queue_name,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						1 AS is_call_agent_queue
					FROM
						call_agent_queues caq
						JOIN call_agents ca ON ( ca.id = caq.call_agent_id )
						JOIN call_queues cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						caq.call_queue_id = ' . ( int ) $intCallQueueId;

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentQueuesByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						caq.*,
						ca.full_name AS call_agent_name,
						cq.name AS call_queue_name,
						cq.call_queue_type_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						1 AS is_call_agent_queue
					FROM
						call_agent_queues caq
						JOIN call_agents ca ON ( ca.id = caq.call_agent_id )
						JOIN call_queues cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						caq.call_agent_id = ' . ( int ) $intCallAgentId . '
					ORDER BY cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentQueuesByCallAgentIds( $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						caq.*,
						ca.id AS call_agent_id,
						ca.full_name AS call_agent_name,
						cq.name AS call_queue_name
					FROM
						call_agent_queues AS caq
						JOIN call_agents ca ON ( ca.id = caq.call_agent_id )
						JOIN call_queues cq ON ( cq.id = caq.call_queue_id )
					WHERE
						caq.call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveCallAgentQueueById( $intCallAgentQueueId, $objVoipDatabase ) {

		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						cq.name AS call_queue_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agent_queues AS caq
						JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						caq.id = ' . ( int ) $intCallAgentQueueId . '
					GROUP BY
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain,
						cq.domain,
						cq.name
					ORDER BY cq.name';

		return self::fetchCallAgentQueue( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveCallAgentQueues( $objVoipDatabase ) {
		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						ca.call_agent_state_type_id,
						ca.call_agent_status_type_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						cq.name AS call_queue_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agent_queues AS caq
						JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					GROUP BY
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						ca.call_agent_state_type_id,
						ca.call_agent_status_type_id,
						cq.queue_phone_extension_id,
						ca.domain,
						cq.domain,
						cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentQueuesByCallQueueIdByCallAgentQueueIds( $intCallQueueId, $arrintCallAgentQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentQueueIds ) ) return NULL;

		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						cq.name AS call_queue_name,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agent_queues AS caq
						JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						cq.id = ' . ( int ) $intCallQueueId . '
						AND caq.id IN ( ' . implode( ',', $arrintCallAgentQueueIds ) . ' )
					GROUP BY
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						call_agent_state_type_name,
						call_agent_status_type_name,
						ca.domain,
						cq.domain,
						cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchPriorityCallAgentQueuesByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) ) return;

		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						cq.name AS call_queue_name,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						CASE
						 WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >=
							DATE_TRUNC( \'day\', NOW()) THEN caq.override_priority
						 WHEN MIN(csq.priority) IS NOT NULL THEN MIN(csq.priority)
						 ELSE 5
						END AS priority
				FROM call_agent_queues AS caq
					 JOIN call_queues AS cq ON (cq.id = caq.call_queue_id)
					 JOIN call_agents AS ca ON (ca.id = caq.call_agent_id)
					 JOIN call_agent_state_types castt ON (castt.id = ca.call_agent_state_type_id)
					 JOIN call_agent_status_types casts ON (casts.id = ca.call_agent_status_type_id)
					 LEFT JOIN call_agent_skills cas ON (cas.call_agent_id = ca.id)
					 LEFT JOIN call_skill_queues csq ON (csq.call_queue_id = cq.id AND
						csq.call_skill_id = cas.call_skill_id)
				WHERE
					ca.id = ' . ( int ) $intCallAgentId . '
				GROUP BY caq.id,
						 caq.call_agent_id,
						 caq.call_queue_id,
						 caq.state,
						 caq.level,
						 caq.position,
						 caq.override_priority,
						 caq.override_expiration,
						 caq.updated_by,
						 caq.updated_on,
						 caq.created_by,
						 caq.created_on,
						 ca.agent_phone_extension_id,
						 cq.queue_phone_extension_id,
						 call_agent_state_type_name,
						 call_agent_status_type_name,
						 ca.domain,
						 cq.domain,
						 cq.name
				ORDER BY cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchOverridePriortyCallAgentQueuesByCallAgentIdsByCallQueueIds( $arrintCallAgentIds, $arrintCallQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valArr( $arrintCallQueueIds ) ) return;

		$strSql = 'SELECT
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain AS call_agent_domain,
						cq.domain AS call_queue_domain,
						cq.name AS call_queue_name,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agent_queues AS caq
						JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						JOIN call_agents AS ca ON ( ca.id = caq.call_agent_id )
						LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
						LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						caq.call_agent_id IN ( ' . implode( ', ', $arrintCallAgentIds ) . ' )
						AND caq.call_queue_id IN ( ' . implode( ', ', $arrintCallQueueIds ) . ' )
						AND caq.override_expiration IS NOT NULL
					GROUP BY
						caq.id,
						caq.call_agent_id,
						caq.call_queue_id,
						caq.state,
						caq.level,
						caq.position,
						caq.override_priority,
						caq.override_expiration,
						caq.updated_by,
						caq.updated_on,
						caq.created_by,
						caq.created_on,
						ca.agent_phone_extension_id,
						cq.queue_phone_extension_id,
						ca.domain,
						cq.domain,
						cq.name
					ORDER BY cq.name';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentQueueByCallAgentIdByCallQueueId( $intCallAgentId, $intCallQueueId, $objVoipDatabase ) {
		return self::fetchCallAgentQueue( 'SELECT *FROM call_agent_queues WHERE call_agent_id = ' . ( int ) $intCallAgentId . ' AND call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchCallAgentQueueByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		return self::fetchCallAgentQueue( 'SELECT *FROM call_agent_queues WHERE call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchCallAgentQueuesByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						call_agent_queues 
					WHERE 
						call_agent_id IN ( 
							SELECT 
								DISTINCT id 
							FROM 
								call_agents 
							WHERE 
								employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) 
						) 
					ORDER BY call_agent_id DESC';

		return self::fetchCallAgentQueues( $strSql, $objVoipDatabase );
	}

}
?>