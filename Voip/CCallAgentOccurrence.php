<?php

class CCallAgentOccurrence extends CBaseCallAgentOccurrence {

	protected $m_strFullName;
	protected $m_strOccurrenceTypeName;
	protected $m_strDisputeReviewedName;
	protected $m_intEmployeeId;
	protected $m_intDaysSinceCreatedOn;
	protected $m_boolIsAllowDispute;

	const ALLOW_DISPUTE_NUM_DAYS		= 21;
	const MAX_OCCURRENCE_CREDIT_LIMIT	= 10;
	const QUARTERLY_CREDIT_VALUE		= 3;

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['full_name'] ) )				$this->setFullName( $arrmixValues['full_name'] );
		if( true == isset( $arrmixValues['occurrence_type_name'] ) )	$this->setOccurrenceTypeName( $arrmixValues['occurrence_type_name'] );
		if( true == isset( $arrmixValues['dispute_reviewed_name'] ) )	$this->setDisputeReviewedName( $arrmixValues['dispute_reviewed_name'] );
		if( true == isset( $arrmixValues['employee_id'] ) )				$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['days_since_created_on'] ) )	$this->setDaysSinceCreatedOn( $arrmixValues['days_since_created_on'] );
		if( true == isset( $arrmixValues['is_allow_dispute'] ) )		$this->setIsAllowDispute( $arrmixValues['is_allow_dispute'] );
		return true;
	}

	public function setFullName( $strFullName ) {
		$this->m_strFullName = $strFullName;
	}

	public function setOccurrenceTypeName( $strOccurrenceTypeName ) {
		$this->m_strOccurrenceTypeName = $strOccurrenceTypeName;
	}

	public function setDisputeReviewedName( $strDisputeReviewedName ) {
		$this->m_strDisputeReviewedName = $strDisputeReviewedName;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setDaysSinceCreatedOn( $intDaysSinceCreatedOn ) {
		$this->m_intDaysSinceCreatedOn = $intDaysSinceCreatedOn;
	}

	public function setIsAllowDispute( $boolIsAllowDispute ) {
		$this->m_boolIsAllowDispute = $boolIsAllowDispute;
	}

	/**
	* Get Functions
	*
	*/

	public function getFullName() {
		return $this->m_strFullName;
	}

	public function getOccurrenceTypeName() {
		return $this->m_strOccurrenceTypeName;
	}

	public function getDisputeReviewedName() {
		return $this->m_strDisputeReviewedName;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getDaysSinceCreatedOn() {
		return $this->m_intDaysSinceCreatedOn;
	}

	public function getIsAllowDispute() {
		if( false == is_bool( $this->m_boolIsAllowDispute ) ) {
			return ( CCallAgentOccurrence::ALLOW_DISPUTE_NUM_DAYS >= $this->getDaysSinceCreatedOn() && COccurrenceType::CREDIT != $this->getOccurrenceTypeId() );
		}

		return $this->m_boolIsAllowDispute;
	}

	/**
	* Validate Functions
	*
	*/

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOccurrenceTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getOccurrenceTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_type_id', 'Occurrence type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOccurrenceDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getOccurrenceDate() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_date', 'Occurrence date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOccurrenceAmount() {
		$boolIsValid = true;

		if( true == is_numeric( $this->getOccurrenceAmount() ) && 0 >= $this->getOccurrenceAmount() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'occurrence_amount', 'Occurrence amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDisputeReason() {
		$boolIsValid = true;

		if( false == valStr( $this->getDisputeReason() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Dispute reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'insert_call_agent_occurrence':
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valOccurrenceDate();
				$boolIsValid &= $this->valOccurrenceTypeId();
				$boolIsValid &= $this->valOccurrenceAmount();
				break;

			case 'dispute_reason':
				$boolIsValid &= $this->valDisputeReason();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>