<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerPhoneNumbers
 * Do not add any new functions to this class.
 */

class CCallerPhoneNumbers extends CBaseCallerPhoneNumbers {

	public static function fetchCallerPhoneNumberByPhoneNumber( $strPhoneNumber, $objDatabase ) {
		if( false == valStr( $strPhoneNumber ) ) return;
		return self::fetchCallerPhoneNumber( 'SELECT * FROM caller_phone_numbers WHERE caller_phone_number = \'' . $strPhoneNumber . '\' LIMIT 1 ;', $objDatabase );
	}

}
?>