<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallEventTypes
 * Do not add any new functions to this class.
 */

class CCallEventTypes extends CBaseCallEventTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchCallEventTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallEventType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallEventType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallEventType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedCallEventTypes( $objVoipDatabase ) {
		return self::fetchCallEventTypes( 'SELECT * FROM call_event_types WHERE is_published = TRUE', $objVoipDatabase );
	}

}
?>