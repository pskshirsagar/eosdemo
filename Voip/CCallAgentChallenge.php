<?php

class CCallAgentChallenge extends CBaseCallAgentChallenge {

	protected $m_strCallAgentAchievementTypeName;
	protected $m_intRewardPointTypeId;
	protected $m_intCallAgentChallengeTypeId;
	protected $m_strCallAgentChallengeTypeName;
	protected $m_boolIsDelete;

	/**
	* Set Functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['call_agent_achievement_type_name'] ) )	$this->setCallAgentAchievementTypeName( $arrmixValues['call_agent_achievement_type_name'] );
		if( isset( $arrmixValues['reward_point_type_id'] ) )				$this->setRewardPointTypeId( $arrmixValues['reward_point_type_id'] );
		if( isset( $arrmixValues['call_agent_challenge_type_id'] ) )		$this->setCallAgentChallengeTypeId( $arrmixValues['call_agent_challenge_type_id'] );
		if( isset( $arrmixValues['call_agent_challenge_type_name'] ) )		$this->setCallAgentChallengeTypeName( $arrmixValues['call_agent_challenge_type_name'] );
		if( isset( $arrmixValues['is_delete'] ) )							$this->setIsDelete( $arrmixValues['is_delete'] );
		return true;
	}

	public function setCallAgentAchievementTypeName( $strCallAgentAchievementTypeName ) {
		$this->m_strCallAgentAchievementTypeName = $strCallAgentAchievementTypeName;
	}

	public function setRewardPointTypeId( $intRewardPointTypeId ) {
		$this->m_intRewardPointTypeId = $intRewardPointTypeId;
	}

	public function setCallAgentChallengeTypeId( $intCallAgentChallengeTypeId ) {
		$this->m_intCallAgentChallengeTypeId = $intCallAgentChallengeTypeId;
	}

	public function setCallAgentChallengeTypeName( $strCallAgentChallengeTypeName ) {
		$this->m_strCallAgentChallengeTypeName = $strCallAgentChallengeTypeName;
	}

	public function setIsDelete( $boolIsDelete ) {
		$this->m_boolIsDelete = $boolIsDelete;
	}

	/**
	* Get Functions
	*
	*/

	public function getCallAgentAchievementTypeName() {
		return $this->m_strCallAgentAchievementTypeName;
	}

	public function getRewardPointTypeId() {
		return $this->m_intRewardPointTypeId;
	}

	public function getCallAgentChallengeTypeId() {
		return $this->m_intCallAgentChallengeTypeId;
	}

	public function getCallAgentChallengeTypeName() {
		return $this->m_strCallAgentChallengeTypeName;
	}

	public function getIsDelete() {
		return $this->m_boolIsDelete;
	}

	/**
	* Validate Functions
	*
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentAchievementTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentAchievementTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Achievement is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTeamId() {
		$boolIsValid = true;

		if( false == valId( $this->getTeamId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Team is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReferenceValue() {
		$boolIsValid = true;

		if( false == valId( $this->getReferenceValue() ) && true == in_array( $this->getCallAgentAchievementTypeId(), [ CCallAgentAchievementType::GRADED_CALLS, CCallAgentAchievementType::HOURS_WORKED ] ) && false == $this->getSuperlativeFlag() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_value', 'Reference value is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMonthOfChallenge() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearOfChallenge() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSuperlative() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notes', 'Tooltip Note is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case 'validate_team_challenge':
				$boolIsValid &= $this->valTeamId();
				$boolIsValid &= $this->valCallAgentAchievementTypeId();
				$boolIsValid &= $this->valNotes();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_company_challenge':
				$boolIsValid &= $this->valCallAgentAchievementTypeId();
				$boolIsValid &= $this->valNotes();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>