<?php

class CStoreProductTag extends CBaseStoreProductTag {

	protected $m_strName;
	protected $m_strDescription;
	protected $m_strImagePath;
	protected $m_strImageSource;
	protected $m_strTempFileName;
	protected $m_intRequiredCoins;
	protected $m_boolIsCallAgentGoal;

	/**
	* Validation functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoreProductId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'product_id', 'Invalid store product id.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Invalid call agent id.' ) );
		}

		return $boolIsValid;
	}

	public function valTags() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleasedBy() {
		$boolIsValid = true;

		if( false == valId( $this->getReleasedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'released_by', 'Invalid released by.' ) );
		}

		return $boolIsValid;
	}

	public function valReleasedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( trim( $strAction ) ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valStoreProductId();
				$boolIsValid &= $this->valCallAgentId();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_store_product_tag_update':
				$boolIsValid &= $this->valReleasedBy();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	* Setter getter functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name'] ) )				$this->setName( $arrmixValues['name'] );
		if( true == isset( $arrmixValues['description'] ) )			$this->setDescription( $arrmixValues['description'] );
		if( true == isset( $arrmixValues['image_path'] ) )			$this->setImagePath( $arrmixValues['image_path'] );
		if( true == isset( $arrmixValues['required_coins'] ) )		$this->setRequiredCoins( $arrmixValues['required_coins'] );
		if( true == isset( $arrmixValues['is_call_agent_goal'] ) )	$this->setIsCallAgentGoal( $arrmixValues['is_call_agent_goal'] );
		return true;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function setImagePath( $strImagePath ) {
		$this->m_strImagePath = $strImagePath;
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function setImageSource( $strImageSource ) {
		$this->m_strImageSource = $strImageSource;
	}

	public function getImageSource() {
		return $this->m_strImageSource;
	}

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function setRequiredCoins( $intRequiredCoins ) {
		$this->m_intRequiredCoins = $intRequiredCoins;
	}

	public function getRequiredCoins() {
		return $this->m_intRequiredCoins;
	}

}
?>