<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisScorecards
 * Do not add any new functions to this class.
 */

class CCallAnalysisScorecards extends CBaseCallAnalysisScorecards {

	public static function fetchPublishedCallAnalysisScorecardsByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;
		return self::fetchCallAnalysisScorecards( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE is_published = true AND property_id = %d AND cid = %d AND deleted_on IS NULL AND deleted_by IS NULL ORDER BY order_num', ( int ) $intPropertyId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchPublishedCallAnalysisScorecardByIdByPropertyIdByCid( $intCallAnalysisScorecardId, $intPropertyId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisScorecard( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE is_published = true AND id = %d AND property_id = %d AND cid = %d ORDER BY order_num', ( int ) $intCallAnalysisScorecardId, ( int ) $intPropertyId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchMaxOrderNumCallAnalysisScorecardByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		$strSql			= sprintf( 'SELECT MAX( order_num ) FROM call_analysis_scorecards WHERE property_id = %d AND cid = %d', ( int ) $intPropertyId, ( int ) $intCid );
		$arrintResponse	= fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrintResponse[0]['max'] ) ) ? $arrintResponse[0]['max'] : 1;
	}

	public static function fetchPublishedCallAnalysisScorecardsByIdsByPropertyIdsByCid( $arrintCallAnalysisScorecardIds, $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT * FROM
						call_analysis_scorecards
					WHERE
						property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cid =' . ( int ) $intCid . '
						AND is_active = true
						AND is_published = true';

		if( false == is_null( $arrintCallAnalysisScorecardIds ) ) {
			$strSql .= ' AND id IN ( ' . implode( ',', ( array ) $arrintCallAnalysisScorecardIds ) . ' )';
		}

		$strSql .= ' ORDER BY order_num';

		return self::fetchCallAnalysisScorecards( sprintf( $strSql ), $objVoipDatabase );
	}

	public static function fetchCustomCallAnalysisScorecardsByCid( $intCid, $objVoipDatabase, $boolIsNewScorecards = false ) {
		if( false == valId( $intCid ) )return NULL;

		$strSql = 'SELECT 
						* 
					FROM 
						call_analysis_scorecards 
					WHERE 
						property_id IS NULL
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';
		if( false == $boolIsNewScorecards ) {
			$strSql .= ' AND ( details->\'is_new_scorecard\' = \'false\' OR details->\'is_new_scorecard\' IS NULL ) ';
		}
		$strSql .= ' ORDER BY order_num ASC';

		return self::fetchCallAnalysisScorecards( $strSql, $objVoipDatabase );
	}

	public static function fetchPublishedCallAnalysisScorecardsByCid( $intCid, $objVoipDatabase ) {
		if( false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisScorecards( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE is_published = true AND property_id IS NULL AND cid = %d AND deleted_on IS NULL AND deleted_by IS NULL ORDER BY order_num', ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchPublishedCallAnalysisScorecardsByCidByPropertyGroupIds( $intCid, $arrintPropertyGroupIds, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;

		$strSql = 'SELECT
						cas.*
					FROM
						call_analysis_scorecards cas
						JOIN call_analysis_scorecard_property_groups caspg ON ( caspg.call_analysis_scorecard_id = cas.id )
					WHERE
						cas.is_published = true AND
						cas.property_id IS NULL AND
						cas.cid = ' . ( int ) $intCid . ' AND
						caspg.property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ' ) AND
						cas.deleted_on IS NULL AND
						cas.deleted_by IS NULL
					ORDER BY
						caspg.created_on DESC
					LIMIT 1';

		return self::fetchCallAnalysisScorecard( $strSql, $objVoipDatabase );
	}

	public static function fetchMaxOrderNumCallAnalysisScorecardByCid( $intCid, $objVoipDatabase ) {
		$strSql			= sprintf( 'SELECT MAX( order_num ) FROM call_analysis_scorecards WHERE property_id IS NULL AND cid = %d', ( int ) $intCid );
		$arrintResponse	= fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrintResponse[0]['max'] ) ) ? $arrintResponse[0]['max'] : 1;
	}

	public static function fetchCallAnalysisScorecardByIdByCid( $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisScorecardId ) || false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisScorecard( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE property_id IS NULL AND id = %d AND cid = %d ORDER BY order_num', ( int ) $intCallAnalysisScorecardId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchDefaultCallAnalysisScorecardByCid( $intCid, $objVoipDatabase ) {
		if( false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisScorecard( sprintf( 'SELECT * FROM call_analysis_scorecards WHERE is_active = true AND property_id IS NULL AND cid = %d',  ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchCallAnalysisScorecardByCidByCallId( $intCid, $intCallId, $objVoipDatabase ) {
		if( false == valId( $intCallId ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						cas.*
					FROM
						call_analysis_scorecards cas
						JOIN call_analysis_categories cac ON ( cac.call_analysis_scorecard_id = cas.id )
						JOIN call_analysis_questions caq ON ( caq.call_analysis_category_id = cac.id )
						JOIN call_analysis_results car ON ( car.call_analysis_question_id = caq.id )
					WHERE
						car.call_id = ' . ( int ) $intCallId . '
						AND cas.cid = ' . ( int ) $intCid . '
					LIMIT 1';

		return self::fetchCallAnalysisScorecard( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisScorecardByCallAnalysisQuestionIdByCid( $intCallAnalysisQuestionId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisQuestionId ) || false == valId( $intCid ) ) return NULL;
		$strSql = 'SELECT
						cas.*
					FROM
						call_analysis_scorecards cas
						JOIN call_analysis_categories cac ON ( cac.call_analysis_scorecard_id = cas.id )
						JOIN call_analysis_questions caq ON ( caq.call_analysis_category_id = cac.id )
					WHERE
						caq.id = ' . ( int ) $intCallAnalysisQuestionId . '
						AND cas.cid = ' . ( int ) $intCid;

		return self::fetchCallAnalysisScorecard( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomCallAnalysisDetailsScorecardsByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return false;

		$strSql = 'SELECT
						cpg.id,
						cpg.property_group_id AS property_id
					FROM
					    call_analysis_scorecard_property_groups cpg
					    LEFT JOIN call_analysis_scorecards cas ON ( cpg.cid = cas.cid AND cpg.call_analysis_scorecard_id = cas.id )
					WHERE
						cpg.cid = ' . ( int ) $intCid . '
						AND cpg.property_group_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')
						AND cas.deleted_by IS NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>