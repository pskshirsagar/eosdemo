<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCategories
 * Do not add any new functions to this class.
 */

class CCallAnalysisCategories extends CBaseCallAnalysisCategories {

	public static function fetchActiveCallAnalysisCategoriesByCid( $intCid, $objVoipDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE deleted_on IS NULL AND cid = %d ORDER BY order_num', ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoryByCidById( $intCid, $intId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_analysis_categories WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intId;
		return self::fetchCallAnalysisCategory( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoriesByScorecardIdByPropertyIdByCid( $intCallAnalysisScorecardId, $intPropertyId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE deleted_on IS NULL AND call_analysis_scorecard_id = %d AND property_id = %d AND cid = %d ORDER BY order_num', ( int ) $intCallAnalysisScorecardId, ( int ) $intPropertyId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchMaxOrderNumCallAnalysisCategoiesByCallAnalysisScorecardIdByCid( $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {
		$strSql	= 'SELECT
						MAX( order_num )
					FROM
						call_analysis_categories
					WHERE
						call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . '
						AND cid = ' . ( int ) $intCid;

		$arrintResponse	= fetchData( $strSql, $objVoipDatabase );

		return ( true == isset( $arrintResponse[0]['max'] ) ) ? $arrintResponse[0]['max'] : 1;
	}

	public static function fetchActiveCallAnalysisCategoriesByCidByPropertyIdByCallAnalysisScorecardId( $intCid, $intPropertyId, $intCallAnalysisScorecardId, $objVoipDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE deleted_on IS NULL AND cid = %d AND property_id = %d AND call_analysis_scorecard_id = %d ORDER BY order_num', ( int ) $intCid, ( int ) $intPropertyId, ( int ) $intCallAnalysisScorecardId ), $objVoipDatabase );
	}

	public static function fetchActiveCallAnalysisCategoriesByScorecardIdByCid( $intScorecardId, $intCid, $objVoipDatabase ) {
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE deleted_on IS NULL AND call_analysis_scorecard_id = %d AND cid = %d ORDER BY order_num', ( int ) $intScorecardId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchCountCallAnalysisCategoryByNameByCallAnalysisScorecardIdByCid( $strName, $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT
						COUNT( id )
					FROM
						call_analysis_categories
					WHERE
						name = \'' . addslashes( $strName ) . '\'
						AND call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . '
						AND cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL AND deleted_on IS NULL LIMIT 1';

		return CCallAnalysisCategories::fetchColumn( $strSql, 'count', $objVoipDatabase );
	}

	// New Functions

	public static function fetchCallAnalysisCategoriesByCallAnalysisScorecardIdByCid( $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisScorecardId ) || false == valId( $intCid ) )return NULL;
		return self::fetchCallAnalysisCategories( sprintf( 'SELECT * FROM call_analysis_categories WHERE deleted_on IS NULL AND call_analysis_scorecard_id = %d AND cid = %d ORDER BY order_num', ( int ) $intCallAnalysisScorecardId, ( int ) $intCid ), $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoriesByIdsByCid( $arrintIds, $intCid, $objVoipDatabase ) {
		if( false == valArr( $arrintIds ) || false == valId( $intCid ) )return NULL;
		$strSql = 'SELECT * FROM call_analysis_categories WHERE cid = ' . ( int ) $intCid . ' AND id IN ( ' . implode( ',', $arrintIds ) . ' )';
		return self::fetchCallAnalysisCategories( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoriesDetailsByCallAnalysisScorecardIdByCid( $intCallAnalysisScorecardId, $intCid, $objVoipDatabase ) {

		$strSql = 'SELECT 
						cac.*,
						json_agg( row_to_json( questions.* ) ORDER BY questions.order_num ASC ) AS call_analysis_questions
					FROM call_analysis_categories cac
						JOIN ( 
							SELECT 
								caq.*,
								json_agg( row_to_json( caa.* ) ORDER BY caa.order_num ASC ) AS call_analysis_answers
							FROM
								call_analysis_answers caa
    							JOIN call_analysis_questions caq ON caq.id = caa.call_analysis_question_id
    						WHERE
    							caq.deleted_on IS NULL
    							AND caa.deleted_on IS NULL
							GROUP BY
								caq.id
							ORDER BY 
								caq.order_num ASC
						) AS questions ON questions.call_analysis_category_id = cac.id AND cac.cid = questions.cid
					WHERE
						cac.cid = ' . ( int ) $intCid . '
						AND cac.call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . '
						AND cac.deleted_on IS NULL
					GROUP BY
						cac.id
					ORDER BY 
						cac.order_num ASC';

		return self::fetchCallAnalysisCategories( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisCategoriesDetailsByCallIdByCidByCallAnalysisScorecardId( $intCallId, $intCid, $intCallAnalysisScorecardId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						cac.*,
						json_agg( row_to_json( questions.* ) ORDER BY questions.order_num ASC ) AS call_analysis_questions
					FROM 
						call_analysis_results car
						JOIN ( 
							SELECT 
								caq.*,
								json_agg( row_to_json( caa.* ) ORDER BY caa.order_num ASC ) AS call_analysis_answers
							FROM
								call_analysis_answers caa
    							JOIN call_analysis_questions caq ON caq.id = caa.call_analysis_question_id
    						WHERE
    							caq.call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . '
    						GROUP BY
								caq.id
							ORDER BY 
								caq.order_num ASC
						) AS questions ON questions.id = car.call_analysis_question_id AND car.cid = questions.cid
						JOIN call_analysis_categories cac ON cac.id = questions.call_analysis_category_id
					WHERE 
						car.cid = ' . ( int ) $intCid . '
						AND car.call_id = ' . ( int ) $intCallId . '
					GROUP BY
						cac.id
					ORDER BY 
						cac.order_num ASC';

		return self::fetchCallAnalysisCategories( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallAnalysisCategories( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_analysis_categories';
		return self::fetchCallAnalysisCategories( $strSql, $objVoipDatabase );
	}

}
?>