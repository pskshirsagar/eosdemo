<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallServiceLevels
 * Do not add any new functions to this class.
 */

class CCallServiceLevels extends CBaseCallServiceLevels {

	public static function fetchCallServiceLevelByAssignedServiceLevel( $intAssignedServiceLevel, $objVoipDatabase ) {
		return self::fetchCallServiceLevel( 'SELECT *FROM call_service_levels WHERE assigned_service_level = ' . ( int ) $intAssignedServiceLevel . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchCallServiceLevelByEvaluatedServiceLevel( $intEvaluatedServiceLevel, $objVoipDatabase ) {
		return self::fetchCallServiceLevel( 'SELECT *FROM call_service_levels WHERE evaluated_service_level = ' . ( int ) $intEvaluatedServiceLevel . ' LIMIT 1', $objVoipDatabase );
	}

}

?>