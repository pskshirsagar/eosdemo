<?php

class CCallAnalysisServiceStat extends CBaseCallAnalysisServiceStat {

	protected $m_strCompanyName;
	protected $m_strPropertyName;

	/**
	* set functions
	*
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) )						$this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['property_name'] ) )						$this->setPropertyName( $arrmixValues['property_name'] );
		return true;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	/**
	* get functions
	*
	*/

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valClientId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'NULL', 'Please select client. ' ) );
		}

		return $boolIsValid;
	}

	public function valAssignedServiceLevel() {
		$boolIsValid = true;
		if( false == preg_replace( '/[^0-9]/', '', $this->getAssignedServiceLevel() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Please enter valid assigned service level.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valClientId();
				$boolIsValid &= $this->valAssignedServiceLevel();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>