<?php

class CCallAnalysisStatusType extends CBaseCallAnalysisStatusType {

	const UNEVALUATED				= 1;
	const EVALUATED					= 2;
	const ANALYZED					= 3;
	const NON_LEAD_CALL				= 5;
	const DISQUALIFIED_LEAD_CALL	= 6;
	const WARM_LEAD					= 7;
	const EARLY_TERMINATION			= 8;
	const FLAGGED_FOR_ANALYSIS		= 9;
	const MAX_CALLS_EVALUATED		= 10;
	const VOICEMAIL					= 11;

	public static $c_arrstrBasicCallAnalysisStatusTypes = [
		self::UNEVALUATED	=> 'Unevaluated',
		self::EVALUATED		=> 'Evaluated',
		self::ANALYZED		=> 'Analyzed'
	];

	public static $c_arrintCallAnalysisStatusTypeIds = [
		self::NON_LEAD_CALL,
		self::DISQUALIFIED_LEAD_CALL,
		self::WARM_LEAD,
		self::EARLY_TERMINATION,
		self::FLAGGED_FOR_ANALYSIS,
		self::MAX_CALLS_EVALUATED,
		self::VOICEMAIL
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>