<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentLogs
 * Do not add any new functions to this class.
 */

class CCallAgentLogs extends CBaseCallAgentLogs {

	public static function fetchCallAgentLogsByCallAgentStatusStateLogFilter( $objPagination, $objCallAgentStatusStateLogFilter, $objVoipDatabase, $boolCountOnly = false, $boolIsDownload = false ) {
		$arrstrWhere = [];

		if( false == is_null( $objCallAgentStatusStateLogFilter->getStartDate() ) && false == is_null( $objCallAgentStatusStateLogFilter->getEndDate() ) && 'Min' != $objCallAgentStatusStateLogFilter->getStartDate() && 'Max' != $objCallAgentStatusStateLogFilter->getEndDate() ) {
			$arrstrWhere[] = 'cal.created_on >= \'' . date( 'Y-m-d', strtotime( $objCallAgentStatusStateLogFilter->getStartDate() ) ) . ' ' . $objCallAgentStatusStateLogFilter->getStartTime() . '\'';
			$arrstrWhere[] = 'cal.created_on <= \'' . date( 'Y-m-d', strtotime( $objCallAgentStatusStateLogFilter->getEndDate() ) ) . ' ' . $objCallAgentStatusStateLogFilter->getEndTime() . '\'';
		}

		if( ( false == is_null( $objCallAgentStatusStateLogFilter->getCallAgentIds() ) ) && ( '' != trim( $objCallAgentStatusStateLogFilter->getCallAgentIds() ) ) ) {
			$arrstrWhere[] = ' cal.call_agent_id IN ( ' . $objCallAgentStatusStateLogFilter->getCallAgentIds() . ' ) ';
		}

		if( ( false == is_null( $objCallAgentStatusStateLogFilter->getCallAgentStatusTypeIds() ) ) && ( '' != trim( $objCallAgentStatusStateLogFilter->getCallAgentStatusTypeIds() ) ) ) {
			$strStatusTypeId = ' cal.call_agent_status_type_id IN ( ' . $objCallAgentStatusStateLogFilter->getCallAgentStatusTypeIds() . ' ) ';
		}

		if( true == valStr( $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() ) ) {
			$arrstrCallAgentOnProjectReasons	 = [];
			$arrintSelectedOnProjectReasonIds	= explode( ',', $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() );
			$arrintUnSelectedOnProjectReasonIds = array_diff( array_keys( CCallAgentStatusType::$c_arrstrCallAgentOnProjectSubStatusTypes ), $arrintSelectedOnProjectReasonIds );

			if( true == valId( $arrintUnSelectedOnProjectReasonIds ) ) {
				if( true == in_array( CCallAgentStatusType::ON_PROJECT_REASON_OTHER, $arrintSelectedOnProjectReasonIds ) ) {
					foreach( $arrintUnSelectedOnProjectReasonIds as $intUnSelectedOnProjectReasonId ) {
						$arrstrCallAgentOnProjectReasons[] = '\'' . CCallAgentStatusType::$c_arrstrCallAgentOnProjectSubStatusTypes[$intUnSelectedOnProjectReasonId] . '\'';
					}

					$arrstrWhere[] = ' can.note NOT IN ( ' . implode( ',', $arrstrCallAgentOnProjectReasons ) . ' )';
				} else {
					foreach( $arrintSelectedOnProjectReasonIds as $intSelectedOnProjectReasonId ) {
						$arrstrCallAgentOnProjectReasons[] = '\'' . CCallAgentStatusType::$c_arrstrCallAgentOnProjectSubStatusTypes[$intSelectedOnProjectReasonId] . '\'';
					}

					$arrstrWhere[] = ' can.note IN ( ' . implode( ',', $arrstrCallAgentOnProjectReasons ) . ' )';
				}
			}

			$arrstrCallAgentOnProjectSubStatusTypes	= CCallAgentStatusType::$c_arrstrCallAgentOnProjectSubStatusTypes;
			$arrintCallAgentOnProjectReasonIds		= explode( ',', $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() );
			$arrstrCallAgentSubStatusNames			= [];

			foreach( $arrintCallAgentOnProjectReasonIds as $intCallAgentOnProjectReasonId ) {
				$arrstrCallAgentSubStatusNames[] = '\'' . $arrstrCallAgentOnProjectSubStatusTypes[$intCallAgentOnProjectReasonId] . '\'';
			}
		}

		if( true == $boolCountOnly ) {
			if( true == valStr( $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() ) ) {
				$strSql = 'SELECT
								COUNT( cal.id )
							FROM
								call_agent_logs cal
								LEFT JOIN call_agent_notes can ON ( can.call_agent_log_id = cal.id AND can.call_agent_id = cal.call_agent_id ) ';
			} else {
				$strSql = 'SELECT COUNT( cal.id ) FROM call_agent_logs cal ';
			}

			if( true == valStr( $strStatusTypeId ) ) {
				$strSql .= ' WHERE ' . implode( ' AND ', $arrstrWhere ) . ' AND ';
			}

			if( true == valStr( $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() ) ) {
				$strSql .= ' can.note IN (' . implode( ' ,', $arrstrCallAgentSubStatusNames ) . ' )  AND ';
			}
		} else {

			$strSqlCondition = '';

			if( true == valArr( $arrstrWhere ) ) {
				$strSqlCondition = str_replace( 'cal.', 'cal1.', implode( ' AND ', $arrstrWhere ) . ' ' );
			}

			if( true == valStr( $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() ) ) {
				$strSql = 'SELECT
								cal.*
								FROM
								(
									SELECT
										cal1.*,
										can.note AS call_agent_sub_status_name,
										TO_CHAR( (
												ROUND( lead( EXTRACT ( EPOCH FROM cal1.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal1.call_agent_id ORDER BY cal1.created_on ) )
												- ROUND ( EXTRACT ( EPOCH FROM cal1.created_on::TIMESTAMP WITH TIME ZONE ) ) || \' SECOND\' ) :: interval, \'HH24:MI:SS\' ) AS call_agent_status_state_formatted_duration
									FROM
										call_agent_logs AS cal1
										LEFT JOIN call_agent_notes AS can ON ( can.call_agent_log_id = cal1.id AND can.call_agent_id = cal1.call_agent_id )
									WHERE
										' . $strSqlCondition . '
								) AS cal';
			} else {
				$strSql = 'SELECT
								cal.*
							FROM
								(
									SELECT
										cal1.*,
										TO_CHAR( (
												ROUND( lead( EXTRACT ( EPOCH FROM cal1.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal1.call_agent_id ORDER BY cal1.created_on ) )
												- ROUND ( EXTRACT ( EPOCH FROM cal1.created_on::TIMESTAMP WITH TIME ZONE ) ) || \' SECOND\' ) :: interval, \'HH24:MI:SS\' ) AS call_agent_status_state_formatted_duration
									FROM
										call_agent_logs AS cal1
									WHERE
										' . $strSqlCondition . '
								) AS cal';
			}

			if( true == valStr( $strStatusTypeId ) ) {
				$strSql .= ' WHERE ';
			}

			if( true == valStr( $objCallAgentStatusStateLogFilter->getCallAgentOnProjectReasonIds() ) ) {
				$strSql .= ' call_agent_sub_status_name IN ( ' . implode( ' ,', $arrstrCallAgentSubStatusNames ) . ' ) AND ';
			}
		}

		if( true == valStr( $strStatusTypeId ) ) {
			$strSql .= $strStatusTypeId;
		}

		if( false == $boolCountOnly ) {
			$strSql .= ' ORDER BY cal.created_on ASC';

			if( false == $boolIsDownload && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
				$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		if( true == $boolCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		}

		return self::fetchCallAgentLogs( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentStatusTypesByInterval( $arrintCallAgentStatusTypeIds, $strDatetime, $intInterval, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cal.call_agent_id,
						casts.name AS call_agent_status_type_name,
						castt.name AS call_agent_state_type_name,
						ca.full_name AS call_agent_name,
						ca.physical_phone_extension_id,
						ca.agent_phone_extension_id,
						cal.created_on
					FROM
						call_agent_logs cal
						JOIN call_agent_state_types castt ON (castt.id = cal.call_agent_state_type_id)
						JOIN call_agent_status_types casts ON (casts.id = cal.call_agent_status_type_id)
						JOIN call_agents AS ca ON (ca.id = cal.call_agent_id)
					WHERE
						cal.call_agent_status_type_id IN (' . implode( ' ,', $arrintCallAgentStatusTypeIds ) . ')
						AND  DATE_TRUNC(\'second\', cal.created_on) BETWEEN timestamp \' ' . $strDatetime . ' \' - interval \' ' . ( int ) $intInterval . ' \' minute AND timestamp \' ' . $strDatetime . ' \'
					ORDER BY
						cal.created_on';

		return self::fetchCallAgentLogs( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentStatusTypesByIntervalByEmployeeIds( $arrintCallAgentStatusTypeIds, $strDatetime, $intInterval, $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStatusTypeIds ) ) {
			return NULL;
		}

		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						cal.call_agent_id,
						casts.name AS call_agent_status_type_name,
						castt.name AS call_agent_state_type_name,
						ca.full_name AS call_agent_name,
						ca.physical_phone_extension_id,
						ca.agent_phone_extension_id,
						cal.created_on
					FROM
						call_agent_logs cal
						JOIN call_agent_state_types castt ON (castt.id = cal.call_agent_state_type_id)
						JOIN call_agent_status_types casts ON (casts.id = cal.call_agent_status_type_id)
						JOIN call_agents AS ca ON (ca.id = cal.call_agent_id)
					WHERE
						cal.call_agent_status_type_id IN (' . implode( ' ,', $arrintCallAgentStatusTypeIds ) . ')
						AND  DATE_TRUNC(\'second\', cal.created_on) BETWEEN timestamp \' ' . $strDatetime . ' \' - interval \' ' . ( int ) $intInterval . ' \' minute AND timestamp \' ' . $strDatetime . ' \'
						AND ca.employee_id IN (' . implode( ' ,', ( array ) $arrintEmployeeIds ) . ')
					ORDER BY
						cal.created_on';

		return self::fetchCallAgentLogs( $strSql, $objVoipDatabase );
	}

	public static function fetchLastCallAgentLogByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT 
					 *
					FROM 
						(
						SELECT 
								*, 
						rank ( ) OVER ( PARTITION BY call_agent_id ORDER BY created_on DESC, id ) 
									  FROM 
												call_agent_logs 
									  WHERE 
											 created_on > \'' . date( 'Y-m-d' ) . ' 00:00:01\'
											 AND created_on < \'' . date( 'Y-m-d' ) . ' 23:59:59\'
									  AND call_agent_id = ' . ( int ) $intCallAgentId . ' 
						) AS sub
									 WHERE
										rank = 1
									 LIMIT 1';

		return self::fetchCallAgentLog( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogDataByAgentId( $intAgentId, $objVoipDatabase ) {

		$strSql = 'SELECT
						cal.*
					FROM
						call_agent_logs AS cal
					WHERE
						cal.call_agent_id = ' . ( int ) $intAgentId . '
						AND cal.created_on > ( now() - INTERVAL \'10 days\' )
						AND created_by <> 1
					ORDER BY
						id DESC LIMIT 1';

		return self::fetchCallAgentLog( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleUnAnsweredCallsDataByCallAgentIdByStartDateByEndDate( $intCallAgentId, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						created_on,
						EXTRACT( ISODOW from  created_on ) AS week_day,
						CONCAT_WS( \':\',EXTRACT( hour from created_on ),EXTRACT( minute from created_on ) ) AS week_day_time_slot,
						CASE WHEN ( EXTRACT( minute from created_on ) >= 25 AND EXTRACT( minute from created_on ) <= 29 ) 
							 THEN TRUNC( ( CAST( concat_ws( \'.\', EXTRACT( hour from created_on ), lpad( CAST( EXTRACT( minute from created_on ) AS VARCHAR ), 2, \'0\' ) ) AS FLOAT ) * 2 ) + 1 )
							 ELSE ROUND( ( CAST( concat_ws( \'.\', EXTRACT( hour from created_on ), lpad( CAST( EXTRACT( minute from created_on ) AS VARCHAR ), 2, \'0\' ) ) AS FLOAT ) * 2 ) + 1 )
								END AS interval_sequence
					FROM
						call_agent_logs
					WHERE
						call_agent_id = ' . ( int ) $intCallAgentId . '
						AND DATE_TRUNC( \'second\', created_on ) BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
						AND call_agent_status_type_id = ' . ( int ) CCallAgentStatusType::AVAILABLE_ON_DEMAND . '
						AND call_agent_state_type_id = ' . ( int ) CCallAgentStateType::NO_ANSWER;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentIdByCallAgentStateTypeIdsByCallAgentStatusTypeIdsByCreatedOn( $intCallAgentId, $arrintCallAgentStateTypeIds, $arrintCallAgentStatusTypeIds, $strIntervalStartEpoch, $strIntervalEndEpoch, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valArr( $arrintCallAgentStateTypeIds ) || false == valArr( $arrintCallAgentStatusTypeIds ) || false == valStr( $strIntervalStartEpoch ) || false == valStr( $strIntervalEndEpoch ) ) {
			return NULL;
		}
		$arrstrWhere	= [];
		$arrstrWhere[] = 'cal.created_on >= \'' . $strIntervalStartEpoch . '\'';
		$arrstrWhere[] = 'cal.created_on <= \'' . $strIntervalEndEpoch . '\'';
		$arrstrWhere[] = ' cal.call_agent_id  = ' . $intCallAgentId;
		$arrstrWhere[] = ' cal.call_agent_state_type_id IN ( ' . implode( ',', $arrintCallAgentStateTypeIds ) . ' ) ';

		$strSql = 'SELECT
					SUM(call_agent_status_state_formatted_duration) AS total_call_agent_status_state_formatted_duration
					FROM
					(
						SELECT
							*
						FROM
						(
							SELECT
								cal.*,
								ROUND( lead( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal.call_agent_id ORDER BY cal.id ) )
								- ROUND ( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) AS call_agent_status_state_formatted_duration
							FROM
								call_agent_logs AS cal
							WHERE ';

		$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';

		$strSql .= ' ) AS sub_query';

		$strSql .= ' WHERE
						call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypeIds ) . ' )';

		$strSql .= ' ) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentIdsByCallAgentStateTypeIdsByCallAgentStatusTypeIdsByCreatedOn( $arrintCallAgentIds, $arrintCallAgentStateTypeIds, $arrintCallAgentStatusTypeIds, $strIntervalStartEpoch, $strIntervalEndEpoch, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valArr( $arrintCallAgentStateTypeIds ) || false == valArr( $arrintCallAgentStatusTypeIds ) || false == valStr( $strIntervalStartEpoch ) || false == valStr( $strIntervalEndEpoch ) ) {
			return NULL;
		}
		$arrstrWhere	= [];
		$arrstrWhere[] = 'cal.created_on >= \'' . $strIntervalStartEpoch . '\'';
		$arrstrWhere[] = 'cal.created_on <= \'' . $strIntervalEndEpoch . '\'';
		$arrstrWhere[] = ' cal.call_agent_id  IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		$strSql = 'SELECT
					call_agent_id,
					SUM(call_agent_status_state_formatted_duration) AS total_call_agent_status_state_formatted_duration
					FROM
					(
						SELECT
							*
						FROM
						(
							SELECT
								cal.*,
								ROUND( lead( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal.call_agent_id ORDER BY cal.id ) )
								- ROUND ( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) AS call_agent_status_state_formatted_duration
							FROM
								call_agent_logs AS cal
							WHERE ';

		$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';

		$strSql .= ' ) AS sub_query';

		$strSql .= ' WHERE
						call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypeIds ) . ' )
						AND call_agent_state_type_id IN (' . implode( ',', $arrintCallAgentStateTypeIds ) . ' ) ';

		$strSql .= ' ) AS sub_query';

		$strSql .= ' GROUP BY call_agent_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentIdsByCallAgentStateTypeIdsByCallAgentStatusTypeIdsByDateIntervals( $arrintCallAgentIds, $arrintCallAgentStateTypeIds, $arrintCallAgentStatusTypeIds, $arrstrDateIntervals, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valArr( $arrintCallAgentStateTypeIds ) || false == valArr( $arrintCallAgentStatusTypeIds ) || false == valArr( $arrstrDateIntervals ) ) {
			return NULL;
		}

		$arrstrWhere	= [];
		$arrstrWhere[]	= ' cal.call_agent_id  IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';
		$strWhereSql	= NULL;

		foreach( $arrstrDateIntervals as $arrstrDateInterval ) {
			$strWhereSql .= ' cal.created_on BETWEEN \'' . $arrstrDateInterval['start_time'] . '\' AND \'' . $arrstrDateInterval['end_time'] . '\' OR';
		}

		$arrstrWhere[] .= ' (' . rtrim( $strWhereSql, 'OR' ) . ')';

		$strSql = 'SELECT
					call_agent_id,
					SUM(call_agent_status_state_formatted_duration) AS total_call_agent_status_state_formatted_duration
					FROM
					(
						SELECT
							*
						FROM
						(
							SELECT
								cal.*,
								ROUND( lead( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal.call_agent_id ORDER BY cal.id ) )
								- ROUND ( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) AS call_agent_status_state_formatted_duration
							FROM
								call_agent_logs AS cal
							WHERE ';

		$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';

		$strSql .= ' ) AS sub_query';

		$strSql .= ' WHERE
						call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypeIds ) . ' )
						AND call_agent_state_type_id IN (' . implode( ',', $arrintCallAgentStateTypeIds ) . ' ) ';

		$strSql .= ' ) AS sub_query';

		$strSql .= ' GROUP BY call_agent_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentLogInLogOutLogsByCallAgentIdByStartDateByEndDate( $intCallAgentId, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						call_agent_status_type_id,
						created_on,
						EXTRACT( ISODOW FROM created_on ) AS week_day,
						CASE WHEN ( EXTRACT( minute from created_on ) >= 25 AND EXTRACT( minute from created_on ) <= 29 ) 
							 THEN TRUNC( ( ( CAST( concat_ws( \'.\', EXTRACT( hour from created_on ), lpad( CAST( EXTRACT( minute from created_on ) AS VARCHAR ), 2, \'0\' ) ) AS FLOAT ) * 2 ) + 1 ) + ( ( EXTRACT( ISODOW FROM created_on ) - 1 ) * 48 ) )
							 ELSE ROUND( ( ( CAST( concat_ws( \'.\', EXTRACT( hour from created_on ), lpad( CAST( EXTRACT( minute from created_on ) AS VARCHAR ), 2, \'0\' ) ) AS FLOAT ) * 2 ) + 1 ) + ( ( EXTRACT( ISODOW FROM created_on ) - 1 ) * 48 ) )
								END AS interval_sequence
					FROM
						(
							SELECT
								call_agent_status_type_id,
								created_on,
								lag ( call_agent_status_type_id ) over ( PARTITION BY CAST ( created_on AS DATE ) ORDER BY created_on ASC ) AS lag_status_type_id
							FROM
								call_agent_logs
							WHERE
								call_agent_id = ' . ( int ) $intCallAgentId . '
								AND created_on BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
								AND ( ( ( call_agent_status_type_id = ' . ( int ) CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' OR call_agent_status_type_id = ' . ( int ) CCallAgentStatusType::AVAILABLE . ' ) AND call_agent_state_type_id = ' . ( int ) CCallAgentStateType::WAITING . ' ) OR ( call_agent_status_type_id = ' . ( int ) CCallAgentStatusType::LOGGED_OUT . ' AND call_agent_state_type_id = ' . ( int ) CCallAgentStateType::IDLE . ' ) )
							GROUP BY
								created_on,
								call_agent_status_type_id
								ORDER BY
								created_on ASC
						) AS innerquery
					WHERE
						( lag_status_type_id IS NULL OR call_agent_status_type_id <> lag_status_type_id )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomCallAgentLogsByCallAgentIdByStartDateByEndDate( $intCallAgentId, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						call_agent_id,
						call_agent_status_type_id,
						call_agent_state_type_id,
						extract( ISODOW  from  created_on ) AS week_day,
						concat_ws ( \'.\', extract( hour from created_on ), lpad( CAST( extract( minute from created_on ) AS VARCHAR ), 2, \'0\' ) ) AS week_day_time_slot,
						ROUND ( ( CAST ( concat_ws ( \'.\', extract( hour from created_on ), lpad( CAST( extract( minute from created_on ) AS VARCHAR ), 2, \'0\') ) AS FLOAT ) * 2 ) + 1 ) AS interval_sequence,
						created_on
					FROM
						call_agent_logs
					WHERE
						created_on BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59\'
						AND call_agent_id = ' . ( int ) $intCallAgentId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomCallAgentLogsByStartDateByEndDateByEmployeeIds( $strStartDate, $strEndDate, $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) || false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						ca.employee_id,
						cal.call_agent_id,
						cal.call_agent_status_type_id,
						cal.call_agent_state_type_id,
						extract( ISODOW  from  cal.created_on ) AS week_day,
						concat_ws ( \'.\', extract( hour from cal.created_on ), lpad( CAST( extract( minute from cal.created_on ) AS VARCHAR ), 2, \'0\' ) ) AS week_day_time_slot,
						ROUND ( ( CAST ( concat_ws ( \'.\', extract( hour from cal.created_on ), lpad( CAST( extract( minute from cal.created_on ) AS VARCHAR ), 2, \'0\') ) AS FLOAT ) * 2 ) + 1 ) AS interval_sequence,
						cal.created_on
					FROM
						call_agent_logs AS cal 
					JOIN call_agents AS ca ON ( ca.id = cal.call_agent_id )
					WHERE
						cal.created_on BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59\'
						AND ca.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchWeeklyCallAgentStatusLogsByCallAgentIdsByCreatedOn( $arrintCallAgentIds, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					sub_query.call_agent_id,
					sub_query.employee_id,
					sub_query.call_agent_log_week,
					SUM(sub_query.call_agent_status_state_formatted_duration) AS weekly_working_hours
				FROM
					(
						SELECT
							cal.call_agent_id,
							cg.employee_id,
							casts.name AS call_agent_status_type_name,
							casts.id AS call_agent_status_type_id,
							ROUND( lead( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) OVER (PARTITION BY cal.call_agent_id ORDER BY cal.id ) )
							- ROUND ( EXTRACT ( EPOCH FROM cal.created_on::TIMESTAMP WITH TIME ZONE ) ) AS call_agent_status_state_formatted_duration,
							TO_CHAR ( CAST ( cal.created_on AS DATE ), \'WW\' ) AS call_agent_log_week
						FROM
							call_agent_logs AS cal
							LEFT JOIN call_agent_status_types AS casts ON ( casts.id = cal.call_agent_status_type_id )
							LEFT JOIN call_agents AS cg ON ( cg.id = cal.call_agent_id ) 
						WHERE
							cal.created_on BETWEEN \'' . $strStartDate . '\'::TIMESTAMP
							AND \'' . $strEndDate . '\'::TIMESTAMP
							AND cal.call_agent_id IN ( ' . sqlIntImplode( $arrintCallAgentIds ) . ' )
						ORDER BY
							cal.created_on
					) AS sub_query
				WHERE
					sub_query.call_agent_status_type_id NOT IN ( ' . CCallAgentStatusType::LOGGED_OUT . ', ' . CCallAgentStatusType::ON_BREAK . ', ' . CCallAgentStatusType::ON_LUNCH . ' )
				GROUP BY
					sub_query.call_agent_id, sub_query.employee_id, sub_query.call_agent_log_week
				ORDER BY
					sub_query.employee_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallFilter( $objCallFilter, $objVoipDatabase, $intReferenceValue = NULL, $boolIsMostMonthlyChallenge = false ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSubSql = '(
						SELECT 
							sub_query.call_agent_id, 
							ROUND( SUM( ( sub_query.call_agent_status_state_formatted_duration ) / 3600 )::numeric, 2 ) AS monthly_working_hours
						FROM 
						(
							SELECT 
								cal.call_agent_id, 
								casts.name AS call_agent_status_type_name,
								casts.id AS call_agent_status_type_id,
								ROUND( lead( EXTRACT(EPOCH FROM cal.created_on :: TIMESTAMP WITH TIME ZONE ) ) OVER ( PARTITION BY cal.call_agent_id ORDER BY cal.id ) ) 
								- ROUND( EXTRACT ( EPOCH FROM cal.created_on :: TIMESTAMP WITH TIME ZONE ) ) AS call_agent_status_state_formatted_duration 
							FROM 
								call_agent_logs AS cal
								LEFT JOIN call_agent_status_types AS casts ON ( casts.id = cal.call_agent_status_type_id )
							WHERE 
								cal.created_on BETWEEN \'' . $objCallFilter->getStartDate() . '\':: TIMESTAMP AND \'' . $objCallFilter->getEndDate() . '\':: TIMESTAMP
								AND cal.call_agent_id IN ( ' . sqlIntImplode( $objCallFilter->getCallAgentIds() ) . ' )
							ORDER BY cal.created_on 
						) AS sub_query
						WHERE 
							sub_query.call_agent_status_type_id NOT IN ( ' . CCallAgentStatusType::LOGGED_OUT . ', ' . CCallAgentStatusType::ON_BREAK . ', ' . CCallAgentStatusType::ON_LUNCH . ' )
						GROUP BY 
							sub_query.call_agent_id
						ORDER BY 
							sub_query.call_agent_id 
						) AS sub_query1';

		if( true == valId( $intReferenceValue ) ) {
			$strSql = 'SELECT 
							* 
						FROM 
							' . $strSubSql . ' 
						WHERE 
							monthly_working_hours >= ' . ( int ) $intReferenceValue;
		}

		if( true == $boolIsMostMonthlyChallenge ) {
			$strSql = 'SELECT 
							sub_query1.call_agent_id, 
							MAX(sub_query1.monthly_working_hours)
						FROM ' . $strSubSql . ' 
						WHERE 
							monthly_working_hours = (
								SELECT 
									MAX(sub_query1.monthly_working_hours)
								FROM 
									' . $strSubSql . ' )
								GROUP BY 
									sub_query1.call_agent_id';
		}

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentLogsByCallAgentStateTypeIdsByCallAgentStatusTypeIdsByCallFilter( $arrintCallAgentStateTypeIds, $arrintCallAgentStatusTypeIds, $objCallFilter, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStateTypeIds ) || false == valArr( $arrintCallAgentStatusTypeIds ) || false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSubSql = 'SELECT 
						call_agent_id, 
						SUM(call_agent_status_state_formatted_duration) AS total_call_agent_status_state_formatted_duration
					FROM 
					(
						SELECT 
							*
						FROM 
						(
							SELECT 
								cal.*, 
								ROUND(lead(EXTRACT(EPOCH FROM cal.created_on :: TIMESTAMP WITH TIME ZONE)) OVER(PARTITION BY cal.call_agent_id ORDER BY cal.id)) 
								- ROUND(EXTRACT(EPOCH FROM cal.created_on :: TIMESTAMP WITH TIME ZONE)) AS call_agent_status_state_formatted_duration
							FROM 
								call_agent_logs AS cal
							WHERE 
								cal.created_on >= \'' . $objCallFilter->getStartDate() . '\'
								AND cal.created_on <= \'' . $objCallFilter->getEndDate() . '\'
								AND cal.call_agent_id  IN ( ' . implode( ',', $objCallFilter->getCallAgentIds() ) . ' )
						) AS sub_query1
						WHERE 
							call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypeIds ) . ' ) 
							AND call_agent_state_type_id IN (' . implode( ',', $arrintCallAgentStateTypeIds ) . ' )
					) AS sub_query2
					GROUP BY 
						call_agent_id';

		$strSql = $strSubSql . '
					HAVING 
						SUM(call_agent_status_state_formatted_duration) = 
						(
							SELECT 
								MAX(sub_query3.total_call_agent_status_state_formatted_duration)
							FROM 
								( ' . $strSubSql . ' ) AS sub_query3 
						)';

		return fetchData( $strSql, $objVoipDatabase );
	}

}

?>