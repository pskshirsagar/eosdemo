<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CDialplans
 * Do not add any new functions to this class.
 */

class CDialplans extends CBaseDialplans {

	public static function fetchCachedDialplans( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDialplan', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCachedDialplan( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDialplan', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCachedActiveDialplanByExtension( $strExtension, $objVoipDatabase ) {
		return self::fetchCachedDialplan( 'SELECT * FROM dialplans WHERE extension = \'' . addslashes( $strExtension ) . '\' AND is_published = true LIMIT 1', $objVoipDatabase );
	}

	public static function fetchActiveDialplans( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM dialplans ORDER BY created_on DESC, name';
		return self::fetchDialplans( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveDialPlanByExtension( $strExtension, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM dialplans WHERE extension = \'' . addslashes( $strExtension ) . '\' AND is_published = TRUE LIMIT 1';
		return self::fetchDialplan( $strSql, $objVoipDatabase );
	}

	public static function fetchDialplanById( $intDialplanId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM dialplans WHERE id = ' . ( int ) $intDialplanId . ' LIMIT 1';
		return self::fetchDialplan( $strSql, $objVoipDatabase );
	}

	public static function fetchDialplansByFilter( $objCallFilter, $objVoipDatabase ) {
		$strWhere = NULL;

		if( false == is_null( $objCallFilter->getGenericData() ) ) {
			$strWhere = ' WHERE
							LOWER( name ) LIKE LOWER( \'%' . addslashes( $objCallFilter->getGenericData() ) . '%\' )
							OR LOWER( file_name ) LIKE LOWER( \'%' . addslashes( $objCallFilter->getGenericData() ) . '%\' )
							OR LOWER( extension ) LIKE LOWER( \'%' . addslashes( $objCallFilter->getGenericData() ) . '%\' )';
		}

		$strSql = 'SELECT
						*
					FROM
						dialplans
						' . $strWhere . '
					ORDER BY
						name';

		return self::fetchDialplans( $strSql, $objVoipDatabase );
	}

}
?>