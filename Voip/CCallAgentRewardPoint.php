<?php

class CCallAgentRewardPoint extends CBaseCallAgentRewardPoint {

	protected $m_intRankPointMappingId;
	protected $m_intCredits;
	protected $m_intRankNumber;

	/**
	* Set Functions
	*/

	public function setDefaults() {
		parent::setDefaults();
		$this->m_fltTotalNumberOfPoints	= 0.0;
		$this->m_fltTotalNumberOfCoins	= 0.0;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['rank_point_mapping_id'] ) )	$this->setRankPointMappingId( $arrmixValues['rank_point_mapping_id'] );
		if( isset( $arrmixValues['rank_number'] ) )				$this->setRankNumber( $arrmixValues['rank_number'] );
		if( isset( $arrmixValues['credits'] ) )					$this->setCredits( $arrmixValues['credits'] );
		return true;
	}

	public function setRankPointMappingId( $intRankPointMappingId ) {
		$this->m_intRankPointMappingId = $intRankPointMappingId;
	}

	public function setRankNumber( $intRankNumber ) {
		$this->m_intRankNumber = $intRankNumber;
	}

	public function setCredits( $intCredits ) {
		$this->m_intCredits = $intCredits;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getRankPointMappingId() {
		return $this->m_intRankPointMappingId;
	}

	public function getRankNumber() {
		return $this->m_intRankNumber;
	}

	public function getCredits() {
		return $this->m_intCredits;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentRewardPointLogId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentRewardPointLogId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_reward_point_log_id', 'Call agent reward point log id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTotalNumberOfPoints() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTotalNumberOfPoints() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_number_of_points', 'Invalid total number of points.' ) );
		}

		return $boolIsValid;
	}

	public function valTotalNumberOfCoins() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTotalNumberOfCoins() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'total_number_of_coins', 'Invalid total number of coins.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valCallAgentRewardPointLogId();
				$boolIsValid &= $this->valTotalNumberOfPoints();
				$boolIsValid &= $this->valTotalNumberOfCoins();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_call_agent_reward_point_update':
				$boolIsValid &= $this->valCallAgentRewardPointLogId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>