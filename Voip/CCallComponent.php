<?php

class CCallComponent extends CBaseCallComponent {

	const EVENT_SOCKET				= 1;
	const DIAL_PLAN					= 2;
	const SIP						= 3;
	const HTTAPI					= 4;
	const XMLCURL					= 5;
	const XMLRPC					= 6;
	const FS_SERVER					= 7;
	const FS_DB						= 8;
	const VOIP_DB					= 9;
	const CHAT_DB					= 10;
	const CALLCENTER1				= 11;
	const CALLCENTER2				= 12;
	const EJABBERED_SERVER			= 13;
	const INBOUND_CALLS_QUEUE		= 14;
	const CALLS_NOTIFICATION_QUEUE	= 15;

	const SIP_PROFILE_INTERNAL	= '5080internal';
	const SIP_PROFILE_EXTERNAL	= '5070external';
	const SIP_PROFILE_TRUNKS	= '5060trunks';

	const SIP_REQUEST_PORT_TRUNK	= 5060;
	const SIP_REQUEST_PORT_INTERNAL = 5080;
	const SIP_REQUEST_PORT_EXTERNAL = 5070;

	public static $c_arrstrCallComponentIdsToNames = array(
		self::EVENT_SOCKET				=> 'Event Socket',
		self::DIAL_PLAN					=> 'Dial Plan',
		self::SIP						=> 'Sofia SIP',
		self::HTTAPI					=> 'Httapi',
		self::XMLCURL					=> 'XML Curl',
		self::XMLRPC					=> 'XML Rpc',
		self::FS_SERVER					=> 'Freeswitch Server',
		self::FS_DB						=> 'Freeswitch DB',
		self::VOIP_DB					=> 'Voip DB',
		self::CHAT_DB					=> 'Chat DB',
		self::CALLCENTER1				=> 'CallCenter1',
		self::CALLCENTER2				=> 'CallCenter2',
		self::EJABBERED_SERVER			=> 'Ejabbered Server',
		self::INBOUND_CALLS_QUEUE		=> 'Inbound Calls Queue',
		self::CALLS_NOTIFICATION_QUEUE	=> 'Calls Notification Queue'
	);

	public static $c_arrstrCallComponentNamesToIds = array(
		'event_socket'				=> self::EVENT_SOCKET,
		'dial_plan'					=> self::DIAL_PLAN,
		'sip'						=> self::SIP,
		'httapi'					=> self::HTTAPI,
		'xmlcurl'					=> self::XMLCURL,
		'xmlrpc'					=> self::XMLRPC,
		'freeswitch'				=> self::FS_SERVER,
		'freeswitch_db'				=> self::FS_DB,
		'voip_db'					=> self::VOIP_DB,
		'chat_db'					=> self::CHAT_DB,
		'callcenter1'				=> self::CALLCENTER1,
		'callcenter2'				=> self::CALLCENTER2,
		'ejabbered_server'			=> self::EJABBERED_SERVER,
		'inbound_calls_queue'		=> self::INBOUND_CALLS_QUEUE,
		'calls_notification_queue'	=> self::CALLS_NOTIFICATION_QUEUE
	);

	public static $c_arrstrCallModules = [
		'configuration'							=> 'configuration',
		'dialplan'								=> 'dialplan',
		'directory'								=> 'directory',
		'freeswitch'							=> 'freeswitch',
		'phrases'								=> 'phrases',
		'languages'								=> 'languages',
		'leasing_center_tv_display'				=> 'leasing_center_tv_display',
		'calls'									=> 'calls',
		'call'									=> 'call',
		'visa_phone_auth'						=> 'visa_phone_auth',
		'property_greeting_calls'				=> 'property_greeting_calls',
		'scheduled_call_records'				=> 'scheduled_call_records',
		'call_system_health_dashboard'			=> 'call_system_health_dashboard',
		'twilio_call_response'					=> 'twilio_call_response',
		'call_system_health_dashboard_report'	=> 'call_system_health_dashboard_report',
		'dialplan_tools'						=> 'dialplan_tools'
	];

	/**
	 * CCallComponent constructor.
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>