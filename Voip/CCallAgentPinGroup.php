<?php

class CCallAgentPinGroup extends CBaseCallAgentPinGroup {
	const QA								= 1;
	const JONESES_CLUB						= 2;
	const OPERATIONAL						= 3;
	const PHONE_CALL_MILESTONES				= 4;
	const AGENT_DESIGNATION					= 5;
	const WORK_ANNIVERSARY					= 6;
	const SPECIAL_TEAMS						= 7;
	const STRATEGIC_CLIENT_CERTIFICATION	= 8;
}

?>