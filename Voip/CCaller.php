<?php

class CCaller extends CBaseCaller {
	protected $m_strCallerPhoneNumber;
	protected $m_strCompanyName;
	protected $m_strPropertyName;

	public function getCallerPhoneNumber() {
		return $this->m_strCallerPhoneNumber;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getFullName() {
		return $this->getFirstName() . ' ' . $this->getLastName();
	}

	public function valEmailAddress() {
		if( true !== CValidation::validateEmailAddress( $this->getEmailAddress() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Valid email address is required.' ) );
			return false;
		}
		return true;
	}

	public function valFirstName() {
		if( '' == $this->getFirstName() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'First Name is required.' ) );
			return false;
		}
		return true;
	}

	public function valLastName() {
		if( '' == $this->getLastName() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Last Name is required.' ) );
			return false;
		}
		return true;
	}

	public function valPropertyId() {
		if( false == valId( $this->getPropertyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Property is required.' ) );
			return false;
		}
		return true;
	}

	public function valCid() {
		if( false == valId( $this->getCid() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Client is required.' ) );
			return false;
		}
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valFirstName() && $this->valLastName() && $this->valEmailAddress() && $this->valPropertyId() && $this->valCid();
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( isset( $arrmixValues['caller_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strCallerPhoneNumber', trim( stripcslashes( $arrmixValues['caller_phone_number'] ) ) ); elseif( isset( $arrmixValues['caller_phone_number'] ) ) $this->setCallerPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['caller_phone_number'] ) : $arrmixValues['caller_phone_number'] );
		if( isset( $arrmixValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrmixValues['company_name'] ) ) ); elseif( isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['company_name'] ) : $arrmixValues['company_name'] );
		if( isset( $arrmixValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrmixValues['property_name'] ) ) ); elseif( isset( $arrmixValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['property_name'] ) : $arrmixValues['caller_phone_number'] );

	}

	public function setCallerPhoneNumber( $strCallerPhoneNumber ) {
		$this->m_strCallerPhoneNumber = $strCallerPhoneNumber;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

}
?>