<?php

class CStoreProductLog extends CBaseStoreProductLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoreProductLogTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreProductLogTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_product_log_type_id', 'Store product log type id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStoreProductId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_product_id', 'Store product id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_store_product_log_insert':
				$boolIsValid &= $this->valStoreProductLogTypeId();
				$boolIsValid &= $this->valStoreProductId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>