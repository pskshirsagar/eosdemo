<?php

class CCallFilterType extends CBaseCallFilterType {

	const LEASING_CENTER_TV_DISPLAY = 1;
	const CALL_SHADOW				= 2;
	const CALL_LOGS					= 3;
	const CALL_DOWNLOAD_LOGS		= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>