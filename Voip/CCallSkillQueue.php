<?php

class CCallSkillQueue extends CBaseCallSkillQueue {

	protected $m_intCallQueueId;
	protected $m_intCallAgentId;
	protected $m_strCallAgentName;
	protected $m_strCallQueueName;
	protected $m_strCallSkillQueuePriority;
	protected $m_strCallQueueDomain;
	protected $m_strCallAgentDomain;
	protected $m_strCallAgentPhysicalExtension;
	protected $m_intCallAgentQueueLevel;
	protected $m_intCallAgentQueuePosition;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_intAgentPhoneExtensionId;
	protected $m_intPhysicalPhoneExtensionId;

	/**
	 * Setter Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_queue_id'] ) )					$this->setCallQueueId( $arrmixValues['call_queue_id'] );
		if( true == isset( $arrmixValues['call_agent_id'] ) )					$this->setCallAgentId( $arrmixValues['call_agent_id'] );
		if( true == isset( $arrmixValues['call_agent_name'] ) )					$this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['call_queue_name'] ) )					$this->setCallQueueName( $arrmixValues['call_queue_name'] );
		if( true == isset( $arrmixValues['call_skill_queue_priority'] ) )		$this->setCallSkillQueuePriority( $arrmixValues['call_skill_queue_priority'] );
		if( true == isset( $arrmixValues['call_queue_domain'] ) )				$this->setCallQueueDomain( $arrmixValues['call_queue_domain'] );
		if( true == isset( $arrmixValues['call_agent_domain'] ) )				$this->setCallAgentDomain( $arrmixValues['call_agent_domain'] );
		if( true == isset( $arrmixValues['call_queue_extension'] ) )			$this->setCallQueueExtension( $arrmixValues['call_queue_extension'] );
		if( true == isset( $arrmixValues['agent_phone_extension_id'] ) )		$this->setAgentPhoneExtensionId( $arrmixValues['agent_phone_extension_id'] );
		if( true == isset( $arrmixValues['physical_phone_extension_id'] ) )		$this->setPhysicalPhoneExtensionId( $arrmixValues['physical_phone_extension_id'] );
		if( true == isset( $arrmixValues['call_agent_queue_level'] ) )			$this->setCallAgentQueueLevel( $arrmixValues['call_agent_queue_level'] );
		if( true == isset( $arrmixValues['call_agent_queue_position'] ) )		$this->setCallAgentQueuePosition( $arrmixValues['call_agent_queue_position'] );
		if( true == isset( $arrmixValues['call_agent_state_type_name'] ) )		$this->setCallAgentStateTypeName( $arrmixValues['call_agent_state_type_name'] );
		if( true == isset( $arrmixValues['call_agent_status_type_name'] ) )		$this->setCallAgentStatusTypeName( $arrmixValues['call_agent_status_type_name'] );
		return;
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallSkillQueuePriority( $intCallSkillQueuePriority ) {
		$this->m_intCallSkillQueuePriority = $intCallSkillQueuePriority;
	}

	public function setCallQueueDomain( $strCallQueueDomain ) {
		$this->m_strCallQueueDomain = $strCallQueueDomain;
	}

	public function setCallAgentDomain( $strCallAgentDomain ) {
		$this->m_strCallAgentDomain = $strCallAgentDomain;
	}

	public function setCallAgentExtension( $strCallAgentExtension ) {
		$this->m_strCallAgentExtension = $strCallAgentExtension;
	}

	public function setCallQueueExtension( $strCallQueueExtension ) {
		$this->m_strCallQueueExtension = $strCallQueueExtension;
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->m_intAgentPhoneExtensionId = $intAgentPhoneExtensionId;
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->m_intPhysicalPhoneExtensionId = $intPhysicalPhoneExtensionId;
	}

	public function setCallAgentPhysicalExtension( $strCallAgentPhysicalExtension ) {
		$this->m_strCallAgentPhysicalExtension = $strCallAgentPhysicalExtension;
	}

	public function setCallAgentQueueLevel( $strCallAgentQueueLevel ) {
		$this->m_strCallAgentQueueLevel = $strCallAgentQueueLevel;
	}

	public function setCallAgentQueuePosition( $strCallAgentQueuePosition ) {
		$this->m_strCallAgentQueuePosition = $strCallAgentQueuePosition;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	/**
	 * Getter Functions
	 *
	 */

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCallQueueExtension() {
		return $this->m_strCallQueueExtension;
	}

	public function getCallQueueDomain() {
		return $this->m_strCallQueueDomain;
	}

	public function getCallAgentDomain() {
		return $this->m_strCallAgentDomain;
	}

	public function getCallAgentPhysicalExtension() {
		return $this->m_strCallAgentPhysicalExtension;
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function getCallAgentQueueLevel() {
		return $this->m_strCallAgentQueueLevel;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallQueueSkillQueuePriority() {
		return $this->m_strCallQueueSkillQueuePriority;
	}

	public function getCallAgentExtension() {
		return $this->m_strCallAgentExtension;
	}

	public function getCallAgentQueuePosition() {
		return $this->m_strCallAgentQueuePosition;
	}

	public function getBuildCallQueueName() {
		return ( $strCallQueueName = $this->getCallQueueExtension() . CCallHelper::SIP_DOMAIN_NAME );
	}

	public function getBuildCallAgentName() {
		return ( $strCallAgentName = $this->getCallAgentExtension() . CCallHelper::SIP_DOMAIN_NAME );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCallQueueId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallQueueId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Call Queue is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallQueueId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>