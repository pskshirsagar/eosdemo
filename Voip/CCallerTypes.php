<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerTypes
 * Do not add any new functions to this class.
 */

class CCallerTypes extends CBaseCallerTypes {

	public static function fetchCallerTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallerType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallerType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallerType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>