<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CGreetings
 * Do not add any new functions to this class.
 */

class CGreetings extends CBaseGreetings {

	// Employee Owned Greetings
	// TODO	Future Project

	// Department Owned Greetings
	// TODO Future Project

	// Property Owned Greetings

	public static function fetchGreetingByPropertyIdByCidByGreetingTypeId( $intPropertyId, $intCid, $intGreetingTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM greetings WHERE property_id = ' . ( int ) $intPropertyId . ' AND cid = ' . ( int ) $intCid . ' AND greeting_type_id = ' . ( int ) $intGreetingTypeId . ' LIMIT 1';
		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingCountByCidByPropertyIds( $intCid, $arrintPropertyIds, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT COUNT(id) FROM greetings WHERE cid = ' . ( int ) $intCid . ' AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		return ( false == is_null( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : NULL;
	}

	public static function fetchPaginatedActiveGreetings( $objPagination, $objCallFilter, $objVoipDatabase, $boolCountOnly = false ) {
		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$strSqlJoin			= ' JOIN greeting_types AS gt ON ( gt.id = g.greeting_type_id )';
		$strSqlJoin			.= ' LEFT JOIN clients AS c ON ( g.cid = c.id )';
		$strSqlJoin			.= ' LEFT JOIN property_call_settings AS pcs ON ( pcs.cid = g.cid AND pcs.property_id = g.property_id )';

		$arrstrWhere[] = 'g.created_by IS NOT NULL';

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$arrstrWhere[] = ' g.cid  IN ( ' . $objCallFilter->getCids() . ' )';
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			if( true == valStr( $objCallFilter->getGreetingTypeIds() ) ) {
				$arrstrWhere[] = ' ( g.property_id IN( ' . $objCallFilter->getPropertyIds() . ' ) OR g.property_id IS NULL )';
			} else {
				$arrstrWhere[] = ' g.property_id IN( ' . $objCallFilter->getPropertyIds() . ' )';
			}

		}

		if( true == valStr( $objCallFilter->getGreetingTypeIds() ) ) {
			$arrstrWhere[] = ' g.greeting_type_id IN( ' . $objCallFilter->getGreetingTypeIds() . ' )';
		}

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(g.id) FROM greetings AS g ';
		} else {
			$strSqlOrderBy		= ' ORDER BY g.id DESC';
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();

			$strSql = 'SELECT
							g.*,
							gt.name AS greeting_type_name,
							c.company_name AS client_name,
							pcs.property_name
						FROM
							greetings AS g ';
		}

		$strSql		.= $strSqlJoin;
		$strWhere	.= ' WHERE ' . implode( ' AND ', $arrstrWhere );
		$strSql		.= $strWhere;
		$strSql		.= $strSqlOrderBy;
		$strSql		.= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchGreetings( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchGreetingByCallQueueIdByCallFileId( $intCallQueueId, $intCallFileId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM greetings WHERE call_queue_id = ' . ( int ) $intCallQueueId . ' AND call_file_id = ' . ( int ) $intCallFileId;

		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingByCallQueueIdByGreetingTypeId( $intCallQueueId, $intGreetingTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM greetings WHERE call_queue_id = ' . ( int ) $intCallQueueId . ' AND greeting_type_id = ' . ( int ) $intGreetingTypeId;

		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

	public static function fetchDefaultGreetingsByCidByPropertyIdByGreetingTypeIds( $intCid, $intPropertyId, $arrintGreetingTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintGreetingTypeIds ) ) return;

		$strSql = 'SELECT
						sub_query.*
					FROM
						(
						SELECT
							g1.*
						FROM
							greetings AS g1
						WHERE
							g1.cid = ' . ( int ) $intCid . '
							AND g1.property_id = ' . ( int ) $intPropertyId . '
							AND g1.greeting_type_id IN ( ' . implode( ', ', $arrintGreetingTypeIds ) . ' )
						UNION
							SELECT
							g2.*
						FROM
							greetings AS g2
						WHERE
							g2.cid IS NULL
							AND g2.property_id IS NULL
							AND g2.greeting_type_id IN ( ' . implode( ', ', $arrintGreetingTypeIds ) . ' )
					) AS sub_query ORDER BY sub_query.id ASC';

		return self::fetchGreetings( $strSql, $objVoipDatabase );
	}

	public static function fetchDefaultGreetingByGreetingTypeId( $intGreetingTypeId, $objVoipDatabase ) {
		if( false == valId( $intGreetingTypeId ) ) return;

		$strSql = 'SELECT
						*
					FROM
						greetings g
					WHERE
						g.cid IS NULL
						AND g.property_id IS NULL
						AND g.greeting_type_id = ' . ( int ) $intGreetingTypeId;

		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

	public static function fetchDefaultGreetingByGreetingTypeIds( $arrintGreetingTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintGreetingTypeIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						greetings g
					WHERE
						g.cid IS NULL
						AND g.property_id IS NULL
						AND g.greeting_type_id IN ( ' . implode( ',', $arrintGreetingTypeIds ) . ')';

		return self::fetchGreetings( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingByPropertyIdsByCidByGreetingTypeId( $arrintPropertyIds, $intCid, $intGreetingTypeId, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return;
		if( false == valId( $intGreetingTypeId ) ) return;

		$strSql = 'SELECT
						*
					FROM
						greetings g
					WHERE
						g.cid = ' . ( int ) $intCid . '
						AND g.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND g.greeting_type_id = ' . ( int ) $intGreetingTypeId;

		return self::fetchGreetings( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingByCidByPropertyIdByGreetingTypeIds( $intCid, $intPropertyId, $arrintGreetingTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintGreetingTypeIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						greetings g
					WHERE
						g.cid = ' . ( int ) $intCid . '
						AND g.property_id = ' . ( int ) $intPropertyId . '
						AND g.greeting_type_id IN ( ' . implode( ',', $arrintGreetingTypeIds ) . ')';

		return self::fetchGreetings( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingsByCidByGreetingTypeId( $intCid, $intGreetingTypeId, $objVoipDatabase, $arrintUploadedGreetingIds = false ) {
		$strGreetingIds = '';

		if( false == valId( $intGreetingTypeId ) ) return;

		if( true == valArr( $arrintUploadedGreetingIds ) ) {
			$strGreetingIds = ' AND g.id NOT IN ( ' . implode( ',', $arrintUploadedGreetingIds ) . ' ) ';
		}

		$strSql = 'SELECT
						*
					FROM
						greetings g
					WHERE
						g.cid = ' . ( int ) $intCid . '
						AND g.greeting_type_id = ' . ( int ) $intGreetingTypeId .
						$strGreetingIds;

		return self::fetchGreetings( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingByCidByCallFileName( $intCid, $strCallFileName, $objVoipDatabase ) {
		if( false == valId( $intCid ) ) return NULL;
		if( false == valStr( $strCallFileName ) ) return NULL;

		$strSql = 'SELECT
						g.*
					FROM
						greetings AS g
						JOIN call_files AS cf ON ( cf.id = g.call_file_id )
					WHERE
						g.cid = ' . ( int ) $intCid . '
						AND cf.file_name = \'' . $strCallFileName . '\' ';

		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

	public static function fetchGreetingByCidByPropertyIdByGreetingTypeId( $intCid, $intPropertyId, $intGreetingTypeId, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intPropertyId ) || false == valId( $intGreetingTypeId ) ) return NULL;

		$strSql = 'SELECT 
						g.*,
						cf.file_name
					FROM 
						greetings AS g
						JOIN call_files AS cf ON ( g.call_file_id = cf.id )
					WHERE 
						g.cid = ' . ( int ) $intCid . '
						AND g.property_id = ' . ( int ) $intPropertyId . '
						AND g.greeting_type_id = ' . ( int ) $intGreetingTypeId . ' LIMIT 1';

		return self::fetchGreeting( $strSql, $objVoipDatabase );
	}

}
?>