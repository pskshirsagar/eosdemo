<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTranscriptions
 * Do not add any new functions to this class.
 */

class CCallTranscriptions extends CBaseCallTranscriptions {

	public static function fetchCallTranscriptionsByCallIds( $arrintCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallIds ) ) return;

		$strSql = 'SELECT
						*
					FROM
						call_transcriptions
					WHERE
						call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )';

		return self::fetchCallTranscriptions( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTranscriptionByCallId( $intCallId, $objDatabase ) {
		if( false == valId( $intCallId ) ) return;
		$strSql = 'SELECT
						id,
						call_id,
						left_channel,
						right_channel,
						details,
						cast( details->>\'call_is_lead_probability\' AS DOUBLE PRECISION ) * 100 AS lead_probability,
						cast( details->>\'call_is_vendor_probability\' AS DOUBLE PRECISION ) * 100 AS vendor_probability,
						cast( details->>\'call_is_support_probability\' AS DOUBLE PRECISION ) * 100 AS support_probability,
						cast( details->>\'call_is_resident_probability\' AS DOUBLE PRECISION ) * 100 AS resident_probability,
						cast( details->>\'call_is_abandoned_probability\' AS DOUBLE PRECISION ) * 100 AS abandoned_probability,
						cast( details->>\'call_is_solicitor_probability\' AS DOUBLE PRECISION ) * 100 AS solicitor_probability,
						cast( details->>\'call_is_work_order_probability\' AS DOUBLE PRECISION ) * 100 AS work_order_probability,
						cast( details->>\'call_is_wrong_number_probability\' AS DOUBLE PRECISION ) * 100 AS wrong_number_probability,
						cast( details->>\'call_is_market_survey_probability\' AS DOUBLE PRECISION ) * 100 AS market_survey_probability
					FROM
						call_transcriptions
					WHERE
						NULLIF(LOWER(left_channel), \'null\') IS NOT NULL
						AND NULLIF(LOWER(right_channel), \'null\') IS NOT NULL 
						AND call_id = ' . $intCallId;

		return self::fetchCallTranscription( $strSql, $objDatabase );
	}

}
?>