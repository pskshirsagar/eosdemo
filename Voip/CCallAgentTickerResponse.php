<?php

class CCallAgentTickerResponse extends CBaseCallAgentTickerResponse {

	const CONGRATS			= 1;
	const GREAT_JOB			= 2;
	const YOU_ARE_AWESOME	= 3;

	public static $c_arrstrCustomTexts = [
		self::CONGRATS			=> 'Congrats!',
		self::GREAT_JOB			=> 'Great Job!',
		self::YOU_ARE_AWESOME	=> 'You\'re awesome!'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentTickerId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentTickerId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_ticker_id', 'Invalid call agent ticker.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Invalid responder.' ) );
		}

		return $boolIsValid;
	}

	public function valResponse() {
		$boolIsValid = true;

		if( false == valId( $this->getResponse() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'response', 'No response added on the ticker.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$this->valCallAgentTickerId();
				$this->valCallAgentId();
				$this->valResponse();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>