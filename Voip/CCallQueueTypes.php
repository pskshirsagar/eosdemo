<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallQueueTypes
 * Do not add any new functions to this class.
 */

class CCallQueueTypes extends CBaseCallQueueTypes {

	public static function fetchCallQueueTypes( $strSql, $objVoipDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CCallQueueType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallQueueType( $strSql, $objVoipDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CCallQueueType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllCallQueueTypes( $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_queue_types ORDER BY name';

		return self::fetchCallQueueTypes( $strSql, $objVoipDatabase );
	}
}
?>