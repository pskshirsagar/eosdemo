<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallTypeProducts
 * Do not add any new functions to this class.
 */

class CCallTypeProducts extends CBaseCallTypeProducts {

	/**
	 * Fetch Functions
	 */

	public static function fetchPublishedCallTypeProductsByCallTypeId( $intCallTypeId, $objVoipDatabase ) {
		if( false == is_numeric( $intCallTypeId ) ) return NULL;

		$strSql = 'SELECT * FROM call_type_products WHERE is_published = true AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchCallTypeProducts( $strSql, $objVoipDatabase );
	}

	public static function fetchPublishedCallTypeProductsByCallTypeIds( $arrintCallTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_type_products WHERE is_published = true AND call_type_id IN (' . implode( ',', $arrintCallTypeIds ) . ')';

		return self::fetchCallTypeProducts( $strSql, $objVoipDatabase );
	}

}
?>