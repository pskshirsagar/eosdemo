<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPhoneExtensionTypes
 * Do not add any new functions to this class.
 */

class CPhoneExtensionTypes extends CBasePhoneExtensionTypes {

	public static function fetchPhoneExtensionTypes( $strSql, $objVoipDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CPhoneExtensionType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

    public static function fetchPhoneExtensionType( $strSql, $objVoipDatabase ) {
        return parent::fetchCachedObject( $strSql, 'CPhoneExtensionType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
    }

	public static function fetchAllPhoneExtensionTypes( $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						phone_extension_types';

		return self::fetchPhoneExtensionTypes( $strSql, $objVoipDatabase );
	}

}

?>