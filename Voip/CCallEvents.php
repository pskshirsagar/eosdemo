<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallEvents
 * Do not add any new functions to this class.
 */

class CCallEvents extends CBaseCallEvents {

	public static function fetchPaginatedCallEventsByCallFilter( $objPagination, $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ON ( ce.call_id )
						ce.call_id,
						c.call_status_type_id
					FROM
						call_events ce
						JOIN calls AS c ON ( c.id = ce.call_id )';

		$arrstrWhere = array();

		if( true == valStr( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = ' c.id IN ( ' . $objCallFilter->getCallId() . ' ) ';
		}

		if( true == valId( $objCallFilter->getCallStatusTypeId() ) ) {
			$arrstrWhere[] = ' c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId();
		}

		if( false == is_null( $objCallFilter->getStartDate() ) && false == is_null( $objCallFilter->getEndDate() ) && 'Min' != $objCallFilter->getStartDate() && 'Max' != $objCallFilter->getEndDate() ) {
			$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' ' . $objCallFilter->getStartTime() . '\'';
			$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' ' . $objCallFilter->getEndTime() . '\'';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$strSql .= ' LIMIT ' . ( int ) $objPagination->getPageSize() . ' OFFSET ' . ( int ) $objPagination->getOffset();

		$strSql = 'SELECT
						ce.*,
						cst.name AS call_status_type_name
					FROM
						call_events ce
						JOIN ( ' . $strSql . ' ) as calls ON ( calls.call_id = ce.call_id )
						JOIN call_status_types cst ON ( cst.id = calls.call_status_type_id )
					ORDER BY
						ce.id ASC';

		return self::fetchCallEvents( $strSql, $objVoipDatabase );
	}

	public static function fetchTotalCallEventsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		$strSql = 'SELECT
						COUNT( DISTINCT c.id )
					FROM
						call_events ce
						JOIN calls AS c ON ( c.id = ce.call_id )';

		$arrstrWhere = array();

		if( true == valStr( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = ' c.id IN ( ' . $objCallFilter->getCallId() . ' ) ';
		}

		if( true == valId( $objCallFilter->getCallStatusTypeId() ) ) {
			$arrstrWhere[] = ' c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId();
		}

		if( false == is_null( $objCallFilter->getStartDate() ) && false == is_null( $objCallFilter->getEndDate() ) && 'Min' != $objCallFilter->getStartDate() && 'Max' != $objCallFilter->getEndDate() ) {
			$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' ' . $objCallFilter->getStartTime() . '\'';
			$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' ' . $objCallFilter->getEndTime() . '\'';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrmixResponse = fetchData( $strSql, $objVoipDatabase );
		return ( true == valArr( $arrmixResponse ) ) ? $arrmixResponse[0]['count'] : NULL;
	}

	public static function fetchCallEventByCallIdByCallEventTypeId( $intCallId, $intCallEventTypeId, $objVoipDatabase ) {
		return self::fetchCallEvent( 'SELECT * FROM call_events WHERE call_id = ' . ( int ) $intCallId . ' AND call_event_type_id = ' . ( int ) $intCallEventTypeId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchSimpleCallEventByCallIdByCallEventTypeId( $intCallId, $intCallEventTypeId, $objVoipDatabase ) {

		$strSql = 'SELECT
						ROUND( EXTRACT( epoch FROM ( NOW() - created_on ) ) ) as call_duration
					FROM
						call_events
					WHERE
						call_id = ' . ( int ) $intCallId . '
						AND call_event_type_id = ' . ( int ) $intCallEventTypeId;

		return fetchData( $strSql, $objVoipDatabase );

	}

	public static function fetchSimpleCallEventByCallId( $intCallId, $objVoipDatabase ) {

		if( false == valId( $intCallId ) ) return NULL;

		$strSql = 'SELECT
						ce.*,
						EXTRACT ( EPOCH FROM ce.created_on - lag(ce.created_on) over (order by ce.id) ) as time_difference,
						c.call_queue_id,
						c.call_agent_id,
						c.cid,
						c.property_id
					FROM
						call_events ce
						LEFT JOIN calls c ON ( ce.call_id = c.id )
					WHERE
						call_id = ' . $intCallId . '
					ORDER BY ce.id ASC';

		return fetchData( $strSql, $objVoipDatabase );

	}

	public static function fetchCallEventsByCallFunnelFilter( $objCallFunnelFilter, $objVoipDatabase ) {

		if( false == valObj( $objCallFunnelFilter, 'CCallFilter' ) ) return NULL;

		$strSql	= ' WITH temp_data as (
						SELECT
							call_event_type_id,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::STARTING . ' then 1 else NULL END ) AS starting_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::GREETING . ' then 1 else NULL END ) AS greeting_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::ROUTE_TO_PROPERTY . ' then 1 else NULL END ) AS route_to_property_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::ROUTE_TO_VOICEMAIL . ' then 1 else NULL END ) AS route_to_voicemail_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::ROUTE_TO_LEASING_CENTER . ' then 1 else NULL END ) AS route_to_leasing_center_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::IN_QUEUE . ' then 1 else NULL END ) AS in_queue_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::BRIDGE_START . ' then 1 else NULL END ) AS bridge_start_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::BRIDGE_END . ' then 1 else NULL END ) AS bridge_end_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::BRIDGE_FAILED . ' then 1 else NULL END ) AS bridge_failed_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::AGENT_OFFERING . ' then 1 else NULL END ) AS agent_offering_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::CALL_START . ' then 1 else NULL END ) AS call_start_calls_count,
							COUNT( CASE WHEN call_event_type_id = ' . CCallEventType::CALL_STOP . ' then 1 else NULL END ) AS call_stop_calls_count
						FROM
							call_events ce
								LEFT JOIN calls c ON ( c.id = ce.call_id ) ';
		$arrstrWhere = array();

		if( false == is_null( $objCallFunnelFilter->getStartDate() ) && false == is_null( $objCallFunnelFilter->getEndDate() ) ) {
			$arrstrWhere[] = ' ce.created_on between \'' . $objCallFunnelFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFunnelFilter->getEndDate() . ' 23:59:59\'';
		}

		if( true == valId( $objCallFunnelFilter->getCallId() ) ) {
			$arrstrWhere[] = 'call_id = ' . $objCallFunnelFilter->getCallId();
		}

		if( false == is_null( $objCallFunnelFilter->getTableName() ) && 'lc_calls' == $objCallFunnelFilter->getTableName() ) {
			$arrstrWhere[] = 'c.call_agent_id IS NOT NULL';
		} elseif( false == is_null( $objCallFunnelFilter->getTableName() ) && 'property_calls' == $objCallFunnelFilter->getTableName() ) {
			$arrstrWhere[] = 'c.call_agent_id IS NULL';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$strSql .= ' GROUP BY
						call_event_type_id )
					SELECT
						row_to_json ( summary_query.* )
					FROM
						(
							SELECT
								COALESCE( SUM ( starting_calls_count ), 0 ) AS starting_calls_count,
								COALESCE( SUM ( greeting_calls_count ), 0 ) AS greeting_calls_count,
								COALESCE( SUM ( route_to_property_calls_count ), 0 ) AS route_to_property_calls_count,
								COALESCE( SUM ( route_to_voicemail_calls_count ), 0 ) AS route_to_voicemail_calls_count,
								COALESCE( SUM ( route_to_leasing_center_calls_count ), 0 ) AS route_to_leasing_center_calls_count,
								COALESCE( SUM ( in_queue_calls_count ), 0 ) AS in_queue_calls_count,
								COALESCE( SUM ( bridge_start_calls_count ), 0 ) AS bridge_start_calls_count,
								COALESCE( SUM ( bridge_end_calls_count ), 0 ) AS bridge_end_calls_count,
								COALESCE( SUM ( bridge_failed_calls_count ), 0 ) AS bridge_failed_calls_count,
								COALESCE( SUM ( agent_offering_calls_count ), 0 ) AS agent_offering_calls_count,
								COALESCE( SUM ( call_start_calls_count ), 0 ) AS call_start_calls_count,
								COALESCE( SUM ( call_stop_calls_count ), 0 ) AS call_stop_calls_count
							FROM
								temp_data
						) summary_query';

		return fetchData( $strSql, $objVoipDatabase );

	}

}
?>