<?php

class CCallDirection extends CBaseCallDirection {

	const TWILIO_OUTBOUND_DIAL  = 'outbound-dial';
	const TWILIO_INBOUND        = 'inbound';

	const INBOUND = 1;
	const OUTBOUND = 2;

}
?>