<?php

class CIvrMenu extends CBaseIvrMenu {

	protected $m_arrobjIvrMenus;
	protected $m_arrobjIvrMenuActions;

	protected $m_boolIsInvalidSecondaryIvrMenu;

	/**
	* Setter function.
	*/

	public function setIvrMenus( $arrobjIvrMenus ) {
		$this->m_arrobjIvrMenus = $arrobjIvrMenus;
	}

	public function setIvrMenuActions( $arrobjIvrMenuActions ) {
		$this->m_arrobjIvrMenuActions = $arrobjIvrMenuActions;
	}

	public function setIsInvalidSecondaryIvrMenu( $boolIsInvalidSecondaryIvrMenu ) {
		$this->m_boolIsInvalidSecondaryIvrMenu = $boolIsInvalidSecondaryIvrMenu;
	}

	/**
	* Get methods.
	*/

	public function getIvrMenus() {
		return $this->m_arrobjIvrMenus;
	}

	public function getIvrMenuActions() {
		return $this->m_arrobjIvrMenuActions;
	}

	public function getIsInvalidSecondaryIvrMenu() {
		return $this->m_boolIsInvalidSecondaryIvrMenu;
	}

	public function addIvrMenu( $objIvrMenu ) {
		$this->m_arrobjIvrMenu[$objIvrMenu->getId()] = $objIvrMenu;
	}

	public function addIvrMenuAction( $objIvrMenuAction ) {
		$this->m_arrobjIvrMenuActions[$objIvrMenuAction->getId()] = $objIvrMenuAction;
	}

	/**
	* Validate Functions.
	*/

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIvrId() {
		$boolIsValid = true;

		if( false == valId( $this->getIvrId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ivr_id', __( 'Ivr id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIvrMenuTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getIvrMenuTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ivr_menu_type_id', __( 'Ivr menu type id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valIvrMenuId() {
		$boolIsValid = true;

		if( true == valId( $this->getIvrMenuId() ) && true == $this->getIsInvalidSecondaryIvrMenu() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ivr_menu_id', __( 'Please add atleast one option in secondary menu or select different Action 1 option.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Failed to update ivr menu name, please try after some time.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIvrId();
				$boolIsValid &= $this->valIvrMenuTypeId();
				$boolIsValid &= $this->valIvrMenuId();
				$boolIsValid &= $this->valName();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions.
	 */

	public function createIvrMenuAction() {
		$objIvrMenuAction = new CIvrMenuAction();
		$objIvrMenuAction->setIvrMenuId( $this->getId() );
		return $objIvrMenuAction;
	}

	public function getIvrPhraseGreeting() {
		return 'phrase:' . $this->getName();
	}

}
?>