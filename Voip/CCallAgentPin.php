<?php

class CCallAgentPin extends CBaseCallAgentPin {

	protected $m_strCallAgentPinName;
	protected $m_strCallAgentPinImagePath;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_id', 'Call agent is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentPinTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->getCallAgentPinTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_pin_type_id', 'Call agent pin is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSourceCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemark() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallAgentId();
				$boolIsValid &= $this->valCallAgentPinTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getCallAgentPinName() {
		return $this->m_strCallAgentPinName;
	}

	public function getCallAgentPinImagePath() {
		return $this->m_strCallAgentPinImagePath;
	}

	public function setCallAgentPinName( $strCallAgentPinName ) {
		return $this->m_strCallAgentPinName = $strCallAgentPinName;
	}

	public function setCallAgentPinImagePath( $strCallAgentPinImagePath ) {
		return $this->m_strCallAgentPinImagePath = $strCallAgentPinImagePath;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['call_agent_pin_name'] ) )			$this->setCallAgentPinName( $arrmixValues['call_agent_pin_name'] );
		if( true == isset( $arrmixValues['call_agent_pin_image_path'] ) )	$this->setCallAgentPinImagePath( $arrmixValues['call_agent_pin_image_path'] );
		return true;
	}

}

?>