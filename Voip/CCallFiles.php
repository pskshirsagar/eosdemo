<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallFiles
 * Do not add any new functions to this class.
 */

class CCallFiles extends CBaseCallFiles {

	public static function fetchCallFileByPropertyIdByGreetingTypeId( $intPropertyId, $intGreetingTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cf.*
					FROM
						call_files AS cf
						INNER JOIN greetings AS g ON ( g.call_file_id = cf.id )
					WHERE
							g.greeting_type_id = ' . ( int ) $intGreetingTypeId . '
						AND ( g.property_id = ' . ( int ) $intPropertyId . ' OR g.property_id IS NULL )
						ORDER BY cf.id DESC
					LIMIT 1';

		return self::fetchCallFile( $strSql, $objVoipDatabase );
	}

	public static function fetchCallFilesByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cf.*,
						gt.id AS greeting_type_id,
						gt.name AS greeting_type_name
					FROM
						call_files AS cf
						JOIN greetings AS g ON ( g.call_file_id = cf.id )
						JOIN greeting_types AS gt ON ( gt.id = g.greeting_type_id )
					WHERE
						call_queue_id = ' . ( int ) $intCallQueueId;

		return self::fetchCallFiles( $strSql, $objVoipDatabase );
	}

	public static function fetchCallFileByCallQueueIdById( $intCallQueueId, $intCallFileId, $objVoipDatabase ) {
		$strSql = 'SELECT
						cf.*,
						gt.id AS greeting_type_id,
						gt.name AS greeting_type_name
					FROM
						call_files AS cf
						JOIN greetings AS g ON ( g.call_file_id = cf.id )
						JOIN greeting_types AS gt ON ( gt.id = g.greeting_type_id )
					WHERE
						cf.id = ' . ( int ) $intCallFileId . '
						AND call_queue_id = ' . ( int ) $intCallQueueId;

		return self::fetchCallFile( $strSql, $objVoipDatabase );
	}

	public static function fetchCallQueueVoicemailCallFullFilePath( $objVoipDatabase ) {
		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
					JOIN greetings AS g ON ( g.call_file_id = cf.id )
				WHERE
						g.greeting_type_id = ' . CGreetingType::CALL_QUEUE_VOICEMAIL . '
					AND g.property_id IS NULL
				ORDER BY cf.id DESC LIMIT 1';

		$objCallFile = self::fetchCallFile( $strSql, $objVoipDatabase );

		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : '';
	}

	public static function fetchStandardGreetingCallFullFilePath( $objVoipDatabase ) {
		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
					JOIN greetings AS g ON ( g.call_file_id = cf.id )
				WHERE
						g.greeting_type_id = ' . CGreetingType::PROPERTY_LEAD . '
					AND g.property_id IS NULL
				ORDER BY cf.id DESC LIMIT 1';

		$objCallFile = self::fetchCallFile( $strSql, $objVoipDatabase );

		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : '';
	}

	public static function fetchRecordingMessageGreetingCallFullFilePath( $objVoipDatabase ) {
		$strSql = 'SELECT
						cf.*
					FROM
						call_files AS cf
						JOIN greetings AS g ON ( g.call_file_id = cf.id )
					WHERE
							g.greeting_type_id = ' . CGreetingType::RECORDING_MESSAGE . '
						AND g.property_id IS NULL
					ORDER BY cf.id DESC LIMIT 1';

		$objCallFile = self::fetchCallFile( $strSql, $objVoipDatabase );

		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : '';
	}

	public static function fetchCallQueueVoicemailCallFullFilePathByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
					JOIN greetings AS g ON ( g.call_file_id = cf.id )
				WHERE
						g.greeting_type_id = ' . CGreetingType::CALL_QUEUE_VOICEMAIL . '
					AND ( g.call_queue_id = ' . ( int ) $intCallQueueId . ' OR g.call_queue_id IS NULL )
					AND g.property_id IS NULL
				ORDER BY cf.id DESC LIMIT 1';

		$objCallFile = self::fetchCallFile( $strSql, $objVoipDatabase );

		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : '';
	}

	public static function fetchStandardGreetingCallFullFilePathByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
					JOIN greetings AS g ON ( g.call_file_id = cf.id )
				WHERE
						g.greeting_type_id = ' . CGreetingType::PROPERTY_LEAD . '
					AND ( g.call_queue_id = ' . ( int ) $intCallQueueId . ' OR g.call_queue_id IS NULL )
					AND g.property_id IS NULL
				ORDER BY cf.id DESC LIMIT 1';

		$objCallFile = self::fetchCallFile( $strSql, $objVoipDatabase );

		return ( true == valObj( $objCallFile, 'CCallFile' ) ) ? $objCallFile->getFullFilePath() : '';
	}

	public static function fetchCallFilesByIds( $arrintCallFileIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallFileIds ) ) return;

		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
				WHERE cf.id IN ( ' . implode( ',', $arrintCallFileIds ) . ' )	';

		return self::fetchCallFiles( $strSql, $objVoipDatabase );
	}

	public static function fetchPropertyGreetingsCallFilesByCid( $intCid, $objVoipDatabase ) {
		if( false == valId( $intCid ) ) return;

		$strSql = 'SELECT
						cf.*,
						g.cid AS client_id
					FROM
						call_files AS cf
						JOIN greetings AS g ON ( g.call_file_id = cf.id )
					WHERE
						g.property_id IS NOT NULL
						AND	cf.file_extension_id =' . CFileExtension::WAV . '
						AND	g.cid =' . ( int ) $intCid . '
					ORDER BY
						cf.id DESC';

		return self::fetchCallFiles( $strSql, $objVoipDatabase );
	}

	public static function fetchCallFileByGreetingTypeId( $intGreetingTypeId, $objVoipDatabase ) {
		$strSql = 'SELECT
					cf.*
				FROM
					call_files AS cf
					JOIN greetings AS g ON ( g.call_file_id = cf.id )
				WHERE
					g.greeting_type_id = ' . ( int ) $intGreetingTypeId . '
					AND g.call_queue_id IS NULL
					AND g.property_id IS NULL
					AND g.cid IS NULL';

		return self::fetchCallFile( $strSql, $objVoipDatabase );
	}

	public static function fetchCallFileByCidByGreetingId( $intCid, $intGreetingId, $objVoipDatabase ) {
		if( false == valId( $intCid ) ) return;
		if( false == valId( $intGreetingId ) ) return;

		$strSql = 'SELECT
						cf.*
					FROM
						call_files AS cf
						JOIN greetings AS g ON ( g.call_file_id = cf.id )
					WHERE
						g.cid =' . ( int ) $intCid . '
						AND g.id = ' . ( int ) $intGreetingId;

		return self::fetchCallFile( $strSql, $objVoipDatabase );
	}
}
?>