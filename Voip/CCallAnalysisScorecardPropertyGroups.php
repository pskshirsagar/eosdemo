<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisScorecardPropertyGroups
 * Do not add any new functions to this class.
 */

class CCallAnalysisScorecardPropertyGroups extends CBaseCallAnalysisScorecardPropertyGroups {

	public static function fetchCallAnalysisScorecardPropertyGroupsByCidByCallAnalysisScorecardIdByPropertyGroupIds( $intCid, $intCallAnalysisScorecardId, $arrintPropertyGroupIds, $objVoipDatabase ) {
		if( false == valId( $intCallAnalysisScorecardId ) || false == valArr( $arrintPropertyGroupIds ) ) return NULL;
		$strSql = 'SELECT * FROM call_analysis_scorecard_property_groups WHERE cid = ' . ( int ) $intCid . ' AND call_analysis_scorecard_id = ' . ( int ) $intCallAnalysisScorecardId . ' AND property_group_id IN ( ' . implode( ',', $arrintPropertyGroupIds ) . ')';
		return parent::fetchCallAnalysisScorecardPropertyGroups( sprintf( $strSql ), $objVoipDatabase );
	}

}
?>