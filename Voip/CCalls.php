<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCalls
 * Do not add any new functions to this class.
 */

class CCalls extends CBaseCalls {

	public static $c_arrintMarketingPhoneExtensionIds		= [ 1865, 6245 ];
	public static $c_arrintResidentPhoneExtensionIds		= [ 1762, 6255, 6259 ];
	public static $c_arrintLeasingPhoneExtensionIds			= [ 2490, 6250 ];
	public static $c_arrintCorePhoneExtensionIds			= [ 6238, 6274, 4431, 6263, 6265 ];
	public static $c_arrintResidentVerifyPhoneExtensionIds	= [ 6248, 6251, 6257 ];
	public static $c_arrintInternationalPhoneExtensionIds	= [ 6970, 6971, 6972, 6973, 6974 ];
	public static $c_arrintHelpdeskPhoneExtensionIds		= [ 6269, 6270, 6271, 6272, 6273 ];
	public static $c_arrintGeneralPhoneExtensionIds			= [ 6070, 6170 ];
	public static $c_arrintAllPhoneExtensionIds				= [ 6238, 6274, 4431, 6263, 6265, 1865, 6245, 2490, 6250, 1762, 6255, 6259, 6970, 6971, 6972, 6973, 6974, 6248, 6251, 6257, 6269, 6270, 6271, 6272, 6273 ];

	const RESIDENT_PHONE_EXTENSION_ID			= 6171;
	const MARKETING_PHONE_EXTENSION_ID			= 6172;
	const LEASING_PHONE_EXTENSION_ID			= 6173;
	const CORE_PHONE_EXTENSION_ID				= 6174;
	const RESIDENT_VERIFY_PHONE_EXTENSION_ID	= 6135;
	const GENERAL_PHONE_EXTENSION_ID			= 6070;
	const CALL_DURATION_SECONDS					= 45;

	public static function fetchCallByCidById( $intCid, $intCallId, $objVoipDatabase ) {
		return self::fetchCall( 'SELECT * from calls WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intCallId, $objVoipDatabase );
	}

	public static function fetchCallInfoByCidById( $intCid, $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT
						c.*,
						ct.name AS call_type_name,
						cst.name AS call_status_type_name
					FROM
						calls c
						JOIN call_types ct ON ( c.call_type_id = ct.id )
						JOIN call_status_types cst ON ( c.call_status_type_id = cst.id )
					WHERE
						cid = ' . ( int ) $intCid . '
						AND c.id = ' . ( int ) $intCallId;

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchQueuedCallsByMaxAttempts( $intMaxAttempts, $objVoipDatabase, $strDestinationPhoneNumber = NULL ) {
		if( false == is_null( $strDestinationPhoneNumber ) ) {
			$strCondition = 'AND destination_phone_number=\'' . addslashes( $strDestinationPhoneNumber ) . '\'';
		}

		$strSql = 'SELECT
						*
					FROM
						calls
					WHERE
						call_status_type_id != ' . ( int ) CCallStatusType::CALLING . '
						AND	call_status_type_id != ' . ( int ) CCallStatusType::ANSWERED . '
						AND (
								( call_today = 1 AND date_trunc(\'day\', NOW()) = date_trunc(\'day\', scheduled_send_datetime) )
									OR
								( call_today = 0 AND date_trunc(\'day\', NOW()) >= date_trunc(\'day\', scheduled_send_datetime) )
						)
						AND CURRENT_TIME >= begin_send_time
						AND CURRENT_TIME <= end_send_time
						AND (
								( sent_on IS NULL AND failed_on IS NULL )
									OR
								( sent_on IS NULL AND failed_on IS NOT NULL AND attempt_count < ' . ( int ) $intMaxAttempts . ' AND NOW() > ( failed_on + INTERVAL \'1 hour\' ) )
						)
						AND is_test_call <> 1
						AND call_type_id = ' . CCallType::OUTBOUND_PROPERTY_SCHEDULED_CALL . '
						AND scheduled_call_id IS NOT NULL
						' . $strCondition . '
					ORDER BY
						call_priority_id DESC,
						scheduled_send_datetime ASC LIMIT 80';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByIds( $arrintCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallIds ) ) return NULL;

		$strSql = 'SELECT 
						c.*, 
						cr.name AS call_result_name 
					FROM 
						calls AS c 
						LEFT JOIN call_results AS cr ON( c.call_result_id = cr.id ) 
					WHERE 
						c.id IN( ' . implode( ',', $arrintCallIds ) . ' )';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByCidByPropertyIdsByIds( $intCid, $arrintPropertyIds, $arrintCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintCallIds ) ) return NULL;
		if( false == valArr( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						calls
					WHERE
						( property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							OR redirect_property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) )
						AND cid = ' . ( int ) $intCid . '
						AND id IN ( ' . implode( ',', $arrintCallIds ) . ' )';
		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallsByCidByPropertyIdsByCallFilter( $intCid, $arrintPropertyIds, $objCallsFilter, $objVoipDatabase ) {

		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;
		// For Drill-in from Report
		$strPhoneNumberSql	= '';
		$strCallSettingsSql = 'LEFT JOIN property_call_settings pcs ON pcs.property_id = c.property_id AND pcs.cid = c.cid';
		$strWhereSql		= '';
		$arrstrWhere = [];
		$strReportColumnName = $objCallsFilter->getReportColumnName();

		if( false == is_null( $objCallsFilter->getPageNo() ) && false == is_null( $objCallsFilter->getCountPerPage() ) ) {
			$intOffset	= ( 0 < $objCallsFilter->getPageNo() ) ? $objCallsFilter->getCountPerPage() * ( $objCallsFilter->getPageNo() - 1 ) : 0;
			$intLimit	= ( int ) $objCallsFilter->getCountPerPage();
		}

		if( ( false == is_null( $objCallsFilter->getGenericData() ) ) && ( '' != trim( $objCallsFilter->getGenericData() ) ) && ( true == preg_match( '/[0-9]/', $objCallsFilter->getGenericData() ) ) ) {
			$strGenericData			= trim( $objCallsFilter->getGenericData() );
			$strGenericDataPhone	= preg_replace( '/[^A-Za-z0-9]/', '', $strGenericData );
			$arrstrWhere[]			= '( CAST( c.id AS text ) LIKE \'%' . addslashes( $strGenericData ) . '%\' OR regexp_replace(c.origin_phone_number, \'[^0-9]+\', \'\', \'g\') LIKE \'%' . addslashes( $strGenericDataPhone ) . '%\' OR regexp_replace(c.destination_phone_number, \'[^0-9]+\', \'\', \'g\') LIKE \'%' . addslashes( $strGenericDataPhone ) . '%\' ) ';
		}

		if( ( '' == trim( $objCallsFilter->getGenericData() ) ) && 'yes' == $objCallsFilter->getShowVanity() ) {
			$arrstrWhere[] = 'c.call_phone_number_id IS NULL';
		}

		if( true == is_null( $objCallsFilter->getPageNo() ) && true == is_null( $objCallsFilter->getCountPerPage() ) && false == is_null( $objCallsFilter->getLeadIds() ) ) {
			$arrstrWhere[]	= 'c.id IN ( ' . $objCallsFilter->getLeadIds() . ' ) ';
		}

		if( true == is_numeric( $objCallsFilter->getCallId() ) ) {
			$arrstrWhere[]	= 'c.id = ' . ( int ) $objCallsFilter->getCallId();
		}

		if( false == is_null( $objCallsFilter->getEmployeeId() ) && true == is_numeric( $objCallsFilter->getEmployeeId() ) ) {
			$arrstrWhere[]	= 'c.employee_id = ' . ( int ) $objCallsFilter->getEmployeeId();
		}

		if( ( false == is_null( $objCallsFilter->getExcludeEmployeeId() ) ) && ( '' != trim( $objCallsFilter->getExcludeEmployeeId() ) ) ) {
			$arrstrWhere[] = '( c.employee_id NOT IN ( ' . $objCallsFilter->getExcludeEmployeeId() . ' ) OR c.employee_id IS NULL ) ';
		}

		if( ( false == is_null( $objCallsFilter->getPropertyIds() ) ) && ( '' != trim( $objCallsFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . $objCallsFilter->getPropertyIds() . ' )';
		} else {
			$arrstrWhere[] = '( c.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) OR c.property_id IS NULL )';
		}

		if( true == valStr( $objCallsFilter->getCompanyEmployeeIds() ) ) {
			$arrstrWhere[] = 'c.company_employee_id IN ( ' . $objCallsFilter->getCompanyEmployeeIds() . ' )';
		}

		if( ( false == is_null( $objCallsFilter->getCallTypeIds() ) ) && ( '' != trim( $objCallsFilter->getCallTypeIds() ) ) ) {
			$arrstrWhere[] = 'c.call_type_id IN ( ' . $objCallsFilter->getCallTypeIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getCallResultIds() ) ) && ( '' != trim( $objCallsFilter->getCallResultIds() ) ) ) {
			$arrstrWhere[] = 'c.call_result_id IN ( ' . $objCallsFilter->getCallResultIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getLeadSourceIds() ) ) && ( '' != trim( $objCallsFilter->getLeadSourceIds() ) ) ) {
			if( '0' == $objCallsFilter->getLeadSourceIds() ) {
				$arrstrWhere[] = 'c.lead_source_id IS NULL ';
			} else {
				if( false === \Psi\CStringService::singleton()->strpos( $objCallsFilter->getLeadSourceIds(), '-1' ) ) {
					$arrstrWhere[] = 'c.lead_source_id IN ( ' . $objCallsFilter->getLeadSourceIds() . ' ) ';
				} else {
					$arrstrWhere[] = '( c.lead_source_id IN ( ' . $objCallsFilter->getLeadSourceIds() . ' ) OR c.lead_source_id IS NULL )';
				}
			}
		}

		if( ( false == is_null( $objCallsFilter->getAnswerStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getAnswerStatusTypeIds() ) ) ) {
			if( CCallFilter::CALL_RESULT_TYPE_UNANSWERED === $objCallsFilter->getAnswerStatusTypeIds() ) {
				$arrstrWhere[] = '( c.call_status_type_id IN ( ' . CCallStatusType::UNANSWERED . ' ) OR c.call_status_type_id IS NULL )';
			}
			if( CCallFilter::CALL_RESULT_TYPE_ANSWERED === $objCallsFilter->getAnswerStatusTypeIds() ) {
				$arrstrWhere[] = 'c.call_status_type_id IN ( ' . CCallStatusType::ANSWERED . ' ) AND c.call_status_type_id IS NOT NULL ';
			}
		}

		if( ( false == is_null( $objCallsFilter->getVoicemailStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getVoicemailStatusTypeIds() ) ) ) {
			if( CCallFilter::VOICE_MAIL_TYPE_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 1 ';
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT;
			}

			if( CCallFilter::VOICE_MAIL_TYPE_UNPLAYED === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 1 AND c.voice_mail_reviewed_on IS NULL';
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT;
			}

			if( CCallFilter::VOICE_MAIL_TYPE_NO_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 0';
			}
		}

		if( false == is_null( trim( $objCallsFilter->getCallIds() ) ) && '' != $objCallsFilter->getCallIds() ) {
			$arrstrWhere[] = 'c.id IN ( ' . $objCallsFilter->getCallIds() . ' ) ';
		}

		if( false == is_null( $objCallsFilter->getApplicantApplicationIds() ) ) {
			$arrstrWhere[] = 'c.applicant_application_id IN ( ' . $objCallsFilter->getApplicantApplicationIds() . ' ) ';
		}

		if( CCallFilter::CALL_ANALYZED_YES == $objCallsFilter->getCallAnalyzed() ) {
			$arrstrWhere[] = 'c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED;
			$arrstrWhere[] = 'c.analyzed_on IS NOT NULL';
		} elseif( CCallFilter::CALL_ANALYZED_NO == $objCallsFilter->getCallAnalyzed() ) {
			$arrstrWhere[] = 'c.call_analysis_status_type_id IS DISTINCT FROM ' . CCallAnalysisStatusType::ANALYZED;
			$arrstrWhere[] = 'c.analyzed_on IS NULL';
		}

		$boolTimeZoneCondition = true;
		if( true == isset( $strReportColumnName ) ) {

			$strCallPeriod = $objCallsFilter->getReportCallPeriod();
			$arrmixReportCondition = self::getReportConditionsByReportColumnName( $strReportColumnName, $strCallPeriod );
			if( ( $arrmixReportCondition[0] ) ) {
				$arrstrWhere[] = $arrmixReportCondition[0];
			}
			$boolTimeZoneCondition = $arrmixReportCondition[1];
		}

		if( false == is_null( $objCallsFilter->getStartDate() ) && false == is_null( $objCallsFilter->getEndDate() ) && 'Min' != $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
			$intStartDate = strtotime( $objCallsFilter->getStartDate() );
			$intEndDate = strtotime( $objCallsFilter->getEndDate() );
			if( $intStartDate <= $intEndDate ) {
				if( true == valStr( $strReportColumnName ) && true == $boolTimeZoneCondition ) {
					$arrstrWhere[] = 'c.call_datetime AT TIME ZONE tz.time_zone_name BETWEEN \'' . date( 'Y-m-d', $intStartDate ) . ' ' . $objCallsFilter->getStartTime() . '\' AND \'' . date( 'Y-m-d', $intEndDate ) . ' ' . $objCallsFilter->getEndTime() . '\'';
				} else {
					$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', $intStartDate ) . ' ' . $objCallsFilter->getStartTime() . '\' AND \'' . date( 'Y-m-d', $intEndDate ) . ' ' . $objCallsFilter->getEndTime() . '\'';
				}

			} elseif( false == is_null( $objCallsFilter->getPageNo() ) && false == is_null( $objCallsFilter->getCountPerPage() ) ) {

				$objCallsFilter->validate();

				$objCallsFilter->changeDateFormat();
			}
		}

		$strDistinctCaller = '';
		if( true == $objCallsFilter->getReportDistinctLeads() ) {
			$strDistinctCaller	= ', ROW_NUMBER() OVER( PARTITION BY c.origin_phone_number, c.lead_source_id, c.property_id ORDER BY c.origin_phone_number ) AS rank';
		}

		$strSql = ' SELECT
						c.id,
						c.cid,
						c.call_phone_number_id,
						c.call_type_id,
						c.call_file_id,
						c.call_status_type_id,
						c.call_response_type_id,
						c.call_result_id,
						c.voice_type_id,
						c.origin_phone_number,
						c.destination_phone_number,
						c.origin_extension,
						c.call_datetime,
						c.call_notes,
						c.is_voice_mail,
						c.property_id,
						c.lead_source_id,
						c.call_duration_seconds,
						c.call_center_talk_time,
						c.wrap_up_time,
						c.created_on,
						c.customer_id,
						c.applicant_application_id,
						c.deleted_on,
						c.voice_mail_reviewed_on,
						c.analyzed_by,
						c.analyzed_on,
						c.details->\'leasing_agent_name\' AS leasing_agent_name,
						cpn.ivr_id
						' . $strDistinctCaller . '
					FROM
						calls c
						' . $strCallSettingsSql . $strPhoneNumberSql . '
						LEFT JOIN call_phone_numbers cpn ON cpn.id = c.call_phone_number_id
						LEFT JOIN time_zones tz ON tz.id = pcs.time_zone_id
					 WHERE
						c.cid = ' . ( int ) $intCid;

		if( CCallFilter::CALL_TRACKER_CURRENT_CALLS == $objCallsFilter->getCallsState() ) {
			if( true == valStr( $strReportColumnName ) ) {
				$strSql .= ' AND ( c.deleted_on IS NULL OR c.deleted_on > \'' . $objCallsFilter->getEndDate() . '\' ) ';
			} else {
				$strSql .= ' AND c.deleted_on IS NULL ';
			}
		}

		if( CCallFilter::CALL_TRACKER_ARCHIVED_CALLS == $objCallsFilter->getCallsState() ) {
			$strSql .= ' AND c.deleted_on IS NOT NULL ';
		}

		$strSql .= 'AND c.is_test_call = 0
				 	AND c.is_auto_detected = 0
					AND c.call_result_id IS DISTINCT FROM ' . CCallResult::SIP_TEST_CALL;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$arrstrOrderBy = [
			'call_id'			=> 'c.id',
			'call_type'			=> 'c.call_type_id',
			'status'			=> 'c.call_status_type_id',
			'property'			=> 'c.property_id',
			'duration'			=> 'c.call_duration_seconds',
			'time_to_listen'	=> 'c.call_datetime',
			'call_datetime'		=> 'c.call_datetime',
			'source'			=> 'c.lead_source_id',
			'phone_number'		=> 'c.origin_phone_number'
		];

		$strOrderBy = ( true == isset( $arrstrOrderBy[$objCallsFilter->getSortBy()] ) ? $arrstrOrderBy[$objCallsFilter->getSortBy()] : 'c.id' );

		if( true == $objCallsFilter->getReportDistinctLeads() ) {
			$strSql = ' SELECT
							*
						FROM
							( '
					  . $strSql .
					  ') AS sub_query
						WHERE
							sub_query.cid = ' . ( int ) $intCid . '
							AND sub_query.rank = 1';

			$arrstrOrderBy = [
				'call_id'			=> 'sub_query.id',
				'call_type'			=> 'sub_query.call_type_id',
				'status'			=> 'sub_query.call_status_type_id',
				'property'			=> 'sub_query.property_id',
				'duration'			=> 'sub_query.call_duration_seconds',
				'time_to_listen'	=> 'sub_query.call_datetime',
				'call_datetime'		=> 'sub_query.call_datetime',
				'source'			=> 'sub_query.lead_source_id',
				'phone_number'		=> 'sub_query.origin_phone_number'
			];

			$strOrderBy = ( true == isset( $arrstrOrderBy[$objCallsFilter->getSortBy()] ) ? $arrstrOrderBy[$objCallsFilter->getSortBy()] : 'sub_query.id' );
		}

		$strSortDirection = ( false == is_null( $objCallsFilter->getSortDirection() ) ) ? $objCallsFilter->getSortDirection() : 'DESC';

		if( false == is_null( $strOrderBy ) ) {
			$strSql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;
		}

		if( false == is_null( $objCallsFilter->getPageNo() ) && false == is_null( $objCallsFilter->getCountPerPage() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}
		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function getReportConditionsByReportColumnName( $strReportColumnName, $strCallPeriod ) : array {
		$strColumnWhereCondition = '';
		$boolTimeZoneCondition = true;
		if( true == isset( $strReportColumnName ) ) {
			switch( $strReportColumnName ) {
				case 'greeting_abandoned':
					$strColumnWhereCondition = '( c.call_center_queue_time <= 30 OR c.call_center_queue_time IS NULL ) AND c.call_type_id NOT IN ( ' . CCallType::CALL_TRACKER_LEAD_VIA_IVR . ', ' . CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR . ', ' . CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR . ' ) ';
					break;

				case 'ivr_abandoned':
					$strColumnWhereCondition = '( c.call_center_queue_time <= 30 OR c.call_center_queue_time IS NULL )';
					break;

				case 'transition_abandoned':
					$strColumnWhereCondition = '( c.call_center_queue_time = 0 OR c.call_center_queue_time IS NULL )';
					break;

				case 'leasing_center_unanswered':
				case 'unanswered_onsite':
				case 'unanswered_calls':
					$strColumnWhereCondition = 'c.call_center_queue_time > 30';
					break;

				case 'abandoned_calls':
				case 'leasing_center_abandoned':
				$strColumnWhereCondition = '( c.call_center_queue_time > 0 AND c.call_center_queue_time <= 30 )';
					break;

				case 'on_site_answered_after_business_hours_count':
				case 'on_site_answered_during_business_hours_count':
				case 'lc_total_answered_after_business_hours_count':
				case 'lc_total_answered_during_business_hours_count':

				$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED;
					break;

				case 'all_lead_calls':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND ( c.call_analysis_status_type_id IN (
						' . CCallAnalysisStatusType::UNEVALUATED . ',
						' . CCallAnalysisStatusType::EVALUATED . ',
						' . CCallAnalysisStatusType::ANALYZED . ',
						' . CCallAnalysisStatusType::NON_LEAD_CALL . ',
						' . CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ',
						' . CCallAnalysisStatusType::WARM_LEAD . ',
						' . CCallAnalysisStatusType::EARLY_TERMINATION . ',
						' . CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ',
						' . CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ',
						' . CCallAnalysisStatusType::VOICEMAIL . '
					) OR c.call_analysis_status_type_id IS NULL ) AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )
					';
					break;

				case 'evaluated_calls':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id IN (
						' . CCallAnalysisStatusType::EVALUATED . ',
						' . CCallAnalysisStatusType::ANALYZED . ',
						' . CCallAnalysisStatusType::NON_LEAD_CALL . ',
						' . CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ',
						' . CCallAnalysisStatusType::WARM_LEAD . ',
						' . CCallAnalysisStatusType::EARLY_TERMINATION . ',
						' . CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ',
						' . CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ',
						' . CCallAnalysisStatusType::VOICEMAIL . '
					) AND c.created_by = ' . CUser::ID_SYSTEM . '
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'unevaluated_calls':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND ( c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::UNEVALUATED . ' OR c.call_analysis_status_type_id IS NULL ) AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . '
					AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'analyzed_calls':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'non_lead':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::NON_LEAD_CALL . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'disqualified_lead':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'warm_lead':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::WARM_LEAD . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' ) ';
					break;

				case 'early_termination':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::EARLY_TERMINATION . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' ) ';
					break;

				case 'voicemail':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::VOICEMAIL . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' ) ';
					break;

				case 'flagged_for_analysis':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'max_calls_evaluated':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' AND c.created_by = ' . CUser::ID_SYSTEM . ' 
					AND c.call_duration_seconds >= ' . self::CALL_DURATION_SECONDS . ' AND c.call_file_id IS NOT NULL AND c.call_result_id NOT IN ( ' . CCallResult::SIP_TEST_CALL . ', ' . CCallResult::TEST_CALL . ' )';
					break;

				case 'mon':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'mon\'';
					break;

				case 'tue':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'tue\'';
					break;

				case 'wed':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'wed\'';
					break;

				case 'thu':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'thu\'';
					break;

				case 'fri':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'fri\'';
					break;

				case 'sat':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'sat\'';
					break;

				case 'sun':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'dy\' ) = \'sun\'';
					break;

				case 'hour_00':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'00\'';
					break;

				case 'hour_01':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'01\'';
					break;

				case 'hour_02':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'02\'';
					break;

				case 'hour_03':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'03\'';
					break;

				case 'hour_04':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'04\'';
					break;

				case 'hour_05':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'05\'';
					break;

				case 'hour_06':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'06\'';
					break;

				case 'hour_07':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'07\'';
					break;

				case 'hour_08':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'08\'';
					break;

				case 'hour_09':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'09\'';
					break;

				case 'hour_10':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'10\'';
					break;

				case 'hour_11':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'11\'';
					break;

				case 'hour_12':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'12\'';
					break;

				case 'hour_13':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'13\'';
					break;

				case 'hour_14':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'14\'';
					break;

				case 'hour_15':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'15\'';
					break;

				case 'hour_16':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'16\'';
					break;

				case 'hour_17':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'17\'';
					break;

				case 'hour_18':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'18\'';
					break;

				case 'hour_19':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'19\'';
					break;

				case 'hour_20':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'20\'';
					break;

				case 'hour_21':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'21\'';
					break;

				case 'hour_22':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'22\'';
					break;

				case 'hour_23':
					$boolTimeZoneCondition = false;
					$strColumnWhereCondition = 'TO_CHAR( c.call_datetime, \'HH24\' ) = \'23\'';
					break;

				default:
					// Default Block
					break;
			}
			if( false == empty( $strCallPeriod ) ) {

				$strHoursElse = 'TRUE';
				$strHoursSql = $strAndCondition = '';
				$arrmixDaysofWeek 	= [ '0' => 'sun', '1' => 'mon', '2' => 'tue', '3' => 'wed', '4' => 'thu', '5' => 'fri', '6' => 'sat' ];
				$strCallSettingsSql = 'JOIN property_call_settings pcs ON ( pcs.cid = c.cid AND pcs.property_id = COALESCE( c.property_id, c.redirect_property_id ) )';

				foreach( $arrmixDaysofWeek as $intDay => $strDay ) {
					if( 'during_office_hours' == $strCallPeriod ) {
						$strHoursElse = 'FALSE';
						$strHoursSql .= ' WHEN extract( DOW FROM c.call_datetime AT TIME ZONE tz.time_zone_name ) = ' . ( int ) $intDay . '
										 AND ( ( c.call_datetime AT TIME ZONE tz.time_zone_name )::TIME BETWEEN pcs.' . $strDay . '_open_time::TIME AND pcs.' . $strDay . '_close_time::TIME ) THEN TRUE';
					} elseif( 'after_office_hours' == $strCallPeriod ) {
						$strHoursSql .= ' WHEN extract( DOW FROM c.call_datetime AT TIME ZONE tz.time_zone_name ) = ' . ( int ) $intDay . '
										 AND ( ( c.call_datetime AT TIME ZONE tz.time_zone_name )::TIME BETWEEN pcs.' . $strDay . '_open_time::TIME AND pcs.' . $strDay . '_close_time::TIME ) THEN FALSE';
					}
				}

				if( false == valStr( $strHoursSql ) ) {
					$strHoursSql = 'WHEN 1 = 1 THEN TRUE';
				}

				if( valStr( $strColumnWhereCondition ) ) {
					$strAndCondition = ' AND ';
				}

				$strCallPeriodWhereCondition = $strAndCondition . '( CASE 
													' . $strHoursSql . '
													ELSE
														' . $strHoursElse . '
												END )';
			}
		}

		return [ $strColumnWhereCondition . $strCallPeriodWhereCondition, $boolTimeZoneCondition ];
	}

	public static function fetchTotalCallsCountByCidByPropertyIdsByCallFilter( $intCid, $arrintPropertyIds, $objCallsFilter, $objVoipDatabase ) {

		if( false == isset( $arrintPropertyIds ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrintPropertyIds ) ) return NULL;

		$arrstrWhere = [];
		$strPhoneNumberSql	= '';
		$strCallSettingsSql = 'LEFT JOIN property_call_settings pcs ON pcs.property_id = c.property_id AND pcs.cid = c.cid';
		$strReportColumnName = $objCallsFilter->getReportColumnName();

		if( ( false == is_null( $objCallsFilter->getGenericData() ) ) && ( '' != trim( $objCallsFilter->getGenericData() ) ) && ( true == preg_match( '/[0-9]/', $objCallsFilter->getGenericData() ) ) ) {
			$strGenericData	= trim( $objCallsFilter->getGenericData() );
			$strGenericDataPhone = preg_replace( '/[^A-Za-z0-9]/', '', $strGenericData );
			$arrstrWhere[] = '( CAST( c.id AS text ) LIKE \'%' . addslashes( $strGenericData ) . '%\' OR regexp_replace(c.origin_phone_number, \'[^0-9]+\', \'\', \'g\') LIKE \'%' . addslashes( $strGenericDataPhone ) . '%\' OR regexp_replace(c.destination_phone_number, \'[^0-9]+\', \'\', \'g\') LIKE \'%' . addslashes( $strGenericDataPhone ) . '%\' ) ';
		}

		if( ( '' == trim( $objCallsFilter->getGenericData() ) ) && 'yes' == $objCallsFilter->getShowVanity() ) {
			$arrstrWhere[] = 'c.call_phone_number_id IS NULL';
		}

		if( true == is_null( $objCallsFilter->getPageNo() ) && true == is_null( $objCallsFilter->getCountPerPage() ) && false == is_null( $objCallsFilter->getLeadIds() ) ) {
			$arrstrWhere[] = 'c.id IN ( ' . $objCallsFilter->getLeadIds() . ' ) ';
		}

		if( true == is_numeric( $objCallsFilter->getCallId() ) ) {
			$arrstrWhere[] = 'c.id = ' . ( int ) $objCallsFilter->getCallId();
		}

		if( false == is_null( $objCallsFilter->getEmployeeId() ) && true == is_numeric( $objCallsFilter->getEmployeeId() ) ) {
			$arrstrWhere[] = 'c.employee_id = ' . ( int ) $objCallsFilter->getEmployeeId();
		}

		if( ( false == is_null( $objCallsFilter->getExcludeEmployeeId() ) ) && ( '' != trim( $objCallsFilter->getExcludeEmployeeId() ) ) ) {
			$arrstrWhere[] = '( c.employee_id NOT IN ( ' . $objCallsFilter->getExcludeEmployeeId() . ' ) OR c.employee_id IS NULL ) ';
		}

		if( ( false == is_null( $objCallsFilter->getPropertyIds() ) ) && ( '' != trim( $objCallsFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . $objCallsFilter->getPropertyIds() . ' ) ';
		} else {
			$arrstrWhere[] = '( c.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) OR c.property_id IS NULL )';
		}

		if( true == valStr( $objCallsFilter->getCompanyEmployeeIds() ) ) {
			$arrstrWhere[] = 'c.company_employee_id IN ( ' . $objCallsFilter->getCompanyEmployeeIds() . ' )';
		}

		if( ( false == is_null( $objCallsFilter->getAnswerStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getAnswerStatusTypeIds() ) ) ) {
			if( CCallFilter::CALL_RESULT_TYPE_UNANSWERED === $objCallsFilter->getAnswerStatusTypeIds() ) {
				$arrstrWhere[] = '( c.call_status_type_id IN ( ' . CCallStatusType::UNANSWERED . ' ) OR c.call_status_type_id IS NULL )';
			}
			if( CCallFilter::CALL_RESULT_TYPE_ANSWERED === $objCallsFilter->getAnswerStatusTypeIds() ) {
				$arrstrWhere[] = 'c.call_status_type_id IN ( ' . CCallStatusType::ANSWERED . ' ) AND c.call_status_type_id IS NOT NULL ';
			}
		}

		if( ( false == is_null( $objCallsFilter->getVoicemailStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getVoicemailStatusTypeIds() ) ) ) {
			if( CCallFilter::VOICE_MAIL_TYPE_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 1 ';
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT;
			}

			if( CCallFilter::VOICE_MAIL_TYPE_UNPLAYED === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 1 AND c.voice_mail_reviewed_on IS NULL';
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT;
			}

			if( CCallFilter::VOICE_MAIL_TYPE_NO_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.is_voice_mail = 0';
			}
		}

		if( ( false == is_null( $objCallsFilter->getLeadSourceIds() ) ) && ( '' != trim( $objCallsFilter->getLeadSourceIds() ) ) ) {
			if( '0' == $objCallsFilter->getLeadSourceIds() ) {
				$arrstrWhere[] = 'c.lead_source_id IS NULL ';
			} else {
				$arrstrWhere[] = 'c.lead_source_id IN ( ' . $objCallsFilter->getLeadSourceIds() . ' ) ';
			}
		}

		// For Drill-in from Report
		$strReportColumnName = $objCallsFilter->getReportColumnName();

		if( ( false == is_null( $objCallsFilter->getCallTypeIds() ) ) && ( '' != trim( $objCallsFilter->getCallTypeIds() ) ) ) {
			$arrstrWhere[] = 'c.call_type_id IN ( ' . $objCallsFilter->getCallTypeIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getCallResultIds() ) ) && ( '' != trim( $objCallsFilter->getCallResultIds() ) ) ) {
			$arrstrWhere[] = 'c.call_result_id IN ( ' . $objCallsFilter->getCallResultIds() . ' ) ';
		}

		if( false == is_null( trim( $objCallsFilter->getCallIds() ) ) && '' != $objCallsFilter->getCallIds() ) {
			$arrstrWhere[] = 'c.id IN ( ' . $objCallsFilter->getCallIds() . ' ) ';
		}

		if( false == is_null( $objCallsFilter->getApplicantApplicationIds() ) ) {
			$arrstrWhere[] = 'c.applicant_application_id IN ( ' . $objCallsFilter->getApplicantApplicationIds() . ' ) ';
		}

		if( CCallFilter::CALL_ANALYZED_YES == $objCallsFilter->getCallAnalyzed() ) {
			$arrstrWhere[] = 'c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED;
			$arrstrWhere[] = 'c.analyzed_on IS NOT NULL';
		} elseif( CCallFilter::CALL_ANALYZED_NO == $objCallsFilter->getCallAnalyzed() ) {
			$arrstrWhere[] = 'c.call_analysis_status_type_id IS DISTINCT FROM ' . CCallAnalysisStatusType::ANALYZED;
			$arrstrWhere[] = 'c.analyzed_on IS NULL';
		}

		$boolTimeZoneCondition = true;

		if( true == isset( $strReportColumnName ) ) {

			$strCallPeriod = $objCallsFilter->getReportCallPeriod();
			$arrmixReportCondition = self::getReportConditionsByReportColumnName( $strReportColumnName, $strCallPeriod );
			if( ( $arrmixReportCondition[0] ) ) {
				$arrstrWhere[] = $arrmixReportCondition[0];
			}
			$boolTimeZoneCondition = $arrmixReportCondition[1];
		}

		if( false == is_null( $objCallsFilter->getStartDate() ) && false == is_null( $objCallsFilter->getEndDate() ) && 'Min' != $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
			$intStartDate = strtotime( $objCallsFilter->getStartDate() );
			$intEndDate = strtotime( $objCallsFilter->getEndDate() );

			if( $intStartDate <= $intEndDate ) {
				if( true == valStr( $strReportColumnName ) && true == $boolTimeZoneCondition ) {
					$arrstrWhere[] = 'c.call_datetime AT TIME ZONE tz.time_zone_name BETWEEN \'' . date( 'Y-m-d', $intStartDate ) . ' ' . $objCallsFilter->getStartTime() . '\' AND \'' . date( 'Y-m-d', $intEndDate ) . ' ' . $objCallsFilter->getEndTime() . '\'';
				} else {
					$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', $intStartDate ) . ' ' . $objCallsFilter->getStartTime() . '\' AND \'' . date( 'Y-m-d', $intEndDate ) . ' ' . $objCallsFilter->getEndTime() . '\'';
				}
			} elseif( false == is_null( $objCallsFilter->getPageNo() ) && false == is_null( $objCallsFilter->getCountPerPage() ) ) {

				$objCallsFilter->valStartDate();
				$objCallsFilter->valEndDate();

				$objCallsFilter->changeDateFormat();
			}
		}

		$strSelectSql = ' SELECT COUNT( c.id ) ';
		if( true == $objCallsFilter->getReportDistinctLeads() ) {
			$strSelectSql	= ' SELECT COUNT( sub_query.origin_phone_number )
				FROM (
					SELECT DISTINCT c.origin_phone_number, c.lead_source_id, c.property_id ';
		}

		$strSql = $strSelectSql . '
					FROM
						calls c
						' . $strCallSettingsSql . $strPhoneNumberSql . '
						LEFT JOIN call_phone_numbers cpn ON cpn.id = c.call_phone_number_id
						LEFT JOIN time_zones tz ON tz.id = pcs.time_zone_id
					WHERE
						c.cid = ' . ( int ) $intCid;

		if( CCallFilter::CALL_TRACKER_CURRENT_CALLS == $objCallsFilter->getCallsState() ) {
			if( true == valStr( $strReportColumnName ) ) {
				$strSql .= ' AND ( c.deleted_on IS NULL OR c.deleted_on > \'' . $objCallsFilter->getEndDate() . '\' ) ';
			} else {
				$strSql .= ' AND c.deleted_on IS NULL ';
			}
		}

		if( CCallFilter::CALL_TRACKER_ARCHIVED_CALLS == $objCallsFilter->getCallsState() ) {
			$strSql .= ' AND c.deleted_on IS NOT NULL ';
		}

		$strSql .= ' AND c.is_test_call = 0
				 	AND c.is_auto_detected = 0
					AND c.call_result_id IS DISTINCT FROM ' . CCallResult::SIP_TEST_CALL;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( $objCallsFilter->getReportDistinctLeads() ) {
			$strSql .= ') AS sub_query';
		}

		$arrstrData = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrstrData ) ) return $arrstrData[0]['count'];

		return 0;
	}

	public static function fetchCallByStastisticsEmailFilter( $objStastisticsEmailFilter, $objVoipDatabase ) {
		if( 0 >= strlen( trim( $objStastisticsEmailFilter->getLeadSourceIds() ) ) ) return [];

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$strSql = '		SELECT
							property_id,
							COUNT(id) AS call_count
						FROM
							calls c
						WHERE
							c.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
							AND c.property_id IN ( ' . implode( ',', $objStastisticsEmailFilter->getPropertyIds() ) . ' )
							AND c.call_datetime BETWEEN \'' . $objStastisticsEmailFilter->getStartDate() . '\' AND \'' . $objStastisticsEmailFilter->getEndDate() . '\'
							AND ( c.call_result_id = ' . CCallResult::LEAD . ' OR ( c.call_type_id = ' . CCallType::CALL_TRACKER_LEAD . ' AND c.call_result_id IS NULL ) )
							AND c.call_result_id <> ' . CCallResult::TEST_CALL;
		if( false == is_null( $objStastisticsEmailFilter->getLeadSourceIds() ) && 0 < strlen( trim( $objStastisticsEmailFilter->getLeadSourceIds() ) ) ) {
			$strSql .= '	AND c.lead_source_id IN ( ' . $objStastisticsEmailFilter->getLeadSourceIds() . ' )';
		}
		$strSql .= '	GROUP BY
							property_id
						ORDER BY
							property_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallAnalysisCallsByCallFilter( $objPagination, $objCallFilter, $objVoipDatabase, $boolIsCountOnly = false, $boolIsNewCallAnalysis = false ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$arrstrWhere = [];
		$strSqlJoin = '';

		if( true == valId( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = ' c.id = ' . ( int ) $objCallFilter->getCallId();
		} elseif( true == valStr( $objCallFilter->getCallIds() ) ) {
			$arrstrWhere[] = 'c.id IN ( ' . $objCallFilter->getCallIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$arrstrWhere[] = ' c.cid IN ( ' . $objCallFilter->getCids() . ' ) ';
		} elseif( ( true == valId( $objCallFilter->getCid() ) ) ) {
			$arrstrWhere[] = ' c.cid = ' . ( int ) $objCallFilter->getCid();
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objCallFilter->getCallResultIds() ) ) {
			$arrstrWhere[] = ' c.call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCompanyEmployeeIds() ) ) {
			$arrstrWhere[] = ' c.company_employee_id IN ( ' . $objCallFilter->getCompanyEmployeeIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCallAnalysisStatusTypes() ) ) {
			$strCallAnalysisStatusTypeIdsClause = ' ( c.call_analysis_status_type_id IN ( ' . $objCallFilter->getCallAnalysisStatusTypes() . ' ) ';

			if( true == in_array( CCallAnalysisStatusType::UNEVALUATED, $objCallFilter->getCallAnalysisStatusTypesArray() ) ) {
				$strCallAnalysisStatusTypeIdsClause .= ' OR c.call_analysis_status_type_id IS NULL )';
			} else {
				$strCallAnalysisStatusTypeIdsClause .= ' )';
			}

			$arrstrWhere[] = $strCallAnalysisStatusTypeIdsClause;
		}

		$arrstrOrderBy = [
			'call_id'			=> 'c.id',
			'call_type'			=> 'c.call_type_id',
			'status'			=> 'c.call_status_type_id',
			'property'			=> 'pcs.property_name',
			'duration'			=> 'c.call_duration_seconds',
			'time_to_listen'	=> 'c.call_datetime',
			'call_datetime'		=> 'c.call_datetime',
			'lead_probability'	=> 'ct.details->>\'call_is_lead_probability\' '
		];

		if( false == is_null( $objCallFilter->getStartDate() ) && false == is_null( $objCallFilter->getEndDate() ) ) {
			$strStartDate	= $objCallFilter->getStartDate();
			$strEndDate		= $objCallFilter->getEndDate();

			if( 'Min' != $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'' . 'AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' == $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' != $strStartDate && 'Max' == $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'';
			}
		}

		if( false == $boolIsCountOnly ) {
			$strSql = ' SELECT
							c.*,
							cast( ct.details->>\'call_is_lead_probability\' AS DOUBLE PRECISION ) * 100 AS lead_probability,
							CASE WHEN ct.details IS NULL THEN \'By System\' ELSE \'By Human\' END AS analyzer';
			$strSqlJoin = ' LEFT JOIN call_analysis_call_sorts cacs ON ( c.property_id = cacs.property_id AND c.cid = cacs.cid )
							LEFT JOIN call_transcriptions ct ON ( c.id = ct.call_id )
							LEFT JOIN clients cl ON ( cl.id = c.cid )';

			if( true == isset( $arrstrOrderBy[$objCallFilter->getSortBy()] ) && 'property' == $objCallFilter->getSortBy() ) {
				$strSql .= ',pcs.property_name';
				$strSqlJoin .= 'LEFT JOIN property_call_settings pcs ON ( c.property_id = pcs.property_id AND c.cid = pcs.cid )';
			}
		} else {
			$strSql = ' SELECT COUNT( c.id ) ';
			$strSqlJoin = ' LEFT JOIN clients cl ON ( cl.id = c.cid )';
		}

		$strSql .= ' FROM
						calls c ' . $strSqlJoin . '
					WHERE
						c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallTrackerCallTypes() ) . ' )
						AND c.call_duration_seconds >= 45
						AND c.call_file_id IS NOT NULL
						AND c.created_by = ' . CUser::ID_SYSTEM;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';

			if( false == valStr( $objCallFilter->getCids() ) ) {
				$strSql .= ' AND cl.company_status_type_id = ' . CCompanyStatusType:: CLIENT;
			}
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );

			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		} else {
			if( true == isset( $arrstrOrderBy[$objCallFilter->getSortBy()] ) ) {
				$strOrderBy = $arrstrOrderBy[$objCallFilter->getSortBy()];
			} else {
				if( true == $boolIsNewCallAnalysis ) {
					$strOrderBy = 'cacs.delta DESC NULLS LAST, ct.details->>\'call_is_lead_probability\' ';
				} else {
					$strOrderBy = 'ct.details->>\'call_is_lead_probability\' ';
				}
			}
			$strSortDirection	= ( false == is_null( $objCallFilter->getSortDirection() ) ) ? $objCallFilter->getSortDirection() : 'DESC NULLS LAST';

			if( false == is_null( $objCallFilter->getSortDirection() ) && 'lead_probability' == $objCallFilter->getSortBy() && 'DESC' == $objCallFilter->getSortDirection() ) {
				$strSortDirection = 'DESC NULLS LAST';
			}

			if( false == is_null( $strOrderBy ) ) {
				$strSql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection . ', c.id DESC';
			}

			if( false == $boolIsCountOnly && false == valId( $objCallFilter->getCallId() ) && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
				$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchNextCallByCallFilterByAnalyzedOn( $objCallFilter, $intCallId, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$arrstrWhere = [];
		$strSqlJoin = '';

		if( true == valId( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = ' c.id = ' . ( int ) $objCallFilter->getCallId();
		} elseif( true == valStr( $objCallFilter->getCallIds() ) ) {
			$arrstrWhere[] = 'c.id IN ( ' . $objCallFilter->getCallIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$arrstrWhere[] = ' c.cid IN ( ' . $objCallFilter->getCids() . ' ) ';
		} elseif( ( true == valId( $objCallFilter->getCid() ) ) ) {
			$arrstrWhere[] = ' c.cid = ' . ( int ) $objCallFilter->getCid();
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objCallFilter->getCallResultIds() ) ) {
			$arrstrWhere[] = ' c.call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCompanyEmployeeIds() ) ) {
			$arrstrWhere[] = ' c.company_employee_id IN ( ' . $objCallFilter->getCompanyEmployeeIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCallAnalysisStatusTypes() ) ) {
			$strCallAnalysisStatusTypeIdsClause = ' ( c.call_analysis_status_type_id IN ( ' . $objCallFilter->getCallAnalysisStatusTypes() . ' ) ';

			if( true == in_array( CCallAnalysisStatusType::UNEVALUATED, $objCallFilter->getCallAnalysisStatusTypesArray() ) ) {
				$strCallAnalysisStatusTypeIdsClause .= ' OR c.call_analysis_status_type_id IS NULL )';
			} else {
				$strCallAnalysisStatusTypeIdsClause .= ' )';
			}

			$arrstrWhere[] = $strCallAnalysisStatusTypeIdsClause;
		}

		$arrstrOrderBy = [
			'call_id'			=> 'c.id',
			'call_type'			=> 'c.call_type_id',
			'status'			=> 'c.call_status_type_id',
			'property'			=> 'pcs.property_name',
			'duration'			=> 'c.call_duration_seconds',
			'time_to_listen'	=> 'c.call_datetime',
			'call_datetime'		=> 'c.call_datetime',
			'lead_probability'	=> 'ct.details->>\'call_is_lead_probability\' '
		];

		if( false == is_null( $objCallFilter->getStartDate() ) && false == is_null( $objCallFilter->getEndDate() ) ) {
			$strStartDate	= $objCallFilter->getStartDate();
			$strEndDate		= $objCallFilter->getEndDate();

			if( 'Min' != $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'' . 'AND \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' == $strStartDate && 'Max' != $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $strEndDate ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' != $strStartDate && 'Max' == $strEndDate ) {
				$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $strStartDate ) ) . ' 00:00:00\'';
			}
		}

		$strOrderBy			= ( true == isset( $arrstrOrderBy[$objCallFilter->getSortBy()] ) ? $arrstrOrderBy[$objCallFilter->getSortBy()] : 'ct.details->>\'call_is_lead_probability\' ' );
		$strSortDirection	= ( false == is_null( $objCallFilter->getSortDirection() ) ) ? $objCallFilter->getSortDirection() : 'DESC NULLS LAST';
		$strOrderBySql		= '';

		if( false == is_null( $strOrderBy ) ) {
			$strOrderBySql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;
			if( 'call_id' != $objCallFilter->getSortBy() ) {
				$strOrderBySql .= ' ,c.id DESC ';
			}
		}

		$strInnerSql = 'SELECT
							c.id,
							LEAD(c.id) OVER ( ' . $strOrderBySql . ' ) AS nextrowid';
		$strSqlJoin = ' LEFT JOIN call_analysis_call_sorts cacs ON ( c.property_id = cacs.property_id AND c.cid = cacs.cid )
						LEFT JOIN call_transcriptions ct ON ( c.id = ct.call_id )';
		if( true == isset( $arrstrOrderBy[$objCallFilter->getSortBy()] ) && 'property' == $objCallFilter->getSortBy() ) {
			$strInnerSql .= ',pcs.property_name';
			$strSqlJoin .= 'LEFT JOIN property_call_settings pcs ON ( c.property_id = pcs.property_id AND c.cid = pcs.cid )';
		}

		$strInnerSql .= ' FROM
							calls c ' . $strSqlJoin . '
						WHERE
							c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallTrackerCallTypes() ) . ' )
						AND c.call_duration_seconds >= 45
						AND c.call_file_id IS NOT NULL
						AND c.created_by = ' . CUser::ID_SYSTEM;

		if( true == valArr( $arrstrWhere ) ) {
			$strInnerSql .= ' AND ';
			$strInnerSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$strInnerSql .= $strOrderBySql;

		$strSql = ' SELECT
						*
					FROM
						calls
					WHERE
						id = ( SELECT
									nextrowid
								FROM ('
				  . $strInnerSql .
				  ' ) AS subquery
								WHERE
									subquery.id = ' . ( int ) $intCallId . '
							)';

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallWithCallTypeNameByCallId( $intCallId, $objVoipDatabase ) {
		if ( false == isset( $intCallId ) ) return NULL;

		$strSql = 'SELECT
						c.*,
						ct.name AS call_type_name
					FROM
						calls c,
						call_types ct
					WHERE
						c.call_type_id = ct.id
						AND c.id =' . ( int ) $intCallId;

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTrackerLatestCallByEmployeeIdByCallStatusTypeIds( $intEmployeeId, $arrintCallStatusTypeIds, $objVoipDatabase ) {
		$strSql = 'SELECT
						id,
						origin_phone_number,
						outbound_call_id
					FROM
						calls
					WHERE
						employee_id = \'' . ( int ) $intEmployeeId . '\'
						AND call_datetime <= NOW()
						AND call_status_type_id IN ( ' . implode( ',', $arrintCallStatusTypeIds ) . ' )
						AND call_type_id NOT IN ( ' . CCallType::INSIDE_SALES . ' )
					ORDER BY
						call_datetime DESC
					LIMIT 1';

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByPropertyIdByOriginPhoneNumbers( $intPropertyId, $arrintPhoneNumbers, $objVoipDatabase ) {
		if( false == valArr( $arrintPhoneNumbers ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						calls
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND origin_phone_number IN ( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsByCallFilter( $objCallsFilter, $objVoipDatabase, $arrstrFieldNames = [ 'id', 'property_id', 'cid' ] ) {
		$strFetchFields = implode( ',', $arrstrFieldNames );

		$strSql = 'SELECT
						' . $strFetchFields . '
					FROM
						calls
					WHERE
						call_status_type_id = ' . CCallStatusType::ANSWERED . '';

		if( true == valStr( $objCallsFilter->getCallResultIds() ) ) {
			$arrstrWhere[] = ' call_result_id IN ( ' . $objCallsFilter->getCallResultIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getCids() ) ) && ( '' != trim( $objCallsFilter->getCids() ) ) && '' == $objCallsFilter->getCid() ) {
			$arrstrWhere[] = ' cid IN ( ' . $objCallsFilter->getCids() . ' ) ';
		} elseif( ( false == is_null( $objCallsFilter->getCid() ) ) && ( '' != trim( $objCallsFilter->getCid() ) ) && '0' != $objCallsFilter->getCid() ) {
			$arrstrWhere[] = ' cid = ' . ( int ) $objCallsFilter->getCid() . ' ';
		}

		if( true == is_numeric( $objCallsFilter->getPropertyId() ) ) {
			$arrstrWhere[] = 'property_id = ' . ( int ) $objCallsFilter->getPropertyId() . ' ';
		} elseif( true == valStr( $objCallsFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'property_id IN ( ' . $objCallsFilter->getPropertyIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getCallTypeIds() ) ) && ( '' != trim( $objCallsFilter->getCallTypeIds() ) ) ) {
			$arrstrWhere[] = 'call_type_id IN ( ' . $objCallsFilter->getCallTypeIds() . ' ) ';
		}

		if( true == valId( $objCallsFilter->getEmployeeId() ) ) {
			$arrstrWhere[] = 'employee_id = ' . ( int ) $objCallsFilter->getEmployeeId();
		}

		if( true == valId( $objCallsFilter->getCallDurationSeconds() ) ) {
			$arrstrWhere[] = 'call_duration_seconds >= ' . ( int ) $objCallsFilter->getCallDurationSeconds();
		}

		$arrstrWhere[] = 'call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59.999999\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$arrmixResponse = fetchData( $strSql, $objVoipDatabase );
		$arrmixCalls = [];

		if( true == valArr( $arrmixResponse ) ) {
			foreach( $arrmixResponse as $arrmixCall ) {
				$arrmixCalls[$arrmixCall['id']] = $arrmixCall;
			}
		}

		return $arrmixCalls;
	}

	public static function fetchPropertyCallsCountByCallFilter( $objCallsFilter, $objVoipDatabase ) {
		$strSql = 'SELECT
						c.property_id, COUNT( c.id )
					FROM
						calls AS c
					WHERE
						c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND c.call_result_id = ' . CCallResult::LEAD;

		if( ( false == is_null( $objCallsFilter->getCids() ) ) && ( '' != trim( $objCallsFilter->getCids() ) ) && '' == $objCallsFilter->getCid() ) {
			$arrstrWhere[] = ' c.cid IN ( ' . $objCallsFilter->getCids() . ' ) ';
		} elseif( ( false == is_null( $objCallsFilter->getCid() ) ) && ( '' != trim( $objCallsFilter->getCid() ) ) && '0' != $objCallsFilter->getCid() ) {
			$arrstrWhere[] = ' c.cid = ' . ( int ) $objCallsFilter->getCid() . ' ';
		}

		if( true == is_numeric( $objCallsFilter->getPropertyId() ) ) {
			$arrstrWhere[] = 'c.property_id = ' . $objCallsFilter->getPropertyId() . ' ';
		} elseif( true == valStr( $objCallsFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . $objCallsFilter->getPropertyIds() . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getCallTypeIds() ) ) && ( '' != trim( $objCallsFilter->getCallTypeIds() ) ) ) {
			$arrstrWhere[] = 'c.call_type_id IN ( ' . $objCallsFilter->getCallTypeIds() . ' ) ';
		}

		$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59.999999\'';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= ' GROUP BY c.property_id ';

		$arrmixResponse = fetchData( $strSql, $objVoipDatabase );

		$arrmixPropertyCallsCounts = [];

		if( true == valArr( $arrmixResponse ) ) {
			foreach( $arrmixResponse as $arrmixPropertyCallsCount ) {
				$arrmixPropertyCallsCounts[$arrmixPropertyCallsCount['property_id']] = $arrmixPropertyCallsCount['count'];
			}
		}

		return $arrmixPropertyCallsCounts;
	}

	public static function fetchCallByCidByPropertyIdById( $intCid, $intPropertyId, $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM calls WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' AND id = ' . ( int ) $intCallId;
		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallById( $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT
						c.*,
						clients.company_name,
						pcs.property_name,
						pcs.marketing_property_name,
						ct.name AS call_type_name,
						cr.name AS call_result_name,
						ca.full_name AS call_agent_full_name,
						ca.preferred_name AS call_agent_preferred_name
					FROM
						calls c
						LEFT JOIN clients ON ( clients.id = c.cid )
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = c.property_id AND pcs.cid = c.cid )
						LEFT JOIN call_types AS ct ON ( ct.id = c.call_type_id )
						LEFT JOIN call_results AS cr ON ( cr.id = c.call_result_id )
						LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )
					WHERE
						c.id = ' . ( int ) $intCallId;

		return parent::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByCallStatusTypeIdsByEmployeeId( $arrintMissingCallStatusTypeIds, $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintMissingCallStatusTypeIds ) || false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT *FROM calls WHERE call_status_type_id IN ( ' . implode( ',', $arrintMissingCallStatusTypeIds ) . ' ) AND employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchSyncCallSystemCalls( $objVoipDatabase ) {
		$strSql = 'SELECT
						c.*,
						ct.name AS call_type_name,
						clients.company_name,
						pcs.property_name,
						cst.name AS call_status_type_name,
						cr.name AS call_result_name
					FROM
						calls AS c
						LEFT JOIN clients ON ( clients.id = c.cid )
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = c.property_id )
						LEFT JOIN call_types AS ct ON ( ct.id = c.call_type_id )
						LEFT JOIN call_status_types AS cst ON ( cst.id = c.call_status_type_id )
						LEFT JOIN call_results AS cr ON ( cr.id = c.call_result_id )
					WHERE
						c.call_status_type_id IN ( ' . implode( ',', CCallStatusType::$c_arrintActiveCallStatusTypeIds ) . ' )
						AND c.call_datetime < ( NOW() - INTERVAL \'10 minute\' )
						AND c.call_datetime > ( CURRENT_DATE - INTERVAL \'7 days\' )
						AND c.call_type_id <> ' . CCallType::VISA_PHONE_AUTH;

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallsByCallFilter( $objPagination, $objCallsFilter, $objVoipDatabase, $boolIsCountOnly = false, $boolIsFromReport = false ) {
		$arrstrWhere = [];
		$strCallTranscriptionSql = '';

		if( ( false == is_null( $objCallsFilter->getCallIds() ) ) && ( '' != trim( $objCallsFilter->getAllLeads() ) ) && ( 'Find a Call ...' != trim( $objCallsFilter->getAllLeads() ) ) ) {
			$strAllCallsData	= trim( $objCallsFilter->getCallIds() );
			$arrstrWhere[] = '( CAST( c.id AS text ) LIKE \'%' . addslashes( $strAllCallsData ) . '%\' OR c.origin_phone_number LIKE \'%' . addslashes( $strAllCallsData ) . '%\' OR c.destination_phone_number LIKE \'%' . addslashes( $strAllCallsData ) . '%\' ) ';
		}

		if( true == is_numeric( $objCallsFilter->getGenericData() ) && ( false == is_null( $objCallsFilter->getGenericData() ) ) && ( '' != trim( $objCallsFilter->getGenericData() ) ) && ( 'Find a Call ...' != trim( $objCallsFilter->getGenericData() ) ) ) {
			$strGenericData	= trim( $objCallsFilter->getGenericData() );
			$arrstrWhere[] = '( CAST( c.id AS text ) LIKE \'' . addslashes( $strGenericData ) . '%\' OR c.origin_phone_number LIKE \'%' . addslashes( $strGenericData ) . '%\' OR c.destination_phone_number LIKE \'%' . addslashes( $strGenericData ) . '%\' ) ';
		}

		if( ( '' == trim( $objCallsFilter->getGenericData() ) ) && 'yes' == $objCallsFilter->getShowVanity() ) {
			$arrstrWhere[] = 'c.call_phone_number_id IS NULL';
		}

		if( true == is_numeric( $objCallsFilter->getCallId() ) ) {
			$arrstrWhere[] = 'c.id = ' . ( int ) $objCallsFilter->getCallId();
		}

		if( true == is_numeric( $objCallsFilter->getCustomerId() ) ) {
			$arrstrWhere[] = 'c.customer_id = ' . ( int ) $objCallsFilter->getCustomerId();
		}

		if( ( false == is_null( $objCallsFilter->getCid() ) ) && ( '' != trim( $objCallsFilter->getCid() ) ) && '0' != $objCallsFilter->getCid() ) {
			$arrstrWhere[] = ' c.cid = ' . ( int ) $objCallsFilter->getCid() . ' ';
		}

		if( ( false == is_null( $objCallsFilter->getOutboundCallId() ) ) && ( '' != trim( $objCallsFilter->getOutboundCallId() ) ) && '0' != $objCallsFilter->getOutboundCallId() ) {
			$arrstrWhere[] = ' c.outbound_call_id = ' . ( int ) $objCallsFilter->getOutboundCallId() . ' ';
		}

		if( true == valArr( $objCallsFilter->getCallIdsArray() ) ) {
			$arrstrWhere[] = 'c.id IN ( ' . implode( ',', $objCallsFilter->getCallIdsArray() ) . ' ) ';
		}

		if( ( true == valArr( $objCallsFilter->getCidsArray() ) ) ) {
			$arrstrWhere[] = ' c.cid IN ( ' . implode( ',', $objCallsFilter->getCidsArray() ) . ' ) ';
		}

		if( ( true == valArr( $objCallsFilter->getPropertyIdsArray() ) ) ) {
			$arrstrWhere[] = 'c.property_id IN ( ' . implode( ',', $objCallsFilter->getPropertyIdsArray() ) . ' ) ';
		}

		if( ( true == valArr( $objCallsFilter->getEmployeeIdsArray() ) ) ) {
			$arrstrWhere[] = ' c.employee_id IN ( ' . implode( ',', $objCallsFilter->getEmployeeIdsArray() ) . ' ) ';
		}

		if( ( true == valArr( $objCallsFilter->getCallTypeIdsArray() ) ) ) {
			$arrstrWhere[] = 'c.call_type_id IN ( ' . implode( ',', $objCallsFilter->getCallTypeIdsArray() ) . ' ) ';
		}

		if( ( true == valArr( $objCallsFilter->getCallResultIdsArray() ) ) ) {
			$arrstrWhere[] = 'c.call_result_id IN ( ' . implode( ',', $objCallsFilter->getCallResultIdsArray() ) . ' ) ';
		}

		if( true == $boolIsFromReport ) {
			$arrstrWhere[] = '( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' )';
		}

		if( ( true == valArr( $objCallsFilter->getLeadSourceIdsArray() ) ) ) {
			$arrstrWhere[] = 'c.lead_source_id IN ( ' . implode( ',', $objCallsFilter->getLeadSourceIdsArray() ) . ' ) ';
		}

		if( ( false == is_null( $objCallsFilter->getAnswerStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getAnswerStatusTypeIds() ) ) ) {
			if( CCallStatusType::UNANSWERED == $objCallsFilter->getAnswerStatusTypeIds() && true === $objCallsFilter->getIsCallQueueTime() ) {
				$arrstrWhere[] = '( c.call_status_type_id IN ( ' . CCallStatusType::UNANSWERED . ' ) AND c.call_center_queue_time > 30 )';
			} elseif( true == $boolIsFromReport && true == in_array( CCallStatusType::ANSWERED, $objCallsFilter->getAnswerStatusTypeIdsArray() ) && true == in_array( CCallStatusType::UNANSWERED, $objCallsFilter->getAnswerStatusTypeIdsArray() ) && true == $objCallsFilter->getIsCallQueueTime() ) {
				$arrstrWhere[] = '( c.call_status_type_id IN ( ' . CCallStatusType::ANSWERED . ' ) OR c.call_status_type_id IN ( ' . CCallStatusType::UNANSWERED . ' ) AND c.call_center_queue_time > 0 )';
			} else {
				$arrstrWhere[] = 'c.call_status_type_id IN ( ' . $objCallsFilter->getAnswerStatusTypeIds() . ' ) ';
			}

		} elseif( true === $objCallsFilter->getIsCallQueueTime() ) {
			$arrstrWhere[] = 'c.call_center_queue_time > 0';
		}

		if( ( false == is_null( $objCallsFilter->getVoicemailStatusTypeIds() ) ) && ( '' != trim( $objCallsFilter->getVoicemailStatusTypeIds() ) ) ) {
			if( CCallFilter::VOICE_MAIL_TYPE_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT . ' AND c.voice_mail_reviewed_on IS NOT NULL';
			}

			if( CCallFilter::VOICE_MAIL_TYPE_UNPLAYED === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT . ' AND c.voice_mail_reviewed_on IS NULL';
			}

			if( true == in_array( CCallFilter::VOICE_MAIL_TYPE_VOICE_MAIL, $objCallsFilter->getVoicemailStatusTypeIdsArray() ) && true == in_array( CCallFilter::VOICE_MAIL_TYPE_UNPLAYED, $objCallsFilter->getVoicemailStatusTypeIdsArray() ) ) {
				$arrstrWhere[] = 'c.call_status_type_id = ' . CCallStatusType::VOICEMAIL_LEFT;
			}

			if( CCallFilter::VOICE_MAIL_TYPE_NO_VOICE_MAIL === $objCallsFilter->getVoicemailStatusTypeIds() ) {
				$arrstrWhere[] = 'c.call_status_type_id <> ' . CCallStatusType::VOICEMAIL_LEFT;
			}
		}

		if( ( false == is_null( $objCallsFilter->getCallDirectionTypeIds() ) ) && ( '' != trim( $objCallsFilter->getCallDirectionTypeIds() ) ) ) {
			if( CCallFilter::CALL_DIRECTION_TYPE_INBOUND === $objCallsFilter->getCallDirectionTypeIds() ) {
				$arrstrWhere[] = 'c.call_type_id NOT IN ( ' . implode( ',', CCallTypeHelper::getAllOutboundCallTypes() ) . ' ) ';
			}

			if( CCallFilter::CALL_DIRECTION_TYPE_OUTBOUND === $objCallsFilter->getCallDirectionTypeIds() ) {
				$arrstrWhere[] = 'c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getAllOutboundCallTypes() ) . ' ) ';
			}
		}

		if( false == is_null( $objCallsFilter->getStartDate() ) && false == is_null( $objCallsFilter->getEndDate() ) ) {

			if( 'Min' != $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
				$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\'' . ' AND \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' == $objCallsFilter->getStartDate() && 'Max' != $objCallsFilter->getEndDate() ) {
				$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
			} elseif( 'Min' != $objCallsFilter->getStartDate() && 'Max' == $objCallsFilter->getEndDate() ) {
				$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $objCallsFilter->getStartDate() ) ) . ' 00:00:00\'';
			}
		}

		if( false == valArr( $objCallsFilter->getCallQueueIds() ) && true == valStr( $objCallsFilter->getCallQueueIds() ) ) {
			$arrstrWhere[] .= ' cq.id IN ( ' . $objCallsFilter->getCallQueueIds() . ' ) ';
		}

		if( 0 < trim( strlen( $objCallsFilter->getPhoneNumber() ) ) && false == is_null( $objCallsFilter->getPhoneNumber() ) ) {
			$intPhoneNumber = preg_replace( '/\D/', '', trim( $objCallsFilter->getPhoneNumber() ) );
			if( 0 < trim( strlen( $intPhoneNumber ) ) ) {
				$arrstrWhere[] = '( regexp_replace(c.origin_phone_number, \'[^0-9]\', \'\', \'g\') like \'' . addslashes( $intPhoneNumber ) . '\' OR  regexp_replace(c.destination_phone_number, \'[^0-9]\', \'\', \'g\') like  \'' . addslashes( $intPhoneNumber ) . '\' )';
			}
		}

		if( false == is_null( $objCallsFilter->getCallAgentName() ) ) {
			$arrstrWhere[] = ' ca.full_name LIKE \'' . $objCallsFilter->getCallAgentName() . '%\'';
		}

		if( false == is_null( $objCallsFilter->getCallAgentData() ) ) {
			$arrstrWhere[] = ' ( CAST( ca.agent_phone_extension_id AS text ) LIKE \'' . $objCallsFilter->getCallAgentData() . '%\'
							OR CAST( ca.physical_phone_extension_id AS text ) LIKE \'' . $objCallsFilter->getCallAgentData() . '%\' )';
		}

		if( true == valArr( $objCallsFilter->getCidsArray() ) && false != valStr( $objCallsFilter->getCompanyStatusTypeIds() ) ) {
			$arrstrWhere[] = ' mc.company_status_type_id IN ( ' . $objCallsFilter->getCompanyStatusTypeIds() . ' )';
		}

		if( CCallTranscription::TRANSCRIBED_CALLS == $objCallsFilter->getTranscribedCall() ) {
			$strCallTranscriptionSql = ' LEFT JOIN call_transcriptions ct ON ( c.id = ct.call_id ) ';
			$arrstrWhere[] = ' ct.details IS NOT NULL ';
		} else if( CCallTranscription::NON_TRANSCRIBED_CALLS == $objCallsFilter->getTranscribedCall() ) {
			$strCallTranscriptionSql = ' LEFT JOIN call_transcriptions ct ON ( c.id = ct.call_id ) ';
			$arrstrWhere[] = ' ct.details IS NULL ';
		}

		if( false == is_null( $objCallsFilter->getCountryCodes() ) ) {
			$arrstrWhere[] = ' cpn.country_code IN ( \'' . implode( '\', \'', explode( ',', $objCallsFilter->getCountryCodes() ) ) . '\' )';
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT
							COUNT( c.id )
						FROM calls c ';

		} else {
			$strSql = 'SELECT
							c.*,
							cpn.phone_number AS call_phone_number,
							cpn.country_code,
							cpn.ivr_id AS ivr_id,
							mc.company_name,
							pcs.property_name,
							pcs.office_phone_number AS property_office_phone_number,
							pcs.timezone_offset,
							tz.time_zone_name,
							cq.name AS call_queue_name,
							CASE WHEN ca.preferred_Name IS NULL THEN ca.full_name ELSE ca.preferred_Name END as call_agent_full_name
						FROM calls c ';
		}
		$strSql .= 'LEFT JOIN call_queues AS cq ON ( c.call_queue_id = cq.id )
					LEFT JOIN call_phone_numbers cpn ON ( cpn.id = c.call_phone_number_id )
					LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id OR ca.id = c.call_agent_id )
					LEFT JOIN clients mc ON ( mc.id = c.cid )
					LEFT JOIN property_call_settings pcs ON ( pcs.cid = c.cid AND pcs.property_id = c.property_id )
					LEFT JOIN time_zones tz ON tz.id = pcs.time_zone_id' . $strCallTranscriptionSql;

		$strSql .= ' WHERE
						( pcs.property_id IS NOT NULL OR c.property_id IS NULL ) ';

		$strSqlCondition = '';

		if( false == $boolIsFromReport ) {
			if( 'archived_calls' == $objCallsFilter->getCallsState() ) {
				$strSqlCondition = ' AND c.deleted_on IS NOT NULL ';
			} elseif( 'current_calls' == $objCallsFilter->getCallsState() ) {
				$strSqlCondition = ' AND c.deleted_on IS NULL ';
			}
		}

		if( 'IN' === $objCallsFilter->getOffice() ) {
				$arrstrWhere[] = 'ca.office_id IN (' . implode( ',', COffice::$c_arrintIndiaOfficeIds ) . ')';
		} elseif( 'US' === $objCallsFilter->getOffice() ) {
			$arrstrWhere[] = 'ca.office_id IN (' . implode( ',', COffice::$c_arrintUSOfficeIds ) . ')';
		}

		$strSqlCondition .= 'AND c.is_test_call = 0
							 AND c.is_auto_detected = 0
							 AND c.call_result_id IS DISTINCT FROM ' . CCallResult::SIP_TEST_CALL;

		$strSql .= $strSqlCondition;

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		} else {
			$arrstrOrderBy = [
				'call_id'			=> 'c.id',
				'call_type'			=> 'c.call_type_id',
				'status'			=> 'c.call_status_type_id',
				'property'			=> 'c.property_id',
				'duration'			=> 'c.call_duration_seconds',
				'time_to_listen'	=> 'c.call_datetime',
				'call_datetime'		=> 'c.call_datetime',
				'updated_on'		=> 'c.updated_on'
			];

			$strOrderBy = ( true == isset( $arrstrOrderBy[$objCallsFilter->getSortBy()] ) ? $arrstrOrderBy[$objCallsFilter->getSortBy()] : 'c.call_datetime' );
			$strSortDirection = ( false == is_null( $objCallsFilter->getSortDirection() ) ) ? $objCallsFilter->getSortDirection() : 'DESC';

			if( false == is_null( $strOrderBy ) ) {
				$strSql .= ' ORDER BY ' . $strOrderBy . ' ' . $strSortDirection;
			}

			if( false == $boolIsCountOnly && true == isset( $objPagination ) && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
				$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallByOutboundCallId( $intOutboundCallId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM calls WHERE outbound_call_id = ' . ( int ) $intOutboundCallId;
		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchCallBySessionUuid( $strSessionUuid, $objVoipDatabase ) {
		if( false == valStr( $strSessionUuid ) ) return NULL;

		$strSql = 'SELECT
						c.*
					FROM
						calls AS c
						JOIN call_members AS cm ON ( cm.call_id = c.id )
					WHERE
						cm.session_uuid = \'' . $strSessionUuid . '\'
					LIMIT 1';

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataByCallAnalysisAverageTimeToAnalyzeFilter( $objCallAnalysisAverageTimeToAnalyzeFilter, $objVoipDatabase ) {
		$arrstrWhere	= [];
		$arrstrWhere[]	= ' deleted_on IS NULL AND call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED;

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() ) && false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) && 'Min' != $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() && 'Max' != $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) {
			$arrstrWhere[] = 'call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
		}

		if( ( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getUserIds() ) ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getUserIds() ) ) ) {
			$arrstrWhere[] = ' analyzed_by IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getUserIds() . ' ) ';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() ) ) ) {
			$arrstrWhere[] = ' cid IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = ' property_id IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() ) ) ) {
			$arrstrWhere[] = ' call_result_id IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() . ' ) ';
		}

		$strSql = 'SELECT
						analyzed_by,
						COUNT( id ) AS number_of_calls,
						AVG((EXTRACT(EPOCH FROM (analyzed_on - call_datetime)))) AS average_time_to_analyze
					FROM
						calls ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		$strSql .= ' GROUP BY
						analyzed_by
					HAVING
						analyzed_by IS NOT NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataByCallAnalysisAverageTimeToAnalyzeFilterByAgentId( $objCallAnalysisAverageTimeToAnalyzeFilter, $intAgentId,$objVoipDatabase ) {
		if ( false == isset( $intAgentId ) ) return NULL;

		$arrstrWhere	= [];
		$arrstrWhere[]	= ' deleted_on IS NULL';
		$arrstrWhere[]	= ' call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED;
		$arrstrWhere[]	= ' analyzed_by = ' . ( int ) $intAgentId . ' ';

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() ) && false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) && 'Min' != $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() && 'Max' != $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) {
			$arrstrWhere[] = ' call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallAnalysisAverageTimeToAnalyzeFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objCallAnalysisAverageTimeToAnalyzeFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() ) ) ) {
			$arrstrWhere[] = ' cid IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = ' property_id IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() ) && ( '' != trim( $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() ) ) ) {
			$arrstrWhere[] = ' call_result_id IN ( ' . $objCallAnalysisAverageTimeToAnalyzeFilter->getCallResultIds() . ' ) ';
		}

		$strSql = 'SELECT
						id,
						analyzed_by,
						analyzed_on,
						call_datetime,
						call_analysis_status_type_id,
						(EXTRACT(EPOCH FROM (analyzed_on - call_datetime))) AS average_time_to_analyze
					FROM
						calls ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere );
		}

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisResultsByCallAnalysisFilter( $objPagination, $objCallAgentCallAnalysisFilter, $objVoipDatabase, $boolIsCountOnly = false, $boolIsDownload = false ) {
		$arrstrWhere = [];

		$arrstrWhere[] = '	c.analyzed_on IS NOT NULL
							AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED;

		if( false == is_null( $objCallAgentCallAnalysisFilter->getStartDate() ) && false == is_null( $objCallAgentCallAnalysisFilter->getEndDate() ) && 'Min' != $objCallAgentCallAnalysisFilter->getStartDate() && 'Max' != $objCallAgentCallAnalysisFilter->getEndDate() ) {
			$arrstrWhere[] = ' c.analyzed_on >= \'' . date( 'Y-m-d', strtotime( $objCallAgentCallAnalysisFilter->getStartDate() ) ) . ' 00:00:00\'';
			$arrstrWhere[] = ' c.analyzed_on <= \'' . date( 'Y-m-d', strtotime( $objCallAgentCallAnalysisFilter->getEndDate() ) ) . ' 23:59:59\'';
		}

		if( ( false == is_null( $objCallAgentCallAnalysisFilter->getUserIds() ) ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getUserIds() ) ) ) {
			$arrstrWhere[] = ' c.analyzed_by IN ( ' . $objCallAgentCallAnalysisFilter->getUserIds() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getCids() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getCids() ) ) ) {
			$arrstrWhere[] = ' c.cid IN ( ' . $objCallAgentCallAnalysisFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getCallResultIds() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getCallResultIds() ) ) ) {
			$arrstrWhere[] = ' c.call_result_id IN ( ' . $objCallAgentCallAnalysisFilter->getCallResultIds() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getPropertyIds() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = ' c.property_id IN ( ' . $objCallAgentCallAnalysisFilter->getPropertyIds() . ' ) ';
		}

		$strSql = 'SELECT
						 id, c.call_duration_seconds, c.call_file_id, c.cid, c.call_analysis_status_type_id, c.analyzed_by, c.analyzed_on, c.property_id, c.call_result_id
					FROM
						calls c
 					WHERE ';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( false == $boolIsCountOnly && false == $boolIsDownload && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			if( true == valArr( $arrstrData ) ) {
				return \Psi\Libraries\UtilFunctions\count( $arrstrData );
			}
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisResultsSummaryByCallAnalysisFilter( $objCallAgentCallAnalysisFilter, $objVoipDatabase ) {
		$strWhere = '';

		if( false == is_null( $objCallAgentCallAnalysisFilter->getStartDate() ) && false == is_null( $objCallAgentCallAnalysisFilter->getEndDate() ) && 'Min' != $objCallAgentCallAnalysisFilter->getStartDate() && 'Max' != $objCallAgentCallAnalysisFilter->getEndDate() ) {
			$strWhere .= ' AND c.analyzed_on >= \'' . date( 'Y-m-d', strtotime( $objCallAgentCallAnalysisFilter->getStartDate() ) ) . ' 00:00:00\'';
			$strWhere .= ' AND c.analyzed_on <= \'' . date( 'Y-m-d', strtotime( $objCallAgentCallAnalysisFilter->getEndDate() ) ) . ' 23:59:59\'';
		}

		if( ( false == is_null( $objCallAgentCallAnalysisFilter->getUserIds() ) ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getUserIds() ) ) ) {
			$strWhere .= ' AND c.analyzed_by IN ( ' . $objCallAgentCallAnalysisFilter->getUserIds() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getCids() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getCids() ) ) ) {
			$strWhere .= ' AND c.cid IN ( ' . $objCallAgentCallAnalysisFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getCallResultIds() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getCallResultIds() ) ) ) {
			$strWhere .= ' AND c.call_result_id IN ( ' . $objCallAgentCallAnalysisFilter->getCallResultIds() . ' ) ';
		}

		if( false == is_null( $objCallAgentCallAnalysisFilter->getPropertyIds() ) && ( '' != trim( $objCallAgentCallAnalysisFilter->getPropertyIds() ) ) ) {
			$strWhere .= ' AND c.property_id IN ( ' . $objCallAgentCallAnalysisFilter->getPropertyIds() . ' ) ';
		}

		$strSql = 'SELECT
						COUNT(id) AS analyzed_call_count, c.analyzed_by
					 FROM
	 					calls c
 					WHERE
						c.analyzed_by IS NOT NULL
						AND c.analyzed_on IS NOT NULL
						AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . $strWhere . '
					GROUP BY c.analyzed_by ';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsCountsByCidByCallResultIdByPropertIdsByFilter( $intCid, $intCallResultId, $arrintPropertyIdsAssociatedWithPsProductCallAnalysis, $objSearchFilter, $objVoipDatabase, $boolReport = false, $boolLeasingAgent = false ) {
		if( true == $boolReport ) {
			$strStartDate 		= $objSearchFilter->getFilterObject( 'period' )->getStartDate();
			$strEndDate 		= $objSearchFilter->getFilterObject( 'period' )->getEndDate();
		} else {
			$strStartDate = $objSearchFilter->getSelectedStartDate();
			$strEndDate = $objSearchFilter->getSelectedEndDate();
		}

		$arrintLeasingAgentIds = $objSearchFilter->getFilterFieldValue( 'leasing_agent_ids' );

		$strSql = 'SELECT
						COUNT(c.id) AS total_analyzed_calls,
						c.property_id '
				  . ( ( true == $boolLeasingAgent ) ? ', c.company_employee_id ' : '' ) . '
					FROM
						calls c
						JOIN call_analysis_results car ON ( car.cid = c.cid AND car.call_id = c.id )
						JOIN call_analysis_questions caq ON ( caq.id = car.call_analysis_question_id AND caq.cid = car.cid )
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.call_result_id <> ' . ( int ) $intCallResultId;

		if( true == valArr( $arrintPropertyIdsAssociatedWithPsProductCallAnalysis ) ) {
			$strSql .= ' AND c.property_id IN ( ' . implode( ',', $arrintPropertyIdsAssociatedWithPsProductCallAnalysis ) . ' )';
		}

		$strSql .= ' AND DATE_TRUNC( \'day\', c.call_datetime ) >= \'' . $strStartDate . '\'
						AND DATE_TRUNC( \'day\', c.call_datetime ) <= \'' . $strEndDate . '\'
						AND c.analyzed_on IS NOT NULL
						AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
						AND car.is_qualified <> 2
						AND ( caq.description LIKE \'%Leasing Professional sets a SPECIFIC appointment with time (2 Hour Window) and day%\' OR caq.description LIKE \'%Caller agrees to a SPECIFIC appointment with time and day.%\' )'
				   . ( ( true == $boolLeasingAgent && true == valArr( $arrintLeasingAgentIds ) ) ? ' AND c.company_employee_id IN ( ' . implode( ',', $arrintLeasingAgentIds ) . ' ) ' : ' AND c.company_employee_id IS NOT NULL ' ) . '
					GROUP BY
						c.property_id ' .
				   ( ( true == $boolLeasingAgent ) ? ', c.company_employee_id': '' );

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByCallAnalysisScorecardIdByFormFilterByCid( $intCallAnalysisScorecardId, $arrmixFormFilter, $intCid, $objVoipDatabase ) {
		$strSql = ' SELECT
						analyzed_calls,
						qualified_weightage,
						total_weightage
					FROM
						(
							SELECT
								COUNT( DISTINCT( CASE WHEN
											c.id = car.call_id
										THEN c.id ELSE NULL
								END ) ) AS analyzed_calls,
								SUM( CASE WHEN
											c.id = car.call_id
											AND car.is_qualified = 1
										THEN car.weight ELSE NULL
								END ) AS qualified_weightage,
								SUM( CASE WHEN
											c.id = car.call_id
											AND ( car.is_qualified = 0 OR car.is_qualified = 1 )
										THEN car.weight ELSE NULL
								END ) AS total_weightage
							FROM
								calls c
								JOIN call_analysis_results car ON ( car.cid = c.cid AND c.id = car.call_id )
								JOIN call_analysis_questions caq ON ( caq.cid = car.cid AND car.call_analysis_question_id = caq.id )
								JOIN call_analysis_categories cac ON ( cac.cid = caq.cid AND caq.call_analysis_category_id = cac.id )
								JOIN call_analysis_scorecards cas ON ( cas.cid = cac.cid AND cac.call_analysis_scorecard_id = cas.id )
							WHERE
								c.cid = ' . ( int ) $intCid . '
								AND c.property_id IN( ' . $arrmixFormFilter['property_id'] . ' )
								AND cas.id = ' . ( int ) $intCallAnalysisScorecardId . '
								AND c.company_employee_id IN ( ' . $arrmixFormFilter['company_employee_ids'] . ')
								AND c.call_datetime BETWEEN \'' . $arrmixFormFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrmixFormFilter['end_date'] . ' 23:59:59.999999 \'
								AND c.analyzed_on IS NOT NULL
								AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
								AND c.call_result_id <> ' . CCallResult::TEST_CALL . '
						 ) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchNotApplicableAnalyzedCallsByCallAnalysisCategoryIdsByFormFilterByCid( $arrintCallAnalysisCategoryIds, $arrmixFormFilter, $intCid, $objVoipDatabase ) {
		$strSql = ' SELECT
						caq.id,
						COUNT ( car.is_qualified ) AS not_applicable_calls_count
					FROM
						call_analysis_questions caq
						JOIN call_analysis_results car ON ( car.cid = caq.cid AND caq.id = car.call_analysis_question_id AND car.is_qualified = 2 )
						JOIN calls c ON (	c.cid = car.cid AND c.id = car.call_id
											AND c.property_id IN ( ' . $arrmixFormFilter['property_id'] . ' )
											AND c.call_datetime BETWEEN \'' . $arrmixFormFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrmixFormFilter['end_date'] . ' 23:59:59.999999 \'
											AND c.analyzed_on IS NOT NULL
											AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
											AND c.call_result_id <> ' . CCallResult::TEST_CALL . ' )
					WHERE
						caq.cid = ' . ( int ) $intCid . '
						AND caq.call_analysis_category_id IN ( ' . implode( ',', ( array ) $arrintCallAnalysisCategoryIds ) . ' )
						AND c.company_employee_id IN ( ' . $arrmixFormFilter['company_employee_ids'] . ')
						AND caq.is_text = false
					GROUP BY
						caq.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchAnalyzedCallsByCallAnalysisCategoryIdsByFormFilterByCid( $arrintCallAnalysisCategoryIds, $arrmixFormFilter, $intCid, $objVoipDatabase ) {
		$strSql = ' SELECT
						caq.id,
						COUNT( DISTINCT c.id ) AS calls_count
					FROM
						call_analysis_questions caq
						LEFT JOIN call_analysis_results car ON ( car.cid = caq.cid AND caq.id = car.call_analysis_question_id )
						JOIN calls c ON (	c.cid = car.cid AND c.id = car.call_id
											AND c.property_id IN ( ' . $arrmixFormFilter['property_id'] . ' )
											AND c.call_datetime BETWEEN \'' . $arrmixFormFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrmixFormFilter['end_date'] . ' 23:59:59.999999 \'
											AND c.analyzed_on IS NOT NULL
											AND c.call_analysis_status_type_id = ' . CCallAnalysisStatusType::ANALYZED . '
											AND c.call_result_id <> ' . CCallResult::TEST_CALL . ' )
					WHERE
						caq.cid = ' . ( int ) $intCid . '
						AND caq.call_analysis_category_id IN ( ' . implode( ',', ( array ) $arrintCallAnalysisCategoryIds ) . ' )
						AND c.company_employee_id IN ( ' . $arrmixFormFilter['company_employee_ids'] . ' )
						AND car.is_qualified <> 2
					GROUP BY
						caq.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valStr( $objCallFilter->getEmployeeIds() ) ) return NULL;

		$strSql = 'SELECT
						c.*
					FROM
						calls c
						JOIN property_call_settings pcs ON ( pcs.property_id = c.property_id AND pcs.is_calendar_enabled = 1 )
					WHERE
						c.employee_id IN( ' . $objCallFilter->getEmployeeIds() . ' )
						AND c.call_result_id = ' . CCallResult::LEAD . '
						AND c.applicant_application_id IS NOT NULL
						AND c.created_on
						BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
						AND \'' . date( 'Y-m-d',	strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59\'';

		return CCalls::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsDataByCallResultIdsByCallFilter( $objCallFilter, $objVoipDatabase, $boolIsDownloadReport = false, $boolIsResidentSupportReport = false ) {
		$strSql = ' SELECT ';
		$strResidentSupportSql = '';
		$strResidentSupportSelectSql = '';

		if( true == $boolIsDownloadReport ) $strSql .= 'employee_id, ';

		if( true == $boolIsResidentSupportReport ) {
			$strResidentSupportSql = 'COALESCE ( resident_support_answered_calls_count, 0 ) AS resident_support_answered_calls_count,
										COALESCE ( resident_support_wrap_up_time, 0 ) AS resident_support_wrap_up_time,';

			$strResidentSupportSelectSql = 'COUNT ( CASE WHEN
														c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
														AND c.call_type_id = ' . CCallType::RESIDENT_SUPPORT . '
													THEN c.id ELSE NULL
											END ) AS resident_support_answered_calls_count,

											SUM ( CASE WHEN
														c.call_type_id = ' . CCallType::RESIDENT_SUPPORT . '
													THEN c.wrap_up_time ELSE NULL
											END ) AS resident_support_wrap_up_time,';
		}

		$strSql .= '	COALESCE ( lead_answered_calls_count, 0 ) AS lead_answered_calls_count,
						COALESCE ( work_order_answered_calls_count, 0 ) AS work_order_answered_calls_count,
						COALESCE ( resident_answered_calls_count, 0 ) AS resident_answered_calls_count,
						COALESCE ( support_answered_calls_count, 0 ) AS support_answered_calls_count,
						COALESCE ( other_answered_calls_count, 0 ) AS other_answered_calls_count,

						COALESCE ( lead_call_duration_seconds, 0 ) AS lead_call_duration_seconds,
						COALESCE ( work_order_call_duration_seconds, 0 ) AS work_order_call_duration_seconds,
						COALESCE ( resident_call_duration_seconds, 0 ) AS resident_call_duration_seconds,
						COALESCE ( support_call_duration_seconds, 0 ) AS support_call_duration_seconds,
						COALESCE ( other_call_duration_seconds, 0 ) AS other_call_duration_seconds,

						COALESCE ( lead_wrap_up_time, 0 ) AS lead_wrap_up_time,
						COALESCE ( work_order_wrap_up_time, 0 ) AS work_order_wrap_up_time,
						COALESCE ( resident_wrap_up_time, 0 ) AS resident_wrap_up_time,
						COALESCE ( support_wrap_up_time, 0 ) AS support_wrap_up_time,
						COALESCE ( other_wrap_up_time, 0 ) AS other_wrap_up_time,' . $strResidentSupportSql . '

						COALESCE ( lead_call_center_queue_time, 0 ) AS lead_call_center_queue_time,
						COALESCE ( work_order_call_center_queue_time, 0 ) AS work_order_call_center_queue_time,
						COALESCE ( resident_call_center_queue_time, 0 ) AS resident_call_center_queue_time,
						COALESCE ( support_call_center_queue_time, 0 ) AS support_call_center_queue_time,
						COALESCE ( other_call_center_queue_time, 0 ) AS other_call_center_queue_time,

						COALESCE ( lead_form_completion_count, 0 ) AS lead_form_completion_count,
						COALESCE ( lead_form_incompletion_count, 0 ) AS lead_form_incompletion_count,
						COALESCE ( resident_form_completion_count, 0 ) AS resident_form_completion_count,
						COALESCE ( resident_form_incompletion_count, 0 ) AS resident_form_incompletion_count,
						COALESCE ( work_order_form_completion_count, 0 ) AS work_order_form_completion_count,
						COALESCE ( work_order_form_incompletion_count, 0 ) AS work_order_form_incompletion_count,

						COALESCE ( move_in_answered_calls_count, 0 ) AS move_in_answered_calls_count,
						COALESCE ( move_in_call_center_queue_time, 0 ) AS move_in_call_center_queue_time,
						COALESCE ( move_in_call_duration_seconds, 0 ) AS move_in_call_duration_seconds

					FROM
						(
							SELECT ';
		if( true == $boolIsDownloadReport ) $strSql .= 'c.employee_id AS employee_id, ';
		$strSql .= '			COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.id ELSE NULL
										END ) AS lead_answered_calls_count,
								COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.id ELSE NULL
										END ) AS work_order_answered_calls_count,
								COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.id ELSE NULL
										END ) AS resident_answered_calls_count,
								COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::SUPPORT . '
												AND c.call_type_id = ' . CCallType::CALL_CENTER . '
											THEN c.id ELSE NULL
										END ) AS support_answered_calls_count,
								COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.id ELSE NULL
										END ) AS other_answered_calls_count,

								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_duration_seconds ELSE NULL
										END ) AS lead_call_duration_seconds,
								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_duration_seconds ELSE NULL
										END ) AS work_order_call_duration_seconds,
								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_duration_seconds ELSE NULL
										END ) AS resident_call_duration_seconds,
								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . CCallResult::SUPPORT . '
												AND c.call_type_id = ' . CCallType::CALL_CENTER . '
											THEN c.call_duration_seconds ELSE NULL
										END ) AS support_call_duration_seconds,
								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_duration_seconds ELSE NULL
										END ) AS other_call_duration_seconds,

								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.wrap_up_time ELSE NULL
										END ) AS lead_wrap_up_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.wrap_up_time ELSE NULL
										END ) AS work_order_wrap_up_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.wrap_up_time ELSE NULL
										END ) AS resident_wrap_up_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::SUPPORT . '
												AND c.call_type_id = ' . CCallType::CALL_CENTER . '
											THEN c.wrap_up_time ELSE NULL
										END ) AS support_wrap_up_time,
								SUM ( CASE WHEN
												c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.wrap_up_time ELSE NULL
										END ) AS other_wrap_up_time, ' . $strResidentSupportSelectSql . '

								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_center_queue_time ELSE NULL
										END ) AS lead_call_center_queue_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_center_queue_time ELSE NULL
										END ) AS work_order_call_center_queue_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_center_queue_time ELSE NULL
										END ) AS resident_call_center_queue_time,
								SUM ( CASE WHEN
												c.call_result_id = ' . CCallResult::SUPPORT . '
												AND c.call_type_id = ' . CCallType::CALL_CENTER . '
											THEN c.call_center_queue_time ELSE NULL
										END ) AS support_call_center_queue_time,
								SUM ( CASE WHEN
												c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
												AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											THEN c.call_center_queue_time ELSE NULL
										END ) AS other_call_center_queue_time,

								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_COMPLETE . '
											THEN c.id ELSE NULL
										END ) AS lead_form_completion_count,
								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::LEAD . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_INCOMPLETE . '
											THEN c.id ELSE NULL
										END ) AS lead_form_incompletion_count,
								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_COMPLETE . '
											THEN c.id ELSE NULL
										END ) AS resident_form_completion_count,
								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::RESIDENT . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_INCOMPLETE . '
											THEN c.id ELSE NULL
										END ) AS resident_form_incompletion_count,
								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_COMPLETE . '
											THEN c.id ELSE NULL
										END ) AS work_order_form_completion_count,
								COUNT ( CASE WHEN
												c.call_result_id = ' . CCallResult::WORK_ORDER . '
												AND c.call_form_status_type_id = ' . CCallFormStatusType::CALL_FORM_STATUS_INCOMPLETE . '
											THEN c.id ELSE NULL
										END ) AS work_order_form_incompletion_count,
								COUNT ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_type_id = ' . CCallType::MOVE_IN_CALLS . '
											THEN c.id ELSE NULL
										END ) AS move_in_answered_calls_count,
								SUM ( CASE WHEN
												c.call_type_id = ' . CCallType::MOVE_IN_CALLS . '
											THEN c.call_center_queue_time ELSE NULL
										END ) AS move_in_call_center_queue_time,
								SUM ( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_type_id = ' . CCallType::MOVE_IN_CALLS . '
											THEN c.call_duration_seconds ELSE NULL
										END ) AS move_in_call_duration_seconds
							FROM
								calls AS c
							WHERE
								c.employee_id IN( ' . $objCallFilter->getEmployeeIds() . ' )
								AND c.call_datetime
								BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
								AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'
								AND ( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' )';
		if( true == $boolIsDownloadReport ) $strSql .= ' GROUP BY employee_id ';
		$strSql .= '	) AS sub_query';

		$arrmixCallsData = fetchData( $strSql, $objVoipDatabase );

		return $arrmixCallsData;
	}

	public static function fetchSimpleClientsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valStr( $objCallFilter->getEmployeeIds() ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT cid
					FROM
						calls
					WHERE
						employee_id IN( ' . $objCallFilter->getEmployeeIds() . ' )
						AND created_on BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
						AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59\'
						AND cid IS NOT NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchCalls( $strQuickSearchKeyword, $strWildCardPrefix, $objVoipDatabase, $objAdminDatabase = NULL ) {

		if( false == $strQuickSearchKeyword ) return;

		$intCallIdLength		= 8;
		$intMinSuffixDivisor	= 9;

		$strSql = ' SELECT
						c.id AS call_id,
						c.origin_phone_number,
						c.destination_phone_number,
						c.deleted_on,
						cst.name AS call_status_type_name,
						ca.full_name,
						ct.name AS call_type_name
					FROM
						calls c
						JOIN call_status_types AS cst ON ( c.call_status_type_id = cst.id )
						JOIN call_types AS ct ON ( c.call_type_id = ct.id )
						LEFT JOIN call_agents AS ca ON ( c.employee_id = ca.employee_id )
					WHERE
						c.is_test_call = 0
						AND c.is_auto_detected = 0
						AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL;

		switch( $strWildCardPrefix ) {
			// wild card letter for phone number
			case 'p':
				$strQuickSearchKeyword = preg_replace( '/[^0-9]/', '', $strQuickSearchKeyword );

				$strWhereSql = ' AND ( c.origin_phone_number LIKE \'' . $strQuickSearchKeyword . '%\'
								 OR c.destination_phone_number LIKE \'' . $strQuickSearchKeyword . '%\' )';
				break;

			// wild card letter for call agent
			case 'ca':
				if( false == valId( $strQuickSearchKeyword ) ) {
					$strWhereSql = ' AND ca.full_name ILIKE \'%' . $strQuickSearchKeyword . '%\' OR ca.username ILIKE \'%' . $strQuickSearchKeyword . '%\'';
				} else {
					$strWhereSql = ' AND ca.id = ' . $strQuickSearchKeyword . ' ';
				}
				break;

			// wild card letter for call type
			case 'ct':
				$strWhereSql = ' AND ( ct.name ILIKE \'%' . $strQuickSearchKeyword . '%\' OR ct.id = ' . ( int ) $strQuickSearchKeyword . ' )';
				break;

			case 't':
				$arrintTask = \Psi\Eos\Admin\CTasks::createService()->fetchCallIdByTaskId( $strQuickSearchKeyword, $objAdminDatabase );
				$intCallId = $arrintTask[0]['call_id'];
				$strWhereSql = ' AND c.id = ' . ( int ) $intCallId;
				break;

			default:
				$strSearchKeywordMaxSuffix = pow( 10,  $intCallIdLength - strlen( $strQuickSearchKeyword ) ) - 1;
				$strSearchKeywordMinSuffix = $strSearchKeywordMaxSuffix / $intMinSuffixDivisor;

				if( 0 == $strSearchKeywordMaxSuffix ) {
					$strSearchKeywordMaxSuffix = '';
					$strSearchKeywordMinSuffix = '';
				}

				if( false == valId( $strQuickSearchKeyword . $strSearchKeywordMaxSuffix ) || false == valId( $strQuickSearchKeyword . $strSearchKeywordMinSuffix ) ) return false;

				$strWhereSql = ' AND ( c.id <= \'' . $strQuickSearchKeyword . $strSearchKeywordMaxSuffix . '\' ';
				$strWhereSql .= ' AND c.id >= \'' . $strQuickSearchKeyword . $strSearchKeywordMinSuffix . '\' ';
				$strWhereSql .= 'AND c.id::TEXT LIKE \'' . $strQuickSearchKeyword . '%\' )';
				break;
		}

		$strSql .= $strWhereSql;
		$strSql .= 'ORDER BY c.id DESC LIMIT 10';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchCallsByCallFilter( $objCallFilter, $objVoipDatabase, $intCallsCount = 10 ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		if( true == valId( $objCallFilter->getEmployeeId() ) ) {
			$strSqlClause = ' AND c.employee_id = ' . $objCallFilter->getEmployeeId();
		}

		$strSql = 'SELECT
						c.id AS call_id,
						c.origin_phone_number,
						c.destination_phone_number,
						cst.name AS call_status_type_name,
						cr.name AS call_result_name,
						ct.name AS call_type_name
					FROM
						calls c
						JOIN call_status_types AS cst ON ( c.call_status_type_id = cst.id )
						JOIN call_results AS cr ON ( c.call_result_id = cr.id )
						JOIN call_types AS ct ON ( c.call_type_id = ct.id )
					WHERE
						( CAST( c.id AS text ) ILIKE \'' . $objCallFilter->getCallId() . '%\'
						OR c.origin_phone_number ILIKE \'%' . implode( '%\' AND c.origin_phone_number ILIKE \'%', $objCallFilter->getPhoneNumber() ) . '%\'
						OR c.destination_phone_number ILIKE \'%' . implode( '%\' AND c.destination_phone_number ILIKE \'%', $objCallFilter->getPhoneNumber() ) . '%\' )
						' . $strSqlClause . '
					ORDER BY
						c.id ASC
					LIMIT
						' . ( int ) $intCallsCount;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsByOutboundCallIds( $arrintOutboundCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallIds ) ) return NULL;

		$strSql = 'SELECT * FROM calls WHERE outbound_call_id IN ( ' . implode( ',', $arrintOutboundCallIds ) . ' )';
		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallsByCallTrackerFilter( $objPagination, $objCallsFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		if( false == valObj( $objCallsFilter, 'CCallFilter' ) ) return NULL;

		$strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';

		if( true == valStr( $objCallsFilter->getCallStatusTypeIds() ) ) {
			$arrstrWhereSql[] = 'c.call_status_type_id IN ( ' . $objCallsFilter->getCallStatusTypeIds() . ' )';
		}

		if( true == valStr( $objCallsFilter->getCallIds() ) ) {
			$arrintCallIds	= [];
			$strCallRange	= '';
			$arrstrCallIds	= explode( ',', $objCallsFilter->getCallIds() );

			if( true == valArr( $arrstrCallIds ) ) {
				foreach( $arrstrCallIds as $strCallId ) {
					if( true == ( 0 < \Psi\CStringService::singleton()->strpos( $strCallId, '-' ) ) ) {
						$arrintCallRangeIds = explode( '-', $strCallId );

						if( true == valArr( $arrintCallRangeIds ) && true == ( 2 == \Psi\Libraries\UtilFunctions\count( $arrintCallRangeIds ) ) ) {
							$strCallRange .= ' OR c.id BETWEEN ' . ( int ) $arrintCallRangeIds[0] . ' AND ' . ( int ) $arrintCallRangeIds[1];
						}
					} elseif( true == is_numeric( trim( $strCallId ) ) ) {
						$arrintCallIds[] = $strCallId;
					}
				}
			}

			if( true == valArr( $arrintCallIds ) ) {
				$arrstrWhereSql[] = ' ( c.id IN ( ' . implode( ',', $arrintCallIds ) . ' )' . $strCallRange . ' ) ';
			} elseif( true == ( 0 < strlen( $strCallRange ) ) ) {
				$arrstrWhereSql[] = \Psi\CStringService::singleton()->substr( $strCallRange, 3, strlen( $strCallRange ) );
			}
		}

		if( true == valStr( $objCallsFilter->getGenericData() ) ) {
			$arrstrWhereSql[] = '( CAST( c.id AS text ) LIKE \'' . addslashes( $objCallsFilter->getGenericData() ) . '%\' )';
		} else {
			if( true == valStr( $objCallsFilter->getAfterDatetime() ) && true == valStr( $objCallsFilter->getBeforeDatetime() ) ) {
				$arrstrWhereSql[] = ' DATE_TRUNC( \'minute\', c.call_datetime ) BETWEEN \'' . $objCallsFilter->getAfterDatetime() . '\' AND \'' . $objCallsFilter->getBeforeDatetime() . '\'';
			}
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT COUNT(c.id) AS count FROM calls c';
		} else {
			$strSql = 'SELECT
							c.*,cst.name AS call_status_type_name
						FROM
							calls AS c
							JOIN call_status_types AS cst
							ON c.call_status_type_id = cst.id';

			$strSqlOrderBy		= ' ORDER BY c.call_datetime DESC';
			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valArr( $arrstrWhereSql ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhereSql );
		}

		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolIsCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchCalls( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchQuickSearchCallTrackerCalls( $strSearchKeyword, $arrintCurrentCallStatusTypes, $objVoipDatabase ) {
		$strSql = 'SELECT
						c.*,cst.name AS call_status_type_name
					FROM
						calls AS c
						JOIN call_status_types AS cst
						ON c.call_status_type_id = cst.id
					WHERE c.call_status_type_id IN (' . implode( ',', array_keys( $arrintCurrentCallStatusTypes ) ) . ')
						AND ( CAST( c.id AS text ) LIKE \'' . addslashes( $strSearchKeyword ) . '%\' )
						AND c.is_test_call = 0
						AND c.is_auto_detected = 0
						AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL . '
					ORDER BY c.call_datetime DESC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchTodaysCompletedCallsByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		$objVoipDatabase->validateAndOpen();

		$strSql = 'SELECT
						COUNT( 1 )
					FROM
						calls c
					WHERE
						c.employee_id = ' . ( int ) $intEmployeeId . '
						AND c.created_on::date = now()::date
						AND c.call_type_id = ' . CCallType::PSI_OFFICE . '
						AND c.call_result_id != ' . CCallResult::TEST_CALL;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallTodaysTotal( $strWhereCondition = NULL, $objVoipDatabase ) {
		if( 'phone_abandon_month_to_date' == $strWhereCondition ) {
			$strWhereCondition = ' c.call_datetime BETWEEN ( date_trunc( \'month\', NOW() ) ) AND NOW() ';
		} elseif( 'phone_abandon_rolling_thirty_days' == $strWhereCondition ) {
			$strWhereCondition = ' c.call_datetime > \'' . date( 'm/d/Y', strtotime( '-30 days' ) ) . '\'';
			$strWhereCondition .= ' AND cm.created_on > \'' . date( 'm/d/Y', strtotime( '-30 days' ) ) . '\'';
		} elseif( 'phone_abandon_yesterday' == $strWhereCondition ) {
			$strWhereCondition = ' c.created_on >= CURRENT_DATE - \'1 days\'::interval AND c.call_datetime::date < now()::date ';
		} elseif( 'phone_abandon_weekend' == $strWhereCondition ) {
			$strWhereCondition = ' c.call_datetime BETWEEN ( now() - INTERVAL \'2 days\' ) AND now() ';
		} else {
			$strWhereCondition = ' c.call_datetime BETWEEN \'' . date( 'Y-m-d' ) . ' 00:00:00 \' AND \'' . date( 'Y-m-d' ) . ' 23:59:59.999999 \' ';
		}

		$strSql = 'SELECT
						COALESCE(SUM (CASE WHEN call_status_type_id = ' . CCallStatusType::ANSWERED . ' THEN 1 ELSE 0 END),0) calls_taken,
						COALESCE(SUM (CASE WHEN call_status_type_id = ' . CCallStatusType::UNANSWERED . ' THEN 1 ELSE 0 END),0) abandoned
					FROM
						calls c
						JOIN call_members cm ON cm.call_id = c.id
					WHERE
							' . $strWhereCondition . '
							AND c.call_type_id = ' . CCallType::CALL_CENTER . '
							AND c.call_result_id != ' . CCallResult::TEST_CALL . '
							AND cm.call_queue_id IN( ' . CCallQueue::SUPPORT_ACCOUNTING_ENTRATA_CORE . ', ' . CCallQueue::SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS . ', ' . CCallQueue::SUPPORT_GENERAL_SUPPORT . ', ' . CCallQueue::COLONIAL_SUPPORT . ', ' . CCallQueue::SUPPORT_GID_WINDSOR . ', ' . CCallQueue::SUPPORT_MARKETING_WEBSITE_PRODUCTS . ', ' . CCallQueue::SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT . ', ' . CCallQueue::SUPPORT_UDR . ', ' . CCallQueue::SUPPORT_TIER_TWO . ')
						';

		$arrintTaskStats = fetchData( $strSql, $objVoipDatabase );

		return $arrintTaskStats;
	}

	public static function fetchSupportCallStatsForTvDisplay( $objVoipDatabase ) {
		$objVoipDatabase->validateAndOpen( false );
		$arrstrCallQueueIds = [ CCallQueue::COLONIAL_SUPPORT, CCallQueue::SUPPORT_GENERAL_SUPPORT, CCallQueue::SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS, CCallQueue::SUPPORT_MARKETING_WEBSITE_PRODUCTS,CCallQueue::SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT, CCallQueue::SUPPORT_ACCOUNTING_ENTRATA_CORE,CCallQueue::SUPPORT_GID_WINDSOR,CCallQueue::SUPPORT_UDR,CCallQueue::SUPPORT_TIER_TWO ];

		$strSql = 'SELECT
							SUM(CASE WHEN call_status_type_id = ' . CCallStatusType::UNANSWERED . ' THEN 1 ELSE 0 END) AS unanswered,
							COUNT (*) AS total_calls,
							COALESCE(ROUND((SUM (CASE WHEN call_status_type_id = ' . CCallStatusType::UNANSWERED . ' THEN 1 ELSE 0 END)::integer/COUNT (*)::numeric(10,2))*100,1),0) AS percent_unanswered,
							TO_CHAR((AVG(response_time)::integer || \' second\')::interval, \'MI:SS\') AS average_response_time,
							round(AVG ( response_time ) ) AS average_response_time_in_seconds,
							COALESCE(SUM (CASE WHEN b_start_time IS NULL AND b_end_time IS NULL THEN 1 ELSE 0 END),0) AS calls_in_queue,
							TO_CHAR((COALESCE(AVG(wait_time)::integer, 0) || \' second\')::interval, \'MI:SS\') AS longest_queue_time,
							round(AVG ( wait_time ) ) AS longest_queue_time_in_seconds
						FROM
							(SELECT
								c.id,
								c.call_center_talk_time,
								c.call_datetime,
								c.call_status_type_id,
								c.call_center_queue_time,
								c.call_result_id,
								queue.updated_on::time queue_time,
								bstart.updated_on::time b_start_time,
								bend.updated_on::time b_end_time,
								COALESCE( ceil(EXTRACT(EPOCH FROM (bstart.updated_on - queue.updated_on) ) ),ceil(EXTRACT(EPOCH FROM (bend.updated_on - queue.updated_on) ) ), ceil(EXTRACT(EPOCH FROM (now() - queue.updated_on) ) )) response_time
							FROM
								calls C
								LEFT JOIN call_events queue ON queue.call_id = c.id AND queue.call_event_type_id = ' . CCallEventType::IN_QUEUE . '
								LEFT JOIN call_events bstart ON bstart.call_id = c.id AND bstart.call_event_type_id = ' . CCallEventType::BRIDGE_START . '
								LEFT JOIN call_events bend ON bend.call_id = c.id AND bend.call_event_type_id = ' . CCallEventType::CALL_STOP . '
							WHERE
								c.call_datetime BETWEEN (now() - interval \'1 hour\') AND now()
								AND c.call_type_id = ' . CCallType::CALL_CENTER . '
								AND c.call_result_id NOT IN ( ' . CCallResult::TEST_CALL . ', ' . CCallResult::SIP_TEST_CALL . ' )
								AND c.call_queue_id IN (' . implode( ',', $arrstrCallQueueIds ) . ')
					 		) sub
						LEFT JOIN
						(
							SELECT
								c.id,
								EXTRACT(EPOCH FROM (now() - queue.updated_on) ) wait_time
							FROM
								calls C
								LEFT JOIN call_events queue ON queue.call_id = c.id AND queue.call_event_type_id = ' . CCallEventType::IN_QUEUE . '
							WHERE
								c.call_datetime BETWEEN (now() - interval \'1 hour\') AND now()
								AND c.call_type_id = ' . CCallType::CALL_CENTER . '
								AND c.call_result_id NOT IN ( ' . CCallResult::TEST_CALL . ', ' . CCallResult::SIP_TEST_CALL . ' )
								AND c.call_queue_id IN (' . implode( ',', $arrstrCallQueueIds ) . ')
								AND c.call_status_type_id IN ( ' . CCallStatusType::IN_CALL_CENTER_QUEUE . ' )
							ORDER BY
								wait_time DESC
								LIMIT 1
						) sub2 ON sub2.id = sub.id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchAvailableAgentsForCall( $objVoipDatabase ) {
		$objVoipDatabase->validateAndOpen( false );
		$arrintCallAgentStatusTypes = [ CCallAgentStatusType::AVAILABLE, CCallAgentStatusType::AVAILABLE_ON_DEMAND, CCallAgentStatusType::ON_BREAK, CCallAgentStatusType::UNUSED, CCallAgentStatusType::ON_LUNCH, CCallAgentStatusType::ON_PROJECT, CCallAgentStatusType::ON_MEETING, CCallAgentStatusType::ON_TRAINING, CCallAgentStatusType::ON_COACHING, CCallAgentStatusType::IN_EMAIL_QUEUE, CCallAgentStatusType::IN_LEASING_CHAT ];

		$strSql = ' SELECT
						COALESCE(SUM ( CASE
								WHEN call_agent_state_type_id = ' . CCallAgentStateType::WAITING . ' AND call_agent_status_type_id IN ( ' . CCallAgentStatusType::AVAILABLE . ' , ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' ) THEN 1
								ELSE 0
							END ),0) AS available_for_calls,
						COALESCE(SUM ( CASE
								WHEN call_agent_state_type_id IN ( ' . CCallAgentStateType::WAITING . ' , ' . CCallAgentStateType::RECEIVING . ' ) THEN 1
								ELSE 0
							END ),0) AS on_call,
						COALESCE(SUM ( CASE
								WHEN call_agent_state_type_id = ' . CCallAgentStateType::IDLE . ' THEN 1
								ELSE 0
							END ),0) AS unavailable
					FROM
					(
						SELECT
							ca.full_name,
							ca.domain,
							ca.offered_call_queue_id,
							ca.updated_on::TIMESTAMP,
							ca.call_agent_state_type_id,
							ca.call_agent_status_type_id,
							row_number() OVER ( PARTITION BY ca.full_name ORDER BY ca.full_name, ca.updated_on DESC )
						FROM
							call_agents ca
							LEFT JOIN
							(
								SELECT
									*
								FROM
								(
									SELECT
										c.id AS call_id,
										c.employee_id AS call_employee_id
									FROM
										calls c
									WHERE
										c.call_type_id IN ( ' . CCallType::CALL_CENTER . ' )
										AND c.call_status_type_id IN ( ' . CCallStatusType::OFFERING_TO_DESTINATION . ' , ' . CCallStatusType::CALL_IN_PROGRESS . ' , ' . CCallStatusType::IN_CALL_CENTER_QUEUE . ' )
										AND c.call_datetime::DATE = now()::DATE
								) AS sub_query
						 	) AS call_query ON call_query.call_employee_id = ca.employee_id
						LEFT JOIN call_agent_queues caq ON ( caq.call_agent_id = ca.id ) AND caq.call_queue_id NOT IN ( ' . CCallQueue::SUPPORT_TIER_TWO . ' , ' . CCallQueue::SUPPORT_UDR . ' , ' . CCallQueue::SUPPORT_GID_WINDSOR . ' )
						LEFT JOIN call_queues cq ON cq.id = caq.call_queue_id
					WHERE
						ca.call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypes ) . ' )
						AND ca.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
						AND cq.description IS NOT NULL
					) AS main_query
				WHERE
				row_number = 1';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsCountByEmployeeIdByStatusTypeIds( $intEmployeeId, $arrintCallStatusTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'WITH call_agent_queues_custom AS (
														SELECT
															caq.call_queue_id AS call_agent_queue_id
														FROM
															call_agents AS ca
															JOIN call_agent_queues caq ON ( caq.call_agent_id = ca.id
																							AND ca.employee_id = ' . ( int ) $intEmployeeId . ' )
													)
					SELECT
						COUNT ( DISTINCT ( c.id ) ) AS total_calls
					FROM
						calls c
						JOIN call_members cm ON ( cm.call_id = c.id )
						JOIN call_queues cq ON ( cm.call_queue_id = cq.id )
						JOIN call_agent_queues_custom caqc ON ( cq.id = caqc.call_agent_queue_id )
					WHERE
						c.call_status_type_id IN ( ' . implode( ',', $arrintCallStatusTypeIds ) . ' )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataForLeasingCenterServiceLevelReport( $intCid, $arrintPropertyIds, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) && false == valId( $intCid ) ) return NULL;
		$strWhereSql = '';
		$strWhereSql .= ' AND c.cid = ' . ( int ) $intCid . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';
		$strLeasingCenterCallTypeIds = implode( ',', ( CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) );

		$strSql = 'WITH i AS (
								SELECT
									DATE_TRUNC( \'minutes\', n ) AS interval_start_time,
									DATE_TRUNC( \'minutes\', ( n + INTERVAL \'30 minutes\' ) ) AS interval_end_time
								FROM
									generate_series( \'' . date( $strStartDate ) . ' 00:00:00\', \'' . date( $strEndDate ) . ' 23:59:59\', \'30 minute\'::INTERVAL ) n
							)
								SELECT
									( CASE
										WHEN ( 0 = ( answered_calls_count + unanswered_calls_in_more_than_thirty_second_count ) ) THEN 100
										WHEN ( 0 < ( answered_calls_count + unanswered_calls_in_more_than_thirty_second_count ) AND 0 < answered_calls_in_thirty_second_count ) THEN
											( ( answered_calls_in_thirty_second_count * 100 ) / ( answered_calls_count + unanswered_calls_in_more_than_thirty_second_count ) )
										ELSE 0
									END ) AS service_level_in_thirty_minute_count
								FROM (
									SELECT
										TO_CHAR( i.interval_start_time , \'YYYY/MM/DD HH:MI AM\' ) AS call_datetime,
										COUNT( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_type_id IN ( ' . $strLeasingCenterCallTypeIds . ' ) ' . $strWhereSql . '
												THEN c.id ELSE NULL
											END ) AS answered_calls_count,
										COUNT( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::UNANSWERED . '
												AND c.call_type_id IN ( ' . $strLeasingCenterCallTypeIds . ' )
												AND ( COALESCE ( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) < 31 )' . $strWhereSql . '
												THEN c.id ELSE NULL
											END ) AS unanswered_calls_in_thirty_second_count,
										COUNT( CASE WHEN
												c.call_status_type_id = ' . CCallStatusType::UNANSWERED . '
												AND c.call_type_id IN ( ' . $strLeasingCenterCallTypeIds . ' )
												AND ( COALESCE ( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) > 30 )' . $strWhereSql . '
												THEN c.id ELSE NULL
											END ) AS unanswered_calls_in_more_than_thirty_second_count,
										COUNT( CASE	WHEN
												c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
												AND c.call_type_id IN ( ' . $strLeasingCenterCallTypeIds . ' )
												AND COALESCE( c.call_center_queue_time, 0 ) < 31 ' . $strWhereSql . '
												THEN c.id ELSE NULL
											END ) AS answered_calls_in_thirty_second_count
										FROM
											i
											LEFT JOIN calls AS c ON ( c.call_datetime >= i.interval_start_time AND c.call_datetime < i.interval_end_time )
										GROUP BY
											i.interval_start_time,
											i.interval_end_time
										ORDER BY
											i.interval_start_time ) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataForLeasingCenterCallQueueTimeReport( $intCid, $arrintPropertyIds, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) && false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						average_call_queue_time
					FROM
					(
						SELECT
							AVG( CASE
									WHEN
										c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
										AND c.call_type_id IN ( ' . implode( ',', ( CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) ) . ' )
									THEN COALESCE( c.call_center_queue_time, 0 )
								END ) AS average_call_queue_time
						FROM
							calls AS c
						WHERE
							c.cid = ' . ( int ) $intCid . '
							AND ( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' )
							AND c.call_datetime BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
							AND c.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ') ) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataForLeasingCenterPerformanceReport( $intCid, $arrintPropertyIds, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) && false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						COUNT(
								CASE WHEN call_result_id = ' . CCallResult::LEAD . '
								THEN 1
						END ) AS number_of_leads,
						COUNT(
							CASE WHEN call_result_id = ' . CCallResult::WORK_ORDER . '
							THEN 1
						END ) AS number_of_work_orders,
						COUNT(
							CASE WHEN call_result_id = ' . CCallResult::RESIDENT . '
							THEN 1
						END ) AS number_of_resident_calls,
						COUNT(
						CASE WHEN call_result_id NOT IN ( ' . CCallResult::TEST_CALL . ',' . CCallResult::LEAD . ',' . CCallResult::WORK_ORDER . ',' . CCallResult::RESIDENT . ' )
							THEN 1
						END ) AS number_of_other_calls
					FROM
						calls
					WHERE
						cid = ' . ( int ) $intCid . '
						AND call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
						AND call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND DATE_TRUNC( \'day\', call_datetime ) BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59.999999\'
						AND call_result_id <> ' . CCallResult::TEST_CALL;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataForLeasingCenterConversionReport( $intCid, $arrintPropertyIds, $strStartDate, $strEndDate, $objVoipDatabase, $objAdminDatabase, $objClientDatabase ) {
		if( false == valArr( $arrintPropertyIds ) && false == valId( $intCid ) ) return NULL;

		$arrstrMixData = [];

		$strSql = 'SELECT
						c.employee_id,
						c.id
					FROM
						calls AS c
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
						AND c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
						AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
						AND c.employee_id IS NOT NULL
						AND DATE_TRUNC( \'day\', c.call_datetime ) BETWEEN \'' . $strStartDate . ' 00:00:00\'
						AND \'' . $strEndDate . ' 23:59:59.999999\'';

		$arrobjCalls = CCalls::fetchCalls( $strSql, $objVoipDatabase );

		if( false == valArr( $arrobjCalls ) ) return NULL;
		$arrintCallIds			= array_keys( rekeyObjects( 'Id', $arrobjCalls ) );
		$arrintEmployeeIds		= array_keys( rekeyObjects( 'EmployeeId', $arrobjCalls ) );
		$arrintUserIds			= array_keys( array_filter( ( array ) CUsers::fetchUsersByEmployeeIds( $arrintEmployeeIds, $objAdminDatabase ) ) );

		if ( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = ' SELECT
							DISTINCT( ca.id )
						FROM
							cached_applications ca
						WHERE
							ca.cid = ' . ( int ) $intCid . '
							AND ca.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
							AND ca.lease_interval_type_id = ' . CLeaseIntervalType::APPLICATION . '
							AND ca.ps_product_id = ' . ( int ) CPsProduct::LEASING_CENTER . '
							-- AND ca.created_by IN ( ' . implode( ',', $arrintUserIds ) . ' )
							AND ca.call_id IN ( ' . implode( ',', $arrintCallIds ) . ' )
							AND ca.application_datetime BETWEEN \'' . $strStartDate . ' 00:00:00\'
							AND \'' . $strEndDate . ' 23:59:59\'';

		$arrobjApplications		= CApplications::fetchApplications( $strSql, $objClientDatabase );

		if ( false == valArr( $arrobjApplications ) ) return NULL;
		$arrintApplicationIds	= array_keys( rekeyObjects( 'Id', $arrobjApplications ) );

		$strSql = 'SELECT
						COUNT( id ) AS guest_cards_inserted_count
					FROM
						applications a
					WHERE
						a.cid = ' . ( int ) $intCid . '
						AND a.id IN ( ' . implode( ',', $arrintApplicationIds ) . ' )';

		$arrmixGuestCardsCreatedStats = fetchData( $strSql, $objClientDatabase );
		$arrstrMixData['guest_cards_inserted_count'] = $arrmixGuestCardsCreatedStats[0]['guest_cards_inserted_count'];

		$strSql = 'SELECT
						COUNT( e.id ) AS event_reference_count
					FROM
						events e
						JOIN applications a ON( a.cid = e.cid AND e.property_id = a.property_id AND a.lease_interval_id = e.lease_interval_id )
						JOIN company_users cu ON ( cu.id = e.created_by AND cu.cid = e.cid )
					WHERE
						cu.company_user_type_id IN ( ' . implode( ',', CCompanyUserType::$c_arrintPsiUserTypeIds ) . ' )
						AND e.event_type_id = ' . ( int ) CEventType::SCHEDULED_APPOINTMENT . '
						AND e.cid = ' . ( int ) $intCid . '
						AND e.created_on BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59\'
						AND a.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND e.ps_product_id = ' . ( int ) CPsProduct::LEASING_CENTER;

		$arrmixAppointmentsSetStats = fetchData( $strSql, $objClientDatabase );
		$arrstrMixData['event_reference_count'] = $arrmixAppointmentsSetStats[0]['event_reference_count'];

		$strSql = 'SELECT
						COUNT( c.id ) AS leads_handled
					FROM
						calls AS c
					WHERE
						c.cid = ' . ( int ) $intCid . '
						AND c.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
						AND c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
						AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
						AND DATE_TRUNC( \'day\', c.call_datetime ) BETWEEN \'' . $strStartDate . ' 00:00:00\'
						AND \'' . $strEndDate . ' 23:59:59.999999\'';

		$arrmixLeadAnsweredCallsStats = fetchData( $strSql, $objVoipDatabase );
		$arrstrMixData['leads_handled'] = $arrmixLeadAnsweredCallsStats[0]['leads_handled'];

		return $arrstrMixData;
	}

	public static function generateSelectSqlForLeasingCenterDistributionReport( $strCallsCountFor, $strDay ) {
		if( false == valStr( $strCallsCountFor ) && false == valStr( $strDay ) ) return NULL;

		$strSelectSql = 'COALESCE ( ' . $strCallsCountFor . '_lead_count_' . $strDay . ', 0 ) AS ' . $strCallsCountFor . '_lead_count_' . $strDay . ',
						 COALESCE ( ' . $strCallsCountFor . '_resident_count_' . $strDay . ', 0 ) AS ' . $strCallsCountFor . '_resident_count_' . $strDay . ',
						 COALESCE ( ' . $strCallsCountFor . '_work_order_count_' . $strDay . ', 0 ) AS ' . $strCallsCountFor . '_work_order_count_' . $strDay . ',
						 COALESCE ( ' . $strCallsCountFor . '_other_count_' . $strDay . ', 0 ) AS ' . $strCallsCountFor . '_other_count_' . $strDay;
		return $strSelectSql;
	}

	public static function generateSqlForLeasingCenterDistributionReport( $intPropertyId, $arrintCallTypeIds, $arrstrSqls, $arrstrDays, $intCallStatusTypeId = NULL ) {
		if( false == valId( $intPropertyId ) && false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strCallsCountSql	= 'SELECT ';
		$strCountSql		= ' FROM ( SELECT ';

		$strFirstDay = $arrstrDays[0];
		foreach( $arrstrDays as $strDay ) {
			$strDay = \Psi\CStringService::singleton()->strtolower( $strDay );
			if( true == isset( $arrstrSqls[$strDay . '_sql'] ) && true == isset( $arrstrSqls[$strDay . '_count_sql'] ) ) {

				if( $strFirstDay == $strDay && '' != $arrstrSqls[$strDay . '_sql'] ) {
					$strCallsCountSql	= $strCallsCountSql . $arrstrSqls['mon_sql'];
					$strCountSql		= $strCountSql . $arrstrSqls['mon_count_sql'];
				}

				if( $strFirstDay != $strDay && '' != $arrstrSqls[$strDay . '_sql'] && 'SELECT ' != $strCallsCountSql ) {
					$strCallsCountSql	= $strCallsCountSql . ',' . $arrstrSqls[$strDay . '_sql'];
					$strCountSql		= $strCountSql . ',' . $arrstrSqls[$strDay . '_count_sql'];
				} elseif( $strFirstDay != $strDay ) {

					$strCallsCountSql	= $strCallsCountSql . $arrstrSqls[$strDay . '_sql'];
					$strCountSql		= $strCountSql . $arrstrSqls[$strDay . '_count_sql'];
				}
			}
		}

		$strCallsCountSql = $strCallsCountSql . $strCountSql;
		$strCallsCountSql = $strCallsCountSql . ' FROM calls AS c WHERE c.property_id = ' . ( int ) $intPropertyId . ' AND c.call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' ) AND c.call_status_type_id = ' . ( int ) $intCallStatusTypeId . ' ) AS sub_query';
		return $strCallsCountSql;
	}

	public static function generateCountSqlForLeasingCenterDistributionReport( $arrintDates, $strOpenTime, $strCloseTime, $strPropertyTimeZoneName, $strDay ) {
		if( false == valArr( $arrintDates ) ) return NULL;

		$arrintOtherCallResultIds = [
			CCallResult::VOICE_MAIL,
			CCallResult::UNKNOWN,
			CCallResult::NO_RESULT,
			CCallResult::TASK_CREATED,
			CCallResult::SALE,
			CCallResult::SOLICITOR,
			CCallResult::VENDOR,
			CCallResult::ABANDONED,
			CCallResult::WRONG_NUMBER
		];

		if( true == is_null( $strOpenTime ) && true == is_null( $strCloseTime ) ) {
			$strOpenTime	= '00:00';
			$strCloseTime	= '23:59';
		} elseif( true == is_null( $strOpenTime ) && false == is_null( $strCloseTime ) ) {
			$strOpenTime = '00:00';
		} elseif( false == is_null( $strOpenTime ) && true == is_null( $strCloseTime ) ) {
			$strCloseTime = '23:59';
		}

		$strDuringHoursClause	= '( 1 = 1 )';
		$strAfterHoursClause	= '( 1 = 1 )';
		$strOnSiteHoursClause	= '( 1 = 1 )';

		foreach( $arrintDates as $intDate ) {
			if( '( 1 = 1 )' == $strDuringHoursClause ) {
				$strDuringHoursClause = $strDuringHoursClause . ' AND ( (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' ' . $strOpenTime . '\'
																			AND date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' ' . $strCloseTime . '\' ) ';
			} else {
				$strDuringHoursClause = $strDuringHoursClause . ' OR (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' ' . $strOpenTime . '\'
																		AND date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' ' . $strCloseTime . '\' ) ';
			}

			if( '( 1 = 1 )' == $strAfterHoursClause ) {
				$strAfterHoursClause = $strAfterHoursClause . ' AND ( ( (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' < \'' . $intDate . ' ' . $strOpenTime . '\'
																			OR date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' > \'' . $intDate . ' ' . $strCloseTime . '\' )
																			AND ( c.call_datetime AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' 00:00:00\' AND c.call_datetime AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' 23:59:00\' ) ) ';
			} else {
				$strAfterHoursClause = $strAfterHoursClause . ' OR ( (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' < \'' . $intDate . ' ' . $strOpenTime . '\'
																		OR date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' > \'' . $intDate . ' ' . $strCloseTime . '\' )
																		AND ( c.call_datetime AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' 00:00:00\' AND c.call_datetime AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' 23:59:00\' ) )';
			}

			if( '( 1 = 1 )' == $strOnSiteHoursClause ) {
				$strOnSiteHoursClause = $strOnSiteHoursClause . ' AND ( (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' ' . $strOpenTime . '\'
																			AND date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' ' . $strCloseTime . '\' ) ';
			} else {
				$strOnSiteHoursClause = $strOnSiteHoursClause . ' OR (	date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' >= \'' . $intDate . ' ' . $strOpenTime . '\'
																		AND date_trunc( \'minute\', c.call_datetime ) AT TIME ZONE \'' . $strPropertyTimeZoneName . '\' <= \'' . $intDate . ' ' . $strCloseTime . '\' ) ';
			}
		}

		if( '( 1 = 1 )' != $strDuringHoursClause ) {
			$strDuringHoursClause = $strDuringHoursClause . ' ) ';
		}

		if( '( 1 = 1 )' != $strAfterHoursClause ) {
			$strAfterHoursClause = $strAfterHoursClause . ' ) ';
		}

		if( '( 1 = 1 )' != $strOnSiteHoursClause ) {
			$strOnSiteHoursClause = $strOnSiteHoursClause . ' ) ';
		}

		$arrstrCountSql['during_hours_count_sql']	= 'COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::LEAD . ' AND ( ' . $strDuringHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_during_business_hours_lead_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::RESIDENT . ' AND ( ' . $strDuringHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_during_business_hours_resident_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::WORK_ORDER . ' AND ( ' . $strDuringHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_during_business_hours_work_order_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id IN ( ' . implode( ',', $arrintOtherCallResultIds ) . ' ) AND ( ' . $strDuringHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_during_business_hours_other_count_' . $strDay;

		$arrstrCountSql['after_hours_count_sql']	= 'COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::LEAD . ' AND ( ' . $strAfterHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_after_business_hours_lead_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::RESIDENT . ' AND ( ' . $strAfterHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_after_business_hours_resident_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id = ' . CCallResult::WORK_ORDER . ' AND ( ' . $strAfterHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_after_business_hours_work_order_count_' . $strDay . ',
														COUNT ( CASE
																	WHEN c.call_result_id IN ( ' . implode( ',', $arrintOtherCallResultIds ) . ' ) AND ( ' . $strAfterHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS lc_answered_after_business_hours_other_count_' . $strDay;

		$arrstrCountSql['on_site_hours_count_sql']	= 'COUNT ( CASE
																	WHEN c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' AND ( ' . $strOnSiteHoursClause . ' )
																	THEN c.id
																	ELSE NULL
																END ) AS on_site_answered_during_hours_count_' . $strDay;

		return $arrstrCountSql;
	}

	public static function fetchTopThreeAgentsWhoTookMostCalls( $objVoipDatabase ) {
		$strSql = 'SELECT
						c.employee_id,
						COUNT(c.id) AS count
					FROM
						calls c
						JOIN call_status_types cst ON cst.id = c.call_status_type_id
						JOIN call_results cr ON cr.id = c.call_result_id
						JOIN call_members cm ON cm.call_id = c.id
						JOIN call_queues cq ON cq.id = cm.call_queue_id
					WHERE
						c.call_datetime > now() - \'7 day\' :: INTERVAL
						AND c.call_type_id = ' . CCallType::CALL_CENTER . '
						AND cr.id != ' . CCallResult::TEST_CALL . '
						AND c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND cq.id IN ( ' . CCallQueue::SUPPORT_ACCOUNTING_ENTRATA_CORE . ', ' . CCallQueue::SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS . ', ' . CCallQueue::SUPPORT_GENERAL_SUPPORT . ', ' . CCallQueue::COLONIAL_SUPPORT . ', ' . CCallQueue::SUPPORT_GID_WINDSOR . ', ' . CCallQueue::SUPPORT_MARKETING_WEBSITE_PRODUCTS . ', ' . CCallQueue::SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT . ', ' . CCallQueue::SUPPORT_UDR . ', ' . CCallQueue::SUPPORT_TIER_TWO . ')
					GROUP BY
						c.employee_id
					ORDER BY
						count desc
					LIMIT 3';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchAbandonedCallsPercentage( $arrmixPreBuildSlideFilters, $objVoipDatabase ) {
		$strSql = 'SELECT
						SUM (CASE WHEN c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' THEN 1 ELSE 0 END) "Answered",
						SUM (CASE WHEN c.call_status_type_id = ' . CCallStatusType::UNANSWERED . ' THEN 1 ELSE 0 END) "Abandoned"
					FROM
						calls C
						JOIN call_results cr ON cr.id = c.call_result_id
						JOIN call_members cm ON cm.call_id = c.id
						JOIN call_queues cq ON cq.id = cm.call_queue_id
					WHERE
						c.call_type_id = ' . CCallType::CALL_CENTER . '
						AND cr.id != ' . CCallResult::TEST_CALL . '
						AND cq.id IN ( ' . CCallQueue::SUPPORT_ACCOUNTING_ENTRATA_CORE . ', ' . CCallQueue::SUPPORT_PM_SUPPORT_RESIDENT_QUESTIONS . ', ' . CCallQueue::SUPPORT_GENERAL_SUPPORT . ', ' . CCallQueue::COLONIAL_SUPPORT . ', ' . CCallQueue::SUPPORT_GID_WINDSOR . ', ' . CCallQueue::SUPPORT_MARKETING_WEBSITE_PRODUCTS . ', ' . CCallQueue::SUPPORT_ONLINE_LEASE_APPLICATION_LEAD_MANAGEMENT . ', ' . CCallQueue::SUPPORT_UDR . ', ' . CCallQueue::SUPPORT_TIER_TWO . ')
						AND c.call_datetime::date BETWEEN \'' . $arrmixPreBuildSlideFilters['start_datetime'] . '\' AND ( \'' . $arrmixPreBuildSlideFilters['end_datetime'] . '\'::date + \'1 day\'::INTERVAL )
						AND cm.created_on BETWEEN \'' . $arrmixPreBuildSlideFilters['start_datetime'] . '\' AND ( \'' . $arrmixPreBuildSlideFilters['end_datetime'] . '\'::date + \'1 day\'::INTERVAL )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataWithCalendarEnabledPropertiesByCallFilter( $objCallFilter, $objVoipDatabase ) {
		$strSql = 'SELECT
						COALESCE ( COUNT( c.id ), 0 ) AS lead_answered_calls_with_google_calendar_count
					FROM
						calls AS c
						JOIN property_call_settings pcs ON ( c.property_id = pcs.property_id )
					WHERE
						c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND c.call_result_id = ' . CCallResult::LEAD . '
						AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
						AND pcs.is_calendar_enabled = 1
						AND c.employee_id IN( ' . $objCallFilter->getEmployeeIds() . ' )
						AND c.call_datetime
						BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
						AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeadCallsForApiByCallFilterByCid( $objCallFilter, $intCid, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						c.*
					FROM
						calls c
					WHERE
						c.origin_phone_number = \'' . addslashes( $objCallFilter->getPhoneNumber() ) . '\'
						AND c.cid = ' . ( int ) $intCid . '
						AND c.call_type_id IN ( ' . implode( ', ', $objCallFilter->getCallTypeIds() ) . ' )
						AND c.call_datetime BETWEEN \'' . $objCallFilter->getAfterDatetime() . '\' AND \'' . $objCallFilter->getBeforeDatetime() . '\'';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterResidentSupportCallsBySearchFilterByMainResultsByPropertyId( $objFormFilter, $arrintCallReusultIds, $intPropertyId, $intCid, $boolIsSecondaryCallResult = false, $objVoipDatabase ) {
		if( false == valArr( $arrintCallReusultIds ) || false == valId( $intCid ) || false == valId( $intPropertyId ) ) return NULL;

		$strCoumnName = ' call_result_id ';

		if( true == $boolIsSecondaryCallResult ) {
			$strCoumnName = ' id ';
		}

		$strSql = 'WITH call_results_temp AS (
						SELECT
							COUNT ( c.id ) AS call_count,
							SUM ( c.call_duration_seconds ) AS total_call_duration_seconds,
							cr.' . $strCoumnName . ' AS call_result_id
						FROM
							calls c
							JOIN call_results cr ON ( cr.id = c.call_result_id )
						WHERE
							c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
							AND c.call_type_id = ' . CCallType::RESIDENT_SUPPORT . '
							AND	c.call_result_id IS NOT NULL
							AND c.call_result_id <> ' . CCallResult::TEST_CALL . '
							AND c.cid = ' . ( int ) $intCid . '
							AND c.property_id = ' . ( int ) $intPropertyId . '
							AND c.call_result_id IN ( ' . implode( ',', $arrintCallReusultIds ) . ' )
							AND c.call_datetime
							BETWEEN \'' . $objFormFilter->getFilterObject( 'period' )->getStartDate() . ' 00:00:00' . '\' AND \'' . $objFormFilter->getFilterObject( 'period' )->getEndDate() . ' 23:59:59.999999' . '\'
						GROUP BY
							cr.' . $strCoumnName . '
					)

					SELECT
						call_result_id,
						call_count,
						COALESCE ( ( CASE
										WHEN ( 0 < total_call_duration_seconds AND 0 < call_count ) THEN ( total_call_duration_seconds / call_count )
										ELSE 0
									END ), 0 ) AS avg_call_duration_time
					FROM
						call_results_temp';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsBySelectedCallFilter( $objCallFilter, $objVoipDatabase ) {
		$strWhereSql = '';

		$strSql = 'SELECT
						*
					FROM
						calls';

		if( true == valStr( $objCallFilter->getCallResultIds() ) ) {
			$arrstrWhere[] = 'call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' )';
		}

		if( true == valId( $objCallFilter->getCallStatusTypeId() ) ) {
			$arrstrWhere[] = 'call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId();
		}

		if( true == valStr( $objCallFilter->getCallTypeIds() ) ) {
			$arrstrWhere[] = 'call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )';
		}

		if( true == valId( $objCallFilter->getEmployeeId() ) ) {
			$arrstrWhere[] = 'employee_id = ' . ( int ) $objCallFilter->getEmployeeId();
		}

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$arrstrWhere[] = 'call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strWhereSql .= implode( ' AND ', $arrstrWhere );
			$strSql .= ' WHERE ' . $strWhereSql;
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchResidentSupportHistoricalStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						COALESCE( dropped_calls_count, 0 ) AS dropped_calls_count,
						COALESCE( move_in_calls_count, 0 ) AS move_in_calls_count,
						COALESCE( nps_calls_count, 0 ) AS nps_calls_count,
						COALESCE( total_calls_count, 0 ) AS total_calls_count,
						COALESCE( answered_calls_in_thirty_second_count, 0 ) AS answered_calls_in_thirty_second_count,
						COALESCE( (	CASE
										WHEN ( 0 < total_calls_count )
										THEN ( ( answered_calls_in_thirty_second_count * 100 ) / ( total_calls_count ) )
									ELSE 0
								END ), 0 ) AS service_level,
						COALESCE( average_response_time, 0 ) AS average_response_time,
						COALESCE( resident_support_premium_calls_dropped_count ) AS resident_support_premium_calls_dropped_count,
						COALESCE( inbound_calls_received, 0 ) AS inbound_calls_received,
						COALESCE( resident_support_basic_calls_dropped, 0 ) AS resident_support_basic_calls_dropped
					FROM
						(
							SELECT
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS dropped_calls_count,
								COUNT( CASE WHEN
												c.call_type_id = ' . ( int ) CCallType::MOVE_IN_CALLS . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS move_in_calls_count,
								COUNT( CASE WHEN
												c.call_type_id = ' . ( int ) CCallType::NET_PROMOTER_SCORE . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS nps_calls_count,
								COUNT( CASE WHEN
												cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS total_calls_count,
								COUNT( CASE WHEN
												cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
												AND c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND COALESCE( c.call_center_queue_time, 0 ) < 31
											THEN c.id ELSE NULL
										END ) AS answered_calls_in_thirty_second_count,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
										THEN COALESCE( c.call_center_queue_time, 0 )
									 END ) AS average_response_time,
								COUNT( CASE WHEN
												c.call_type_id = ' . ( int ) CCallType::RESIDENT_SUPPORT . '
												AND c.call_status_type_id = ' . ( int ) CCallStatusTYpe::UNANSWERED . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS resident_support_premium_calls_dropped_count,
								COUNT( CASE WHEN
												c.call_type_id = ' . ( int ) CCallType::RESIDENT_SUPPORT . '
												AND c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS inbound_calls_received,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )
											THEN c.id ELSE NULL
										END ) AS resident_support_basic_calls_dropped
							FROM
								calls AS c
								LEFT JOIN call_types AS ct ON ( c.call_type_id = ct.id )
						 		LEFT JOIN call_queues AS cq ON ( ct.default_call_queue_id = cq.id )
							WHERE
								( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' ) ';

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$strSql .= ' AND c.cid IN ( ' . $objCallFilter->getCids() . ' )';
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			$strSql .= ' AND c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' )';
		}

		if( true == valStr( $objCallFilter->getTeamIds() ) && true == valStr( $objCallFilter->getEmployeeIds() ) ) {
			$strSql .= ' AND c.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )';
		}

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59.999999\'';
		}

		$strSql .= ' ) AS sub_query';

		$arrmixResponseData = fetchData( $strSql, $objVoipDatabase );

		return $arrmixResponseData[0];
	}

	public static function fetchPaginatedCallsByCallStatusTypeIdsByCallTypeIdsByCallFilterByCallAgentIds( $arrintCallStatusTypeIds, $arrintCallTypeIds, $objPagination, $objCallShadowsFilter, $arrintCallAgentsIds, $objVoipDatabase, $boolCountOnly = false ) {
		$arrstrWhere = [];

		if( true == $boolCountOnly ) {
			$strSql = ' SELECT COUNT(c.id)';
		} else {
			$strSql = ' SELECT
						c.*,
						pcs.property_name AS property_name,
						clients.company_name AS company_name,
						ca.full_name AS call_agent_full_name,
						ca.physical_phone_extension_id AS call_agent_physical_phone_extension,
						ca.agent_phone_extension_id AS call_agent_agent_phone_extension,
						castt.name AS call_agent_status_type_name,
						casts.name AS call_agent_state_type_name,
						ct.name AS call_type_name,
						cq.name AS call_queue_name,
						cst.name AS call_status_type_name,
						cm.session_uuid AS call_uuid ';
		}

		$strSql .= 'FROM
						calls AS c
						JOIN call_members AS cm ON ( cm.call_id = c.id )
						JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = c.property_id )
						LEFT JOIN clients ON ( clients.id = c.cid )
						JOIN call_status_types AS cst ON ( cst.id = c.call_status_type_id )
						JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
						JOIN call_types AS ct ON ( ct.id = c.call_type_id )
						JOIN call_queues AS cq ON ( cq.id = cm.call_queue_id )
					WHERE
						c.call_status_type_id IN ( ' . implode( ',', $arrintCallStatusTypeIds ) . ' )
						AND c.call_type_id IN ( ' . implode( ', ', $arrintCallTypeIds ) . ' )
						AND c.call_datetime::DATE = now()::DATE ';

		if( true == valId( $objCallShadowsFilter->getCallQueueId() ) ) {
			$arrstrWhere[] = ' cq.id = ' . $objCallShadowsFilter->getCallQueueId();
		}

		if( true == valId( $objCallShadowsFilter->getCallTypeId() ) ) {
			$arrstrWhere[] = ' ct.id = ' . $objCallShadowsFilter->getCallTypeId();
		}

		if( true == valId( $objCallShadowsFilter->getCallAgentId() ) ) {
			$arrstrWhere[] = ' ca.id = ' . $objCallShadowsFilter->getCallAgentId();
		}

		if( true == valId( $objCallShadowsFilter->getCallId() ) ) {
			$arrstrWhere[] = ' c.id = ' . $objCallShadowsFilter->getCallId();
		}

		if( ( false == is_null( $objCallShadowsFilter->getPropertyIds() ) ) && ( '' != trim( $objCallShadowsFilter->getPropertyIds() ) ) ) {
			$arrstrWhere[] = ' pcs.property_id IN ( ' . $objCallShadowsFilter->getPropertyIds() . ' ) ';
		}

		if( ( false == is_null( $objCallShadowsFilter->getCids() ) ) && ( '' != trim( $objCallShadowsFilter->getCids() ) ) ) {
			$arrstrWhere[] = ' clients.id IN ( ' . $objCallShadowsFilter->getCids() . ' ) ';
		}

		if( true == valId( $objCallShadowsFilter->getTeamId() ) && ( '' != trim( $objCallShadowsFilter->getEmployeeIds() ) ) ) {
			$arrstrWhere[] = ' ca.employee_id IN ( ' . $objCallShadowsFilter->getEmployeeIds() . ' ) ';
		}

		if( true == valArr( $arrintCallAgentsIds ) ) {
			$arrstrWhere[] = ' c.call_agent_id IN ( ' . implode( ',', $arrintCallAgentsIds ) . ' )';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( false == $boolCountOnly ) {
			$strSql .= ' ORDER BY
						c.id DESC';
		}

		if( false == $boolCountOnly && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterPerformanceReportCallStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		$intInProgressCallStatusTypeId	= CCallStatusType::CALL_IN_PROGRESS;
		$intAnsweredCallStatusTypeId	= CCallStatusType::ANSWERED;
		$intUnansweredCallStatusTypeId	= CCallStatusType::UNANSWERED;

		$intLeadCallResultId			= CCallResult::LEAD;
		$intWorkOrderCallResultId		= CCallResult::WORK_ORDER;
		$intResidentCallResultId		= CCallResult::RESIDENT;
		$intNoResultCallResultId		= CCallResult::NO_RESULT;
		$intUnknownCallResultId			= CCallResult::UNKNOWN;
		$intWrongNumberCallResultId		= CCallResult::WRONG_NUMBER;
		$intTestCallResultId			= CCallResult::TEST_CALL;

		$intMoveInCallTypeId			= CCallType::MOVE_IN_CALLS;
		$intSupportCallTypeId			= CCallType::CALL_CENTER;

		$strWhereSql					= '';

		if( true == valId( $objCallFilter->getCid() ) ) {
			$strWhereSql .= ' AND c.cid = ' . ( int ) $objCallFilter->getCid();
		}

		if( true == valStr( $objCallFilter->getPropertyIds() ) ) {
			$strWhereSql .= ' AND c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' ) ';
		}

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			if( true == in_array( CCallQueue::MOVE_IN_CALLS, explode( ',', $objCallFilter->getCallQueueIds() ) ) ) {
				$strWhereSql .= ' AND ( ( c.call_queue_id IN ( ' . $objCallFilter->getCallQueueIds() . ' ) ) OR c.call_type_id = ' . CCallType::MOVE_IN_CALLS . ' )';
			} else {
				$strWhereSql .= ' AND c.call_queue_id IN ( ' . $objCallFilter->getCallQueueIds() . ' )';
			}
		}

		if( false == valStr( $objCallFilter->getCallQueueIds() ) && true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strWhereSql .= ' AND ca.department_id IN ( ' . $objCallFilter->getDepartmentIds() . ' )';
		}

		if( true == valStr( $objCallFilter->getEmployeeIds() ) ) {
			$strWhereSql .= ' AND c.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' ) ';
		}

		$strWhereSql .= '	AND c.is_test_call = 0
							AND c.is_auto_detected = 0
							AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL;

		$strSql = 'SELECT
						calls_in_queue_count,
						calls_in_progress_count,
						calls_last_hour_count,
						answered_last_hour_calls_in_thirty_second_count,
						answered_calls_in_thirty_second_count,
						unanswered_last_hour_calls_count,
						answered_calls_count,
						unanswered_calls_count,
						abandoned_thirty_calls_count,
						average_call_duration_time,
						average_call_queue_time,
						lead_answered_calls_count,
						lead_unanswered_calls_count,
						work_order_answered_calls_count,
						work_order_unanswered_calls_count,
						resident_answered_calls_count,
						resident_unanswered_calls_count,
						utility_billing_answered_calls_count,
						utility_billing_unanswered_calls_count,
						support_answered_calls_count,
						support_unanswered_calls_count,
						move_in_calls_count,
						other_answered_calls_count,
						other_unanswered_calls_count,
						no_result_answered_calls_count,
						no_result_unanswered_calls_count,
						unknown_answered_calls_count,
						unknown_unanswered_calls_count,
						wrong_number_answered_calls_count,
						wrong_number_unanswered_calls_count,
						null_calls_count,
						pre_queue_calls_count,
						average_abandoned_time,
						(	CASE
									WHEN ( 0 < ( answered_calls_last_hour_count + unanswered_last_hour_calls_count ) AND 0 < answered_last_hour_calls_in_thirty_second_count )
									THEN ( ( answered_last_hour_calls_in_thirty_second_count * 100 ) / ( answered_calls_last_hour_count + unanswered_last_hour_calls_count ) )
								ELSE 0
							END ) AS service_level_for_last_hour,
						(	CASE
									WHEN ( 0 < ( answered_calls_count + unanswered_calls_count ) AND 0 < answered_calls_in_thirty_second_count )
									THEN ( ( answered_calls_in_thirty_second_count * 100 ) / ( answered_calls_count + unanswered_calls_count ) )
								ELSE 0
							END ) AS service_level_for_today,
						(	CASE
									WHEN ( 0 < ( answered_calls_count + unanswered_calls_count + abandoned_thirty_calls_count ) )
									THEN ( ( answered_calls_count + unanswered_calls_count + abandoned_thirty_calls_count ) )
								ELSE 0
							END ) AS total_calls_count
					FROM
					(
					SELECT
							COUNT( CASE WHEN
											( c.call_status_type_id IN ( ' . implode( ',', CCallStatusType::$c_arrintInQueueCallStatusTypeIds ) . ' ) AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' ) )
											OR ( c.call_status_type_id IN ( ' . CCallStatusType::CALLING . ',' . CCallStatusType::ATTENDED_CALL_TRANSFER . ' ) AND c.call_type_id IN ( ' . CCallType::RESIDENT_INSURE . ',' . CCallType::RESIDENT_VERIFY . ' ) )
										THEN c.id ELSE NULL
									END ) AS calls_in_queue_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intInProgressCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS calls_in_progress_count,
							COUNT( CASE WHEN
											c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND	c.call_datetime >= ( NOW() - INTERVAL \'1 hour\')
											AND c.call_datetime <= NOW()
										THEN c.id ELSE NULL
									END ) AS calls_last_hour_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND c.call_datetime >= ( NOW() - INTERVAL \'1 hour\')
											AND c.call_datetime <= NOW()
										THEN c.id ELSE NULL
									END ) AS answered_calls_last_hour_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND c.call_datetime >= ( NOW() - INTERVAL \'1 hour\')
											AND c.call_datetime <= NOW()
											AND COALESCE( c.call_center_queue_time, 0 ) < 31
										THEN c.id ELSE NULL
									END ) AS answered_last_hour_calls_in_thirty_second_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND c.call_datetime >= ( NOW() - INTERVAL \'1 hour\')
											AND c.call_datetime <= NOW()
											AND COALESCE( c.call_center_queue_time, 0 ) > 30
										THEN c.id ELSE NULL
									END ) AS unanswered_last_hour_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) < 31
										THEN c.id ELSE NULL
									END ) AS answered_calls_in_thirty_second_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) > 30
										THEN c.id ELSE NULL
									END ) AS unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND ( COALESCE( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) < 31 )
										THEN c.id ELSE NULL
									END ) AS abandoned_thirty_calls_count,
							AVG( CASE
									WHEN
										c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
										AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
									THEN COALESCE( c.call_duration_seconds, 0 )
								 END ) AS average_call_duration_time,

							AVG( CASE
									WHEN
										c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
										AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
									THEN COALESCE( c.call_center_queue_time, 0 )
								 END ) AS average_call_queue_time,

							COUNT( CASE	WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intLeadCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS lead_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intLeadCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS lead_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intWorkOrderCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intWorkOrderCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intResidentCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS resident_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intResidentCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS resident_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . implode( ',', CUtilityBillingCallType::$c_arrintUtilityBillingCallTypeIds ) . ' )
										THEN c.id ELSE NULL
									END ) AS utility_billing_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . implode( ',', CUtilityBillingCallType::$c_arrintUtilityBillingCallTypeIds ) . ' )
										THEN c.id ELSE NULL
									END ) AS utility_billing_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id = ' . ( int ) $intSupportCallTypeId . '
										THEN c.id ELSE NULL
									END ) AS support_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id = ' . ( int ) $intSupportCallTypeId . '
										THEN c.id ELSE NULL
									END ) AS support_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_type_id = ' . ( int ) $intMoveInCallTypeId . '
										THEN c.id ELSE NULL
									END ) AS move_in_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS other_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS other_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intNoResultCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS no_result_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intNoResultCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS no_result_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intUnknownCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL END ) AS unknown_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intUnknownCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL END ) AS unknown_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intWrongNumberCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS wrong_number_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_result_id = ' . ( int ) $intWrongNumberCallResultId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS wrong_number_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_result_id IS NULL
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN c.id ELSE NULL
									END ) AS null_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) = 0
										THEN c.id
									END ) AS pre_queue_calls_count,
							AVG( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
										THEN COALESCE( c.call_center_queue_time, 0 )
									END ) AS average_abandoned_time
							FROM
								calls AS c';

		if( false == valStr( $objCallFilter->getCallQueueIds() ) && true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ' LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )';
		}

		$strSql .= ' WHERE
						( c.call_result_id IS NULL OR c.call_result_id <> ' . ( int ) $intTestCallResultId . ' )
						AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . '\' AND \'' . $objCallFilter->getEndDate() . '\'' . $strWhereSql . ' ) AS sub_query';
		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterHistoricalReportCallStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		$strSql = 'SELECT
						COALESCE( answered_calls_count, 0 ) AS answered_calls_count,
						COALESCE( answered_calls_in_thirty_second_count, 0 ) AS answered_calls_in_thirty_second_count,
						COALESCE( unanswered_calls_count, 0 ) AS unanswered_calls_count,
						COALESCE( abandoned_thirty_calls_count, 0 ) AS abandoned_thirty_calls_count,
						COALESCE( average_call_duration_time, 0 ) AS average_call_duration_time,
						COALESCE( average_wrap_up_time, 0 ) AS average_wrap_up_time,
						COALESCE( average_lead_call_duration_time, 0 ) AS average_lead_call_duration_time,
						COALESCE( average_lead_wrap_up_time, 0 ) AS average_lead_wrap_up_time,
						COALESCE( average_work_order_call_duration_time, 0 ) AS average_work_order_call_duration_time,
						COALESCE( average_work_order_wrap_up_time, 0 ) AS average_work_order_wrap_up_time,
						COALESCE( average_resident_call_duration_time, 0 ) AS average_resident_call_duration_time,
						COALESCE( average_resident_wrap_up_time, 0 ) AS average_resident_wrap_up_time,
						COALESCE( average_other_call_duration_time, 0 ) AS average_other_call_duration_time,
						COALESCE( average_other_wrap_up_time, 0 ) AS average_other_wrap_up_time,
						COALESCE( average_call_queue_time, 0 ) AS average_call_queue_time,
						COALESCE( lead_answered_calls_count, 0 ) AS lead_answered_calls_count,
						COALESCE( lead_unanswered_calls_count, 0 ) AS lead_unanswered_calls_count,
						COALESCE( work_order_answered_calls_count, 0 ) AS work_order_answered_calls_count,
						COALESCE( work_order_unanswered_calls_count, 0 ) AS work_order_unanswered_calls_count,
						COALESCE( resident_answered_calls_count, 0 ) AS resident_answered_calls_count,
						COALESCE( resident_unanswered_calls_count, 0 ) AS resident_unanswered_calls_count,
						COALESCE( soliciter_answered_calls_count, 0 ) AS soliciter_answered_calls_count,
						COALESCE( soliciter_unanswered_calls_count, 0 ) AS soliciter_unanswered_calls_count,
						COALESCE( vendor_answered_calls_count, 0 ) AS vendor_answered_calls_count,
						COALESCE( vendor_unanswered_calls_count, 0 ) AS vendor_unanswered_calls_count,
						COALESCE( unknown_answered_calls_count, 0 ) AS unknown_answered_calls_count,
						COALESCE( unknown_unanswered_calls_count, 0 ) AS unknown_unanswered_calls_count,
						COALESCE( wrong_number_answered_calls_count, 0 ) AS wrong_number_answered_calls_count,
						COALESCE( wrong_number_unanswered_calls_count, 0 ) AS wrong_number_unanswered_calls_count,
						COALESCE( test_answered_calls_count, 0 ) AS test_answered_calls_count,
						COALESCE( test_unanswered_calls_count, 0 ) AS test_unanswered_calls_count,
						COALESCE( other_answered_calls_count, 0 ) AS other_answered_calls_count,
						COALESCE( other_unanswered_calls_count, 0 ) AS other_unanswered_calls_count,
						COALESCE( pre_queue_calls_count, 0 ) AS pre_queue_calls_count,
						COALESCE( average_abandoned_time, 0 ) AS average_abandoned_time,
						COALESCE( maximum_call_center_call_queue_time, 0 ) AS maximum_call_center_call_queue_time,
						COALESCE( (	CASE
										WHEN ( 0 < ( answered_calls_count + unanswered_calls_count ) AND 0 < answered_calls_in_thirty_second_count )
										THEN ( ( answered_calls_in_thirty_second_count * 100 ) / ( answered_calls_count + unanswered_calls_count ) )
									ELSE 0
								END ), 0 ) AS service_level_for_duration,
						COALESCE( (	CASE
										WHEN ( 0 < ( answered_calls_count + unanswered_calls_count + abandoned_thirty_calls_count ) )
										THEN ( ( answered_calls_count + unanswered_calls_count + abandoned_thirty_calls_count ) )
									ELSE 0
								END ), 0 ) AS total_calls_count
					FROM
					(
					 SELECT
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
												AND COALESCE( c.call_center_queue_time, 0 ) < 31
											THEN c.id ELSE NULL
										END ) AS answered_calls_in_thirty_second_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
												AND COALESCE( c.call_center_queue_time, 0 ) > 30
											THEN c.id ELSE NULL
										END ) AS unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
												AND ( COALESCE( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) < 31 )
											THEN c.id ELSE NULL
										END ) AS abandoned_thirty_calls_count,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_duration_seconds, 0 )
									 END ) AS average_call_duration_time,
								AVG( CASE
										WHEN
											c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.wrap_up_time, 0)
									 END ) AS average_wrap_up_time,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_duration_seconds, 0 )
									 END ) AS average_lead_call_duration_time,
								AVG( CASE
										WHEN
											c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.wrap_up_time, 0)
									 END ) AS average_lead_wrap_up_time,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_duration_seconds, 0 )
									 END ) AS average_work_order_call_duration_time,
								AVG( CASE
										WHEN
											c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.wrap_up_time, 0)
									 END ) AS average_work_order_wrap_up_time,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_duration_seconds, 0 )
									 END ) AS average_resident_call_duration_time,
								AVG( CASE
										WHEN
											c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.wrap_up_time, 0)
									 END ) AS average_resident_wrap_up_time,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_duration_seconds, 0 )
									 END ) AS average_other_call_duration_time,
								AVG( CASE
										WHEN
											c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.wrap_up_time, 0)
									 END ) AS average_other_wrap_up_time,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_center_queue_time, 0 )
									 END ) AS average_call_queue_time,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS lead_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS lead_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS resident_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS resident_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::SOLICITOR . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS soliciter_answered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::SOLICITOR . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS soliciter_unanswered_calls_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::VENDOR . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS vendor_answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::VENDOR . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS vendor_unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND	c.call_result_id = ' . ( int ) CCallResult::UNKNOWN . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS unknown_answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND	c.call_result_id = ' . ( int ) CCallResult::UNKNOWN . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS unknown_unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::WRONG_NUMBER . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS wrong_number_answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::WRONG_NUMBER . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS wrong_number_unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::TEST_CALL . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS test_answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_result_id = ' . ( int ) CCallResult::TEST_CALL . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS test_unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS other_answered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN c.id ELSE NULL
										END ) AS other_unanswered_calls_count,
								COUNT( CASE WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
												AND COALESCE( c.call_center_queue_time, 0 ) = 0
											THEN c.id ELSE NULL
										END ) AS pre_queue_calls_count,
								AVG( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_center_queue_time, 0 )
									END ) AS average_abandoned_time,
								MAX( CASE
										WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN COALESCE( c.call_center_queue_time, 0 )
									END ) AS maximum_call_center_call_queue_time
							FROM
								calls AS c';

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' LEFT JOIN call_queues AS cq ON ( c.call_queue_id = cq.id )';
		}

		if( false == valStr( $objCallFilter->getCallQueueIds() ) && true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ' LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )';
		}

		$strSql .= ' WHERE ( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' )';

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND c.call_datetime >= \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						 AND c.call_datetime <= \'' . $objCallFilter->getEndDate() . ' 23:59:59\' ';
		}

		if( true == valId( $objCallFilter->getCid() ) ) {
			$strSql .= ' AND c.cid IN ( ' . $objCallFilter->getCid() . ' )';
		}

		if( true == valId( $objCallFilter->getPropertyId() ) ) {
			$strSql .= ' AND c.property_id IN ( ' . $objCallFilter->getPropertyId() . ' )';
		}

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )';
		}

		if( false == valStr( $objCallFilter->getCallQueueIds() ) && true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ' AND ca.department_id IN ( ' . $objCallFilter->getDepartmentIds() . ' )';
		}

		$strSql .= ' AND c.is_test_call = 0
					 AND c.is_auto_detected = 0
					 AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL;

		$strSql .= ' ) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchDownloadLeasingCenterHistoricalReportCallStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		switch( $objCallFilter->getIntervalTypeId() ) {

			case NULL:
			case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_HOUR:
				$strSql						= ' sub_query.call_hour, ';
				$strFetchGroupByFieldSql	= 'TO_CHAR(c.call_datetime, \'YYYY/MM/DD HH24\' ) AS call_hour,';
				$strGroupByOrderByField		= 'call_hour';
				break;

			case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_DAILY:
				$strSql						= ' sub_query.call_date, ';
				$strFetchGroupByFieldSql	= 'TO_CHAR(c.call_datetime, \'YYYY/MM/DD\') AS call_date,';
				$strGroupByOrderByField		= 'call_date';
				break;

			case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_WEEKLY:
				$strSql						= ' sub_query.call_week, ';
				$strFetchGroupByFieldSql	= 'TO_CHAR(cast(c.call_datetime AS date), \'IW\') AS call_week,';
				$strGroupByOrderByField		= 'call_week';
				break;

			case CCallFilter::LEASING_CENTER_REPORT_INTERVAL_MONTHLY:
				$strSql						= ' sub_query.call_month, ';
				$strFetchGroupByFieldSql	= 'TO_CHAR( c.call_datetime, \'Month\' ) AS call_month,';
				$strGroupByOrderByField		= 'call_month';
				break;

			default:
				// Default empty case
				break;
		}

		$strSql = 'SELECT
						' . $strSql . '
						COALESCE( sub_query.answered_calls, 0 ) AS answered_calls_count,
						COALESCE( sub_query.answered_calls_in_thirty_second, 0) AS answered_calls_in_thirty_second_count,
						COALESCE( sub_query.unanswered_calls, 0 ) AS unanswered_calls_count,
						COALESCE( sub_query.abandoned_thirty_calls, 0) AS abandoned_thirty_calls_count,
						COALESCE( sub_query.avg_calls_duration_time, 0 ) AS average_calls_duration_time,
						COALESCE( sub_query.average_wrap_up_time, 0 ) AS average_wrap_up_time,
						COALESCE( average_lead_call_duration_time, 0 ) AS average_lead_call_duration_time,
						COALESCE( average_lead_wrap_up_time, 0 ) AS average_lead_wrap_up_time,
						COALESCE( average_work_order_call_duration_time, 0 ) AS average_work_order_call_duration_time,
						COALESCE( average_work_order_wrap_up_time, 0 ) AS average_work_order_wrap_up_time,
						COALESCE( average_resident_call_duration_time, 0 ) AS average_resident_call_duration_time,
						COALESCE( average_resident_wrap_up_time, 0 ) AS average_resident_wrap_up_time,
						COALESCE( average_other_call_duration_time, 0 ) AS average_other_call_duration_time,
						COALESCE( average_other_wrap_up_time, 0 ) AS average_other_wrap_up_time,
						COALESCE( sub_query.avg_calls_queue_time, 0 ) AS average_calls_queue_time,
						COALESCE( sub_query.lead_answered_calls, 0 ) AS lead_answered_calls_count,
						COALESCE( sub_query.lead_unanswered_calls, 0 ) AS lead_unanswered_calls_count,
						COALESCE( sub_query.work_order_answered_calls, 0 ) AS work_order_answered_calls_count,
						COALESCE( sub_query.work_order_unanswered_calls, 0 ) AS work_order_unanswered_calls_count,
						COALESCE( sub_query.resident_answered_calls, 0 ) AS resident_answered_calls_count,
						COALESCE( sub_query.resident_unanswered_calls, 0 ) AS resident_unanswered_calls_count,
						COALESCE( soliciter_answered_calls_count, 0 ) AS soliciter_answered_calls_count,
						COALESCE( soliciter_unanswered_calls_count, 0 ) AS soliciter_unanswered_calls_count,
						COALESCE( vendor_answered_calls_count, 0 ) AS vendor_answered_calls_count,
						COALESCE( vendor_unanswered_calls_count, 0 ) AS vendor_unanswered_calls_count,
						COALESCE( unknown_answered_calls_count, 0 ) AS unknown_answered_calls_count,
						COALESCE( unknown_unanswered_calls_count, 0 ) AS unknown_unanswered_calls_count,
						COALESCE( wrong_number_answered_calls_count, 0 ) AS wrong_number_answered_calls_count,
						COALESCE( wrong_number_unanswered_calls_count, 0 ) AS wrong_number_unanswered_calls_count,
						COALESCE( test_answered_calls_count, 0 ) AS test_answered_calls_count,
						COALESCE( test_unanswered_calls_count, 0 ) AS test_unanswered_calls_count,
						COALESCE( sub_query.other_answered_calls_count, 0 ) AS other_answered_calls_count,
						COALESCE( sub_query.other_unanswered_calls_count, 0 ) AS other_unanswered_calls_count,
						COALESCE( sub_query.pre_queue_calls_count, 0) AS pre_queue_calls_count,
						COALESCE( sub_query.avg_abandoned_time, 0 ) AS average_abandoned_time,
						COALESCE( sub_query.maximum_call_center_call_queue_time, 0 ) AS maximum_call_center_call_queue_time,
						COALESCE( (	CASE
										WHEN ( 0 < ( sub_query.answered_calls + sub_query.unanswered_calls ) AND 0 < sub_query.answered_calls_in_thirty_second )
										THEN ( ( sub_query.answered_calls_in_thirty_second * 100 ) / ( sub_query.answered_calls + sub_query.unanswered_calls ) )
									ELSE 0
								END ), 0 ) AS service_level,
						COALESCE( (	CASE
										WHEN ( 0 < ( answered_calls + unanswered_calls + abandoned_thirty_calls ) )
										THEN ( ( answered_calls + unanswered_calls + abandoned_thirty_calls ) )
									ELSE 0
								END ), 0 ) AS total_calls_count
					FROM (
						SELECT
							' . $strFetchGroupByFieldSql . '
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) < 31
										THEN c.id ELSE NULL
									END ) AS answered_calls_in_thirty_second,
							COUNT(	CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS answered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) > 30
										THEN c.id ELSE NULL
									END ) AS unanswered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											AND ( COALESCE( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) < 31 )
										THEN c.id ELSE NULL
									END ) AS abandoned_thirty_calls,
							AVG( CASE
									WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN COALESCE( c.call_duration_seconds, 0 )
								 END ) AS avg_calls_duration_time,
							AVG( CASE
								WHEN
									c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.wrap_up_time, 0)
							 END ) AS average_wrap_up_time,
						AVG( CASE
								WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.call_duration_seconds, 0 )
							 END ) AS average_lead_call_duration_time,
						AVG( CASE
								WHEN
									c.call_result_id = ' . ( int ) CCallResult::LEAD . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.wrap_up_time, 0)
							 END ) AS average_lead_wrap_up_time,
						AVG( CASE
								WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.call_duration_seconds, 0 )
							 END ) AS average_work_order_call_duration_time,
						AVG( CASE
								WHEN
									c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.wrap_up_time, 0)
							 END ) AS average_work_order_wrap_up_time,
						AVG( CASE
								WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.call_duration_seconds, 0 )
							 END ) AS average_resident_call_duration_time,
						AVG( CASE
								WHEN
									c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.wrap_up_time, 0)
							 END ) AS average_resident_wrap_up_time,
						AVG( CASE
								WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.call_duration_seconds, 0 )
							 END ) AS average_other_call_duration_time,
						AVG( CASE
								WHEN
									c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN COALESCE( c.wrap_up_time, 0)
							 END ) AS average_other_wrap_up_time,
							AVG( CASE
									WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN COALESCE( c.call_center_queue_time, 0 )
								 END ) AS avg_calls_queue_time,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS lead_answered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::LEAD . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS lead_unanswered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_answered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::WORK_ORDER . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS work_order_unanswered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS resident_answered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_result_id = ' . ( int ) CCallResult::RESIDENT . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS resident_unanswered_calls,
							COUNT( CASE WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::SOLICITOR . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN c.id ELSE NULL
							END ) AS soliciter_answered_calls_count,
					COUNT( CASE WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::SOLICITOR . '
									AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
								THEN c.id ELSE NULL
							END ) AS soliciter_unanswered_calls_count,
					COUNT( CASE WHEN
									c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
									AND c.call_result_id = ' . ( int ) CCallResult::VENDOR . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS vendor_answered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND c.call_result_id = ' . ( int ) CCallResult::VENDOR . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS vendor_unanswered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND	c.call_result_id = ' . ( int ) CCallResult::UNKNOWN . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS unknown_answered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND	c.call_result_id = ' . ( int ) CCallResult::UNKNOWN . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS unknown_unanswered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND c.call_result_id = ' . ( int ) CCallResult::WRONG_NUMBER . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS wrong_number_answered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND c.call_result_id = ' . ( int ) CCallResult::WRONG_NUMBER . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS wrong_number_unanswered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND c.call_result_id = ' . ( int ) CCallResult::TEST_CALL . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS test_answered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND c.call_result_id = ' . ( int ) CCallResult::TEST_CALL . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS test_unanswered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
										AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS other_answered_calls_count,
						COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND c.call_result_id IN ( ' . implode( ',', CCallResult::$c_arrintOtherCallResultIds ) . ' )
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
									THEN c.id ELSE NULL
								END ) AS other_unanswered_calls_count,
							COUNT( CASE WHEN
										c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
										AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
										AND COALESCE( c.call_center_queue_time, 0 ) = 0
									THEN c.id ELSE NULL
								END ) AS pre_queue_calls_count,

							AVG( CASE	WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) < 31
										THEN COALESCE( c.call_center_queue_time, 0 ) ELSE NULL
									END ) AS avg_abandoned_time,
							MAX( CASE	WHEN
												c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
												AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterAndSupportCallTypes() ) . ' )
											THEN COALESCE( c.call_center_queue_time, 0 )
										END ) AS maximum_call_center_call_queue_time
						FROM
							calls AS c';

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' LEFT JOIN call_queues AS cq ON ( c.call_queue_id = cq.id )';
		}

		if( true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ' LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )';
		}

		$strSql .= ' WHERE ( c.call_result_id IS NULL OR c.call_result_id <> ' . CCallResult::TEST_CALL . ' )';

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND c.call_datetime >= \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						 AND c.call_datetime <= \'' . $objCallFilter->getEndDate() . ' 23:59:59\' ';
		}

		if( true == valId( $objCallFilter->getCid() ) ) {
			$strSql .= ' AND c.cid IN ( ' . $objCallFilter->getCid() . ' )';
		}

		if( true == valId( $objCallFilter->getPropertyId() ) ) {
			$strSql .= ' AND c.property_id IN ( ' . $objCallFilter->getPropertyId() . ' )';
		}

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )';
		}

		if( false == valStr( $objCallFilter->getCallQueueIds() ) && true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ' AND ca.department_id IN ( ' . $objCallFilter->getDepartmentIds() . ' )';
		}

		$strSql .= ' AND c.is_test_call = 0
					 AND c.is_auto_detected = 0
					 AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL;

		$strSql .= ' GROUP BY ' . $strGroupByOrderByField . ' ORDER BY ' . $strGroupByOrderByField . '
					) AS sub_query
					ORDER BY ' . $strGroupByOrderByField;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterPerformanceMetricsReportCallStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						COALESCE( sub_query.answered_calls, 0 ) AS answered_calls_count,
						COALESCE( sub_query.answered_calls_in_thirty_second, 0) AS answered_calls_in_thirty_second_count,
						COALESCE( (	CASE
										WHEN ( 0 < ( sub_query.answered_calls + sub_query.unanswered_calls ) AND 0 < sub_query.answered_calls_in_thirty_second )
										THEN ( ( sub_query.answered_calls_in_thirty_second * 100 ) / ( sub_query.answered_calls + sub_query.unanswered_calls ) )
									ELSE 0
								END ), 0 ) AS service_level,
						COALESCE( (	CASE
										WHEN ( 0 < ( sub_query.answered_calls + sub_query.unanswered_calls + sub_query.abandoned_thirty_calls ) )
										THEN ( ( sub_query.answered_calls + sub_query.unanswered_calls + sub_query.abandoned_thirty_calls ) )
									ELSE 0
								END ), 0 ) AS total_calls_count,
						COALESCE( sub_query.abandoned_thirty_calls, 0 ) AS abandoned_thirty_calls_count
					FROM (
						SELECT
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) < 31
										THEN c.id ELSE NULL
									END ) AS answered_calls_in_thirty_second,
							COUNT(	CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) . ' )
										THEN c.id ELSE NULL
									END ) AS answered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) . ' )
											AND COALESCE( c.call_center_queue_time, 0 ) > 30
										THEN c.id ELSE NULL
									END ) AS unanswered_calls,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) CCallStatusType::UNANSWERED . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypeIds() ) . ' )
											AND ( COALESCE( c.call_center_queue_time, 0 ) > 0 AND COALESCE( c.call_center_queue_time, 0 ) < 31 )
										THEN c.id ELSE NULL
									END ) AS abandoned_thirty_calls
						FROM
							calls AS c';

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' LEFT JOIN call_queues AS cq ON ( c.call_queue_id = cq.id )';
		}

		$strSql .= '	WHERE
							( c.call_result_id IS NOT NULL ) ';

		if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
			$strSql .= ' AND c.call_datetime >= \'' . $objCallFilter->getStartDate() . ' 00:00:00\'
						 AND c.call_datetime <= \'' . $objCallFilter->getEndDate() . ' 23:59:59\' ';
		}

		if( true == valStr( $objCallFilter->getCids() ) ) {
			$strSql .= ' AND c.cid IN ( ' . $objCallFilter->getCids() . ' )';
		}

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )';
		}

		$strSql .= ' AND c.is_test_call = 0
					 AND c.is_auto_detected = 0
					 AND c.call_result_id <> ' . CCallResult::SIP_TEST_CALL;

		$strSql .= ') AS sub_query ';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterCallsInQueueReportDataByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						c.id,
						c.cid,
						c.property_id,
						c.call_status_type_id,
						c.employee_id,
						c.origin_phone_number,
						c.call_datetime,
						cst.name AS call_status_type_name,
						ct.name AS call_type_name,
						cq.name AS call_queue_name,
						ca.agent_phone_extension_id AS call_agent_agent_phone_extension,
						ca.physical_phone_extension_id AS call_agent_physical_phone_extension,
						ca.full_name AS call_agent_full_name,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						pcs.property_name,
						cl.company_name
					FROM
						calls c
						LEFT JOIN call_status_types AS cst ON ( cst.id = c.call_status_type_id )
						LEFT JOIN call_types AS ct ON ( ct.id = c.call_type_id )
						LEFT JOIN call_members AS cm ON ( cm.call_id = c.id AND cm.call_agent_id = c.call_agent_id )
						LEFT JOIN call_queues AS cq ON ( cq.id = cm.call_queue_id )
						LEFT JOIN call_agents AS ca ON ( ca.employee_id = c.employee_id )
						LEFT JOIN call_agent_state_types AS castt ON (castt.id = ca.call_agent_state_type_id)
						LEFT JOIN call_agent_status_types AS casts ON (casts.id = ca.call_agent_status_type_id)
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = c.property_id )
						LEFT JOIN clients AS cl ON ( cl.id = c.cid )
					WHERE
						c.call_datetime::DATE = NOW()::DATE
						AND c.call_status_type_id IN ( ' . sqlIntImplode( $objCallFilter->getCallStatusTypeIds() ) . ' )
						AND c.call_type_id IN ( ' . sqlIntImplode( $objCallFilter->getCallTypeIds() ) . ' )';

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' AND cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' ) ';
		}

		$strSql .= ' ORDER BY c.id DESC';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchUnassociatedLeadCallsByPropertyIdByOriginPhoneNumbers( $intPropertyId, $arrintPhoneNumbers, $objVoipDatabase ) {
		if( false == valArr( $arrintPhoneNumbers ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						calls
					WHERE
						property_id = ' . ( int ) $intPropertyId . '
						AND applicant_application_id IS NULL
						AND customer_id IS NULL
						AND origin_phone_number IN ( \'' . implode( '\',\'', $arrintPhoneNumbers ) . '\' )';

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallByOriginPhoneNumberByDestinationPhoneNumber( $strOriginPhoneNumber, $strDestinationPhoneNumber, $objVoipDatabase ) {

		$strSql = 'SELECT
						id,
						cid,
						property_id,
						call_status_type_id
					FROM
						calls
					WHERE
						origin_phone_number = \'' . $strOriginPhoneNumber . '\'
						AND destination_phone_number = \'' . $strDestinationPhoneNumber . '\'
						AND created_on > ( NOW() - INTERVAL \'90 SECOND\' )
					ORDER BY
						id DESC
					LIMIT 1';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallDetailsById( $intCallId, $objVoipDatabase ) {

		$strSql = 'SELECT
						c.destination_phone_number,
						c.call_datetime,
						pcs.property_name,
						mc.company_name,
						ca.full_name
					FROM
						calls c
						LEFT JOIN property_call_settings AS pcs ON ( pcs.property_id = c.property_id )
						LEFT JOIN clients AS mc ON ( mc.id = c.cid )
						LEFT JOIN call_agents AS ca ON ( ca.id = c.call_agent_id )
					WHERE
						c.id = ' . ( int ) $intCallId;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLastHourCallsCountByCallQueueIdsByCallTypeIds( $arrintCallQueueIds, $arrintCallTypeIds, $objVoipDatabase ) {

		$strSql = 'SELECT
						COUNT( CASE
								WHEN
									c.call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' )
								THEN c.id ELSE NULL
							END ) AS calls_last_hour_count
					FROM
						calls AS c
					WHERE
						( c.call_result_id IS NULL
							OR c.call_result_id <> ' . ( int ) CCallResult::TEST_CALL . ' )
						AND c.call_datetime BETWEEN \'' . date( 'Y-m-d H:00:00', strtotime( '-1 hour' ) ) . '\' AND \'' . date( 'Y-m-d H:00:00' ) . '\'
						AND c.call_queue_id IN ( ' . implode( ',', $arrintCallQueueIds ) . ' )';

		$arrintResponse = fetchData( $strSql, $objVoipDatabase );

		if( true == isset( $arrintResponse[0]['calls_last_hour_count'] ) ) {
			return $arrintResponse[0]['calls_last_hour_count'];
		}

		return 0;
	}

	public static function fetchSimplePropertyWiseCallCountStatisticsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = ' SELECT
						c.property_id,
						COUNT(	CASE
									WHEN
										c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
									THEN
										c.id
									ELSE
										NULL
								END ) AS leasing_center_anserwered_call_count,
						COUNT(	CASE
									WHEN
										c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
										AND c.call_result_id = ' . CCallResult::LEAD . '
									THEN
										c.id
									ELSE
										NULL
								END) AS leasing_center_lead_answered_call_count,
						COUNT(	CASE
									WHEN
										c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallTrackerCallTypes() ) . ' )
									THEN
										c.id
									ELSE
										NULL
								END) AS on_site_anserwered_call_count,
						COUNT(	CASE
									WHEN
										c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallTrackerCallTypes() ) . ' )
										AND c.call_result_id = ' . CCallResult::LEAD . '
									THEN
										c.id
									ELSE
										NULL
								END) AS on_site_lead_answered_call_count
					FROM
						calls AS c
					WHERE
						c.cid IN ( ' . $objCallFilter->getCids() . ' )
						AND c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' )
						AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
						AND c.call_status_type_id = ' . $objCallFilter->getCallStatusTypeId() . '
						AND c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
						AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'
					GROUP BY
						c.property_id
					ORDER BY
						c.property_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleLeasingCenterAndOnSiteCsvCallIdsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT
						ARRAY_TO_STRING( ARRAY_AGG( CASE
							WHEN
								c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' )
							THEN
								c.id
							ELSE
								NULL
						END ), \',\' ) AS leasing_center_commma_seperated_call_ids,
						ARRAY_TO_STRING( ARRAY_AGG( CASE
							WHEN
								c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallTrackerCallTypes() ) . ' )
							THEN
								c.id
							ELSE
								NULL
						END ), \',\' ) AS on_site_commma_seperated_call_ids
					FROM
						calls AS c
					WHERE
						c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
						AND c.call_status_type_id = ' . $objCallFilter->getCallStatusTypeId() . '
						AND c.call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' )
						AND c.cid IN ( ' . $objCallFilter->getCids() . ' )
						AND c.property_id IN ( ' . $objCallFilter->getPropertyIds() . ' )
						AND c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'
						AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleLeasingCenterSurveyGenerationCallsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM (
						SELECT
							id AS call_id,
							cid,
							call_type_id,
							employee_id,
							property_id,
							call_result_id,
							call_datetime,
							ROW_NUMBER() OVER( PARTITION BY employee_id ORDER BY RANDOM() ) row_number
						FROM
							calls
						WHERE
							call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . '  )
							AND call_result_id IN ( ' . $objCallFilter->getCallResultIds() . '  )
							AND call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
							AND call_duration_seconds >= ' . ( int ) $objCallFilter->getCallDurationSeconds() . '
							AND call_datetime > ( NOW() - INTERVAL \' ' . $objCallFilter->getIntervalDays() . ' days \' )
							AND employee_id IS NOT NULL
					) AS sub_query 
					WHERE 
						sub_query.row_number IN ( 1, 2 )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsForSupportQaSurveysByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) || false == valStr( $objCallFilter->getEmployeeIds() ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM (
							WITH 
								call_limits AS (
									SELECT
										employee_id,
										COUNT ( id ),
										CASE WHEN CEIL( ROUND( COUNT ( id ), 2 ) * ' . ( int ) $objCallFilter->getPercentageCallCount() . ' / 100 ) > 2 THEN CEIL( ROUND( COUNT ( id ), 2 ) * ' . ( int ) $objCallFilter->getPercentageCallCount() . ' / 100 ) ELSE 2 END AS percentage_call
									FROM
										calls
									WHERE
										call_type_id = ' . ( int ) CCallType::CALL_CENTER . '
										AND call_result_id = ' . ( int ) CCallResult::SUPPORT . '
										AND call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
										AND call_duration_seconds >= ' . ( int ) $objCallFilter->getCallDurationSeconds() . '
										AND call_datetime > ( NOW() - INTERVAL \' ' . ( int ) $objCallFilter->getIntervalDays() . ' days \' )
										AND employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
									GROUP BY
										employee_id
								)
								SELECT
									c.id AS call_id,
									c.call_type_id,
									c.employee_id,
									call_limits.percentage_call,
									c.property_id,
									c.call_result_id,
									c.call_datetime,
									ROW_NUMBER() OVER( PARTITION BY c.employee_id ORDER BY RANDOM() ) row_number
								FROM
									call_limits
									INNER JOIN calls AS c ON ( c.employee_id = call_limits.employee_id )
								WHERE
									c.call_type_id = ' . ( int ) CCallType::CALL_CENTER . '
									AND c.call_result_id = ' . ( int ) CCallResult::SUPPORT . '
									AND c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
									AND c.call_duration_seconds >= ' . ( int ) $objCallFilter->getCallDurationSeconds() . '
									AND c.call_datetime > ( NOW() - INTERVAL \' ' . ( int ) $objCallFilter->getIntervalDays() . ' days \' )
						) AS sub_query
					WHERE
						sub_query.row_number <= sub_query.percentage_call
					ORDER BY
						sub_query.employee_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleLeasingCenterOrSupportQaSurveyCallByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strWhere = '';

		if( true == valArr( $objCallFilter->getCallIds() ) ) {
			$strWhere = ' AND c.id NOT IN (' . implode( ',', $objCallFilter->getCallIds() ) . ') ';
		}

		$strSql = 'SELECT
						c.id,
						c.cid,
						c.employee_id,
						c.call_datetime
					FROM
						calls c
						JOIN (
								SELECT
									id,
									employee_id,
									date_trunc(\'week\', call_datetime) AS week_start_date,
									(date_trunc(\'week\', call_datetime) + \'6 days\' :: interval) AS week_end_date
								FROM
									calls
								WHERE
									id = ' . ( int ) $objCallFilter->getCallId() . '
							) AS filtered_calls ON ( filtered_calls.employee_id = c.employee_id AND c.call_datetime > filtered_calls.week_start_date AND c.call_datetime < filtered_calls.week_end_date )
					WHERE
						c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
						AND c.call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' )
						AND c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
						AND c.call_duration_seconds >= 45
						' . $strWhere . '
					LIMIT 1';

		return self::fetchCall( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallsDataByCallAgentIdByStartDateByEndDate( $intCallAgentId, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valId( $intCallAgentId ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT
						CAST( call_datetime AS Date ),
						EXTRACT( ISODOW from call_datetime ) AS week_day,
						CONCAT_WS( \':\', EXTRACT( hour from call_datetime ), FLOOR( EXTRACT( minute from call_datetime ) / 30 ) * 30 ) AS week_day_time_slot,
						ROUND ( ( CAST ( CONCAT_WS( \'.\', ( EXTRACT( hour from call_datetime ) ), ( FLOOR( EXTRACT( minute from call_datetime ) / 30 ) * 30 ) ) AS FLOAT ) * 2 ) + 1 ) AS interval_sequence,
						id,
						call_datetime AS call_start_time,
						( call_datetime + call_duration_seconds * interval \'1 second\' + call_center_queue_time  * interval \'1 second\' ) AS call_end_time,
						COALESCE( call_center_queue_time, 0 ) AS waiting_time,
						COALESCE( wrap_up_time, 0 ) AS wrap_up_time
					FROM
						calls
					WHERE
						call_agent_id = ' . ( int ) $intCallAgentId . '
						AND call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND call_datetime BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59.999999\'
				ORDER BY
						id ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentCallsCountStatsByCallFilter( $objCallFilter, $objVoipDatabase, $arrstrDateIntervals = [] ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strOrderBySql	= '';
		$strWhereSql	= '';
		$strWhere		= '';

		if( true == valStr( $objCallFilter->getSortBy() ) && true == valStr( $objCallFilter->getSortDirection() ) ) {
			$strOrderBySql = ' ORDER BY ' . $objCallFilter->getSortBy() . ' ' . $objCallFilter->getSortDirection();
		}

		if( true == valId( $objCallFilter->getCallDurationSeconds() ) ) {
			$strWhereSql = ' AND c.call_center_talk_time > ' . $objCallFilter->getCallDurationSeconds();
		}

		if( true == valArr( $arrstrDateIntervals ) ) {
			foreach( $arrstrDateIntervals as $arrstrDateInterval ) {
				$strWhere .= ' c.call_datetime BETWEEN \'' . $arrstrDateInterval['start_time'] . '\' AND \'' . $arrstrDateInterval['end_time'] . '\' OR';
			}

			$strWhereSql .= ' AND (' . rtrim( $strWhere, 'OR' ) . ' )';
		} else {
			$strWhereSql .= ' AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59.999999\'';
		}

		$strSql = 'SELECT
						COALESCE(sub_query.call_count, 0) AS answered_call_count,
						sub_query.employee_id,
						sub_query.call_agent_id,
						sub_query.call_agent_name,
						sub_query.preferred_name
					FROM (
							SELECT
								count( CASE WHEN c.call_status_type_id = 4 THEN c.id ELSE NULL END ) AS call_count,
								c.employee_id AS employee_id,
								ca.id AS call_agent_id,
								ca.full_name AS call_agent_name,
								CASE
									WHEN ca.preferred_name IS NOT NULL THEN ca.preferred_name
									ELSE CONCAT( ca.name_first, \' \', ca.name_last )
								END AS preferred_name
							FROM
								calls AS c
								JOIN call_agents AS ca ON ( c.employee_id = ca.employee_id )
							WHERE
								c.call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
								AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
								AND c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getCallLogCallResults() ) . ' )
								AND c.employee_id IS NOT NULL
								AND c.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
								AND c.call_result_id IS NOT NULL
								AND c.call_result_id NOT IN ( ' . ( int ) CCallResult::UNKNOWN . ',' . ( int ) CCallResult::TEST_CALL . ',' . ( int ) CCallResult::SUPPORT . ' ) ' . $strWhereSql . '
							GROUP BY
								c.employee_id,
								ca.id,
								ca.preferred_name,
								CONCAT( ca.name_first, \' \', ca.name_last ),
								ca.full_name' . $strOrderBySql . '
					) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentMaxCallsCountStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strWhereSql = NULL;

		if( true == valId( $objCallFilter->getCallDurationSeconds() ) ) {
			$strWhereSql = ' AND c.call_center_talk_time < ' . $objCallFilter->getCallDurationSeconds();
		}

		$strWhereSql .= ' AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . '\' AND \'' . $objCallFilter->getEndDate() . '\'';

		$strSubSql = '(
						SELECT
							COALESCE(sub_query.call_count, 0) AS answered_call_count,
							sub_query.employee_id,
							sub_query.call_agent_id,
							sub_query.call_agent_name
							sub_query.preferred_name
						FROM 
						(
							SELECT
								count( CASE WHEN c.call_status_type_id = 4 THEN c.id ELSE NULL END ) AS call_count,
								c.employee_id AS employee_id,
								ca.id AS call_agent_id,
								ca.full_name AS call_agent_name,
								CASE
									WHEN ca.preferred_name IS NOT NULL THEN ca.preferred_name
									ELSE CONCAT( ca.name_first, \' \', ca.name_last )
								END AS preferred_name
							FROM
								calls AS c
								JOIN call_agents AS ca ON ( c.employee_id = ca.employee_id )
							WHERE
								c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
								AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
								AND c.call_result_id IN ( ' . $objCallFilter->getCallResultIds() . ' )
								AND c.employee_id IS NOT NULL
								AND c.employee_id IN ( ' . implode( ',', $objCallFilter->getEmployeeIds() ) . ' )
								AND c.call_result_id IS NOT NULL
								AND c.call_result_id NOT IN ( ' . ( int ) CCallResult::UNKNOWN . ',' . ( int ) CCallResult::TEST_CALL . ',' . ( int ) CCallResult::SUPPORT . ' ) ' . $strWhereSql . '
							GROUP BY
								c.employee_id,
								ca.id,
								ca.full_name,
								CONCAT( ca.name_first, \' \', ca.name_last ),
								ca.preferred_name
						) AS sub_query
					)';

		$strSql = 'SELECT
						call_agent_id,
						MAX( answered_call_count )
					FROM
						' . $strSubSql . ' AS sub_query1
					GROUP BY
						call_agent_id, answered_call_count
					HAVING
						answered_call_count =
						(
							SELECT
								MAX( answered_call_count )
							FROM ' . $strSubSql . ' AS sub_query1
						)';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallBreakdownStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strOrderBySql = '';

		if( true == valStr( $objCallFilter->getSortBy() ) && true == valStr( $objCallFilter->getSortDirection() ) ) {
			$strOrderBySql = ' ORDER BY ' . $objCallFilter->getSortBy() . ' ' . $objCallFilter->getSortDirection();
		}

		$strSql = 'SELECT
						employee_id,
						call_agent_name,
						preferred_name,
						COALESCE( ROUND( average_handle_time ), 0 ) AS average_handle_time,
						COALESCE( lead_answered_calls_count, 0 ) AS lead_answered_calls_count,
						COALESCE( ROUND( average_lead_call_time ), 0 ) AS average_lead_call_time,
						COALESCE( ROUND( average_lead_wrap_up_time ), 0 ) AS average_lead_wrap_up_time,
						COALESCE( work_order_answered_calls_count, 0) AS work_order_answered_calls_count,
						COALESCE( ROUND( average_work_order_call_time ), 0 ) AS average_work_order_call_time,
						COALESCE( ROUND( average_work_order_wrap_up_time ), 0 ) AS average_work_order_wrap_up_time,
						COALESCE( resident_answered_calls_count, 0 ) AS resident_answered_calls_count,
						COALESCE( ROUND( average_resident_call_time ), 0 ) AS average_resident_call_time,
						COALESCE( ROUND( average_resident_wrap_up_time ), 0 ) AS average_resident_wrap_up_time,
						COALESCE( other_answered_calls_count, 0 ) AS other_answered_calls_count,
						COALESCE( ROUND( average_other_call_time ), 0 ) AS average_other_call_time,
						COALESCE( ROUND( average_other_wrap_up_time ), 0 ) AS average_other_wrap_up_time
					FROM (
							SELECT
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id IN ( ' . CCallResult::LEAD . ', ' . CCallResult::WORK_ORDER . ', ' . CCallResult::RESIDENT . ', ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
													THEN ( c.call_duration_seconds + c.call_center_queue_time + c.wrap_up_time )
												END ), 0 ) AS average_handle_time,
									COUNT(
										CASE
											WHEN c.call_result_id = ' . CCallResult::LEAD . '
											THEN c.id ELSE NULL
										END ) AS lead_answered_calls_count,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::LEAD . '
													THEN ( c.call_duration_seconds + c.call_center_queue_time + c.wrap_up_time )
												END ), 0 ) AS average_lead_call_time,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::LEAD . '
													THEN c.wrap_up_time
												END ), 0 ) AS average_lead_wrap_up_time,
									COUNT(
										CASE
											WHEN c.call_result_id = ' . CCallResult::WORK_ORDER . '
											THEN c.id ELSE NULL
										END ) AS work_order_answered_calls_count,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::WORK_ORDER . '
													THEN ( c.call_duration_seconds + c.call_center_queue_time + c.wrap_up_time)
												END ), 0 ) AS average_work_order_call_time,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::WORK_ORDER . '
													THEN c.wrap_up_time
												END ), 0 ) AS average_work_order_wrap_up_time,
									COUNT(
										CASE
											WHEN c.call_result_id = ' . CCallResult::RESIDENT . '
											THEN c.id ELSE NULL
										END ) AS resident_answered_calls_count,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::RESIDENT . '
													THEN ( c.call_duration_seconds + c.call_center_queue_time + c.wrap_up_time)
												END ), 0 ) AS average_resident_call_time,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id = ' . CCallResult::RESIDENT . '
													THEN c.wrap_up_time
												END ), 0 ) AS average_resident_wrap_up_time,
									COUNT(
										CASE
											WHEN c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
											THEN c.id ELSE NULL
										END ) AS other_answered_calls_count,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
													THEN (c.call_duration_seconds + c.call_center_queue_time + c.wrap_up_time)
												END ), 0 ) AS average_other_call_time,
									COALESCE( AVG(
												CASE
													WHEN c.call_result_id IN ( ' . implode( ',', CCallResultHelper::getLeasingCenterOtherContactsCallResults() ) . ' )
													THEN c.wrap_up_time
												END ), 0 ) AS average_other_wrap_up_time,
									c.employee_id,
									ca.full_name AS call_agent_name,
									CASE
										WHEN ca.preferred_name IS NOT NULL THEN ca.preferred_name
										ELSE CONCAT( ca.name_first, \' \', ca.name_last ) 
									END AS preferred_name
							FROM
								calls AS c
								JOIN call_agents AS ca ON (c.employee_id = ca.employee_id)
							WHERE
								c.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
								AND c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
								AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
								AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59.999999\'
								AND c.call_result_id IS NOT NULL
								AND c.call_result_id <> ' . ( int ) CCallResult::TEST_CALL . '
							GROUP BY
								c.employee_id,
								ca.full_name,
								CONCAT( ca.name_first, \' \', ca.name_last ),
								ca.preferred_name' . $strOrderBySql . '
						) AS sub_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePhonePerformanceStatsByCallFilter( $objCallFilter, $objVoipDatabase, $arrstrDateIntervals = [] ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strWhereSql	= '';
		$strWhere		= '';

		if( true == valArr( $arrstrDateIntervals ) ) {
			foreach( $arrstrDateIntervals as $arrstrDateInterval ) {
				$strWhere .= ' call_datetime BETWEEN \'' . $arrstrDateInterval['start_time'] . '\' AND \'' . $arrstrDateInterval['end_time'] . '\' OR';
			}

			$strWhereSql .= ' AND (' . rtrim( $strWhere, 'OR' ) . ' )';
		} else {
			$strWhereSql .= ' AND call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59.999999\'';
		}

		$strSql = 'SELECT
						DISTINCT applicant_application_id,
						employee_id,
						id,
						cid,
						call_datetime
					FROM
						calls
					WHERE
						employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
						AND call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
						AND call_result_id = ' . ( int ) CCallResult::LEAD . '
						AND call_type_id IN ( ' . implode( ',', CCallTypeHelper::getLeasingCenterBasicCallTypes() ) . ' ) ' . $strWhereSql . '
					ORDER BY
						call_datetime ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterCallStats( $objVoipDatabase ) {

		$intInProgressCallStatusTypeId	= CCallStatusType::CALL_IN_PROGRESS;
		$intAnsweredCallStatusTypeId	= CCallStatusType::ANSWERED;
		$intUnansweredCallStatusTypeId	= CCallStatusType::UNANSWERED;
		$intTestCallResultId			= CCallResult::TEST_CALL;

		$arrintIvrActions = [
			CIvrAction::FORWARD_TO_EXTERNAL_NUMBER,
			CIvrAction::FORWARD_TO_EXTERNAL_NUMBER_WITH_RECORDING
		];

		$arrintPropertyCallTypeIds = [
			CCallType::CALL_TRACKER_LEAD,
			CCallType::CALL_TRACKER_MAINTENANCE,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY
		];

		$strSql = 'SELECT
						calls_in_queue_count,
						calls_in_progress_count,
						calls_last_hour_count,
						answered_calls_last_hour_count,
						unanswered_last_hour_calls_count,
						total_calls_count,
						total_property_calls_count,
						total_visa_calls_count,
						total_ivr_calls_count,
						total_ivr_third_party_calls_count
					FROM
					(
					SELECT
							COUNT( CASE WHEN
											c.call_status_type_id IN ( ' . implode( ',', CCallStatusType::$c_arrintInQueueCallStatusTypeIds ) . ' )
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS calls_in_queue_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intInProgressCallStatusTypeId . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
										THEN c.id ELSE NULL
									END ) AS calls_in_progress_count,
							COUNT( CASE WHEN
											c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
											AND	c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS calls_last_hour_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intAnsweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS answered_calls_last_hour_count,
							COUNT( CASE WHEN
											c.call_status_type_id = ' . ( int ) $intUnansweredCallStatusTypeId . '
											AND c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
											AND COALESCE( c.call_center_queue_time, 0 ) > 30
										THEN c.id ELSE NULL
									END ) AS unanswered_last_hour_calls_count,
							COUNT( CASE WHEN
											c.call_type_id IN ( ' . implode( ',', CCallTypeHelper::getCallShadowCallTypes() ) . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS total_calls_count,
							COUNT( CASE WHEN
											c.call_type_id IN ( ' . implode( ',', $arrintPropertyCallTypeIds ) . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS total_property_calls_count,
							COUNT( CASE WHEN
											c.call_type_id IN ( ' . CCallType::VISA_PHONE_AUTH . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS total_visa_calls_count,
							COUNT( CASE WHEN
											cpn.call_forward_preference_id = ' . CCallForwardPreference::IVR . '
											AND cpn.ivr_id IS NOT NULL
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS total_ivr_calls_count,
							COUNT( CASE WHEN
											cpn.call_forward_preference_id = ' . CCallForwardPreference::IVR . '
											AND cpn.ivr_id IS NOT NULL
											AND ima.ivr_action_id IN ( ' . implode( ',', $arrintIvrActions ) . ' )
											AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\' ) AND NOW()
										THEN c.id ELSE NULL
									END ) AS total_ivr_third_party_calls_count
							FROM
								calls AS c
								LEFT JOIN call_phone_numbers cpn ON ( cpn.id = c.call_phone_number_id)
								LEFT JOIN ivrs i ON ( cpn.ivr_id = i.id )
								LEFT JOIN ivr_menus im ON ( im.ivr_id = i.id )
								LEFT JOIN ivr_menu_actions ima ON ( ima.ivr_menu_id = im.id )';

		$strSql .= ' WHERE
						c.is_test_call = 0
						AND c.is_auto_detected = 0
						AND ( c.call_result_id IS NULL OR c.call_result_id <> ' . ( int ) $intTestCallResultId . ' )
						AND c.call_datetime BETWEEN ( NOW() - INTERVAL \'1 hour\') AND NOW() ) AS sub_query';

		$arrmixCallsData = fetchData( $strSql, $objVoipDatabase );
		return $arrmixCallsData[0];
	}

	public static function fetchAgentsDetailsAvailableForCall( $objVoipDatabase ) {

		$objVoipDatabase->validateAndOpen( false );

		$arrintCallAgentStatusTypes = [ CCallAgentStatusType::AVAILABLE, CCallAgentStatusType::AVAILABLE_ON_DEMAND, CCallAgentStatusType::ON_BREAK, CCallAgentStatusType::UNUSED, CCallAgentStatusType::ON_LUNCH, CCallAgentStatusType::ON_PROJECT, CCallAgentStatusType::ON_MEETING, CCallAgentStatusType::ON_TRAINING, CCallAgentStatusType::ON_COACHING, CCallAgentStatusType::IN_EMAIL_QUEUE, CCallAgentStatusType::IN_LEASING_CHAT ];

		$strSql = '	SELECT
						cq.queue_phone_extension_id,
						ca.call_agent_state_type_id,
						ca.call_agent_status_type_id,
						CASE WHEN ca.call_agent_status_type_id IN( ' . CCallAgentStatusType::AVAILABLE . ' , ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' ) THEN ca.full_name ELSE NULL END as available,
						CASE WHEN ca.call_agent_state_type_id IN ( ' . CCallAgentStateType::WAITING . ' , ' . CCallAgentStateType::RECEIVING . ' ) AND ca.call_agent_status_type_id IN( ' . CCallAgentStatusType::AVAILABLE . ' , ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' ) THEN ca.full_name ELSE NULL END as waiting,
						ca.id
					FROM
						call_agents ca
						LEFT JOIN
						(
							SELECT
								*
							FROM
							(
								SELECT
									c.id AS call_id,
									c.employee_id AS call_employee_id
								FROM
									calls c
								WHERE
									c.call_type_id IN ( ' . CCallType::CALL_CENTER . ' )
								AND c.call_status_type_id IN ( ' . CCallStatusType::OFFERING_TO_DESTINATION . ' , ' . CCallStatusType::CALL_IN_PROGRESS . ' , ' . CCallStatusType::IN_CALL_CENTER_QUEUE . ' )
								AND c.call_datetime::DATE = now()::DATE
							) AS sub_query
						) AS call_query ON call_query.call_employee_id = ca.employee_id
						LEFT JOIN call_agent_queues caq ON ( caq.call_agent_id = ca.id ) AND caq.call_queue_id NOT IN ( ' . CCallQueue::SUPPORT_TIER_TWO . ' , ' . CCallQueue::SUPPORT_UDR . ' , ' . CCallQueue::SUPPORT_GID_WINDSOR . ' )
						LEFT JOIN call_queues cq ON cq.id = caq.call_queue_id AND cq.queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintAllPhoneExtensionIds ) . ' )
					WHERE
						ca.call_agent_status_type_id IN ( ' . implode( ',', $arrintCallAgentStatusTypes ) . ' )
						AND ca.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
						AND cq.description IS NOT NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallStatsForTvDisplay( $objVoipDatabase ) {

		$objVoipDatabase->validateAndOpen( false );

		$strSql = 'SELECT
						COUNT( c.id ) AS calls_in_queue,
						CASE
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintResidentPhoneExtensionIds ) . ' ) THEN \'resident_calls\'
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintLeasingPhoneExtensionIds ) . ' ) THEN \'leasing_calls\'
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintCorePhoneExtensionIds ) . ' ) THEN \'core_calls\'
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintMarketingPhoneExtensionIds ) . ' ) THEN \'marketing_calls\'
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintResidentVerifyPhoneExtensionIds ) . ' ) THEN \'resVerify_calls\'
							WHEN queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintGeneralPhoneExtensionIds ) . ' ) THEN \'general_calls\'
							ELSE \'other\'
						END AS product_name,
						TO_CHAR( ( MAX(CASE
							WHEN c.call_status_type_id IN ( ' . CCallStatusType::ATTENDED_CALL_TRANSFER . ',' . CCallStatusType::IN_CALL_CENTER_QUEUE . ',' . CCallStatusType::OFFERING_TO_DESTINATION . ' )
								AND ( c.call_result_id IS NULL OR c.call_result_id NOT IN ( ' . CCallResult::TEST_CALL . ', ' . CCallResult::SIP_TEST_CALL . ' ) ) THEN COALESCE ( ceil ( EXTRACT ( EPOCH FROM( NOW ( ) - ce.updated_on ) ) ), 0 )
							ELSE NULL
						  END )::integer || \' second\')::interval, \'MI:SS\') AS longest_queue_time_in_seconds
					FROM
						calls c
						JOIN call_queues cq ON c.call_queue_id = cq.id AND cq.queue_phone_extension_id IN ( ' . implode( ',', self::$c_arrintAllPhoneExtensionIds ) . ' )
						LEFT JOIN call_events ce ON ce.call_id = c.id AND ce.call_event_type_id = ' . CCallEventType::IN_QUEUE . '
					WHERE
						c.call_status_type_id IN ( ' . CCallStatusType::IN_CALL_CENTER_QUEUE . ' )
						AND c.call_datetime BETWEEN (now() - interval \'1 hour\') AND now()
					GROUP BY
						product_name';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterTwilioCallsByCallDateTime( $strCallDateTime, $objVoipDatabase ) {
		$strSql = 'WITH temp_data as (
						SELECT *
							FROM (
									SELECT
										MIN( c.call_datetime ) AS call_datetime,
										COUNT( c.id ) AS call_count,
										COUNT( CASE WHEN ( c.call_response_type_id = ' . CCallResponseType::WRONG_MESSAGE_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::INVALID_MESSAGE_RESPONSE . ' ) THEN 1 ELSE NULL END ) as incorrect_response_calls_count,
										COUNT( CASE WHEN ( ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NOT NULL ) THEN 1 ELSE NULL END ) as no_response_calls_count,
										COUNT( CASE WHEN ( ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . '  OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NULL AND c.session_uuid IS NOT NULL ) THEN 1 ELSE NULL END ) as unanswered_calls_count,
										COUNT( CASE WHEN ( ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . '  OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NULL AND c.session_uuid IS NULL ) THEN 1 ELSE NULL END ) as failed_calls_count,
										COUNT( CASE WHEN c.call_response_type_id is NULL THEN 1 ELSE NULL END ) as answered_calls_count
									FROM calls c
									WHERE
										c.call_datetime BETWEEN \'' . $strCallDateTime . ' 00:00:00\'
										AND \'' . $strCallDateTime . ' 23:59:59.999999\'
										AND c.is_test_call = 1
										AND c.is_auto_detected = 1
									GROUP BY
										date_part( \'hour\', c.call_datetime ),
										floor( date_part(\'minute\', c.call_datetime) / 30 )
								) data
								RIGHT JOIN generate_series(\'' . $strCallDateTime . ' 00:00:00\', \'' . $strCallDateTime . ' 23:59:59\', \'30 minute\'::INTERVAL) range
								ON ( data.call_datetime BETWEEN range AND range + INTERVAL \'30 minutes\' ) )
								SELECT
										row_to_json ( summary_query.* )
									FROM
										(
											SELECT
												COALESCE( SUM ( incorrect_response_calls_count ), 0 ) AS incorrect_response_calls_count,
												COALESCE( SUM ( no_response_calls_count ), 0 ) AS no_response_calls_count,
												COALESCE( SUM ( unanswered_calls_count ), 0 ) AS unanswered_calls_count,
												COALESCE( SUM ( failed_calls_count ), 0 ) AS failed_calls_count,
												COALESCE( SUM ( answered_calls_count ), 0 ) AS answered_calls_count
											FROM
												temp_data
										) summary_query
									UNION ALL
									SELECT
										row_to_json ( graph_query.* )
									FROM
										(
											SELECT
												TO_CHAR( range, \'HH:MI AM\' ) AS start_time,
												call_count::TEXT,
												incorrect_response_calls_count,
												no_response_calls_count,
												unanswered_calls_count,
												failed_calls_count,
												answered_calls_count
											FROM
												temp_data
										) graph_query';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterTwilioCallDetailsByCallDateTimeByCallResponseTypeId( $strCallDateTime, $intCallResponseTypeId, $objVoipDatabase ) {
		$strSql		= ' SELECT
							c.*
						FROM
							calls c
						WHERE
							c.is_test_call = 1
							AND c.is_auto_detected = 1
							AND c.call_datetime between \'' . $strCallDateTime . '\' AND ( \'' . $strCallDateTime . '\'::timestamp + INTERVAL \'30 minutes\' ) ';

		switch( $intCallResponseTypeId ) {
			case CCallResponseType::SUCCESS:
				$strSql .= 'AND c.call_response_type_id is NULL ';
				break;

			case CCallResponseType::WRONG_MESSAGE_RESPONSE:
				$strSql .= 'AND ( c.call_response_type_id = ' . CCallResponseType::WRONG_MESSAGE_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::INVALID_MESSAGE_RESPONSE . ' ) ';
				break;

			case CCallResponseType::NO_RESPONSE_FROM_USER:
				$strSql .= 'AND ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NOT NULL ';
				break;

			case CCallResponseType::NO_ANSWER_FROM_USER:
				$strSql .= 'AND ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NULL AND c.session_uuid IS NOT NULL';
				break;

			case CCallResponseType::ORIGINATOR_CANCEL_RESPONSE:
				$strSql .= 'AND ( c.call_response_type_id = ' . CCallResponseType::NO_ANSWER_FROM_USER . ' OR c.call_response_type_id = ' . CCallResponseType::ORIGINATOR_CANCEL_RESPONSE . ' OR c.call_response_type_id = ' . CCallResponseType::NO_RESPONSE_FROM_USER . ' ) AND c.call_agent_id IS NULL AND c.session_uuid IS NULL';
				break;

			default:
				// Do nothing
				break;
		}

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallDetailsForGraphByPropertyIdsByCid( $arrmixPropertyIds, $strCompareCallsSql, $intCid, $objVoipDatabase ) {
		$strSql = ' SELECT
							EXTRACT( hour FROM c.call_datetime ) AS call_hour,
							count( id ) AS all_maintenance_calls_count
						FROM
							calls AS c
						WHERE
							c.cid = ' . ( int ) $intCid . '
							AND c.property_id IN ( ' . implode( ', ', $arrmixPropertyIds ) . ' )
							AND call_result_id = ' . CCallResult::WORK_ORDER . '
							AND call_type_id IN ( ' . CCallType::CALL_CENTER_LEAD . ', ' . CCallType::CALL_CENTER_MAINTENANCE . ', ' . CCallType::CALL_CENTER_EMERGENCY_MAINTENANCE . ', ' . CCallType::NOTIFICATIONS . ', ' . CCallType::CALL_TRACKING_OFFICE . ' )
							AND ' . $strCompareCallsSql . '
						GROUP BY
							call_hour
						ORDER BY
							call_hour';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleAverageWrapUpTimeByCallAgentIdsByStartDateByEndDate( $arrintCallAgentIds, $strStartDate, $strEndDate, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT
						call_agent_id,
						employee_id,
						AVG( wrap_up_time ) AS avg_wrap_up_time
					FROM
						calls
					WHERE
						call_agent_id IN ( ' . sqlIntImplode( $arrintCallAgentIds ) . ' )
						AND call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
						AND call_datetime BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59.999999\'
					GROUP BY
						call_agent_id,
						employee_id';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentsCallAnalysisDataByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT 
						c.analyzed_by,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . '
									THEN c.id ELSE NULL
								END ), 0 ) AS total_analyzed_calls,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id IN ( ' . $objCallFilter->getCallAnalysisStatusTypes() . ' )
									THEN c.id ELSE NULL
								END ), 0 ) AS total_evaluated,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' 
									THEN c.id ELSE NULL
								END ), 0 ) AS total_non_lead_call,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' 
									THEN c.id ELSE NULL
								END ), 0 ) AS total_disqualified_lead_call,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . '
									THEN c.id ELSE NULL
								END ), 0 ) AS total_warm_lead,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' 
									THEN c.id ELSE NULL
								END ), 0 ) AS total_early_termination,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' 
									THEN c.id ELSE NULL
								END ), 0 ) AS total_flagged_for_analysis,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' 
									THEN c.id ELSE NULL
								END ), 0 ) AS total_max_calls_evaluated,
						COALESCE( COUNT( CASE WHEN 
										c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . '
									THEN c.id ELSE NULL
								END ), 0 ) AS total_voicemail
					FROM 
						calls AS c
					WHERE 
						c.analyzed_on BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:01\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'
						AND c.call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
						AND c.analyzed_by IN ( ' . $objCallFilter->getUserIds() . ' )
						AND c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
						AND c.call_duration_seconds >= ' . ( int ) $objCallFilter->getCallDurationSeconds() . '
					GROUP BY
						c.analyzed_by';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAnalysisPerformanceDataByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return NULL;

		$strSql = 'SELECT COUNT(*) AS all_calls,
						SUM( CASE WHEN c.call_result_id = ' . ( int ) CCallResult::LEAD . ' THEN 1 ELSE 0 END ) AS lead_calls,
						SUM( CASE WHEN ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) THEN 1 ELSE 0 END ) AS non_lead_calls,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::UNEVALUATED . ' THEN 1 ELSE 0 END ) AS unevaluated_all,
						SUM(
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' THEN 1 ELSE 0 END + 
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' THEN 1 ELSE 0 END +
							CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' THEN 1 ELSE 0 END
							) AS evaluated_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' THEN 1 ELSE 0 END ) AS analyzed_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' THEN 1 ELSE 0 END ) AS non_lead_calls_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' THEN 1 ELSE 0 END ) AS disqualified_lead_calls_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' THEN 1 ELSE 0 END ) AS warm_leads_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' THEN 1 ELSE 0 END ) AS early_terminations_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' THEN 1 ELSE 0 END ) AS flagged_for_analysis_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' THEN 1 ELSE 0 END ) AS max_calls_evaluated_all,
						SUM( CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' THEN 1 ELSE 0 END ) AS voicemail_all,
							SUM( CASE WHEN ( c.call_analysis_status_type_id = 1 AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS unevaluated_lead,
							SUM( 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END + 
							CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END 
							) AS evaluated_lead,
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS analyzed_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS non_lead_calls_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS disqualified_lead_calls_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS warm_leads_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS early_terminations_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS flagged_for_analysis_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS max_calls_evaluated_lead, 
							SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END ) AS voicemail_lead,
								SUM( CASE WHEN ( c.call_analysis_status_type_id = 1 AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS unevaluated_non_lead,
								SUM( 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END + 
								CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END 
								) AS evaluated_non_lead,
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS analyzed_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS non_lead_calls_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS disqualified_lead_calls_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS warm_leads_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS early_terminations_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS flagged_for_analysis_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS max_calls_evaluated_non_lead, 
								SUM( CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' AND ( c.call_result_id != ' . ( int ) CCallResult::LEAD . ' OR c.call_result_id IS NULL ) ) THEN 1 ELSE 0 END ) AS voicemail_non_lead,
									SUM( 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::ANALYZED . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::NON_LEAD_CALL . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::DISQUALIFIED_LEAD_CALL . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::WARM_LEAD . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::EARLY_TERMINATION . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::FLAGGED_FOR_ANALYSIS . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::MAX_CALLS_EVALUATED . ' THEN 1 ELSE 0 END + 
									CASE WHEN c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::VOICEMAIL . ' THEN 1 ELSE 0 END + 
									CASE WHEN ( c.call_analysis_status_type_id = ' . ( int ) CCallAnalysisStatusType::UNEVALUATED . ' AND c.call_result_id = ' . ( int ) CCallResult::LEAD . ' ) THEN 1 ELSE 0 END 
									) AS original_leads
					FROM 
						calls AS c
					WHERE
						c.cid IN ( ' . $objCallFilter->getCids() . ' )
						AND c.call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59.999999\'
						AND c.call_type_id IN ( ' . sqlIntImplode( $objCallFilter->getCallTypeIds() ) . ' )
						AND c.call_status_type_id = ' . ( int ) $objCallFilter->getCallStatusTypeId() . '
						AND c.call_duration_seconds >= ' . ( int ) $objCallFilter->getCallDurationSeconds() . '
						AND c.call_file_id IS NOT NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallsHistoryByCallFilter( $objPagination, $objCallFilter, $objVoipDatabase, $boolIsCountOnly = false ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) || false == valId( $objCallFilter->getEmployeeId() ) ) return NULL;

		$arrstrWhere = [];

		if( true == valId( $objCallFilter->getCallId() ) ) {
			$arrstrWhere[] = 'c.id = ' . ( int ) $objCallFilter->getCallId();
		} else {
			if( 'archived_calls' == $objCallFilter->getCallsState() ) {
				$arrstrWhere[] = ' c.deleted_on IS NOT NULL ';
			} elseif( 'current_calls' == $objCallFilter->getCallsState() ) {
				$arrstrWhere[] = ' c.deleted_on IS NULL ';
			}

			if( true == valStr( $objCallFilter->getStartDate() ) && true == valStr( $objCallFilter->getEndDate() ) ) {
				if( 'Min' != $objCallFilter->getStartDate() && 'Max' != $objCallFilter->getEndDate() ) {
					$arrstrWhere[] = 'c.call_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'' . ' AND \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
				} elseif( 'Min' == $objCallFilter->getStartDate() && 'Max' != $objCallFilter->getEndDate() ) {
					$arrstrWhere[] = 'c.call_datetime <= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getEndDate() ) ) . ' 23:59:59.999999\'';
				} elseif( 'Min' != $objCallFilter->getStartDate() && 'Max' == $objCallFilter->getEndDate() ) {
					$arrstrWhere[] = 'c.call_datetime >= \'' . date( 'Y-m-d', strtotime( $objCallFilter->getStartDate() ) ) . ' 00:00:00\'';
				}
			}

			if( true == valStr( $objCallFilter->getPhoneNumber() ) ) {
				$intPhoneNumber = preg_replace( '/\D/', '', trim( $objCallFilter->getPhoneNumber() ) );

				if( 0 < trim( strlen( $intPhoneNumber ) ) ) {
					$arrstrWhere[] = '( c.origin_phone_number = \'' . addslashes( $intPhoneNumber ) . '\' OR c.destination_phone_number = \'' . addslashes( $intPhoneNumber ) . '\' )';
				}
			}

			if( true == valArr( $objCallFilter->getCidsArray() ) ) {
				$arrstrWhere[] = ' c.cid IN ( ' . implode( ',', $objCallFilter->getCidsArray() ) . ' ) ';
			}

			if( true == valArr( $objCallFilter->getPropertyIdsArray() ) ) {
				$arrstrWhere[] = 'c.property_id IN ( ' . implode( ',', $objCallFilter->getPropertyIdsArray() ) . ' ) ';
			}

			if( true == valArr( $objCallFilter->getCallResultIdsArray() ) ) {
				$arrstrWhere[] = 'c.call_result_id IN ( ' . implode( ',', $objCallFilter->getCallResultIdsArray() ) . ' ) ';
			}

			if( true == valStr( $objCallFilter->getAnswerStatusTypeIds() ) ) {
				if( true == valId( $objCallFilter->getAnswerStatusTypeIds() ) && CCallStatusType::UNANSWERED == $objCallFilter->getAnswerStatusTypeIds() ) {
					$arrstrWhere[] = ' ( c.call_status_type_id = ' . CCallStatusType::UNANSWERED . ' AND c.call_center_queue_time > 30 ) ';
				} elseif( true == valId( $objCallFilter->getAnswerStatusTypeIds() ) && CCallStatusType::ANSWERED == $objCallFilter->getAnswerStatusTypeIds() ) {
					$arrstrWhere[] = ' c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' ';
				} else {
					$arrstrWhere[] = ' ( c.call_status_type_id = ' . CCallStatusType::ANSWERED . ' OR ( c.call_status_type_id = ' . CCallStatusType::UNANSWERED . ' AND c.call_center_queue_time > 30 ) ) ';
				}
			}
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT
							COUNT( * )
						FROM calls AS c ';
		} else {
			$strSql = ' SELECT
							c.*,
							cst.name AS call_status_type_name,
							cr.name As call_result_name
						FROM
							calls AS c ';
		}

		$strSql .= 'LEFT JOIN call_status_types AS cst ON ( c.call_status_type_id = cst.id )
					LEFT JOIN call_results AS cr ON ( c.call_result_id = cr.id )
					WHERE
						c.employee_id =  ' . ( int ) $objCallFilter->getEmployeeId() . '
						AND c.call_result_id NOT IN ( ' . CCallResult::TEST_CALL . ', ' . CCallResult::SIP_TEST_CALL . ' )';

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' AND ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = fetchData( $strSql, $objVoipDatabase );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		} else {
			$strSql .= ' ORDER BY c.call_datetime DESC';

			if( false == $boolIsCountOnly && true == isset( $objPagination ) && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
				$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallDetailRecordsByIds( $arrintCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallIds ) ) return NULL;

		$strSql = 'WITH call_event_query AS
					(
						SELECT
							*,
							ROW_NUMBER() OVER( ORDER BY created_on ) AS call_event_order
						FROM
							call_events
							WHERE call_id IN ( ' . implode( ',', $arrintCallIds ) . ' ) )
				SELECT
					c.id,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce5.updated_on - ce1.updated_on ) ) ), 0) AS call_duration_seconds,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce3.updated_on - ce2.updated_on ) ) ), 0) AS call_center_queue_time,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce5.updated_on - ce6.updated_on ) ) ), 0) AS call_center_voicemail_time,
					COALESCE( ceil(EXTRACT(EPOCH FROM (ce6.updated_on - ce2.updated_on ) ) ), 0) AS call_center_voicemail_queue_time,
					COALESCE
					(
						(
							CASE WHEN ( ce4.id IS NOT NULL AND ce3.id IS NOT NULL )
									THEN ( ceil( EXTRACT(EPOCH FROM ( ce4.created_on - ce3.created_on ) ) ) )
									WHEN ( ce4.id IS NULL AND ce3.id IS NOT NULL )
									THEN ( ceil( EXTRACT(EPOCH FROM ( now() - ce3.created_on ) ) ) )
									ELSE 0
							END
						), 0
					) AS call_center_talk_time
				FROM calls c
					LEFT JOIN call_event_query AS ce1 ON (ce1.call_id = c.id AND ce1.call_event_type_id = ' . CCallEventType::CALL_START . ' )
					LEFT JOIN call_event_query AS ce2 ON (ce2.call_id = c.id AND ce2.call_event_type_id = ' . CCallEventType::IN_QUEUE . ' )
					LEFT JOIN call_event_query AS ce3 ON (ce3.call_id = c.id AND ce3.call_event_type_id = ' . CCallEventType::BRIDGE_START . ' OR ce3.call_event_type_id = ' . CCallEventType::BRIDGE_FAILED . ' )
					LEFT JOIN call_event_query AS ce4 ON (ce4.call_id = c.id AND ce4.call_event_type_id = ' . CCallEventType::BRIDGE_END . ' )
					LEFT JOIN call_event_query AS ce5 ON (ce5.call_id = c.id AND ce5.call_event_type_id = ' . CCallEventType::CALL_STOP . ' )
					LEFT JOIN call_event_query AS ce6 ON (ce6.call_id = c.id AND ce6.call_event_type_id = ' . CCallEventType::ROUTE_TO_VOICEMAIL . ' )
				WHERE
					c.id IN ( ' . implode( ',', $arrintCallIds ) . ' )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsForTranscriptionsByPropertyIds( $arrintPropertyIds, $objVoipDatabase, $intLimit = 10 ) {

		if( false == valArr( $arrintPropertyIds ) ) return;

		$arrintCallTypeIds = [
			CCallType::CALL_TRACKER_LEAD_VIA_IVR,
			CCallType::CALL_TRACKER_MAINTENANCE_VIA_IVR,
			CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY_VIA_IVR,
			CCallType::LEASING_CENTER_OUTBOUND
		];

		$arrintCallIds = [ 44920386, 44612636, 44218283, 45463123, 45428341, 45239777, 44929353, 44928916, 44880300 ];

		$strSql = 'SELECT 
						*
					FROM 
						calls
					WHERE 
					calls.id > 43957460
					AND calls.id NOT IN (' . implode( ', ', $arrintCallIds ) . ' )
					AND cid IS NOT NULL 
					AND call_duration_seconds >= 45
					AND call_file_id IS NOT NULL
					AND call_status_type_id = ' . CCallStatusType::ANSWERED . '
					AND call_type_id NOT IN ( ' . implode( ', ', $arrintCallTypeIds ) . ' ) 
					AND property_id IS NOT NULL
					AND property_id IN ( ' . implode( ', ', $arrintPropertyIds ) . ' ) 
					AND calls.id NOT IN (
											SELECT 
												call_id
											FROM 
												call_transcriptions 
										) 
					LIMIT ' . ( int ) $intLimit;

		return CCalls::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentAnsweredCallStatsByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		if( false == valId( $intEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						COUNT( id ) AS answered_call_count
					FROM
						calls AS c
					WHERE
						c.call_status_type_id = ' . CCallStatusType::ANSWERED . '
						AND c.employee_id = ' . ( int ) $intEmployeeId . '
					GROUP BY
						c.employee_id
					HAVING 
						COUNT( id ) >= 10000';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSuccessCallsByOutboundCallIds( $arrintOutboundCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintOutboundCallIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						calls
					WHERE
						outbound_call_id IN ( ' . implode( ',', $arrintOutboundCallIds ) . ' )
						AND call_status_type_id = ' . CCallStatusType::ANSWERED;

		return self::fetchCalls( $strSql, $objVoipDatabase );
	}

	public static function fetchCallsArrayByCallFilter( $objCallFilter, $objVoipDatabase, $arrstrCallColumns = [ 'id', 'cid', 'property_id', 'applicant_application_id' ] ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) return false;

		if( true == valStr( $objCallFilter->getCallColumns() ) ) {
			$strCallColumns = $objCallFilter->getCallColumns();
		} else {
			$strCallColumns = implode( ', ', $arrstrCallColumns );
		}

		$strSql = 'SELECT 
						 ' . $strCallColumns . ' 
					FROM 
						calls 
					WHERE cid IN ( ' . $objCallFilter->getCids() . ' )
					AND call_status_type_id = ' . ( int ) CCallStatusType::ANSWERED . '
					AND call_type_id IN ( ' . $objCallFilter->getCallTypeIds() . ' )
					AND call_datetime BETWEEN \'' . $objCallFilter->getStartDate() . '\' AND \'' . $objCallFilter->getEndDate() . '\'
					AND applicant_application_id IS NOT NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchAsyncCallsToEventByCidByCallTypeIdsByCallDatetime( $intCid, $arrintCallTypeIds, $strStartDateTime, $strEndDateTime, $objVoipDatabase, $intRecordLimit ) {
		if( false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strWhereCondition = NULL;
		if( false == is_null( $strStartDateTime ) && false == is_null( $strEndDateTime ) ) {
			$strStartDateTime  = date( 'Y-m-d', strtotime( $strStartDateTime ) ) . ' 00:00:00';
			$strEndDateTime    = date( 'Y-m-d', strtotime( $strEndDateTime ) ) . ' 23:59:59.999999';
			if( strtotime( $strStartDateTime ) <= strtotime( $strEndDateTime ) ) {
				$strWhereCondition .= ' AND call_datetime BETWEEN \'' . $strStartDateTime . '\' AND \'' . $strEndDateTime . '\'';
			}
		}

		$strSql = 'SELECT 
						*
					FROM 
						calls 
					WHERE
						cid = ' . ( int ) $intCid . ' 
						AND details->\'is_synced\' IS NULL
						AND call_type_id IN( ' . implode( ',', $arrintCallTypeIds ) . ' )' . $strWhereCondition . ' LIMIT ' . ( int ) $intRecordLimit;

		return CCalls::fetchCalls( $strSql, $objVoipDatabase );
	}

}
?>