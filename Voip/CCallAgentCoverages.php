<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentCoverages
 * Do not add any new functions to this class.
 */

class CCallAgentCoverages extends CBaseCallAgentCoverages {

	public static function fetchCallAgentCoveragesByEmployeeIdByEmployeeVacationRequestIds( $intEmployeeId, $arrintEmployeeVacationRequestIds, $objVoipDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valArr( $arrintEmployeeVacationRequestIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND employee_vacation_request_id IN ( ' . implode( ',', $arrintEmployeeVacationRequestIds ) . ' )
						AND covered_by IS NULL
						AND covered_on IS NULL';

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentCoveragesByEmployeeIdByEmployeeVacationRequestId( $intEmployeeId, $intEmployeeVacationRequestId, $objVoipDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valId( $intEmployeeVacationRequestId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND employee_vacation_request_id = ' . ( int ) $intEmployeeVacationRequestId . '
						AND covered_by IS NOT NULL
						AND covered_on IS NOT NULL';

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentCoveragesByEmployeeIdByEmployeeVacationRequestIdByIsCoverageRequired( $intEmployeeId, $intEmployeeVacationRequestId, $objVoipDatabase ) {
		if( false == valId( $intEmployeeId ) || false == valId( $intEmployeeVacationRequestId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND employee_vacation_request_id = ' . ( int ) $intEmployeeVacationRequestId . '
						AND covered_by IS NOT NULL';

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallAgentCoveragesByCoveredBy( $intCallAgentId, $objPagination, $objVoipDatabase, $boolCountOnly = false ) {
		if( false == valId( $intCallAgentId ) ) return NULL;

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(id) AS call_agent_coverage_count FROM call_agent_coverages WHERE covered_by = ' . ( int ) $intCallAgentId;
		} else {
			if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) return NULL;
			$strSql = 'SELECT
							*
						FROM
							call_agent_coverages
						WHERE
							covered_by = ' . ( int ) $intCallAgentId . '
						ORDER BY
							begin_datetime DESC
						OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolCountOnly ) {
			$arrintCoverageRequests = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintCoverageRequests[0]['call_agent_coverage_count'] ) ) ? $arrintCoverageRequests[0]['call_agent_coverage_count'] : 0;
		} else {
			return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
		}

	}

	public static function fetchPaginatedIndependentCallAgentCoveragesByCallAgentIdByCreatedBy( $objPagination, $intCallAgentId, $intCreatedBy, $objVoipDatabase, $boolCountOnly = false ) {
		if( false == valId( $intCallAgentId ) || false == valId( $intCreatedBy ) ) return NULL;

		$strOrderBy			= NULL;
		$strOffsetLimit		= NULL;
		$strWhereSql		= ' WHERE employee_vacation_request_id IS NULL AND call_agent_id = ' . ( int ) $intCallAgentId . ' AND created_by = ' . ( int ) $intCreatedBy;

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(id) AS coverage_request_count FROM call_agent_coverages';
		} else {
			if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) return NULL;

			$strSql = 'SELECT
							*
						FROM
							call_agent_coverages';

			$strOrderBy		= ' ORDER BY begin_datetime DESC';
			$strOffsetLimit = ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		$strSql .= $strWhereSql;
		$strSql .= $strOrderBy;
		$strSql .= $strOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintCoverageRequests = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintCoverageRequests[0]['coverage_request_count'] ) ) ? $arrintCoverageRequests[0]['coverage_request_count'] : 0;
		} else {
			return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchPaginatedIndependentCallAgentCoveragesByCoveredBy( $intCallAgentId, $objPagination, $objVoipDatabase, $boolCountOnly = false ) {
		if( false == valId( $intCallAgentId ) ) return NULL;

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(id) AS coverage_request_count FROM call_agent_coverages WHERE employee_vacation_request_id IS NULL AND covered_by = ' . ( int ) $intCallAgentId;
		} else {
			if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) return NULL;
			$strSql = 'SELECT
							*
						FROM
							call_agent_coverages
						WHERE
							employee_vacation_request_id IS NULL
							AND covered_by = ' . ( int ) $intCallAgentId . '
						ORDER BY
							begin_datetime DESC
						OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolCountOnly ) {
			$arrintCoverageRequests = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintCoverageRequests[0]['coverage_request_count'] ) ) ? $arrintCoverageRequests[0]['coverage_request_count'] : 0;
		} else {
			return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchPaginatedIndependantCallAgentCoveragesByCallAgentCoverageRequestReportFilter( $objCallAgentCoverageRequestsFilter, $objPagination, $objVoipDatabase, $boolIsCountOnly = false ) {
		$strSqlOffsetLimit	= NULL;
		$strSqlOrderBy		= NULL;
		$arrstrWhereSql		= [];
		$strStatusSql		= [];

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT
							COUNT(cac.id) AS call_agent_coverage_count
						FROM
							call_agent_coverages AS cac 
							JOIN call_agents AS ca ON( cac.call_agent_id = ca.id )
						WHERE
							ca.department_id = \'' . ( int ) $objCallAgentCoverageRequestsFilter->getDepartmentId() . '\'
							AND cac.employee_vacation_request_id IS NULL
							AND cac.is_coverage_required = true';
		} else {
			if( false == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) return NULL;

			$strSql = 'SELECT 
							cac.* 
						FROM 
							call_agent_coverages AS cac 
							JOIN call_agents AS ca ON( cac.call_agent_id = ca.id ) 
						WHERE 
							ca.department_id = \'' . ( int ) $objCallAgentCoverageRequestsFilter->getDepartmentId() . '\'
							AND employee_vacation_request_id IS NULL 
							AND is_coverage_required = true';

			if( true == valStr( $objCallAgentCoverageRequestsFilter->getSortBy() ) && true == valStr( $objCallAgentCoverageRequestsFilter->getSortDirection() ) ) {
				if( 'status' == $objCallAgentCoverageRequestsFilter->getSortBy() ) {
					$strSqlOrderBy = ' ORDER BY cac.approved_by ' . $objCallAgentCoverageRequestsFilter->getSortDirection() . ', cac.call_agent_id ' . $objCallAgentCoverageRequestsFilter->getSortDirection();
				} else {
					$strSqlOrderBy = ' ORDER BY cac.begin_datetime ' . $objCallAgentCoverageRequestsFilter->getSortDirection() . ', cac.end_datetime ' . $objCallAgentCoverageRequestsFilter->getSortDirection() . ', cac.id ' . $objCallAgentCoverageRequestsFilter->getSortDirection();
				}
			}

			$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == valStr( $objCallAgentCoverageRequestsFilter->getStartDate() ) && true == valStr( $objCallAgentCoverageRequestsFilter->getEndDate() ) ) {
			$arrstrWhereSql[] = ' (
										( \'' . $objCallAgentCoverageRequestsFilter->getStartDate() . '\' BETWEEN cac.begin_datetime::DATE AND cac.end_datetime::DATE ) OR
										( \'' . $objCallAgentCoverageRequestsFilter->getEndDate() . '\' BETWEEN cac.begin_datetime::DATE AND cac.end_datetime::DATE ) OR
										( \'' . $objCallAgentCoverageRequestsFilter->getStartDate() . '\' <= cac.end_datetime::DATE AND \'' . $objCallAgentCoverageRequestsFilter->getEndDate() . '\' >= cac.end_datetime::DATE )
									)';
		} else {
			$arrstrWhereSql[] = ' cac.begin_datetime >= NOW()';
		}

		if( true == valStr( $objCallAgentCoverageRequestsFilter->getCallAgentVacationRequestStatuses() ) ) {
			$arrstrCallAgentVacationRequestStatuses = explode( ',', $objCallAgentCoverageRequestsFilter->getCallAgentVacationRequestStatuses() );
			foreach( $arrstrCallAgentVacationRequestStatuses as $strStatus ) {
				if( 'pending' == $strStatus ) {
					$strStatusSql[] = ' cac.approved_by IS NULL AND cac.denied_by IS NULL ';
				}

				if( 'approved' == $strStatus ) {
					$strStatusSql[] = ' cac.approved_by IS NOT NULL AND cac.denied_by IS NULL ';
				}

				if( 'denied' == $strStatus ) {
					$strStatusSql[] = ' cac.approved_by IS NULL AND cac.denied_by IS NOT NULL ';
				}
			}
		}

		if( true == valArr( $arrstrWhereSql ) ) {
			$strSql .= ' AND ' . implode( ' AND ', $arrstrWhereSql );
		}

		if( true == valArr( $strStatusSql ) ) {
			$strSql .= ' AND ( ' . implode( ' OR ', $strStatusSql ) . ' )';
		}

		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolIsCountOnly ) {
			$arrintCoverageRequests = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintCoverageRequests[0]['call_agent_coverage_count'] ) ) ? $arrintCoverageRequests[0]['call_agent_coverage_count'] : 0;
		} else {
			return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallAgentCoveragesByIds( $arrintIds, $objVoipDatabase ) {
		if( false == valArr( $arrintIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						id IN ( ' . implode( ',', $arrintIds ) . ' )';

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentCoveragesByIdByCallAgentIdByStartDateByEndDateByCreatedBy( $intCallAgentCoverageId, $intCallAgentId, $strBeginDatetime, $strEndDatetime, $intCreatedBy, $boolExcludeSelf, $objVoipDatabase ) {
		if( false == valId( $intCallAgentCoverageId ) || false == valId( $intCallAgentId ) || false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) || false == valId( $intCreatedBy ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						call_agent_id = ' . ( int ) $intCallAgentId . '
						AND begin_datetime <= \'' . $strBeginDatetime . '\'
						AND end_datetime >= \'' . $strEndDatetime . '\'
						AND created_by = ' . ( int ) $intCreatedBy;

		if( true == $boolExcludeSelf ) {
			$strSql .= ' AND id <> ' . ( int ) $intCallAgentCoverageId;
		}

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

	public static function fetchApprovedCallAgentCoveragesCallAgentIdsByStartDateByEndDate( $arrintCallAgentIds, $strBeginDatetime, $strEndDatetime, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valStr( $strBeginDatetime ) || false == valStr( $strEndDatetime ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						call_agent_coverages
					WHERE
						covered_by IN ( ' . sqlIntImplode( $arrintCallAgentIds ) . ' )
						AND date_trunc( \'day\', begin_datetime ) >= date_trunc( \'day\', \'' . $strBeginDatetime . '\'::timestamp )
						AND date_trunc( \'day\', end_datetime ) <= date_trunc( \'day\', \'' . $strEndDatetime . '\'::timestamp )
						AND approved_by IS NOT NULL
						AND approved_on IS NOT NULL';

		return self::fetchCallAgentCoverages( $strSql, $objVoipDatabase );
	}

}
?>