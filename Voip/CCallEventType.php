<?php

class CCallEventType extends CBaseCallEventType {

	/**
	 * Call Event Types.
	 */
	const STARTING								= 1;
	const GREETING								= 2;
	const ROUTE_TO_PROPERTY						= 3;
	const ROUTE_TO_VOICEMAIL					= 4;
	const ROUTE_TO_LEASING_CENTER				= 5;
	const IN_QUEUE								= 6;
	const BRIDGE_START							= 7;
	const BRIDGE_END							= 8;
	const BRIDGE_FAILED							= 9;
	const AGENT_OFFERING						= 10;
	const CALL_START							= 11;
	const CALL_STOP								= 12;

	/**
	 * Custom Events
	 */
	const CALL_TRACKER_PROPERTY							= 'CallTracker::Property';
	const CALL_TRACKER_PROPERTY_VOICEMAIL				= 'CallTracker::PropertyVoicemail';
	const CALL_TRACKER_CALL_CENTER						= 'CallTracker::CallCenter';
	const CALL_TRACKER_ROUTE_TO_CALL_CENTER				= 'CallTracker::RouteToCallCenter';
	const CALL_TRACKER_CALL_CENTER_QUEUE				= 'CallTracker::CallCenterQueue';
	const CALL_TRACKER_ATTENDED_CALL_CENTER_QUEUE		= 'CallTracker::AttendedCallCenterQueue';
	const CALL_TRACKER_CALL_CENTER_QUEUE_OVERFLOW		= 'CallTracker::CallCenterQueueOverflow';
	const CALL_TRACKER_CALL_CENTER_VOICEMAIL			= 'CallTracker::CallCenterVoicemail';
	const CALL_CENTER_VISA_CARD_NUMBER_INPUT			= 'CallCenter::VisaCardNumberInput';

	/**
	 * Vanity number.
	 */
	const CALL_TRACKER_VANITY_NUMBER						= 'CallTracker::VanityNumber';
	const CALL_TRACKER_IVR_CALL								= 'CallTracker::IvrCall';
	const CALL_TRACKER_MAIN_SUPPORT_LINE					= 'CallTracker::MainSupportLine';
	const CALL_TRACKER_EMPLOYEE_EXTENSION_CALL				= 'CallTracker::EmployeeExtensionCall';
	const CALL_TRACKER_PROPERTY_SOLUTIONS_VOICEMAIL			= 'CallTracker::PropertySolutionsVoicemail';
	const CALL_TRACKER_PROPERTY_SCHEDULED_CALL				= 'CallTracker::PropertyScheduledCall';
	const CALL_TRACKER_SCHEDULED_OUTBOUND_CALL_BLOCK		= 'CallTracker::ScheduledOutboundCallBlock';
	const CALL_TRACKER_SCHEDULED_CALL_HANGUP				= 'CallTracker::ScheduledCallHangup';
	const CALL_TRACKER_PROPERTY_ANSWER_CALL					= 'CallTracker::PropertyAnswerCall';
	const CALL_TRACKER_LEASING_CENTER_OUTBOUND				= 'CallTracker::LeasingCenterOutbound';
	const CALL_TRACKER_SUPPORT_OUTBOUND_CALL				= 'CallTracker::SupportOutboundCall';
	const CALL_TRACKER_PROPERTY_IVR_VOICEMAIL				= 'CallTracker::PropertyIvrVoicemail';
	const CALL_TRACKER_PROPERTY_IVR_CALL					= 'CallTracker::PropertyIvrCall';
	const CALL_TRACKER_PROPERTY_IVR_KEY_PRESS				= 'CallTracker::PropertyIvrKeyPress';
	const CALL_TRACKER_VACANCY_ORGANIC_PAID_FORWARD_CALLS	= 'CallTracker::VacancyOrganicPaidForwardCalls';
	const CALL_TRACKER_LEASING_CENTER_OUTBOUND_ANSWER_CALL	= 'CallTracker::LeasingCenterOutboundAnswerCall';
	const CALL_TRACKER_MOVE_IN_OUTBOUND_CALL				= 'CallTracker::MoveInOutboundCall';
	const CALL_TRACKER_CALL_RINGING                         = 'CallTracker::CallRinging';

	/**
	 * New CallCenter Event Type.
	 */
	const CALL_CENTER_INFO					= 'callcenter::info';
	const CALL_CENTER_AGENT_AUTHENTICATION	= 'CallCenter::AgentAuthentication';
	const CALL_CENTER_AGENT_LOGIN			= 'CallCenter::AgentLogin';
	const CALL_CENTER_AGENT_LOGOUT			= 'CallCenter::AgentLogout';
	const CALL_CENTER_AGENT_REMOVE			= 'CallCenter::AgentRemove';
	const CALL_CENTER_AGENT_MEETING			= 'CallCenter::AgentMeeting';
	const CALL_CENTER_AGENT_LUNCH			= 'CallCenter::AgentLunch';
	const CALL_CENTER_AGENT_PROJECT			= 'CallCenter::AgentProject';
	const CALL_CENTER_AGENT_TRAINING		= 'CallCenter::AgentTraining';
	const CALL_CENTER_AGENT_COACHING		= 'CallCenter::AgentCoaching';
	const CALL_CENTER_AGENT_EMAIL_QUEUE		= 'CallCenter::AgentEmailQueue';
	const CALL_CENTER_AGENT_LEASING_CHAT	= 'CallCenter::AgentLeasingChat';
	const CALL_CENTER_VOICEMAIL				= 'CallCenter::Voicemail';
	const CALL_CENTER_AGENT_ON_DEMAND	   	= 'CallCenter::AgentOnDemand';
	const CALL_CENTER_AGENT_RELOGIN		 	= 'CallCenter::AgentReLogin';
	const CALL_CENTER_AGENT_ON_BREAK		= 'CallCenter::AgentOnBreak';
	const CALL_CENTER_OUTBOUND_CALL         = 'CallCenter::OutboundCall';

	// Manual Followup Queue call.
	const OUTBOUND_CALL_MANUAL_FOLLOWUP     = 'OutboundCall::ManualFollowup';

	/**
	 * Call Center Event
	 */
	const CALL_CENTER_AGENT_STATUS_CHANGE	= 'agent-status-change';
	const CALL_CENTER_AGENT_STATE_CHANGE	= 'agent-state-change';
	const CALL_CENTER_AGENT_OFFERING		= 'agent-offering';
	const CALL_CENTER_BRIDGE_AGENT_START	= 'bridge-agent-start';
	const CALL_CENTER_BRIDGE_AGENT_END		= 'bridge-agent-end';
	const CALL_CENTER_BRIDGE_AGENT_FAIL		= 'bridge-agent-fail';
	const CALL_CENTER_MEMBER_QUEUE_START	= 'member-queue-start';
	const CALL_CENTER_MEMBER_QUEUE_END		= 'member-queue-end';

	/**
	 * Predefined FS System Event
	 */
	const CUSTOM							= 'CUSTOM';
	const MODULE_UNLOAD						= 'MODULE_UNLOAD';
	const MODULE_LOAD						= 'MODULE_LOAD';
	const FREESWITCH_HEARTBEAT				= 'HEARTBEAT';
	const CHANNEL_STATE						= 'CHANNEL_STATE';
	const CHANNEL_HANGUP					= 'CHANNEL_HANGUP';
	const CHANNEL_HANGUP_COMPLETE			= 'CHANNEL_HANGUP_COMPLETE';
	const CHANNEL_APPLICATION				= 'CHANNEL_APPLICATION';
	const CHANNEL_ANSWER					= 'CHANNEL_ANSWER';
	const CHANNEL_EXECUTE_COMPLETE			= 'CHANNEL_EXECUTE_COMPLETE';
	const BACKGROUND_JOB					= 'BACKGROUND_JOB';
	const DTMF								= 'DTMF';
	const CALL_CENTER_MODULE				= 'callcenter';
	const INBOUND_CALL_DIRECTION			= 'inbound';
	const OUTBOUND_CALL_DIRECTION			= 'outbound';
	const MENU_ENTER						= 'menu::enter';
	const MENU_EXIT							= 'menu::exit';

	// Twilio Call Back Event.
	// The initiated event is fired when Twilio starts dialing the call.
	const TWILIO_INITIATED                  = 'initiated';
	// The ringing event is fired when the call starts ringings.
	const TWILIO_RINGING                    = 'ringing';
	// The in-progress event is fired when call is answered and in progress.
	const TWILIO_IN_PROGRESS                = 'in-progress';
	// The answred event is fired when the call is answered.
	const TWILIO_ANSWERED                   = 'answered';
	// the completed event is fired when the call is completed, regardless of the termination status: busy, canceled, completed, failed or no-answer.
	// If no StatusCallbackEvent is specified, completed will be fired by default.
	const TWILIO_COMPLETED                  = 'completed';

}

?>