<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCallDisputes
 * Do not add any new functions to this class.
 */

class CCallAnalysisCallDisputes extends CBaseCallAnalysisCallDisputes {

	public static function fetchPaginatedCallAnalysisCallDisputes( $objPagination, $objCallAnalysisCallDisputeFilter, $objVoipDatabase, $boolIsFromQuickSearch = false, $boolCountOnly = false ) {
		$strSqlOffsetLimit  = '';
		$strSqlOrderBy    = '';
		$arrstrWhere = [];

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(cacd.id)';
		} else {
			$strSql = 'SELECT
              cacd.*,
              c.company_name AS company_name,
              ca.preferred_name AS analyzed_by_name';

			$strSqlOrderBy    = ' ORDER BY c.company_name';
			$strSqlOffsetLimit  = ( false == $boolIsFromQuickSearch ) ? ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize() : '';
		}

		$strSql .= ' FROM
            call_analysis_call_disputes AS cacd
            LEFT JOIN clients AS c ON ( c.id = cacd.cid )
            LEFT JOIN call_agents AS ca ON ( ca.id = cacd.call_agent_id )';
		if( true == valStr( $objPagination->getQuickSearch() ) ) {

			$arrstrWhere[] = '( CAST( cacd.call_id AS TEXT ) = \'' . addslashes( $objPagination->getQuickSearch() ) . '\' )';
		}

		if( true == valStr( $objCallAnalysisCallDisputeFilter->getCids() ) ) {
			$arrstrWhere[] = 'cacd.cid  IN ( ' . $objCallAnalysisCallDisputeFilter->getCids() . ' )';
		}

		if( true == valStr( $objCallAnalysisCallDisputeFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = ' cacd.property_id  IN ( ' . $objCallAnalysisCallDisputeFilter->getPropertyIds() . ' )';
		}

		if( true == is_numeric( $objCallAnalysisCallDisputeFilter->getCallId() ) ) {
			$arrstrWhere[]  = 'cacd.call_id = ' . ( int ) $objCallAnalysisCallDisputeFilter->getCallId();
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhere ) . ' ';
		}

		$strSql .= $strSqlOrderBy;
		$strSql .= $strSqlOffsetLimit;

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return parent::fetchCallAnalysisCallDisputes( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallAnalysisCallDisputeById( $intCallAnalysisCallDisputeId, $objVoipDatabase ) {
		$strSql = 'SELECT
            cacd.*,
            c.company_name AS company_name,
            ca.preferred_name AS analyzed_by_name
          FROM
            call_analysis_call_disputes AS cacd
            LEFT JOIN clients AS c ON ( c.id = cacd.cid )
            LEFT JOIN call_agents AS ca ON ( ca.id = cacd.call_agent_id )
          WHERE
            cacd.id = ' . ( int ) $intCallAnalysisCallDisputeId;

		return parent::fetchCallAnalysisCallDispute( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAnalysisCallDisputeByCallId( $intCallId, $objVoipDatabase ) {
		return self::fetchCallAnalysisCallDispute( sprintf( 'SELECT * FROM call_analysis_call_disputes WHERE call_id = %d', $intCallId ), $objVoipDatabase );
	}

}
?>