<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgents
 * Do not add any new functions to this class.
 */

class CCallAgents extends CBaseCallAgents {

	public static function fetchAllCallAgents( $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					ORDER BY
						ca.full_name
					ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallAgentsByDepartmentIdByCallQueueId( $intOffset, $intPageSize, $intDepartmentId = NULL, $intCallQueueId = NULL, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM call_agents ca
						JOIN call_agent_state_types castt ON (castt.id = ca.call_agent_state_type_id)
						JOIN call_agent_status_types casts ON (casts.id = ca.call_agent_status_type_id)
						LEFT JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
						LEFT JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
					WHERE 
						ca.agent_phone_extension_id IS NOT NULL';

		if( true == valId( $intDepartmentId ) && true == valId( $intCallQueueId ) ) {
			$strSql .= ' AND ca.department_id = ' . ( int ) $intDepartmentId . ' OR caq.call_queue_id = ' . ( int ) $intCallQueueId;
		}

		$strSql .= ' ORDER BY ca.full_name ASC';

		if( false == is_null( $intOffset ) && false == is_null( $intPageSize ) ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedActiveCallAgentsByCallAgentFilter( $objPagination, $objCallAgentFilter, $objVoipDatabase, $boolCountOnly = false ) {
		 $strSqlOffsetLimit	= '';
		$strSqlOrderBy		= '';
		$arrstrWhereSql		= array();

		if( true == $boolCountOnly ) {
			$strSql = 'SELECT COUNT(ca.id) as count FROM call_agents ca';
		} else {
			$strSql = 'SELECT
							ca.*,
							ca.id AS call_agent_id,
							castt.name AS call_agent_state_type_name,
							casts.name AS call_agent_status_type_name
						FROM
							call_agents ca
							JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
							JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )';

			$strSqlOrderBy		= ' ORDER BY ca.full_name ASC';

			if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
				$strSqlOffsetLimit	= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
			}
		}

		if( true == valStr( $objCallAgentFilter->getCallQueueIds() ) ) {
			$strSql				.= ' JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )';
			$arrstrWhereSql[]	= ' caq.call_queue_id IN (' . $objCallAgentFilter->getCallQueueIds() . ')';
		}

		if( false == is_null( $objCallAgentFilter->getCallAgentIsDisabled() ) && false == valStr( $objCallAgentFilter->getGenericData() ) ) {
			$arrstrWhereSql[] = ' ca.is_disabled = ' . ( true == $objCallAgentFilter->getCallAgentIsDisabled() ? 'TRUE' : 'FALSE' );
		}

		if( true == valStr( $objCallAgentFilter->getDepartmentIds() ) ) {
			$arrstrWhereSql[] = ' ca.department_id IN (' . $objCallAgentFilter->getDepartmentIds() . ')';
		}

		if( true == valStr( $objCallAgentFilter->getEmployeeIds() ) ) {
			$arrstrWhereSql[] = ' ca.employee_id IN (' . $objCallAgentFilter->getEmployeeIds() . ')';
		}

		if( true == valStr( $objCallAgentFilter->getTeamIds() ) ) {
			$arrstrWhereSql[] = ' ca.team_id IN(' . $objCallAgentFilter->getTeamIds() . ')';
		}

		if( false == valStr( $objCallAgentFilter->getGenericData() ) && true == valStr( $objCallAgentFilter->getOffice() ) ) {
			if( 'IN' === $objCallAgentFilter->getOffice() ) {
				$arrstrWhereSql[] = 'ca.office_id IN (' . implode( ',', COffice :: $c_arrintIndiaOfficeIds ) . ')';
			} else if( 'US' === $objCallAgentFilter->getOffice() ) {
				$arrstrWhereSql[] = 'ca.office_id IN (' . implode( ',', COffice :: $c_arrintUSOfficeIds ) . ')';
			}
		}

		if( true == valStr( $objCallAgentFilter->getGenericData() ) ) {
			$strGenericData		= $objCallAgentFilter->getGenericData();
			$arrstrWhereSql[]	= ' ( CAST( ca.agent_phone_extension_id AS text ) LIKE \'' . addslashes( $strGenericData ) . '%\'
										OR CAST( ca.physical_phone_extension_id AS text ) LIKE \'' . addslashes( $strGenericData ) . '%\'
										OR CAST( ca.desk_phone_extension_id AS text ) LIKE \'' . addslashes( $strGenericData ) . '%\'
										OR lower( ca.full_name ) LIKE lower( \'%' . addslashes( $strGenericData ) . '%\' )
										OR lower( ca.preferred_name ) LIKE lower( \'%' . addslashes( $strGenericData ) . '%\' ) ) ';
		}

		if( true == valArr( $arrstrWhereSql ) ) {
			$strSql .= ' WHERE ';
			$strSql .= implode( ' AND ', $arrstrWhereSql );
		}

		$strSql .= $strSqlOrderBy;

		if( true == valStr( $strSqlOffsetLimit ) ) {
			$strSql .= $strSqlOffsetLimit;
		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		} else {
			return self::fetchCallAgents( $strSql, $objVoipDatabase );
		}
	}

	public static function fetchCallAgentByPhysicalPhoneExtensionId( $intPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						cpn.phone_number AS phone_number
					FROM
						call_agents AS ca
					JOIN call_phone_numbers AS cpn ON ( cpn.employee_id = ca.employee_id )
					JOIN phone_extensions AS pe ON ( pe.id = cpn.phone_extension_id )
					WHERE
						pe.id = ' . ( int ) $intPhoneExtensionId . '
					LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByDeskPhoneExtensionId( $intDeskPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						cpn.phone_number AS phone_number,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents AS ca
					JOIN phone_extensions AS pe ON ( pe.id = ca.desk_phone_extension_id )
					LEFT JOIN call_phone_numbers AS cpn ON ( cpn.employee_id = ca.employee_id )
					LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
					LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						pe.id = ' . ( int ) $intDeskPhoneExtensionId . '
					LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByCallAgentId( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.id = ' . ( int ) $intCallAgentId;

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallAgentsWithNotNullAgentPhoneExtensionId( $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.agent_phone_extension_id IS NOT NULL
					ORDER BY
						ca.full_name
					ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallAgentsWithAgentExtension( $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.agent_phone_extension_id IS NOT NULL
					ORDER BY
						ca.full_name
					ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentStatsByCallStatusTypeIdsByCallAgentStatusTypeIdsByCallTypeIdsByCallQueueIdsByDepartmentIdsByEmployeeIds( $arrintCallStatusTypeIds, $arrintCallAgentStatusTypeIds, $arrintCallTypeIds, $strCallQueueIds, $strDepartmentIds, $strEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStatusTypeIds ) || false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strSql = 'SELECT
							ca.*,
							castt.name AS call_agent_state_type_name,
							casts.name AS call_agent_status_type_name,
							call_query.call_id,
							call_query.call_type_name,
							call_query.call_queue_name,
							call_query.call_queue_id,
							call_query.queue_phone_extension_id,
							call_query.origin_phone_number AS phone_number,
							EXTRACT(EPOCH FROM now()::timestamp(0)) - EXTRACT(EPOCH FROM ca.updated_on::timestamp(0)) as total_time_in_current_state_status_in_seconds
						FROM call_agents ca
							JOIN call_agent_state_types castt ON (castt.id = ca.call_agent_state_type_id)
							JOIN call_agent_status_types casts ON (casts.id = ca.call_agent_status_type_id)
							LEFT OUTER JOIN
									(
										SELECT
											*
										FROM
											(
											SELECT
												c.id AS call_id,
												c.destination_phone_number AS destination_phone_number,
												c.origin_phone_number AS origin_phone_number,
												cq.id AS call_queue_id,
												ct.name AS call_type_name,
												c.employee_id AS call_employee_id,
												cq.name AS call_queue_name,
												cq.queue_phone_extension_id
											FROM
												calls AS c
												LEFT JOIN call_types AS ct ON ( ct.id = c.call_type_id )
												LEFT JOIN call_queues AS cq ON ( cq.id = c.call_queue_id )
											WHERE
												c.call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' )
												AND c.call_status_type_id IN ( ' . implode( ',', $arrintCallStatusTypeIds ) . ' )
												AND DATE_TRUNC( \'day\', c.call_datetime) = \'' . date( 'Y-m-d' ) . '\'
											) AS sub_query
									) AS call_query ON call_query.call_employee_id = ca.employee_id
							LEFT JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
						WHERE ca.call_agent_status_type_id IN ( ' . implode( ', ', $arrintCallAgentStatusTypeIds ) . ' )';

		if( true == valStr( $strCallQueueIds ) ) {
			$strSql .= ' AND caq.call_queue_id IN ( ' . $strCallQueueIds . ' )';
		}

		if( true == valStr( $strDepartmentIds ) ) {
			$strSql .= ' AND ca.department_id IN ( ' . $strDepartmentIds . ' )';
		}

		if( true == valStr( $strEmployeeIds ) ) {
			$strSql .= ' AND ca.employee_id IN ( ' . $strEmployeeIds . ' )';
		}

		$strSql .= ' ORDER BY
					ca.call_agent_status_type_id ASC,
					ca.call_agent_state_type_id ASC,
					total_time_in_current_state_status_in_seconds DESC ';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByUsernameByPassword( $strUsername, $strPassword, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_agents WHERE username = \'' . addslashes( ( string ) $strUsername ) . '\' AND password = \'' . addslashes( ( string ) $strPassword ) . '\'';
		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_agents WHERE employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1';
		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByEmployeeIdByQueueExtension( $intEmployeeId, $intQueuePhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*
					FROM
						call_agents AS ca
					JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
					JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
					WHERE
						cq.queue_phone_extension_id = ' . ( int ) $intQueuePhoneExtensionId . '
						AND ca.employee_id = ' . ( int ) $intEmployeeId . '
					LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByAgentPhoneExtensionId( $intAgentPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents AS ca
						LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
						LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						agent_phone_extension_id = ' . ( int ) $intAgentPhoneExtensionId . ' LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByIds( $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents AS ca
					LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
					LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )
					ORDER BY ca.full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchAssociateCallAgentsByCallQueueId( $intCallQueueId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.id,
						ca.employee_id,
						ca.full_name,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						ca.domain,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name,
						cq.domain AS call_queue_domain,
						cq.queue_phone_extension_id,
						CASE
							WHEN caq.override_priority IS NOT NULL AND caq.override_expiration >= DATE_TRUNC( \'day\', NOW() )
							THEN caq.override_priority
							WHEN MIN( csq.priority ) IS NOT NULL
							THEN MIN( csq.priority )
							ELSE 5
						END AS priority
					FROM
						call_agents AS ca
					JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
					JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
					JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
					JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					LEFT JOIN call_agent_skills cas ON ( cas.call_agent_id = ca.id )
					LEFT JOIN call_skill_queues csq ON ( csq.call_queue_id = cq.id AND csq.call_skill_id = cas.call_skill_id )
					WHERE
						cq.id = ' . ( int ) $intCallQueueId . '
					GROUP BY
						ca.id,
						ca.employee_id,
						ca.full_name,
						ca.agent_phone_extension_id,
						ca.physical_phone_extension_id,
						ca.domain,
						call_agent_state_type_name,
						call_agent_status_type_name,
						call_queue_domain,
						cq.queue_phone_extension_id,
						caq.override_priority,
						caq.override_expiration,
						csq.priority';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT * FROM call_agents WHERE employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' ) ORDER BY full_name';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByCallerUsername( $intPhysicalPhoneExtensionId, $objVoipDatabase ) {
		$strSql = 'SELECT *FROM call_agents WHERE physical_phone_extension_id = ' . ( int ) $intPhysicalPhoneExtensionId . ' LIMIT 1';
		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsWithPriorityOverrideExpiration( $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						caq.call_queue_id,
						caq.call_agent_id
					FROM
						call_agents AS ca
					JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
					WHERE
						caq.override_priority IS NOT NULL
					AND caq.override_expiration IS NOT NULL
					AND caq.override_expiration <= DATE_TRUNC( \'day\', NOW() )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByIdByEmployeeId( $intCallAgentId, $intEmployeeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.id = ' . ( int ) $intCallAgentId . '
						AND ca.employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByDepartmentIds( $arrintDepartmentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						ca.department_id IN( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )
					ORDER BY
						ca.full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByCallQueueIds( $arrintCallQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallQueueIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ca.employee_id
					FROM
						call_agents AS ca
						JOIN call_agent_queues AS caq ON ( ca.id = caq.call_agent_id )
					WHERE
						caq.call_queue_id IN ( ' . implode( ',', $arrintCallQueueIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentsByIdsByCallQueueIds( $arrintCallAgentIds, $arrintCallQueueIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) || false == valArr( $arrintCallQueueIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ca.employee_id
					FROM
						call_agents AS ca
						JOIN call_agent_queues AS caq ON ( ca.id = caq.call_agent_id )
					WHERE
						caq.call_queue_id IN ( ' . implode( ',', $arrintCallQueueIds ) . ')
						AND ca.id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByPhysicalPhoneExtensionIds( $arrintPhysicalPhoneExtensions, $objVoipDatabase ) {
		if( false == valArr( $arrintPhysicalPhoneExtensions ) ) return;

		return self::fetchCallAgents( 'SELECT * FROM call_agents WHERE physical_phone_extension_id IN ( ' . implode( ', ', $arrintPhysicalPhoneExtensions ) . ' ) ', $objVoipDatabase );
	}

	public static function fetchQuickSearchCallAgents( $strSearchData, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						(
							CAST( ca.agent_phone_extension_id AS text ) LIKE \'' . addslashes( $strSearchData ) . '%\'
						OR
							CAST( ca.physical_phone_extension_id AS text ) LIKE \'' . addslashes( $strSearchData ) . '%\'
						OR
							CAST( ca.desk_phone_extension_id AS text ) LIKE \'' . addslashes( $strSearchData ) . '%\'
						OR
							lower( ca.preferred_name ) LIKE lower( \'%' . addslashes( $strSearchData ) . '%\' ) 
						OR
							lower( ca.full_name ) LIKE lower( \'%' . addslashes( $strSearchData ) . '%\' ) ) 
					ORDER BY ca.preferred_name ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByDepartmentIdByIsDisabled( $intDepartmentId, $boolIsDisabled, $objVoipDatabase, $arrintDesignationIds = [] ) {
		if( false == valId( $intDepartmentId ) ) return NULL;

		$strWhere = NULL;

		if( true == valArr( $arrintDesignationIds ) ) {
			$strWhere = ' AND designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' )
							AND team_id IS NOT NULL';
		}

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						department_id = ' . ( int ) $intDepartmentId . '
						AND is_disabled = ' . ( true == $boolIsDisabled ? 'TRUE' : 'FALSE' ) . $strWhere . '
					ORDER BY
						full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByEmployeeIdsWithoutCallAgentStatusTypeIds( $arrintEmployeeIds, $arrintCallAgentStatusTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND call_agent_status_type_id NOT IN ( ' . implode( ', ', $arrintCallAgentStatusTypeIds ) . ' )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsCountByEmployeeIdByStatusTypeIds( $intEmployeeId, $arrintStatusTypeIds, $objVoipDatabase ) {

		if( false == valArr( $arrintStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'WITH call_agent_queues_custom AS (
														SELECT
															caq.call_queue_id AS call_agent_queue_id
														FROM
															call_agents AS ca
															JOIN call_agent_queues caq ON ( caq.call_agent_id = ca.id
																							AND ca.employee_id = ' . ( int ) $intEmployeeId . ' )
													)
					SELECT
						COUNT ( DISTINCT ( ca.id ) ) AS total_agents
					FROM
						call_agents ca
						JOIN call_agent_queues caq ON ( ca.id = caq.call_agent_id )
						JOIN call_agent_queues_custom caqc ON ( caq.call_queue_id = caqc.call_agent_queue_id )
					WHERE
						ca.call_agent_status_type_id IN ( ' . implode( ',', $arrintStatusTypeIds ) . ' )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleInCallCallAgentsByCallAgentStateTypeIds( $arrintCallAgentStateTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStateTypeIds ) ) return NULL;

		$strSql = 'SELECT
						ca.*,
						c.id AS call_id
					FROM
						call_agents AS ca
						LEFT JOIN calls AS c ON (ca.employee_id = c.employee_id AND c.call_status_type_id IN ( ' . implode( ', ', CCallStatusType::$c_arrintActiveCallStatusTypeIds ) . ' ) )
					WHERE
						ca.call_agent_state_type_id IN ( ' . implode( ', ', $arrintCallAgentStateTypeIds ) . ' )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByCallAgentStateTypeIds( $arrintCallAgentStateTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStateTypeIds ) ) return NULL;

		$strSql = 'SELECT 
							* 
						FROM (
							SELECT
								c.call_datetime,
								ca.*,
								castt.name AS call_agent_state_type_name,
								casts.name AS call_agent_status_type_name,
								RANK () OVER ( PARTITION BY ca.id ORDER BY c.call_datetime DESC )
							FROM
								call_agents AS ca
								LEFT JOIN calls AS c ON (ca.employee_id = c.employee_id AND c.call_status_type_id IN ( ' . implode( ', ', CCallStatusType::$c_arrintActiveCallStatusTypeIds ) . ' ) )
								LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
								LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
							WHERE
								ca.call_agent_state_type_id IN ( ' . implode( ', ', $arrintCallAgentStateTypeIds ) . ' )
								AND c.id IS NOT NULL
							) AS sub_query
						WHERE
						rank = 1
						AND call_datetime < ( NOW() - INTERVAL \'10 minute\' )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchSortedCallAgentsByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase, $strOrderByIntervalSequence = NULL, $intIntervalStartEpoch = NULL, $intIntervalDayOfWeek = NULL, $arrintLoggedInCallAgentStatusType = NULL ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		if( 'DESC' == $strOrderByIntervalSequence || 'ASC' == $strOrderByIntervalSequence ) {
			$strSql = ' WITH filtered_call_agents AS (
						SELECT
							*
						FROM
							call_agents
						WHERE
							employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
							AND is_disabled = FALSE
					)
					SELECT
						ca.*,
						CASE
							WHEN casd.call_agent_status_type_id IN ( ' . implode( ',', $arrintLoggedInCallAgentStatusType ) . ' ) THEN 0
							ELSE 1
						END AS SEQUENCE
					FROM
						filtered_call_agents ca
						LEFT JOIN call_agent_schedules cas ON ( ca.id = cas.call_agent_id
							AND ( ( interval_start_epoch <= ' . ( int ) $intIntervalStartEpoch . ' ) OR ( interval_start_epoch >= ' . ( int ) $intIntervalStartEpoch . ' AND interval_start_epoch <  ' . ( int ) strtotime( '+7 days', $intIntervalStartEpoch ) . ' ) )
							AND ( interval_end_epoch IS NULL OR interval_end_epoch > ' . ( int ) $intIntervalStartEpoch . ' )
							AND is_active IS TRUE )
						LEFT JOIN call_agent_schedule_details casd ON ( cas.id = casd.call_agent_schedule_id AND casd.interval_day_of_week = ' . ( int ) $intIntervalDayOfWeek . ' AND casd.call_agent_status_type_id IS NOT NULL )
					ORDER BY
						SEQUENCE,
						casd.interval_sequence ' . $strOrderByIntervalSequence . ' nulls last,
						ca.full_name';
		} else {
			$strSql = 'SELECT
							ca.*
						FROM
							call_agents ca
						WHERE
							ca.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
							AND is_disabled = FALSE
						ORDER BY
							ca.full_name';
		}

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsWithDeskPhoneExtensionIdNotHavingCallPhoneNumber( $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*
					FROM
						call_agents AS ca
						LEFT JOIN call_phone_numbers AS cpn ON( ca.employee_id = cpn.employee_id )
					WHERE
						cpn.employee_id IS NULL AND
						ca.is_disabled <> TRUE AND
						ca.desk_phone_extension_id IS NOT NULL';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentInfoByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						id,call_agent_status_type_id
					FROM
						call_agents
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1';
		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentDetailsByEmployeeId( $intEmployeeId, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents AS ca
						LEFT JOIN call_agent_state_types AS castt ON ( castt.id = ca.call_agent_state_type_id )
						LEFT JOIN call_agent_status_types AS casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByUserId( $intUserId, $objVoipDatabase ) {
		return self::fetchCallAgent( 'SELECT *FROM call_agents WHERE user_id = ' . ( int ) $intUserId . ' LIMIT 1', $objVoipDatabase );
	}

	public static function fetchCallAgentByUserIdByDepartmentId( $intUserId, $intDepartmentId, $objVoipDatabase ) {
		if( false == valId( $intUserId ) || false == valId( $intDepartmentId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						user_id = ' . ( int ) $intUserId . '
						AND department_id = ' . ( int ) $intDepartmentId;

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsAvailableforChatByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND call_agent_status_type_id IN ( ' . implode( ',', array( CCallAgentStatusType::AVAILABLE, CCallAgentStatusType::IN_LEASING_CHAT, CCallAgentStatusType::AVAILABLE_ON_DEMAND ) ) . ' )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByEmployeeIdsByDepartmentIds( $arrintEmployeeIds, $arrintDepartmentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strSql .= ' AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' )';
		}

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchLeasingCenterPerformanceReportNewCallAgentStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		$strSql = 'SELECT
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::AVAILABLE . '
										AND ca.call_agent_state_type_id = ' . CCallAgentStateType::WAITING . '
									THEN ca.id
									ELSE NULL
								END
							) available_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . '
										AND ca.call_agent_state_type_id = ' . CCallAgentStateType::WAITING . '
									THEN ca.id
									ELSE NULL
								END
							) available_on_demand_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_BREAK . '
									THEN ca.id
									ELSE NULL
								END
							) on_break_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_LUNCH . '
									THEN ca.id
									ELSE NULL
								END
							) on_lunch_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_PROJECT . '
									THEN ca.id
									ELSE NULL
								END
							) on_project_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_MEETING . '
									THEN ca.id
									ELSE NULL
								END
							) on_meeting_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_TRAINING . '
									THEN ca.id
									ELSE NULL
								END
							) on_training_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::ON_COACHING . '
									THEN ca.id
									ELSE NULL
								END
							) on_coaching_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::IN_EMAIL_QUEUE . '
									THEN ca.id
									ELSE NULL
								END
							) in_email_queue_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id = ' . CCallAgentStatusType::IN_LEASING_CHAT . '
									THEN ca.id
									ELSE NULL
								END
							) in_leasing_chat_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_status_type_id <> ' . CCallAgentStatusType::LOGGED_OUT . '
									THEN ca.id
									ELSE NULL
								END
							) logged_in_agents_count,
						COUNT( DISTINCT CASE
									WHEN
										ca.call_agent_state_type_id = ' . CCallAgentStateType::IN_A_QUEUE_CALL . '
										OR (
												(	ca.call_agent_status_type_id = ' . CCallAgentStatusType::AVAILABLE . '
													OR ca.call_agent_status_type_id = ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . '
												)
												AND ca.call_agent_state_type_id = ' . CCallAgentStateType::RECEIVING . '
											)
									THEN ca.id
									ELSE NULL
								END
							) on_call_agents_count
						FROM
							call_agents AS ca';

		if( true == valStr( $objCallFilter->getCallQueueIds() ) ) {
			$strSql .= ' JOIN call_agent_queues AS caq ON ( caq.call_agent_id = ca.id )
						 JOIN call_queues AS cq ON ( cq.id = caq.call_queue_id )
						WHERE
							cq.id IN ( ' . $objCallFilter->getCallQueueIds() . ' )';
		}

		if( true == valStr( $objCallFilter->getDepartmentIds() ) ) {
			$strSql .= ( true == valStr( $objCallFilter->getCallQueueIds() ) ? ' AND ' : ' WHERE ' ) . 'ca.department_id IN ( ' . $objCallFilter->getDepartmentIds() . ' )';
		}

		if( true == valStr( $objCallFilter->getEmployeeIds() ) ) {
			$strSql .= ( true == valStr( $objCallFilter->getCallQueueIds() ) ? ' AND ' : ' WHERE ' ) . 'ca.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )';
		}

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByUserIds( $arrintUserIds, $objVoipDatabase ) {
		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )
					ORDER BY
						full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentsByIds( $arrintCallAgentIds, $objVoipDatabase ) {

		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT
						ca.id,
						ca.full_name,
						ca.preferred_name
					FROM
						call_agents AS ca
					WHERE
						ca.id IN ( ' . implode( ',', $arrintCallAgentIds ) . ')
					ORDER BY
						full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchAvailableCallAgentById( $intCallAgentId, $objVoipDatabase ) {
		$strSql = 'SELECT 
						*
					FROM 
						call_agents 
					WHERE 
						id = ' . ( int ) $intCallAgentId . ' 
						AND call_agent_status_type_id IN ( ' . CCallAgentStatusType::AVAILABLE . ', ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . '  )
						AND call_agent_state_type_id = ' . CCallAgentStateType::WAITING;

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByUsername( $strUsername, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_agents WHERE LOWER( username ) = \'' . $strUsername . '\'';
		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchAvailableCallAgentByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		$strSql = 'SELECT 
						*
					FROM 
						call_agents 
					WHERE 
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) 
						AND ( 
							(
								call_agent_status_type_id IN ( ' . CCallAgentStatusType::AVAILABLE . ', ' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' )
								AND call_agent_state_type_id = ' . CCallAgentStateType::WAITING . '
							)
							OR
							(
								call_agent_status_type_id IN ( ' . CCallAgentStatusType::IN_LEASING_CHAT . ' )
								AND call_agent_state_type_id = ' . CCallAgentStateType::IDLE . '
							)
						)';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentByIdByCallAgentStateTypeIdByCallAgentStatusTypeIds( $intId, $intCallAgentStateTypeId, $arrintCallAgentStatusTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStatusTypeIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						id = ' . ( int ) $intId . '
						AND call_agent_state_type_id = ' . ( int ) $intCallAgentStateTypeId . '
						AND call_agent_state_type_id IN ( ' . implode( ', ', $arrintCallAgentStatusTypeIds ) . ' )';

		return self::fetchCallAgent( $strSql, $objVoipDatabase );
	}

	public static function fetchConflictLoggedInCallAgentCountByPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId, $objVoipDatabase ) {
		$strWhere = ' WHERE
						physical_phone_extension_id = ' . ( int ) $intPhysicalPhoneExtensionId . '
						AND call_agent_status_type_id IN ( ' . CCallAgentStatusType::AVAILABLE . ',' . CCallAgentStatusType::AVAILABLE_ON_DEMAND . ' )';

		return self::fetchCallAgentCount( $strWhere, $objVoipDatabase );
	}

	public static function fetchCallAgentsByOfficeIds( $arrintOfficeIds, $objVoipDatabase ) {
		$strSql = 'SELECT
						ca.*,
						ca.id AS call_agent_id,
						castt.name AS call_agent_state_type_name,
						casts.name AS call_agent_status_type_name
					FROM
						call_agents ca
						JOIN call_agent_state_types castt ON ( castt.id = ca.call_agent_state_type_id )
						JOIN call_agent_status_types casts ON ( casts.id = ca.call_agent_status_type_id )
					WHERE
						office_id IN ( ' . implode( ',', $arrintOfficeIds ) . ' )
					ORDER BY
						ca.full_name ASC';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchTerminatedCallAgentsByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						( physical_phone_extension_id IS NOT NULL OR agent_phone_extension_id IS NOT NULL )
						AND employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByTeamIds( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						team_id IN ( ' . sqlIntImplode( $arrintTeamIds ) . ' )
						AND date_terminated IS NULL
						AND is_disabled = FALSE';

		return self::fetchCallAgents( $strSql, $objDatabase );
	}

	public static function fetchGamificationMonthlyLeaderboardCallAgents( $objVoipDatabase, $intTeamId = NULL, $intLimitNumberOfCallAgents = NULL, $intCallAgentDesignationId = NULL ) {
		$strWhereClause		= NULL;
		$strRestrictClause	= NULL;
		$strOrderByClause	= NULL;

		if( ( true == valId( $intTeamId ) ) && ( false == in_array( $intCallAgentDesignationId, CDesignation::$c_arrintLeasingCenterShiftManagerDesignationIds ) ) ) {
			$strWhereClause	= ' AND ca.team_id = ' . ( int ) $intTeamId;
		}

		if( true == in_array( $intCallAgentDesignationId, CDesignation::$c_arrintLeasingCenterShiftManagerDesignationIds ) ) {
			$strOrderByClause	= ' , team_leaderboard_rank ASC';
		} else {
			$strOrderByClause	= ' , monthly_leaderboard_rank ASC';
		}

		if( true == valId( $intLimitNumberOfCallAgents ) ) {
			$strRestrictClause	= ' LIMIT ' . ( int ) $intLimitNumberOfCallAgents;
		}

		$strSql = 'SELECT
						ca.*,
						SUM ( carpl.earned_number_of_points ) AS total_earned_number_of_points,
						ROW_NUMBER() OVER (ORDER BY SUM(carpl.earned_number_of_points) DESC ) AS monthly_leaderboard_rank,
						CASE 
							WHEN ca.team_id = ' . ( int ) $intTeamId . '
							THEN ROW_NUMBER() OVER (PARTITION BY ca.team_id ORDER BY SUM(carpl.earned_number_of_points) DESC ) 
						END AS team_leaderboard_rank
					FROM 
						call_agents AS ca
						JOIN call_agent_reward_point_logs AS carpl ON ( ca.id = carpl.call_agent_id )
						AND carpl.added_date BETWEEN \'' . date( 'Y-m-01' ) . '\' AND NOW()
					WHERE
						ca.department_id = ' . CDepartment::CALL_CENTER . '
						AND carpl.earned_number_of_points > 0
						AND ca.is_disabled = false AND ca.date_terminated IS NULL
						' . $strWhereClause . '
					GROUP BY 
						ca.id
					ORDER BY
						total_earned_number_of_points DESC' . $strOrderByClause . $strRestrictClause;

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchTeamWiseGamificationLeaderboardCallAgents( $objVoipDatabase, $intLimitNumberOfCallAgents = 3 ) {
		$strSql = 'SELECT
						*
					FROM (
						SELECT 
							ca.id,
							ca.team_id,
							ca.employee_id,
							ca.full_name,
							SUM ( carpl.earned_number_of_points ) AS total_earned_number_of_points,
							row_number() OVER( PARTITION BY ca.team_id ORDER BY SUM(carpl.earned_number_of_points) DESC ) AS rn
						FROM
							call_agents AS ca
							JOIN call_agent_reward_point_logs AS carpl ON ( carpl.call_agent_id = ca.id )
							AND carpl.added_date BETWEEN \'' . date( 'Y-m-01' ) . '\' AND NOW()
						GROUP BY 
							ca.id
						ORDER BY 
							ca.team_id ASC
						) AS sub_query
					WHERE 
						sub_query.rn <= ' . ( int ) $intLimitNumberOfCallAgents;

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByDepartmentIdByDesignationIdsByDateStarted( $intDepartmentId, $arrintDesignationIds, $strDateStarted, $objVoipDatabase ) {
		if( false == valId( $intDepartmentId ) || false == valArr( $arrintDesignationIds ) || false == valStr( $strDateStarted ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						call_agents
					WHERE
						department_id = ' . ( int ) $intDepartmentId . ' 
						AND designation_id IN ( ' . sqlIntImplode( $arrintDesignationIds ) . ' )
						AND date_started = \' ' . $strDateStarted . ' \'
						AND date_terminated IS NULL OR date_terminated > NOW()';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentsByUsernames( $arrstrUsernames, $objVoipDatabase ) {

		if( false == valArr( $arrstrUsernames ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agents 
					WHERE 
						LOWER( username ) IN ( \'' . implode( '\',\'', $arrstrUsernames ) . '\' )';

		return self::fetchCallAgents( $strSql, $objVoipDatabase );
	}

}
?>