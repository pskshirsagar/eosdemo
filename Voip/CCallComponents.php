<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallComponents
 * Do not add any new functions to this class.
 */

class CCallComponents extends CBaseCallComponents {

	public static function fetchCallComponent( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallComponent', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>