<?php

class CIvrOfficeHour extends CBaseIvrOfficeHour {

	public static $c_arrstrWeekDays = array(
		'monday'		=> 1,
		'tuesday'		=> 2,
		'wednesday'		=> 3,
		'thursday'		=> 4,
		'friday'		=> 5,
		'saturday'		=> 6,
		'sunday'		=> 7
	);

	public function getWeekDayName() {
		return array_search( $this->getWeekDay(), self::$c_arrstrWeekDays );
	}

	public function valOfficeTiming() {
		$boolIsValid = true;

		if( strtotime( $this->getOpenTime() ) >= strtotime( $this->getCloseTime() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Opening office time can not be greater than closing office time on ' . \Psi\CStringService::singleton()->ucfirst( $this->getWeekDayName() ) . '.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOfficeTiming();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getFormatOfficeHours() {
		$strFormatOfficeHours = NULL;

		if( false == valStr( $this->getOpenTime() ) && false == valStr( $this->getCloseTime() ) ) {
			return;
		}

		$strOpenTime = strtotime( \Psi\CStringService::singleton()->substr( $this->getOpenTime(), 0, 5 ) );
		$strCloseTime = strtotime( \Psi\CStringService::singleton()->substr( $this->getCloseTime(), 0, 5 ) );

		$strOpenTimeHour = date( 'H', $strOpenTime );
		$strCloseTimeHour = date( 'H', $strCloseTime );

		$strOpenTimeMins = date( 'i', $strOpenTime );
		$strCloseTimeMins = date( 'i', $strCloseTime );

		$strFormatOfficeHours .= ( $strOpenTimeHour !== $strCloseTimeHour ) ? ( $strOpenTimeHour . '[' . ( int ) ( $strOpenTimeMins / 10 ) . '-5][' . ( int ) ( $strOpenTimeMins % 10 ) . '-9]|' ) : '';

		$strTempOpenTimeHours = $strOpenTime;

		for( $intCnt = $strOpenTimeHour;$intCnt < ( $strCloseTimeHour - 1 );$intCnt++ ) {
			$strTempOpenTimeHours = strtotime( '+ 1 hour', $strTempOpenTimeHours );
			$strFormatOfficeHours .= date( 'H', $strTempOpenTimeHours ) . '[0-5][0-9]|';
		}

		if( 0 !== ( int ) ( $strCloseTimeMins % 11 ) && 0 !== ( int ) ( $strCloseTimeMins % 10 ) ) {
			$strFormatOfficeHours .= $strCloseTimeHour . '[0-' . ( int ) ( $strCloseTimeMins / 10 ) . '][0-' . ( int ) ( $strCloseTimeMins % 10 ) . ']';
			return $strFormatOfficeHours;
		}

		for( $intCnt = 0; $intCnt < $strCloseTimeMins; $intCnt++ ) {
			$strFormatOfficeHours .= $strCloseTimeHour . sprintf( '%\'.02d|', $intCnt );
		}

		$strFormatOfficeHours .= $strCloseTimeHour . $strCloseTimeMins;
		return $strFormatOfficeHours;
	}

}
?>