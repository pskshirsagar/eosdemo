<?php

class CInboundCallResponseType extends CBaseInboundCallResponseType {

	const UNAUTHORIZED_REQUEST				= 1;
	const PAYMENT_ACTIVATION_ID_EMPTY		= 2;
	const INVALID_PAYMENT_ACTIVATION_ID		= 3;
	const PAYMENT_NOT_EXISTS				= 4;
	const INVALID_PAYMENT_STATUS_TYPE		= 5;
	const SYSTEM_FAILED						= 6;
	const MANUAL_CAPTURING_REQUIRED			= 7;
	const AUTHORIZE_PAYMENT_ONLY			= 8;
	const PAYMENT_PROCESS_SUCCESSFULLY		= 9;
}
?>