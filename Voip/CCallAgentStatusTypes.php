<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentStatusTypes
 * Do not add any new functions to this class.
 */

class CCallAgentStatusTypes extends CBaseCallAgentStatusTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchCallAgentStatusTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallAgentStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallAgentStatusType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallAgentStatusType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveCallAgentStatusTypes( $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_agent_status_types
					WHERE id IN ( ' . implode( ',', array_keys( CCallAgentStatusType::$c_arrstrCallAgentStatusTypes ) ) . ' )
					ORDER BY name';

		return parent::fetchCallAgentStatusTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentStatusTypesByIds( $arrintCallAgentStatusType, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentStatusType ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_agent_status_types
					WHERE id IN ( ' . implode( ',', $arrintCallAgentStatusType ) . ' )';

		return self::fetchCallAgentStatusTypes( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentStatusTypes( $objVoipDatabase ) {
		$arrstrResponses = fetchData( 'SELECT id, name FROM call_agent_status_types', $objVoipDatabase );

		if( false == valArr( $arrstrResponses ) ) return;

		foreach( $arrstrResponses as $arrmixResponses ) {
			$arrstrCallAgentStatusTypes[$arrmixResponses['id']] = $arrmixResponses['name'];
		}

		return $arrstrCallAgentStatusTypes;
	}

}
?>