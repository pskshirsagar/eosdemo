<?php

class CCallAgentRestoreRequest extends CBaseCallAgentRestoreRequest {

	protected $m_strCallAgentName;
	protected $m_strOriginCallAgentName;
	protected $m_strDestinationCallAgentName;
	protected $m_strRequestedCallAgentName;

	/**
	 * Setter Getter functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_agent_name'] ) ) $this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['origin_call_agent_name'] ) ) $this->setOriginCallAgentName( $arrmixValues['origin_call_agent_name'] );
		if( true == isset( $arrmixValues['destination_call_agent_name'] ) ) $this->setDestinationCallAgentName( $arrmixValues['destination_call_agent_name'] );
		if( true == isset( $arrmixValues['requested_call_agent_name'] ) ) $this->setRequestedCallAgentName( $arrmixValues['requested_call_agent_name'] );
		return;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function setOriginCallAgentName( $strOriginCallAgentName ) {
		$this->m_strOriginCallAgentName = $strOriginCallAgentName;
	}

	public function getOriginCallAgentName() {
		return $this->m_strOriginCallAgentName;
	}

	public function setDestinationCallAgentName( $strDestinationCallAgentName ) {
		$this->m_strDestinationCallAgentName = $strDestinationCallAgentName;
	}

	public function getDestinationCallAgentName() {
		return $this->m_strDestinationCallAgentName;
	}

	public function setRequestedCallAgentName( $strRequestedCallAgentName ) {
		$this->m_strRequestedCallAgentName = $strRequestedCallAgentName;
	}

	public function getRequestedCallAgentName() {
		return $this->m_strRequestedCallAgentName;
	}

	/**
	 * Validate functions
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>