<?php

class CStoreProductRank extends CBaseStoreProductRank {

	protected $m_intFrequencyId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strImagePath;
	protected $m_strRequestFormUrl;
	protected $m_strImageSource;
	protected $m_fltCostAmount;
	protected $m_boolIsAllowCallAgentRedeem;

	/**
	* Setter Getter Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['frequency_id'] ) )		$this->setFrequencyId( $arrmixValues['frequency_id'] );
		if( isset( $arrmixValues['name'] ) )				$this->setName( $arrmixValues['name'] );
		if( isset( $arrmixValues['description'] ) )			$this->setDescription( $arrmixValues['description'] );
		if( isset( $arrmixValues['image_path'] ) )			$this->setImagePath( $arrmixValues['image_path'] );
		if( isset( $arrmixValues['request_form_url'] ) )	$this->setRequestFormUrl( $arrmixValues['request_form_url'] );
		if( isset( $arrmixValues['cost_amount'] ) )			$this->setCostAmount( $arrmixValues['cost_amount'] );
		if( isset( $arrmixValues['is_published'] ) )		$this->setIsPublished( $arrmixValues['is_published'] );
		return true;
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->m_intFrequencyId = $intFrequencyId;
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function getName() {
		return $this->m_strName;
	}

	public function setDescription( $strDescription ) {
		$this->m_strDescription = $strDescription;
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function setImagePath( $strImagePath ) {
		$this->m_strImagePath = $strImagePath;
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function setRequestFormUrl( $strRequestFormUrl ) {
		$this->m_strRequestFormUrl = $strRequestFormUrl;
	}

	public function getRequestFormUrl() {
		return $this->m_strRequestFormUrl;
	}

	public function setImageSource( $strImageSource ) {
		$this->m_strImageSource = $strImageSource;
	}

	public function getImageSource() {
		return $this->m_strImageSource;
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->m_fltCostAmount = $fltCostAmount;
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function setIsAllowCallAgentRedeem( $boolIsAllowCallAgentRedeem ) {
		$this->m_boolIsAllowCallAgentRedeem = $boolIsAllowCallAgentRedeem;
	}

	public function getIsAllowCallAgentRedeem() {
		return $this->m_boolIsAllowCallAgentRedeem;
	}

	/**
	* Validate Functions
	*/

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStoreProductId() {
		$boolIsValid = true;

		if( false == valId( $this->getStoreProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'store_product_id', 'Perk is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRankPointMappingId() {
		$boolIsValid = true;

		if( false == valId( $this->getRankPointMappingId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rank_point_mapping_id', 'Rank is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_perk_insert':
				$boolIsValid &= $this->valStoreProductId();
				$boolIsValid &= $this->valRankPointMappingId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>