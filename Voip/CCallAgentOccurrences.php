<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAgentOccurrences
 * Do not add any new functions to this class.
 */

class CCallAgentOccurrences extends CBaseCallAgentOccurrences {

	public static function fetchSimpleCallAgentOccurrencesStatsByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strOrderBySql = '';

		if( true == valStr( $objCallFilter->getSortBy() ) && true == valStr( $objCallFilter->getSortDirection() ) ) {
			$strOrderBySql = ' ORDER BY ' . $objCallFilter->getSortBy() . ' ' . $objCallFilter->getSortDirection();
		}

		$strSql = 'SELECT
						ca.employee_id,
						ca.full_name AS call_agent_name,
						CASE
							WHEN ca.preferred_name IS NOT NULL THEN ca.preferred_name
							ELSE CONCAT( ca.name_first, \' \', ca.name_last )
						END AS preferred_name,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::CREDIT . ' THEN cao.occurrence_amount END ), 0 ) AS credit_occurrences,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::REPORTED_TARDY . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS reported_tardy,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::UNREPORTED_TARDY . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS unreported_tardy,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::APPROVED_EARLY_LEAVE . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS approved_early_leave,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::UNAPPROVED_EARLY_LEAVE . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS unapproved_early_leave,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::REPORTED_ABSENCE . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS reported_absence,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id = ' . ( int ) COccurrenceType::UNREPORTED_ABSENCE . ' AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS unreported_absence,
						COALESCE( SUM( CASE WHEN cao.occurrence_type_id IN ( ' . $objCallFilter->getOccurrenceTypeIds() . ' ) AND cao.is_dispute_approved = FALSE THEN cao.occurrence_amount END ), 0 ) AS total_occurrences
					FROM
						call_agent_occurrences AS cao
						JOIN call_agents AS ca ON ( cao.call_agent_id = ca.id )
					WHERE
						ca.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
						AND cao.created_on BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'
					GROUP BY
						ca.employee_id,
						ca.full_name,
						CONCAT( ca.name_first, \' \', ca.name_last ),
						ca.preferred_name' . $strOrderBySql;

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentOccurrencesByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = ' WITH filtered_call_agent_occurrences AS (
						SELECT
							ca.employee_id AS employee_id,
							ca.full_name AS call_agent_name,
							CASE
								WHEN ca.preferred_name IS NOT NULL THEN ca.preferred_name
								ELSE CONCAT( ca.name_first, \' \', ca.name_last )
							END AS preferred_name,
							COALESCE( SUM( CASE WHEN
												cao.occurrence_type_id <> ' . ( int ) COccurrenceType::CREDIT . '
												AND ( cao.is_dispute_approved <> TRUE )
											THEN
												cao.occurrence_amount
										END ), 0 ) AS occurrence_amount,
							COALESCE( SUM( CASE WHEN
												occurrence_type_id = ' . ( int ) COccurrenceType::CREDIT . '
											THEN
												cao.occurrence_amount
										END ), 0 ) AS credit_amount
						FROM
							call_agent_occurrences AS cao
							JOIN call_agents AS ca ON ( cao.call_agent_id = ca.id )
						WHERE
							ca.employee_id IN ( ' . $objCallFilter->getEmployeeIds() . ' )
						GROUP BY
							ca.employee_id,
							ca.full_name,
							CONCAT( ca.name_first, \' \', ca.name_last ),
							ca.preferred_name )
					SELECT
						employee_id,
						call_agent_name,
						preferred_name,
						( occurrence_amount - credit_amount ) AS net_amount
					FROM
						filtered_call_agent_occurrences
					GROUP BY
						employee_id,
						call_agent_name,
						preferred_name,
						occurrence_amount,
						credit_amount';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleAllCallAgentCreditStatsByCallAgentIds( $arrintCallAgentIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'WITH filtered_call_agent_occurrences AS (
						SELECT
						ca.id,
						ca.full_name AS call_agent_name,
						COALESCE( SUM( CASE WHEN
											cao.occurrence_type_id <> ' . COccurrenceType::CREDIT . '
											AND ( cao.is_dispute_approved <> TRUE )
											THEN
												cao.occurrence_amount
									END ), 0 ) AS occurrence_amount,
						COALESCE( SUM( CASE WHEN
												occurrence_type_id = ' . COccurrenceType::CREDIT . '
											THEN
												cao.occurrence_amount
									END ), 0 ) AS credit_amount
						FROM
							call_agents AS ca
							LEFT JOIN call_agent_occurrences AS cao ON ( cao.call_agent_id = ca.id )
						WHERE
							ca.is_disabled = FALSE
							AND ca.department_id = ' . ( int ) CDepartment::CALL_CENTER . '
							AND ca.id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )
						GROUP BY
							ca.id,
							call_agent_name)
					SELECT
						id,
						call_agent_name,
						( occurrence_amount - credit_amount ) AS net_amount
					FROM
						filtered_call_agent_occurrences
					WHERE
						0 > ( occurrence_amount - credit_amount )
						AND 10 > abs( occurrence_amount - credit_amount )
				UNION
					SELECT
						id,
						call_agent_name,
						( occurrence_amount - credit_amount ) AS net_amount
					FROM
						filtered_call_agent_occurrences
					WHERE
						0 <= ( occurrence_amount - credit_amount )';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallAgentOccurrencesWeeklyPerfectAttendanceByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = 'SELECT
					sub_query.call_agent_id,
					sub_query.employee_id,
					COALESCE(CASE
						WHEN ( sub_query.total_week - COUNT( sub_query.call_agent_id ) ) > 0 THEN ( sub_query.total_week - COUNT( sub_query.call_agent_id ) ) ELSE 0
					END ) AS perfect_attendance
				FROM
					(
						SELECT
							cgo.call_agent_id,
							cg.employee_id,
							count ( cgo.id ),
							TO_CHAR ( CAST ( cgo.occurrence_date AS DATE ), \'WW\' ) AS occurrence_week,
							TRUNC(DATE_PART(\'day\', \'' . $objCallFilter->getEndDate() . '\'::timestamp -  \'' . $objCallFilter->getStartDate() . '\'::timestamp )/7)::int AS total_week
						FROM
							call_agent_occurrences AS cgo
							LEFT JOIN call_agents AS cg ON ( cg.id = cgo.call_agent_id ) 
						WHERE
							cgo.occurrence_date BETWEEN \'' . $objCallFilter->getStartDate() . '\'::timestamp
							AND \'' . $objCallFilter->getEndDate() . '\'::timestamp
							AND cgo.call_agent_id IN ( ' . sqlIntImplode( $objCallFilter->getCallAgentIds() ) . ' )
							AND cgo.occurrence_type_id <> ' . COccurrenceType::CREDIT . '
						GROUP BY
							cgo.call_agent_id,
							cg.employee_id,
							occurrence_week
					) AS sub_query
				GROUP BY
					sub_query.call_agent_id, sub_query.employee_id, sub_query.total_week';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallAgentOccurrencesPerfectAttendanceByCallFilter( $objCallFilter, $objVoipDatabase, $arrstrDateIntervals = [] ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strWhere		= NULL;
		$strWhereSql	= NULL;

		if( true == valArr( $arrstrDateIntervals ) ) {
			foreach( $arrstrDateIntervals as $arrstrDateInterval ) {
				$strWhere .= ' ( occurrence_date BETWEEN \'' . $arrstrDateInterval['start_time'] . '\' AND \'' . $arrstrDateInterval['end_time'] . '\' ) OR';
			}

			$strWhereSql .= ' ( ' . rtrim( $strWhere, 'OR' ) . ' )';
		} else {
			$strWhereSql .= ' occurrence_date BETWEEN \'' . $objCallFilter->getStartDate() . ' 00:00:00\' AND \'' . $objCallFilter->getEndDate() . ' 23:59:59\'';
		}

		$strSql = ' SELECT
						*
					FROM
						call_agent_occurrences
					WHERE
						call_agent_id IN ( ' . sqlIntImplode( $objCallFilter->getCallAgentIds() ) . ' )
						AND occurrence_type_id <> ' . COccurrenceType::CREDIT . '
						AND is_dispute_approved <> TRUE
						AND ' . $strWhereSql;

		return self::fetchCallAgentOccurrences( $strSql, $objVoipDatabase );
	}

	public static function fetchPerfectAttendanceCallAgentOccurrencesByCallFilter( $objCallFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallFilter, 'CCallFilter' ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						call_agent_id,
						SUM ( occurrence_amount ) AS occurrence_amount
					FROM
						call_agent_occurrences
					WHERE
						call_agent_id IN ( ' . sqlIntImplode( $objCallFilter->getCallAgentIds() ) . ' )
						AND occurrence_type_id <> ' . COccurrenceType::CREDIT . '
						AND is_dispute_approved <> TRUE
						AND occurrence_date BETWEEN \'' . $objCallFilter->getStartDate() . '\' AND \'' . $objCallFilter->getEndDate() . '\'
					GROUP BY call_agent_id';

		return self::fetchCallAgentOccurrences( $strSql, $objVoipDatabase );
	}

}
?>