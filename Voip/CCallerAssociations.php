<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallerAssociations
 * Do not add any new functions to this class.
 */

class CCallerAssociations extends CBaseCallerAssociations {

	public static function fetchCallerAssociationsByCallerPhoneNumber( $strPhoneNumber, $objDatabase ) {
		$strSql = 'SELECT ca.* FROM caller_associations ca
						JOIN caller_phone_numbers cpn ON ca.caller_phone_number_id = cpn.id
					WHERE 
						cpn.caller_phone_number = \'' . $strPhoneNumber . '\'';
		return self::fetchCallerAssociations( $strSql, $objDatabase );
	}

	public static function fetchCallerAssociationByCallerPhoneNumberByNameByCidByPropertyId( $strCallerPhoneNumber, $strFullName, $intCid, $intPropertyId, $objDatabase ) {
		$strSql = 'SELECT 
						ca.* 
					FROM 
						caller_associations ca
						JOIN caller_phone_numbers cpn ON ca.caller_phone_number_id = cpn.id
						JOIN callers c ON c.id = ca.caller_id
					WHERE
						cpn.caller_phone_number = \'' . $strCallerPhoneNumber . '\'
						AND first_name || last_name = \'' . addslashes( $strFullName ) . '\'';

		if( true == valId( $intCid ) ) {
			$strSql .= 'AND c.cid = ' . ( int ) $intCid;
		}

		if( true == valId( $intPropertyId ) ) {
			$strSql .= 'AND c.property_id = ' . ( int ) $intPropertyId;
		}

		$strSql .= 'LIMIT 1';

		return self::fetchCallerAssociation( $strSql, $objDatabase );
	}

	public static function fetchSimpleCallerAssociationByCallerPhoneNumber( $strPhoneNumber, $objDatabase ) {
		$strSql = 'SELECT
						c.company_name,
						CASE
							WHEN COUNT(pcs.property_name) > 1 THEN \'Multiple\'
							WHEN COUNT(pcs.property_name) = 1 THEN MAX( pcs.property_name )
							WHEN COUNT(pcs.property_name) = 0 THEN \'ALL\'
						END AS property_name
					FROM caller_associations ca
						JOIN caller_phone_numbers cpn ON ca.caller_phone_number_id = cpn.id
						LEFT JOIN clients c ON c.id = ca.cid
						LEFT JOIN property_call_settings pcs ON pcs.property_id = ca.property_id AND pcs.cid = ca.cid
					WHERE cpn.caller_phone_number = \'' . $strPhoneNumber . '\'
					GROUP BY c.company_name
					LIMIT 1';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCallerAssociationByCallerPhoneNumber( $strPhoneNumber, $objDatabase ) {
		$strSql = 'SELECT ca.* FROM caller_associations ca
						JOIN caller_phone_numbers cpn ON ca.caller_phone_number_id = cpn.id
					WHERE 
						cpn.caller_phone_number = \'' . $strPhoneNumber . '\'
					ORDER BY ca.id DESC
					LIMIT 1';
		return self::fetchCallerAssociation( $strSql, $objDatabase );
	}

}
?>