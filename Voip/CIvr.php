<?php

class CIvr extends CBaseIvr {

	protected $m_arrobjIvrMenus;
	protected $m_arrobjIvrMenuActions;
	protected $m_arrobjIvrMenuTypes;
	protected $m_arrobjIvrActions;
	protected $m_arrobjIvrOfficeHours;

	protected $m_objIvrMenu;
	protected $m_objIvrMenuAction;

	protected $m_strPropertyIds;
	protected $m_intPropertyId;

	/**
	* Construct Function.
	*/

	/**
	 * Set values
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['property_ids'] ) )	$this->setPropertyIds( $arrmixValues['property_ids'] );
		if( true == isset( $arrmixValues['property_id'] ) )		$this->setPropertyId( $arrmixValues['property_id'] );

		return;
	}

	/**
	 * Setter Functions.
	 */

	public function setIvrMenus( $arrobjIvrMenus ) {
		$this->m_arrobjIvrMenus = $arrobjIvrMenus;
	}

	public function setIvrMenuActions( $arrobjIvrMenuActions ) {
		$this->m_arrobjIvrMenuActions = $arrobjIvrMenuActions;
	}

	public function setIvrOfficeHours( $arrobjIvrOfficeHours ) {
		$this->m_arrobjIvrOfficeHours = $arrobjIvrOfficeHours;
	}

	public function setPropertyId( $intPropertyId ) {
		$this->m_intPropertyId = $intPropertyId;
	}

	/**
	 * Get Functions
	 */

	public function getIvrMenus() {
		return $this->m_arrobjIvrMenus;
	}

	public function getIvrMenuActions() {
		return $this->m_arrobjIvrMenuActions;
	}

	public function getIvrOfficeHours() {
		return $this->m_arrobjIvrOfficeHours;
	}

	public function getPropertyIds() {
		return $this->m_strPropertyIds;
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function getPropertyIdsArray() {
		if( true == valStr( $this->m_strPropertyIds ) ) {
			return array_filter( ( array ) explode( ',', $this->m_strPropertyIds ) );
		} else {
			return array();
		}
	}

	/**
	 * Set Functions
	 */

	public function setPropertyIds( $strPropertyIds ) {
		if( true == valStr( $strPropertyIds ) ) {
			$this->m_strPropertyIds = $strPropertyIds;
		} elseif( true == valArr( $strPropertyIds ) ) {
			$this->m_strPropertyIds = implode( ',', array_filter( $strPropertyIds ) );
		} else {
			$this->m_strPropertyIds = NULL;
		}
	}

	/**
	 * Adder Function.
	 */

	public function addIvrMenu( $objIvrMenu ) {
		if( false == valOb( $objIvrMenu, 'CIvrMenu' ) ) return NULL;
		$this->m_arrobjIvrMenus[$objIvrMenu->getId()] = $objIvrMenu;
	}

	public function addIvrOfficeHour( $objIvrOfficeHour ) {
		$this->m_arrobjIvrOfficeHours[$objIvrOfficeHour->getWeekDay()] = $objIvrOfficeHour;
	}

	public function addIvrMenuActions( $objIvrMenuAction ) {
		if( false == valObj( $objIvrMenuAction, 'CIvrMenuAction' ) ) return NULL;
		$this->m_arrobjIvrMenuActions[$objIvrMenuAction->getId()] = $objIvrMenuAction;
	}

	/*
	 *  Validate FUnction.
	 */

	public function valCid() {
		$boolIsValid = true;

		if( false == valId( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'Client id does not appear to be valid.' ) ) );
		}

		return $boolIsValid;
	}

	public function valName( $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Call menu title is required.' ) ) );
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '/^[a-zA-Z0-9\s]+$/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Call menu title should not contain any special characters.' ) ) );
		} elseif( true == $this->validateIsNameDuplicate( $objVoipDatabase ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', __( 'Call menu with this title already exists.' ) ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valName( $objVoipDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/*
	 * Create Functions.
	 */

	public function createPropertyIvr() {
		$objPropertyIvr = new CPropertyIvr();
		$objPropertyIvr->setCid( $this->getCid() );
		$objPropertyIvr->setIvrId( $this->getId() );
		return $objPropertyIvr;
	}

	public function createIvrMenu() {
		$objIvrMenu = new CIvrMenu();
		$objIvrMenu->setCid( $this->getCid() );
		$objIvrMenu->setIvrId( $this->getId() );
		return $objIvrMenu;
	}

	public function createIvrLibrary() {
		$objIvrLibrary = new CIvrLibrary();
		$objIvrLibrary->setIvr( $this );
		return $objIvrLibrary;
	}

	/**
	 * Fetch Queries.
	 */

	public function fetchIvrMenusByIvrMenuTypeId( $intIvrMenuTypeId, $objVoipDatabase ) {
		if( true == $this->getIsOverrideAfterOfficeHours() ) {
			$intIvrMenuTypeId = CIvrMenuType::DURING_OFFICE_HOURS;
		}

		return CIvrMenus::fetchIvrMenus( 'SELECT * FROM ivr_menus WHERE ivr_id = ' . ( int ) $this->getId() . ' AND ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . ' ORDER BY id ASC', $objVoipDatabase );
	}

	public function fetchIvrMenuActionsByIvrMenuTypeId( $intIvrMenuTypeId, $objVoipDatabase ) {
		if( true == $this->getIsOverrideAfterOfficeHours() ) {
			$intIvrMenuTypeId = CIvrMenuType::DURING_OFFICE_HOURS;
		}

		$strSql = 'SELECT
						ima.*,
						ia.handler AS ivr_menu_action_handler,
						ia.params AS ivr_menu_action_params,
						ims.name AS ivr_sub_menu_name
					FROM
						ivr_menu_actions AS ima
						JOIN ivr_actions AS ia ON ( ia.id = ima.ivr_action_id )
						LEFT JOIN ivr_menus AS ims ON ( ims.id = ima.sub_ivr_menu_id )
					WHERE
						ima.cid = ' . ( int ) $this->getCid() . '
						AND ima.ivr_menu_id
									IN
										(
											SELECT
												im.id
											FROM
												ivr_menus AS im
											WHERE
												im.ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . '
											AND im.ivr_id = ' . ( int ) $this->getId() . '
										)
					ORDER BY action_key ASC';

		return CIvrMenuActions::fetchIvrMenuActions( $strSql, $objVoipDatabase );
	}

	public function validateIsNameDuplicate( $objVoipDatabase ) {
		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) return false;

		$strWhere = ' WHERE cid = ' . ( int ) $this->getCid() . ' AND LOWER( name ) = LOWER( ' . $this->sqlName() . ' )';

		if( true == valId( $this->getId() ) ) $strWhere .= ' AND id <> ' . ( int ) $this->getId();

		if( true == valId( CIvrs::fetchIvrCount( $strWhere, $objVoipDatabase ) ) ) {
			return true;
		}

		return false;
	}

}
?>