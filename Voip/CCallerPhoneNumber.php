<?php

class CCallerPhoneNumber extends CBaseCallerPhoneNumber {

	public function valCallerPhoneNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getCallerPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is blank.', NULL ) );
		} elseif( false == CValidation::checkPhoneNumberFullValidation( $this->getCallerPhoneNumber(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Phone number is invalid.', NULL ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCallerPhoneNumber();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createCallerAssociation() {
		$objCallerAssociation = new CCallerAssociation();
		$objCallerAssociation->setCallerPhoneNumberId( $this->getId() );
		return $objCallerAssociation;
	}

}
?>