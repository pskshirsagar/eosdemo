<?php

class CPhoneExtension extends CBasePhoneExtension {

	protected $m_strDomain;
	protected $m_strCallAgentName;
	protected $m_intExtensionStatusId;
	protected $m_intEmployeeId;

	const START_AGENT_PHONE_EXTENSION_ID = 7000;
	const END_AGENT_PHONE_EXTENSION_ID = 9999;
	const START_PHYSICAL_PHONE_EXTENSION_ID = 100;
	const END_PHYSICAL_PHONE_EXTENSION_ID = 5999;
	const START_DESK_PHONE_EXTENSION_ID = 100;
	const END_DESK_PHONE_EXTENSION_ID = 5999;
	const VOICEMAIL = 4001;
	const EMERGENCY_SERVICE_CALL = 911;

	/**
	 * Override Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['domain'] ) ) $this->setDomain( $arrmixValues['domain'] );
		if( true == isset( $arrmixValues['call_agent_name'] ) ) $this->setCallAgentName( $arrmixValues['call_agent_name'] );
		if( true == isset( $arrmixValues['extension_status_id'] ) ) $this->setExtensionStatusId( $arrmixValues['extension_status_id'] );
		if( true == isset( $arrmixValues['employee_id'] ) ) $this->setEmployeeId( $arrmixValues['employee_id'] );
		return;
	}

	/**
	 * Custom get methods
	 */

 	public function getRegisterExtension() {
		return $this->getExtension() . CCallHelper::SIP_DOMAIN_NAME;
	}

 	public function getDecryptedPassword() {
		if( true == valStr( $this->getPassword() ) ) {
			return utf8_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( trim( $this->getPassword() ), CONFIG_SODIUM_KEY_PHONE_EXTENSION_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_PHONE_EXTENSION_PASSWORD ] ) );
		} else {
			return NULL;
		}
	}

 	public function getDecryptedVoicemailPin() {

		if( true == valStr( $this->getVoicemailPin() ) ) {
			return utf8_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( trim( $this->getVoicemailPin() ), CONFIG_SODIUM_KEY_PHONE_EXTENSION_PASSWORD, [ 'legacy_secret_key' => CONFIG_KEY_PHONE_EXTENSION_PASSWORD ] ) );
		} else {
			return NULL;
		}
	}

 	public function getDomain() {
		return ( true == valStr( $this->m_strDomain ) ? $this->m_strDomain : 'sip2.entrata.com' );
	}

	public function getCallAgentName() {
		return $this->m_strCallAgentName;
	}

	public function getExtensionStatusId() {
		return $this->m_intExtensionStatusId;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	/**
	 * set Functions
	 */

 	public function setDomain( $strDomain ) {
		$this->m_strDomain = $strDomain;
	}

	public function setCallAgentName( $strCallAgentName ) {
		$this->m_strCallAgentName = $strCallAgentName;
	}

	public function setExtensionStatusId( $intExtensionStatusId ) {
		$this->m_intExtensionStatusId = $intExtensionStatusId;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function encryptPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strPassword, CONFIG_SODIUM_KEY_PHONE_EXTENSION_PASSWORD );
		}

		return;
	}

	public function setEncryptedVoicemailPin( $strVoicemailPin ) {
		if( true == valStr( $strVoicemailPin ) ) {
			$this->setVoicemailPin( $this->encryptPassword( $strVoicemailPin ) );
		}
	}

	public function setEncryptedPassword( $strPassword ) {
		if( true == valStr( $strPassword ) ) {
			$this->setPassword( $this->encryptPassword( $strPassword ) );
		}
	}

	/**
	 * Validate function
	 */

	public function valPassword() {
		$boolIsValid	= true;
		$strPattern		= '/[^a-zA-Z0-9\#\%\(\)\[\]\@\!\*\?\^\$\-\_\=\{\}\+]/';

		if( false == valStr( $this->getDecryptedPassword(), 8 ) || true == preg_match( $strPattern, $this->getDecryptedPassword() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Phone password must be at least 8 characters long and only #%()[]@!?^*$-_={}+ special characters are allowed.' ) );
		}
		return $boolIsValid;
	}

 	public function valVoicemailPin() {
		$boolIsValid = true;

		if( true == is_null( $this->getDecryptedVoicemailPin() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Voicemail pin can not be blank.' ) );
		} else if( false == valId( $this->getDecryptedVoicemailPin() ) ) {
			$boolIsValid &= false;
		    $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Voicemail pin should be numeric.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPassword();
				$boolIsValid &= $this->valVoicemailPin();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case.
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Misc functions.
	 * @return string
	 */

	public function isVoicemailActivated() {
		return ( CPhoneExtensionType::CONFERENCE_PHONE_EXTENSION !== $this->getPhoneExtensionTypeId() ) ? 'true' : 'false';
	}

	public function generateDialString() {
		return '{^^:sip_invite_domain=' . CCallHelper::getSipDomainName() . ':presence_id=' . $this->getRegisterExtension() . '}${sofia_contact(' . $this->getRegisterExtension() . '},${verto_contact(' . $this->getRegisterExtension() . ')}';
	}

}
?>