<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallChunks
 * Do not add any new functions to this class.
 */

class CCallChunks extends CBaseCallChunks {

	public static function fetchCallChunksByCallIdOrderByOrderNum( $intCallId, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_chunks WHERE call_id=' . ( int ) $intCallId . ' ORDER BY order_num';
		return self::fetchCallChunks( $strSql, $objVoipDatabase );
	}

	public static function fetchCallChunksByCallIds( $arrintCallIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallIds ) ) return false;

		return self::fetchCallChunks( 'SELECT *FROM call_chunks WHERE call_id IN ( ' . implode( ',', $arrintCallIds ) . ' ) ORDER BY order_num', $objVoipDatabase );
	}

	public static function fetchFutureScheduledCallChunksByCallTypeIdByCallChunkTypeIdByLimit( $intCallTypeId, $intCallChunkTypeId, $intAvailablePorts, $objVoipDatabase ) {
        $intAvailablePorts = ( $intAvailablePorts > 0 ) ? $intAvailablePorts : 10;

		$strSql = 'SELECT
						ck.*,
						c.cid AS cid,
						c.property_id AS property_id
					FROM
						call_chunks AS ck
					JOIN calls AS c ON ( c.id = ck.call_id ) 
					WHERE 
						ck.call_chunk_type_id = ' . ( int ) $intCallChunkTypeId . '
						AND c.call_type_id = ' . ( int ) $intCallTypeId . ' 
						AND c.call_status_type_id = ' . CCallStatusType::WAITING_TO_SEND . '
						AND c.scheduled_call_id IS NOT NULL
						AND c.is_test_call <> 1
					ORDER BY c.id ASC 
					LIMIT ' . ( int ) $intAvailablePorts;

		return self::fetchCallChunks( $strSql, $objVoipDatabase );
	}

}
?>