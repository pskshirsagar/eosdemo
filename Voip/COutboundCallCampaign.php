<?php

class COutboundCallCampaign extends CBaseOutboundCallCampaign {

	const OUTBOUND_MOVE_IN 			= 1;
	const OUTBOUND_TRAINING_MOVE_IN	= 2;
	const SUPPORT_OUTBOUND_DIALER	= 3;
	const NET_PROMOTER_SCORE		= 4;

	/**
	 * Load Functions
	 */

 	public function loadCallQueueId() {

		$intCallQueueId = NULL;

		switch( $this->getId() ) {
			case self::OUTBOUND_MOVE_IN:
				$intCallQueueId = CCallQueue::MOVE_IN_CALLS;
				break;

			case self::OUTBOUND_TRAINING_MOVE_IN:
				$intCallQueueId = CCallQueue::TRAINING_MOVE_IN_CALLS;
				break;

			case self::SUPPORT_OUTBOUND_DIALER:
				break;

			case self::NET_PROMOTER_SCORE:
				break;

			default:
				// default case
				break;
		}

		return $intCallQueueId;
	}

 	public function loadCallQueueIds( $objVoipDatabase ) {

		if( false == valObj( $objVoipDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_ERROR );
		}

		$arrintCallQueueIds = NULL;

		switch( $this->getId() ) {
			case self::OUTBOUND_MOVE_IN:
				$arrintCallQueueIds = CCallQueues::fetchSimpleCallQueuesByCallQueueTypeId( CCallQueueType::INDIVIDUAL, $objVoipDatabase );
				break;

			case self::OUTBOUND_TRAINING_MOVE_IN:
				$arrintCallQueueIds = CCallQueues::fetchSimpleCallQueuesByCallQueueTypeId( CCallQueueType::INDIVIDUAL, $objVoipDatabase );
				break;

			case self::SUPPORT_OUTBOUND_DIALER:
				$arrintCallQueueIds = CCallQueues::fetchSimpleCallQueuesByCallQueueTypeId( CCallQueueType::INDIVIDUAL, $objVoipDatabase );
				break;

			case self::NET_PROMOTER_SCORE:
				$arrintCallQueueIds = array( CCallQueue::NPS_CALLS => CCallQueue::NPS_CALLS );
				break;

			default:
				// default case
				break;
		}

		return $arrintCallQueueIds;
	}

	/**
	 * Fetch Functions
	 */

 	public function fetchCallTypePsProductIds( $objVoipDatabase ) {

		$arrintCallTypePsProductIds = array();

		$arrobjCallTypeProducts = CCallTypeProducts::fetchPublishedCallTypeProductsByCallTypeId( $this->getCallTypeId(), $objVoipDatabase );

		if( true == valArr( $arrobjCallTypeProducts ) ) {
			foreach( $arrobjCallTypeProducts as $objCallTypeProduct ) {
				$arrintCallTypePsProductIds[] = $objCallTypeProduct->getPsProductId();
			}
		}

		return $arrintCallTypePsProductIds;
	}
}
?>