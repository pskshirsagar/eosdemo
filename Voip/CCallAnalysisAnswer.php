<?php

class CCallAnalysisAnswer extends CBaseCallAnalysisAnswer {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAnalysisQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;

		if( true == is_null( $this->getAnswer() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'answer', 'Answer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPoints() {
		$boolIsValid = true;

		switch( NULL ) {
			default:
				if( true == is_null( $this->getPoints() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'points', 'Points are required.' ) );
					break;
				}

				if( false == is_numeric( $this->getPoints() ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'points', 'Only numbers are allowed in points.' ) );
					break;
				}
		}

		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRank() {
		$boolIsValid = true;

		if( true == is_null( $this->getRank() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rank', 'Rank is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $intCallAnalysisQuestionTypeId = CCallAnalysisQuestionType::MULTIPLE_CHOICE ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				if( CCallAnalysisQuestionType::MULTIPLE_CHOICE == $intCallAnalysisQuestionTypeId ) {
					$boolIsValid &= $this->valAnswer();
					$boolIsValid &= $this->valPoints();
					$boolIsValid &= $this->valRank();
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intUserId, $objVoipDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intUserId, $objVoipDatabase, $boolReturnSqlOnly );
	}

}
?>