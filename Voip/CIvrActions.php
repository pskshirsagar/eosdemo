<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrActions
 * Do not add any new functions to this class.
 */

class CIvrActions extends CBaseIvrActions {

	public static function fetchAllIvrActions( $objVoipDatabase ) {
		return self::fetchIvrActions( 'SELECT * FROM ivr_actions ORDER BY order_num', $objVoipDatabase );
	}
}
?>