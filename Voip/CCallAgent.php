<?php

use Psi\Libraries\UtilHash\CHash;

class CCallAgent extends CBaseCallAgent {

	const CALL_AGENT_ID_PRAFULL_JAISWAL	= 410;

	protected $m_arrobjCallQueues;
	protected $m_arrobjCallAgentQueues;
	protected $m_arrobjCallAgentSkills;
	protected $m_arrobjCallAgentPreferences;
	protected $m_objRenderTemplate;

	protected $m_objCallAgentQueue;
	protected $m_objEmployee;
	protected $m_objCoachDetail;
	protected $m_intCallAgentId;
	protected $m_intCallAgentQueueId;
	protected $m_intCallQueueId;
	protected $m_intCallQueueLevel;
	protected $m_intLevel;
	protected $m_intPosition;
	protected $m_intPriority;
	protected $m_intQueuePhoneExtensionId;
	protected $m_intSessionExpirationTime;
	protected $m_intTotalTimeInCurrentStateStatusInSeconds;
	protected $m_intCallAgentLogId;
	protected $m_intLastOfferedCallTime;
	protected $m_intMonthlyLeaderboardRank;
	protected $m_intTeamLeaderboardRank;
	protected $m_fltTotalNumberOfPoints;
	protected $m_strCallAgentStateTypeName;
	protected $m_strCallAgentStatusTypeName;
	protected $m_strCallerName;
	protected $m_strCallQueueIds;
	protected $m_strCallSkillIds;
	protected $m_strCallQueueName;
	protected $m_strCallTypeName;
	protected $m_strEncryptedPassword;
	protected $m_strPhoneNumber;
	protected $m_strState;
	protected $m_strUnencryptedPassword;
	protected $m_strBreakOrProjectStartedOn;
	protected $m_strCallAgentPinTypeIds;
	protected $m_boolIsCallAgentQueue;
	protected $m_boolIsLoggedIn;
	protected $m_intCallId;
	protected $m_objXmppUser;
	protected $m_intTotalEarnedNumberOfPoints;

	public function setDefaults() {
		parent::setDefaults();
		$this->setCallAgentStatusTypeId( CCallAgentStatusType::LOGGED_OUT );
		$this->setCallAgentStateTypeId( CCallAgentStateType::IDLE );
		$this->setAgentPhoneExtensionId( NULL );
		$this->setDeskPhoneExtensionId( NULL );
		$this->setPhysicalPhoneExtensionId( NULL );
		$this->setLastOfferedCallId( NULL );
		$this->setOfferedCallQueueId( NULL );
		$this->setBreakOrProjectStartedOn( NULL );

		return true;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_agent_id'] ) )									$this->setCallAgentId( $arrmixValues['call_agent_id'] );
		if( true == isset( $arrmixValues['call_queue_id'] ) )									$this->setCallQueueId( $arrmixValues['call_queue_id'] );
		if( true == isset( $arrmixValues['call_agent_queue_id'] ) )								$this->setCallAgentQueueId( $arrmixValues['call_agent_queue_id'] );
		if( true == isset( $arrmixValues['queue_phone_extension_id'] ) )						$this->setQueuePhoneExtensionId( $arrmixValues['queue_phone_extension_id'] );
		if( true == isset( $arrmixValues['call_queue_level'] ) )								$this->setCallQueueLevel( $arrmixValues['call_queue_level'] );
		if( true == isset( $arrmixValues['call_agent_state_type_name'] ) )						$this->setCallAgentStateTypeName( $arrmixValues['call_agent_state_type_name'] );
		if( true == isset( $arrmixValues['call_agent_status_type_name'] ) )						$this->setCallAgentStatusTypeName( $arrmixValues['call_agent_status_type_name'] );
		if( true == isset( $arrmixValues['phone_number'] ) )									$this->setPhoneNumber( $arrmixValues['phone_number'] );
		if( true == isset( $arrmixValues['call_type_name'] ) )									$this->setCallTypeName( $arrmixValues['call_type_name'] );
		if( true == isset( $arrmixValues['is_call_agent_queue'] ) )								$this->setIsCallAgentQueue( $arrmixValues['is_call_agent_queue'] );
		if( true == isset( $arrmixValues['call_queue_name'] ) )									$this->setCallQueueName( $arrmixValues['call_queue_name'] );
		if( true == isset( $arrmixValues['caller_name'] ) )										$this->setCallerName( $arrmixValues['caller_name'] );
		if( true == isset( $arrmixValues['unencrypted_password'] ) )							$this->setUnencryptedPassword( $arrmixValues['unencrypted_password'] );
		if( true == isset( $arrmixValues['call_queue_ids'] ) )									$this->setCallQueueIds( $arrmixValues['call_queue_ids'] );
		if( true == isset( $arrmixValues['call_skill_ids'] ) )									$this->setCallSkillIds( $arrmixValues['call_skill_ids'] );
		if( true == isset( $arrmixValues['priority'] ) )										$this->setPriority( $arrmixValues['priority'] );
		if( true == isset( $arrmixValues['position'] ) ) 										$this->setPosition( $arrmixValues['position'] );
		if( true == isset( $arrmixValues['state'] ) )											$this->setState( $arrmixValues['stage'] );
		if( true == isset( $arrmixValues['level'] ) ) 											$this->setLevel( $arrmixValues['level'] );
		if( true == isset( $arrmixValues['total_time_in_current_state_status_in_seconds'] ) )	$this->setTotalTime( $arrmixValues['total_time_in_current_state_status_in_seconds'] );
		if( true == isset( $arrmixValues['last_offered_call_time'] ) )							$this->setLastOfferedCallTime( $arrmixValues['last_offered_call_time'] );
		if( true == isset( $arrmixValues['total_number_of_points'] ) )							$this->setTotalNumberOfPoints( $arrmixValues['total_number_of_points'] );
		if( true == isset( $arrmixValues['call_id'] ) )											$this->setCallId( $arrmixValues['call_id'] );
		if( true == isset( $arrmixValues['call_agent_pin_type_ids'] ) )							$this->setCallAgentPinTypeIds( $arrmixValues['call_agent_pin_type_ids'] );
		if( true == isset( $arrmixValues['team_id'] ) )											$this->setTeamId( $arrmixValues['team_id'] );
		if( true == isset( $arrmixValues['total_earned_number_of_points'] ) )					$this->setTotalEarnedNumberOfPoints( $arrmixValues['total_earned_number_of_points'] );
		if( true == isset( $arrmixValues['monthly_leaderboard_rank'] ) )						$this->setMonthlyLeaderboardRank( $arrmixValues['monthly_leaderboard_rank'] );
		if( true == isset( $arrmixValues['team_leaderboard_rank'] ) )							$this->setTeamLeaderboardRank( $arrmixValues['team_leaderboard_rank'] );

		$this->setAllowDifferentialUpdate( true );
		return true;
	}

	public function setCallAgentQueue( $objCallAgentQueue ) {
		$this->m_objCallAgentQueue = $objCallAgentQueue;
	}

	public function setEmployee( $objEmployee ) {
		$this->m_objEmployee = $objEmployee;
	}

	public function setCoachDetail( $objCoachDetail ) {
		$this->m_objCoachDetail = $objCoachDetail;
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->m_intCallAgentId = $intCallAgentId;
	}

	public function setCallAgentQueueId( $intCallAgentQueueId ) {
		$this->m_intCallAgentQueueId = $intCallAgentQueueId;
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setCallQueueLevel( $intCallQueueLevel ) {
		$this->m_intCallQueueLevel = $intCallQueueLevel;
	}

	public function setLevel( $intLevel ) {
		$this->m_intLevel = $intLevel;
	}

	public function setPosition( $intPosition ) {
		$this->m_intPosition = $intPosition;
	}

	public function setPriority( $intPriority ) {
		$this->m_intPriority = $intPriority;
	}

	public function setQueuePhoneExtensionId( $intQueuePhoneExtensionId ) {
		$this->m_intQueuePhoneExtensionId = $intQueuePhoneExtensionId;
	}

	public function setSessionExpirationTime( $intSessionExpirationTime ) {
		$this->m_intSessionExpirationTime = $intSessionExpirationTime;
	}

	public function setTotalTime( $intTotalTimeInCurrentStateStatusInSeconds ) {
		$this->m_intTotalTimeInCurrentStateStatusInSeconds = $intTotalTimeInCurrentStateStatusInSeconds;
	}

	public function setLastOfferedCallTime( $intLastOfferedCallTime ) {
		$this->m_intLastOfferedCallTime = $intLastOfferedCallTime;
	}

	public function setTotalNumberOfPoints( $fltTotalNumberOfPoints ) {
		$this->m_fltTotalNumberOfPoints = $fltTotalNumberOfPoints;
	}

	public function setCallAgentStateTypeName( $strCallAgentStateTypeName ) {
		$this->m_strCallAgentStateTypeName = $strCallAgentStateTypeName;
	}

	public function setCallAgentStatusTypeName( $strCallAgentStatusTypeName ) {
		$this->m_strCallAgentStatusTypeName = $strCallAgentStatusTypeName;
	}

	public function setCallerName( $strCallerName ) {
		$this->m_strCallerName = $strCallerName;
	}

	public function setCallQueueIds( $arrintCallQueueIds ) {
		if( true == valArr( $arrintCallQueueIds ) ) {
			$this->m_strCallQueueIds = implode( ',', $arrintCallQueueIds );
		} elseif( false == valArr( $arrintCallQueueIds ) ) {
			$this->m_strCallQueueIds = NULL;
		}
	}

	public function setCallSkillIds( $arrintCallSkillIds ) {
		$this->m_strCallSkillIds = NULL;

		if( true == valArr( $arrintCallSkillIds ) ) {
			$this->m_strCallSkillIds = implode( ',', $arrintCallSkillIds );
		}
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	public function setCallTypeName( $strCallTypeName ) {
		$this->m_strCallTypeName = $strCallTypeName;
	}

	public function setEncryptedPassword( $strEncryptedPassword ) {
		$this->m_strEncryptedPassword = $strEncryptedPassword;
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = $strPhoneNumber;
	}

	public function setState( $strState ) {
		$this->m_strState = $strState;
	}

	public function setUnencryptedPassword( $strUnencryptedPassword ) {
		$this->m_strUnencryptedPassword = trim( $strUnencryptedPassword );
		$this->m_strPassword            = CHash::createService()->hashUserOld( trim( $strUnencryptedPassword ) );
	}

	public function setIsCallAgentQueue( $boolIsCallAgentQueue ) {
		$this->m_boolIsCallAgentQueue = $boolIsCallAgentQueue;
	}

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = $boolIsLoggedIn;
	}

	public function setCallAgentLogId( $intCallAgentLogId ) {
		$this->m_intCallAgentLogId = $intCallAgentLogId;
	}

	public function setCallAgentPreferences( $arrobjCallAgentPreferences ) {
		$this->m_arrobjCallAgentPreferences = $arrobjCallAgentPreferences;
	}

	public function setBreakOrProjectStartedOn( $strBreakOrProjectStartedOn ) {
		$this->m_strBreakOrProjectStartedOn = $strBreakOrProjectStartedOn;
	}

	public function setCallAgentPinTypeIds( $strCallAgentPinTypeIds ) {
		$this->m_strCallAgentPinTypeIds = $strCallAgentPinTypeIds;
	}

	public function setCallId( $intCallId ) {
		$this->m_intCallId = $intCallId;
	}

	public function setTotalEarnedNumberOfPoints( $intTotalEarnedNumberOfPoints ) {
		$this->m_intTotalEarnedNumberOfPoints = $intTotalEarnedNumberOfPoints;
	}

	public function setMonthlyLeaderboardRank( $intMonthlyLeaderboardRank ) {
		$this->m_intMonthlyLeaderboardRank = $intMonthlyLeaderboardRank;
	}

	public function setTeamLeaderboardRank( $intTeamLeaderboardRank ) {
		$this->m_intTeamLeaderboardRank = $intTeamLeaderboardRank;
	}

	/**
	 * Get Functions
	 */

	public function getCallQueues() {
		return $this->m_arrobjCallQueues;
	}

	public function getCallAgentQueues() {
		return $this->m_arrobjCallAgentQueues;
	}

	public function getCallAgentSkills() {
		return $this->m_arrobjCallAgentSkills;
	}

	public function getCallAgentQueue() {
		return $this->m_objCallAgentQueue;
	}

	public function getEmployee() {
		return $this->m_objEmployee;
	}

	public function getCoachDetail() {
		return $this->m_objCoachDetail;
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function getCallAgentQueueId() {
		return $this->m_intCallAgentQueueId;
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getCallQueueLevel() {
		return $this->m_intCallQueueLevel;
	}

	public function getLevel() {
		return $this->m_intLevel;
	}

	public function getPosition() {
		return $this->m_intPosition;
	}

	public function getPriority() {
		return $this->m_intPriority;
	}

	public function getQueuePhoneExtensionId() {
		return $this->m_intQueuePhoneExtensionId;
	}

	public function getSessionExpirationTime() {
		return $this->m_intSessionExpirationTime;
	}

	public function getTimeInCurrentStatusState() {
		return CCallHelper::changeCallDurationFormat( $this->m_intTotalTimeInCurrentStateStatusInSeconds );
	}

	public function getLastOfferedCallTime() {
		return $this->m_intLastOfferedCallTime;
	}

	public function getTotalNumberOfPoints() {
		return $this->m_fltTotalNumberOfPoints;
	}

	public function getCallAgentStateTypeName() {
		return $this->m_strCallAgentStateTypeName;
	}

	public function getCallAgentStatusTypeName() {
		return $this->m_strCallAgentStatusTypeName;
	}

	public function getCallerName() {
		return $this->m_strCallerName;
	}

	public function getCallQueueIds() {
		return $this->m_strCallQueueIds;
	}

	public function getCallSkillIds() {
		return $this->m_strCallSkillIds;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function getCallTypeName() {
		return $this->m_strCallTypeName;
	}

	public function getEncryptedPassword() {
		return $this->m_strEncryptedPassword;
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function getState() {
		return $this->m_strState;
	}

	public function getUnencryptedPassword() {
		return $this->m_strUnencryptedPassword;
	}

	public function getIsCallAgentQueue() {
		return $this->m_boolIsCallAgentQueue;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getCallAgentLogId() {
		return $this->m_intCallAgentLogId;
	}

	public function getBreakOrProjectStartedOn() {
		return $this->m_strBreakOrProjectStartedOn;
	}

	public function getCallAgentPinTypeIds() {
		return $this->m_strCallAgentPinTypeIds;
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function getTotalEarnedNumberOfPoints() {
		return $this->m_intTotalEarnedNumberOfPoints;
	}

	public function getMonthlyLeaderboardRank() {
		return $this->m_intMonthlyLeaderboardRank;
	}

	public function getTeamLeaderboardRank() {
		return $this->m_intTeamLeaderboardRank;
	}

	/**
	 * Build Functions
	 */

	public function getBuildCallAgentName() {
		return ( $this->getAgentPhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME );
	}

	public function getContact() {
		return ( true == valStr( $this->getPhysicalPhoneExtensionId() ) ) ? ( $this->getPhysicalPhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME ) : '';
	}

	public function getBuildCallAgentContact() {
        return ( true == valStr( $this->getPhysicalPhoneExtensionId() ) ) ? ( '[call_timeout=40]user/' . $this->getPhysicalPhoneExtensionId() . CCallHelper::SIP_DOMAIN_NAME ) : '';
	}

	public function getForwardAlways() {
		return $this->getOrFetchCallAgentPreferenceValue( CCallAgentPreference::FORWARD_ALWAYS );
	}

	public function getForwardAtBusy() {
		return $this->getOrFetchCallAgentPreferenceValue( CCallAgentPreference::FORWARD_AT_BUSY );
	}

	public function getForwardAtNoAnwser() {
		return $this->getOrFetchCallAgentPreferenceValue( CCallAgentPreference::FORWRAD_AT_NO_ANSWER );
	}

	public function getIsRecordOutboundCall() {
		return $this->getOrFetchCallAgentPreferenceValue( CCallAgentPreference::IS_RECORD_OUTBOUND_CALL );
	}

	public function getOrFetchCallAgentPreferenceValue( $strCallAgentPreference ) {
		if( false == valArr( $this->m_arrobjCallAgentPreferences ) ) {
			return;
		}

		return ( true == array_key_exists( $strCallAgentPreference, $this->m_arrobjCallAgentPreferences ) ? $this->m_arrobjCallAgentPreferences[$strCallAgentPreference]->getValue() : '' );
	}

	public function getOutboundCallerNumber() {
		if( true == valStr( $this->getPhoneNumber() ) ) {
			return $this->getPhoneNumber();
		}

		return ( CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CCallPhoneNumber::LEASING_CENTER_LINE : CCallPhoneNumber::MAIN_SUPPORT_LINE;
	}

	public function getOutboundCallerName() {
		return ( true == valStr( $this->getFullName() ) ? $this->getFullName() : CONFIG_COMPANY_NAME_FULL );
	}

	public function getCallAgentPreferredName() {

		if( true == valStr( $this->m_strPreferredName ) ) {
			return $this->m_strPreferredName;
		} else {
			return $this->m_strFullName;
		}
	}

	public function getAgentState() {
		switch( $this->getNextCallAgentStateTypeId() ) {
			case CCallAgentStateType::WAITING:
				$strAgentState = 'Waiting';
				break;

			case CCallAgentStateType::IDLE:
				$strAgentState = 'Idle';
				break;

			case CCallAgentStateType::RECEIVING:
				$strAgentState = 'Receiving';
				break;

			case CCallAgentStateType::IN_A_QUEUE_CALL:
				$strAgentState = 'In a Queue Call';
				break;

			case CCallAgentStateType::IN_A_OUTBOUND_CALL:
				$strAgentState = 'In a Outbound Call';
				break;

			case CCallAgentStateType::DIALING:
				$strAgentState = 'Dialing';
				break;

			case CCallAgentStateType::REMOVED:
				$strAgentState = 'Removed';
				break;

			default:
				$strAgentState = 'Idle';
				break;
		}

		return $strAgentState;
	}

	public function getOrFetchCoachDetail( $objAdminDatabase ) {
		if( false == valObj( $this->m_objCoachDetail, 'CEmployeeGoal' ) ) {
			$this->m_objCoachDetail = \Psi\Eos\Admin\CEmployeeGoals::createService()->fetchEmployeeCoachDetailByCallAgentEmployeeId( $this->getEmployeeId(), $objAdminDatabase );
		}

		return $this->m_objCoachDetail;
	}

	public function createCallAgentDetail() {
		$objCallAgentDetail = new CCallAgentDetail();

		$objCallAgentDetail->setCallAgentId( $this->getId() );
		return $objCallAgentDetail;
	}

	public function getJabberId() {
		return sprintf( 'Entrata_%s', $this->getUserId() );
	}

	public function getJabberPassword() {
		return md5( sprintf( '%s_%s', $this->getUsername(), $this->getCallAgentId() ) );
	}

	/**
	 * Adder Functions
	 */

	public function addCallQueue( $objCallQueue ) {
		$this->m_arrobjCallQueues[$objCallQueue->getId()] = $objCallQueue;
	}

	public function addCallAgentQueue( $objCallAgentQueue ) {
		$this->m_arrobjCallAgentQueues[$objCallAgentQueue->getId()] = $objCallAgentQueue;
	}

	public function addCallAgentSkill( $objCallAgentSkill ) {
		$this->m_arrobjCallAgentSkills[$objCallAgentSkill->getId()] = $objCallAgentSkill;
	}

	/**
	 * Create Or Fetch Functions
	 */

	public function createCallAgentQueue() {
		$objCallAgentQueue = new CCallAgentQueue();
		$objCallAgentQueue->setDefaults();
		$objCallAgentQueue->setCallAgentId( $this->getId() );
		$objCallAgentQueue->setCallQueueId( CCallQueue::PROPERTY_SUPPORT );
		$objCallAgentQueue->setState( CTier::READY );
		$objCallAgentQueue->setLevel( CCallAgentSkill::DEFAULT_PRIORITY );
		$objCallAgentQueue->setPosition( CCallAgentSkill::DEFAULT_POSITION );
		return $objCallAgentQueue;
	}

	public function createAgent() {
		$objAgent = new CAgent();
		$objAgent->setDefaults();
		$objAgent->setCallAgentId( $this->getId() );
		$objAgent->setName( $this->getBuildCallAgentName() );
		$objAgent->setSystem( CAgent::SINGLE_BOX );
		$objAgent->setType( CAgent::CALLBACK );
		$objAgent->setStatus( CCallAgentStatusType::$c_arrstrCallAgentStatusTypeIdToStr[$this->getCallAgentStatusTypeId()] );
		$objAgent->setState( CCallAgentStateType::$c_arrstrCallAgentStateTypeIdToStr[$this->getCallAgentStateTypeId()] );
		$objAgent->setContact( $this->getBuildCallAgentContact() );
		$objAgent->setMaxNoAnswer( CAgent::MAX_NO_ANSWER );
		$objAgent->setWrapUpTime( CAgent::WRAP_UP_TIME );
		$objAgent->setRejectDelayTime( CAgent::REJECT_DELAY_TIME );
		$objAgent->setBusyDelayTime( CAgent::BUSY_DELAY_TIME );
		return $objAgent;
	}

	public function createTier() {
		$objTier = new CTier();
		$objTier->setDefaults();
		$objTier->setCallAgentId( $this->getCallAgentId() );
		$objTier->setAgent( $this->getBuildCallAgentName() );
		return $objTier;
	}

	public function createCallAgentLog() {
		$objCallAgentLog = new CCallAgentLog();
		$objCallAgentLog->setDefaults();
		$objCallAgentLog->setCallAgentId( $this->getId() );
		$objCallAgentLog->setCallAgentStatusTypeId( $this->getCallAgentStatusTypeId() );
		$objCallAgentLog->setCallAgentStateTypeId( $this->getCallAgentStateTypeId() );
		return $objCallAgentLog;
	}

	public function createCallAgentPreference() {
		$objCallAgentPreference = new CCallAgentPreference();
		$objCallAgentPreference->setDefaults();
		$objCallAgentPreference->setCallAgentId( $this->getId() );
		$objCallAgentPreference->setEmployeeId( $this->getEmployeeId() );
		return $objCallAgentPreference;
	}

	public function createCallAgentNote() {
		$objCallAgentNote = new CCallAgentNote();
		$objCallAgentNote->setDefaults();
		$objCallAgentNote->setCallAgentId( $this->getId() );
		$objCallAgentNote->setCallAgentLogId( $this->getCallAgentLogId() );
		return $objCallAgentNote;
	}

	public function createSurvey( $objCall, $intSurveyTypeId, $intSurveyTemplateId ) {
		if( false == valObj( $objCall, 'CCall' ) || false == valId( $intSurveyTypeId ) || false == valId( $intSurveyTemplateId ) ) {
			return NULL;
		}

		$objSurvey = new CSurvey();
		$objCallAgentSurveyHelper = new CCallAgentSurveyHelper();
		$objSurvey->setSurveyTypeId( $intSurveyTypeId );
		$objSurvey->setSurveyTemplateId( $intSurveyTemplateId );
		$objSurvey->setTriggerReferenceId( $objCall->getId() );
		$objSurvey->setSubjectReferenceId( $this->getEmployeeId() );
		$arrmixCallDetails = $objCallAgentSurveyHelper->processCallDetails( $objCall );
		$objSurvey->setCallDetails( $arrmixCallDetails );
		return $objSurvey;
	}

	public function createCallAgentRank( $objVoipDatabase, $intRankNumber = CRankPointMapping::DEFAULT_RANK_NUMBER ) {
		$objRankPointMapping = \Psi\Eos\Voip\CRankPointMappings::createService()->fetchCurrentAndNextRankPointMappingDetailByRankNumber( $intRankNumber, $objVoipDatabase );

		if( true == valObj( $objRankPointMapping, 'CRankPointMapping' ) ) {
			$objCallAgentRank = new CCallAgentRank();
			$objCallAgentRank->setCallAgentId( $this->getId() );
			$objCallAgentRank->setRankPointMappingId( $objRankPointMapping->getId() );
			$objCallAgentRank->setNextRankPointMappingId( $objRankPointMapping->getNextRankPointMappingId() );
			$objCallAgentRank->setMyCredits( $objRankPointMapping->getCredits() );
			$objCallAgentRank->setIsCurrentActiveRank( true );
			return $objCallAgentRank;
		}

		return false;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;

		if( false == valId( $this->getId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Call agent id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valConflictAgentPhoneExtensionId( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valId( $this->getAgentPhoneExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_extension', 'Agent extension is required.' ) );
		} elseif( true == valId( $this->fetchConflictCallAgentByAgentPhoneExtensionId( $this->getAgentPhoneExtensionId(), $objVoipDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_extension', 'Login extension ' . $this->getAgentPhoneExtensionId() . ' is already occupied.' ) );
		}

		return $boolIsValid;
	}

	public function valStaticWrapUpTime() {
		$boolIsValid = true;

		if( CDepartment::CALL_CENTER == $this->getDepartmentId() ) {
			if( false == valStr( $this->getStaticWrapUpTime() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'static_wrap_up_time', 'Static wrap-up time is required.' ) );
			} elseif( 1 > $this->getStaticWrapUpTime() || 90 < $this->getStaticWrapUpTime() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'static_wrap_up_time', 'Static wrap-up time should be between 1 and 90' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCallAgentLogoutStatus() {
		$boolIsValid = true;

		if( true == valId( $this->getCallAgentStatusTypeId() ) && CCallAgentStatusType::LOGGED_OUT != $this->getCallAgentStatusTypeId() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_status_type_id', 'Call agent ' . $this->getFullName() . ' is not logout.' ) );
		}

		return $boolIsValid;
	}

	public function valCallAgentState() {
		$boolIsValid = true;

		if( true == valId( $this->getCallAgentStateTypeId() ) && ( CCallAgentStateType::RECEIVING == $this->getCallAgentStateTypeId() || CCallAgentStateType::IN_A_QUEUE_CALL == $this->getCallAgentStateTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'call_agent_state_type_id', 'Call agent ' . $this->getFullName() . ' call is in progress.' ) );
		}

		return $boolIsValid;
	}

	public function valLogin() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strUsername ) || false == valStr( $this->getUnencryptedPassword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unencrypted_password', 'Username and/or password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhysicalAndAgentPhoneExtensionId( $objVoipDatabase ) {
		$boolIsValid = true;

		if( true == valId( CCallAgents::fetchConflictLoggedInCallAgentCountByPhysicalPhoneExtensionId( $this->getPhysicalPhoneExtensionId(), $objVoipDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agent_phone_extension_id', 'Call agent already registered with different desk.' ) );
		}

		return $boolIsValid;
	}

	public function valPhysicalPhoneExtensionId( $objVoipDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getPhysicalPhoneExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Agent phone extension is required.' ) );
		} elseif( false == valId( $this->getPhysicalPhoneExtensionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Agent phone extension should be numeric.' ) );
		} elseif( false == valId( CPhoneExtensions::fetchPhysicalPhoneExtensionCountById( $this->getPhysicalPhoneExtensionId(), $objVoipDatabase ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Agent phone extension does not exist.' ) );
		}

		return $boolIsValid;
	}

	public function validateInsertCallAgent( $objVoipDatabase ) {
		$boolIsValid = true;

		if( true == valStr( $this->getCallQueueIds() ) ) {
			$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objVoipDatabase );
		}

		if( CDepartment::TECHNICAL_SUPPORT == $this->getDepartmentId() && true == valId( $this->getAgentPhoneExtensionId() ) ) {
			if( false == valStr( $this->getPhysicalPhoneExtensionId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Desk phone extension is required.' ) );
			} elseif( false == valId( $this->getPhysicalPhoneExtensionId() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'physical_phone_extension_id', 'Desk phone extension should be numeric.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objVoipDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objVoipDatabase );
				$boolIsValid &= $this->valCallAgentState();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_login':
				$boolIsValid &= $this->valLogin();
				break;

			case 'call_agent_logout':
				break;

			case 'login_call_agent':
				$boolIsValid &= $this->valPhysicalPhoneExtensionId( $objVoipDatabase );
				$boolIsValid &= $this->valPhysicalAndAgentPhoneExtensionId( $objVoipDatabase );
				break;

			case 'insert_call_agent':
				$boolIsValid &= $this->validateInsertCallAgent( $objVoipDatabase );
				$boolIsValid &= $this->valStaticWrapUpTime();
				break;

			case 'associate_call_agent_queue':
				$boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objVoipDatabase );
				break;

			case 'associate_call_agent_skill':
				// $boolIsValid &= $this->valConflictAgentPhoneExtensionId( $objVoipDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Inherited Functions
	 */

	public function insertOrUpdate( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( false == valId( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase );
		} else {
			return $this->update( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase );
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( false == parent::insert( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		$objCallAgentLog = $this->createCallAgentLog();

		if( false == $objCallAgentLog->insert( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {
			$objAgent = $this->createAgent();

			if( false == $objAgent->insert( $intCurrentUserId, $objFreeswitchDatabase ) ) {
				return false;
			}
		}

		$objCallAgentRank = $this->createCallAgentRank( $objVoipDatabase );

		if( true == valObj( $objCallAgentRank, 'CCallAgentRank' ) ) {
			$objCallAgentRank->insert( $intCurrentUserId, $objVoipDatabase );
		}

		return true;
	}

	public function update( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase = NULL ) {
		if( false == parent::update( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}

		// Load the most recent call agent log, and make sure status has changed before we write a new log record.
		$objCallAgentLog = $this->fetchLastCallAgentLog( $objVoipDatabase );

		if( false == valObj( $objCallAgentLog, 'CCallAgentLog' ) || ( int ) $objCallAgentLog->getCallAgentStatusTypeId() != $this->getCallAgentStatusTypeId() || ( int ) $objCallAgentLog->getCallAgentStateTypeId() != $this->getCallAgentStateTypeId() ) {
			$objCallAgentLog = $this->createCallAgentLog();

			if( false == $objCallAgentLog->insert( $intCurrentUserId, $objVoipDatabase ) ) {
				return false;
			}

			$this->setCallAgentLogId( $objCallAgentLog->getId() );
		}

		if( true == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {
			$objAgent = CAgents::fetchAgentByCallAgentId( $this->getId(), $objFreeswitchDatabase );
			$objAgent = ( true == valObj( $objAgent, 'CAgent' ) && true == valId( $objAgent->getId() ) ) ? $objAgent : $this->createAgent();

			if( true == valId( $this->getAgentPhoneExtensionId() ) ) {
				$objAgent->setName( $this->getbuildCallAgentName() );
			}

			if( true == valId( $this->getAgentPhoneExtensionId() ) ) {
				return $objAgent->insertOrUpdate( $intCurrentUserId, $objFreeswitchDatabase );
			} else {
				return $objAgent->delete( $intCurrentUserId, $objFreeswitchDatabase );
			}
		}

		return true;
	}

	/**
	 * Support Functions
	 */

	public function processLogin( $objVoipDatabase ) {
		$objCallAgent = CCallAgents::fetchCallAgentByUsernameByPassword( $this->getUsername(), $this->getPassword(), $objVoipDatabase );

		if( true == valObj( $objCallAgent, 'CCallAgent' ) ) {
			$objCallAgent->setIsLoggedIn( true );
			$objCallAgent->setSessionExpirationTime( time() + ( 24 * 3600 ) );

			return $objCallAgent;
		}

		$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );

		return false;
	}

	public function logout() {
		$this->setIsLoggedIn( false );

		return true;
	}

	public function onBreak( $objVoipDatabase ) {
		$this->setCallAgentStatusTypeId( CCallAgentStatusType::ON_BREAK );
		$this->setCallAgentStateTypeId( CCallAgentStateType::IDLE );

		if( false == $this->update( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

	public function getOrFetchEmployee( $objAdminDatabase ) {
		if( false == valObj( $this->m_objEmployee, 'CEmployee' ) ) {
			$this->m_objEmployee = CEmployees::fetchEmployeeById( $this->m_intEmployeeId, $objAdminDatabase );
		}

		return $this->m_objEmployee;
	}

	public function deleteCallAgentQueues( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase ) {
		$arrobjCallAgentQueues = $this->fetchCallAgentQueues( $objVoipDatabase );

		if( true == valArr( $arrobjCallAgentQueues ) ) {
			foreach( $arrobjCallAgentQueues as $objCallAgentQueue ) {
				if( false == $objCallAgentQueue->delete( $intCurrentUserId, $objVoipDatabase, $objFreeswitchDatabase ) ) {
					return false;
				}
			}
		}

		$arrobjTiers = CTiers::fetchTiersByCallAgentId( $this->getCallAgentId(), $objFreeswitchDatabase );
		if( false == valArr( $arrobjTiers ) ) return true;
		if( false == CTiers::bulkDelete( $arrobjTiers, $objFreeswitchDatabase ) ) return false;

		return true;
	}

	public function deleteCallAgentSkills( $intCurrentUserId, $objVoipDatabase ) {
		$arrobjCallAgentSkills = $this->fetchCallAgentSkills( $objVoipDatabase );

		if( true == valArr( $arrobjCallAgentSkills ) ) {
			foreach( $arrobjCallAgentSkills as $objCallAgentSkill ) {
				if( false == $objCallAgentSkill->delete( $intCurrentUserId, $objVoipDatabase ) ) {
					return false;
				}
			}
		}

		return true;
	}

	public function updatePhoneExtensions( $intCurrentUserId, $objVoipDatabase ) {
		$objCallAgent = $this->fetchCallAgentByCallAgentId( $objVoipDatabase );

		if( true == valObj( $objCallAgent, 'CCallAgent' ) ) {
			$objCallAgent->setAgentPhoneExtensionId( NULL );
			$objCallAgent->setPhysicalPhoneExtensionId( NULL );
			$objCallAgent->setDeskPhoneExtensionId( NULL );
		}
		if( false == $objCallAgent->update( $intCurrentUserId, $objVoipDatabase ) ) {
			return false;
		}
		return true;
	}

	public function updateCallPhoneNumber( $intCurrentUserId, $objVoipDatabase ) {
		$arrobjCallPhoneNumbers = $this->fetchCallPhoneNumberByEmployeeId( $objVoipDatabase );

		if( true == valArr( $arrobjCallPhoneNumbers ) ) {
			foreach( $arrobjCallPhoneNumbers as $arrobjCallPhoneNumber ) {
				$arrobjCallPhoneNumber->setPhoneExtensionId( NULL );
				$arrobjCallPhoneNumber->setEmployeeId( NULL );
				if( false == $arrobjCallPhoneNumber->update( $intCurrentUserId, $objVoipDatabase ) ) {
					return false;
				}
			}
		}
		return true;
	}

	public function deleteAgent( $intCurrentUserId, $objFreeswitchDatabase ) {
		$objAgent = CAgents::fetchAgentByCallAgentId( $this->getId(), $objFreeswitchDatabase );
		if( true == valObj( $objAgent, 'CAgent' ) && false == $objAgent->delete( $intCurrentUserId, $objFreeswitchDatabase ) ) {
			return false;
		}
		return true;
	}

	public function deleteChatQueue( $intCurrentUserId, $objChatDatabase ) {
		$arrobjChatQueueEmployees = \Psi\Eos\Chats\CChatQueueEmployees::createService()->fetchChatQueueEmployeesByEmployeeId( $this->getEmployeeId(), $objChatDatabase );
		if( true == valArr( $arrobjChatQueueEmployees ) ) {
			foreach( $arrobjChatQueueEmployees as $arrobjChatQueueEmployee ) {
				if( false == $arrobjChatQueueEmployee->delete( $intCurrentUserId, $objChatDatabase ) ) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCallAgentQueues( $objVoipDatabase ) {
		return CCallAgentQueues::fetchCallAgentQueues( 'SELECT * FROM call_agent_queues WHERE call_agent_id = ' . ( int ) $this->getId(), $objVoipDatabase );
	}

	public function fetchAssociateCallQueues( $objVoipDatabase ) {
		return \Psi\Eos\Voip\CCallQueues::createService()->fetchAssociateCallQueuesByCallAgentId( $this->getId(), $objVoipDatabase );
	}

	public function fetchConflictCallAgentByAgentPhoneExtensionId( $strAgentPhoneExtensionId, $objVoipDatabase ) {
		$strWhere = ' WHERE
						agent_phone_extension_id = \'' . $strAgentPhoneExtensionId . '\'';

		if( true == valId( $this->getId() ) ) {
			$strWhere .= ' AND id <> ' . $this->getId();
		}

		return CCallAgents::fetchCallAgentCount( $strWhere, $objVoipDatabase );
	}

	public function fetchLastCallAgentLog( $objVoipDatabase ) {
		return CCallAgentLogs::fetchLastCallAgentLogByCallAgentId( $this->getId(), $objVoipDatabase );
	}

	public function fetchCallAgentSkills( $objVoipDatabase ) {
		$strSql = 'SELECT
						cas.*,
						cs.name AS call_skill_name
					FROM
						call_agent_skills AS cas
						JOIN call_skills AS cs ON ( cs.id = cas.call_skill_id )
						JOIN call_agents AS ca ON ( ca.id = cas.call_agent_id )
					WHERE
						ca.id = ' . ( int ) $this->getId();

		return CCallAgentSkills::fetchCallAgentSkills( $strSql, $objVoipDatabase );
	}

	public function fetchCallSkills( $objVoipDatabase ) {
		$strSql = 'SELECT
						cs.*
					FROM
						call_skills AS cs
						JOIN call_agent_skills AS cas ON ( cas.call_skill_id = cs.id )
						JOIN call_agents AS ca ON ( ca.id = cas.call_agent_id )
					WHERE
						ca.id = ' . ( int ) $this->getId();

		return CCallSkills::fetchCallSkills( $strSql, $objVoipDatabase );
	}

	public function fetchAgent( $objFreeswitchDatabase ) {
		return CAgents::fetchAgentByCallAgentId( $this->getId(), $objFreeswitchDatabase );
	}

	public function fetchTiers( $objFreeswitchDatabase ) {
		return CTiers::fetchTiersByCallAgentId( $this->getId(), $objFreeswitchDatabase );
	}

	public function fetchEmployeePreferenceByKey( $strEmployeePreference, $objAdminDatabase ) {
		return CEmployeePreferences::fetchEmployeePreferenceByEmployeeIdByKey( $this->getEmployeeId(), $strEmployeePreference, $objAdminDatabase );
	}

	public function fetchCallAgentPreferences( $objVoipDatabase ) {
		return CCallAgentPreferences::fetchReKeyedCallAgentPreferencesByCallAgentId( $this->getId(), $objVoipDatabase );
	}

	public function fetchTierByCallQueueId( $intCallQueueId, $objFreeswitchDatabase ) {
		$strSql = 'SELECT *FROM tiers WHERE call_agent_id = ' . ( int ) $this->getId() . ' AND call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1';

		return CTiers::fetchTier( $strSql, $objFreeswitchDatabase );
	}

	public function fetchCallAgentByCallAgentId( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgent( 'SELECT * FROM call_agents WHERE id = ' . ( int ) $this->getId(), $objVoipDatabase );
	}

	public function fetchCallPhoneNumberByEmployeeId( $objVoipDatabase ) {
		return CCallPhoneNumbers::fetchCallPhoneNumbers( ' SELECT * FROM call_phone_numbers WHERE employee_id = ' . ( int ) $this->getEmployeeId(), $objVoipDatabase );
	}

	/**
	 * Other Functions
	 */

	public function processLeaseSignedSuccess( $objApplicationDataObject, $objCall, $intUserId, $objEmailDatabase, $objVoipDatabase ) {
		$arrmixDetails						= ( array ) $objCall->getDetails();
		$arrmixActiveRewardPointMultipliers	= $arrmixDetails['active_multipliers'] ?? [];

		CCallAgentHelper::processCallAgentRewardPoints( $this->getId(), $objVoipDatabase, NULL, CRewardPointSetting::LEASE_GENERATED, $intUserId, ( array ) $arrmixActiveRewardPointMultipliers );

		$arrmixCallAgents	= Psi\Eos\Voip\CCallAgents::createService()->fetchSimpleCallAgentsWithManagerDetailsByEmployeeIds( [ $this->getEmployeeId(), CEmployee::ID_ALICIA_MOSS, CEmployee::ID_RYAN_TYSON, CEmployee::ID_TREVOR_RILEY ], $objVoipDatabase );
		$arrmixCallAgents	= rekeyArray( 'id', $arrmixCallAgents );
		$arrstrEmailAddress	= array_merge( array_keys( rekeyArray( 'call_agent_email', $arrmixCallAgents ) ), [ $arrmixCallAgents[$this->getId()]['call_agent_manager_email'] ] );

		$objSystemEmail  = new CSystemEmail();
		$objSystemEmail->setSystemEmailTypeId( CSystemEmailType::PS_EMPLOYEE );
		$objSystemEmail->setToEmailAddress( implode( ',', array_filter( $arrstrEmailAddress ) ) );
		$objSystemEmail->setBccEmailAddress( CSystemEmail::KRUCH_EMAIL_ADDRESS );
		$objSystemEmail->setSubject( 'Congratulations! Thanks to you, a new lease was just signed!' );

		$arrmixTemplateParameters	= [];

		$arrmixTemplateParameters['call_agent_name_first']	= $this->getNameFirst();
		$arrmixTemplateParameters['applicant_full_name']	= $objApplicationDataObject->m_objApplicant->getNameFirst() . ' ' . $objApplicationDataObject->m_objApplicant->getNameLast();
		$arrmixTemplateParameters['property_name']			= $objApplicationDataObject->m_objProperty->getPropertyName();
		$arrmixTemplateParameters['lease_start_date']		= $objApplicationDataObject->m_objApplication->getLeaseStartDate();
		$arrmixTemplateParameters['call_url']				= 'https://clientadmin' . CConfig::get( 'rwx_login_suffix' ) . '/?module=call_details-new&call_id=' . $objCall->getId();
		$arrmixTemplateParameters['call']					= $objCall;
		$arrmixTemplateParameters['logo_url']				= CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;

		$objRenderTemplate = $this->setRenderTemplate( 'support/calls/call_agents/congrats_call_agent_email.tpl', $arrmixTemplateParameters, NULL, false, false );
		$strHtmlEmailContent = $objRenderTemplate->nestedFetchTemplate( 'support/calls/call_agents/congrats_call_agent_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmail->setHtmlContent( $strHtmlEmailContent );

		if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
			return false;
		}

		return true;
	}

	public function setRenderTemplate( $strTemplate, $arrmixTemplateParameters, $strDefaultTemplate = NULL, $boolIsExitTags = true, $boolAssignTemplateData = true ) {

		$this->m_objRenderTemplate = new CRenderTemplate( $strTemplate, PATH_INTERFACES_CLIENT_ADMIN, false );

		if( false == is_null( $strDefaultTemplate ) ) {
			$this->m_objRenderTemplate->setDefaultTemplate( PATH_INTERFACES_CLIENT_ADMIN . $strDefaultTemplate );
		}

		if( true == $boolIsExitTags ) {
			$this->loadExitTags();
		}

		if( true == $boolAssignTemplateData ) {

			$arrmixTemplateData = ( array ) $this->getCommonTemplateData();
			foreach( $arrmixTemplateData as $strKey => $strValue ) {
				$this->m_objRenderTemplate->assign( $strKey, $strValue );
			}
		}

		if( true == valArr( $arrmixTemplateParameters ) ) {

			foreach( $arrmixTemplateParameters as $strKey => $strValue ) {
				$this->m_objRenderTemplate->assign( $strKey, $strValue );
			}
		}

		return $this->m_objRenderTemplate;
	}

	/**
	 * Actors methods.
	 */

	public function login( $intUserId, $objVoipDatabase, $objFreeswitchDatabase ) {
		$this->setCallAgentStatusTypeId( ( CDepartment::TECHNICAL_SUPPORT == $this->getDepartmentId() || CDepartment::CALL_CENTER == $this->getDepartmentId() ) ? CCallAgentStatusType::AVAILABLE : CCallAgentStatusType::AVAILABLE_ON_DEMAND );
		$this->setCallAgentStateTypeId( CCallAgentStateType::WAITING );
		$this->setCallQueueId( NULL );

		$objAgent = $this->fetchAgent( $objFreeswitchDatabase );

		if( false == valObj( $objAgent, 'CAgent' ) ) {
			return false;
		}

		$objAgent->resetAgentStats();
		$objAgent->setContact( $this->getBuildCallAgentContact() );

		if( CDepartment::CALL_CENTER == $this->getDepartmentId() || CDepartment::TECHNICAL_SUPPORT == $this->getDepartmentId() ) {
			$objAgent->setWrapUpTime( CAgent::WRAP_UP_TIME );
			$objAgent->setStatus( CAgent::STATUS_AVAILABLE );
		} else {
			$objAgent->setStatus( CAgent::STATUS_AVAILABLE_ON_DEMAND );
			$objAgent->setWrapUpTime( 0 );
		}

		$objAgent->setState( CAgent::STATE_WAITING );

		if( false == $this->update( $intUserId, $objVoipDatabase ) || false == $objAgent->update( $intUserId, $objFreeswitchDatabase ) ) {
			return false;
		}

		return true;
	}

	public function loggedOut() {
		$this->setCallAgentStatusTypeId( CCallAgentStatusType::LOGGED_OUT );
		$this->setCallAgentStateTypeId( CCallAgentStateType::IDLE );
		$this->setNextCallAgentStateTypeId( NULL );
		$this->setNextCallAgentStatusTypeId( NULL );
		$this->setLastOfferedCallId( NULL );
		$this->setOfferedCallQueueId( NULL );
		$this->setPhysicalPhoneExtensionId( NULL );
		$this->setNextCallAgentStateTypeId( NULL );
		$this->setNextCallAgentStatusTypeId( NULL );
		$this->setRejectDelayTime( NULL );
		$this->setNoAnswerDelayTime( NULL );
		$this->setLastBridgeStart( NULL );
		$this->setLastBridgeEnd( NULL );
		$this->setLastOfferedCall( NULL );
		$this->setNoAnswerCount( NULL );
		$this->setTalkTime( NULL );

		return;
	}

	public function availableOnDemand( $objVoipDatabase ) {
		$this->setCallAgentStatusTypeId( CCallAgentStatusType::AVAILABLE_ON_DEMAND );
		$this->setCallAgentStateTypeId( CCallAgentStateType::WAITING );

		if( false == $this->update( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

	public function available( $objVoipDatabase ) {
		$this->setCallAgentStatusTypeId( CCallAgentStatusType::AVAILABLE );
		$this->setCallAgentStateTypeId( CCallAgentStateType::WAITING );

		if( false == $this->update( CUser::ID_SYSTEM, $objVoipDatabase ) ) {
			return false;
		}

		return true;
	}

	public function isRecordOutboundCall() {
		return ( 1 == $this->getIsRecordOutboundCall() ? 'true' : 'false' );
	}

	public function isLoggedOut() {
		return ( CCallAgentStatusType::LOGGED_OUT == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isAvailable() {
		return ( CCallAgentStatusType::AVAILABLE == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isAvailableOnDemand() {
		return ( CCallAgentStatusType::AVAILABLE_ON_DEMAND == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnBreak() {
		return ( CCallAgentStatusType::ON_BREAK == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnLunch() {
		return ( CCallAgentStatusType::ON_LUNCH == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnProject() {
		return ( CCallAgentStatusType::ON_PROJECT == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnMeeting() {
		return ( CCallAgentStatusType::ON_MEETING == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnTraining() {
		return ( CCallAgentStatusType::ON_TRAINING == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isInLeasingChat() {
		return ( CCallAgentStatusType::IN_LEASING_CHAT == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isOnCoaching() {
		return ( CCallAgentStatusType::ON_COACHING == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isInEmailQueue() {
		return ( CCallAgentStatusType::IN_EMAIL_QUEUE == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function isIdle() {
		return ( CCallAgentStateType::IDLE == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isWaiting() {
		return ( CCallAgentStateType::WAITING == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isReceiving() {
		return ( CCallAgentStateType::RECEIVING == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isInAQueueCall() {
		return ( CCallAgentStateType::IN_A_QUEUE_CALL == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isRemoved() {
		return ( CCallAgentStateType::REMOVED == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isUnused() {
		return ( CCallAgentStateType::UNUSED == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isNoAnswer() {
		return ( CCallAgentStateType::NO_ANSWER == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isUserBusy() {
		return ( CCallAgentStateType::USER_BUSY == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isMidnightBridge() {
		return ( CCallAgentStateType::MIDNIGHT_BRIDGE == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isDialing() {
		return ( CCallAgentStateType::DIALING == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function isInAOutboundCall() {
		return ( CCallAgentStateType::IN_A_OUTBOUND_CALL == $this->getCallAgentStateTypeId() ) ? true : false;
	}

	public function sendRequestForCallAgentShadow( CCallAgent $objCallAgent ) {

		if( false == valId( $objCallAgent->getPhysicalPhoneExtensionId() ) ) {
			return false;
		}

		$arrstrOptions[] = 'contact_stream=' . $objCallAgent->getContact();

		$arrobjXmlRpcValues = [
			new PhpXmlRpc\Value( 'originate', 'string' ),
			new PhpXmlRpc\Value( '{' . implode( ',', $arrstrOptions ) . '}user/' . $this->getPhysicalPhoneExtensionId() . ' ' . 'route_call_to_shadow_agent XML internal', 'string' ),
		];

		$objXmlRpcClient  = new PhpXmlRpc\Client( '/RPC2', CFreeswitchFactory::getFreeswitchConfig()['url'], 8080 );
		$objXmlRpcMessage = new PhpXmlRpc\Request( 'freeswitch.api', $arrobjXmlRpcValues );

		$objXmlRpcClient->setCredentials( CFreeswitchFactory::getFreeswitchConfig()['username'], CFreeswitchFactory::getFreeswitchConfig()['password'], 1 );

		return $objXmlRpcClient->send( $objXmlRpcMessage );
	}

	public function isHrApprovedOff() {
		return ( CCallAgentStatusType::HR_APPROVED_OFF == $this->getCallAgentStatusTypeId() ) ? true : false;
	}

	public function createStoreProductTag() {
		$objStoreProductTag = new CStoreProductTag();
		$objStoreProductTag->setCallAgentId( $this->getId() );
		$objStoreProductTag->setIsCallAgentGoal( 1 );
		return $objStoreProductTag;
	}

	public function releaseStoreProductTag( $intStoreProductId, $intUserId, $objVoipDatabase ) {
		if( false == valId( $intStoreProductId ) ) {
			return true;
		}

		$objStoreProductTag = \Psi\Eos\Voip\CStoreProductTags::createService()->fetchActiveStoreProductTagByStoreProductIdByCallAgentId( $intStoreProductId, $this->getId(), $objVoipDatabase );

		if( true == valObj( $objStoreProductTag, 'CStoreProductTag' ) ) {
			$objStoreProductTag->setReleasedBy( $intUserId );
			$objStoreProductTag->setReleasedOn( 'NOW()' );

			if( false == $objStoreProductTag->validate( 'validate_store_product_tag_update' ) || false == $objStoreProductTag->update( $intUserId, $objVoipDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function isCoveringAShift( $objVoipDatabase ) : bool {
		$strSql = 'SELECT
						call_agent_id,
						covered_by,
						begin_datetime,
						end_datetime,
						is_coverage_required,
						approved_by,
						approved_on
					FROM
						call_agent_coverages
					WHERE
						covered_by = ' . $this->getId() . '
						AND begin_datetime <= NOW() 
						AND end_datetime >= NOW()
						AND is_coverage_required IS TRUE
						AND approved_by IS NOT NULL
						AND approved_on IS NOT NULL
						AND denied_by IS NULL
						AND denied_on IS NULL
					LIMIT 1';

		return valArr( fetchData( $strSql, $objVoipDatabase ) );
	}

	public function getOfficeLocation() : string {
		switch( true ) {
			case in_array( $this->getOfficeId(), Coffice::$c_arrintIndiaOfficeIds ):
				return 'IN';
			case in_array( $this->getOfficeId(), Coffice::$c_arrintUSOfficeIds ):
				return 'US';
			default:
				return '--';
		}
	}

}
?>