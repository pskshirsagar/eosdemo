<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CPropertyIvrs
 * Do not add any new functions to this class.
 */

class CPropertyIvrs extends CBasePropertyIvrs {

	public static function fetchPropertyIvrsByIvrIdByPropertyIdsByCid( $intIvrId, $arrintPropertyIds, $intCid, $objVoipDatabase ) {
		if( false == valId( $intIvrId ) || false == valArr( $arrintPropertyIds ) || false == valId( $intCid ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						property_ivrs
					WHERE
						ivr_id  = ' . ( int ) $intIvrId . '
						AND cid = ' . ( int ) $intCid . '
						AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )';

		return self::fetchPropertyIvrs( $strSql, $objVoipDatabase );
	}

	public static function fetchSimplePropertyIvrsByCid( $intCid, $objVoipDatabase, $intIvrId = NULL ) {
		if( false == valId( $intCid ) ) return NULL;

		if( true == valId( $intIvrId ) ) {
			$strIvrIdWhereClause = ' AND ivr_id <> ' . ( int ) $intIvrId;
		}

		$strSql = 'SELECT
						property_id
					FROM
						property_ivrs
					WHERE
						cid = ' . ( int ) $intCid . $strIvrIdWhereClause;

		$arrmixPropertyIds = fetchData( $strSql, $objVoipDatabase );

		if( true == valArr( $arrmixPropertyIds ) ) {
			foreach( $arrmixPropertyIds as $arrintPropertyId ) {
				$arrintPropertyIds[] = $arrintPropertyId['property_id'];
			}

			return $arrintPropertyIds;
		} else {
			return NULL;
		}
	}

}
?>