<?php

class CCallTranscription extends CBaseCallTranscription {

	protected $m_intLeadProbability;
	protected $m_intVendorProbability;
	protected $m_intSupportProbability;
	protected $m_intResidentProbability;
	protected $m_intAbandonedProbability;
	protected $m_intSolicitorProbability;
	protected $m_intWorkOrderProbability;
	protected $m_intWrongNumberProbability;
	protected $m_intMarkerSurveyProbability;

	const TRANSCRIBED_CALLS					= 1;
	const NON_TRANSCRIBED_CALLS				= 2;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getLeadProbability() {
		return $this->m_intLeadProbability;
	}

	public function getVendorProbability() {
		return $this->m_intVendorProbability;
	}

	public function getSupportProbability() {
		return $this->m_intSupportProbability;
	}

	public function getResidentProbability() {
		return $this->m_intResidentProbability;
	}

	public function getAbandonedProbability() {
		return $this->m_intAbandonedProbability;
	}

	public function getSolicitorProbability() {
		return $this->m_intSolicitorProbability;
	}

	public function getWorkOrderProbability() {
		return $this->m_intWorkOrderProbability;
	}

	public function getWrongNumberProbability() {
		return $this->m_intWrongNumberProbability;
	}

	public function getMarketSurveyProbability() {
		return $this->m_intMarkerSurveyProbability;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lead_probability'] ) )					$this->setLeadProbability( $arrmixValues['lead_probability'] );
		if( true == isset( $arrmixValues['vendor_probability'] ) )					$this->setVendorProbability( $arrmixValues['vendor_probability'] );
		if( true == isset( $arrmixValues['support_probability'] ) )					$this->setSupportProbability( $arrmixValues['support_probability'] );
		if( true == isset( $arrmixValues['resident_probability'] ) )				$this->setResidentProbability( $arrmixValues['resident_probability'] );
		if( true == isset( $arrmixValues['abandoned_probability'] ) )				$this->setAbandonedProbability( $arrmixValues['abandoned_probability'] );
		if( true == isset( $arrmixValues['solicitor_probability'] ) )				$this->setSolicitorProbability( $arrmixValues['solicitor_probability'] );
		if( true == isset( $arrmixValues['work_order_probability'] ) )				$this->setWorkOrderProbability( $arrmixValues['work_order_probability'] );
		if( true == isset( $arrmixValues['wrong_number_probability'] ) )			$this->setWrongNumberProbability( $arrmixValues['wrong_number_probability'] );
		if( true == isset( $arrmixValues['market_survey_probability'] ) )			$this->setMarkerSurveyProbability( $arrmixValues['market_survey_probability'] );

		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function setLeadProbability( $intLeadProbability ) {
		$this->m_intLeadProbability = $intLeadProbability;
	}

	public function setVendorProbability( $intVendorProbability ) {
		$this->m_intVendorProbability = $intVendorProbability;
	}

	public function setSupportProbability( $intSupportProbability ) {
		$this->m_intSupportProbability = $intSupportProbability;
	}

	public function setResidentProbability( $intResidentProbability ) {
		$this->m_intResidentProbability = $intResidentProbability;
	}

	public function setAbandonedProbability( $intAbandonedProbability ) {
		$this->m_intAbandonedProbability = $intAbandonedProbability;
	}

	public function setSolicitorProbability( $intSolicitorProbability ) {
		$this->m_intSolicitorProbability = $intSolicitorProbability;
	}

	public function setWorkOrderProbability( $intWorkOrderProbability ) {
		$this->m_intWorkOrderProbability = $intWorkOrderProbability;
	}

	public function setWrongNumberProbability( $intWrongNumberProbability ) {
		$this->m_intWrongNumberProbability = $intWrongNumberProbability;
	}

	public function setMarkerSurveyProbability( $intMarkerSurveyProbability ) {
		$this->m_intMarkerSurveyProbability = $intMarkerSurveyProbability;
	}

}
?>