<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CInboundCallResponseTypes
 * Do not add any new functions to this class.
 */

class CInboundCallResponseTypes extends CBaseInboundCallResponseTypes {

	/**
	 * Fetch Functions
	 */

	public static function fetchInboundCallResponseTypes( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInboundCallResponseType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchInboundCallResponseType( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInboundCallResponseType', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>