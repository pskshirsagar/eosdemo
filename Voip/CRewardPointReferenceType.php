<?php

class CRewardPointReferenceType extends CBaseRewardPointReferenceType {

	const REWARD_POINT_SETTING			= 1;
	const CALL_AGENT_PIN_TYPE			= 2;
	const CALL_AGENT_CHALLENGE			= 3;
	const CALL_AGENT_REDEEMED_PRODUCT	= 4;
	const REWARD_POINT_MULTIPLIER		= 5;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>