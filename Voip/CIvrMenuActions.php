<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CIvrMenuActions
 * Do not add any new functions to this class.
 */

class CIvrMenuActions extends CBaseIvrMenuActions {

	public static function fetchIvrMenuActionsByCidByIvrIdByIvrMenuTypeId( $intCid, $intIvrId, $intIvrMenuTypeId, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intIvrId ) || false == valId( $intIvrMenuTypeId ) ) return NULL;

		$strSql = ' SELECT
						ima.*,
						ia.name AS ivr_action_name,
						ia.description AS ivr_action_description
					FROM
						ivr_menu_actions AS ima
						JOIN ivr_menus AS im ON ( ima.ivr_menu_id = im.id AND ima.cid = im.cid )
						JOIN ivr_actions AS ia ON ( ima.ivr_action_id = ia.id )
					WHERE
						im.cid = ' . ( int ) $intCid . '
						AND im.ivr_id = ' . ( int ) $intIvrId . '
						AND im.ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . '
					ORDER BY
						ima.ivr_menu_id,
						ima.action_key';

		return self::fetchIvrMenuActions( $strSql, $objVoipDatabase );
	}

	public static function fetchIvrMenuActionsByIdByCid( $intIvrMenuActionId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intIvrMenuActionId ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						ima.*
					FROM
						ivr_menu_actions AS ima
					WHERE
						ima.ivr_menu_id = (
							SELECT
								sub_ivr_menu_id
							FROM
								ivr_menu_actions
							WHERE
								id = ' . ( int ) $intIvrMenuActionId . '
								AND cid = ' . ( int ) $intCid . '
						)
						OR ima.id = ' . ( int ) $intIvrMenuActionId . '
						AND ima.cid = ' . ( int ) $intCid . '
					ORDER BY
						ima.id';

		return self::fetchIvrMenuActions( $strSql, $objVoipDatabase );
	}

	public static function fetchIvrMenuActionsByIvrIdByIvrMenuTypeIdByCidByIsOverrideAfterOfficeHours( $intIvrId, $intIvrMenuTypeId, $intCid, $boolIsOverrideAfterOfficeHours, $objVoipDatabase ) {
		if( true == $boolIsOverrideAfterOfficeHours ) {
			$intIvrMenuTypeId = CIvrMenuType::DURING_OFFICE_HOURS;
		}

		$strSql = 'SELECT
						ima.*,
						cf.file_name AS greeting_file_name,
						cf.file_path AS greeting_file_path
					FROM
						ivr_menu_actions AS ima
						JOIN ivr_menus AS im ON ( im.id = ima.ivr_menu_id )
						JOIN ivrs AS i ON ( i.id = im.ivr_id )
						JOIN ivr_menu_types AS imt ON ( imt.id = im.ivr_menu_type_id )
						JOIN greetings AS g ON ( g.id = ima.greeting_id AND g.cid = i.cid )
						JOIN call_files AS cf ON ( cf.id = g.call_file_id )
					WHERE
						i.id = ' . ( int ) $intIvrId . '
						AND im.ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . '
						AND g.greeting_type_id = ' . CGreetingType::IVR . '
						AND im.cid = ' . ( int ) $intCid . '
					ORDER BY
						ima.action_key ASC';

		return self::fetchIvrMenuActions( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleIvrMenuActionsByIvrIdByIvrMenuTypeId( $intIvrId, $intIvrMenuTypeId, $objVoipDatabase ) {
		if( false == valId( $intIvrId ) || false == valId( $intIvrMenuTypeId ) ) return NULL;

		$strSql = 'SELECT
						im.id,
						im.ivr_menu_type_id,
						im.ivr_menu_id,
						im.name,
						ima.id AS ivr_menu_action_id,
						ima.ivr_menu_id AS ivr_menu_action_ivr_menu_id,
						ima.sub_ivr_menu_id,
						ima.ivr_action_id,
						ima.greeting_id,
						ima.action_key,
						ima.name AS ivr_menu_action_name,
						ima.action_parameter,
						cf.id AS call_file_id
					FROM
						ivr_menus AS im
						JOIN ivr_menu_actions AS ima ON ( ( im.cid = ima.cid ) AND ( im.id = ima.ivr_menu_id ) )
						JOIN greetings AS g ON ( ima.cid = g.cid AND ima.greeting_id = g.id )
						JOIN call_files AS cf ON ( g.call_file_id = cf.id )
					WHERE
						im.ivr_id = ' . ( int ) $intIvrId . '
						AND im.ivr_menu_type_id = ' . ( int ) $intIvrMenuTypeId . '
					ORDER BY
						ima.action_key,im.ivr_menu_type_id ASC';

		return fetchData( $strSql, $objVoipDatabase );
	}

}
?>