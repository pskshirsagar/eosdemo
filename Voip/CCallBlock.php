<?php

class CCallBlock extends CBaseCallBlock {

	public function valValidPhoneNumber() {
		$boolIsValid = true;
		if( true == is_null( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Phone Number is Required.' ) ) );
			$boolIsValid = false;
		} elseif( 10 != strlen( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Please enter valid phone number.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valReason() {
		$boolIsValid = true;
		if( true == is_null( $this->getReason() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Reason is Required.' ) ) );
			$boolIsValid = false;
		} elseif( 255 <= strlen( $this->getReason() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Only 255 character allow.' ) ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valValidPhoneNumber();
				$boolIsValid &= $this->valReason();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
        }

		return $boolIsValid;
	}

}
?>