<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallPhoneNumbers
 * Do not add any new functions to this class.
 */

class CCallPhoneNumbers extends CBaseCallPhoneNumbers {

	public static function fetchCallPhoneNumbersByIds( $arrintCallPhoneNumbersIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallPhoneNumbersIds ) ) return NULL;
		$strSql	= 'SELECT * FROM call_phone_numbers  WHERE id IN ( ' . implode( ',', $arrintCallPhoneNumbersIds ) . ' ) AND released_on IS NULL';
		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumberByPropertyIdById( $intPropertyId, $intCallPhoneNumberId, $objVoipDatabase ) {
		$strSql	= 'SELECT * FROM call_phone_numbers  WHERE id = ' . ( int ) $intCallPhoneNumberId . ' AND released_on IS NULL AND property_id = ' . ( int ) $intPropertyId;
		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumberByPhoneNumber( $strPhoneNumber, $objVoipDatabase ) {
		if( true == is_null( $strPhoneNumber ) || 0 == strlen( $strPhoneNumber ) ) return NULL;

		$strSql = 'SELECT
						cpn.*,
						cq.name AS call_queue_name,
						cq.queue_phone_extension_id,
						cq.domain AS call_queue_domain,
						i.name AS ivr_name,
						i.is_override_after_office_hours AS is_override_after_office_hours
					FROM
						call_phone_numbers AS cpn
						LEFT JOIN call_queues AS cq ON ( cq.id = cpn.call_queue_id )
						LEFT JOIN ivrs AS i ON ( i.id = cpn.ivr_id )
					WHERE
						phone_number = \'' . addslashes( $strPhoneNumber ) . '\'
						AND released_on IS NULL
					ORDER BY
						id
					LIMIT 1';

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumberByPhoneNumbersByCallTypeId( $arrstrPhoneNumber, $objVoipDatabase, $intCallTypeId = NULL ) {
		if( false == valArr( $arrstrPhoneNumber ) ) return NULL;

		$strSql = 'SELECT * FROM call_phone_numbers WHERE phone_number IN ( \'' . implode( '\',\'', $arrstrPhoneNumber ) . '\' ) AND released_on IS NULL';

		if( false == is_null( $intCallTypeId ) ) $strSql .= ' AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByPropertyId( $intPropertyId, $objVoipDatabase ) {
		$strSql	= 'SELECT
						DISTINCT( cpn.* ),
						ct.name AS call_type,
						i.name AS ivr_name
					FROM
						call_phone_numbers AS cpn
						JOIN call_types AS ct ON ( ct.id = cpn.call_type_id )
						LEFT JOIN ivrs AS i ON ( i.id = cpn.ivr_id )
					WHERE
						cpn.property_id = ' . ( int ) $intPropertyId . '
						AND ct.id = cpn.call_type_id
						AND cpn.released_on IS NULL
						AND cpn.suspended_on IS NULL
					ORDER BY
						cpn.id DESC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByCidByCallTypeId( $intCid, $intCallTypeId, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valId( $intCallTypeId ) ) {
			return NULL;
		}

		$strSql	= 'SELECT
						DISTINCT( cpn.* ),
						ct.name AS call_type
					FROM
						call_phone_numbers AS cpn
						JOIN call_types AS ct ON ( ct.id = cpn.call_type_id )
					WHERE
						cpn.cid = ' . ( int ) $intCid . '
						AND cpn.call_type_id = ' . ( int ) $intCallTypeId . '
						AND cpn.released_on IS NULL
						AND cpn.suspended_on IS NULL
					ORDER BY
						cpn.id DESC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchValidCallPhoneNumbersByPropertyId( $intPropertyId, $arrintExcludedCallPhoneNumberIds, $arrintVanityNumberTypeIds, $objVoipDatabase, $boolIsVerified = true, $boolIsSuspended = true, $boolIsDeleted = true ) {
		$strSql	= 'SELECT
						DISTINCT( cpn.* )
					FROM
						call_phone_numbers cpn
					WHERE
						cpn.property_id = ' . ( int ) $intPropertyId . '
						AND cpn.released_on IS NULL ';

 		$strSql	.= ( true == $boolIsVerified ) ? ' AND cpn.is_verified = true' : '';
		$strSql	.= ( false == $boolIsSuspended ) ? ' AND cpn.suspended_on IS NULL' : '';
		$strSql	.= ( true == valArr( $arrintExcludedCallPhoneNumberIds ) ) ? ' AND id NOT IN (' . implode( ', ', $arrintExcludedCallPhoneNumberIds ) . ') ' : '';
		$strSql	.= ( true == valArr( $arrintVanityNumberTypeIds ) ) ? ' AND cpn.call_type_id IN (' . implode( ', ', $arrintVanityNumberTypeIds ) . ') ' : '';
		$strSql	.= ( false == $boolIsDeleted ) ? ' AND cpn.deleted_on IS NULL ' : '';
		$strSql	.= ' ORDER BY
						cpn.id DESC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchPaginatedCallPhoneNumbersByCallPhoneNumberFilter( $intPageNo, $intPageSize, $objCallPhoneNumberFilter, $objVoipDatabase, $boolCountOnly = false ) {
		$arrstrWhere	= array();
		$strSubSql		= NULL;

		if( true == $boolCountOnly ) {
			$strSelectSql = 'COUNT(cpn.id) ';
		} else {
			$strSelectSql = '
							cpn.phone_number,
							cpn.id,
							cpn.cid,
							cpn.property_id,
							cpn.suspended_by,
							cpn.released_by,
							cpn.deleted_by,
							ct.name AS call_type,
							cpn.is_toll_free,
							csp.name AS call_service_provider_name,
							pcs.property_name AS property_name,
							c.company_name AS company_name,
							pe.extension AS extension,
							cpn.monthly_fee,
							cq.name AS call_queue,
							ca.full_name AS employee_name,
							cpn.employee_id,
							cpn.country_code,
							cpn.call_type_id';
		}
		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$strSql = 'SELECT ' . $strSelectSql . '
					FROM
						call_phone_numbers AS cpn
						LEFT JOIN clients as c ON cpn.cid = c.id
						LEFT JOIN call_service_providers AS csp ON cpn.call_service_provider_id = csp.id
						LEFT JOIN property_call_settings AS pcs ON cpn.property_id = pcs.property_id AND pcs.cid = c.id
						LEFT JOIN call_types AS ct ON cpn.call_type_id = ct.id
						LEFT JOIN phone_extensions AS pe ON cpn.phone_extension_id = pe.id
						LEFT JOIN call_queues AS cq ON cpn.call_queue_id = cq.id
						LEFT JOIN call_agents AS ca ON cpn.employee_id = ca.employee_id
						LEFT JOIN countries AS ctr ON ctr.code = cpn.country_code';

		$strOrderBy 	= ' ORDER BY cpn.phone_number, ct.name ASC, csp.name ASC';
		$strOrderByType = 'DESC';

		if( false == is_null( $objCallPhoneNumberFilter->getOrderByType() ) && 1 == $objCallPhoneNumberFilter->getOrderByType() ) {
			$strOrderByType = 'ASC';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getOrderByField() ) && 0 < strlen( trim( $objCallPhoneNumberFilter->getOrderByField() ) ) ) {
			$strOrderBy = ' ORDER BY cpn.' . addslashes( $objCallPhoneNumberFilter->getOrderByField() ) . ' ' . $strOrderByType;
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCid() ) ) {
			$arrstrWhere[] = 'cpn.cid = ' . ( int ) $objCallPhoneNumberFilter->getCid();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCids() ) ) {
			$arrstrWhere[] = 'cpn.cid IN ( ' . implode( ', ', array_filter( explode( ',', $objCallPhoneNumberFilter->getCids() ) ) ) . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getClientIds() ) && true == valStr( $objCallPhoneNumberFilter->getClientIds() ) ) {
			$arrstrWhere[] = 'cpn.cid IN ( ' . implode( ', ', array_filter( explode( ',', $objCallPhoneNumberFilter->getClientIds() ) ) ) . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPortalType() ) && 0 < strlen( $objCallPhoneNumberFilter->getPortalType() ) ) {
			$arrstrWhere[] = 'cpn.call_type_id = ' . ( int ) $objCallPhoneNumberFilter->getPortalType();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCallTypeIds() ) && true == valStr( $objCallPhoneNumberFilter->getCallTypeIds() ) ) {
			$arrstrWhere[] = 'cpn.call_type_id IN ( ' . $objCallPhoneNumberFilter->getCallTypeIds() . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPropertyId() ) ) {
			$arrstrWhere[] = 'cpn.property_id = ' . ( int ) $objCallPhoneNumberFilter->getPropertyId();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPropertyIds() ) && true == valStr( $objCallPhoneNumberFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'cpn.property_id IN ( ' . $objCallPhoneNumberFilter->getPropertyIds() . ' ) ';
		}

		if( 2 != $objCallPhoneNumberFilter->getIsVerified() && false == is_null( $objCallPhoneNumberFilter->getIsVerified() ) ) {
			if( 1 == $objCallPhoneNumberFilter->getIsVerified() ) {
				$arrstrWhere[] = 'cpn.is_verified = true';
			} else {
				$arrstrWhere[] = 'cpn.is_verified = false';
			}
		}

		if( 2 != $objCallPhoneNumberFilter->getIsTollFree() && false == is_null( $objCallPhoneNumberFilter->getIsTollFree() ) ) {
			if( 1 == $objCallPhoneNumberFilter->getIsTollFree() ) {
				$arrstrWhere[] = 'cpn.is_toll_free = true';
			} else {
				$arrstrWhere[] = 'cpn.is_toll_free = false';
			}
		}

		if( false == is_null( $objCallPhoneNumberFilter->getReleasedBy() ) && 1 == $objCallPhoneNumberFilter->getReleasedBy() ) {
			$arrstrWhere[] = 'cpn.released_by IS NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getReleasedBy() ) && 0 == $objCallPhoneNumberFilter->getReleasedBy() && true == valStr( $objCallPhoneNumberFilter->getReleasedBy() ) ) {
			$arrstrWhere[] = 'cpn.released_by IS NOT NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getDeletedBy() ) && 1 == $objCallPhoneNumberFilter->getDeletedBy() && true == valStr( $objCallPhoneNumberFilter->getDeletedBy() ) ) {
			$arrstrWhere[] = 'cpn.deleted_by IS NOT NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getDeletedBy() ) && 0 == $objCallPhoneNumberFilter->getDeletedBy() && true == valStr( $objCallPhoneNumberFilter->getDeletedBy() ) ) {
			$arrstrWhere[] = 'cpn.deleted_by IS NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCallServiceProviderId() ) ) {
			$arrstrWhere[] = 'cpn.call_service_provider_id = ' . ( int ) $objCallPhoneNumberFilter->getCallServiceProviderId();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCompanyStatusTypeIds() ) ) {
			$arrstrWhere[] = 'c.company_status_type_id IN ( \'' . implode( '\', \'', explode( ',', $objCallPhoneNumberFilter->getCompanyStatusTypeIds() ) ) . '\' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCountryCodes() ) ) {
			$arrstrWhere[] = 'cpn.country_code IN ( \'' . implode( '\', \'', explode( ',', $objCallPhoneNumberFilter->getCountryCodes() ) ) . '\' ) ';
		}

		$intPhoneNumber = trim( $objCallPhoneNumberFilter->getPhoneNumber() );
		if( false == empty( $intPhoneNumber ) ) {
			$strPhoneNumber = preg_replace( '/[^A-Za-z0-9]/', '', $objCallPhoneNumberFilter->getPhoneNumber() );
			$arrstrWhere[] = 'cpn.phone_number LIKE \'%' . addslashes( $strPhoneNumber ) . '%\' ';
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSubSql  = implode( ' AND ', $arrstrWhere );
		}

		if( false == is_null( $strSubSql ) ) {
			$strSql .= ' WHERE ' . $strSubSql;
		}

		if( true == $boolCountOnly ) {
			$arrintResponse = fetchData( $strSql, $objVoipDatabase );
			return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
		}

		$strSql .= $strOrderBy . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return fetchData( $strSql, $objVoipDatabase );

	}

	public static function fetchCallPhoneNumbersByCallPhoneNumberFilter( $objCallPhoneNumberFilter, $objVoipDatabase ) {
		if( false == valObj( $objCallPhoneNumberFilter, 'CCallPhoneNumberFilter' ) ) return NULL;

		$arrstrWhere	= array();
		$strSubSql		= NULL;
		$strOrderByType = 'DESC';
		$strSql			= 'SELECT
							DISTINCT
							ON ( cpn.phone_number ) cpn.phone_number,
							cpn.id,
							cpn.cid,
							cpn.property_id,
							ct.name AS call_type,
							cpn.is_toll_free,
							csp.name AS call_service_provider_name,
							pcs.property_name AS property_name,
							c.company_name AS company_name
						FROM
							call_phone_numbers AS cpn
							LEFT JOIN clients as c ON cpn.cid = c.id
							LEFT JOIN call_service_providers AS csp ON cpn.call_service_provider_id = csp.id
							LEFT JOIN property_call_settings AS pcs ON cpn.property_id = pcs.property_id
							LEFT JOIN call_types AS ct ON cpn.call_type_id = ct.id';
		$strOrderBy = ' ORDER BY cpn.phone_number, c.company_name ASC, pcs.property_name ASC';

		if( false == is_null( $objCallPhoneNumberFilter->getOrderByType() ) && 1 == $objCallPhoneNumberFilter->getOrderByType() ) {
			$strOrderByType = 'ASC';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getOrderByField() ) && 0 < strlen( trim( $objCallPhoneNumberFilter->getOrderByField() ) ) ) {
			$strOrderBy = ' ORDER BY cpn.' . addslashes( $objCallPhoneNumberFilter->getOrderByField() ) . ' ' . $strOrderByType;
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCid() ) ) {
			$arrstrWhere[] = 'cpn.cid = ' . ( int ) $objCallPhoneNumberFilter->getCid();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCids() ) && true == valStr( $objCallPhoneNumberFilter->getCids() ) ) {
			$arrstrWhere[] = 'cpn.cid IN ( ' . $objCallPhoneNumberFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPortalType() ) && 0 < strlen( $objCallPhoneNumberFilter->getPortalType() ) ) {
			$arrstrWhere[] = 'cpn.call_type_id = ' . ( int ) $objCallPhoneNumberFilter->getPortalType();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCallTypeIds() ) && true == valStr( $objCallPhoneNumberFilter->getCallTypeIds() ) ) {
			$arrstrWhere[] = 'cpn.call_type_id IN ( ' . $objCallPhoneNumberFilter->getCallTypeIds() . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPropertyId() ) ) {
			$arrstrWhere[] = 'cpn.property_id = ' . ( int ) $objCallPhoneNumberFilter->getPropertyId();
		}

		if( false == is_null( $objCallPhoneNumberFilter->getPropertyIds() ) && true == valStr( $objCallPhoneNumberFilter->getPropertyIds() ) ) {
			$arrstrWhere[] = 'cpn.property_id IN ( ' . $objCallPhoneNumberFilter->getPropertyIds() . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getIsVerified() ) && 2 != $objCallPhoneNumberFilter->getIsVerified() ) {
			if( 1 == $objCallPhoneNumberFilter->getIsVerified() || true == $objCallPhoneNumberFilter->getIsVerified() ) {
				$arrstrWhere[] = 'cpn.is_verified = true';
			} else {
				$arrstrWhere[] = 'cpn.is_verified = false';
			}
		}

		if( true == valStr( $objCallPhoneNumberFilter->getIsTollFree() ) && 2 != $objCallPhoneNumberFilter->getIsTollFree() ) {
			if( 1 == $objCallPhoneNumberFilter->getIsTollFree() || true == $objCallPhoneNumberFilter->getIsTollFree() ) {
				$arrstrWhere[] = 'cpn.is_toll_free = true';
			} else {
				$arrstrWhere[] = 'cpn.is_toll_free = false';
			}
		}

		if( false == is_null( $objCallPhoneNumberFilter->getReleasedBy() ) && 1 == $objCallPhoneNumberFilter->getReleasedBy() ) {
			$arrstrWhere[] = 'cpn.released_by IS NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getReleasedBy() ) && 0 == $objCallPhoneNumberFilter->getReleasedBy() && true == valStr( $objCallPhoneNumberFilter->getReleasedBy() ) ) {
			$arrstrWhere[] = 'cpn.released_by IS NOT NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getDeletedBy() ) && 1 == $objCallPhoneNumberFilter->getDeletedBy() && true == valStr( $objCallPhoneNumberFilter->getDeletedBy() ) ) {
			$arrstrWhere[] = 'cpn.deleted_by IS NOT NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getDeletedBy() ) && 0 == $objCallPhoneNumberFilter->getDeletedBy() && true == valStr( $objCallPhoneNumberFilter->getDeletedBy() ) ) {
			$arrstrWhere[] = 'cpn.deleted_by IS NULL';
		}

		if( false == is_null( $objCallPhoneNumberFilter->getCallServiceProviderId() ) ) {
			$arrstrWhere[] = 'cpn.call_service_provider_id = ' . ( int ) $objCallPhoneNumberFilter->getCallServiceProviderId();
		}

		if( true == valArr( $arrstrWhere ) ) {
			$strSubSql  = implode( ' AND ', $arrstrWhere );
		}

		if( false == is_null( $strSubSql ) ) {
			$strSql .= ' WHERE ' . $strSubSql;
		}

		$strSql .= $strOrderBy;
		$strSql = 'SELECT * FROM
					( ' . $strSql . ' ) AS call_phone_number
					ORDER BY
						call_phone_number.company_name ASC,
						call_phone_number.property_name ASC,
						call_phone_number.call_type ASC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByPhoneNumbers( $arrstrPhoneNumbers, $objVoipDatabase, $objCallPhoneNumberFilter = NULL ) {
		if( false == valArr( $arrstrPhoneNumbers ) ) return NULL;

		$strWhereClause = '';

		if( false == is_null( $objCallPhoneNumberFilter ) && false == is_null( $objCallPhoneNumberFilter->getCid() ) ) {
			$strWhereClause .= ' AND cid = ' . ( int ) $objCallPhoneNumberFilter->getCid();
		}

		if( false == is_null( $objCallPhoneNumberFilter ) && true == valStr( $objCallPhoneNumberFilter->getCids() ) ) {
			$strWhereClause .= ' AND cid IN ( ' . $objCallPhoneNumberFilter->getCids() . ' ) ';
		}

		if( false == is_null( $objCallPhoneNumberFilter ) && false == is_null( $objCallPhoneNumberFilter->getPropertyId() ) ) {
			$strWhereClause .= ' AND property_id = ' . ( int ) $objCallPhoneNumberFilter->getPropertyId();
		}

		if( false == is_null( $objCallPhoneNumberFilter ) && true == valStr( $objCallPhoneNumberFilter->getPropertyIds() ) ) {
			$strWhereClause .= ' AND property_id IN ( ' . $objCallPhoneNumberFilter->getPropertyIds() . ' ) ';
		}

		$strSql = 'SELECT * FROM call_phone_numbers WHERE phone_number IN (\'' . implode( "','", $arrstrPhoneNumbers ) . '\') AND released_on IS NULL' . $strWhereClause;

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersPropertyIdsByCallTypeId( $arrintPropertyIds, $intCallTypeId, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql	= ' SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						deleted_on IS NULL
						AND deleted_by IS NULL
						AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchVerifiedCallPhoneNumberByCidPropertyId( $intCid, $intPropertyId, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) ) return NULL;

		$strSql	= 'SELECT
						cpn.phone_number
					FROM
						call_phone_numbers cpn
					WHERE
						cpn.cid = ' . ( int ) $intCid . '
						AND cpn.property_id = ' . ( int ) $intPropertyId . '
						AND cpn.call_type_id = ' . CCallType::RESIDENT_SUPPORT . '
						AND cpn.deleted_on IS NULL
						AND cpn.deleted_by IS NULL
						AND cpn.is_verified = true
						AND cpn.verified_on IS NOT NULL
						LIMIT 1;';

		$arrmixPhoneNumber = fetchData( $strSql, $objVoipDatabase );
		if( false == valArr( $arrmixPhoneNumber ) ) return NULL;
		$strVanityPhoneNumber = array_pop( $arrmixPhoneNumber );
		return $strVanityPhoneNumber['phone_number'];
	}

	public static function fetchCallPhoneNumberCountByCidByPropertyIdsByCallTypeIds( $intCid, $arrintPropertyIds, $arrintCallTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintPropertyIds ) || false == valArr( $arrintCallTypeIds ) ) return NULL;

		$strSql	= 'SELECT
						call_type_id,
						COUNT(call_type_id)
					FROM
						call_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' )
					GROUP BY
						call_type_id
					ORDER BY
						call_type_id';

		$arrmixPhoneNumberCounts = fetchData( $strSql, $objVoipDatabase );

		if( false == valArr( $arrmixPhoneNumberCounts ) ) return NULL;

		foreach( $arrmixPhoneNumberCounts as $arrmixPhoneNumberCount ) {
			$arrintPhoneNumberCounts[$arrmixPhoneNumberCount['call_type_id']] = $arrmixPhoneNumberCount['count'];
		}

		return $arrintPhoneNumberCounts;
	}

	public static function fetchSimpleCallPhoneNumbersByPropertyIdByCidByCallTypeIds( $intPropertyId, $intCid, $arrintCallTypeIds, $objVoipDatabase ) {

		if( false == valArr( $arrintCallTypeIds ) ) return false;

		$strSql = 'SELECT
						phone_number
					FROM
						call_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND released_on IS NULL
						AND suspended_on IS NULL
						AND deleted_on IS NULL
						AND call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ')';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchUnverifiedCallPhoneNumbers( $objVoipDatabase ) {
		return self::fetchCallPhoneNumbers( 'SELECT * FROM call_phone_numbers WHERE is_verified = false AND verified_on IS NULL AND suspended_on IS NULL AND released_on IS NULL', $objVoipDatabase );
	}

	public static function fetchActiveCallPhoneNumberByPhoneNumber( $strPhoneNumber, $objVoipDatabase ) {
		if( false == valStr( $strPhoneNumber ) ) return NULL;

		$strSql = 'SELECT
						cpn.*,
						cq.name AS call_queue_name,
						cq.queue_phone_extension_id,
						cq.domain AS call_queue_domain,
						i.name AS ivr_name,
						i.is_override_after_office_hours AS is_override_after_office_hours
					FROM
						call_phone_numbers AS cpn
						LEFT JOIN call_queues AS cq ON ( cq.id = cpn.call_queue_id )
						LEFT JOIN ivrs AS i ON ( i.id = cpn.ivr_id )
					WHERE
						phone_number = \'' . addslashes( $strPhoneNumber ) . '\'
						AND released_on IS NULL
						AND suspended_on IS NULL
					ORDER BY
						id
					LIMIT 1';

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByCallTypeIds( $arrintCallTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallTypeIds ) ) return;
		$strSql = 'SELECT * FROM call_phone_numbers WHERE call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' ) AND released_on IS NULL';
		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallPhoneNumbersByPhoneNumbers( $arrstrPhoneNumbers, $objVoipDatabase ) {
		if( false == valArr( $arrstrPhoneNumbers ) ) return;
		$strSql = 'SELECT id, phone_number FROM call_phone_numbers WHERE phone_number IN (\'' . implode( "','", $arrstrPhoneNumbers ) . '\') ';
		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchQuickSearchCallPhoneNumbers( $strSearchKeyword, $objVoipDatabase ) {
		$strSql = 'SELECT * FROM call_phone_numbers WHERE phone_number LIKE \'' . $strSearchKeyword . '%\' ORDER BY phone_number ASC LIMIT 10';
		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByCidByCallTypeIds( $intCid, $arrintCallTypeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallTypeIds ) || false == valId( $intCid ) ) return;
		$strSql = 'SELECT * FROM call_phone_numbers WHERE call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' ) AND released_on IS NULL AND cid =' . ( int ) $intCid;
		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveCallPhoneNumbersByCidByPropertyIdsByCallForwardPreferenceId( $intCid, $arrintPropertyIds, $intCallForwardPreferenceId, $objVoipDatabase ) {
		if( false == valId( $intCid ) || false == valArr( $arrintPropertyIds ) || false == valId( $intCallForwardPreferenceId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND	property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND call_forward_preference_id = ' . ( int ) $intCallForwardPreferenceId . '
						AND released_on IS NULL';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchAllCallPhoneNumbersByIds( $arrintCallPhoneNumbersIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallPhoneNumbersIds ) ) return NULL;

		$strSql	= 'SELECT DISTINCT
							ON ( cpn.phone_number ) cpn.phone_number,
							cpn.id,
							cpn.cid,
							cpn.property_id,
							ct.name AS call_type,
							cpn.is_toll_free,
							csp.name AS call_service_provider_name,
							pcs.property_name AS property_name,
							c.company_name AS company_name
					FROM
						call_phone_numbers AS cpn
							LEFT JOIN clients as c ON cpn.cid = c.id
							LEFT JOIN call_service_providers AS csp ON cpn.call_service_provider_id = csp.id
							LEFT JOIN property_call_settings AS pcs ON cpn.property_id = pcs.property_id
							LEFT JOIN call_types AS ct ON cpn.call_type_id = ct.id
					WHERE
						cpn.id IN ( ' . implode( ',', $arrintCallPhoneNumbersIds ) . ' )
						ORDER BY
							cpn.phone_number ASC,
							ct.name ASC,
							csp.name ASC';
		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveRandomTestCallPhoneNumberForTwilioByCallTypeIds( $arrintCallTypeIds, $objVoipDatabase ) {

		$strSql	= 'SELECT
						id,
						cid,
						property_id,
						phone_number,
						call_type_id,
						lead_routing_condition,
						maintenance_routing_condition,
						maintenance_emergency_routing_condition,
						is_office_forwarding,
						call_forward_preference_id,
						routing_condition
					FROM (
							SELECT
								cpn.id As id,
								cpn.cid As cid,
								cpn.property_id As property_id,
								cpn.phone_number AS phone_number,
								cpn.call_type_id AS call_type_id,
								pcs.lead_routing_condition AS lead_routing_condition,
								pcs.maintenance_routing_condition AS maintenance_routing_condition,
								pcs.maintenance_emergency_routing_condition AS maintenance_emergency_routing_condition,
								pcs.is_office_forwarding AS is_office_forwarding,
								cpn.call_forward_preference_id AS call_forward_preference_id,
								CASE
									WHEN cpn.call_type_id = ' . CCallType::CALL_TRACKER_LEAD . ' AND cpn.call_forward_preference_id = 1 AND pcs.lead_routing_condition = \'ALL\' THEN pcs.lead_routing_condition
									WHEN cpn.call_type_id = ' . CCallType::CALL_TRACKER_MAINTENANCE . ' AND cpn.call_forward_preference_id = 1 AND pcs.maintenance_routing_condition = \'ALL\' THEN pcs.maintenance_routing_condition
									WHEN cpn.call_type_id = ' . CCallType::CALL_TRACKER_MAINTENANCE_EMERGENCY . ' AND cpn.call_forward_preference_id = 1 AND pcs.maintenance_emergency_routing_condition = \'ALL\' THEN pcs.maintenance_emergency_routing_condition
									WHEN cpn.call_type_id = ' . CCallType::CALL_TRACKING_OFFICE . ' AND pcs.is_office_forwarding = 1 THEN \'ALL\'
								END AS routing_condition
							FROM
								call_phone_numbers cpn
								JOIN property_call_settings pcs ON (cpn.property_id = pcs.property_id )
								JOIN clients c ON ( c.id = cpn.cid AND c.company_status_type_id = ' . CCompanyStatusType::CLIENT . ' )
							WHERE
								cpn.call_type_id IN ( ' . implode( ',', $arrintCallTypeIds ) . ' )
								AND ( cpn.call_queue_id IS NULL OR cpn.call_queue_id IN ( ' . CCallQueue::LEASING_ENGLISH . ', ' . CCallQueue::LEASING_RESIDENT_ENGLISH . ' ) )
								AND cpn.released_on IS NULL
								AND cpn.suspended_on IS NULL
						) as subSql
					WHERE
						routing_condition = \'ALL\'
					ORDER BY
						RANDOM()
					LIMIT 1';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchNewlyPurchasedCallPhoneNumbers( $objVoipDatabase ) {
		$strSql = 'SELECT
						cpn.*,
						c.company_name,
						pcs.property_name
					FROM
						call_phone_numbers AS cpn
						LEFT JOIN clients as c ON cpn.cid = c.id
						LEFT JOIN property_call_settings AS pcs ON cpn.property_id = pcs.property_id
					WHERE
						cpn.created_on >= CURRENT_TIMESTAMP - interval \'24 hour\'
						AND cpn.call_service_provider_id IN ( ' . implode( ',', [ CCallServiceProvider::VERACITY, CCallServiceProvider::VITELITY ] ) . ' )
					ORDER BY
						cpn.created_on DESC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallPhoneNumbersByIds( $arrintCallPhoneNumbersIds, $objVoipDatabase ) {
		if( false == valArr( $arrintCallPhoneNumbersIds ) ) return NULL;
		$strSql	= 'SELECT
						id,
						phone_number
					FROM
						call_phone_numbers
					WHERE id IN ( ' . implode( ',', $arrintCallPhoneNumbersIds ) . ' ) ';
		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchSimpleCallPhoneNumbersByPhoneNumber( $strPhoneNumber, $objVoipDatabase ) {

		$strSql = 'SELECT
						phone_number,
						count( id ) AS total_call_phone_numbers_count,
						sum( CASE when suspended_on IS NOT NULL then 1 else 0 END ) AS suspended_call_phone_numbers_count,
						date_part( \'day\',( now()::timestamp - max( suspended_on::timestamp ) ) ) AS suspended_days
					FROM
						call_phone_numbers
					WHERE
						released_on is null
						AND phone_number like \'%' . $strPhoneNumber . '%\'
					GROUP BY
						phone_number';

		return ( array ) fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveCallPhoneNumberByPropertyIdByCidByCallTypeId( $intPropertyId, $intCid, $intCallTypeId, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valId( $intCallTypeId ) ) return NULL;

		$strSql	= ' SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						deleted_on IS NULL
						AND released_on IS NULL
						AND suspended_on IS NULL
						AND	property_id = ' . $intPropertyId . '
						AND cid = ' . $intCid . '
						AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumberByPropertyIdByCidByCallTypeId( $intPropertyId, $intCid, $intCallTypeId, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) || false == valId( $intCallTypeId ) ) return NULL;

		$strSql	= ' SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						deleted_on IS NULL
						AND	property_id = ' . $intPropertyId . '
						AND cid = ' . $intCid . '
						AND call_type_id = ' . ( int ) $intCallTypeId;

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCustomCallPhoneNumbersByPropertyIdsByCid( $arrintPropertyIds, $intCid, $objVoipDatabase ) {

		$strSql	= 'SELECT
						DISTINCT( cpn.* )
					FROM
						call_phone_numbers AS cpn
						JOIN call_types AS ct ON ( ct.id = cpn.call_type_id )
						LEFT JOIN ivrs AS i ON ( i.id = cpn.ivr_id )
					WHERE
						cpn.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
						AND cpn.cid = ' . ( int ) $intCid . '
						AND cpn.is_verified = true
						AND ct.id = cpn.call_type_id
						AND cpn.released_on IS NULL
						AND cpn.suspended_on IS NULL';

		return fetchData( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) return NULL;

		$strSql	= ' SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						cid = ' . $intCid . '
						AND	property_id = ' . $intPropertyId . '
						AND deleted_on IS NULL
						AND deleted_by IS NULL';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveCallPhoneNumbersByCallServiceProviderIdByExcludePhoneNumbers( $intCallServiceProviderId, $arrstrPhoneNumbers, $objVoipDatabase ) {
		if( false == valArr( $arrstrPhoneNumbers ) ) return false;

		$strSql = 'SELECT 
						*
					FROM 
						call_phone_numbers 
					WHERE 
						released_on IS NULL 
						AND released_by IS NULL 
						AND call_service_provider_id = ' . ( int ) $intCallServiceProviderId . ' 
						AND phone_number NOT IN (\'' . implode( '\',\'', $arrstrPhoneNumbers ) . '\' )';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersByEmployeeIds( $arrintEmployeeIds, $objVoipDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						id,
						phone_number,
						employee_id,
						phone_extension_id
					FROM
						call_phone_numbers 
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );

	}

	public static function fetchAllInactivatedCorporateCareCallPhoneNumbers( $objVoipDatabase ) {

		$strSql = 'SELECT
						cpn .*
					FROM 
						call_phone_numbers AS cpn
						LEFT JOIN company_call_settings AS ccs ON( ccs.cid = cpn.cid )
					WHERE
						call_type_id = ' . CCallType::CORPORATE_CARE_LINE . '
						AND ( ccs.is_activate_corporate_care_line IS false OR cpn.deleted_on IS NOT NULL )
						AND cpn.released_by IS NULL';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchInactivatedCorporateCareCallPhoneNumbersByCid( $intCid, $objVoipDatabase ) {

		$strSql = 'SELECT
						cpn .*
					FROM 
						call_phone_numbers AS cpn
						JOIN company_call_settings AS ccs ON( ccs . cid = cpn . cid )
					WHERE
						cpn.call_type_id = ' . CCallType::CORPORATE_CARE_LINE . '
						AND cpn.cid = ' . ( int ) $intCid . '
						AND ccs.is_activate_corporate_care_line IS false
						AND cpn.released_on IS NULL
						AND cpn.deleted_on IS NULL';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchOutboundDefaultCallPhoneNumberByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM
						call_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_sms_enabled IS TRUE
						AND ( details ->>\'is_outbound_default\' ) = \'true\'
						AND released_on IS NULL
						AND suspended_on IS NULL
					ORDER BY ID DESC
					LIMIT 1';

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );
	}

	public static function fetchCallPhoneNumbersSmsEnabledByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		$strSql	= 'SELECT
						DISTINCT( cpn.* ),
						ct.name AS call_type
					FROM
						call_phone_numbers AS cpn
						JOIN call_types AS ct ON ( ct.id = cpn.call_type_id )
					WHERE
						cpn.cid =  ' . ( int ) $intCid . '
						AND cpn.property_id = ' . ( int ) $intPropertyId . '
						AND ct.id = cpn.call_type_id
						AND cpn.is_sms_enabled IS TRUE
						AND cpn.released_on IS NULL
						AND cpn.suspended_on IS NULL
					ORDER BY
						cpn.id DESC';

		return self::fetchCallPhoneNumbers( $strSql, $objVoipDatabase );
	}

	public static function fetchActiveSmsEnabledCallPhoneNumberByPropertyIdByCid( $intPropertyId, $intCid, $objVoipDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						call_phone_numbers
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND is_sms_enabled IS TRUE
						AND deleted_on IS NULL
						AND suspended_on IS NULL
						AND released_on IS NULL
					ORDER BY id DESC
					LIMIT 1';

		return self::fetchCallPhoneNumber( $strSql, $objVoipDatabase );

	}

}
?>