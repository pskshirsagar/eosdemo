<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallAnalysisCallDisputeCategories
 * Do not add any new functions to this class.
 */

class CCallAnalysisCallDisputeCategories extends CBaseCallAnalysisCallDisputeCategories {

	public static function fetchCallAnalysisCallDisputeCategoriesByCallAnalysisCallDisputeId( $intCallAnalysisCallDisputeId, $objVoipDatabase ) {

		$strSql = 'SELECT
            cacdc.*,
            cac.name AS call_analysis_category_name
          FROM
            call_analysis_call_dispute_categories AS cacdc
            LEFT JOIN call_analysis_categories AS cac ON ( cac.id = cacdc.call_analysis_category_id )
          WHERE
            cacdc.call_analysis_call_dispute_id = ' . ( int ) $intCallAnalysisCallDisputeId;

		return self::fetchCallAnalysisCallDisputeCategories( $strSql, $objVoipDatabase );
	}

}
?>