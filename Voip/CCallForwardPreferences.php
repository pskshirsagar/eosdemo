<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Voip\CCallForwardPreferences
 * Do not add any new functions to this class.
 */

class CCallForwardPreferences extends CBaseCallForwardPreferences {

	public static function fetchCallForwardPreferences( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCallForwardPreference', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchCallForwardPreference( $strSql, $objVoipDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCallForwardPreference', $objVoipDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchActiveCallForwardPreferences( $objVoipDatabase ) {
		return parent::fetchCallForwardPreferences( 'SELECT *FROM call_forward_preferences WHERE is_published = true', $objVoipDatabase );
	}
}
?>